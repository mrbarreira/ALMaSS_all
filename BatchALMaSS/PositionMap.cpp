/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*//**
\file
\brief
<B>PositionMap.cpp This file contains the source for the PositionMap class</B> \n
*/
/**
\file
 by Chris J. Topping \n
 Version of June 2003 \n
 \n
 Doxygen formatted comments in July 2008 \n
*/
//---------------------------------------------------------------------------

#include "ALMaSS_Setup.h"
#include "../Landscape/ls.h"
#include "../Landscape/tole_declaration.h"
#include "../Landscape/maperrormsg.h"

#include "../BatchALMaSS/positionmap.h"

// 64-bit 32-bit compatability
#ifndef __64BIT
#define __3264minus1 31
#define __3264minus0 32
#define __3264divide 5
#else
#define __3264minus1 63
#define __3264minus0 64
#define __3264divide 6
#endif

//-----------------------------------------------------------------------------
SimplePositionMap::SimplePositionMap()
{
	;// Default do nothing constructor
}
//-----------------------------------------------------------------------------

SimplePositionMap::SimplePositionMap(Landscape * L)
{
  m_maxx=L->SupplySimAreaWidth();
  m_maxy=L->SupplySimAreaHeight();
  m_TheMap = new bool[ m_maxx*m_maxy ];
  for (unsigned y=0; y<m_maxy; y++)
  {
    for (unsigned x=0; x<m_maxx; x++)
    {
      ClearMapValue(x,y);
    }
  }
}
//-----------------------------------------------------------------------------

SimplePositionMap::SimplePositionMap(unsigned a_size)
{
  m_maxx=a_size;
  m_maxy=m_maxx;
  m_TheMap = new bool[ m_maxx*m_maxy ];
  for (unsigned y=0; y<m_maxy; y++)
  {
    for (unsigned x=0; x<m_maxx; x++)
    {
      ClearMapValue(x,y);
    }
  }
}
//-----------------------------------------------------------------------------

SimplePositionMap::~SimplePositionMap()
{
  delete m_TheMap;
}
//-----------------------------------------------------------------------------

PositionMap::PositionMap(Landscape * L)
{
  m_ALandscape = L;
  // must make sure that we have whole words
  m_xmaxx=m_ALandscape->SupplySimAreaWidth();
  m_maxx=(m_xmaxx+__3264minus1)/__3264minus0;
  m_maxy=m_ALandscape->SupplySimAreaHeight();
  m_TheMap = new PointerInt[ m_maxx*m_maxy ];
  Init();
}
//-----------------------------------------------------------------------------

PositionMap::PositionMap(unsigned a_size)
{
  // must make sure that we have whole words
  m_xmaxx=a_size;
  m_maxx=(m_xmaxx+__3264minus1)/__3264minus0;
  m_maxy=m_maxx;
  m_TheMap = new PointerInt[ m_maxx*m_maxy ];

  Init();
}
//-----------------------------------------------------------------------------

PositionMap::~PositionMap()
{
  delete m_TheMap;
}
//-----------------------------------------------------------------------------


void PositionMap::Init()
{
  for (unsigned y=0; y<m_maxy; y++)
  {
    for (unsigned x=0; x<m_xmaxx; x++)
    {
      ClearMapValue(x,y);
    }
  }
  // I guess there is a neater way to do this, but this works at least!
  m_TheBitMaskArray[0]=0;
  for (int i=1; i<__3264minus0; i++) {
	  m_TheBitMaskArray[i]=m_TheBitMaskArray[i-1]+(PointerInt(1) << (i-1));
  }
  for (int i=0; i<__3264minus0; i++) {
	  m_TheBitMaskArray[i]^= 0xFFFFFFFFFFFFFFFF;
	  m_TheBitMaskArray2[i]= ~m_TheBitMaskArray[i]; // 1s compliment
  }
#ifdef __64BIT
  m_TheBitMaskArray[__3264minus0] = 0xFFFFFFFFFFFFFFFF;
#else
  m_TheBitMaskArray[__3264minus0] = 0xFFFFFFFF;
#endif
  m_TheBitMaskArray2[__3264minus0]= 0;
}
//-----------------------------------------------------------------------------


void PositionMap::SetMapValue(unsigned x, unsigned y)
{

  PointerInt theBit= x & __3264minus1;
  // divide by 32
  // Calculate index:
  PointerInt index= (x>> __3264divide )+(m_maxx*y);
  m_TheMap[index] = m_TheMap[index] | (PointerInt(0x01) << theBit);

}
//-----------------------------------------------------------------------------


void PositionMap::ClearMapValue(unsigned x, unsigned y)
{
  PointerInt theBit= x & __3264minus1;

  // divide by 32
  // Calculate index:
  PointerInt index= (x>> __3264divide )+(m_maxx*y);
  m_TheMap[index] &= ~(0x01 << theBit);
}
//-----------------------------------------------------------------------------


int PositionMap::GetMapValue(unsigned x, unsigned y)
{
  PointerInt theBit= x & __3264minus1;
  // divide by 32
  // Calculate index:
  PointerInt index= (x>> __3264divide )+(m_maxx*y);
  PointerInt res =((m_TheMap[index] >> theBit) & 0x01); // 0-1
  return int(res);
}
//-----------------------------------------------------------------------------


int PositionMap::GetTotalSpace(int x, int y)
{
  // This is a debug function which returns the total empty area of the map
  int space=0;
  for (int i=0; i<x; i++)
  {
    for (int j=0; j<y; j++)
    {
      if (GetMapValue(i,j)==0) space++;
    }
  }
  return space;
}
//-----------------------------------------------------------------------------


int PositionMap::GetTotalN(int x, int y)
{
  // This is a debug function which returns the total filled area of the map
  int space=0;
  for (int i=0; i<x; i++)
  {
    for (int j=0; j<y; j++)
    {
      if (GetMapValue(i,j)!=0) space++;
    }
  }
  return space;
}
//-----------------------------------------------------------------------------


int PositionMap::GetMapDensity(unsigned x, unsigned y)
{
  if (y==0) y=1;

  PointerInt TheTotal=0;
  PointerInt theBit= (x-1) & __3264minus1; // Identifies which bit to check
  PointerInt index= (x>> __3264divide )+(m_maxx*(y+1));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y-1));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  theBit= (x+1) & __3264minus1;
  index= (x>> __3264divide )+(m_maxx*(y+1));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y-1));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  theBit= x & __3264minus1;
  index= (x>> __3264divide )+(m_maxx*(y+1));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y-1));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  return int(TheTotal);
}
//-----------------------------------------------------------------------------

int PositionMap::GetMapDensity(unsigned x, unsigned y, unsigned range)
{
	// THIS METHOD IS UNTESTED AND PROBABLY IS BUGGY!!

	// This method is called with the topleft coord as x,y - will probably need range checking in the calling function!
	// for negative co-ords
	// Efficiency is high for large areas, for small areas then it is less efficient than testing each square
	// NB this method will automatically wrap-round  if it meets the edge of X co-ord or Y-Coord
	PointerInt TheTotal=0;
	PointerInt theBit= x & __3264minus1; // Identifies which bit to check first e.g.  49 & 31 = 17
	PointerInt index= ( x>> __3264divide  )+( m_maxx * ( y ) ); // This is the start index
	PointerInt yextent=y+range;
	if (yextent>m_maxy) {
		yextent=yextent-m_maxy;
		// This code is duplicated just to avoid the extra call and parameter pass
		// NB BitCount is inline.
		for (PointerInt yy=y; yy<yextent; yy++) { // Do all this for each y
			PointerInt remain=range-(__3264minus0-theBit); // e.g 55-17=38
			if (remain>=0) { // Need to continue
			  // We need the rest of the 32 bits
			  TheTotal+=BitCount(m_TheMap[index]& m_TheBitMaskArray[theBit]); // NB if we are at the last bit then the mask is 0x08000 not 0x00001 !!
			}
			else {
				// Stop this y here because remain is negative
				// We need all the rest of the bits from our start point (theBit) to theBit+range
				// e.g. theBit=17, remain = 3, we needs bits 17,18,19
				TheTotal+=BitCount(m_TheMap[index]& (m_TheBitMaskArray[theBit]^m_TheBitMaskArray[theBit+range]));
				remain=0;
			}
			// Now if remain is >32 we need to take a whole PointerInt else only the low remain bits
			while (remain>0) {
				if (remain>__3264minus0) {
					TheTotal+=BitCount(m_TheMap[++index]);
				} else {
					TheTotal+=BitCount(m_TheMap[++index]& m_TheBitMaskArray2[remain]); // Use a second array to avoid the extra operation of inverting the first
				}
				remain-=__3264minus0;
			}
		}
		yextent=m_maxy;
	}
	for (PointerInt yy=y; yy<yextent; yy++) { // Do all this for each y
		PointerInt remain=range-(__3264minus0-theBit); // e.g 55-17=38
		if (remain>=0) { // Need to continue
		  // We need the rest of the 32 bits
		  TheTotal+=BitCount(m_TheMap[index]& m_TheBitMaskArray[theBit]); // NB if we are at the last bit then the mask is 0x08000 not 0x00001 !!
		}
		else {
			// Stop this y here because remain is negative
			// We need all the rest of the bits from our start point (theBit) to theBit+range
			// e.g. theBit=17, remain = 3, we needs bits 17,18,19
			TheTotal+=BitCount(m_TheMap[index]& (m_TheBitMaskArray[theBit]^m_TheBitMaskArray[theBit+range]));
			remain=0;
		}
		// Now if remain is >32 we need to take a whole PointerInt else only the low remain bits
		while (remain>0) {
			if (remain>__3264minus0) {
				TheTotal+=BitCount(m_TheMap[++index]);
			} else {
				TheTotal+=BitCount(m_TheMap[++index]& m_TheBitMaskArray2[remain]); // Use a second array to avoid the extra operation of inverting the first
			}
			remain-=__3264minus0;
		}
	}
	return int(TheTotal);
}
//-----------------------------------------------------------------------------
bool PositionMap::GetMapPositive(unsigned x, unsigned y, unsigned range)
{
	// THIS METHOD IS UNTESTED AND PROBABLY IS BUGGY!!

	// This method is called with the topleft coord as x,y - will probably need range checking in the calling function!
	// for negative co-ords
	// Efficiency is high for large areas, for small areas then it is less efficient than testing each square
	// NB this method will automatically wrap-round  if it meets the edge of X co-ord or Y-Coord
	PointerInt theBit= PointerInt(x) & __3264minus1; // Identifies which bit to check first e.g.  49 & 31 = 17
	PointerInt index= ( PointerInt(x)>> __3264divide  )+( m_maxx * ( PointerInt(y) ) ); // This is the start index
	PointerInt yextent=y+range;
	if (yextent>m_maxy) {
		yextent=yextent-m_maxy;
		// This code is duplicated just to avoid the extra call and parameter pass
		// NB BitCount is inline.
		for (unsigned yy=y; yy<yextent; yy++) { // Do all this for each y
			PointerInt remain=range-(__3264minus0-theBit); // e.g 55-17=38
			if (remain>=0) { // Need to continue
			  // We need the rest of the 32 bits
			  if ((m_TheMap[index]& m_TheBitMaskArray[theBit])!=0) return true; // NB if we are at the last bit then the mask is 0x08000 not 0x00001 !!
			}
			else {
				// Stop this y here because remain is negative
				// We need all the rest of the bits from our start point (theBit) to theBit+range
				// e.g. theBit=17, remain = 3, we needs bits 17,18,19
				if ((m_TheMap[index]& (m_TheBitMaskArray[theBit]^m_TheBitMaskArray[theBit+range]))!=0) return true;
				remain=0;
			}
			// Now if remain is >32 we need to take a whole PointerInt else only the low remain bits
			while (remain>0) {
				if (remain>__3264minus0) {
					if(m_TheMap[++index]!=0) return true;
				} else {
					if ((m_TheMap[++index]& m_TheBitMaskArray2[remain])!=0) return true; // Use a second array to avoid the extra operation of inverting the first
				}
				remain-=__3264minus0;
			}
		}
		yextent=m_maxy;
	}
	for (PointerInt yy=y; yy<yextent; yy++) { // Do all this for each y
		PointerInt remain=range-(__3264minus0-theBit); // e.g 55-17=38
		if (remain>=0) { // Need to continue
		  // We need the rest of the 32 bits
		  if ((m_TheMap[index]& m_TheBitMaskArray[theBit])!=0) return true; // NB if we are at the last bit then the mask is 0x08000 not 0x00001 !!
		}
		else {
			// Stop this y here because remain is negative
			// We need all the rest of the bits from our start point (theBit) to theBit+range
			// e.g. theBit=17, remain = 3, we needs bits 17,18,19
			if ((m_TheMap[index]& (m_TheBitMaskArray[theBit]^m_TheBitMaskArray[theBit+range]))!=0) return true;
			remain=0;
		}
		// Now if remain is >32 we need to take a whole PointerInt else only the low remain bits
		while (remain>0) {
			if (remain>__3264minus0) {
				if(m_TheMap[++index]!=0) return true;
			} else {
				if ((m_TheMap[++index]& m_TheBitMaskArray2[remain])!=0) return true; // Use a second array to avoid the extra operation of inverting the first
			}
			remain-=__3264minus0;
		}
	}
	return false;
}
//-----------------------------------------------------------------------------


int PositionMap::GetMapDensity5x5(unsigned x, unsigned y)
{
  if (y<2) y=2; // Don't run over the world backwards - big -ve number results
  if (x<2) x=2;

  PointerInt index;
  PointerInt TheTotal=0;
  PointerInt theBit= (x-2) & __3264minus1; // Identifies which bit to check
  index = (x>> __3264divide )+(m_maxx*(y+2));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y+1));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y-1));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y-2));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);

  theBit= (x-1) & __3264minus1; // Identifies which bit to check
  index = (x>> __3264divide )+(m_maxx*(y+2));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y+1));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y-1));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y-2));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);

  theBit= x & __3264minus1;
  index = (x>> __3264divide )+(m_maxx*(y+2));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y+1));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y-1));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y-2));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);

  theBit= (x+1) & __3264minus1;
  index = (x>> __3264divide )+(m_maxx*(y+2));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y+1));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y-1));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y-2));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);

  theBit= (x+2) & __3264minus1;
  index = (x>> __3264divide )+(m_maxx*(y+2));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y+1));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y-1));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  index= (x>> __3264divide )+(m_maxx*(y-2));
  TheTotal+=((m_TheMap[index] >> theBit) & 0x01);
  return int(TheTotal);
}
//-----------------------------------------------------------------------------

bool PositionMap::GetMapDensity32(unsigned x, unsigned y)
{
  // Returns true if there is another animal in 32x32m

  // IMPORTANT - this function does not include bounds checking
  // Bounds checking on Y is needed before the call here

  // this is an odd density method. It assumes that all that is important is
  // whether there is another beetle within the 32m represented by each point
  // So two beetles could be side by side, but in different uints and not kill
  // each other, but conversely, 31 m apart, in the same one and kill each other
  // This function is therefore only used as a speed optimisation
  //
  PointerInt index;
  PointerInt theBit; // Identifies which bit to check
  for (unsigned int yoff=y-16; yoff<y; yoff++)
  {
    index = (x>> __3264divide )+(m_maxx*(yoff));
    if (m_TheMap[index]>0) return true;
  }
  // Special case for 'y'
  theBit= x & __3264minus1; // Identifies which bit to check
  theBit=PointerInt(1)<<theBit;
  index = (x>> __3264divide )+(m_maxx*(y));;
  if((m_TheMap[index]^theBit)>0)
     return true; // use XOR to remove the calling beetle

  for (unsigned int yoff=y+1; yoff<y+16; yoff++)
  {
    index = (x>> __3264divide )+(m_maxx*(yoff));
    if (m_TheMap[index]>0) return true;
  }
  return false;
}
//-----------------------------------------------------------------------------

bool PositionMap::GetMapPositiveB(unsigned x, unsigned y, unsigned range)
{
	// This is a speed optimisation. We assume that we never need to go outside our actual uint
	// so if our x is at 25 and range is 10 then we use x-3 (35-32) as the start point.

	PointerInt theBit= x & __3264minus1; // Identifies which bit to check first e.g.  49 & 31 = 17
	PointerInt index= ( x>> __3264divide  )+( m_maxx * ( y ) ); // This is the start index
	// Create the mask
	//theBit has 0-31
	PointerInt n1=theBit+range;
	PointerInt n2=(n1 & 32) << __3264divide;	// check the 6th bit (or 7th in 64-bit)
									// shift this down to the first bit
	// Get the excess bits of our total range (will be 4 if n1 == 35) or zero if n2 is zero
	PointerInt n5=theBit-((n1-__3264minus1)*n2);  // move theBit along so that we don't over run the uint
	// We now need from theBit bit to theBit+range mask - done fast because we have look up tables
	PointerInt mask=(m_TheBitMaskArray[n5]^m_TheBitMaskArray[n5+range]);
	PointerInt yextent=y+range;
	if (yextent>m_maxy) yextent=m_maxy;
	for (PointerInt yy=y; yy<yextent; yy++) { // Do all this for each y
		//res=res|(m_TheMap[index]&mask); // Can also test for non-zero here, but in a sparse matrix this slows things down too much
		if (m_TheMap[index]&mask) return true; // Can also test for non-zero here, but in a sparse matrix this slows things down too much
		index+=m_maxx;
	}
//	if (res) return true; else return false;
	return false;
}
//-----------------------------------------------------------------------------


ScalablePositionMap::ScalablePositionMap(Landscape * L, int a_ScaleFactor)
	: SimplePositionMap()
{
	m_ScaleFactor = a_ScaleFactor;

	m_maxx=(L->SupplySimAreaWidth()) >> m_ScaleFactor;
	m_maxy=(L->SupplySimAreaHeight())>> m_ScaleFactor;
	m_TheMap = new bool[ m_maxx*m_maxy ];
	for (unsigned y=0; y<m_maxy; y++)
	{
		for (unsigned x=0; x<m_maxx; x++)
		{
			m_TheMap[x+m_maxx*y] = false;
		}
	}
}
//-----------------------------------------------------------------------------


ScalablePositionMap::~ScalablePositionMap()
{
  ;
}
//-----------------------------------------------------------------------------
