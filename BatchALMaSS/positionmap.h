/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*//**
\file
\brief
<B>PositionMap.h This file contains the headers for the PositionMap class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 Version of June 2003 \n
 \n
 Doxygen formatted comments in July 2008 \n
*/
//---------------------------------------------------------------------------
#ifndef PositionMapH
#define PositionMapH
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

/**
\brief
Used to map locations of individuals for density estimates - space inefficient but good for testing
*/
class SimplePositionMap
{
 public:
	bool* m_TheMap;
	unsigned int m_maxx, m_maxy;
	virtual bool GetMapValue(unsigned a_x, unsigned a_y) 
	{
		return m_TheMap[a_x+a_y*m_maxx];
	}
	
	virtual void SetMapValue(unsigned a_x, unsigned a_y)
	{
		m_TheMap[a_x+a_y*m_maxx]=true;
	}
	
	virtual void ClearMapValue(unsigned a_x, unsigned a_y)
	{
		m_TheMap[a_x+a_y*m_maxx]=false;
	}
	
	virtual int GetMapDensity(unsigned a_x, unsigned a_y, unsigned a_range)
	{
		int dens=0;
		for (unsigned x = a_x; x<a_x+a_range; x++) 
		{
			for (unsigned y = a_y; y<a_y+a_range; y++) 
				if (m_TheMap[x+y*m_maxx]) dens++;
		}
		return dens;
	}
	virtual bool GetMapPositive(unsigned a_x, unsigned a_y, unsigned a_range)
	{
		for (unsigned x = a_x; x<a_x+a_range; x++) 
		{
			for (unsigned y = a_y; y<a_y+a_range; y++) 
				if (m_TheMap[x+y*m_maxx]) return true;
		}
		return false;
	}
	SimplePositionMap();
	SimplePositionMap(unsigned a_size);
	SimplePositionMap(Landscape* L);
	virtual ~SimplePositionMap();
};

/**
\brief
Used to map locations of individuals for density estimates. Each cell can only contain one value (it is binary).
*/
class ScalablePositionMap : public SimplePositionMap
{
 protected:
	 /** \brief A binary scaling factor ( 1 = 2x2m 2=4x4m 3 =8x8m etc.. ) */
	 int m_ScaleFactor;
 public:
	 ScalablePositionMap(Landscape* L, int a_ScaleFactor);
	 ~ScalablePositionMap();

	 /** \brief Get the value a x,y */
	 bool GetMapValue(unsigned a_x, unsigned a_y) 
	 {
		 /** 
		 * We assume that the calling code uses maximum resolution, and that all scaling is done here. So the call is e.g. in m, but
		 * the return value is calculated per the m_ScaleFactor.\n
		 * @param a_x x-coordinate in the raw system metric (e.g. metres).
		 * @param a_y y-coordinate in the raw system metric (e.g. metres).
		 * @return bool - whether the cell specified is occupied (true) or not (false)
		 */
		 a_x = a_x >> m_ScaleFactor;
		 a_y = a_y >> m_ScaleFactor;
		 return m_TheMap[a_x+a_y*m_maxx];
	 }
	 void SetMapValue(unsigned a_x, unsigned a_y)
	 {
		 a_x = a_x >> m_ScaleFactor;
		 a_y = a_y >> m_ScaleFactor;
		 m_TheMap[a_x+a_y*m_maxx]=true;
	 }
	 void ClearMapValue(unsigned a_x, unsigned a_y)
	 {
		 a_x = a_x >> m_ScaleFactor;
		 a_y = a_y >> m_ScaleFactor;
		 m_TheMap[a_x+a_y*m_maxx]=false;
	 }
	 int GetMapDensity(unsigned a_x, unsigned a_y, unsigned a_range)
	 {
		 int dens=0;
		 a_x = a_x >> m_ScaleFactor;
		 a_y = a_y >> m_ScaleFactor;
		 a_range = a_range >> m_ScaleFactor;
		 for (unsigned x = a_x; x<a_x+a_range; x++) 
		 {
			 for (unsigned y = a_y; y<a_y+a_range; y++) 
				 if (m_TheMap[x+y*m_maxx]) dens++;
		 }
		 return dens;
	 }
	 bool GetMapPositive(unsigned a_x, unsigned a_y, unsigned a_range)
	 {
		 a_x = a_x >> m_ScaleFactor;
		 a_y = a_y >> m_ScaleFactor;
		 a_range = a_range >> m_ScaleFactor;
		 for (unsigned x = a_x; x<a_x+a_range; x++) 
		 {
			 for (unsigned y = a_y; y<a_y+a_range; y++) 
				 if (m_TheMap[x+y*m_maxx]) return true;
		 }
		 return false;
	 }
};
//-------------------------------------------------------------------------------

/**
\brief
Used to map locations of individuals for density estimates
*/
class PositionMap
{
 public:
  PointerInt* m_TheMap;
  PointerInt m_xmaxx, m_maxx, m_maxy;
  PointerInt m_TheBitMaskArray[65]; // for 64-bit
  PointerInt m_TheBitMaskArray2[65];
  int GetMapValue(unsigned x, unsigned y);
  void SetMapValue(unsigned x, unsigned y);
  void ClearMapValue(unsigned x, unsigned y);
  int GetMapDensity(unsigned x, unsigned y);
  int GetMapDensity(unsigned x, unsigned y, unsigned range);
  bool GetMapPositive(unsigned x, unsigned y, unsigned range);
  bool GetMapPositiveB(unsigned x, unsigned y, unsigned range);
  int GetMapDensity5x5(unsigned x, unsigned y);
  bool GetMapDensity32(unsigned x, unsigned y);
  int GetTotalSpace(int x, int y);
  int GetTotalN(int x, int y);
  PositionMap(unsigned a_size);
  PositionMap(Landscape * L);
  ~PositionMap();
 protected:
  Landscape* m_ALandscape;
  void Init();


// Inline function for speed
PointerInt BitCount (PointerInt n) {
	unsigned int nCount=0 ;
	for(; n; n&=(n-1)){
		nCount++;
	}
	return nCount ;
}

};
//------------------------------------------------------------------------------



//---------------------------------------------------------------------------
#endif
