/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>Movementmap.h This file contains the headers for the MovementMap class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 Version of June 2003 \n
 \n
 Doxygen formatted comments in July 2008 \n
*/
//---------------------------------------------------------------------------
#ifndef MovementMapH
#define MovementMapH

//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
class TAnimal;

/**
\brief
Movement maps are used for rapid computing of animal movement
*/
class MovementMap
{
 public:
  uint32* m_TheMap;
  uint32 maxx;
  uint32 maxy;
  int GetMapValue(unsigned x, unsigned y);
  void SetMapValue(unsigned x, unsigned y, unsigned value);
  void SetMapValue0(unsigned x, unsigned y);
  void SetMapValue1(unsigned x, unsigned y);
  void SetMapValue2(unsigned x, unsigned y);
  void SetMapValue3(unsigned x, unsigned y);
  MovementMap(Landscape * L, int spref);
  ~MovementMap();
 protected:
  Landscape* m_ALandscape;
  void Init(int spref);
};
//------------------------------------------------------------------------------

/**
\brief
Movement maps are used for rapid computing of animal movement
*/
class MovementMap16
{
 public:
  uint32* m_TheMap;
  uint32 maxx;
  uint32 maxy;
  int GetMapValue(unsigned x, unsigned y);
  void SetMapValue(unsigned x, unsigned y, unsigned value);
  void ClearMapValue(unsigned x, unsigned y);
  MovementMap16(Landscape * L);
  ~MovementMap16();
 protected:
  Landscape* m_ALandscape;
  void Init();
};
//------------------------------------------------------------------------------


/**
\brief
Used to map locations of animals in space
*/
template <class a_type> class IDMap
{
public:
	vector<a_type> m_TheMap;
	int maxx;
	int maxy;
	a_type GetMapValue(unsigned x, unsigned y) {
		return m_TheMap[x + (maxx*y)];
	}
	void SetMapValue(unsigned x, unsigned y, a_type p) {
		m_TheMap[x + (maxx*y)] = p;
	}
	void ClearMapValue(unsigned x, unsigned y) {
		m_TheMap[x + (maxx*y)] = NULL;
	}
	IDMap(Landscape * L) {
		maxx = L->SupplySimAreaWidth();
		maxy = L->SupplySimAreaHeight();
		m_TheMap.resize(maxx*maxy);
		for (int y = 0; y<maxy; y++) {
			for (int x = 0; x<maxx; x++) {
				SetMapValue(x, y, NULL);
			}
		}
	}
	~IDMap() {
		;
	}
};
//------------------------------------------------------------------------------


/**
\brief
Used to map locations of animals in space
*/
class IDMapScaled : public IDMap<TAnimal*>
{
public:
	int m_scale;
	TAnimal* GetMapValue(unsigned a_x, unsigned a_y) {
		/** On calling the a_x,a_y are in map coordinates not grid coords */
		a_x /= m_scale;
		a_y /= m_scale;
		return m_TheMap[a_x + (maxx*a_y)];
	}
	void SetMapValue(unsigned a_x, unsigned a_y, TAnimal* p) {
		/** On calling the a_x,a_y are in map coordinates not grid coords */
		a_x /= m_scale;
		a_y /= m_scale;
		m_TheMap[a_x + (maxx*a_y)] = p;
	}
	void ClearMapValue(unsigned a_x, unsigned a_y){
		/** On calling the a_x,a_y are in map coordinates not grid coords */
		a_x /= m_scale;
		a_y /= m_scale;
		m_TheMap[a_x + (maxx*a_y)] = NULL;
	}
	IDMapScaled(Landscape * L, int a_gridsize);
	~IDMapScaled();
};
//------------------------------------------------------------------------------


//---------------------------------------------------------------------------
#endif
