/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>Movementmap.cpp This file contains the source for the MovementMap class</B> \n
*/
/**
\file
 by Chris J. Topping \n
 Version of June 2003 \n
 All rights reserved. \n
 \n
 Doxygen formatted comments in July 2008 \n
*/
//---------------------------------------------------------------------------

#define _CRT_SECURE_NO_DEPRECATE
#include "ALMaSS_Setup.h"
#include "../Landscape/ls.h"
#include "../Landscape/tole_declaration.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../Landscape/maperrormsg.h"


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

MovementMap::MovementMap(Landscape * L, int spref)
{
  m_ALandscape = L;
  // must make sure that we have whole words
  maxx=(m_ALandscape->SupplySimAreaWidth()+15)/16;
  maxy=m_ALandscape->SupplySimAreaHeight();
  m_TheMap = new uint32[ maxx*maxy ];

  Init(spref);
}
//-----------------------------------------------------------------------------

MovementMap::~MovementMap()
{
  delete m_TheMap;
}
//-----------------------------------------------------------------------------


void MovementMap::Init(int spref)
{
  char error_num[20];
  for (int y=0; y<m_ALandscape->SupplySimAreaHeight(); y++)
  {
    for (int x=0; x<m_ALandscape->SupplySimAreaWidth(); x++)
    {
      TTypesOfLandscapeElement tole;
      tole=m_ALandscape->SupplyElementType(x,y);
      uint32 colour=0;
	  if (spref==0)
	  {
		  // Beetle
		  switch (tole)
		  {

		  case tole_Hedges: // 130
		  case tole_RoadsideVerge: // 13
		  case tole_FieldBoundary: // 160
		  case tole_HedgeBank:
		  case tole_BeetleBank:
		  case tole_NaturalGrassWet:
		  case tole_RoadsideSlope:
		  case tole_WaterBufferZone:
			  colour=1;
			  break;
		  case tole_Marsh: // 95
		  case tole_Scrub: // 70
		  case tole_Railway: // 118
		  case tole_PermanentSetaside: // 33
		  case tole_RiversidePlants: // 98
		  case tole_PitDisused: // 75
		  case tole_Garden: //11
		  case tole_Track:  // 123
		  case tole_SmallRoad:  // 122
		  case tole_LargeRoad:  // 121
		  case tole_MetalledPath:
		  case tole_Carpark:
		  case tole_Churchyard:
		  case tole_Saltmarsh:
		  case tole_PlantNursery:
		  case tole_Vildtager:
		  case tole_HeritageSite:			  colour = 2;
		  case tole_Copse:
		  case tole_WoodyEnergyCrop:
		  case tole_WoodlandMargin:
		  case tole_IndividualTree:
		  case tole_RiversideTrees: // 97
		  case tole_DeciduousForest: // 40
		  case tole_MixedForest:     // 60
		  case tole_ConiferousForest: // 50
		  case tole_YoungForest:
		  case tole_StoneWall: // 15
		  case tole_ActivePit: // 115
		  case tole_Fence: // 225
			  break;
		  case tole_Field:  // 20 & 30
		  case tole_UnsprayedFieldMargin :
		  case tole_PermPasture: // 35
		  case tole_PermPastureLowYield: // 35
		  case tole_PermPastureTussocky: // 26
		  case tole_PermPastureTussockyWet:
		  case tole_Heath:
		  case tole_NaturalGrassDry: // 110
		  case tole_AmenityGrass:
		  case tole_Parkland:
		  case tole_Orchard:  
		  case tole_OrchardBand:
		  case tole_MownGrass:  
		  case tole_Wasteland: // 209
		  case tole_UnknownGrass:
			  colour=0;
			  break;
		  case tole_Building: // 5
		  case tole_Freshwater: // 90
		  case tole_FishFarm: // 220
		  case tole_Pond:
		  case tole_River: // 96
		  case tole_Saltwater:  // 80
		  case tole_Coast: // 100
		  case tole_BareRock: // 59
		  case tole_UrbanNoVeg:
		  case tole_UrbanVeg:
		  case tole_UrbanPark:
		  case tole_BuiltUpWithParkland:
		  case tole_SandDune:
	 	  case tole_Stream:
		  case tole_Pylon:
		  case tole_WindTurbine:
		  case tole_DrainageDitch:
		  case tole_Canal:
			  colour = 3;
			  break;
			case tole_Foobar: // 999   !! type unknown - should not happen
			default:
			 sprintf( error_num, "%d", tole );
			  g_msg->Warn( WARN_FILE,
			   "MovementMap::Init(): Unknown landscape element type:",
			 error_num );
			 exit( 1 );
		}
	}
    else if (spref==1) {  // Partridge
      switch (tole)
      {
        case tole_Hedges: // 130
        case tole_PermanentSetaside: // 33
        case tole_RoadsideVerge: // 13
        case tole_FieldBoundary: // 160
        case tole_HedgeBank:
        case tole_BeetleBank:
        case tole_UnsprayedFieldMargin: // 31
		case tole_Vildtager:
		case tole_WaterBufferZone:
         colour=3;
         break;
        case tole_Railway: // 118
        case tole_Scrub: // 70
        case tole_Field:  // 20 & 30
        case tole_PermPasture: // 35
        case tole_NaturalGrassDry: // 110
        case tole_RiversidePlants: // 98
	 	case tole_RoadsideSlope: 
        case tole_RiversideTrees: // 97
        case tole_YoungForest:  //55
		case tole_Wasteland:
		case tole_UnknownGrass:
			colour=2;
         break;
        case tole_PitDisused: // 75
		case tole_NaturalGrassWet:
        case tole_Marsh: // 95
        case tole_Garden: //11
        case tole_Track:  // 123
        case tole_SmallRoad:  // 122
        case tole_LargeRoad:  // 121
         colour=1;
         break;
		case tole_IndividualTree:
		case tole_PlantNursery:
		case tole_WindTurbine:
		case tole_WoodyEnergyCrop:
		case tole_WoodlandMargin:
		case tole_Pylon:
		case tole_DeciduousForest: // 40
        case tole_MixedForest:     // 60
        case tole_ConiferousForest: // 50
        case tole_StoneWall: // 15
		case tole_Fence: // 225
        case tole_Building: // 5
        case tole_ActivePit: // 115
		case tole_Pond:
		case tole_Freshwater: // 90
		case tole_FishFarm:
        case tole_River: // 96
        case tole_Saltwater:  // 80
        case tole_Coast: // 100
        case tole_BareRock: // 59
		case tole_AmenityGrass:
		case tole_Parkland:
		case tole_UrbanNoVeg:
		case tole_UrbanPark:
		case tole_BuiltUpWithParkland:
		case tole_SandDune:
		case tole_Copse:
		case tole_MetalledPath:
		case tole_Carpark:
		case tole_Churchyard:
		case tole_Saltmarsh:
		case tole_Stream:
		case tole_HeritageSite:
			colour=0;
			break;
		case tole_Foobar: // 999   !! type unknown - should not happen
        default:
			sprintf( error_num, "%d", tole );
			g_msg->Warn( WARN_FILE,
				"MovementMap::Init(): Unknown landscape element type:",
				error_num );
			exit( 1 );
	  }
	}  // End partridge
	else if (spref==2)
	{  // Spider
		switch ( tole )
		{
		case tole_IndividualTree:
		case tole_WindTurbine:
		case tole_WoodyEnergyCrop:
		case tole_WoodlandMargin:
		case tole_Pylon:
		case tole_NaturalGrassWet:
		case tole_MetalledPath:
		case tole_Carpark:
		case tole_Churchyard:
		case tole_Saltmarsh:
		case tole_HeritageSite:
		case tole_Hedges:
		case tole_HedgeBank:
        case tole_BeetleBank:
		case tole_Marsh:
		case tole_PermanentSetaside:
		case tole_NaturalGrassDry:
		case tole_RiversidePlants:
		case tole_RiversideTrees:
		case tole_DeciduousForest:
		case tole_MixedForest:
		case tole_ConiferousForest:
		case tole_YoungForest:
		case tole_StoneWall:
		case tole_Fence:
		case tole_Building:
		case tole_ActivePit:
		case tole_PitDisused:
		case tole_Scrub:
		case tole_Track:
		case tole_SmallRoad:
		case tole_LargeRoad:
        case tole_BareRock:
		case tole_UrbanNoVeg:
		case tole_SandDune:
		case tole_Copse:
		case tole_MownGrass:
			colour = 2;
			break;
		case tole_PlantNursery:
		case tole_Vildtager:
		case tole_RoadsideSlope:
		case tole_PermPasture: //moved from case 2 to case 3
		case tole_PermPastureLowYield:
		case tole_PermPastureTussocky: //moved from case 2 to case 3
		case tole_RoadsideVerge:
		case tole_Railway:
		case tole_FieldBoundary:
		case tole_Garden:
		case tole_Coast:
		case tole_Heath:
		case tole_AmenityGrass:
		case tole_Parkland:
		case tole_BuiltUpWithParkland:
		case tole_UrbanPark:
		case tole_Wasteland:
		case tole_WaterBufferZone:
			colour = 3;
			break;
		case tole_Pond:
		case tole_Freshwater:
		case tole_FishFarm:
		case tole_River:
		case tole_Saltwater:
		case tole_Stream:
			colour=0;
			break;
		case tole_Field: // Special case
		case tole_UnsprayedFieldMargin:  // Special case
			colour=1;
			break;
		default:
			sprintf( error_num, "%d", tole );
			g_msg->Warn( WARN_FILE,
				"MovementMap::Init(): Unknown landscape element type:",
				error_num );
			exit( 1 );
		}// End spider
	}
	SetMapValue(x,y,colour);
	}
  }
}
//-----------------------------------------------------------------------------


void MovementMap::SetMapValue(unsigned x, unsigned y, unsigned value)
{

  uint32 theBit= x & 15;  // Identify the bit within 16 possibilities

  // Multiply by two
  theBit=theBit<<1;       // Double it because we use two bits per location

  // divide by 16
  // Calculate index:
  uint32 index= (x>>4)+(y*maxx); // maxx is the number of uints per row + x/16 because there are 16 locations per uint

  value &= 0x03;
  uint32 NewVal = m_TheMap[index] & ~( 0x03 << theBit ); // Make a hole two bits wide at the right location
  m_TheMap[index] = NewVal | (value << theBit); // Fill the hole

}
//-----------------------------------------------------------------------------


void MovementMap::SetMapValue0(unsigned x, unsigned y)
{
  uint32 theBit=x & 15;
  theBit=theBit<<1; // Multiply by two
  // divide by 16
  uint32 index= (x>>4)+(y*maxx);
  m_TheMap[index]  &= ~(0x03<< theBit);
}
//-----------------------------------------------------------------------------


void MovementMap::SetMapValue1(unsigned x, unsigned y)
{
  uint32 theBit=x & 15;
  theBit=theBit<<1; // Multiply by two
  // divide by 16
  uint32 index= (x>>4)+(maxx*y);
  m_TheMap[index]  &= ~(0x03 << theBit);
  m_TheMap[index]  |=  (0x01 << theBit);
}
//-----------------------------------------------------------------------------


void MovementMap::SetMapValue2(unsigned x, unsigned y)
{
  uint32 theBit=x & 15;
  // Multiply by two
  theBit=theBit<<1;
  // divide by 16
  uint32 index= (x>>4)+(maxx*y);
  m_TheMap[index] &= ~(0x03 << theBit);
  m_TheMap[index]  |= (0x02 << theBit);
}
//-----------------------------------------------------------------------------


void MovementMap::SetMapValue3(unsigned x, unsigned y)
{
  uint32 theBit=x & 15;
  // Multiply by two
  theBit=theBit<<1;
  // divide by 16
  uint32 index= (x>>4)+(maxx*y);
  m_TheMap[index]  |= (0x03 << theBit);
}
//-----------------------------------------------------------------------------


int MovementMap::GetMapValue(unsigned x, unsigned y)
{
  uint32 theBit=x & 15;
  // Multiply by two
  theBit=theBit<<1;
  // divide by 16
  uint32 index= (x>>4)+(maxx*y);
  return ((m_TheMap[index] >> theBit) & 0x03); // 0-3
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------

MovementMap16::MovementMap16(Landscape * L)
{
  m_ALandscape = L;
  // must make sure that we have whole words
  maxx=(m_ALandscape->SupplySimAreaWidth()+1)/2;
  maxy=m_ALandscape->SupplySimAreaHeight();
  m_TheMap = new uint32[ maxx*maxy ];

  Init();
}
//-----------------------------------------------------------------------------

MovementMap16::~MovementMap16()
{
  delete m_TheMap;
}
//-----------------------------------------------------------------------------


void MovementMap16::Init()
{

  for (int y=0; y<m_ALandscape->SupplySimAreaHeight(); y++)
  {
    for (int x=0; x<m_ALandscape->SupplySimAreaWidth(); x++)
    {
      SetMapValue(x,y,0);
    }
  }
}
//-----------------------------------------------------------------------------


void MovementMap16::SetMapValue(unsigned x, unsigned y, unsigned value)
{

  uint32 theWord= x & 1; // is the last bit set?

  // Multiply by 16o
  theWord=theWord<<4;

  // divide by 16
  // Calculate index:
  uint32 index= (x>>1)+(y*maxx);

  value &= 0xFF;
  uint32 NewVal = m_TheMap[index] & ~( 0xFFFF << theWord );
  m_TheMap[index] = NewVal | (value << theWord);

}
//-----------------------------------------------------------------------------


void MovementMap16::ClearMapValue(unsigned x, unsigned y)
{
  uint32 theWord=x & 1; // is the last bit set?
  theWord=theWord<<4; // Multiply by 16
  // divide by 2
  uint32 index= (x>>1)+(y*maxx); // y and x/2
  m_TheMap[index]  &= ~(0xFFFF<< theWord);
}
//-----------------------------------------------------------------------------

int MovementMap16::GetMapValue(unsigned x, unsigned y)
{
  uint32 theWord=x & 1;
  // Multiply by 16
  theWord=theWord<<4;
  // divide by 16
  uint32 index= (x>>1)+(maxx*y);
  return ((m_TheMap[index] >> theWord) & 0xFFFF); //
}
//-----------------------------------------------------------------------------

IDMapScaled::IDMapScaled(Landscape * L, int a_gridsize) : IDMap<TAnimal*>(L)
{
	m_scale = a_gridsize;
	maxx = (L->SupplySimAreaWidth() / m_scale) + 1;
	maxy = (L->SupplySimAreaHeight() / m_scale) + 1;
	m_TheMap.resize(maxx*maxy);
	for (int y = 0; y<maxy; y++)
	{
		for (int x = 0; x<maxx; x++)
		{
			SetMapValue(x, y, NULL);
		}
	}
}
//-----------------------------------------------------------------------------

IDMapScaled::~IDMapScaled()
{
	;
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


