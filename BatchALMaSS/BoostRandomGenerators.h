/**
\file
\brief
<b>BoostRandomGenerators.h Boost headers for using the boost random number generation utilities</b>
*/

#ifndef BOOSTRANDOMGENERATORSH

#define BOOSTRANDOMGENERATORSH

#undef min
#undef max

#ifndef __UNIX
#pragma warning( push, 3 ) // temp switch to level 3 warnings due to boost creating a pile or warnings at level 4.
#endif

//** FOR BOOST RANDOM **//
#include <ctime>            // std::time
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/lagged_fibonacci.hpp>
#include <boost/generator_iterator.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>

typedef boost::lagged_fibonacci19937 base_generator_type;
typedef boost::uniform_int<> distribution_type;
typedef boost::random::uniform_int_distribution<> distribution_type2;
typedef boost::random::uniform_real_distribution<double> distribution_type3;

// Added to be able to draw from distributions.
typedef boost::random::mt19937 Mersenne_Twister;
typedef boost::random::normal_distribution<double> NormalDistDouble;   // Normal Distribution
typedef boost::variate_generator<Mersenne_Twister, NormalDistDouble> Variate_gen;    // Variate generator

// Added to be able to draw from distributions.
//typedef boost::random::mt19937 g_twist;  // Mersenne Twister
//typedef boost::random::normal_distribution<double> g_normal;   // Normal Distribution
//typedef boost::variate_generator<g_twist, g_normal> g_variate_gen;    // Variate generator

// END BOOST
#ifndef __UNIX
#pragma warning( pop )
#endif

#endif

