/**
\file
\brief
Boost headers for using the boost random number generation utilities
*/

#ifndef __BOOSTRANDOMGENERATORS.H
#define __BOOSTRANDOMGENERATORS.H

//** FOR BOOST RANDOM **//
#include <ctime>            // std::time
#include <boost/random.hpp> 
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/lagged_fibonacci.hpp>
#include <boost/generator_iterator.hpp>

typedef boost::lagged_fibonacci19937 base_generator_type;
typedef boost::uniform_int<> distribution_type;
typedef boost::variate_generator<base_generator_type&, distribution_type> gen_type_int;

base_generator_type g_generator(static_cast<unsigned int>(std::time(0)));
gen_type_int g_chance_gen010000(g_generator, distribution_type(0, 10000));
boost::generator_iterator<gen_type_int> g_chance010000(&g_chance_gen010000);
gen_type_int g_chance_gen01000(g_generator, distribution_type(0, 1000));
boost::generator_iterator<gen_type_int> g_chance01000(&g_chance_gen01000);

boost::uniform_real<> uni_dist(0,1);
boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni(g_generator, uni_dist);

// END BOOST

#endif

