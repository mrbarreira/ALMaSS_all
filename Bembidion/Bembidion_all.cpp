/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include <string.h>
#include <iostream>
#include <fstream>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/AOR_Probe.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/positionmap.h"
#include "../Bembidion/bembidion_all.h"

#include "../BatchALMaSS/BoostRandomGenerators.h"
extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;

//---------------------------------------------------------------------------

using namespace std;

//---------------------------------------------------------------------------

// Externals
extern CfgInt cfg_pm_eventfrequency;
extern CfgInt cfg_pm_eventsize;
extern CfgBool cfg_RipleysOutput_used;
extern CfgBool cfg_AOROutput_used;
extern CfgBool cfg_ReallyBigOutput_used;
extern CfgBool cfg_ReallyBigOutputMonthly_used;
extern const int Vector_x[8];
extern const int Vector_y[8];
//---------------------------------------------------------------------------

//Pesticide configs
/** \brief  Pesticide body burden is multiplied by this daily*/
static CfgFloat cfg_BemAdultPPPElimiationRate("BEM_ADPPPELIMIATIONRATE", CFG_CUSTOM, 0.0, 0.0, 1.0); 
/** \brief  Adult PPP threshold for effect */
static CfgFloat cfg_BemAdultPPPThreshold("BEM_ADPPPTHRESHOLD", CFG_CUSTOM, 9999999999.9);
/** \brief  The probability of daily effect per day on threshold excedence */
static CfgFloat cfg_BemAdultPPPEffectProb("BEM_ADPPPEFFECTPROB", CFG_CUSTOM, 0, 0.0, 1.0);
/** \brief  The probability of daily effect per day on threshold excedence */
static CfgFloat cfg_BemAdultPPPEffectProbDecay("BEM_ADPPPEFFECTPROBDECAY", CFG_CUSTOM, 0, 0.0, 1.0);
/** \brief Controls whether pesticide mortality location should be recorded */
static CfgBool cfg_SavePesticideMortLocation("BEM_SAVEPESTMORTLOC", CFG_CUSTOM, false);
/** \brief Controls whether in-field off-field location should be recorded */
static CfgBool cfg_SaveInfieldLocation("BEM_SAVEINFIELDLOC", CFG_CUSTOM, false);
/** \brief Defines the crop reference for in-crop if pest mort location or in-field locations are switched on */
static CfgInt cfg_InCropRef("BEM_INCROPREF", CFG_CUSTOM, 603); // 603 == tov_WWheatPTreatment
/** \brief Interval for recording in-crop locations - > 364 is ignored */
static CfgInt cfg_SaveInfieldLocationInterval("BEM_INCROPREFINTERVAL", CFG_CUSTOM, 10000);
/** \brief Start day in year to record in-field locations */
static CfgInt cfg_SaveInfieldLocationStartDay("BEM_INCROPREFSTARTDAY", CFG_CUSTOM, -2); // -2 will cause counting on 1st of every month


//---------------------------------------------------------------------------

/** \brief Controls the numbers of adults entering the simulation */
static CfgInt cfg_beetlestartnos("BEM_STARTNO",CFG_CUSTOM,10000);
/** \brief Adult/Larve density dependent range*/
static CfgInt cfg_DDepRange("BEM_DDEPRANGE",CFG_CUSTOM,3);
/** \brief Larval density dependent mortality */
static CfgInt cfg_LDDepMort0("BEM_LDDEPMORTZERO",CFG_CUSTOM,3); //1-7
/** \brief Larval density dependent mortality */
static CfgInt cfg_LDDepMort1("BEM_LDDEPMORTONE",CFG_CUSTOM,0); //0-100
/** \brief Adult density dependent mortality constant*/
static CfgInt cfg_ADDepMort0("BEM_ADDEPMORTZERO",CFG_CUSTOM,2);
/** \brief Chance of death if there is another adult within the density-dependent range square this beetle is in. */
static CfgInt cfg_ADDepMort1("BEM_ADDEPMORTONE",CFG_CUSTOM,10);  //0-1000
/** \brief Max daily movement in m */
static CfgInt cfg_MaxDailyMovement("BEM_MAXDAILYMOVEMENT",CFG_CUSTOM,14);

const char* SimulationName="Bembidion lampros";

// Must sure that the static arrays are actually initialised
/**  \brief Static array to hold a list of co-ordinates */
param_Point Bembidion_Adult::pPoint;
/**  \brief Static array to hold the history of adult positions */
param_List15 Bembidion_Adult::pList;

/**  \brief Facilitating array for divisions by integers (= *1/N) */
extern double g_SpeedyDivides[2001];

//const int AdultActivityThreshold = 10; //Chiverton 1988, Mitchell 1963
/**  \brief Daily fixed mortality probability */
const double DailyEggMort          =   0.007;
/**  \brief Daily fixed mortality probability */
const double DailyLarvaeMort       =   0.001;
/**  \brief Daily fixed mortality probability */
const double DailyPupaeMort        =   0.001;
/**  \brief Daily fixed mortality probability */
const double DailyAdultMort        =   0.001;
/**  \brief Daily fixed mortality probability based on three larval stages and temperature */
const double LarvalDailyMort [3][6]   = {{0.0855,0.0855,0.1036,0.0563,0.0515,0.0657},
                                      {0.0626,0.0626,0.0940,0.0529,0.0492,0.0633},
                                      {0.0631,0.0631,0.0545,0.0268,0.0236,0.0295}};
/**  \brief Inflection point in larval day degree calculations */
const double DevelopmentInflectionPoint = 12.0;
/**  \brief Day degree threshold constant for all stages */
const double DevelConst1      	= 3.0; // Threshold for all development stages
/**  \brief Day degree constant */
const double EggDevelConst2     = 178.58;
/**  \brief Day degree constant */
const double LDevelConst2 [3] 	= {101.4,107.4,190.4};
/**  \brief Day degree constant */
const double PupaDevelConst2    = 147.7;
/**  \brief Day degree constant above inflection point */
const double above12Egg 		= EggDevelConst2/124.9;
/**  \brief Day degree constant above inflection point */
const double above12Larvae[3] 	= {(LDevelConst2[0]/87.5),(LDevelConst2[1]/95.2),(LDevelConst2[2]/189.3)};
/**  \brief Day degree constant above inflection point */
const double above12Pupae 		= EggDevelConst2/132.3;
/**  \brief Temperature threshold for egg laying */
const double AdultEggLayingThreshold = 3; //6;
const double EggProductionSlope	= 0.6;
/**  \brief The maximum number of eggs it is possible to produce per female */
static CfgInt cfg_TotalNoEggs("BEM_TOTALEGGS",CFG_CUSTOM,256); // was 257 010904
/**  \brief Adult dispersal temp threshold for DD calcuation */
const float DispersalThreshold 	= 6.0;
/** brief Day degrees required before dispersal */
const int DipsersalDayDegrees 	= 8;
/** brief Probability of hibernation in field */
const int FieldHibernateChance 	= 25;    //25
/** brief Max. observed daily movement in m minus 1 */
int MaxDailyMovement;//14; // max observed daily movement in m minus 1
/** brief Turning rate of adults */
const int AdultTurnRate         = 80;
/** brief Start of aggregation behaviour */
const int StartAggregatingDay   = 270;
/** brief Chance of starting aggregating on StartAggregatingDay */
const int StartAggregatingDayProb = 260;
/** brief Day at which hibernation can start if in the correct habitat type */
const int StopAggregationDay	= 280;

/** brief  Farm Operation Mortality */
static CfgFloat cfg_BeetleStriglingMort("BEM_STRIGLINGMORT", CFG_CUSTOM, 0.250);
/** brief  Farm Operation Mortality */
static CfgFloat cfg_BeetleHarvestMort("BEM_HARVESTMORT", CFG_CUSTOM, 0.290);
/** brief  Farm Operation Mortality */
static CfgFloat cfg_Egg_SoilCultivationMortality("BEM_EGGSOILMORT",CFG_CUSTOM,0.500);
/** brief  Farm Operation Mortality */
static CfgFloat cfg_Larva_SoilCultivationMortality("BEM_LARVAESOILMORT",CFG_CUSTOM,0.500);
/** brief  Farm Operation Mortality */
static CfgFloat cfg_Pupa_SoilCultivationMortality("BEM_PUPAESOILMORT",CFG_CUSTOM,0.500);
/** brief  Farm Operation Mortality */
static CfgFloat cfg_Adult_SoilCultivationMortality("BEM_ADULTSOILMORT",CFG_CUSTOM,0.270);

/** brief  Farm Operation Mortality */
static CfgFloat cfg_Egg_InsecticideApplication("BEM_EGGINSECTICIDEMORT", CFG_CUSTOM,0);
/** brief  Farm Operation Mortality */
static CfgFloat cfg_Larva_InsecticideApplication("BEM_LARVAEINSECTICIDEMORT", CFG_CUSTOM,0.800);
/** brief  Farm Operation Mortality */
static CfgFloat cfg_Pupa_InsecticideApplication("BEM_PUPAEINSECTICIDEMORT", CFG_CUSTOM,0.800);
/** brief  Farm Operation Mortality */
static CfgFloat cfg_Adult_InsecticideApplication("BEM_ADULTINSECTICIDEMORT", CFG_CUSTOM,0.800);

/** brief  Farm Operation Mortality */
static CfgInt cfg_PesticideTrialEggTreatmentMort("BEM_PTRIALEGGMORT",CFG_CUSTOM,0);
/** brief  Farm Operation Mortality */
static CfgInt cfg_PesticideTrialLarvaeTreatmentMort("BEM_PTRIALLARVAEMORT",CFG_CUSTOM,0);
/** brief  Farm Operation Mortality */
static CfgInt cfg_PesticideTrialPupaeTreatmentMort("BEM_PTRIALPUPAEMORT",CFG_CUSTOM,0);
/** brief  Farm Operation Mortality */
static CfgInt cfg_PesticideTrialAdultTreatmentMort("BEM_PTRIALADULTMORT",CFG_CUSTOM,0);

/** brief  Farm Operation Mortality */
const int PesticideTrialAdultToxicMort      = 0;
/** brief  Farm Operation Mortality */
const int PesticideTrialPupaeToxicMort      = 0;
/** brief  Farm Operation Mortality */
const int PesticideTrialLarvaeToxicMort     = 0;
/** brief  Farm Operation Mortality */
const int PesticideTrialEggToxicMort        = 0;


// Initialise any static variables
double Bembidion_Adult::m_AdultPPPElimRate = 0;  // Initialize static variable
double Bembidion_Adult::m_PPPEffectProb = 0;  // Initialize static variable
double Bembidion_Adult::m_PPPEffectProbDecay = 0;  // Initialize static variable
double Bembidion_Adult::m_PPPThreshold = 0;  // Initialize static variable

//---------------------------------------------------------------------------
//                          BEMBIDION_BASE
//---------------------------------------------------------------------------
Bembidion_Base::Bembidion_Base( int x, int y, Landscape* L,
	Bembidion_Population_Manager * BPM )
	: TAnimal( x, y, L ) {
	m_OurPopulation = BPM;
	CurrentBState = tobs_Initiation;
}
//---------------------------------------------------------------------------

void Bembidion_Base::ReInit(int x, int y, Landscape* L, Bembidion_Population_Manager * BPM) {
	ReinitialiseObject( x, y, L );
	m_OurPopulation = BPM;
	CurrentBState = tobs_Initiation;
}
//---------------------------------------------------------------------------


/**
Do the housekeeping necessary to die as an object in the system.\n
*/
void Bembidion_Base::st_Die()
{
    CurrentBState=tobs_Destroy;
    m_CurrentStateNo=-1;
    m_StepDone=true;
#ifdef __LAMBDA_RECORD
   m_OurPopulation->LamdaDeath(m_Location_x, m_Location_y);
#endif
}
//---------------------------------------------------------------------------


/**
Used for experimental manipulation of the population. This method clones a beetle
a_beetles times at the same location as the original.
*/
void Bembidion_Base::CopyMyself(int a_beetle)
{
   struct_Bembidion * BS;
   BS = new struct_Bembidion;
   BS->x   = m_Location_x;
   BS->y   = m_Location_y;
   BS->L   = m_OurLandscape;
   BS->BPM = m_OurPopulation;
   m_OurPopulation->CreateObjects(a_beetle,this,NULL,BS,1);
   delete BS;
}
//---------------------------------------------------------------------------


/**
Used for experimental manipulation of the population. This method clones a beetle
a_beetles times within +/- 32m of the individuals location using wrap around.\n
*/
void Bembidion_Base::CopyMyselfB(int a_beetle)
{
	struct_Bembidion * BS;
	for (int i=0; i<a_beetle; i++) {
	    BS = new struct_Bembidion;
	    int coord = ((m_Location_x + (random(64)-32))+m_OurPopulation->SimW) % m_OurPopulation->SimW ;
	    BS->x   = coord;
		coord = ((m_Location_y + (random(64)-32))+m_OurPopulation->SimH) % m_OurPopulation->SimH ;
	    BS->y   = coord;
	    BS->L   = m_OurLandscape;
	    BS->BPM = m_OurPopulation;
	    m_OurPopulation->CreateObjects(1,this,NULL,BS,1);
		delete BS;
	}

}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//                           BEMBIDION_EGG
//---------------------------------------------------------------------------

Bembidion_Egg_List::Bembidion_Egg_List(int today, Bembidion_Population_Manager * BPM,
                                        Landscape* L): Bembidion_Base(0,0,L,BPM)
{
    m_DayMade=today;
}
//---------------------------------------------------------------------------


int Bembidion_Egg_List::st_Develop()
{
    if (m_OurPopulation->SupplyEDayDeg(m_DayMade)>EggDevelConst2) // Hatch
    {
      return 1; // go to hatch
    }
    else return 0; // carry on developing
}
//---------------------------------------------------------------------------


void Bembidion_Egg_List::st_Hatch()
{
   struct_Bembidion * BS;
   BS = new struct_Bembidion;
   int size=(int)EggList.size();

   for (int i=0; i<size; i++)
   {
     BS->x   = EggList[i].m_x;
     BS->y   = EggList[i].m_y;
     BS->L   = m_OurLandscape;
     BS->BPM = m_OurPopulation;
     m_OurPopulation->CreateObjects(1,this,NULL,BS,1);
   }
   EggList.erase(EggList.begin(),EggList.end()); // empty the list
   delete BS;
#ifdef __LAMBDA_RECORD
	  m_OurPopulation->LamdaBirth(m_Location_x,m_Location_y,size);
#endif

}
//---------------------------------------------------------------------------

void Bembidion_Egg_List::BeginStep()
{
  DailyMortality();
}
//---------------------------------------------------------------------------


void Bembidion_Egg_List::Step()
{
 if (m_StepDone || m_CurrentStateNo == -1) return;
 switch (CurrentBState)
 {
  case tobs_Initiation:    // Initial
   CurrentBState=tobs_EDeveloping;
   break;
  case tobs_EDeveloping: // Developing
   switch(st_Develop())
   {
   case 0:  // nothing
    m_StepDone = true;
    break;
   case 1:  // hatch
    CurrentBState=tobs_Hatching;
    break;
   }
   break;
  case tobs_Hatching:
   st_Hatch();
   CurrentBState=tobs_Initiation;
   m_StepDone=true;
   break;
  default:
    m_OurLandscape->Warn("Bembidion Egg - Unknown State",NULL);
	g_msg->Warn(WARN_BUG, "State attempted was: ", int(CurrentBState));
    exit(1);
 }
}
//---------------------------------------------------------------------------


void Bembidion_Egg_List::DailyMortality()
{
  // Needs to go through all eggs in the list and test for background mortality
  // and while we are here for OnFarmEvent stuff
  int size=(int)EggList.size();
  for (int i=size-1; i>=0; i--)
  {
    if (g_rand_uni()<DailyEggMort)
    {
      // Remove the Egg it is dead
      //EggList.erase(EggList.begin()+i);
	  EggList[i].m_x = -999; // Code for kill it
    }
    else
    {
      int res=0;
      FarmToDo event;
      if((event =(FarmToDo) m_OurLandscape->SupplyLastTreatment(m_Location_x,
                                          m_Location_y, &res)) != sleep_all_day)

 switch(event)
 {
 case  sleep_all_day:
  break;
 case  autumn_plough:
  if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
      //EggList.erase(EggList.begin()+i);
	  EggList[i].m_x = -999; // Code for kill it
  break;
 case  stubble_plough:
	 if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
		 //EggList.erase(EggList.begin()+i);
		 EggList[i].m_x = -999; // Code for kill it
	 break;
 case  stubble_cultivator_heavy:
	 if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
		 //EggList.erase(EggList.begin()+i);
		 EggList[i].m_x = -999; // Code for kill it
	 break;
 case  heavy_cultivator_aggregate:
	 if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
		 //EggList.erase(EggList.begin()+i);
		 EggList[i].m_x = -999; // Code for kill it
	 break;
 case  autumn_harrow:
  if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
      //EggList.erase(EggList.begin()+i);
	  EggList[i].m_x = -999; // Code for kill it
  break;
 case  preseeding_cultivator:
	 if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
		 //EggList.erase(EggList.begin()+i);
		 EggList[i].m_x = -999; // Code for kill it
	 break;
 case  preseeding_cultivator_sow:
	 if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
		 //EggList.erase(EggList.begin()+i);
		 EggList[i].m_x = -999; // Code for kill it
	 break;
 case  autumn_roll:
  if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
      //EggList.erase(EggList.begin()+i);
	  EggList[i].m_x = -999; // Code for kill it
  break;
 case  autumn_sow:
  if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
      //EggList.erase(EggList.begin()+i);
	  EggList[i].m_x = -999; // Code for kill it
  break;
 case  winter_plough:
  if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
      //EggList.erase(EggList.begin()+i);
	  EggList[i].m_x = -999; // Code for kill it
  break;
 case  deep_ploughing:
  if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
      //EggList.erase(EggList.begin()+i);
	  EggList[i].m_x = -999; // Code for kill it
  break;
 case  spring_plough:
  if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
      //EggList.erase(EggList.begin()+i);
	  EggList[i].m_x = -999; // Code for kill it
  break;
 case  spring_harrow:
  if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
      //EggList.erase(EggList.begin()+i);
	  EggList[i].m_x = -999; // Code for kill it
  break;
 case  shallow_harrow:
	 if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
		 //EggList.erase(EggList.begin()+i);
		 EggList[i].m_x = -999; // Code for kill it
	 break;
 case  spring_roll:
  break;
 case  spring_sow:
  break;
 case  spring_sow_with_ferti:
	 break;
case  fp_npks:
  break;
case  fp_npk:
  break;
case  fp_pk:
  break;
case  fp_liquidNH3:
  break;
case  fp_slurry:
  break;
case  fp_ammoniumsulphate:
	break;
case  fp_manganesesulphate:
  break;
case  fp_manure:
  break;
case  fp_greenmanure:
  break;
case  fp_sludge:
  break;
case  fp_rsm:
	break;
case  fp_calcium:
	break;
case  fa_npks:
	break;
case  fa_npk:
  break;
case  fa_pk:
  break;
case  fa_slurry:
  break;
case  fa_ammoniumsulphate:
  break;
case  fa_manganesesulphate:
	break;
case  fa_manure:
  break;
case  fa_greenmanure:
  break;
case  fa_sludge:
  break;
case  fa_rsm:
	break;
case  fa_calcium:
	break;
case  herbicide_treat:
  break;
case  growth_regulator:
  break;
case  fungicide_treat:
  break;
case  insecticide_treat:
  if (g_rand_uni()<cfg_Egg_InsecticideApplication.value())
      //EggList.erase(EggList.begin()+i);
	  EggList[i].m_x = -999; // Code for kill it
  break;
case  molluscicide:
  break;
case  row_cultivation:
  break;
case  strigling:
  break;
case  hilling_up:
  break;
case  water:
  break;
case  swathing:
  break;
case  harvest:
  break;
case  cattle_out:
  break;
case  cattle_out_low:
  break;
case  cut_to_hay:
  break;
case  cut_to_silage:
  break;
case  straw_chopping:
  break;
case  hay_turning:
  break;
case  hay_bailing:
  break;
case  burn_straw_stubble:
  break;
case  stubble_harrowing:
  break;
case  autumn_or_spring_plough:
  if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
      //EggList.erase(EggList.begin()+i);
	  EggList[i].m_x = -999; // Code for kill it
  break;
case flammebehandling:    // 50
  break;
case mow:
  break;
case cut_weeds:
  break;
case pigs_out:
  break;
case strigling_sow:
  if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
      //EggList.erase(EggList.begin()+i);
	  EggList[i].m_x = -999; // Code for kill it
  break;
case  strigling_hill:
	if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
		//EggList.erase(EggList.begin()+i);
		EggList[i].m_x = -999; // Code for kill it
	break;
case trial_insecticidetreat:
  if (random(1000)<cfg_PesticideTrialEggTreatmentMort.value())
      //EggList.erase(EggList.begin()+i);
	  EggList[i].m_x = -999; // Code for kill it
  break;
case trial_toxiccontrol:
  if (random(1000)<PesticideTrialEggToxicMort)
      //EggList.erase(EggList.begin()+i);
	  EggList[i].m_x = -999; // Code for kill it
  break;
case trial_control:
case product_treat:
 break;
case glyphosate:
 break;
case syninsecticide_treat:
 if (random(1000)<cfg_PesticideTrialEggTreatmentMort.value())
       //EggList.erase(EggList.begin()+i);
	  EggList[i].m_x = -999; // Code for kill it
 break;
case biocide:
	break;
case  bed_forming:
	if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
		//EggList.erase(EggList.begin()+i);
		EggList[i].m_x = -999; // Code for kill it
	break;
case flower_cutting:
	break;
case  bulb_harvest:
	if (g_rand_uni()<cfg_Egg_SoilCultivationMortality.value())
		//EggList.erase(EggList.begin()+i);
		EggList[i].m_x = -999; // Code for kill it
	break;
case straw_covering:
	break;
case straw_removal:
	break;
default:
 m_OurLandscape->Warn("Bembidion Egg - Treatment in Daily Mortality",NULL);
 exit(1);
}
 }
 }
 // Now sort the list based on x values
 SortXR();
 // Then erase all the -999
 vector<APoint>::reverse_iterator ritE=EggList.rend();
 vector<APoint>::reverse_iterator ritB=EggList.rbegin();
 while (ritB!=ritE) {
    if ((*ritB).m_x<0) ritB++; else break;
 }
 EggList.erase((ritB).base(),EggList.end());
}
//---------------------------------------------------------------------------

/**
Sort TheArray w.r.t. the current X in reverse order
*/
void Bembidion_Egg_List::SortXR() {
  sort( EggList.begin(), EggList.end(), CompareEggX() );
}

//-----------------------------------------------------------------------------


//---------------------------------------------------------------------------
//                           BEMBIDION_LARVAE
//---------------------------------------------------------------------------



Bembidion_Larvae::Bembidion_Larvae( int x, int y, Landscape* L,
	Bembidion_Population_Manager * BPM ) :
	Bembidion_Base( x, y, L, BPM ) {
	m_LarvalStage = 0;
	m_AgeDegrees = 0;
	m_DayMade = L->SupplyDayInYear();
}
//---------------------------------------------------------------------------

void Bembidion_Larvae::ReInit( int x, int y, Landscape* L,	Bembidion_Population_Manager * BPM ) {
	Bembidion_Base::ReInit( x, y, L, BPM );
	m_LarvalStage = 0;
	m_AgeDegrees = 0;
	m_DayMade = L->SupplyDayInYear();
}
//---------------------------------------------------------------------------

bool Bembidion_Larvae::OnFarmEvent( FarmToDo event )
{
 switch(event)
 {
 case  sleep_all_day:
  break;
 case  autumn_plough:
  if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState=tobs_LDying;
  break;
 case  stubble_plough: // assumed to be the same as for autumn plough
	 if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState = tobs_LDying;
	 break;
 case  stubble_cultivator_heavy: // assumed to be the same as for autumn plough
	 if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState = tobs_LDying;
	 break;
 case  heavy_cultivator_aggregate: // assumed to be the same as for autumn plough
	 if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState = tobs_LDying;
	 break;
 case  autumn_harrow:
	 if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState = tobs_LDying;
	 break;
 case  preseeding_cultivator: // assumed to be the same as for harrow
	 if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState = tobs_LDying;
	 break;
 case  preseeding_cultivator_sow: // assumed to be the same as for harrow
	 if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState = tobs_LDying;
	 break;
 case  autumn_roll:
  if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState=tobs_LDying;
  break;
 case  autumn_sow:
  if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState=tobs_LDying;
  break;
 case  winter_plough:
  if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState=tobs_LDying;
  break;
 case  deep_ploughing:
  if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState=tobs_LDying;
  break;
 case  spring_plough:
  if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState=tobs_LDying;
  break;
 case  spring_harrow:
  if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState=tobs_LDying;
  break;
 case  shallow_harrow:
	 if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState = tobs_LDying;
	 break;
 case  spring_roll:
  break;
 case  spring_sow:
  break;
 case  spring_sow_with_ferti:
	 break;
 case  fp_npks:
	 break;
 case  fp_npk:
	 break;
 case  fp_pk:
	 break;
 case  fp_liquidNH3:
	 break;
 case  fp_slurry:
	 break;
 case  fp_manganesesulphate:
	 break;
 case  fp_ammoniumsulphate:
	 break;
 case  fp_manure:
	 break;
 case  fp_greenmanure:
	 break;
 case  fp_sludge:
	 break;
 case  fp_rsm:
	 break;
 case  fp_calcium:
	 break;
 case  fa_npks:
	 break;
 case  fa_npk:
	 break;
 case  fa_pk:
	 break;
 case  fa_slurry:
	 break;
 case  fa_ammoniumsulphate:
	 break;
 case  fa_manganesesulphate:
	 break;
 case  fa_manure:
	 break;
 case  fa_greenmanure:
	 break;
 case  fa_sludge:
	 break;
 case  fa_rsm:
	 break;
 case  fa_calcium:
	 break;
case  herbicide_treat:
  break;
case  growth_regulator:
  break;
case  fungicide_treat:
  break;
case  insecticide_treat:
  if (g_rand_uni()<cfg_Larva_InsecticideApplication.value()) CurrentBState=tobs_LDying;
  break;
case  molluscicide:
  break;
case  row_cultivation:
  break;
case  strigling:
	//if (g_rand_uni()<cfg_BeetleStriglingMort.value()) CurrentBState=tobs_LDying;
break;
case  hilling_up:
  break;
case  water:
  break;
case  swathing:
  break;
case  harvest:
  break;
case  cattle_out:
  break;
case  cattle_out_low:
  break;
case  cut_to_hay:
  break;
case  cut_to_silage:
  break;
case  straw_chopping:
  break;
case  hay_turning:
  break;
case  hay_bailing:
  break;
case  flammebehandling:
  break;
case  stubble_harrowing:
  break;
case  autumn_or_spring_plough:
  if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState=tobs_LDying;
  break;
case  burn_straw_stubble:
  break;
case mow:
  break;
case cut_weeds:
  break;
case pigs_out:
  break;
case strigling_sow:
  if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState=tobs_LDying;
  break;
case strigling_hill:
	if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState = tobs_LDying;
	break;
case trial_insecticidetreat:
  if (random(1000)<cfg_PesticideTrialLarvaeTreatmentMort.value()) CurrentBState=tobs_LDying;
  break;
case trial_toxiccontrol:
  if (random(1000)<PesticideTrialLarvaeToxicMort) CurrentBState=tobs_LDying;
  break;
case trial_control:
case product_treat:
 break;
case glyphosate:
 break;
case syninsecticide_treat:
 if (random(1000)<cfg_PesticideTrialLarvaeTreatmentMort.value()) CurrentBState=tobs_LDying;
 break;
case biocide:
 break;
case  bed_forming:
	if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState = tobs_LDying;
	break;
case flower_cutting:
	break;
case  bulb_harvest:
	if (g_rand_uni()<cfg_Larva_SoilCultivationMortality.value()) CurrentBState = tobs_LDying;
	break;
case straw_covering:
	break;
case straw_removal:
	break;
default:
  m_OurLandscape->Warn("Bembidion Larvae - Unknown Treatment in Daily Mortality"
                                                                         ,NULL);
  exit(1);
  }
 // Must incorporate a test here in case the animal is dead - killing it twice
 // can be a bad idea
 if (CurrentBState==tobs_LDying) return true;
  else
 return false;
}
//---------------------------------------------------------------------------

bool Bembidion_Larvae::TempRelatedLarvalMortality(int temp2)
{
/**
	The idea here is that we calculate how long a larvae would expect to be
	at a temperature (based on today's temp)
	We also calculate the background mortality expected at that temperature
	then divide the total mortality by the number of days
	then apply that mortality today
	Problem is that we are dealing with percentages so this is not straight
	forward.
	The solution used here is to calculate a set of development and daily
	mort for 5 degree intervals then choose the closest.
*/
	if (temp2<0) temp2=0; else if (temp2>25) temp2=25;
	temp2=(int)(floor((float)temp2+2.0)*0.2); // same as (temp2+2)/5
	// take a mortality test
	if (g_rand_uni()<(LarvalDailyMort[m_LarvalStage][temp2])) return true;// die
	else return false;
}
//------------------------------------------------------------------------------

int Bembidion_Larvae::st_Develop()
{
/**
	Determines temperature related mortality.
	If the larvae survives this then a density-dependent mortality is applied.
	If the the larvae survives the second mortality then development occurs based on the temperature experienced since the day before. Development rate depends on the larval stage. The code is speeded by these calculations being carried out by the Bembidion_Population_Manager for all eggs, larvae and adults extant each day, hence only one calculation needs to be done for each life-stage instead of potentially millions.
*/
   int temp2=(int) floor(m_OurLandscape->SupplyTemp()+0.5);
   if (TempRelatedLarvalMortality(temp2))
   {
     // To ensure that we don't influence anyone else clear the position
     m_OurPopulation->m_LarvaePosMap->ClearMapValue(m_Location_x,m_Location_y);
     return 2; // die
   }
   // Find out if there is any danger of using wrap round
   if ((m_Location_x==0)||(m_Location_y==0)||(m_Location_x>=
       m_OurPopulation->SimW-2)||(m_Location_y>=m_OurPopulation->SimH-2))
   {
     // No density dependent mortality at the edge (this is a fudge, but a small one with large landscapes)
   }
   else
   {
     // Do density dependent mortality based on the +/- 1 m in all directions.
     if (m_OurPopulation->m_LarvaePosMap->GetMapDensity(m_Location_x-1,m_Location_y-1,cfg_DDepRange.value())>=m_OurPopulation->LDDepMort0) {
       if (g_rand_uni()<m_OurPopulation->LDDepMort1)
       {
        // To ensure that we don't influence anyone else clear the position
        m_OurPopulation->m_LarvaePosMap->ClearMapValue(m_Location_x,m_Location_y);
        return 2; // die
       }
	 }
   }
   // Still alive so now do the development bit
   if (m_OurPopulation->SupplyLDayDeg(m_LarvalStage, m_DayMade )>LDevelConst2[m_LarvalStage])
   {
     m_LarvalStage++; // turn into next larval stage
   }
   if (m_LarvalStage==3) return 1; // become a pupa
     else return 0; // carry on developing
}
//---------------------------------------------------------------------------


void Bembidion_Larvae::st_Pupate()
{
   // Must clear the map position
   m_OurPopulation->m_LarvaePosMap->ClearMapValue(m_Location_x, m_Location_y);
   struct_Bembidion * BS;
   BS = new struct_Bembidion;
   BS->x   = m_Location_x;
   BS->y   = m_Location_y;
   BS->L   = m_OurLandscape;
   BS->BPM = m_OurPopulation;
   BS->DayDegrees=(int)m_AgeDegrees;
   // carry the extra degrees over to next stage
   m_OurPopulation->CreateObjects(2,this,NULL,BS,1);
   delete BS;
}
//---------------------------------------------------------------------------


void Bembidion_Larvae::KillThis()
{
 CurrentBState=tobs_LDying;
}
//---------------------------------------------------------------------------


void Bembidion_Larvae::BeginStep()
{
  CheckManagement();
  if (DailyMortality())CurrentBState=tobs_LDying;
}
//---------------------------------------------------------------------------


void Bembidion_Larvae::Step()
{
 if (m_StepDone || m_CurrentStateNo == -1) return;
 switch (CurrentBState)
 {
  case 0:    // Initial
   CurrentBState=tobs_LDeveloping;
   break;
  case tobs_LDeveloping: // Developing
   switch(st_Develop())
   {
   case 0:  // nothing
    m_StepDone = true;
    break;
   case 1:  // pupating
    CurrentBState=tobs_Pupating;
    break;
   case 2:  // die
    CurrentBState=tobs_LDying;
    break;
   }
   break;
  case tobs_Pupating:
   st_Pupate();
   m_CurrentStateNo=-1; //Destroys object
   CurrentBState=tobs_Destroy;
   m_StepDone=true;
   break;
  case tobs_LDying:
   // Remove itself from the position map
   m_OurPopulation->m_LarvaePosMap->ClearMapValue(m_Location_x, m_Location_y);
   st_Die();
   m_StepDone=true;
   break;
  default:
    m_OurLandscape->Warn("Bembidion Larvae - Unknown State",NULL);
	g_msg->Warn(WARN_BUG, "State attempted was: ", int(CurrentBState));
    exit(1);
  }
}
//---------------------------------------------------------------------------


bool Bembidion_Larvae::DailyMortality()
{
  if (g_rand_uni()<DailyLarvaeMort)
  {
	  return true;
  }
  return false;
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//                           BEMBIDION_PUPAE
//---------------------------------------------------------------------------


Bembidion_Pupae::Bembidion_Pupae(int x, int y,Landscape* L, Bembidion_Population_Manager * BPM): Bembidion_Base(x,y,L,BPM)
{
    m_AgeDegrees=0;
    m_DayMade=L->SupplyDayInYear();
}
//---------------------------------------------------------------------------


void Bembidion_Pupae::ReInit( int x, int y, Landscape* L, Bembidion_Population_Manager * BPM ) {
	Bembidion_Base::ReInit( x, y, L, BPM );
	m_AgeDegrees = 0;
	m_DayMade = L->SupplyDayInYear();
}
//---------------------------------------------------------------------------

bool Bembidion_Pupae::OnFarmEvent(FarmToDo event)
{
 switch(event)
 {
 case  sleep_all_day:
  break;
 case  autumn_plough:
  if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState=tobs_PDying;
  break;
 case  stubble_plough: // assumed to be the same as for autumn plough
	 if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState = tobs_PDying;
	 break;
 case  stubble_cultivator_heavy: // assumed to be the same as for autumn plough
	 if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState = tobs_PDying;
	 break;
 case  heavy_cultivator_aggregate: // assumed to be the same as for autumn plough
	 if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState = tobs_PDying;
	 break;
 case  autumn_harrow:
	 if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState = tobs_PDying;
	 break;
 case  preseeding_cultivator: // assumed to be the same as for harrow
	 if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState = tobs_PDying;
	 break;
 case  preseeding_cultivator_sow: // assumed to be the same as for harrow
	 if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState = tobs_PDying;
	 break;
 case  autumn_roll:
  if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState=tobs_PDying;
  break;
 case  autumn_sow:
  if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState=tobs_PDying;
  break;
 case  winter_plough:
  if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState=tobs_PDying;
  break;
 case  deep_ploughing:
  if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState=tobs_PDying;
  break;
 case  spring_plough:
  if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState=tobs_PDying;
  break;
 case  spring_harrow:
  if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState=tobs_PDying;
  break;
 case  shallow_harrow:
	if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState = tobs_PDying;
	break;
 case  spring_roll:
  break;
 case  spring_sow:
  break;
 case  spring_sow_with_ferti:
	 break;
 case  fp_npks:
	 break;
 case  fp_npk:
	 break;
 case  fp_pk:
	 break;
 case  fp_liquidNH3:
	 break;
 case  fp_slurry:
	 break;
 case  fp_manganesesulphate:
	 break;
 case  fp_ammoniumsulphate:
	 break;
 case  fp_manure:
	 break;
 case  fp_greenmanure:
	 break;
 case  fp_sludge:
	 break;
 case  fp_rsm:
	 break;
 case  fp_calcium:
	 break;
 case  fa_npks:
	 break;
 case  fa_npk:
	 break;
 case  fa_pk:
	 break;
 case  fa_slurry:
	 break;
 case  fa_ammoniumsulphate:
	 break;
 case  fa_manganesesulphate:
	 break;
 case  fa_manure:
	 break;
 case  fa_greenmanure:
	 break;
 case  fa_sludge:
	 break;
 case  fa_rsm:
	 break;
 case  fa_calcium:
	 break;
case  herbicide_treat:
  break;
case  growth_regulator:
  break;
case  fungicide_treat:
  break;
case  insecticide_treat:
  if (g_rand_uni()<cfg_Pupa_InsecticideApplication.value()) CurrentBState=tobs_PDying;
  break;
case  molluscicide:
  break;
case  row_cultivation:
  break;
case  strigling:
//if (g_rand_uni()<cfg_BeetleStriglingMort.value())  CurrentBState=tobs_PDying;
  break;
case  hilling_up:
  break;
case  water:
  break;
case  swathing:
  break;
case  harvest:
  break;
case  cattle_out:
  break;
case  cattle_out_low:
  break;
case  cut_to_hay:
  break;
case  cut_to_silage:
  break;
case  straw_chopping:
  break;
case  hay_turning:
  break;
case  hay_bailing:
  break;
case  flammebehandling:
  break;
case  stubble_harrowing:
  break;
case  autumn_or_spring_plough:
  if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState=tobs_PDying;
  break;
case  burn_straw_stubble:
  break;
case mow:
  break;
case cut_weeds:
  break;
case pigs_out:
  break;
case strigling_sow:
  if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState=tobs_PDying;
  break;
case strigling_hill:
	if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState = tobs_PDying;
	break;
case trial_insecticidetreat:
  if (random(1000)<cfg_PesticideTrialPupaeTreatmentMort.value()) CurrentBState=tobs_PDying;
  break;
case trial_toxiccontrol:
  if (random(1000)<PesticideTrialPupaeToxicMort) CurrentBState=tobs_PDying;
  break;
case trial_control:
case product_treat:
case glyphosate:
 break;
case syninsecticide_treat:
  if (random(1000)<cfg_PesticideTrialPupaeTreatmentMort.value()) CurrentBState=tobs_PDying;
  break;
case biocide:
	break;
case  bed_forming:
	if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState = tobs_PDying;
	break;
case flower_cutting:
	break;
case  bulb_harvest:
	if (g_rand_uni()<cfg_Pupa_SoilCultivationMortality.value()) CurrentBState = tobs_PDying;
	break;
case straw_covering:
	break;
case straw_removal:
	break;
 default:
   m_OurLandscape->Warn("Bembidion Pupae - Unknown Treatment in Daily Mortality",NULL);
   g_msg->Warn(WARN_BUG, "State attempted was: ", int(CurrentBState));
   exit(1);
 }
 // Must incorporate a test here in case the animal is dead - killing it twice
 // can be a bad idea
 if (CurrentBState==tobs_PDying) return true;
  else
 return false;
}
//---------------------------------------------------------------------------


int Bembidion_Pupae::st_Develop()
{
   double DailyMortChance;
   int temp2=(int) floor(m_OurLandscape->SupplyTemp()+0.5);
   int LengthOfStageAtTemp;
   // watch out for -ve & zero
   if (temp2 <1) temp2=1;
   if (temp2<13)
   {
     // How long to develop at this temp
     LengthOfStageAtTemp = (int)(PupaDevelConst2*g_SpeedyDivides[temp2]);
     // Get the mortality per day
     // Equation modified from Boye Jensen 1990
     DailyMortChance = (100-(-7.1429+1.42857*temp2))*
                                             g_SpeedyDivides[LengthOfStageAtTemp];
   }
    else if (temp2<23)
    {
      // How long to develop at this temp
     LengthOfStageAtTemp = (int)(PupaDevelConst2*g_SpeedyDivides[temp2]);
     // Get the mortality per day
     // Equation modified from Boye Jensen 1990
     DailyMortChance = (100-(-74.623+7.26415*temp2))
                                          *g_SpeedyDivides[LengthOfStageAtTemp];
    }
     else // temp>=23
     {
     // How long to develop at this temp
     LengthOfStageAtTemp = (int)(PupaDevelConst2*g_SpeedyDivides[temp2]);
     // Get the mortality per day
     // Equation modified from Boye Jensen 1990
     DailyMortChance = (20.0)*g_SpeedyDivides[LengthOfStageAtTemp];
     }
    // take a mortality test
    if (random(1000) < (DailyMortChance*10)) return 2;   // die
    // Still alive so now do the development bit
    if (temp2>0) m_AgeDegrees+=temp2;
    if (m_OurPopulation->SupplyPDayDeg(m_DayMade )>PupaDevelConst2)
    {
      return 1; // go to emerge
    }
    else return 0; // carry on developing
}
//---------------------------------------------------------------------------


void Bembidion_Pupae::st_Emerge()
{
   struct_Bembidion * BS;
   BS = new struct_Bembidion;
   int NoToMake=1;
   BS->x   = m_Location_x;
   BS->y   = m_Location_y;
   BS->L   = m_OurLandscape;
   BS->BPM = m_OurPopulation;
   // object destroyed by OwnerRTS
   m_OurPopulation->CreateObjects(3,this,NULL,BS,NoToMake);
   delete BS;
}
//---------------------------------------------------------------------------


void Bembidion_Pupae::KillThis()
{
 CurrentBState=tobs_PDying;
}
//---------------------------------------------------------------------------


void Bembidion_Pupae::BeginStep()
{
  CheckManagement();
  if (DailyMortality())CurrentBState=tobs_PDying;
}
//---------------------------------------------------------------------------


void Bembidion_Pupae::Step()
{
 if (m_StepDone || m_CurrentStateNo == -1) return;
 switch (CurrentBState)
 {
  case 0:    // Initial
   CurrentBState=tobs_PDeveloping;
   break;
  case tobs_PDeveloping: // Developing
   switch(st_Develop())
   {
   case 0:  // nothing
    m_StepDone = true;
    break;
   case 1:  // emerging
    CurrentBState=tobs_Emerging;
    break;
   case 2:  // die
    CurrentBState=tobs_PDying;
    break;
   }
   break;
  case tobs_Emerging:
   st_Emerge();
   m_StepDone=true;
   CurrentBState=tobs_Destroy;
   m_CurrentStateNo=-1; //Destroys object
   break;
  case tobs_PDying:
   st_Die();
   m_StepDone=true;
   break;
  default:
    m_OurLandscape->Warn("Bembidion Pupae - Unknown State",NULL);
    exit(1);
  }
}
//---------------------------------------------------------------------------


bool Bembidion_Pupae::DailyMortality()
{
  if (g_rand_uni()<DailyPupaeMort)
  {
    return true;
  }
  return false;
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//                           BEMBIDION_ADULTS
//---------------------------------------------------------------------------
Bembidion_Adult::Bembidion_Adult( int x, int y, Landscape* L, Bembidion_Population_Manager * BPM ) :
Bembidion_Base( x, y, L, BPM ) {
	Init();
}
//---------------------------------------------------------------------------

void Bembidion_Adult::ReInit( int x, int y, Landscape* L, Bembidion_Population_Manager * BPM ) {
	Bembidion_Base::ReInit( x, y, L, BPM );
	Init();
}
//---------------------------------------------------------------------------

Bembidion_Adult::~Bembidion_Adult()
{
}
//---------------------------------------------------------------------------

void Bembidion_Adult::Init()
{
    m_negDegrees=0; //No negative degrees to sum
    // don't want to start accumulating Hibernate degrees until Jan 1st
    m_HibernateDegrees=-99999;
    OldDirection=random(8);
    m_EggCounter=0;
    m_CanReproduce=false;
	m_body_burden = 0;
	m_currentPPPEffectProb = 0;
}
//---------------------------------------------------------------------------


bool Bembidion_Adult::OnFarmEvent(FarmToDo event)
{
 switch(event)
 {
 case  sleep_all_day:
  break;
 case  autumn_plough:
  if (g_rand_uni()<cfg_Adult_SoilCultivationMortality.value()) CurrentBState=tobs_ADying;
  break;
 case  stubble_plough: // assumed to be the same as for autumn plough
	 if (g_rand_uni()<cfg_Adult_SoilCultivationMortality.value()) CurrentBState = tobs_ADying;
	 break;
 case  stubble_cultivator_heavy: // assumed to be the same as for autumn plough
	 if (g_rand_uni()<cfg_Adult_SoilCultivationMortality.value()) CurrentBState = tobs_ADying;
	 break;
 case  heavy_cultivator_aggregate: // assumed to be the same as for autumn plough
	 if (g_rand_uni()<cfg_Adult_SoilCultivationMortality.value()) CurrentBState = tobs_ADying;
	 break;
 case  autumn_harrow:
  if (g_rand_uni()<cfg_Adult_SoilCultivationMortality.value()) CurrentBState=tobs_ADying;
  break;
 case  preseeding_cultivator: // assumed to be the same as for harrow
	 if (g_rand_uni()<cfg_Adult_SoilCultivationMortality.value()) CurrentBState = tobs_ADying;
	 break;
 case  preseeding_cultivator_sow: // assumed to be the same as for harrow
	 if (g_rand_uni()<cfg_Adult_SoilCultivationMortality.value()) CurrentBState = tobs_ADying;
	 break;
 case  autumn_roll:
  if (g_rand_uni()<cfg_Adult_SoilCultivationMortality.value()) CurrentBState=tobs_ADying;
  break;
 case  autumn_sow:
  if (g_rand_uni()<cfg_Adult_SoilCultivationMortality.value()) CurrentBState=tobs_ADying;
  break;
 case  winter_plough:
  if (g_rand_uni()<cfg_Adult_SoilCultivationMortality.value()) CurrentBState=tobs_ADying;
  break;
 case  deep_ploughing:
  if (g_rand_uni()<cfg_Adult_SoilCultivationMortality.value()) CurrentBState=tobs_ADying;
  break;
 case  spring_plough:
  if (g_rand_uni()<cfg_Adult_SoilCultivationMortality.value()) CurrentBState=tobs_ADying;
  break;
 case  spring_harrow:
  if (g_rand_uni()<cfg_Adult_SoilCultivationMortality.value()) CurrentBState=tobs_ADying;
  break;
 case  shallow_harrow:
	 if (g_rand_uni()<cfg_Adult_SoilCultivationMortality.value()) CurrentBState = tobs_ADying;
	 break;
 case  spring_roll:
  break;
 case  spring_sow:
  break;
 case  spring_sow_with_ferti:
	 break;
case  fp_npks:
  break;
case  fp_npk:
  break;
case  fp_pk:
  break;
case  fp_liquidNH3:
  break;
case  fp_slurry:
  break;
case  fp_manganesesulphate:
  break;
case  fp_ammoniumsulphate:
	break;
case  fp_manure:
  break;
case  fp_greenmanure:
  break;
case  fp_sludge:
  break;
case  fp_rsm:
	break;
case  fp_calcium:
	break;
case  fa_npks:
	break;
case  fa_npk:
  break;
case  fa_pk:
  break;
case  fa_slurry:
  break;
case  fa_ammoniumsulphate:
  break;
case  fa_manganesesulphate:
	break;
case  fa_manure:
  break;
case  fa_greenmanure:
  break;
case  fa_sludge:
  break;
case  fa_rsm:
	break;
case  fa_calcium:
	break;
case  herbicide_treat:
  break;
case  growth_regulator:
  break;
case  fungicide_treat:
  break;
case  insecticide_treat:
  if (g_rand_uni()<cfg_Adult_InsecticideApplication.value()) CurrentBState=tobs_ADying;
  break;
case  molluscicide:
  break;
case  row_cultivation:
  break;
case  strigling:
if (g_rand_uni()<cfg_BeetleStriglingMort.value()) CurrentBState=tobs_ADying;
  break;
case  hilling_up:
  break;
case  water:
  break;
case  swathing:
  break;
case  harvest:
  if (g_rand_uni()<cfg_BeetleHarvestMort.value()) CurrentBState = tobs_ADying;
  break;
case  cattle_out:
  break;
case  cattle_out_low:
  break;
case  cut_to_hay:
  break;
case  cut_to_silage:
  break;
case  straw_chopping:
  break;
case  hay_turning:
  break;
case  hay_bailing:
  break;
case  flammebehandling:
  break;
case  stubble_harrowing:
  break;
case  autumn_or_spring_plough:
  if (g_rand_uni()<cfg_Adult_SoilCultivationMortality.value()) CurrentBState=tobs_ADying;
  break;
case  burn_straw_stubble:
  break;
case mow:
  break;
case cut_weeds:
  break;
case pigs_out:
  break;
case strigling_sow:
  if (g_rand_uni()<cfg_Adult_SoilCultivationMortality.value()) CurrentBState=tobs_ADying;
  break;
case strigling_hill:
	if (g_rand_uni()<cfg_BeetleStriglingMort.value()) CurrentBState = tobs_ADying;
	break;
case trial_insecticidetreat:
  if (random(1000)<cfg_PesticideTrialAdultTreatmentMort.value()) CurrentBState=tobs_ADying;
  break;
case trial_toxiccontrol:
  if (random(1000)<PesticideTrialAdultToxicMort) CurrentBState=tobs_ADying;
  break;
case trial_control:
case product_treat:
case glyphosate:
	break;
case syninsecticide_treat:
 if (random(1000)<cfg_PesticideTrialAdultTreatmentMort.value()) CurrentBState=tobs_ADying;
 break;
case biocide:
	break;
case  bed_forming:
	if (g_rand_uni()<cfg_Adult_SoilCultivationMortality.value()) CurrentBState = tobs_ADying;
	break;
case flower_cutting:
	break;
case  bulb_harvest:
	if (g_rand_uni()<cfg_Adult_SoilCultivationMortality.value()) CurrentBState = tobs_ADying;
	break;
case straw_covering:
	break;
case straw_removal:
	break;
 default:
   m_OurLandscape->Warn("Bembidion Adult - Unknown Treatment in Daily Mortality"
                                                                          ,NULL);
   exit(1);
 }
 // Must incorporate a test here in case the animal is dead - killing it twice
 // can be a bad idea
 if (CurrentBState==tobs_ADying) return true;
  else
 return false;
}
//---------------------------------------------------------------------------


/**
If it is not yet time to start aggregating then the beetle does its daily movement and returns a zero value,
indicating it will stay in this state. If date has exceeded StartAggregatingDay, then the beetle has a increasing
probability with time of changing to aggregative behaviour.\n
If the beetle starts aggregating and it is from last season, then it is assumed that it will die.\n
*/
int Bembidion_Adult::st_Forage()
{
  if (DailyMovement(random(MaxDailyMovement)+1, true)==2) return 2;// Die
  int Day=m_OurLandscape->SupplyDayInYear();
  if (Day<StartAggregatingDay) return 0;
  else
  {
    if (random(100)<Day-StartAggregatingDayProb)
    {
     // Stop laying eggs now
     if (m_CanReproduce==true) // must be from last year so kill it
     return 2;
     return 1; // start aggregating
    }
  }
  return 0;
}
//-----------------------------------------------------------------------------


int Bembidion_Adult::st_Aggregate()
{
   return DailyMovement(random(MaxDailyMovement)+1, false);
}
//-----------------------------------------------------------------------------

/**
Accumulates day degrees from the beginning of the year and starts dispersal when
the sum exceeds a threshold or at the beginning of March.\n
At the end of hibernation the beetle is allowed to reproduce via the m_CanReproduce attribute.\n
*/
int Bembidion_Adult::st_Hibernate()
{
    int day=m_OurLandscape->SupplyDayInYear();
    if (day==0) m_HibernateDegrees=0;
    else if (day>100) return 0; // don't disperse if after spring
    double temp=m_OurLandscape->SupplyTemp();
    if (temp>DispersalThreshold) m_HibernateDegrees+=temp-DispersalThreshold;
    if ((day>=60)||(m_HibernateDegrees>=DipsersalDayDegrees))
    {
      //If we don't start dispersing then ensure we enter this code next time
      m_HibernateDegrees=DipsersalDayDegrees;
      // test against the chance of dispersal
      // assume that it takes about one month to get 120 DDs
      // 30*(10-6)=120 - ALSO ASSUMES MEAN TEMP OF 10 DEGREES C
      // Therefore the probability of dispersing per day is that prob when
      // summed culmulatively gives 0.92 after 30 days (0.92 is the observed
      // dispersal % at 120DDs above 6 degrees C
      // this value is 0.081
      if (random(1000)<81)
      {
        m_CanReproduce=true;//Can only repoduce after hibernating else too young
        return 1; // dispersing
      }
      if (day>90)
      {
        m_CanReproduce=true;//Can only repoduce after hibernating else too young
        return 1; // dispersing
      }
    }
    return 0; // still hibernating
}
//-----------------------------------------------------------------------------


int Bembidion_Adult::st_Dispersal()
{
   if (WinterMort()) return 2; // die  - a once only test
   /**
   Will now move in a direction away from high densities of beetles and towards good habitat
   */
   DailyMovement(random(MaxDailyMovement)+1, true);
   return 0;
}
//---------------------------------------------------------------------------

// This version has no ageing whilst hibernating
bool Bembidion_Adult::st_Aging()
{
    double temp=m_OurLandscape->SupplyTemp();
    if (temp<0.0) m_negDegrees+=temp;
    // Check should the beetle die today if not hibernating
    if (CurrentBState!=tobs_Hibernating)
    {
      if (DailyMortality()) return true; // go to die
      if (m_CanReproduce)
      {
         if (m_EggCounter<0) return true; // All eggs laid so die
       }
    }
    return false; // carry on
}
//---------------------------------------------------------------------------

bool Bembidion_Adult::WinterMort()
{
   // Beetles are immobile during winter so only call this once at the
   // end of overwintering
   double mortchance;
   if (m_negDegrees>-40)
   {
      mortchance = 0.94925+(m_negDegrees*0.00426);
   }
   else if (m_negDegrees>-80)
   {
      mortchance = 1.16913+(m_negDegrees*0.01149);
   }
   else if (m_negDegrees>-107)
   {
      mortchance = 0.6665+(m_negDegrees*0.00528);
   }
   else mortchance=0.1; // max 90% mortality
   if (random(1000) < ((1-mortchance)*1000)) return true; // die
   else return false;
}
//---------------------------------------------------------------------------

/**
Depending whether the beetle is dispersing or aggregating this calls either MoveTo or MoveToAggr.
*/
int Bembidion_Adult::DailyMovement(int p_distance, bool p_disp_aggreg)
{
	// If its too cold then do not do any movement today
	if (m_OurLandscape->SupplyTemp() < 1.0) return 0;
	// are we aggregating or dispersing?
	if (p_disp_aggreg == true)
	{
		// Dispersing
		int turning = AdultTurnRate; // almost no change of turning
		// Do the movement
		MoveTo(p_distance, OldDirection, turning);
		if (DDepMort()) return 2; // die
		return 0; // carry on dispersing
	}
	else
	{
		return MoveToAggr(p_distance, OldDirection, AdultTurnRate);
	}
}
//---------------------------------------------------------------------------


void Bembidion_Adult::MoveTo(int p_distance,unsigned p_direction,int p_turning)
{
  /**
  Move p_distance steps in p_direction with p_turning out of 100 chance of
  changing p_direction. This function will alter m_Location_x & m_Location_y.\n
  p_direction gives the preferred direction (0-7)\n
  p_distance is the number of steps\n
  The beetles will stop moving in suitable hibernatio habitat late in the year.\n
  \n
  This method is very similar to Bembidion_Adult::MoveToAggr but differs in the direction of movement
  relative to habitat types and the end of movement behaviour. In this case the beetl tries to lay eggs
  in MoveToAggre it test for hibernation conditions.\n
  */
  int vx = m_Location_x;
  int vy = m_Location_y;
  pPoint.direction=p_direction;
  pList.nsteps=0;
  if (random(100)<p_turning)
  {
     // change direction
     if (random(2)==1) pPoint.direction=((1+pPoint.direction) & 7U);
      else pPoint.direction=((7+pPoint.direction) & 0x07); //same as p_direction-1 but +ve
  }

  /*
    Improved speed here by checking if the max distance goes out of bounds
    i.e. x+p_distance, y+p_distance & x-p_distance,y-p_distance
    if not then there is no need to check for out of bounds later at all
  */

  if ((vx-p_distance<0) || (vy-p_distance<0) || (vx+p_distance>=m_OurPopulation->SimW)
          || (vy+p_distance>=m_OurPopulation->SimH))
  {
    //---- Duplicated with out of bounds check for speed
    for (int i=0; i<p_distance; i++)
    {
      // test the squares at Vector, Vector+1+2, Vector-1-2
      // if they are inaccessible (water,buildings,forest) try another square
      //
      int qual=3;
      int tries=0;
      while ((qual==3)&&(tries++<10))
      {
        // first watch out for out of bounds
        pPoint.x=vx+Vector_x[pPoint.direction];
        pPoint.y=vy+Vector_y[pPoint.direction];
        if (pPoint.x<0) pPoint.x=
                       (pPoint.x+m_OurPopulation->SimW)%m_OurPopulation->SimW;
         else if (pPoint.x>=m_OurPopulation->SimW)
                                      pPoint.x=pPoint.x%m_OurPopulation->SimW;
        if (pPoint.y<0)
        {
          pPoint.y=(pPoint.y+m_OurPopulation->SimH)%m_OurPopulation->SimH;
        }
        else if (pPoint.y>=m_OurPopulation->SimH)
             {
               pPoint.y=pPoint.y%m_OurPopulation->SimH;
             }
        qual=MoveTo_quality_assess();
      }
      if (qual!=3)
      {
        pList.BeenThereX[pList.nsteps]=pPoint.x;
        pList.BeenThereY[pList.nsteps++]=pPoint.y;
        vx=pPoint.x;
        vy=pPoint.y;
        OldDirection=pPoint.direction;
      }
      else
      {
        pList.BeenThereX[pList.nsteps]=vx;
        pList.BeenThereY[pList.nsteps++]=vy;
      }
    }
  }
  //---- Duplicated for speed
  else // No Out of bounds possible
  {
    for (int i=0; i<p_distance; i++)
    {
      // test the squares at Vector, Vector+1+2, Vector-1-2
      // if they are inaccessible (water,buildings,forest) try another square
      //
      int qual=3;
      int tries=0;
      while ((qual==3)&&(tries++<10))
      {
        pPoint.x=vx+Vector_x[pPoint.direction];
        pPoint.y=vy+Vector_y[pPoint.direction];
        qual=MoveTo_quality_assess();
      }
      if (qual!=3)
      {
        pList.BeenThereX[pList.nsteps]=pPoint.x;
        pList.BeenThereY[pList.nsteps++]=pPoint.y;
        vx=pPoint.x;
        vy=pPoint.y;
        OldDirection=pPoint.direction;
      }
      else
      {
        pList.BeenThereX[pList.nsteps]=vx;
        pList.BeenThereY[pList.nsteps++]=vy;
      }
    }
  }
  // alter the location
  m_OurPopulation->m_AdultPosMap->ClearMapValue(m_Location_x,m_Location_y);
  m_Location_x=vx;
  m_Location_y=vy;
  m_OurPopulation->m_AdultPosMap->SetMapValue(m_Location_x,m_Location_y);
  /** Try to reproduce */
  CanReproduce();
}
//---------------------------------------------------------------------------

void Bembidion_Adult::CanReproduce()
{
  //are we reproducing?
  if (m_CanReproduce)
  {
    TTypesOfLandscapeElement tole;
    tole=m_OurLandscape->SupplyElementType(m_Location_x, m_Location_y);
    // First find out how many eggs to produce depending on temperature
    int NoEggs=m_OurPopulation->TodaysEggProduction;
    // Calcuated in the pop man so it only needs calculating once
    switch(tole)
    {
      case tole_Field:
      case tole_Orchard:
	  case tole_FieldBoundary:
	  case tole_UnsprayedFieldMargin:
		  break;
      case tole_PermPasture:
      case tole_PermPastureLowYield:
      case tole_PermPastureTussocky:
      case tole_RoadsideVerge:
	  case tole_NaturalGrassDry: // 110
      case tole_NaturalGrassWet:
	  case tole_YoungForest:
	  case tole_MownGrass:
	  case tole_BeetleBank:
	  case tole_WaterBufferZone:
		NoEggs = NoEggs>>1;
       break;
      default:
       NoEggs=0;
       break;
    }
    if (NoEggs>0)
    {
     for (int i=0; i<NoEggs; i++)
     {
      int place=random(pList.nsteps);
      Reproduce(pList.BeenThereX[place],pList.BeenThereY[place]);
     }
     m_EggCounter-=NoEggs;
     // _EggCounter will go negative when all eggs are used
     //- allows a small over-production
    }
  }
};
//---------------------------------------------------------------------------

void Bembidion_Adult::Reproduce(int p_x, int p_y)
{
   struct_Bembidion * BS;
   BS = new struct_Bembidion;
   BS->x   = p_x;
   BS->y   = p_y;
   BS->L   = m_OurLandscape;
   BS->BPM = m_OurPopulation;
   BS->DayDegrees=0;
   BS->HowMany=1;
   // don't need delete because object destroyed by Population_Manager
   m_OurPopulation->CreateObjects(0,this,NULL,BS,1);
   delete BS;
}
//---------------------------------------------------------------------------



/**
A version of MoveTo where movement is determined by the suitability of the habitat moved to.
*/
int Bembidion_Adult::MoveTo_quality_assess()
{
   int qual;
   // get the quality of the test area
   // if it is 3 then don't go here
   // 2 can walk there but it is non-habitat
   // 1 linear features e.g. hedges, roadside verges
   // 1 grassland
   // 0 crop field
   if (!m_OurPopulation->m_AdultPosMap->GetMapValue(pPoint.x,pPoint.y))
   {
     qual = m_OurPopulation->m_MoveMap->GetMapValue(pPoint.x,pPoint.y);
   }
   else qual=3;
   // if it is 3 then don't go here
   switch (qual)
   {
     case 3:  // Cannot go
      // try another direction
      if (random(2)==1) pPoint.direction=((1+pPoint.direction) & 0x07);
      else pPoint.direction=((7+pPoint.direction)& 0x07);
      break;
     case 2:
      // Have a 40% chance of moving on to it
      if (random(100)>=40)
      {
        // try another direction
        if (random(2)==1) pPoint.direction=((1+pPoint.direction) & 0x07);
         else pPoint.direction=((7+pPoint.direction) & 0x07); // Reverse
        break;
      }
     case 1:
     case 0:
      break;
     default:
      // Should never happen
      m_OurLandscape->Warn("Bembidion Adult:: MoveTo QA Bad Direction",NULL);
      exit(1);
   }
   return qual;
}
//---------------------------------------------------------------------------

int Bembidion_Adult::MoveToAggr(int p_distance,unsigned p_direction,int p_turning)
{
  /**
  Move p_distance steps in p_direction with p_turning out of 100 chance of
  changing p_direction. This function will alter m_Location_x & m_Location_y.\n
  p_direction gives the preferred direction (0-7)\n
  p_distance is the number of steps\n
  The beetles will stop moving in suitable hibernatio habitat late in the year.
  */
  int vx = m_Location_x;
  int vy = m_Location_y;
  int tvx=0;
  int tvy=0;
  if (random(100)<p_turning)
  {
     // change direction
     if (random(2)==1) p_direction=((1+p_direction) & 0x07);
      else p_direction=((7+p_direction) & 0x07);
  }

  /*
    Improved speed here by checking if the max distance goes out of bounds
    i.e. x+p_distance, y+p_distance & x-p_distance,y-p_distance
    if not then there is no need to check for out of bounds later at all
  */
  if ((vx-p_distance<0) || (vy-p_distance<0)
                                      || (vx+p_distance>=m_OurPopulation->SimW)
                                      || (vy+p_distance>=m_OurPopulation->SimH))
  {
  //---- Duplicated with out of bounds check for speed
  for (int i=0; i<p_distance; i++)
  {
    // test the squares at Vector, Vector+1+2, Vector-1-2
    // if they are inaccessible (water,buildings,forest) try another square
    //
    int qual=3;
    int tries=0;
    while ((qual==3)&&(tries++<10))
    {
      // first watch out for out of bounds
      tvx=vx+Vector_x[p_direction];
      tvy=vy+Vector_y[p_direction];
      if (tvx<0) tvx=(tvx+m_OurPopulation->SimW)%m_OurPopulation->SimW;
       else if (tvx>=m_OurPopulation->SimW) tvx=tvx%m_OurPopulation->SimW;
      if (tvy<0) {tvy=(tvy+m_OurPopulation->SimH)%m_OurPopulation->SimH;}
       else if (tvy>=m_OurPopulation->SimH) {tvy=tvy%m_OurPopulation->SimH;}
      // get the quality of the test area
      qual = m_OurPopulation->m_MoveMap->GetMapValue(tvx,tvy);
      // if it is 3 then don't go here
      switch (qual)
      {
        case 3:  // Cannot go
         // try another direction
         if (random(2)==1) p_direction=((1+p_direction)  & 0x07);
          else p_direction=((7+p_direction) & 0x07);
         break;
        case 2:
         // Have a 40% chance of moving on to it
         if (random(100)>=40)
         {
           // try another direction
           if (random(2)==1) p_direction=((1+p_direction) & 0x07);
            else p_direction=((7+p_direction) & 0x07);
           break;
         }
        case 1:
          // This is where we want to be
          i=p_distance;
        case 0:
         break;
        default:
        // Should never happen
        m_OurLandscape->Warn("Bembidion Adult:: MoveTo Aggr Bad Direction",NULL);
        exit(1);
      }
    }
    if (qual!=3)
    {
      vx=tvx;
      vy=tvy;
      OldDirection=p_direction;
    }
  }
  }
  //---- Duplicated for speed
  else // No Out of bounds possible
  {
  for (int i=0; i<p_distance; i++)
  {
    tvx=vx+Vector_x[p_direction];
    tvy=vy+Vector_y[p_direction];
    // if they are inaccessible (water,buildings,forest) try another square
    //
    int qual=3;
    int tries=0;
    while ((qual==3)&(tries++<10))
    {
      // get the quality of the test area
      // if it is 3 then don't go here
      // 2 can walk there but it is non-habitat
      // 1 linear features e.g. hedges, roadside verges
      // 1 grassland
      // 0 crop field
      qual = m_OurPopulation->m_MoveMap->GetMapValue(tvx,tvy);
      // if it is 3 then don't go here
      switch (qual)
      {
        case 3:  // Cannot go
         // try another direction
         if (random(2)==1) p_direction=((1+p_direction) & 0x07);
          else p_direction=((7+p_direction) & 0x07);
         break;
        case 2:
         // Have a 40% chance of moving on to it
         if (random(100)>=40)
         {
           // try another direction
           if (random(2)==1) p_direction=((1+p_direction) & 0x07);
            else p_direction=((7+p_direction) & 0x07);
           break;
         }
        case 1:
          // where we want to be so stop for a while
          i=p_distance;
        case 0:
         break;
        default:
        // Should never happen
        m_OurLandscape->Warn("Bembidion Adult:: MoveTo Aggr 2 Bad Direction",NULL);
        exit(1);
      }
    }
    if (qual!=3)
    {
      vx=tvx;
      vy=tvy;
      OldDirection=p_direction;
    }
  }
  }
  // alter the location
  m_OurPopulation->m_AdultPosMap->ClearMapValue(m_Location_x,m_Location_y);
  m_Location_x=vx;
  m_Location_y=vy;
  m_OurPopulation->m_AdultPosMap->SetMapValue(m_Location_x,m_Location_y);
  int hab=m_OurPopulation->m_MoveMap->GetMapValue(m_Location_x, m_Location_y);
  if (m_OurLandscape->SupplyDayInYear()>363)
  {
     // Has reached January so must decide if can hibernate here or die
     if ((hab==1)||(random(100)<FieldHibernateChance)) return 1;
      else return 2; // die
  }
  else
  if (m_OurLandscape->SupplyDayInYear()>StopAggregationDay)
  {
	  TTypesOfLandscapeElement tole = m_OurLandscape->SupplyElementType(m_Location_x, m_Location_y);
	  switch (tole)
	  {
		  case tole_Hedges: // 130
		  case tole_RoadsideVerge: // 13
		  case tole_FieldBoundary: // 160
		  case tole_HedgeBank:
		  case tole_BeetleBank:
		  case tole_PermPasture: // 35
		  case tole_PermPastureLowYield: // 35
		  case tole_PermPastureTussocky: // 26
		  case tole_Heath:
		  case tole_NaturalGrassDry: // 110
		  case tole_NaturalGrassWet:
		  case tole_AmenityGrass:
		  case tole_Orchard:  //55
		  case tole_MownGrass:  //58
		  case tole_YoungForest:
		  case tole_WaterBufferZone:
			  return 1; //late in year in an OK place so stop
		  default:
			  return 0; // Carry on aggregating
	  }
  }
  return 0; // Carry on aggregating
}
//---------------------------------------------------------------------------
//-------------------------------------------------------------------------------------
void Bembidion_Adult::InternalPesticideHandlingAndResponse() {
	/**
	* If the body burden exceeds the trigger then an effect is tested for and implemented depending on the pesticide type.
	*/
	// Test against the threshold level for Adult to determine if an effect needs to be tested for
	if (m_body_burden > m_PPPThreshold) {
		// We are above the PPP body burden threshold, so make a test for effect
		if (g_rand_uni() <m_PPPEffectProb) {
			TTypesOfPesticide tp = m_OurLandscape->SupplyPesticideType();
			switch (tp) {
			case ttop_NoPesticide:
				break;
			case ttop_ReproductiveEffects: // Reproductive effects
			   // TODO
				break;
			case ttop_AcuteEffects: // Acute mortality
				st_Die(); // Kill this one now
				break;
			case ttop_AcuteDelayedEffects:
				m_currentPPPEffectProb = m_PPPEffectProbDecay;
				break;
			case ttop_MultipleEffects:
				break;
			default:
				g_msg->Warn("Unknown pesticide type used in Bembidion_Adult::InternalPesticideHandlingAndResponse() pesticide code ", int(tp));
				exit(47);
			}
		}
	}
}
//-------------------------------------------------------------------------------------

void Bembidion_Adult::BeginStep()
{
  CheckManagement();
  // Die if mortality occurs or is too old
  if(st_Aging()) CurrentBState=tobs_ADying;
}
//---------------------------------------------------------------------------

void Bembidion_Adult::EndStep()
{
#ifdef __BEETLEPESTICIDE1
	m_body_burden *= m_AdultPPPElimRate;
	// Pick up the pesticide to add to the body burden
	m_body_burden += m_OurLandscape->SupplyPesticide(m_Location_x, m_Location_y, ppp_1);
	InternalPesticideHandlingAndResponse();
	if (m_currentPPPEffectProb != 0)
	{
		// Kill the beetle following a fixed probability of death. The test is taken each day until it dies.
		if (g_rand_uni() < m_currentPPPEffectProb) st_Die();
	}
#endif  
}
//---------------------------------------------------------------------------

void Bembidion_Adult::Step()
{
  if (m_StepDone || m_CurrentStateNo == -1) return;
  switch (CurrentBState)
  {
  case 0:  // nothing
    CurrentBState=tobs_Foraging;
    break;
  case tobs_Foraging:  // Forage
    switch (st_Forage())
    {
     case 0:
      m_StepDone=true;
      break;
     case 1:
      CurrentBState=tobs_Aggregating;
      m_StepDone=true;
      break;
     case 2:
      CurrentBState=tobs_ADying;
      m_StepDone=true;
      break;
    }
    break;
  case tobs_Aggregating:  // Aggregate
    switch (st_Aggregate())
    {
     case 0:
      m_StepDone=true;
      break;
     case 1:
      CurrentBState=tobs_Hibernating;
      m_StepDone=true;
      break;
     case 2:
      CurrentBState=tobs_ADying;
      m_StepDone=true;
      break;
     default:
     // Should never happen
     m_OurLandscape->Warn("Bembidion Adult Unknown Return State From st_Aggregate",NULL);
     exit(1);
    }
    break;
  case tobs_Hibernating:  // Hibernate
    switch (st_Hibernate())
    {
     case 0:
      m_StepDone=true;
      break;
     case 1:
      CurrentBState=tobs_Dispersing; // Dispersal
      m_StepDone=true;
      break;
     default:
     m_OurLandscape->Warn("Bembidion Adult Unknown Return State From st_Hibernate",NULL);
     exit(1);
      break;
    }
    break;
  case tobs_Dispersing:  // Disperse
    switch (st_Dispersal())
    {
     case 0:
      m_EggCounter=cfg_TotalNoEggs.value(); // start the egg production counter
      CurrentBState=tobs_Foraging; // Forage
      m_StepDone=true;
      break;
     case 2:
      CurrentBState=tobs_ADying; // die
      break;
     default:
     m_OurLandscape->Warn("Bembidion Adult Unknown Return State From st_Dispersal",NULL);
     exit(1);
    }
    break;
  case tobs_ADying:  // Die
    m_OurPopulation->m_AdultPosMap->ClearMapValue(m_Location_x, m_Location_y);
    st_Die();
    m_StepDone=true;
    break;
  default:
  m_OurLandscape->Warn("Bembidion Adult Unknown State",NULL);
  g_msg->Warn(WARN_BUG, "State attempted was: ", int(CurrentBState));
  exit(1);
  }
}
//---------------------------------------------------------------------------

/**
Determines whether mortality has occured due to density-independen factors. Uses DailyAdultMort as a probability of
dying.\n
*/
bool Bembidion_Adult::DailyMortality()
{
	if (g_rand_uni()<DailyAdultMort)
	{
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------


/**
Determines whether mortality has occured due to density-dependence. Uses ADDepMort1 as a probability of
dying if adult beetles are found by GetMapDensity.\n
*/
bool Bembidion_Adult::DDepMort()
{
//   Modified ***CJT*** 26-05-2009
   int range=cfg_DDepRange.value();
   int x=m_Location_x;
   int y=m_Location_y;
   // Find out if there is any danger of using wrap round
   if ((x<0)||(y<0)|| (x>=m_OurPopulation->SimW-range) ||(y>=m_OurPopulation->SimH-range))
   {
	   int diff = 0;
	   if (x<0) {
		   diff = abs(x);
	   } else if (x>=m_OurPopulation->SimW-range) diff = (x+range)-m_OurPopulation->SimW;
	   if (y<0) {
		   if (diff< abs(y)) diff = abs(y);
	   } else if (y>=m_OurPopulation->SimH-range){
		   int diff2=(y+range)-m_OurPopulation->SimH;
		   if (diff2>diff) diff=diff2;
	   }
	   range-=diff;
   }
   if (m_OurPopulation->m_AdultPosMap->GetMapDensity(m_Location_x-range,m_Location_y-range,range*2)>m_OurPopulation->ADDepMort0) {
		   if (g_rand_uni()<m_OurPopulation->ADDepMort1) return true; // die
	   }
   return false;
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//                      BEMBIDION_POPULATION_MANAGER
//---------------------------------------------------------------------------


Bembidion_Population_Manager::Bembidion_Population_Manager(Landscape* p_L)
   : Population_Manager(p_L)
{
  // Four lists are needed so need to remove 6 of the ten default arrays
  // Eggs, Larvae, Pupae & Adults
  for (int i=0; i<4; i++)
  {
    TheArray.pop_back();
  }
  m_MoveMap=new MovementMap(p_L, 0); // 0 for beetles
  m_LarvaePosMap=new SimplePositionMap(p_L);
  m_AdultPosMap=new SimplePositionMap(p_L);
// Create the egg lists
  for (int i=0; i<365; i++)
  {
    m_EList[i]=new Bembidion_Egg_List(i,this,p_L);

  }
  // Clear the m_DayDeg array
  for (int i=0; i<365; i++)
  {
    m_EDayDeg[i]=0;
    m_LDayDeg[i][0]=0;
    m_LDayDeg[i][1]=0;
    m_LDayDeg[i][2]=0;
    m_PDayDeg[i]=0;
  }
  MaxDailyMovement=cfg_MaxDailyMovement.value();
  LDDepMort0=cfg_LDDepMort0.value();
  LDDepMort1=cfg_LDDepMort1.value()/100.0;
  ADDepMort0=cfg_ADDepMort0.value();
  ADDepMort1=cfg_ADDepMort1.value()/100.0;
  TodaysEggProduction=0;
  Init();
}
//---------------------------------------------------------------------------


Bembidion_Population_Manager::~Bembidion_Population_Manager()
{
  for (int i=0; i<365; i++) delete m_EList[i];
  delete m_MoveMap;
  delete m_LarvaePosMap;
  delete m_AdultPosMap;
}
//---------------------------------------------------------------------------

/**
Initialises the population manager and creates the initial beetle population.\n
Housekeeping actions such as setting up probe files and recording life-stage and state names are done here.\n
An optimisation is to sort the larval list by x-coordinate to speed searching later. This removes the default
randomising function for larvae.\n
*/
void Bembidion_Population_Manager::Init()
{

 // autom. called by constructor
	if (cfg_RipleysOutput_used.value()) {
		OpenTheRipleysOutputProbe("");
	}
	if ( cfg_ReallyBigOutput_used.value() ) {
    OpenTheReallyBigProbe();
  } else ReallyBigOutputPrb=0;
 strcpy(m_SimulationName,"Bembidion");
 // Create cfg_beetlestartnos adults
 struct_Bembidion* aps;
 aps = new struct_Bembidion;
 aps->BPM = this;
 aps->L = m_TheLandscape;
 for (int i=0; i<cfg_beetlestartnos.value(); i++)
 {
   do
   {
     aps->x = random(m_TheLandscape->SupplySimAreaWidth());
     aps->y = random(m_TheLandscape->SupplySimAreaHeight());
   } while (!IsStartHabitat(aps->x, aps->y));
   CreateObjects(3,NULL,NULL,aps,1);
 }
 delete aps;
 m_AdPopSize=cfg_beetlestartnos.value();
 m_EPopSize=0;
 m_LPopSize=0;
 m_PPopSize=0;

// Load List of Animal Classes
  m_ListNames[0]="Egg";
  m_ListNames[1]="Larva";
  m_ListNames[2]="Pupa";
  m_ListNames[3]="Adult";
  m_ListNameLength = 4;
  m_population_type = TOP_Beetle;

// Load State Names
  StateNames[tobs_Initiation] = "Initiation";
//Egg
  StateNames[tobs_EDeveloping] = "Developing";
  StateNames[tobs_Hatching] = "Hatching";
  StateNames[tobs_EDying] = "Dying";
//Larva
  StateNames[tobs_LDeveloping] = "Developing";
  StateNames[tobs_Pupating] = "Pupating";
  StateNames[tobs_LDying] = "Dying";
//Pupa
  StateNames[tobs_PDeveloping] = "Developing";
  StateNames[tobs_Emerging] = "Emerging";
  StateNames[tobs_PDying] = "Dying";
//Adult
  StateNames[tobs_Foraging] = "Foraging";
  StateNames[tobs_Aggregating] = "Aggregating";
  StateNames[tobs_Hibernating] = "Hibernating";
  StateNames[tobs_Dispersing] = "Dispersing";
  StateNames[tobs_ADying] = "Dying";

  // Ensure that larvae are sorted w.r.t. x position not shuffled
  BeforeStepActions[1]=1; // 1 = SortX

#ifdef __RECORD_RECOVERY_POLYGONS
	/* Open the output file and append */
	ofstream ofile("RecoveryPolygonsCounter.txt",ios::out);
	ofile << "This file records the number of females in each polygon each day" << endl;
	ofile.close();
	/* Open the polygon recovery file and read in polygons to m_RecoveryPolygons */
	ifstream ifile("RecoveryPolygonsList.txt",ios::in);
	int n;
	ifile >> n;
	m_RecoveryPolygons[0] = n;
	for (int i=0; i<n; i++) ifile >> m_RecoveryPolygons[1+i];
	for (int i=0; i<n; i++) m_RecoveryPolygonsC[1+i]=0;
	ifile.close();
#endif

#ifdef __BEETLEPESTICIDE1
	PestMortLocOutputOpen();
	m_InFieldNo = 0;
	m_OffFieldNo = 0;
	m_InCropNo = 0;
	m_InCropRef = m_TheLandscape->TranslateVegTypes(cfg_InCropRef.value());
	if (cfg_SaveInfieldLocation.value()) LocOutputOpen();
#endif
	Bembidion_Adult ba(0,0,NULL,NULL);

	// Initialise any static variables
	ba.SetAdultPPPElimRate(cfg_BemAdultPPPElimiationRate.value());  // Initialize static variable
	ba.SetPPPThreshold(cfg_BemAdultPPPThreshold.value());  // Initialize static variable
	ba.SetPPPEffectProb(cfg_BemAdultPPPEffectProb.value());  // Initialize static variable
	ba.SetPPPEffectProbDecay(cfg_BemAdultPPPEffectProbDecay.value());  // Initialize static variable
}
//---------------------------------------------------------------------------
#ifdef __RECORD_RECOVERY_POLYGONS
void Bembidion_Population_Manager::RecordRecoveryPolygons()
{
	/**
	Only used for recovery pesticide experiment. 
	Loops through all beetle and records the numbers in any polygon identified as needing to be recorded in m_RecoveryPolygons[]
	*/
	int sz = (int) GetLiveArraySize[bob_Adult];
    for (int j=0; j<sz; j++)
    {
      int p = TheArray[bob_Adult][j]->SupplyPolygonRef();
	  for (int i=1; i<=m_RecoveryPolygons[0]; i++)
	  {
		  if (p == m_RecoveryPolygons[i]) {
			  m_RecoveryPolygonsC[i]++;
			  break;
		  }
	  }
    }
	/* Open the output file and append */
	ofstream ofile("RecoveryPolygonsCounter.txt",ios::app);
	for (int i=1; i<=m_RecoveryPolygons[0]; i++)
	{
		ofile << m_RecoveryPolygonsC[i] << '\t';
		m_RecoveryPolygonsC[i] = 0;
	}
	ofile << endl;
	ofile.close();
}
//---------------------------------------------------------------------------
#endif

bool Bembidion_Population_Manager::IsStartHabitat(int a_x, int a_y)
{
    TTypesOfLandscapeElement tole;
    tole=m_TheLandscape->SupplyElementType(a_x, a_y);
    switch(tole)
    {
      case tole_Field:
      case tole_Orchard:
      case tole_PermPasture:
      case tole_PermPastureLowYield:
      case tole_PermPastureTussocky:
      case tole_RoadsideVerge:
      case tole_NaturalGrassDry:
      case tole_NaturalGrassWet:
      case tole_FieldBoundary:
      case tole_UnsprayedFieldMargin:
	  case tole_YoungForest:
	  case tole_WaterBufferZone:
		  return true;
	  default:
		  return false;
	}
}

void Bembidion_Population_Manager::DoFirst()
{
	/**
	This method removes any young stages that have survived until winter.\n
	It subsequently calculates day degree development for each day for each beetle stage. This is an optimising
	strategy to prevent each beetle individual calculating these.\n
	Finally it replaces the Egg_List BeginStep functionality. This is also an optimisation since it only requires
	365 Egg_lists, one for each day of the year instead of millions of individual eggs.\n
	NB this breaks the traditional ALMaSS protocol for handling animal individuals.\n
	*/
#ifdef __RECORD_RECOVERY_POLYGONS
	/**
	There is also a special bit of code that is used for pesticide tests
	*/
	RecordRecoveryPolygons();
#endif
  int today=m_TheLandscape->SupplyDayInYear();
  // WHOOAA!! This is really dangerous if these objects are registered anywhere
  // else in the system
  if (today==364)
  {
	  for (unsigned j = 0; j<GetLiveArraySize( bob_Egg ); j++)
    {
      TheArray[bob_Egg][j]->KillThis(); // Destroy this
    }
	  for (unsigned j = 0; j<GetLiveArraySize( bob_Larva ); j++)
    {
      TheArray[bob_Larva][j]->KillThis(); // Destroy this
    }
	  for (unsigned j = 0; j<GetLiveArraySize( bob_Pupa ); j++)
    {
      TheArray[bob_Pupa][j]->KillThis(); // Destroy this
    }
  }
  else if (today==0)
  {
   for (int i=0; i<365; i++)
   {
     m_EList[i]->EggList.clear();
     m_EDayDeg[i]=0;
     m_LDayDeg[i][0]=0;
     m_LDayDeg[i][1]=0;
     m_LDayDeg[i][2]=0;
     m_PDayDeg[i]=0;
   }
  }

  double temptoday=m_TheLandscape->SupplyTemp();
  // Calculate the number of eggs laid today
  if (temptoday>AdultEggLayingThreshold)
    TodaysEggProduction=(int) floor(0.5+(((double)temptoday-
                          (double)AdultEggLayingThreshold)*EggProductionSlope));
  else TodaysEggProduction=0;
  if (TodaysEggProduction>10)TodaysEggProduction=10; // ensure no bigger than 10
  // Calculated the DayDegrees for the different stages today
  if (temptoday>DevelConst1)
  {
   temptoday-=DevelConst1; // Subtract the threshold
   if (temptoday>DevelopmentInflectionPoint)
   {
    for(int i=0; i<=today; i++)
    {
      double et,l1t,l2t,l3t,pt;
      et=temptoday*above12Egg;
      l1t=temptoday*above12Larvae[0];
      l2t=temptoday*above12Larvae[1];
      l3t=temptoday*above12Larvae[2];
      pt=temptoday*above12Pupae;
      m_EDayDeg[i]+=et;
      m_LDayDeg[i][0]+=l1t;
      m_LDayDeg[i][1]+=l2t;
      m_LDayDeg[i][2]+=l3t;
      m_PDayDeg[i] += pt;
    }
   }
   else
   {
    for(int i=0; i<=today; i++)
    {
      m_EDayDeg[i]+=temptoday;
      m_LDayDeg[i][0]+=temptoday;
      m_LDayDeg[i][1]+=temptoday;
      m_LDayDeg[i][2]+=temptoday;
      m_PDayDeg[i]+=temptoday;
    }
   }
  }
  // Store the current population sizes
  m_EPopSize = (int)GetLiveArraySize( bob_Egg );
  m_LPopSize = (int)GetLiveArraySize( bob_Larva );
  m_PPopSize = (int)GetLiveArraySize( bob_Pupa );
  m_AdPopSize = (int)GetLiveArraySize( bob_Adult);

#ifdef __LAMBDA_RECORD
  if (today==March) {
	  LamdaDumpOutput();
	  LamdaClear();
  }
#endif

#ifdef __BEETLEPESTICIDE1
  if (today==0) PestMortLocOutput();
#endif

  // This replaces the BeginStep function for the eggs
  for (int i=0; i<365; i++)
  {
    m_EList[i]->BeginStep();
  }
}
//---------------------------------------------------------------------------

void Bembidion_Population_Manager::DoBefore()
{
  // This replaces the step function for the eggs
  for (int i=0; i<365; i++) m_EList[i]->SetStepDone(false);
  for (int i=0; i<365; i++) m_EList[i]->Step();

}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

/*
class ReturnLessThanX
{
  public:
    bool operator()(TAnimal* A1, int x) const
    {
      return (A1->Supply_m_Location_x()<x);
    }
};
*/
//---------------------------------------------------------------------------
/*
class ReturnMoreThanX
{
  public:
    bool operator()(int x,TAnimal* A1) const
    {
      return (A1->Supply_m_Location_x()>x);
    }
};
*/
//---------------------------------------------------------------------------

/**
 Outputs the adult locations if needed \n
*/
void Bembidion_Population_Manager::DoLast()
{
	Population_Manager::DoLast();
#ifdef __BEETLEPESTICIDE1
	int today=m_TheLandscape->SupplyDayInYear();
	if (cfg_SaveInfieldLocation.value()) 
	{
		if (today>=cfg_SaveInfieldLocationStartDay.value())
		{
			if (cfg_SaveInfieldLocationStartDay.value() == -2) 
			{
				if (m_TheLandscape->SupplyDayInMonth() == 1) DoInFieldLocationOutput();
			}
			else
			{
				if ( (today - cfg_SaveInfieldLocationStartDay.value()) % cfg_SaveInfieldLocationInterval.value() == 0) DoInFieldLocationOutput();
			}
		}
	}
#endif
}
//---------------------------------------------------------------------------

/**
 This sorts the larval list according to state, then removes the dead ones. This is a little faster than
 leaving the job to the default population manager functionality.\n
*/
void Bembidion_Population_Manager::DoAlmostLast()
{
}
//---------------------------------------------------------------------------

/**
The parent class Probe method needs to be overridden because of the use of
the Egg_List class. Otherwise functionality is the same as parent class.\n
*/
float Bembidion_Population_Manager::Probe(int ListIndex,probe_data* p_TheProbe)
{
  // Counts through the list and goes through each area to see if the animal
  // is standing there and if the farm, veg or element conditions are met
  AnimalPosition Sp;
  float NumberSk = 0;
  if (ListIndex==0)
  {
     unsigned X;
     unsigned Y;
     // Four possibilites
     // either NoVegTypes or NoElementTypes or NoFarmTypes is >0 or all==0
     if (p_TheProbe->m_NoFarms!=0)
     {
       for (int n=0; n<365; n++)
       {
        for (unsigned j=0; j<m_EList[n]->EggList.size(); j++)
        {
         Y=m_EList[n]->EggList[j].m_y;
         X=m_EList[n]->EggList[j].m_x;
         unsigned Farm=m_TheLandscape->SupplyFarmOwner(X,Y);
         for (unsigned i=0; i<p_TheProbe->m_NoAreas; i++)
         {
           if ((X>=p_TheProbe->m_Rect[i].m_x1)
            && (Y>=p_TheProbe->m_Rect[i].m_y1)
            && (X<=p_TheProbe->m_Rect[i].m_x2)
            && (Y<=p_TheProbe->m_Rect[i].m_y2))
              for (unsigned k=0; k<p_TheProbe->m_NoFarms; k++)
              {
                if (p_TheProbe->m_RefFarms[k]==Farm)
                NumberSk++; // it is in the square so increment number
              }
         }
        }
       }
     }
     else if (p_TheProbe->m_NoEleTypes!=0)
     {
       for (int n=0; n<365; n++)
       {
        for (unsigned j=0; j<m_EList[n]->EggList.size(); j++)
        {
         Y=m_EList[n]->EggList[j].m_y;
         X=m_EList[n]->EggList[j].m_x;
         int EleType = (int)m_TheLandscape->SupplyElementType(X,Y);
         for (unsigned i=0; i<p_TheProbe->m_NoAreas; i++)
         {
           if ((X>=p_TheProbe->m_Rect[i].m_x1)
            && (Y>=p_TheProbe->m_Rect[i].m_y1)
            && (X<=p_TheProbe->m_Rect[i].m_x2)
            && (Y<=p_TheProbe->m_Rect[i].m_y2))
              for (unsigned k=0; k<p_TheProbe->m_NoEleTypes; k++)
              {
                if (p_TheProbe->m_RefEle[k]==EleType)
                NumberSk++; // it is in the square so increment number
              }
         }
        }
       }
     }
     else if (p_TheProbe->m_NoVegTypes!=0)
     {
       for (int n=0; n<365; n++)
       {
        for (unsigned j=0; j<m_EList[n]->EggList.size(); j++)
        {
         Y=m_EList[n]->EggList[j].m_y;
         X=m_EList[n]->EggList[j].m_x;
         int VegType = (int)m_TheLandscape->SupplyVegType(X,Y);
         for (unsigned i=0; i<p_TheProbe->m_NoAreas; i++)
         {
           if ((X>=p_TheProbe->m_Rect[i].m_x1)
            && (Y>=p_TheProbe->m_Rect[i].m_y1)
            && (X<=p_TheProbe->m_Rect[i].m_x2)
            && (Y<=p_TheProbe->m_Rect[i].m_y2))
               for (unsigned k=0; k<p_TheProbe->m_NoVegTypes; k++)
               {
                 if (p_TheProbe->m_RefVeg[k]==VegType)
                    NumberSk++; // it is in the square so increment number
               }
             }
           }
         }
     }
     else // both must be zero
     {
         for (int n=0; n<365; n++)
         {
           for (unsigned j=0; j<m_EList[n]->EggList.size(); j++)
           {
             Y=m_EList[n]->EggList[j].m_y;
             X=m_EList[n]->EggList[j].m_x;
             for (unsigned i=0; i<p_TheProbe->m_NoAreas; i++)
             {
               if ((X>=p_TheProbe->m_Rect[i].m_x1)
                && (Y>=p_TheProbe->m_Rect[i].m_y1)
                && (X<=p_TheProbe->m_Rect[i].m_x2)
                && (Y<=p_TheProbe->m_Rect[i].m_y2))
                  NumberSk++; // it is in the square so increment number

             }
           }
         }
      }
  }
  else
  {
     // Four possibilites
     // either NoVegTypes or NoElementTypes or NoFarmTypes is >0 or all==0
     if (p_TheProbe->m_NoFarms!=0)
     {
		 for (unsigned j = 0; j<GetLiveArraySize( ListIndex ); j++)
       {
         Sp=TheArray[ListIndex][j]->SupplyPosition();
         unsigned Farm=TheArray[ListIndex][j]->SupplyFarmOwnerRef();
         for (unsigned i=0; i<p_TheProbe->m_NoAreas; i++)
         {
           if ((Sp.m_x>=p_TheProbe->m_Rect[i].m_x1)
            && (Sp.m_y>=p_TheProbe->m_Rect[i].m_y1)
            && (Sp.m_x<=p_TheProbe->m_Rect[i].m_x2)
            && (Sp.m_y<=p_TheProbe->m_Rect[i].m_y2))
              for (unsigned k=0; k<p_TheProbe->m_NoFarms; k++)
              {
                if (p_TheProbe->m_RefFarms[k]==Farm)
                NumberSk++; // it is in the square so increment number
              }
         }
       }
     }
     else if (p_TheProbe->m_NoEleTypes!=0)
     {
		 for (unsigned j = 0; j < (unsigned) GetLiveArraySize( ListIndex ); j++)
       {
         Sp=TheArray[ListIndex][j]->SupplyPosition();
         for (unsigned i=0; i<p_TheProbe->m_NoAreas; i++)
         {
           if ((Sp.m_x>=p_TheProbe->m_Rect[i].m_x1)
            && (Sp.m_y>=p_TheProbe->m_Rect[i].m_y1)
            && (Sp.m_x<=p_TheProbe->m_Rect[i].m_x2)
            && (Sp.m_y<=p_TheProbe->m_Rect[i].m_y2))
              for (unsigned k=0; k<p_TheProbe->m_NoEleTypes; k++)
              {
                if (p_TheProbe->m_RefEle[k]==Sp.m_EleType)
                NumberSk++; // it is in the square so increment number
              }
         }
       }
     }
     else
     {
       if (p_TheProbe->m_NoVegTypes!=0)
       {
		   for (unsigned j = 0; j<(unsigned)GetLiveArraySize( ListIndex ); j++)
         {
           Sp=TheArray[ListIndex][j]->SupplyPosition();
           for (unsigned i=0; i<p_TheProbe->m_NoAreas; i++)
           {
             if ((Sp.m_x>=p_TheProbe->m_Rect[i].m_x1)
                 && (Sp.m_y>=p_TheProbe->m_Rect[i].m_y1)
                  && (Sp.m_x<=p_TheProbe->m_Rect[i].m_x2)
                   && (Sp.m_y<=p_TheProbe->m_Rect[i].m_y2))

             {
               for (unsigned k=0; k<p_TheProbe->m_NoVegTypes; k++)
               {
                 if (p_TheProbe->m_RefVeg[k]==Sp.m_VegType)
                    NumberSk++; // it is in the square so increment number
               }
             }
           }
         }
       }
       else // both must be zero
       {
		   for (unsigned j = 0; j<(unsigned)GetLiveArraySize( ListIndex ); j++)
         {
           Sp=TheArray[ListIndex][j]->SupplyPosition();
           for (unsigned i=0; i<p_TheProbe->m_NoAreas; i++)
           {
             if ((Sp.m_x>=p_TheProbe->m_Rect[i].m_x1)
              && (Sp.m_y>=p_TheProbe->m_Rect[i].m_y1)
              && (Sp.m_x<=p_TheProbe->m_Rect[i].m_x2)
              && (Sp.m_y<=p_TheProbe->m_Rect[i].m_y2))
                NumberSk++; // it is in the square so increment number
           }
         }
       }
     }
  }
     return NumberSk;
}
//---------------------------------------------------------------------------

/**
All bembidion objects that are created must be created using this method. Data on the location and other attributes
are passed in data, and the number to create in number.\n
*/
void Bembidion_Population_Manager::CreateObjects(int ob_type,
           TAnimal * /* pvo */ ,void* /* null */ ,struct_Bembidion * data,int number)
{
   Bembidion_Adult*  new_Adult;
   Bembidion_Pupae*  new_Pupa;
   Bembidion_Larvae* new_Larva;

   for (int i=0; i<number; i++)
   {
    if (ob_type == 0)
    {
      m_EList[data->L->SupplyDayInYear()]->AddEgg(data->x, data->y);
    }
	if (ob_type == 1) {
		// Will not create a new larva in a square already occupied
		if (m_LarvaePosMap->GetMapValue( data->x, data->y ) == 0) {
			m_LarvaePosMap->SetMapValue( data->x, data->y );
			if (unsigned(TheArray[ob_type].size())>GetLiveArraySize(ob_type )) {
				// We need to reuse an object
				dynamic_cast<Bembidion_Larvae*>(TheArray[ob_type][GetLiveArraySize(ob_type) ])->ReInit(data->x, data->y, data->L, this);
				IncLiveArraySize(ob_type);
			}
			else {
				new_Larva = new Bembidion_Larvae( data->x, data->y, data->L, this );
				//TheArray[ ob_type ].insert( TheArray[ ob_type ].begin(), new_Larva );
				TheArray[ ob_type ].push_back( new_Larva );
				IncLiveArraySize(ob_type);
			}
		}
	}
    if (ob_type == 2)
    {
		if (unsigned(TheArray[ob_type].size())>GetLiveArraySize(ob_type)) {
			// We need to reuse an object
			dynamic_cast<Bembidion_Pupae*>(TheArray[ob_type][GetLiveArraySize(ob_type)])->ReInit(data->x, data->y, data->L, this);
			IncLiveArraySize(ob_type);
		}
		else {
			new_Pupa = new Bembidion_Pupae( data->x, data->y, data->L, this );
			TheArray[ ob_type ].push_back( new_Pupa );
			IncLiveArraySize(ob_type);
		}
    }
    if (ob_type == 3)
    {
		if (unsigned(TheArray[ob_type].size())>GetLiveArraySize(ob_type)) {
			// We need to reuse an object
			dynamic_cast<Bembidion_Adult*>(TheArray[ob_type][GetLiveArraySize(ob_type)])->ReInit(data->x, data->y, data->L, this);
			IncLiveArraySize(ob_type);
		}
		else {
			new_Adult = new Bembidion_Adult( data->x, data->y, data->L, this );
			TheArray[ ob_type ].push_back( new_Adult );
			IncLiveArraySize(ob_type);
		}
    }
   }
}
//-----------------------------------------------------------------------------



/**
A spatial output. This dumps the x,y co-ordinates for all adults to a text file.\n
*/
void Bembidion_Population_Manager::TheRipleysOutputProbe( FILE* a_prb ) {
  Bembidion_Adult* FS;
  unsigned totalF = (unsigned)(unsigned)GetLiveArraySize( bob_Adult );
  int x,y;
  int w = m_TheLandscape->SupplySimAreaWidth();
  int h = m_TheLandscape->SupplySimAreaWidth();
  fprintf(a_prb,"%d %d %d %d %d\n", 0,w ,0, h, totalF);
  for (unsigned j=0; j<totalF; j++)      //adult females
  {
		  FS = dynamic_cast<Bembidion_Adult*>(TheArray[bob_Adult][j]);
		  if (FS->GetCurrentStateNo() != -1) {
			  x = FS->Supply_m_Location_x();
			  y = FS->Supply_m_Location_y();
			  fprintf(a_prb, "%d\t%d\n", x, y);
		  }
  }
  fflush(a_prb);
}
//-----------------------------------------------------------------------------

/**
Another spatial output method. This time with extra information over and above the x,y, coord found in the Ripley Probe.\n
*/
void Bembidion_Population_Manager::TheReallyBigOutputProbe(  ) {
  Bembidion_Adult* FS;
  unsigned totalF = (unsigned)GetLiveArraySize( bob_Adult );
  int x,y;
  int w = m_TheLandscape->SupplySimAreaWidth();
  int h = m_TheLandscape->SupplySimAreaWidth();
  fprintf(ReallyBigOutputPrb,"%d %d %d %d %d\n", 0,w ,0, h, totalF);
  for (unsigned j=0; j<totalF; j++)      //adult females
  {
	  FS=dynamic_cast<Bembidion_Adult*>(TheArray[3][j]);
	  if (FS->GetCurrentStateNo() != -1) {
		  x = FS->Supply_m_Location_x();
		  y = FS->Supply_m_Location_y();
		  int poly = m_TheLandscape->SupplyPolyRef(x, y);
		  fprintf(ReallyBigOutputPrb, "%d\t%d\t%d\n", x, y, poly);
	  }
  }
  fflush(ReallyBigOutputPrb);
}
void Bembidion_Population_Manager::TheAOROutputProbe()
{
	m_AOR_Probe->DoProbe(bob_Adult);
}
//-----------------------------------------------------------------------------

/**
	This method simply alters populations on 1st January - either by increasing or decreasing them. Increases are by
	'cloning', decrease by pro-rata mortality. The effect is under the control of configuration variables cfg_pm_eventfrequency
	and cfg_pm_eventsize.\n
*/
void Bembidion_Population_Manager::Catastrophe( void ) {

	// First do we have the right day?
	int today = m_TheLandscape->SupplyDayInYear();
	if (today!=1) return;
	// First do we have the right year?
	int year = m_TheLandscape->SupplyYearNumber()-m_catastrophestartyear;
	if (year%cfg_pm_eventfrequency.value()!=0) return;

	Bembidion_Adult* BA = NULL;
	// Now if the % decrease is higher or lower than 100 we need to do different things
	int esize=cfg_pm_eventsize.value();
	if (esize<100) {
		unsigned size2;
		size2 = (unsigned)GetLiveArraySize( bob_Adult );
			for ( unsigned j = 0; j < size2; j++ ) {
				if (random(100) > esize) {
                        BA = dynamic_cast < Bembidion_Adult * > ( TheArray[ bob_Adult ] [ j ] );
					    BA->CurrentBState=tobs_ADying; // Kill it
				}
			}
		}
	else if (esize>100) {
		// This also requires a copy method in the target animals
		// esize also needs translating  120 = 20%, 200 = 100%
		if (esize<200) {
			esize-=100;
			unsigned size2;
			size2 = (unsigned)GetLiveArraySize( bob_Adult );
			for ( unsigned j = 0; j < size2; j++ ) {
				if (random(100) < esize) {
                        BA = dynamic_cast < Bembidion_Adult * > ( TheArray[ bob_Adult ] [ j ] );
					    BA->CopyMyself(bob_Adult); // Duplicate it
					}
				}
		} else {
			esize-=100;
			esize/=100; // this will throw away fractional parts so will get 1, 2, 3  from 200, 350 400
				unsigned size2;
				size2 = (unsigned)GetLiveArraySize( bob_Adult );
				for ( unsigned j = 0; j < size2; j++ ) {
					for ( int e=0; e<esize; e++) {
						BA = dynamic_cast < Bembidion_Adult * > (TheArray[bob_Adult][j]);
						BA->CopyMyself(bob_Adult); // Duplicate it
						}
				}
			}
		}
	else return; // No change so do nothing
}
//-----------------------------------------------------------------------------
#ifdef __BEETLEPESTICIDE1

void Bembidion_Population_Manager::PestMortLocOutputError( void )
{
	cout << "Cannot open output file for Bembidion pesticide location output " << "Bem_PesticideLocationMort.txt" << endl;
	char ch;
	cin >> ch;
	exit(1);
}

void Bembidion_Population_Manager::PestMortLocOutputOpen( void )
{
	ofstream ofile("Bem_PesticideLocationMort.txt",ios::out);
	if ( !ofile.is_open() ) PestMortLocOutputError();
	ofile << "Year" << '\t' << "inCrop" << '\t' << "inField" << '\t' << "offField" << endl;
	ofile.close();
}

void Bembidion_Population_Manager::PestMortLocOutput( void )
{
	ofstream ofile("Bem_PesticideLocationMort.txt",ios::app);
	if ( !ofile.is_open() ) PestMortLocOutputError();
	ofile << m_TheLandscape->SupplyYearNumber()-1 << '\t' << m_InCropNo << '\t' << m_InFieldNo << '\t' << m_OffFieldNo << endl;
	ofile.close();
	m_InCropNo = 0;
	m_InFieldNo = 0;
	m_OffFieldNo = 0;
}

void Bembidion_Population_Manager::LocOutputOpen( void )
{
	ofstream ofile("Bem_Locations.txt",ios::out);
	if ( !ofile.is_open() ) PestMortLocOutputError();
	ofile << "Year" << '\t'  << "Day" << '\t' ;
	for (int i=0; i< 100; i++) 
	{
		ofile << "inCrop" << i << '\t' << "inField" << i << '\t' << "offField" << i << '\t';
	}
	ofile << "inCropTotal" << '\t' << "inFieldTotal" << '\t' << "offFieldTotal" << endl;
	ofile.close();
}


void Bembidion_Population_Manager::DoInFieldLocationOutput()
{
	// Records only adults
	unsigned no = (unsigned)GetLiveArraySize( bob_Adult );
	int incrop[100]; 
	int infield[100]; 
	int offfield[100];
	int mw10 = (m_TheLandscape->SupplySimAreaWidth()/10);
	int mh10 = (m_TheLandscape->SupplySimAreaHeight()/10);
	// Zero the counts
	for (int i=0; i<100; i++)
	{
		incrop[i] = 0; 
		infield[i] = 0; 
		offfield[i] = 0; 
	}
	for (unsigned i=0; i<no; i++)
	{
		// Need to get the element type, if a field need to check the crop
		APoint p = TheArray[bob_Adult][i]->SupplyPoint();
		// Find the right grid cell of our 100.
		int cell = (p.m_x / mw10) + ((p.m_y / mh10) * 10);
		TTypesOfLandscapeElement tole = m_TheLandscape->SupplyElementType(p.m_x, p.m_y);
		if  (tole == tole_Orchard) incrop[cell]++;
		else if ((tole == tole_Field) || ((tole == tole_UnsprayedFieldMargin)))
		{
			if (m_TheLandscape->SupplyVegType(p.m_x, p.m_y) == m_InCropRef) incrop[cell]++; 
		    else infield[cell]++;
		} else offfield[cell]++;
	}
	ofstream ofile("Bem_Locations.txt",ios::app);
	if ( !ofile.is_open() ) LocOutputError();
	ofile << m_TheLandscape->SupplyYearNumber() << '\t' <<  m_TheLandscape->SupplyDayInYear() << '\t';
	int incr=0,offc=0,offf=0;
	for (int i=0; i< 100; i++) 
	{
		ofile << incrop[i] << '\t' << infield[i] << '\t' << offfield[i] << '\t';
		incr += incrop[i];
		offc += infield[i];
		offf += offfield[i];
	}
	ofile << incr << '\t' << offc << '\t' << offf << endl;
	ofile.close();
}

void Bembidion_Population_Manager::LocOutputError( void )
{
	cout << "Cannot open output file for Bembidion location output " << "Bem_Locations.txt" << endl;
	char ch;
	cin >> ch;
	exit(1);
}


#endif











