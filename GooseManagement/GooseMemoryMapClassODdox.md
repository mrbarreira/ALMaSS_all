# ALMaSS Goose Memory Map Class Documentation

# Overview {#label-Memmap-Overview}

##  1. Purpose {#label-Memmap-Purpose}
The goose memory map provides a means for recording the goose's experiences in terms of food resources known and threats experienced.

##  2. State variables and scales {#label-Memmap-StateVarAndScales}
- All memories are held in a GooseMemoryLocation construct comprising:
	- #GooseMemoryLocation::m_age is the age of the memory.
	- #GooseMemoryLocation::m_x is the x-coordinate location.
	- #GooseMemoryLocation::m_y is the y-cordinate location.
	- #GooseMemoryLocation::m_polygonid is an reference number to the polygon at the memory location.
	- #GooseMemoryLocation::m_grain is grain resource memory strength.
	- #GooseMemoryLocation::m_grazing is the grazing resource memory strength.
	- #GooseMemoryLocation::m_foodresource is a variable describing the remembered food resouces.
	- #GooseMemoryLocation::m_threat is a variable describing the remembered threat at the location.
	- #GooseMemoryLocation::m_score is a score used to assess the usefulness of the location.
	- #GooseMemoryLocation::m_infinitememorystop is a limit at which a memory is so small it is forgotten.

- All memories are handled by GooseMemoryMap with the following attributes:
	- #GooseMemoryMap::m_memorylocations is the list of memory locations held.
	- #GooseMemoryMap::m_threatdecayrate is the rate at which threat memories decay per day.

##  3. Process Overview and Scheduling {#label-Memmap-ProcessOverAndSchedu}
Each time a location is visited it is scored for both threats and food resources. This and its location form the individual memory. 
Any threat memories will decay with time at a certain rate, defined by #GooseMemoryMap::m_threatdecayrate (see #GooseMemoryMap::DecayAllMemory()). This decay will occur daily until  GooseMemoryLocation::m_infinitememorystop is reached at which point the memory is forgotten and deleted. 
All memories are contained in a list GooseMemoryMap::m_memorylocations which can be consulted to determine the overall score for a location (its attractiveness) using #GooseMemoryMap::GetBestFeedingScore() or its food resource using #GooseMemoryMap::GetFoodRes() or threat level using #GooseMemoryMap::GetThreat(). The best/optimal location currently known about can be found using #GooseMemoryMap::GetBestFeedingScore() and the total threat level can also be queried using #GooseMemoryMap::GetTotalThreats(), which can be used to determine if a migration stop location is too dangerous a place to stay.
