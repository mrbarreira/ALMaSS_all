/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>Partridge_all.cpp This file contains the code for all partridge lifestage classes</B> \n
*/
/**
\file
 by Chris J. Topping \n
 Original version 15th October 2008 \n
 \n
 Doxygen formatted comments in October 2008 \n
*/
//---------------------------------------------------------------------------

// Includes from ALMaSS

#include <iostream>
#include <fstream>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Partridge/Partridge_Communication.h"
#include "../Partridge/Partridge_All.h"
#include "../Partridge/Partridge_Population_Manager.h"
#include "../Partridge/Partridge_Covey.h"
#include "../BatchALMaSS/BoostRandomGenerators.h"

//---------------------------------------------------------------------------


//
// partridge_all.cpp
//

extern double g_FoodNeed[200];
extern int g_MaxWalk[200];
extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;
extern double g_par_rainfactor;

/** \brief Generates variation around the laying trigger */
static CfgInt cfg_par_triggerlayingvar( "PAR_TRIGGERLAYINGVAR", CFG_CUSTOM, 48 );
/** \brief Food extraction rate */
/** The extraction rate includes the amount of insects removed per m2, plus assimilation efficiency to convert the g/insects/m2 to kcal available */
static CfgFloat cfg_par_extraction_rate( "PAR_EXTRACTION_RATE", CFG_CUSTOM, 0.0085 );
/** \brief Maximum physiological lifespan */
CfgInt cfg_par_max_lifespan( "PAR_MAX_LIFESPAN", CFG_CUSTOM, 3650 );
/** \brief Minimum physiological lifespan */
CfgInt cfg_par_min_lifespan( "PAR_MIN_LIFESPAN", CFG_CUSTOM, 1825 );
/** \brief The earliest covey dissolve date */
CfgInt cfg_par_start_dissolve( "PAR_START_DISSOLVE", CFG_CUSTOM, 30 );
/** \brief The number of minutes daylight as a minimum before triggering egg-laying behaviour */
static CfgInt cfg_par_triggerlaying( "PAR_TRIGGERLAYING", CFG_CUSTOM, 850 );
/** \brief Time period after finding territory before making nest */
static CfgInt cfg_buildupwait("PAR_BUILDUP_WAIT",CFG_CUSTOM,130); // Fitted 1/02/2008
/** \brief Days used for nestbuilding */
static CfgInt cfg_par_days_to_make_nest( "PAR_DAYS_TO_MAKE_NEST", CFG_CUSTOM, 5 );
/** \brief Lenght of incubation period */
static CfgInt cfg_par_incubation_period( "PAR_INCUBATION_PERIOD", CFG_CUSTOM, 24 );
/** \brief Age at maturing in days */
CfgInt cfg_par_mature_threshold( "PAR_MATURE_THRESHOLD", CFG_CUSTOM, 84 );
/** \brief Latest date for a reproduction attempt */
CfgInt cfg_par_last_brood_date( "PAR_LAST_BROOD_DATE", CFG_CUSTOM, 180 );
/** \brief Latest date a male will mate search */
CfgInt cfg_par_male_gives_up( "PAR_MALE_GIVES_UP", CFG_CUSTOM, 170 );
/** \brief The minimum territory quality needed in early season */
CfgInt cfg_par_max_terr_qual( "PAR_MAX_TERR_QUAL", CFG_CUSTOM, 13000 );//Added on March12th 2007.
/** \brief The minimum territory quality needed in late season */
CfgInt cfg_par_min_terr_qual( "PAR_MIN_TERR_QUAL", CFG_CUSTOM, 6900 );
/** \brief The maximum flying distance for males */
static CfgInt cfg_par_MaleFlyingDistance( "PAR_MALEFLYINGDISTANCE", CFG_CUSTOM, 2000 );
/** \brief Male mate searching radius */
static CfgInt cfg_par_MaleMateSearchArea( "PAR_MALEMATESEARCHAREA", CFG_CUSTOM, 500 );
/** \brief Female max daily movement */
/** According to Dick Potts: females stay closer to the covey than males so the female search area is much smaller than the males */
static CfgInt cfg_par_female_movement_max( "PAR_FEMALE_MOVEMENT_MAX", CFG_CUSTOM, 100 );
/** \brief Minimum clutch size */
static CfgInt cfg_par_min_no_eggs( "PAR_MIN_NO_EGGS", CFG_CUSTOM, 7 );
/** \brief Days taken to lay 1 egg */
static CfgFloat cfg_par_days_per_egg( "PAR_DAY_PER_EGG", CFG_CUSTOM, 1.5 );

// Density-depende mortality
/** \brief Increasing hen predation with density (slope) */
static CfgFloat cfg_par_female_incubation_mortality_slope( "PAR_FEMALE_INCUBATION_MORTALITY_SLOPE", CFG_CUSTOM, -0.00122 );
/** \brief Increasing hen predation with density (intercept) */
static CfgFloat cfg_par_female_incubation_mortality_const( "PAR_FEMALE_INCUBATION_MORTALITY_CONST", CFG_CUSTOM, 1.00192 );

// Start background mortality
/** \brief Unused */
static CfgInt cfg_par_base_mortality( "PAR_BASE_MORTALITY", CFG_CUSTOM, 0 );
/** \brief Clutch - daily probability of non-explicitly modelled mortality */
static CfgFloat cfg_par_clutch_mortality( "PAR_CLUTCH_MORTALITY", CFG_CUSTOM, 0.00135 );
/** \brief Chick - daily probability of non-explicitly modelled mortality */
static CfgFloat cfg_par_chick_mortality( "PAR_CHICK_MORTALITY", CFG_CUSTOM, 0.02043 );
/** \brief Chick2 - scaler applied to adult mortalities */
/** 3rd March 2008. Chick2 mortality is now a scaler applied to adult mort - so 1.0 = adult, 2.0 = double adult */
static CfgFloat cfg_par_chick2_mortality( "PAR_CHICK_TWO_MORTALITY", CFG_CUSTOM, 1.0 );
/** \brief Adults - daily probability of non-explicitly modelled mortality in Summer */
static CfgFloat cfg_par_adult_mortalitySummer( "PAR_ADULT_MORTALITYSUMMER", CFG_CUSTOM, 0.0001 );
/** \brief Adults - daily probability of non-explicitly modelled mortality in Spring */
static CfgFloat cfg_par_adult_mortalitySpring( "PAR_ADULT_MORTALITYSPRING", CFG_CUSTOM, 0.0096 );
/** \brief Adults - daily probability of non-explicitly modelled mortality in Winter */
static CfgFloat cfg_par_adult_mortalityWinter( "PAR_ADULT_MORTALITYWINTER", CFG_CUSTOM, 0.0217 );
/** \brief Adults - extra mortality scaler if alone */
static CfgFloat cfg_par_adult_mortality_alone( "PAR_ADULT_MORTALITY_ALONE", CFG_CUSTOM, 1.4 );
// End background mortality

/** \brief Chick - no. days or negative energy before death */
static CfgInt cfg_par_starve_threshold( "PAR_STARVE_THRESHOLD", CFG_CUSTOM, 4 );
/** \brief Proportion of infertile eggs */
static CfgFloat cfg_par_infertile_eggs( "PAR_INFERTILE_EGGS", CFG_CUSTOM, 0.08 );
// Farm mortality factors
/** \brief Adult % mortality on cutting */
static CfgFloat cfg_par_ad_cut( "PAR_AD_CUT_MORT", CFG_CUSTOM, 0.01 );
/** \brief Chick % mortality on cutting */
static CfgFloat cfg_par_ch_cut( "PAR_CH_CUT_MORT", CFG_CUSTOM, 0.05 );
/** \brief Clutch % mortality on cutting */
static CfgFloat cfg_par_cl_cut( "PAR_CL_CUT_MORT", CFG_CUSTOM, 0.29 );
/** \brief Chance of female death if incubating a mown clutch */
static CfgFloat cfg_female_mowing_chance( "PAR_FEMALEMOWNMORTALITY", CFG_CUSTOM, 1 );

/** \brief Clutch mortality on soil cultivation */
//static CfgFloat cfg_par_soil_cultivation_cl( "PAR_SOILCULTIVATIONMORT_CL", CFG_CUSTOM, 1.0 );
/** \brief Chick mortality on soil cultivation */
//static CfgFloat cfg_par_soil_cultivation_ch( "PAR_SOILCULTIVATIONMORT_CH", CFG_CUSTOM, 0.01 );
/** \brief Adult mortality on soil cultivation */
//static CfgFloat cfg_par_soil_cultivation_a( "PAR_SOILCULTIVATIONMORT_A", CFG_CUSTOM, 0.001 );

Partridge_Population_Manager * g_manager;
static long g_partridge_id = 0;

/**
  If a_flock is NULL, then assume this is an animal created during population manager initialization and that covey creation/addtion is controlled by the calling method.\n
*/
Partridge_Base::Partridge_Base( int a_born_x, int a_born_y, int a_x, int a_y, int a_family_counter, Partridge_Covey * a_covey,
     Landscape * a_map, Partridge_Population_Manager * a_manager ) : TAnimal( a_x, a_y, a_map )
     {
       m_age = 0;
       m_born_x = a_born_x;
       m_born_y = a_born_y;
       m_UncleStatus = false;
       m_family_counter = a_family_counter;
       m_id = g_partridge_id++;
	   m_covey = a_covey;
       // **CJT** NB it is the responsibility of the calling method
       // to ensure that if a_covey is NULL, then the bird is added later
       if ( m_covey ) m_covey->AddMember( this );
       m_state = pars_Initiation;
       m_OurPopulationManager = a_manager;
       m_signal = -9999; // used to trap non-set signal output
}

//------------------------------------------------------------------------------

/**
Checks whether the supplied bird is a potential mate, or is too closely related.\n
*/
bool Partridge_Base::PossibleMate( Partridge_Base * a_partridge )
{
  if ( ( a_partridge->GetFamily() < m_family_counter - 1 || a_partridge->GetFamily() > m_family_counter + 1 )
       // Possible age check?
       )
       {
         return true;
  }
  return false;
}

//------------------------------------------------------------------------------

Partridge_Base::~Partridge_Base( void )
{
  // Line removed because it causes all kinds of concurrency problems
  // removal from covey must occur when the bird is killed, not later in the day
  //  m_covey->RemoveMember( this );
}

//------------------------------------------------------------------------------


/**
Only called when a bird must make a new covey the bird will be the first member and the only one on calling. NB all housekeeping in the population manager is handled by the
Covey constructor. The caller is responsible for removing the bird from the present covey before this call is made (or immediately after).\n
*/
void Partridge_Base::MakeCovey( void )
{
  m_covey = new Partridge_Covey( this, m_OurPopulationManager, m_Location_x, m_Location_y, m_OurLandscape );
}

//------------------------------------------------------------------------------

/**
Moves the bird from its cuurent covey to the one supplied. \n
*/
void Partridge_Base::SwitchCovey( Partridge_Covey * a_covey )
{
	m_covey->RemoveMember( this );
	m_covey = a_covey;
	m_covey->AddMember( this );
}

//------------------------------------------------------------------------------

/**
This should not really be used.
*/
bool Partridge_Base::DailyMortality( void )
{
  int chance = random( 100000 );
  if ( chance < cfg_par_base_mortality.value() ) return true;
  return false;
}

//------------------------------------------------------------------------------
/**
A method required only for experimental manipulations - e.g. sudden doubling of population size.
*/
void Partridge_Base::CopyMyself(int a_Ptype) {
	AdultPartridge_struct * aps;
	aps = new AdultPartridge_struct;
	aps->bx = m_born_x;
	aps->by = m_born_y;
	aps->x = m_Location_x;
	aps->y = m_Location_y;
	aps->L = m_OurLandscape;
	aps->family_counter = random(1000000);
	aps->age = m_age;
	if (a_Ptype==pob_Male) {
		aps->sex=true;
	} else {
		aps->sex=false;
	}
	m_OurPopulationManager->CreateCloneObjects( a_Ptype, aps );
	delete aps;
}
//-----------------------------------------------------------------------------
/**
The constructor. Ensures sensible values for attributes not set by the base constructor.
*/
Partridge_Clutch::Partridge_Clutch( int a_born_x, int a_born_y, Partridge_Female * a_mother, Partridge_Covey * a_covey,
     Landscape * a_map, int a_num_eggs, int a_family_counter, Partridge_Population_Manager * a_manager ) :
     Partridge_Base( a_born_x, a_born_y, a_born_x, a_born_y, a_family_counter, a_covey, a_map, a_manager )
     {
       m_mother = a_mother;
       m_clutch_size = a_num_eggs;
       m_object_type = pob_Clutch;
	   m_underincubation = false;
}

//------------------------------------------------------------------------------

double Partridge_Female::GetNestingCoverDensity()
{
  return m_OurPopulationManager->GetNestingCoverDensity( m_Location_x, m_Location_y );
}
//------------------------------------------------------------------------------

/**
Does nothing more than check bacground mortalities and management mortalities.\n
*/
void Partridge_Clutch::BeginStep( void )
{

  if ( !DailyMortality() ) CheckManagement();
  else
  {
    ClDying();
  }
}

//------------------------------------------------------------------------------

/**
Does little here except initiating behaviour and ensuring m_StepDone is true on death.\n
*/
void Partridge_Clutch::Step( void )
{
  if ( m_CurrentStateNo == -1 || m_StepDone )
    return;

#ifdef __PAR_DEBUG
  CheckMatePointers();
#endif

  switch ( m_state )
  {
    case pars_Initiation:
      m_state = pars_ClDeveloping;
    case pars_ClDeveloping:
    case pars_ClHatching:
    case pars_Destroy:
    case pars_ClDying:
      m_StepDone = true;
    break;
    default:
      m_OurLandscape->Warn( "Partridge_Clutch::Step - unknown pars type", NULL );
      exit( 1 );
  }
}

//------------------------------------------------------------------------------

/**
Does the development for the clutch here.\n
*/
void Partridge_Clutch::EndStep( void )
{
  if ( m_CurrentStateNo == -1 )
    return;

  switch ( m_state )
  {
    case pars_ClDeveloping:
      m_state = ClDeveloping();
    break;
    case pars_ClHatching:
    break;
    case pars_ClDying:
    break;
    case pars_Destroy:
      // Destroy is only called if all housekeeping wrt to removal of the object
      // is done - object will be destroyed by PopulationManager
    break;
    default:
      m_OurLandscape->Warn( "Partridge_Clutch::EndStep - unknown pars type", NULL );
      exit( 1 );
  }
}

//------------------------------------------------------------------------------

/** No functionality any longer because this is done in the female incubtation due to the need to have density-dependence calculated first. */
bool Partridge_Clutch::DailyMortality( void )
{
  return false;
}

//------------------------------------------------------------------------------
/**
  This controls behaviour on the death of a clutch due to management.
  If the clutch is hit there is a very good chance that the hen dies too, so need to tell her.\n
*/
void Partridge_Clutch::AgDying()
{

  m_OurPopulationManager->m_Ourkfactors->m_ThisYear->incNoAgDeadClutches();
  m_OurPopulationManager->m_Ourkfactors->m_ThisYear->incNoClutchesPredated(); // All becomes part of k1
  // The line below is really debug information, this identifies the caller
  m_OurPopulationManager->m_comms_data->m_clutch = this;
  m_OurPopulationManager->m_comms_data->m_female = m_mother;
  m_OurPopulationManager->m_messagecentre.PassMessage( m_OurPopulationManager->m_comms_data, pcomm_ClutchMown );
  ClDying();
}
//------------------------------------------------------------------------------

/**
Chooses the response to external management events. Responses can either be set via config variables or directly here.\n
*/
bool Partridge_Clutch::OnFarmEvent( FarmToDo event )
{
  switch ( event )
  {
    case sleep_all_day:
    break;
    case autumn_plough:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case autumn_harrow:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case autumn_roll:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case autumn_sow:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case winter_plough:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case deep_ploughing:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case spring_plough:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case spring_harrow:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case spring_roll:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case spring_sow:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case fp_npks:
    break;
    case fp_npk:
    break;
    case fp_pk:
    break;
    case fp_liquidNH3:
      if ( g_rand_uni() < 80 ) AgDying();
    break;
    case fp_slurry:
      if ( g_rand_uni() < 50 ) AgDying();
    break;
    case fp_manganesesulphate:
    break;
    case fp_manure:
      if ( g_rand_uni() < 0.20 ) AgDying();
    break;
    case fp_greenmanure:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case fp_sludge:
    break;
    case fa_npk:
    break;
    case fa_pk:
    break;
    case fa_slurry:
      if ( g_rand_uni() < 0.50 ) AgDying();
    break;
    case fa_ammoniumsulphate:
    break;
    case fa_manure:
      if ( g_rand_uni() < 0.20 ) AgDying();
    break;
    case fa_greenmanure:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case fa_sludge:
    break;
    case herbicide_treat:
    break;
    case growth_regulator:
    break;
    case fungicide_treat:
    break;
    case syninsecticide_treat:
    case insecticide_treat:
	case product_treat:
	case glyphosate:
    break;
    case molluscicide:
    break;
    case row_cultivation:
      if ( g_rand_uni() < 0.50 ) AgDying();
    break;
    case strigling:
    break;
    case hilling_up:
      if ( g_rand_uni() < 0.50 ) AgDying();
    break;
    case water:
      if ( g_rand_uni() < 0.01 ) AgDying();
    break;
    case swathing:
    break;
    case harvest:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case cattle_out:
    break;
    case cattle_out_low:
    break;
    case cut_to_hay:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case cut_to_silage:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case straw_chopping:
    break;
    case hay_turning:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case hay_bailing:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;

    case stubble_harrowing:
    break;
    case autumn_or_spring_plough:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case burn_straw_stubble:
    break;
    case mow:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case cut_weeds:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case pigs_out:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    case strigling_sow:
      if ( g_rand_uni() < cfg_par_cl_cut.value() ) AgDying();
    break;
    default:
      m_OurLandscape->Warn( "Skylark_Clutch::OnFarmEvent(): Unknown event type:", m_OurLandscape->EventtypeToString( event ) );
      exit( 1 );
  }
  if ( m_state == pars_Destroy ) return true;
  else
    return false;
}

//------------------------------------------------------------------------------

/**
This method controls the development of the clutch and its hatching. Hatching occurs when the clutch has been incubated for a predefined period.\n
Timing: Occurs at the end of a day (e.g. midnight-1 minute). Must create Clutch_Size number of Partridge_Chick Objects.
They tell their mother that they have been created when they exist in the system.\n
*/
Partridge_State Partridge_Clutch::ClDeveloping( void )
{
	if (!m_underincubation) return pars_ClDeveloping;
	if ( ++m_age < cfg_par_incubation_period.value() ) {
		return pars_ClDeveloping;
	}
	//The line below is really debug information, this identifies the caller
	m_OurPopulationManager->m_comms_data->m_clutch = this;
	//  Call the mother via the message centre
	m_OurPopulationManager->m_comms_data->m_female = m_mother;
	m_OurPopulationManager->m_comms_data->m_male = m_mother->GetMate();
	// Remove the infertile eggs
	int clutchsize = 0;
    // correct for infertile eggs:
    for ( int u = 0; u < m_clutch_size; u++ ) {
		if ( g_rand_uni() >= cfg_par_infertile_eggs.value() ) clutchsize++;
    }
    if ( clutchsize == 0 ) // all infertile
    {
		m_OurPopulationManager->m_messagecentre.PassMessage( m_OurPopulationManager->m_comms_data, pcomm_AllInfertile );
		m_StepDone = true; // Note this does more than stop the Step code executing
		m_covey->NestLeave(); // indicate that we are finished with the nest
		m_covey->RemoveMember( this );
		m_mother = NULL; // just in case they get some other weird message
		m_CurrentStateNo = -1;
		return pars_Destroy;
	}
	//  Call the mother via the message centre
	m_OurPopulationManager->m_messagecentre.PassMessage( m_OurPopulationManager->m_comms_data, pcomm_EggsHatch );
	// Do what is needed to create the new chick objects
	//  m_OurPopulationManager->AddMaturedE();
	Chick_struct * Ch;
	Ch = new Chick_struct;
	Ch->x = m_Location_x;
	Ch->y = m_Location_y;
	Ch->bx = m_Location_x;
	Ch->by = m_Location_y;
	Ch->Mum = m_mother;
	Ch->m_covey = m_covey;
	Ch->family_counter = m_family_counter;
	m_OurPopulationManager->CreateObjects( pob_Chick, Ch, clutchsize );
	delete Ch;
	m_CurrentStateNo = -1;
	m_OurPopulationManager->AddHatchSuccess( clutchsize );
	m_StepDone = true; // Note this does more than stop the Step code executing
	m_covey->NestLeave(); // indicate that we are finished with the nest
	m_covey->RemoveMember( this );
	m_mother = NULL; // just in case they get some other weird message and try
	// to do something with the mother pointer
	//m_covey->SanityCheck2(m_covey->GetOurChicks() );
	return pars_Destroy;
}

//------------------------------------------------------------------------------

/**
Removes the clutch from the system after doing any necessary house-keeping.\n
*/
void Partridge_Clutch::ClDying( void )
{
  // Before this method is called all necessary communications need to be carried
  // out
  // All the housekeeping necessary to die
  if ( m_CurrentStateNo == -1 ) return; // already killed by something else
  m_CurrentStateNo = -1;
  m_StepDone = true;
  // Do any statistics recording
  m_OurPopulationManager->m_Ourkfactors->m_ThisYear->incNoDeadClutches();
  // Now tidy up before vanishing totally
  m_mother = NULL;
  m_covey->RemoveMember( this );
  m_state = pars_Destroy;
}

//------------------------------------------------------------------------------

/**
Response to a pcomm_MumDeadC message.\n
*/
void Partridge_Clutch::OnMumDead( void )
{
  if ( m_mother ) // if this is NULL the clutch is already dying
  {
    m_mother = NULL;
  }
  ClDying();
}

//------------------------------------------------------------------------------

/**
Called as a direct call from Partridge_Female::OnMateDying \n
*/
void Partridge_Clutch::OnGivenUp( void )
{
  if ( m_mother ) // if this is NULL the clutch is alread dying
  {
    m_mother = NULL;
    ClDying();
  }
}

//------------------------------------------------------------------------------

/**
Response to a pcomm_ClutchEaten
*/
void Partridge_Clutch::OnEaten( void )
{
  if ( m_mother ) // if this is NULL the clutch is alread dying
  {
    ClDying();
  }
}

//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//                            Partridge_Chick
//------------------------------------------------------------------------------

/**
The constructor. Ensures sensible values for attributes not set by the base constructor.
*/
Partridge_Chick::Partridge_Chick( int a_born_x, int a_born_y, Partridge_Female * a_mother, Partridge_Covey * a_covey,
     Landscape * a_map, bool a_sex, int a_family_counter, Partridge_Population_Manager * a_manager ) :
     Partridge_Base( a_born_x, a_born_y, a_born_x, a_born_y, a_family_counter, a_covey, a_map, a_manager )
     {
       m_age = 0;
       m_starve = 0;
       m_sex = a_sex;
       m_object_type = pob_Chick;
       m_covey->OnAddChick( a_mother );

}

//------------------------------------------------------------------------------

/**
BeginStep checks for daily mortality and management events, also deals with knock-on deaths for other chicks in the covey.\n
*/
void Partridge_Chick::BeginStep( void )
{
  if ( m_CurrentStateNo == -1 )
  {
    return;
  }
  if ( !DailyMortality() )
  {
    CheckManagement();
    //m_covey->SanityCheck2(m_covey->GetOurChicks() );
    m_covey->SetChickAge( m_age );
    // This call is because it is OK to get multiple calls to this function
    // but it is not OK if no-one calls it!
    m_covey->MoveDistance( g_MaxWalk[m_age], 10, 10000 );
  }
  else
  {
    m_signal = 1;
    ChDying();
    // The death of this chick will impact on the others, so that their deaths are more likely
    m_covey->ChickExtraMortality();
  }
}

//------------------------------------------------------------------------------

/**
Step only takes care of maturing for this class.\n
*/
void Partridge_Chick::Step( void )
{
  if ( m_CurrentStateNo == -1 || m_StepDone )
    return;

  switch ( m_state )
  {
    case pars_Initiation:
      m_state = pars_ChDeveloping;
    case pars_ChDeveloping:
    case pars_ChDying:
      m_StepDone = true;
    break;
    case pars_ChMaturing:
      ChMaturing();
      m_StepDone = true;
    break;

    default:
      m_OurLandscape->Warn( "Partridge_Chick::Step - unknown pars type", NULL );
      exit( 1 );
  }
}

//------------------------------------------------------------------------------

/**
EndStep does the developing for the Chick. This ensures that someone (adult) has called the methods necessary to collect food for developing that day during Step.\n
*/
void Partridge_Chick::EndStep( void )
{
  if ( m_CurrentStateNo == -1 )
    return;

  switch ( m_state )
  {
    case pars_Initiation:
    case pars_ChMaturing:
    break;
    case pars_ChDying:
      ChDying();
    break;
    case pars_ChDeveloping:
      m_state = ChDeveloping();
    break;
    default:
      m_OurLandscape->Warn( "Partridge_Chick::EndStep - unknown pars type", NULL );
      exit( 1 );
  }
}

//------------------------------------------------------------------------------

/**
Check and respond to farm events either using config variables for cutting or direct inline values.\n
*/
bool Partridge_Chick::OnFarmEvent( FarmToDo event )
{
  switch ( event )
  {
    case sleep_all_day:
    break;
    case autumn_plough:
    break;
    case autumn_harrow:
    break;
    case autumn_roll:
    break;
    case autumn_sow:
    break;
    case winter_plough:
    break;
    case deep_ploughing:
    break;
    case spring_plough:
    break;
    case spring_harrow:
    break;
    case spring_roll:
    break;
    case spring_sow:
    break;
    case fp_npks:
    break;
    case fp_npk:
    break;
    case fp_pk:
    break;
    case fp_liquidNH3:
    break;
    case fp_slurry:
      if ( g_rand_uni() < 0.01 ) ChDying();
    break;


    case fp_manganesesulphate:
    break;
    case fp_manure:
    break;
    case fp_greenmanure:
    break;
    case fp_sludge:
    break;
    case fa_npk:
    break;
    case fa_pk:
    break;
    case fa_slurry:
      if ( g_rand_uni() < 0.01 ) ChDying();
    break;
    case fa_ammoniumsulphate:
    break;
    case fa_manure:
    break;
    case fa_greenmanure:
    break;
    case fa_sludge:
    break;
    case herbicide_treat:
    case glyphosate:
	break;
    case growth_regulator:
    break;
    case fungicide_treat:
    break;
    case syninsecticide_treat:
    case insecticide_treat:
	case product_treat:
    break;
    case molluscicide:
    break;
    case row_cultivation:
    break;
    case strigling:
      if ( g_rand_uni() < 0.10 ) ChDying();
    break;
    case hilling_up:
    break;
    case water:
      if ( g_rand_uni() < 0.01 ) ChDying();
    break;
    case swathing:
    break;
    case harvest:
      if ( g_rand_uni() < cfg_par_ch_cut.value() ) ChDying();
    break;
    case cattle_out:
    break;
    case cattle_out_low:
    break;
    case cut_to_hay:
      if ( g_rand_uni() < cfg_par_ch_cut.value() ) ChDying();
    break;
    case cut_to_silage:
      if ( g_rand_uni() < cfg_par_ch_cut.value() ) ChDying();
    break;
    case straw_chopping:
    break;
    case hay_turning:
    break;
    case hay_bailing:
      if ( g_rand_uni() < cfg_par_ch_cut.value() ) ChDying();
    break;
    case stubble_harrowing:
    break;
    case autumn_or_spring_plough:
    break;
    case burn_straw_stubble:
    break;
    case mow:
      if ( g_rand_uni() < cfg_par_ch_cut.value() ) ChDying();
    break;
    case cut_weeds:
      if ( g_rand_uni() < cfg_par_ch_cut.value() ) ChDying();
    break;
    case pigs_out:
    break;
    case strigling_sow:
      if ( g_rand_uni() < 10 ) ChDying();
    break;
    default:
      m_OurLandscape->Warn( "Skylark_Clutch::OnFarmEvent(): Unknown event type:", m_OurLandscape->EventtypeToString( event ) );
      exit( 1 );
  }
  // Must incorporate a test here in case the animal is dead - killing it twice
  // can be a bad idea
  if ( m_state == pars_Destroy ) return true;
  else
    return false;
}

//------------------------------------------------------------------------------

/**
Does the simple probability test against chance of daily mortality for non-specifically modelled mortalities.\n
*/
bool Partridge_Chick::DailyMortality( void )
{
	if ( g_rand_uni() < cfg_par_chick_mortality.value() )
	{
		// Optional output
		//    m_OurPopulationManager->AddEaten();
		return true;
	}
	return false;
}

//------------------------------------------------------------------------------

/**
Chick development is simplified to developing as long as the chick is not starving for too long. This is determined by the age dependent energy needs and rainfall. This could also be augmented to cinclude temperature at a later stage.\n
At 42 days old the Chick becomes a Partridge_Chick2.\n
*/
Partridge_State Partridge_Chick::ChDeveloping( void )
{
  m_age++;
  // have we eaten enough to survive?
  double food = m_covey->SupplyFoodToday();
  double ex = cfg_par_extraction_rate.value();
  food *= ex;
#ifdef __RAIN_HURTS
  if ( food > ( g_FoodNeed[m_age] * (1+( g_par_rainfactor * m_OurLandscape->SupplyRain()) ) ))
  {
    m_starve = 0;
  }
#else
  if ( food > g_FoodNeed[m_age]  )
  {
    m_starve = 0;
  }
#endif
  else if ( ++m_starve > cfg_par_starve_threshold.value() )
  {
    m_signal = 4;
    ChDying(); // Kill it now
	//m_OurPopulationManager->WriteParJuvMort(m_OurLandscape->SupplyYearNumber(),m_OurLandscape->SupplyDayInYear(),m_age,m_signal);
	m_OurPopulationManager->m_Ourkfactors->m_ThisYear->incNoStarvedChicks();
    return pars_ChDying;
  }
  if ( m_age < 42 )
  {
    return pars_ChDeveloping;
  }
  else
    return pars_ChMaturing;
}

//------------------------------------------------------------------------------

/**
Matures the Chick object into a Partridge_Chick2 object, passing all the necessary informatin using a Chick_struct.\n
*/
Partridge_State Partridge_Chick::ChMaturing( void )
{
  //m_covey->SanityCheck2(m_covey->GetOurChicks() );
  Chick_struct * Ch;
  Ch = new Chick_struct;
  Ch->x = m_Location_x;
  Ch->y = m_Location_y;
  Ch->bx = m_Location_x;
  Ch->by = m_Location_y;
  Ch->Mum = NULL;
  Ch->m_covey = m_covey;
  Ch->sex = m_sex;
  Ch->family_counter = m_family_counter;
  // Create objects will result in the object being added to the covey
  m_OurPopulationManager->CreateObjects( pob_Chick2, Ch, 1 );
  // object will be destroyed by death state
  // but must let Dad know anyway
  delete Ch;
  // Set up the flags to destroy this object
  m_CurrentStateNo = -1;
  m_StepDone = true;
  m_covey->RemoveMember( this );
  //m_covey->SanityCheck2(m_covey->GetOurChicks() );
  return pars_Destroy;
}

//------------------------------------------------------------------------------

void Partridge_Chick::ChDying( void )
{
  /** All the housekeeping necessary to die.\n*/
  m_covey->OnChickDeath();
  m_covey->RemoveMember( this );
  m_state = pars_Destroy;
  m_CurrentStateNo = -1;
  m_StepDone = true;
  // Keep the k_factor analysis happy and uptodate
  if ( m_age > 42 )
  {
    if ( m_sex == false )
    {
      m_OurPopulationManager->m_Ourkfactors->m_ThisYear->incNoDeadFemales();
    }
    else
      m_OurPopulationManager->m_Ourkfactors->m_ThisYear->incNoDeadMales();
  }
  m_OurPopulationManager->m_Ourkfactors->m_ThisYear->incNoDeadChicks();

}

//------------------------------------------------------------------------------

/**
Called by any method that directly kills a chick. Will cause housekeeping to be done by informing those objects that need to know.\n
*/
void Partridge_Chick::OnYouAreDead()
{
  ChDying();
}

//------------------------------------------------------------------------------
//                            Partridge_Chick2
//------------------------------------------------------------------------------

/**
Constructor for the Chick2. Uses the Partridge_Chick::Partridge_Chick constructor and then corrects details.\n
*/
Partridge_Chick2::Partridge_Chick2( int a_born_x, int a_born_y, Partridge_Female * a_mother, Partridge_Covey * a_covey,
     Landscape * a_map, bool a_sex, int a_family_counter, Partridge_Population_Manager * a_manager ) :
     Partridge_Chick( a_born_x, a_born_y, a_mother, a_covey, a_map, a_sex, a_family_counter, a_manager )
     {
       // Need to correct two naughtly little details that were messed up by the chick constructor
       // First we are not a chick any longer
       m_object_type = pob_Chick2;
       // Second The covey now thinks it has an extra chick
       m_covey->RemoveExtraChick();
}

//------------------------------------------------------------------------------

/**
Check and respond to farm events either using config variables for cutting or direct inline values.\n
*/
bool Partridge_Chick2::OnFarmEvent( FarmToDo event )
{
  switch ( event )
  {
    case sleep_all_day:
    break;
    case autumn_plough:
    break;
    case autumn_harrow:
    break;
    case autumn_roll:
    break;
    case autumn_sow:
    break;
    case winter_plough:
    break;
    case deep_ploughing:
    break;
    case spring_plough:
    break;
    case spring_harrow:
    break;
    case spring_roll:
    break;
    case spring_sow:
    break;
    case fp_npks:
    break;
    case fp_npk:
    break;
    case fp_pk:
    break;
    case fp_liquidNH3:
    break;
    case fp_slurry:
    break;
    case fp_manganesesulphate:
    break;
    case fp_manure:
    break;
    case fp_greenmanure:
    break;
    case fp_sludge:
    break;
    case fa_npk:
    break;
    case fa_pk:
    break;
    case fa_slurry:
    break;
    case fa_ammoniumsulphate:
    break;
    case fa_manure:
    break;
    case fa_greenmanure:
    break;
    case fa_sludge:
    break;
	case glyphosate:
    case herbicide_treat:
    break;
    case growth_regulator:
    break;
    case fungicide_treat:
    break;
    case syninsecticide_treat:
    case insecticide_treat:
	case product_treat:
    break;
    case molluscicide:
    break;
    case row_cultivation:
    break;
    case strigling:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) Dying();
    break;
    case hilling_up:
    break;
    case water:
    break;
    case swathing:
    break;
    case harvest:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) Dying();
    break;
    case cattle_out:
    break;
    case cattle_out_low:
    break;
    case cut_to_hay:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) Dying();
    break;
    case cut_to_silage:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) Dying();
    break;
    case straw_chopping:
    break;
    case hay_turning:
    break;
    case hay_bailing:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) Dying();
    break;
    case stubble_harrowing:
    break;
    case autumn_or_spring_plough:
    break;
    case burn_straw_stubble:
    break;
    case mow:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) Dying();
    break;
    case cut_weeds:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) Dying();
    break;
    case pigs_out:
    break;
    case strigling_sow:
      if ( g_rand_uni() < 0.01 ) Dying();
    break;
    default:
      m_OurLandscape->Warn( "Skylark_Clutch::OnFarmEvent(): Unknown event type:", m_OurLandscape->EventtypeToString( event ) );
      exit( 1 );
  }
  // Must incorporate a test here in case the animal is dead - killing it twice
  // can be a bad idea
  if ( m_state == pars_Destroy ) return true;
  else
    return false;
}

//------------------------------------------------------------------------------

/**
Does the simple probability test against chance of daily mortality for non-specifically modelled mortalities.\n
*/
bool Partridge_Chick2::DailyMortality( void )
{
	double extramort=cfg_par_chick2_mortality.value();
	int today = m_OurLandscape->SupplyDayInYear();
	// Is it winter?
	if (( ( today >= October ) || ( today < April )) ) {
		if (m_OurPopulationManager->SupplyShouldFlock())  {	// We are in the "Winter Period"
			if ( (g_rand_uni()) < (cfg_par_adult_mortalityWinter.value()*extramort) ) {
				return true;
			}
			} else {
				// Must test summer mort instead;
			if ( g_rand_uni() < (cfg_par_adult_mortalitySummer.value()*extramort )) {
				return true;
			}
		}
	}
  return false;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

/**
Developing is basically being alive each day until becoming an adult object.\n
*/
Partridge_State Partridge_Chick2::ChDeveloping( void )
{
  m_age++;
  if ( m_age < cfg_par_mature_threshold.value() )
  {
    return pars_ChDeveloping;
  }
  else
    return pars_ChMaturing;
}

//------------------------------------------------------------------------------

/**
Matures the Chick2 object into an Partridge_Male or Partridge_Female. Passes all useful information using an AdultPartridge_struct.\n
*/
Partridge_State Partridge_Chick2::ChMaturing( void )
{
  AdultPartridge_struct * aps;
  aps = new AdultPartridge_struct;
  aps->bx = m_born_x;
  aps->by = m_born_y;
  aps->x = m_Location_x;
  aps->y = m_Location_y;
  aps->family_counter = m_family_counter;
  aps->m_covey = m_covey;
  if ( m_sex == true ) m_OurPopulationManager->CreateObjects( pob_Male, aps, 1 );
  else
    m_OurPopulationManager->CreateObjects( pob_Female, aps, 1 );
  // object will be destroyed by death state
  // but must let Dad know anyway
  delete aps;
  // Set up the flags to destroy this object
  m_CurrentStateNo = -1;
  m_StepDone = true;
  m_covey->RemoveMember( this );
  m_covey->OnChickMature();
  //  m_OurPopulationManager->AddMatured();
  return pars_Destroy;
}

//------------------------------------------------------------------------------

/**
BefinStep just checks background mortality and management events.\n
*/
void Partridge_Chick2::BeginStep( void )
{
  if ( !DailyMortality() )
  {
    CheckManagement();
    m_covey->SetChickAge( m_age );
    // This call is because it is OK to get multiple calls to this function
    // but it is not OK if no-one calls it!
    m_covey->MoveDistance( g_MaxWalk[m_age], 10, 10000 );
  }
  else
  {
    m_signal = 1;
    ChDying();
  }
}

//------------------------------------------------------------------------------

/**
Step only deals with maturing.\n
*/
void Partridge_Chick2::Step( void )
{
  if ( m_CurrentStateNo == -1 || m_StepDone )
    return;

  switch ( m_state )
  {
    case pars_Initiation:
      m_state = pars_ChDeveloping;
    case pars_ChDeveloping:
    case pars_ChDying:
      m_StepDone = true;
    break;
    case pars_ChMaturing:
      ChMaturing();
      m_StepDone = true;
    break;

    default:
      m_OurLandscape->Warn( "Partridge_Chick2::Step - unknown pars type", NULL );
      exit( 1 );
  }
}

//------------------------------------------------------------------------------

/**
EndStep does development and dying.\n
*/
void Partridge_Chick2::EndStep( void )
{
  if ( m_CurrentStateNo == -1 )
    return;

  switch ( m_state )
  {
    case pars_Initiation:
    case pars_ChMaturing:
    break;
    case pars_ChDying:
      ChDying();
    break;
    case pars_ChDeveloping:
      m_state = ChDeveloping();
    break;
    default:
      m_OurLandscape->Warn( "Partridge_Chick::EndStep - unknown pars type", NULL );
      exit( 1 );
  }
}

//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//                            Partridge_Female
//------------------------------------------------------------------------------

/**
Constructor - ensures that attributes are set to sensible starting values, otherwise uses the Partridge_Base constructor.\n
*/
Partridge_Female::Partridge_Female( int a_born_x, int a_born_y, int a_x, int a_y, Partridge_Covey * a_covey, Landscape * a_map,
     int a_family_counter, Partridge_Population_Manager * a_manager ) :
     Partridge_Base( a_born_x, a_born_y, a_x, a_y, a_family_counter, a_covey, a_map, a_manager )
     {
       m_MyMate = NULL;
       m_HaveTerritory = false;
       m_object_type = pob_Female;
	   m_MyOldHusband=NULL;
       m_MyClutch = NULL;
       m_clutch_size = 0;
       m_lifespan = random( cfg_par_max_lifespan.value() - cfg_par_min_lifespan.value() ) + cfg_par_min_lifespan.value();
	   m_triggerlayingvar=random(cfg_par_triggerlayingvar.value());
}

//------------------------------------------------------------------------------

/**
The BeginStep code for the female partridge. Only function is the test for mortality of different kinds.
Might as well remove her here than waste CPU time thinking about the Step code later this time-step.\n
*/
void Partridge_Female::BeginStep( void )
{
  m_age++; // Get a day older
  if ( m_age >= m_lifespan )
  {
    m_signal = 0; // Too old
    FDying();
  }
  else if ( !DailyMortality() ) CheckManagement();
  else
  {
    m_signal = 1; // Killed
  }
#ifdef __PAR_DEBUG
  CheckMatePointers();
#endif
}

//------------------------------------------------------------------------------

/**
The Step code for the female partridge. This is the main state-transition control for the female.
It will be called repeatedly each time-step until m_StepDone is set to true.\n
*/
void Partridge_Female::Step( void )
{

  if ( m_CurrentStateNo == -1 || m_StepDone )
    return;
  switch ( m_state )
  {
    case pars_Initiation:
    case pars_FFlocking:
      m_state = FFlocking();
      m_StepDone = true;
    break;
    case pars_FFindingTerritory:
      m_state = FFindingTerritory();
      m_StepDone = true;
    break;
    case pars_FAttractingMate:
      m_state = FAttractingMate();
      m_StepDone = true;
    break;
    case pars_FBuildingUpResources:
      m_state = FBuildingUpResources();
      m_StepDone = true;
    break;
    case pars_FMakingNest:
      m_state = FMakingNest();
      m_StepDone = true;
    break;
    case pars_FLaying:
      m_state = FLaying();
      m_StepDone = true;
    break;
    case pars_FStartingNewBrood:
      m_state = FStartingNewBrood( false );
      m_StepDone = true;
    break;
    case pars_FIncubating:
      m_state = FIncubating();
      m_StepDone = true;
    break;
    case pars_FCaringForYoung:
      m_state = FCaringForYoung();
      m_StepDone = true;
    break;
    case pars_FDying:
      m_StepDone = true;
    break;
    default:
      char st[255];
	  sprintf(st,"%d\n",m_state);
      m_OurLandscape->Warn( "Partridge_Female::Step - unknown pars type ", st );
      exit( 1 );
  }
#ifdef __PAR_DEBUG
  CheckMatePointers();
#endif
}

//------------------------------------------------------------------------------

/**
Nothing to do.\n
*/
void Partridge_Female::EndStep( void )
{
#ifdef __PAR_DEBUG
#endif
}
//------------------------------------------------------------------------------

/**
This method is responsible for applying a general mortality, ie not explicitly modelled. There are three periods considered, a spring mortality applied
during 'territory' finding (see Partridge_Female::FFindingTerritory), a winter mortality applied daily between October and April, and the rest of the year
a summer mortality. In addition there is a scaling factor (extramort) which is applied if the bird is alone. All parameter values are under user control
through configuration variables.\n
*/
bool Partridge_Female::DailyMortality( void )
{
	bool dead = false;
	double chance = g_rand_uni();
	double extramort=1.0;
	if ( m_covey->GetCoveySize() == 1 ) {
		extramort=cfg_par_adult_mortality_alone.value();
	}
    int today = m_OurLandscape->SupplyDayInYear();
	if (m_state == pars_FFindingTerritory)  {
		if ( chance < cfg_par_adult_mortalitySpring.value()*extramort ) dead = true;
	}
	else {
		// Is it winter?
		if ( ( today >= October ) || ( today < April ) ) {
			// We are in the "Winter Period"
			if ( chance < cfg_par_adult_mortalityWinter.value()*extramort ) dead = true;
		}
		else // Not winter and not finding territory
		{
			if ( chance < (cfg_par_adult_mortalitySummer.value()*extramort) ) dead = true;
		}
	}
	if ( dead ) {
		FDying();
		return true;
	}
	return false;
}

//------------------------------------------------------------------------------

/**
There are only a few management events that cause direct effects for the female, but these may be important eg mowing/cutting events.
When she is on the nest mortaltiy is controlled via the Partridge_Clutch object not this code, since she is assumed not to escape so easily
(see Partridge_Clutch::AgDying).\n
*/
bool Partridge_Female::OnFarmEvent( FarmToDo event )
{
  switch ( event )
  {
    case sleep_all_day:
    break;
    case autumn_plough:
    break;
    case autumn_harrow:
    break;
    case autumn_roll:
    break;
    case autumn_sow:
    break;
    case winter_plough:
    break;
    case deep_ploughing:
    break;
    case spring_plough:
    break;
    case spring_harrow:
    break;
    case spring_roll:
    break;

    case spring_sow:
    break;
    case fp_npks:
    break;
    case fp_npk:
    break;
    case fp_pk:
    break;
    case fp_liquidNH3:
    break;
    case fp_slurry:
    break;
    case fp_manganesesulphate:
    break;
    case fp_manure:
    break;
    case fp_greenmanure:
    break;
    case fp_sludge:
    break;
    case fa_npk:
    break;
    case fa_pk:
    break;
    case fa_slurry:
    break;
    case fa_ammoniumsulphate:
    break;
    case fa_manure:
    break;
    case fa_greenmanure:
    break;
    case fa_sludge:
    break;
	case glyphosate:
    case herbicide_treat:
    break;
    case growth_regulator:
    break;
    case fungicide_treat:
    break;
    case syninsecticide_treat:
    case insecticide_treat:
	case product_treat:
    break;
    case molluscicide:
    break;
    case row_cultivation:
    break;
    case strigling:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) FDying();
    break;
    case hilling_up:
    break;
    case water:
    break;
    case swathing:
    break;
    case harvest:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) FDying();
    break;
    case cattle_out:
    break;
    case cattle_out_low:
    break;
    case cut_to_hay:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) FDying();
    break;
    case cut_to_silage:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) FDying();
    break;
    case straw_chopping:
    break;
    case hay_turning:
    break;
    case hay_bailing:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) FDying();
    break;
    case stubble_harrowing:
    break;
    case autumn_or_spring_plough:
    break;
    case burn_straw_stubble:
    break;
    case mow:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) FDying();
    break;
    case cut_weeds:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) FDying();
    break;
    case pigs_out:
    break;
    case strigling_sow:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) FDying();
    break;
    default:
      m_OurLandscape->Warn( "Partridge_Female::OnFarmEvent(): Unknown event type:",
           m_OurLandscape->EventtypeToString( event ) );
      exit( 1 );
  }
  // Must incorporate a test here in case the animal is dead - killing it twice
  // can be a bad idea
  if ( m_state == pars_Destroy ) return true;
  else
    return false;
}



//------------------------------------------------------------------------------

/**
Ensures that the covey is moving around while flocking. NB: It is OK for *all* the members of the covey to call this method every day. It only has any effect
on the first call.\n
The birds wait in this state until they get a message initiating breeding behaviour.
*/
Partridge_State Partridge_Female::FFlocking( void )
{
  m_covey->MoveDistance( m_covey->GetmaxAllowedMove(), 10, m_covey->GetmaxFoodNeedToday() );
  return pars_FFlocking;
}
//------------------------------------------------------------------------------

/**
This is simply a waiting state where we assume the female is building up physiologial resources before breeding. Since we don't know what triggers this we origianlly
used a fixed counter from the point were territory has been fixed. This produced variation in the starting laying date determined by when the pair fixes their
breeding territory for this season. However this did not seem satisfactory so it has been replaced with a measure of daylength with variation.\n
*/
Partridge_State Partridge_Female::FBuildingUpResources( void )
{
#ifdef __PAR_DEBUG
  CheckMatePointers();
  /* if ( m_nest_counter < 1 ) { exit( 0 ); } */
#endif
    // Note that with default settings the m_buildupwait is always gong to be 0 or less on calling this method.
    if (--m_buildupwait < 0) {
		// daylength que from when they start the covey dissolve
		// This could be altered to include bad weather delays
		if ( m_OurLandscape->SupplyDaylength() >= (cfg_par_triggerlaying.value()+m_triggerlayingvar) )
		{
			m_nest_counter = cfg_par_days_to_make_nest.value();
			return pars_FMakingNest;
		}
	}
	return pars_FBuildingUpResources;
}
//------------------------------------------------------------------------------

/**
We start with a test for the end of the breeding season. If it is not too late then we determine the eventual size of the clutch here and create this when the nest is completed. The clutch object is created with zero eggs and the eggs are added in Partridge_Female::FLaying.\n
Determining the number of eggs to be produced has been done by fitting a curve to Danish data from 1953 since we do not know how to do this mechanistically:\n
\image html ParEggsPerClutch.png
Clutch size with fitted curve used to create a probability distribution hard-coded below. This distribution is scaled up and down to obtain the correct mean clutch size recorded when parameter fitting.\n
The male will just carry on guarding so no need to tell him when the nest is complete. Originally needed to do a special calculation here to calculate the km nesting cover/km2 present.
This does not change during the nest's existence so no danger of errors there and this code was slow so an alternative was to calculate all possible values on a coarser grid at start of
simulation, then store these - this is the current implementation.\n
*/
Partridge_State Partridge_Female::FMakingNest( void )
{
  //
      if ( m_OurLandscape->SupplyDayInYear() >= cfg_par_last_brood_date.value() )
      {
        if ( m_MyMate )
        {
          m_OurPopulationManager->m_comms_data->m_female = this;
          m_OurPopulationManager->m_comms_data->m_male = m_MyMate;
          m_OurPopulationManager->m_messagecentre.PassMessage( m_OurPopulationManager->m_comms_data, pcomm_StoppingBreeding );
        }
        // No success so clear the oldmate
        RemoveOldMate(false);
        m_MyMate = NULL;
        m_covey->SetFixed( false );
        m_HaveTerritory = false;
        return pars_FFlocking;
      }
  // data
  const int clutchsize[19] =
  {
	// This array holds the probabilities of clutch sizes in 20 categories.
	// below from Damerham
	  143, 385, 725, 1163, 1700, 2336, 3070, 3902, 4833, 5863, 6990, 8217, 9037, 9607, 9928, 10000, 10000, 10000
	// The data below is from fitting a 3rd order polynomial to the Danish Paludan 1953 for Danish conditions
	// 48, 198, 451, 807, 1266, 1828, 2493, 3261, 4132, 5106, 6182, 7361, 8416, 9208, 9736, 10000, 10000, 10000
  };
  if ( --m_nest_counter == 0 )  {
    if ( m_clutch_size == 0 )
    { // If it is not zero it must be 2nd brood and is already set so don't alter it
      int chance = random( 10000 );
      for ( int i = 0; i < 19; i++ )
      {
        if ( clutchsize[i] > chance )
        {
          m_clutch_size = cfg_par_min_no_eggs.value() + i;
		  break;
		}
      }
    }
#ifdef __PAR_DEBUG
    CheckMatePointers();
    if ( m_OurLandscape->SupplyDayInYear() > 200 )
    {
      int rubbish = 0;
    }
#endif
	// NOW MAKE THE NEST
    // Find and move to a nest location
    bool fnd = m_covey->NestFindLocation();
    if ( !fnd )
	{
      // No nest site - no breeding here
      // so find another territory
      // Tell the male too
      if ( m_MyMate )
      {
        m_OurPopulationManager->m_comms_data->m_female = this;
        m_OurPopulationManager->m_comms_data->m_male = m_MyMate;
        m_OurPopulationManager->m_messagecentre.PassMessage( m_OurPopulationManager->m_comms_data, pcomm_MovingHome );
      }
      return pars_FFindingTerritory;
    }
    unsigned int family = m_family_counter + 1;
    // Make a clutch object
    Clutch_struct * ac;
    ac = new Clutch_struct;
    ac->x = m_Location_x;
    ac->y = m_Location_y;
    ac->bx = m_Location_x;
    ac->by = m_Location_y;
    ac->Mum = this;
    ac->No = 1; // Was clutch size but 24/01/08 the nest is built first then slowly filled. // m_clutch_size;
    ac->family_counter = family;
    ac->m_covey = m_covey;
    m_OurPopulationManager->CreateObjects( pob_Clutch, ac, 1 );
    delete ac;
    if ( m_clutchproduced )
    {
      // This must be a re-lay, so don't count it towards total clutches laid
      m_OurPopulationManager->m_Ourkfactors->m_ThisYear->decNoClutches();
    }
    m_clutchproduced = true;
	m_startlayingday = m_OurLandscape->SupplyDayInYear();
    return pars_FLaying;
  }
  return pars_FMakingNest;
}
//------------------------------------------------------------------------------

/**
Need to add eggs to clutch, here the problem is that eggs are not produced one each day
We have: cfg_par_days_per_egg.value() and m_clutch_size and m_startlayingday so we just add one egg each cfg_par_days_per_egg.value() days until we have the size wee need.\n
If she completes her laying this is counted as a success and the female remembers the male as a good mate and makes a transition to FIncubating.\n
*/
Partridge_State Partridge_Female::FLaying( void )
{
	// 24th Jan 2008 CJT added the code below to allow for clutch predation before clutch completion
	if ( g_rand_uni() < cfg_par_clutch_mortality.value() ) { // only the eggs
		m_OurPopulationManager->m_Ourkfactors->m_ThisYear->incNoClutchesPredated();
		m_OurPopulationManager->m_comms_data->m_female = this;
		m_OurPopulationManager->m_comms_data->m_clutch = m_MyClutch;
		m_OurPopulationManager->m_messagecentre.PassMessage( m_OurPopulationManager->m_comms_data, pcomm_ClutchEaten );
		m_covey->NestLeave(); // Should not strictly be necessary, but safe
		m_state = FStartingNewBrood( true );
		m_MyClutch = NULL;
		return m_state;
	}
	int days = m_OurLandscape->SupplyDayInYear()-m_startlayingday;
	int cs = (int) (days / cfg_par_days_per_egg.value());
	m_MyClutch->SetClutchSize(cs);
	if (cs<m_clutch_size) return pars_FLaying;
	else {
	    // Remember this male as a good mate
		SetOldMate(m_MyMate);
		m_MyMate->SetOldMate(this);
		m_MyClutch->OnStartIncubating();
		m_OurPopulationManager->WriteParClutches( m_OurLandscape->SupplyGlobalDate(), m_clutch_size, m_clutch_number );
	    return pars_FIncubating;
   }
}
//------------------------------------------------------------------------------

/** Need to test whether or not to start again.\n
If it is a brood that is lost then this behaviour is not an option, otherwise we will end here on the loss of a clutch or nest.
The data in the lines below needs to be checked if it describes the distribution of 2nd brood sizes. Currently it is the same as 1st clutch minus 4.\n
See also Partridge_Female::MakingNest
\n
*/
Partridge_State Partridge_Female::FStartingNewBrood( bool a_waslaying )
{
	m_clutch_number++;
	const int clutchsize[19] =
	{
	// This array holds the probabilities of clutch sizes in 20 categories.
	// below from Damerham
	  143, 385, 725, 1163, 1700, 2336, 3070, 3902, 4833, 5863, 6990, 8217, 9037, 9607, 9928, 10000, 10000, 10000
	// The data below is from fitting a 3rd order polynomial to the Danish Paludan 1953 for Danish conditions
	// 48, 198, 451, 807, 1266, 1828, 2493, 3261, 4132, 5106, 6182, 7361, 8416, 9208, 9736, 10000, 10000, 10000
	};
	int today = m_OurLandscape->SupplyDayInYear();
	if ( (cfg_par_last_brood_date.value() >= today ) && (m_clutch_number<2)) {
		int chance = random( 10000 );
		for ( int i = 0; i < 19; i++ ) {
			if ( clutchsize[i] > chance ) {
			m_clutch_size = cfg_par_min_no_eggs.value() + i - 4; // Subtract 4 to match with UK data for second clutches
			break;
		}
    }
	if (!a_waslaying) {
		m_nest_counter = 10; // 10 days refraction period (Aebishcher & Potts - loose idea but implemented for want of hard data )
	} else m_nest_counter = 1;
	return pars_FMakingNest; // Inlcudes finding nest location
  }
  else  {
	  // Too late or too many attempts
#ifdef __PAR_DEBUG
    CheckMatePointers();
    if ( m_OurLandscape->SupplyDayInYear() < 30 )
    {
      m_OurLandscape->Warn( "Partridge_Female::FSTartingNewBrood", "Too early" );
      exit( 1 );
    }
#endif
    // tell the male we are giving up
    if ( m_MyMate != NULL )
    {
      m_OurPopulationManager->m_comms_data->m_female = this;
      m_OurPopulationManager->m_comms_data->m_male = m_MyMate;
      m_OurPopulationManager->m_messagecentre.PassMessage( m_OurPopulationManager->m_comms_data, pcomm_StoppingBreeding );
    }
    m_HaveTerritory = false;
    m_MyMate = NULL;
    m_covey->SetFixed( false );
    return pars_FFlocking;
  }
}
//------------------------------------------------------------------------------

/**
Arrives here from Laying, and stays until either the clutch tells her its hatched, or the clutch is dead - so just wait unless eaten via density-dependent
    mortality depending upon the number of nests nearby. NB this is a local density-dependence.\n
*/
Partridge_State Partridge_Female::FIncubating( void )
{
  if ( g_rand_uni() < cfg_par_clutch_mortality.value() )
  { // only the eggs
    m_OurPopulationManager->m_Ourkfactors->m_ThisYear->incNoClutchesPredated();
    m_OurPopulationManager->m_comms_data->m_female = this;
    m_OurPopulationManager->m_comms_data->m_clutch = m_MyClutch;
    m_OurPopulationManager->m_messagecentre.PassMessage( m_OurPopulationManager->m_comms_data, pcomm_ClutchEaten );
    m_covey->NestLeave();
    m_state = FStartingNewBrood( false );
    m_MyClutch = NULL;
    return m_state;
  }
  // Only need to calculate the heavy bit if the nest survives egg predation
  double dens = 0.000001 + m_OurPopulationManager->ClutchDensity( m_covey->X(), m_covey->Y(), 500 );
  // Have use DPotts idea as the basis but modified this to account for the local rather than global density.
  double todaysmortchance = cfg_par_female_incubation_mortality_const.value()
       - ( pow( (double) dens, (double) cfg_par_female_incubation_mortality_slope.value() ) );
  todaysmortchance *= 100000;
  if ( random( 100000 ) < todaysmortchance )
  {
    // both eaten
    FDying();
    // Register the death with k_factors
    m_OurPopulationManager->m_Ourkfactors->m_ThisYear->incNoHensPredated();
    m_OurPopulationManager->m_Ourkfactors->m_ThisYear->incNoClutchesPredated();
    m_covey->NestLeave();
    return pars_FDying; // This does nothing actually
  }
  return pars_FIncubating;
}



//------------------------------------------------------------------------------

/**
Will stay in this state until her young counter is zero - this is caused
by messages from the young, so nothing to do but wait. Distance move is calculated as a function of chick food needs, so uses chick age.\n
*/
Partridge_State Partridge_Female::FCaringForYoung( void )
{
  int ch_age = m_covey->GetChickAge();
  m_covey->MoveDistance( g_MaxWalk[ch_age], 10, 10000 );
  return pars_FCaringForYoung;
}
//------------------------------------------------------------------------------

/**
Territory here is used to mean a place where breeding is possible and not a territory in the normal definition of a defended area. \n
\n
First we have to be sure we are not too late in the year, if so start flocking.\n
Next we make a movement using Partridge_Covey::FlyTo and determine the quality of the territory compared to a minimum acceptable quality.\n
Next we check whether this area is already occupied by a covey. If this is OK then we settle here. If we fail at any of these steps then we have 49 more tries
in this time-step before giving up until the next day.\n
*/
Partridge_State Partridge_Female::FFindingTerritory( void )
{
	// First check that we are not too late in the year
	if ( m_OurPopulationManager->SupplyShouldFlock() )	{
		if ( m_MyMate ) {
		  m_OurPopulationManager->m_comms_data->m_female = this;
		  m_OurPopulationManager->m_comms_data->m_male = m_MyMate;
		  m_OurPopulationManager->m_messagecentre.PassMessage( m_OurPopulationManager->m_comms_data, pcomm_FemaleGivingUp );
		  m_MyMate = NULL;
		}
		m_covey->SetFixed( false ); // ensure we do not have a fixed territory loc
		return pars_FFlocking;
	}
	int steps = 0;
	do	{
		if (m_covey->FlyTo( random( cfg_par_female_movement_max.value() ) )) {
			// OK so ask the covey about quality here
			double q = m_covey->AssessHabitatQualityFaster();
			int today = m_OurLandscape->SupplyDayInYear();
			int dayspast = today - cfg_par_start_dissolve.value(); // start day as soon as covey dissolve starts to happen
			double terr_score = m_OurPopulationManager->GetHabitatQuality(dayspast);
			// Test whether the score is above the minimum quality required.
			if ( q > terr_score )
			{
				// Is the covey too close to another, therefore this location is unsuitable?
				if ( !m_covey->TooClose() ) {
					m_HaveTerritory = true;
					m_covey->FixHabitat();
					m_covey->SetFixed( true ); // Can read this with IsFixed()
		#ifdef __PAR_DEBUG
				CheckMatePointers();
		#endif
					m_clutch_number = 0;
					if ( !m_MyMate ) return pars_FAttractingMate;
					else {
						// No need to tell the male, he will check the m_HaveTerritory flag
						m_buildupwait = cfg_buildupwait.value();
						return pars_FBuildingUpResources;
					}
				}
			}
		}
	} while ( ++steps < 50 ); // this many goes per day
	m_HaveTerritory = false;
	// try again tomorrow
	return pars_FFindingTerritory;
}
//------------------------------------------------------------------------------


Partridge_State Partridge_Female::FAttractingMate( void )
{
/**
This just loops because it is the male who controls this. She will stay doing this until either she is pulled out by a male arriving or give up breeding date is reached.\n
During this period her covey peg is locked in position following Partridge_Female::FFindingTerritory, so she cannot be moved by other covies.\n
*/
  if ( !m_OurPopulationManager->SupplyShouldFlock() )
  {
    return pars_FAttractingMate;
  }
  else
  {
    // Unlock covey 'peg'.
    m_covey->SetFixed( false );
    return pars_FFlocking;
  }
}
//------------------------------------------------------------------------------

/**
Must do all the clean-up necessary to remove the female from the simulation without leaving any stray pointers to this object.\n
*/
void Partridge_Female::FDying( void )
{
  if ( m_CurrentStateNo == -1 ) return; // Don't kill me twice
  // Do I have kids? - dont' care - taken care of by covey
  // Do I have a clutch & mate, if so tell them
  m_OurPopulationManager->m_comms_data->m_female = this;
  m_OurPopulationManager->m_comms_data->m_male = m_MyMate;
  m_OurPopulationManager->m_comms_data->m_clutch = m_MyClutch;
  m_OurPopulationManager->m_messagecentre.PassMessage( m_OurPopulationManager->m_comms_data, pcomm_MumDeadC );
  m_MyMate = NULL;
  RemoveOldMate(false);
  m_CurrentStateNo = -1;
  m_StepDone = true;
  m_covey->NestLeave(); // Just in case this is not already done
  m_covey->RemoveMember( this );
  m_state = pars_Destroy;
  // Output as necessary
  m_OurPopulationManager->m_Ourkfactors->m_ThisYear->incNoDeadFemales();
  //m_covey->SanityCheck2(m_covey->GetOurChicks() );
  //  m_OurPopulationManager->WriteParAdMort( m_OurLandscape->SupplyGlobalDate(), m_age, 1, m_signal );
}



//------------------------------------------------------------------------------

//''''''''''''''''  Female Interface Functions '''''''''''''''''''''''''''''''''

/**
Called following a pcomm_WaitForMale message from Partridge_Population_Manager::DissolveCovey \n
*/
void Partridge_Female::OnWaitForMale()
{
  m_covey->SetFixed( false );
  if (m_UncleStatus) {
	  SetUncleStatus( false );
  }
  RemoveOldMate(false);
  m_state = pars_FAttractingMate;
}



//------------------------------------------------------------------------------
/**
Arrives here following pcomm_Mating message from either Partridge_Population_Manager::DissolveCovey or Partridge_Male::MFindingMate.\n
*/
void Partridge_Female::OnMating( PartridgeCommunicationData * pc_data )
{
  m_MyMate = pc_data->m_male;
  RemoveOldMate(false);
  SwitchCovey( pc_data->m_covey );
  // The state she needs to go to depends on she has a
  // territory or not
  if ( HaveTerritory() )
  {
    m_state = pars_FBuildingUpResources;
	m_buildupwait = cfg_buildupwait.value();
  }
  else
    m_state = pars_FFindingTerritory;
  m_clutch_number = 0;
  m_clutchproduced = false;
}



//------------------------------------------------------------------------------

/**
Arrives here follwing a pcomm_ClutchDead or a pcomm_AllInfertile message. \n
*/
void Partridge_Female::OnClutchDeath()
{
	if (m_state == pars_FIncubating) {
		m_state = FStartingNewBrood(false);
	} else m_state = FStartingNewBrood(true);
	m_covey->NestLeave();
	m_MyClutch = NULL;
}
//------------------------------------------------------------------------------

/**
Arrives here as a result of a pcomm_ClutchMown message. This determines whether the female dies with the clutch.\n
*/
void Partridge_Female::OnClutchMown()
{
	if (m_state==pars_FIncubating) {
		if ( g_rand_uni() < cfg_female_mowing_chance.value() )
		{
			//Optional output - can depend on the definition of predation in use.
			//m_OurPopulationManager->m_Ourkfactors->incNoHensPredated();
			FDying();
		}
	}
	OnClutchDeath();
}
//------------------------------------------------------------------------------

/**
Response to the pcomm_SetClutch message received on Partridge_Clutch object creation.\n
*/
void Partridge_Female::OnSetMyClutch( PartridgeCommunicationData * pc_data )
{
  m_MyClutch = pc_data->m_clutch;
  m_state = pars_FIncubating;
}
//------------------------------------------------------------------------------

/**
Response to pcomm_EggsHatch message arriving on clutch hatching.\n
*/
void Partridge_Female::OnEggsHatch()
{
  m_state = pars_FCaringForYoung;
  m_covey->NestLeave();
  m_MyClutch = NULL;
}
//------------------------------------------------------------------------------

/**
Response to pcomm_ChicksMature message arriving when the last chick matures.\n
*/
void Partridge_Female::OnChicksMatured()
{
  m_state = pars_FFlocking;
  m_MyMate = NULL;
  m_HaveTerritory = false;
}
//------------------------------------------------------------------------------

/**
Response to pcomm_AllChicksDead message arriving when the last chick dies.\n
*/
void Partridge_Female::OnChicksDead()
{
  m_state = pars_FFlocking;
  m_HaveTerritory = false;
  m_MyMate = NULL;
}
//------------------------------------------------------------------------------

/**
Response to a pcomm_MaleDying message from Partridge_Male::MDying.\n
*/
void Partridge_Female::OnMateDying( PartridgeCommunicationData * pc_data )
{
  if ( pc_data->m_male != m_MyMate )
  {
    m_OurLandscape->Warn( "Partridge_Female::OnMateDying(): ""m_MyMate is not the mate that is dying", "" );
    exit( 1 );
  }
  RemoveOldMate(false);
  m_MyMate = NULL;
  // What she does now depends on whether the chicks have hatched etc.
  switch ( m_state )
  {
    case pars_FCaringForYoung:
    break; // keep doing it
    case pars_FFlocking:
      m_state = pars_FFlocking;
    break;
    case pars_FFindingTerritory:
      m_state = pars_FFindingTerritory;
    break;
    case pars_FIncubating:
    case pars_FLaying:
      //break;
      m_state = pars_FFindingTerritory;
      m_MyClutch->OnGivenUp();
      m_MyClutch = NULL;
      m_covey->SetFixed( false );
      m_covey->NestLeave();
	  break;
    case pars_FMakingNest:
    case pars_FBuildingUpResources:
    case pars_FStartingNewBrood:
      m_state = pars_FAttractingMate;
      m_covey->SetFixed( false );
      m_covey->NestLeave();
    break;
    default:
      m_OurLandscape->Warn( "Partridge_Female::OnMateDeath - unknown pars type", NULL );
      exit( 1 );
  }
}



//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//''''''''''''''  Female Interface Functions End  ''''''''''''''''''''''''''''''

//------------------------------------------------------------------------------
//                            Partridge_Male
//------------------------------------------------------------------------------

Partridge_Male::Partridge_Male( int a_born_x, int a_born_y, int a_x, int a_y, Partridge_Covey * a_covey, Landscape * a_map,
     int a_family_counter, Partridge_Population_Manager * a_manager ) :
     Partridge_Base( a_born_x, a_born_y, a_x, a_y, a_family_counter, a_covey, a_map, a_manager )
     {
		m_MyOldWife=NULL; // Cant call removeOldMate due to knock on effects
       m_MyMate = NULL;
       m_object_type = pob_Male;
       m_lifespan = random( cfg_par_max_lifespan.value() - cfg_par_min_lifespan.value() ) + cfg_par_min_lifespan.value();
	   //m_visitors_pass = false;
}



//------------------------------------------------------------------------------

/**
The BeginStep code for the male partridge. Its only function is the test for mortality of different kinds.
Might as well remove him here than waste CPU time thinking about the Step code later this time-step.\n
*/
void Partridge_Male::BeginStep( void )
{
#ifdef __PAR_DEBUG
  CheckMatePointers();
#endif
  m_age++; // Get a day older
  if ( m_age >= m_lifespan )
  {
    m_signal = 0;
    MDying();
  }
  else if ( !DailyMortality() ) CheckManagement();
  else
  {
    m_signal = 1;
    MDying();
  }
#ifdef __PAR_DEBUG
  CheckMatePointers();
#endif
}
//------------------------------------------------------------------------------

/**
Male Step code is where all his behaviour is carried out.\n
*/
void Partridge_Male::Step( void )
{
#ifdef __PAR_DEBUG
  CheckMatePointers();

#endif
  if ( m_CurrentStateNo == -1 || m_StepDone )
    return;
  switch ( m_state )
  {
    case pars_Initiation:
      m_state = pars_MFlocking;
    break;
    case pars_MFlocking:
      m_state = MFlocking();
      m_StepDone = true;
    break;
    case pars_MFindingMate:
      m_state = MFindingMate();
      m_StepDone = true;
    break;
    case pars_MPairing:
      m_state = MPairing();
      m_StepDone = true;
    break;
    case pars_MGuardingMate:
      m_state = MGuardingMate();
      m_StepDone = true;
    break;
    case pars_MCaringForYoung:
      m_state = MCaringForYoung();
      m_StepDone = true;
    break;
    case pars_MDying:
    case pars_Destroy:
      m_StepDone = true;
    break;
    case pars_MFollowingMate:
      m_state = MFollowingMate();
      m_StepDone = true;
    break;
    default:
      m_OurLandscape->Warn( "Partridge_Male::Step - unknown pars type", NULL );
      exit( 1 );
  }
#ifdef __PAR_DEBUG
  CheckMatePointers();
#endif
}



//------------------------------------------------------------------------------

/**
Male EndStep - nothing to do here. Currently just checks for unknown states.\n
*/
void Partridge_Male::EndStep( void )
{
  switch ( m_state )
  {
    case pars_Initiation:
    case pars_MFlocking:
    case pars_MFindingMate:
    case pars_MPairing:
    case pars_MGuardingMate:
    case pars_MFollowingMate:
    case pars_MCaringForYoung:
    case pars_MDying:
    case pars_Destroy:
    break;
    default:
      m_OurLandscape->Warn( "Partridge_Male::EndStep - unknown pars type", NULL );
      exit( 1 );
  }
}
//------------------------------------------------------------------------------

/**
Check for and respond to management events.
*/
bool Partridge_Male::OnFarmEvent( FarmToDo event )
{
  switch ( event )
  {
    case sleep_all_day:
    break;
    case autumn_plough:
    break;
    case autumn_harrow:
    break;
    case autumn_roll:
    break;
    case autumn_sow:
    break;
    case winter_plough:
    break;
    case deep_ploughing:
    break;
    case spring_plough:
    break;
    case spring_harrow:
    break;
    case spring_roll:
    break;
    case spring_sow:
    break;
    case fp_npks:
    break;
    case fp_npk:
    break;
    case fp_pk:
    break;
    case fp_liquidNH3:
    break;
    case fp_slurry:
    break;
    case fp_manganesesulphate:
    break;
    case fp_manure:
    break;
    case fp_greenmanure:
    break;
    case fp_sludge:
    break;
    case fa_npk:
    break;
    case fa_pk:
    break;
    case fa_slurry:
    break;
    case fa_ammoniumsulphate:
    break;
    case fa_manure:
    break;
    case fa_greenmanure:
    break;
    case fa_sludge:
    break;
	case glyphosate:
    case herbicide_treat:
    break;
    case growth_regulator:
    break;
    case fungicide_treat:
    break;
    case syninsecticide_treat:
    case insecticide_treat:
	case product_treat:
    break;
    case molluscicide:
    break;
    case row_cultivation:
    break;
    case strigling:
      if ( g_rand_uni() < 0.01 ) MDying();
    break;
    case hilling_up:
    break;
    case water:
    break;
    case swathing:
    break;
    case harvest:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) MDying();
    break;
    case cattle_out:
    break;
    case cattle_out_low:
    break;
    case cut_to_hay:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) MDying();
    break;
    case cut_to_silage:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) MDying();
    break;
    case straw_chopping:
    break;
    case hay_turning:
    break;
    case hay_bailing:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) MDying();
    break;
    case stubble_harrowing:
    break;
    case autumn_or_spring_plough:
    break;
    case burn_straw_stubble:
    break;
    case mow:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) MDying();
    break;
    case cut_weeds:
      if ( g_rand_uni() < cfg_par_ad_cut.value() ) MDying();
    break;
    case pigs_out:
    break;
    case strigling_sow:
      if ( g_rand_uni() < 0.01 ) MDying();
    break;
    default:
      m_OurLandscape->Warn( "Skylark_Clutch::OnFarmEvent(): Unknown event type:", m_OurLandscape->EventtypeToString( event ) );
      exit( 1 );
  }
  // Must incorporate a test here in case the animal is dead - killing it twice
  // can be a bad idea
  if ( m_state == pars_Destroy ) return true;
  else
    return false;
}



//------------------------------------------------------------------------------

/**
Test for background mortality - identical to the female except that the male responds to female activity.\n
*/
bool Partridge_Male::DailyMortality( void )
{

  double chance = g_rand_uni();
  double extramort=1.0;

// extra mortality chance if in a covey alone
  if ( m_covey->GetCoveySize() == 1 )
  {
    extramort=cfg_par_adult_mortality_alone.value();
  }
	// Winter mortality between April and Sept
	int today = m_OurLandscape->SupplyDayInYear();
	// If the female is looking for a territory then the mortality is higher
	if (m_MyMate!=NULL)  {
		if (m_MyMate->WhatState()==pars_FFindingTerritory) {
			if ( chance < cfg_par_adult_mortalitySpring.value() ) return true;
			return false;
		}
	}
	// Is it winter?
	if (( ( today >= October ) || ( today < April )) ) {
		if (m_OurPopulationManager->SupplyShouldFlock())  {	// We are in the "Winter Period"
			if ( chance < cfg_par_adult_mortalityWinter.value()*extramort ) {
				return true;
			} else return false;
		} else { // Finding territory value
			if ( chance < cfg_par_adult_mortalitySpring.value()*extramort ) {
				return true;
			} else return false;
		}
	} else // Not winter and not finding territory
	{
			if ( chance < (cfg_par_adult_mortalitySummer.value()*extramort) ) return true;
	}
  return false;

}
//------------------------------------------------------------------------------

/**
Flocking. Do Nothing but wait for the covey to tell us OnStartBreedingBehaviour
*/
Partridge_State Partridge_Male::MFlocking( void )
{
  m_covey->MoveDistance( 1000, 10, 10000 );
  return pars_MFlocking;
}
//------------------------------------------------------------------------------

/**
Caused by being unpaired on covey dissolve, he must find a mate. If the date is early then he'll try to hop into a nearby covey to find a mate if there is one. Later he goes on a more desparate search alone (which incurs a higher mortality if unsuccessful see Partridge_Male::DailyMortality.\n
*/
Partridge_State Partridge_Male::MFindingMate( void )
{
	if ( m_UncleStatus )
	{
		// This is no good - he can't stay an uncle anymore - must leave
		m_covey->RemoveMember( this );
		MakeCovey();
		m_UncleStatus = false; // just in case
		return pars_MFlocking; // this will send him back here eventually.
	}
	if ( m_OurPopulationManager->SupplyShouldFlock() )
	{
		m_UncleStatus = false; // just in case
		m_covey->SetFixed( false ); // ensure we do not have a fixed territory loc
		//}
		return pars_MFlocking;
	}
  // Depends on the date what happens.
  // If early then he will keep hopping if possible, if late and breeding is
  // starting then he must wander.
  //
  if ( ( cfg_par_start_dissolve.value() + 30 ) > m_OurLandscape->SupplyDayInYear() )
  {
    // Still early so we need a list of possible coveys to hop to
    if ( m_OurPopulationManager->FillCoveyNeigbourList( m_covey, 500, m_Location_x, m_Location_y ) )
    {
      Partridge_Covey * pc = m_covey->FindNeighbour();
      if ( pc )
      {
        m_MyMate = pc->FindMeAMateInCovey( m_family_counter );
        if ( m_MyMate )
        {
          m_covey->RemoveMember( this ); // NB the next two lines MUST come after a call to RemoveMember for adults if they are not dying
		  m_UncleStatus = false;
          MakeCovey(); // Has his own now.
        }
      }
    }
  }
  else
  {
    // Must leave the covey he is with if he is not already alone
    if ( m_covey->GetCoveySize() > 1 )
    {
      m_covey->RemoveMember( this );
//      m_visitors_pass = false;
	  m_UncleStatus = false;
      MakeCovey();
    }
    m_covey->FlyTo( cfg_par_MaleFlyingDistance.value() );
    m_MyMate = m_covey->FindMateInArea( cfg_par_MaleMateSearchArea.value() );
  }
  // If he has found a mate he will have made his covey and now calls her to him
  if ( m_MyMate )  {
    // The line below is really debug information, this identifies the caller
    m_OurPopulationManager->m_comms_data->m_male = this;
    //  Call the female via the message centre
    m_OurPopulationManager->m_comms_data->m_female = m_MyMate;
    m_OurPopulationManager->m_comms_data->m_covey = m_covey;
    // This message also sets the females covey to the male's
    m_OurPopulationManager->m_messagecentre.PassMessage( m_OurPopulationManager->m_comms_data, pcomm_Mating );
#ifdef __PAR_DEBUG
    CheckMatePointers();
#endif
    if ( !m_MyMate->HaveTerritory() ) return pars_MFollowingMate;
    return pars_MGuardingMate;
  }
  return pars_MFindingMate;
}



//------------------------------------------------------------------------------

/**
If he has a territory then he is must go to mate guarding otherwise he must carry on and find one by following her lead. \n
*/
Partridge_State Partridge_Male::MPairing( void )
{
#ifdef __PAR_DEBUG
  CheckMatePointers();

#endif
  if ( m_MyMate->HaveTerritory() ) return pars_MGuardingMate;
  return pars_MFollowingMate;
}
//------------------------------------------------------------------------------

Partridge_State Partridge_Male::MFollowingMate( void )
{
/**
Will enter this state if he is paired, but the female has not yet found a territory.\n
Waits here until the mate has found a territory
*/
#ifdef __PAR_DEBUG
  CheckMatePointers();

#endif
  if ( !m_MyMate->HaveTerritory() ) return pars_MFollowingMate;
  return pars_MGuardingMate;
}



//------------------------------------------------------------------------------

Partridge_State Partridge_Male::MGuardingMate( void )
{
  /**
  He will sit here until he is told to do otherwise - either mate dead, eggs have hatched or female given up.
  */
  return pars_MGuardingMate;
}
//------------------------------------------------------------------------------

/**
This simple ensures that there is a call to a foraging movement, the size of which is dependent upon the age of the young.\n
*/
Partridge_State Partridge_Male::MCaringForYoung( void )
{
  // dist move is calculated as a function of chick food need-
  // First get the chick age
  int ch_age = m_covey->GetChickAge();
  m_covey->MoveDistance( g_MaxWalk[ch_age], 10, 10000 );
  return pars_MCaringForYoung;
}
//------------------------------------------------------------------------------

/**
Do the housekeeping for dying.\n
*/
void Partridge_Male::MDying( void )
{

  if ( m_CurrentStateNo != -1 )
  { // Don't kill me twice
    // Tell my mate if I have one
    if ( m_MyMate )
    {
      // The line below is really debug information, this identifies the caller
      m_OurPopulationManager->m_comms_data->m_male = this;
      //  Call the female via the message centre
      m_OurPopulationManager->m_comms_data->m_female = m_MyMate;
      m_OurPopulationManager->m_messagecentre.PassMessage( m_OurPopulationManager->m_comms_data, pcomm_MaleDying );
      m_MyMate = NULL;
    }
	RemoveOldMate(false);
    m_CurrentStateNo = -1;
    m_StepDone = true;
    //m_covey->SanityCheck2(m_covey->GetOurChicks() );

    m_covey->RemoveMember( this );
    m_state = pars_Destroy;
    m_OurPopulationManager->m_Ourkfactors->m_ThisYear->incNoDeadMales();
    //  m_OurPopulationManager->WriteParAdMort( m_OurLandscape->SupplyGlobalDate(), m_age, 0, m_signal );
  }
}
//------------------------------------------------------------------------------

/**
Removes a memory of the old mate. If knockon is true this is reciprocal.\n
*/
void Partridge_Male::RemoveOldMate( bool a_knockon ) {
	  if (m_MyOldWife!= NULL) {
		  if (!a_knockon) m_MyOldWife->RemoveOldMate(true);
		  m_MyOldWife= NULL;
	  }
  }
//------------------------------------------------------------------------------


//''''''''''''''''''''  Male Interface Functions   '''''''''''''''''''''''''''''

/**
Response to pcomm_EggsHatch message arriving on clutch hatching.\n
*/
void Partridge_Male::OnLookAfterKids( void )
{
  // He doesn't actually do anything, just records that fact that this is
  // what his biological counterpart would be doing now.
  m_state = pars_MCaringForYoung;
#ifdef __PAR_DEBUG
  CheckMatePointers();
#endif
}
//------------------------------------------------------------------------------

/**
Response to pcomm_AllChicksDead message arriving when the last chick dies.\n
*/
void Partridge_Male::OnChicksDead( void )
{
  m_MyMate = NULL;
  m_state = pars_MFlocking;
}
//------------------------------------------------------------------------------

/**
Response to pcomm_ChicksMature message arriving when the last chick matures.\n
*/
void Partridge_Male::OnChicksMatured( void )
{
  m_MyMate = NULL;
  m_state = pars_MFlocking;
}
//------------------------------------------------------------------------------

/**
Response to the pcomm_StoppingBreeding message, caused by the female giving up breeding.\n
*/
void Partridge_Male::OnStoppingBreeding( void )
{
  m_MyMate = NULL;
  m_state = pars_MFlocking;

}
//------------------------------------------------------------------------------
/**
Got this message from Partridge_Population_Manager::DissolveCovey because he has no mate and covey dissolve has happended.\n
*/
void Partridge_Male::StartBreedingBehaviour( void )
{
	m_state = pars_MFindingMate;
	RemoveOldMate(false);
}
//------------------------------------------------------------------------------

/**
A pcomm_MovingHome message from the female if she cannot find a nest location in the current 'territory'.\n
*/
void Partridge_Male::OnMovingHome( void )
{
  m_state = pars_MGuardingMate;
}
//------------------------------------------------------------------------------

/**
Arrives here following pcomm_MatingM message from Partridge_Population_Manager::DissolveCovey.\n
*/
void Partridge_Male::OnMating( Partridge_Female * a_mate )
{
#ifdef __PAR_DEBUG
  if ( m_MyMate != NULL )
  {
    m_OurLandscape->Warn( "Partridge_Male::OnMating(): ""m_MyMate is not NULL on Mating", "" );
    exit( 1 );
  }
#endif
	m_MyMate = a_mate;
    RemoveOldMate(false);
	m_state = pars_MPairing;
	SetFamily( m_MyMate->GetFamily() ); // synchronize families
#ifdef __PAR_DEBUG
  CheckMatePointers();
#endif

}
//------------------------------------------------------------------------------

/**
Response to a pcomm_MumDeadC message received when the female dies.\n
*/
void Partridge_Male::OnMateDying()
{
#ifdef __PAR_DEBUG
  CheckMatePointers();

#endif
    RemoveOldMate(false);
	m_MyMate = NULL;
	// This will send him to finding a mate if it is early, otherwise
	// indirectly to flocking unless he still has kids in which case he has to
	// look after them
	//
	if ( m_covey->GetOurChicks() == 0 ) m_state = pars_MFindingMate;
}



//------------------------------------------------------------------------------

/**
Response to a pcomm_FemaleGivingUp message received if the female gives up during territory finding.\n
*/
void Partridge_Male::OnFemaleGivingUp()
{
#ifdef __PAR_DEBUG
  CheckMatePointers();

#endif
	m_MyMate = NULL;
	RemoveOldMate(false);
	m_state = pars_MFlocking;
	m_covey->SetFixed( false );
}
//------------------------------------------------------------------------------
//''''''''''''''  Male Interface Functions End  ''''''''''''''''''''''''''''''''

/**
This is a debug function.\n
*/
void Partridge_Male::CheckMatePointers()
{
  //**CJT** DEBUG
  if ( m_MyMate )
  {
    // Check for mutual pointers here
    if ( m_MyMate->GetMate() != this )
    {
      m_OurLandscape->Warn( "Partridge_Male::CheckMatePointers(): ""Non-mutual mate pointers", "" );
      exit( 1 );
    }
    if ( m_state == pars_MFlocking )
    {
      m_OurLandscape->Warn( "Partridge_Male::CheckMatePointers(): ""Mated & Flocking", "" );
      exit( 1 );
    }
    if ( m_MyMate->GetCurrentStateNo() == -1 )
    {
      m_OurLandscape->Warn( "Partridge_Male::CheckMatePointers(): ""Dead Mate", "" );
      exit( 1 );
    }
  }
}

/**
This is a debug function.\n
*/
void Partridge_Female::CheckMatePointers()
{
  //**CJT** DEBUG
  if ( m_MyMate )
  {
    // Check for mutual pointers here
    if ( m_MyMate->GetMate() != this )
    {
      m_OurLandscape->Warn( "Partridge_Female::CheckMatePointers(): ""Non-mutual mate pointers", "" );
      exit( 1 );
    }
    if ( m_state == pars_FFlocking )
    {
      m_OurLandscape->Warn( "Partridge_Female::CheckMatePointers(): ""Mated & Flocking", "" );

      exit( 1 );
    }
    if ( m_MyMate->GetCurrentStateNo() == -1 )
    {
      m_OurLandscape->Warn( "Partridge_Female::CheckMatePointers(): ""Dead Mate", "" );
      exit( 1 );
    }
  }
}



//-----------------------------------------------------------------------------
//----------------- class k_factor methods ------------------------------------
//-----------------------------------------------------------------------------

k_factors::k_factors()
{
  kfactorsfile = fopen("k_factors_output.txt", "w" );
  if ( !kfactorsfile ) {
      g_msg->Warn( "k_factors::k_factors()","Cannot open k_factors_output.txt" );
      exit( 1 );
  }
  m_ThisYear = new population_attributes;
  m_LastYear = new population_attributes;
  reset();
}
//-----------------------------------------------------------------------------

k_factors::~k_factors()
{
  fclose( kfactorsfile );
  delete m_ThisYear;
  delete m_LastYear;
}

//-----------------------------------------------------------------------------
//----------------- class population_attributes methods ------------------------------------
//-----------------------------------------------------------------------------

population_attributes::population_attributes()
{
  reset();
}
//-----------------------------------------------------------------------------

population_attributes::~population_attributes()
{
}
//-----------------------------------------------------------------------------

void k_factors::Output_kfactors()
{
  //calcDickPottsk();
  fprintf( kfactorsfile, "%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\n", m_rk1, m_rk2, m_rk3, m_rk4, m_rk5, m_rk6, m_dpk1,
       m_dpk2, m_dpk3, m_dpk4, m_dpk5 );
  fflush( kfactorsfile );
  DumpInfoDatabase();
  reset();
}
//-----------------------------------------------------------------------------

void population_attributes::copyself(population_attributes* a_population_attributes)
{
  m_NoClutches = a_population_attributes->m_NoClutches;
  m_NoChicksHatched = a_population_attributes->m_NoChicksHatched;
  m_NoChicksSixWeeks = a_population_attributes->m_NoChicksSixWeeks;
  m_NoChicksSept = a_population_attributes->m_NoChicksSept;
  m_NoOldMales = a_population_attributes->m_NoOldMales;
  m_NoOldFemales = a_population_attributes->m_NoOldFemales;
  m_geomeanBroodSize = a_population_attributes->m_geomeanBroodSize;
  m_NoMalesSept = a_population_attributes->m_NoMalesSept;
  m_NoFemalesSept = a_population_attributes->m_NoFemalesSept;
  m_NoShotBirds = a_population_attributes->m_NoShotBirds;
  m_NoShotFemales = a_population_attributes->m_NoShotFemales;
  m_NoClutchesHatched = a_population_attributes->m_NoClutchesHatched;
  m_NoClutchesPredated = a_population_attributes->m_NoClutchesPredated;
  m_NoHensPredated = a_population_attributes->m_NoHensPredated;
  m_NoDeadChicks = a_population_attributes->m_NoDeadChicks;
  m_NoDeadFemales = a_population_attributes->m_NoDeadFemales;
  m_NoDeadMales = a_population_attributes->m_NoDeadMales;
  m_NoPairsApril = a_population_attributes->m_NoPairsApril;
  m_NoFemalesApril = a_population_attributes->m_NoFemalesApril;
  m_NoMalesApril = a_population_attributes->m_NoMalesApril;
  m_NoTerritorialFemalesApril = a_population_attributes->m_NoTerritorialFemalesApril;
  m_NoBirdsApril = a_population_attributes->m_NoBirdsApril;
  m_NoClutchesReplacements = a_population_attributes->m_NoClutchesReplacements;
  m_NoDeadClutches = a_population_attributes->m_NoDeadClutches;
  m_NoAgDeadClutches = a_population_attributes->m_NoAgDeadClutches;
  m_NoChick1sAug = a_population_attributes->m_NoChick1sAug;
  m_NoChick2sAug= a_population_attributes->m_NoChick2sAug;
  m_NoMalesAug= a_population_attributes->m_NoMalesAug;
  m_NoFemsAug = a_population_attributes->m_NoFemsAug;
  m_NoStarvedChicks = a_population_attributes->m_NoStarvedChicks;
  m_NoTerritorialFemalesMay = a_population_attributes->m_NoTerritorialFemalesMay;
  m_NoNonTerritorialFemalesMay = a_population_attributes->m_NoNonTerritorialFemalesMay;
}
//-----------------------------------------------------------------------------

void population_attributes::reset()
{
  m_NoClutches = 0;
  m_NoChicksHatched = 0;
  m_NoChicksSixWeeks = 0;
  m_NoChicksSept = 0;
  m_NoOldMales = 0;
  m_NoOldFemales = 0;
  m_geomeanBroodSize = 0.0;
  m_NoMalesSept = 0;
  m_NoFemalesSept = 0;
  m_NoShotBirds = 0;
  m_NoShotFemales = 0;
  m_NoClutchesHatched = 0;
  m_NoClutchesPredated = 0;
  m_NoHensPredated = 0;
  m_NoDeadChicks = 0;
  m_NoDeadFemales = 0;
  m_NoDeadMales = 0;
  m_NoPairsApril = 0;
  m_NoFemalesApril = 0;
  m_NoMalesApril = 0;
  m_NoTerritorialFemalesApril = 0;
  m_NoBirdsApril = 0;
  m_NoClutchesReplacements = 0;
  m_NoDeadClutches = 0;
  m_NoAgDeadClutches = 0;
  m_NoChick1sAug = 0;
  m_NoChick2sAug= 0;
  m_NoMalesAug= 0;
  m_NoFemsAug = 0;
  m_NoStarvedChicks=0;
  m_NoTerritorialFemalesMay = 0;
  m_NoNonTerritorialFemalesMay = 0;
}
//-----------------------------------------------------------------------------

void k_factors::reset()
{
  m_ThisYear-> reset();
  m_rk1 = 0;
  m_rk2 = 0;
  m_rk3 = 0;
  m_rk4 = 0;
  m_rk5 = 0;
  m_dpk1 = 0;
  m_dpk2 = 0;
  m_dpk3 = 0;
  m_dpk4 = 0;
  m_dpk5 = 0;

}

void k_factors::calcreal_1()
{
  // These can be calculated on Sept 1st.
  double anum = 1 + ( m_ThisYear->m_NoClutches - m_ThisYear->m_NoHensPredated );
  m_rk2 = log10( ( 1 + m_ThisYear->m_NoClutches ) / anum );
  anum = 1 + m_ThisYear->m_NoClutchesHatched;
  m_rk1 = log10( ( 1 + m_ThisYear->m_NoClutches ) / anum ) - m_rk2;
  anum = 1 + m_ThisYear->m_NoChicksSept;
  m_rk3 = log10( ( 1 + m_ThisYear->m_NoChicksHatched ) / anum );
}
//-----------------------------------------------------------------------------

void k_factors::calcreal_2()
{
  // This can only be calculated after shooting
  double NoBirdsAfterShoot = ( ( m_ThisYear->m_NoMalesSept + m_ThisYear->m_NoFemalesSept ) - m_ThisYear->m_NoShotBirds );
  double anum = 1 + NoBirdsAfterShoot;
  m_rk4 = log10( ( 1 + m_ThisYear->m_NoMalesSept + m_ThisYear->m_NoFemalesSept ) / anum );
}
//-----------------------------------------------------------------------------

void k_factors::calcreal_3()
{
  // K5 is the number of males and females in Sept/those that survive to
  // breeding time. This means k5 can only be calculated in April.
  double anum = 1 + m_ThisYear->m_NoFemalesSept - m_ThisYear->m_NoShotBirds / 2;
  m_rk5 = log10( anum / ( double )( 1 + m_ThisYear->m_NoFemalesApril ) );
  anum = 1 + m_ThisYear->m_NoMalesSept + m_ThisYear->m_NoFemalesSept - m_ThisYear->m_NoShotBirds;
  m_rk6 = log10( anum / ( double )( 1 + m_ThisYear->m_NoBirdsApril ) );
}
//-----------------------------------------------------------------------------

void k_factors::calcDickPottsk()
{
  // Dick makes some assumptions such it is assumed old males seen on 1st sept
  // is the same number as the number of pairs in Spring.
  // He also uses a calculation for the number of chicks hatched based on
  // the geometric mean of brood size on 1st September.
  // k2 assumes the differences between old males and old females is female on nest mortality

  // Requires the following values to be set before entry:
  // 1) GeoMean brood size, 1st sept.
  // 2) No. old males
  // 3) No. old females
  // 4) No. Males Sept
  // 5) No. Females Sept
  // 6) No. Females Sept of last year
  // 7) No. chicks at 1st Sept.

  // Calculate HatchedChicks
  double HatchedChicks;
  double CSR;
  if ( m_LastYear->m_geomeanBroodSize < 10.0 ) CSR = 0.03665 * ( pow( ( double )m_LastYear->m_geomeanBroodSize, ( double )1.293 ) );
  else
    CSR = m_LastYear->m_geomeanBroodSize / 13.84;
  if ( CSR > 0 ) HatchedChicks = m_LastYear->m_NoChicksSept / CSR;
  else
    HatchedChicks = 0;
  //
  double Broods = HatchedChicks / 13.8; // 13.8 is the mean brood size used by Dick, but we use 14
  // k2 is assuming old males is the same as pairs
  m_dpk2 = log10( ( double )( 1 + m_LastYear->m_NoOldMales ) / ( double )( 1 + m_LastYear->m_NoOldFemales ) );
  // k1 assumes old males in sept is the same as no. pairs in spring
  m_dpk1 = log10( ( double )( 1 + m_LastYear->m_NoOldMales ) / ( 1 + Broods ) ) - m_dpk2;
  // k3 contains a calculated number of chicks at six weeks based on the number
  // of chicks in Sept and brood size.
  m_dpk3 = log10( ( double )( 1 + HatchedChicks ) / ( double )( 1 + m_LastYear->m_NoChicksSept ) );
  // k4 Shooting numbers recorded so should be solid
  double NoBirdsAfterShoot = ( ( m_LastYear->m_NoMalesSept + m_LastYear->m_NoFemalesSept ) - m_LastYear->m_NoShotBirds );
  m_dpk4 = log10( ( double )( ( 1 + m_LastYear->m_NoMalesSept + m_LastYear->m_NoFemalesSept ) ) / ( double )( 1 + NoBirdsAfterShoot ) );
  // k5 - tricky one. Assumes 1:1 sex ratio. Assumes that the number of females
  // at the end of year 1 is equal to the number of breeding males and this is
  // equal to the number of old males in year 2. Hence the difference is the number
  // of birds that die of everything else except hen mortality.
  m_dpk5 = log10( ( double )( 1 + (m_LastYear->m_NoFemalesSept - m_LastYear->m_NoShotFemales)) / ( double )( 1 + m_ThisYear->m_NoOldMales ) );
  DumpInfoDatabase1( ( int )Broods, ( int )HatchedChicks, CSR );

  m_LastYear->copyself(m_ThisYear);
  //m_ThisYear->reset();  // ***CJT*** 6/7/2010 removed because otherwise our counts are destroyed - why was this here?
}
//-----------------------------------------------------------------------------

void k_factors::DumpInfoDatabase1( int Broods, int HC, double CSR )
{
  FILE * idb = fopen("k_facInfoBase.txt", "a" );
  if ( !idb ) {
      g_msg->Warn( "k_factors::DumpInfoDatabase1","Cannot open k_facInfoBase.txt" );
      exit( 1 );
  }
  fprintf( idb, "%d\t%d\t%d\t%g\t", HC, Broods, m_ThisYear->m_NoOldMales, CSR );
  fclose( idb );
}
//-----------------------------------------------------------------------------

void k_factors::DumpInfoDatabase()
{
  FILE * idb = fopen("k_facInfoBase.txt", "a" );
  if (!idb) {
      g_msg->Warn( "k_factors::DumpInfoDatabase","Cannot open k_facInfoBase.txt" );
      exit( 1 );
  }
  fprintf( idb, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%g\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n",
	 m_ThisYear->m_NoClutches,m_ThisYear->m_NoClutchesHatched, m_ThisYear->m_NoHensPredated, m_ThisYear->m_NoChicksSixWeeks, m_ThisYear->m_NoChicksSept, m_ThisYear->m_NoMalesSept, m_ThisYear->m_NoFemalesSept,  m_ThisYear->m_NoDeadFemales, m_ThisYear->m_NoDeadMales, m_ThisYear->m_NoShotBirds,
	 m_ThisYear->m_NoShotFemales, m_ThisYear->m_NoChicksHatched, m_ThisYear->m_NoOldMales, m_ThisYear->m_NoOldFemales, m_ThisYear->m_geomeanBroodSize,
     m_ThisYear->m_NoClutchesPredated, m_ThisYear->m_NoFemalesApril, m_ThisYear->m_NoTerritorialFemalesApril, m_ThisYear->m_NoDeadChicks, m_ThisYear->m_NoDeadClutches,
     m_ThisYear->m_NoClutchesReplacements, m_ThisYear->m_NoAgDeadClutches, m_ThisYear->m_NoPairsApril, m_ThisYear->m_NoMalesApril,m_ThisYear->m_NoTerritorialFemalesMay, m_ThisYear->m_NoNonTerritorialFemalesMay,
	 m_ThisYear->m_NoMalesDec, m_ThisYear->m_NoFemalesDec, m_ThisYear->m_NoChick1sAug, m_ThisYear->m_NoChick2sAug,
	 m_ThisYear->m_NoMalesAug, m_ThisYear->m_NoFemsAug, m_ThisYear->m_NoStarvedChicks );

  /*
       1m_NoClutches,
       2m_NoClutchesHatched,
	   3m_NoHensPredated,
	   4m_NoChicksSixWeeks,
	   5m_NoChicksSept,
	   6m_NoMalesSept,
	   7m_NoFemalesSept,
       8m_NoDeadFemales,
	   9m_NoDeadMales,
	   10m_NoShotBirds,
	   11m_NoChicksHatched,
	   12m_NoOldMales,
	   13m_NoOldFemales,
	   14m_geomeanBroodSize,
       15m_NoClutchesPredated,
	   16m_NoFemalesApril,
	   17m_NoTerritorialFemalesApril,
	   18m_NoDeadChicks,
	   19m_NoDeadClutches,
       20m_NoClutchesReplacements,
	   21m_NoAgDeadClutches,
	   22m_NoPairsApril,
	   23m_NoMalesApril,
	   24m_NoTerritorialFemalesMay,
	   25m_NoNonTerritorialFemalesMay
	   26m_NoMalesDec
	   27m_NoFemalesDec
	   28m_NoChick1sAug,
	   29m_NoChick2sAug,
	   30m_NoMalesAug,
	   31m_NoFemsAug
	   32m_NoStarvedChicks
	   */

  fclose( idb );
}
//-----------------------------------------------------------------------------

/**
Open a k-factor output file.\n
*/
void k_factors::CreateInfoDatabaseFile()
{
  FILE * idb = fopen("k_facInfoBase.txt", "w" );
  if (!idb) {
      g_msg->Warn("k_factors::CreateInfoDatabaseFile","Cannot open k_facInfoBase.txt" );
      exit( 1 );
  }
  fclose( idb );
}
//-----------------------------------------------------------------------------

/**
A debug function.\n
*/
int Partridge_Male::AmIaMember()
{
  if ( m_covey->GetCurrentStateNo() == -1 )
  {
    //exit(0);
    return 1;
  }
  if ( !m_covey->IsMember( this ) ) return 2;
  return 0;
}


