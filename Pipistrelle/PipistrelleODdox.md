/**
\mainpage ALMaSS Pipistrelle ODdox Documentation

\htmlonly 
<style type="text/css">
body {
    color: #000000
    background: white;
  }
h2  {
    color: #0022aa
    background: white;
  }
</style>
  <h1>
<small>
<small>
<small>
<small>
Created by:<br>
</small>
</small>
Chris J. Topping<br>
<small>Department of Bioscience,<br>
Grenaavej 14<br>
DK-8410 Roende<br>
Denmark<br>
<br>
5th February 2016<br>
</small>
</small>
</small>
</h1>
\endhtmlonly
\n
<HR> 
<br>
\htmlonly
<h2><big>The Pipistrelle model description following ODdox protocol</big></h2>
\endhtmlonly
<br>
\htmlonly
<h2><big>Overview</big></h2>
\endhtmlonly
<br>
<h2>  1. Purpose </h2>
The Pipistrelle model has been developed with the intention to determine the resiliance of the population to agrochemical usage assuming that the stressors affect mortality or reproduction
of the Pipistrelles exposed. The aim is to be able to quantify impacts of stressors on populations of Pipistrelles in different landscape structures under different agricultural management regimes.<br>
<br>
<h2>  2. State variables and scales </h2>
<br>
A class attribute overview is presented below, navigate to the individual entry to obtain further details, or look at 'Process Overview and Scheduling' for functions and behaviours 
below:<br>
<br>
<h3>Pipistrelle_Base</h3>
	- Attribute: Pipistrelle_Base::m_Age The age of the Pipistrelle (units days).
	- Attribute: Pipistrelle_Base::m_OurPopulationManager This is a time saving pointer to the correct Pipistrelle_Population_Manager object.
	- Attribute: Pipistrelle_Base::m_CurrentPipistrelleState This is the current behavioural state. The possible list is held by TTypeOfPipistrelleState

<h3>Pipistrelle_Young</h3>
* Class Pipistrelle_Juvenile represents the juvenile stage of the Pipistrelle. It is free living and moves in the landscape. \n
In addition to inherited attributes from Pipistrelle_Base, each young has:\n
 - Attribute: Pipistrelle_Young::m_weight which is the weight of the Pipistrelle in g

<h3>Pipistrelle_Adult</h3>
The adult class handles those attributes and functions common to all adults. 
In addition to inherited attributes from Pipistrelle_Young, each adult has:\n
	- Attribute: Pipistrelle_Adult::m_myMate Pointer to the mate if any, otherwise holds NULL. 

<h3>Pipistrelle_Male</h3>

<h3>Pipistrelle_Female</h3>
In addition to inherited attributes from Pipistrelle_Adult, each female has:\n


<h2>  3. Process Overview and Scheduling</h2>
<br>
As with all ALMaSS models the Pipistrelle model is based on a state machine which can be described by a state-transition diagram. 
Pipistrelles are in a behavioural state and due to decisions or external events move via transitions to another state where they will exhibit 
different behaviour.<br>

<h3>Pipistrelle_Base Behaviours & Functions</h3>
The Pipistrelle_Base contains all the behaviour that is common to all Pipistrelles, but there should never be a object of this type created. Therefore it does not have a state-transition diagram.
	- Behaviour: Pipistrelle_Base::st_Dying This is the way that dead Pipistrelles are removed from the simulation.
	- Function: Pipistrelle_Base::Pipistrelle_Base The constructor for the Pipistrelle_Base class.
	- Function: Pipistrelle_Base::~Pipistrelle_Base Pipistrelle destructor.


<h3>Pipistrelle_Young Behaviours & Functions</h3>
State-Transition diagram:
\image html Pipistrelle_Young_STDiagram.png
New behaviours are introduced here: st_Develop
	- Behaviour Pipistrelle_Young::st_Develop 	First determines whether the juvenile sucumbs to daily mortality, if so return toPipistrelles_Die state. If the juvenile survives:
		- The age is incremented
		- Weight is added as a daily increment based on an input parameter
		- If the total weight exceeds the parameter value held in Pipistrelle_Juvenile::m_JuvenileDevelopmentWeight, the juvenile matures and the state toPipistrelles_NextStage is returned
		- The behavioural state st_EvaluateHabitat is called following st_Develop, which leads to either a dispersal state or return to this state on the next day.


<h3>Pipistrelle_Adult Behaviours & Functions</h3>
Like Pipistrelle_Base there should never be an object of Pipistrelle_Adult. This class is just used to house the behaviours common to both sexes as adults.
New behaviours are introduced here st_Migrate, but others are modified st_EvaluateHabitat, st_Develop & st_Overwinter.

<h3>Pipistrelle_Male Behaviours & Functions</h3>
State-Transition diagram:
\image html Pipistrelle_Male_STDiagram.png

<h3>Pipistrelle_Female Behaviours & Functions</h3>
State-Transition diagram:
\image html Pipistrelle_Female_STDiagram.png

<h3>Pipistrelle_Population_Manager</h3>
The Pipistrelle population manager is responsible for handling all lists of Pipistrelles and warrens and scheduling their behaviour. It provides the facilities for output and handles central 
Pipistrelle functions needed to interact with the landscape. <br>

- Function: Pipistrelle_Population_Manager::Pipistrelle_Population_Manager Pipistrelle_Population_Manager Constructor.
- Function: Pipistrelle_Population_Manager::~Pipistrelle_Population_Manager The destructor for the Pipistrelle_Population_Manager class.



\htmlonly
<h2><big>Design</big></h2>
\endhtmlonly
<br>
<h2>  4. Design Concepts</h2>
<br>
<h3>  4.a Emergence</h3>
Todo<br>
<br>
<h3>  4.b Adaptation</h3>
Todo<br>
<br>
<h3>  4.c Fitness</h3>
Todo<br>
<br>
<h3>  4.d Prediction</h3>
Todo<br>
<br>
<h3>  4.e Sensing</h3>
Todo<br>
<br>
<h3>  4.f Interaction</h3>
Todo<br>
<br>
<h3>  4.g Stochasticity</h3>
Todo<br>
<br>
<h3>  4.h Collectives</h3>
Todo<br>
<br>
<h3>  4.i Observation</h3>
Todo<br>
<br>
<h2> 5. Initialisation</h2>
Todo<br>
<br>
<h2> 6. Inputs </h2>
<h3>Input variables</h3>
The upper-case names are the names used in the configuration file. See example below.
    - Pipistrelle_ADULT_MORTALITYCHANCE	#cfg_PipistrelleAdultMortalityChance	Daily adult mortality probability, excluding environmental mortality 	Default value is 0.0005 

<h2> 7. Interconnections</h2>
The Pipistrelle model relies on environmental data primarily from the Landscape and Weather classes. They are also dependent on interactions with the Farm and Crop classes
for information on management. Pesticide use and uptake depends on the Pesticide class.
The Pipistrelle_Population_Manager is descended from Population_Manager and performs the role of an auditor in the simulation. Many of the functions and behaviours needed to execute and
ALMaSS model are maintained by the Population_Manager.<br>
<br>
<h2> 8. References</h2>
<br>
*/
