/*
*******************************************************************************************************
Copyright (c) 2012, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file PipistrellePopulationManager.cpp
\brief <B>The main source code for all predator lifestage and population manager classes</B>
*/
/**  \file PipistrellePopulationManager.cpp
Version of  2 November 2012 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------

#include <string.h>
#include <iostream>
#include <fstream>
#include<vector>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Pipistrelle/Pipistrelle.h"
#include "../Pipistrelle/Pipistrelle_Population_Manager.h"


static CfgInt cfg_bat_maxforagedistance( "BAT_MAXFORAGEEDGEDISTANCE", CFG_CUSTOM, 20 );

//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------

Pipistrelle_Population_Manager::~Pipistrelle_Population_Manager (void)
{
   // Should all be done by the Population_Manager destructor
}
//---------------------------------------------------------------------------

Pipistrelle_Population_Manager::Pipistrelle_Population_Manager(Landscape* L) : Population_Manager(L)
{
    // Load List of Animal Classes
	m_ListNames[0] = "Pipistrelle_Young";
	m_ListNames[0] = "Pipistrelle_Larva";
	m_ListNames[0] = "Pipistrelle_Juvenile";
	m_ListNames[0] = "Pipistrelle_Male";
	m_ListNames[0] = "Pipistrelle_Female";
	m_ListNameLength = 5;
	// We need one vector for each life stage
	for (unsigned int i=0; i<(10-m_ListNameLength); i++)
	{
		TheArray.pop_back();
	}
    strcpy( m_SimulationName, "Pipistrelle Simulation" );
	// Create some animals
	struct_Pipistrelle* sp;
	sp = new struct_Pipistrelle;
	sp->NPM = this;
	sp->L = m_TheLandscape;
	for (int i=0; i< 1000; i++) // This will need to be an input variable (config)
	{
		sp->x = random(SimW);
		sp->y = random(SimH);
		CreateObjects(0,NULL,sp,1); // 0 = our Pipistrelle
	}
}

//---------------------------------------------------------------------------
void Pipistrelle_Population_Manager::CreateObjects(int ob_type,
           TAnimal * ,struct_Pipistrelle * data, int number)
{
	Pipistrelle_Male*  new_Pipistrelle_Male;
	Pipistrelle_Female*  new_Pipistrelle_Female;
	for (int i = 0; i<number; i++)
   {
	   /*
	   new_Pipistrelle_Male = new Pipistrelle_Male(data->x, data->y, data->L, data->NPM);
	   new_Pipistrelle_Female = new Pipistrelle_Female(data->x, data->y, data->L, data->NPM);
	   TheArray[ob_type].push_back(new_Pipistrelle);
	   */
   }
}
//---------------------------------------------------------------------------
void Pipistrelle_Population_Manager::PreProcessForageHabitat() {
	/** 
	* This method is the main facilitating method for the bat model. It runs through the landscape and identifies all 1m2 that are foraging habitats.
	* The rules for a foraging habitat is that it is with a predefined distance from wooded habitats or hedgerows (the list can be expanded).
	* The result is 2 maps with at 1m resolution with the following values Map1 0 = foraging habitat 1 = nothing special, Map 2 1 = edge location of building or woodland, 0 = other
	* Both these maps use the BinaryMapBase class. We have two loops for a bit of optimisation. The first is for those situations where we cannot possibly go off the edge of the map, the second is 
	* all the edge situations where we need to test for map wrap-around (al
	*/
	// Just to avoid multiple .value() calls the config variables are copied to local variables
	int maxviableforagedistance = cfg_bat_maxforagedistance.value();
	int x_extent = m_TheLandscape->SupplySimAreaWidth() - maxviableforagedistance;
	int y_extent = m_TheLandscape->SupplySimAreaHeight() - maxviableforagedistance;
	// Now we loop through the whole map square by square using an inner and outer loop
	// This is the inner loop
	for (int x = maxviableforagedistance; x < x_extent; x++) {
		for (int y = maxviableforagedistance; y < y_extent; x++) {
			// If it is an edge habitat then it records this in Map2, if a potential forage habitat then if its close enough it records this in Map1 as a 0, otherwise Map1 gets a 1.
			// Test for edge first
			TTypesOfLandscapeElement tole = m_TheLandscape->SupplyElementType( x, y );
			// This inner loop simply searches from the point out maxviableforagedistance is all directions and assesses if we hit an edge habitat.
			for (int i = x - maxviableforagedistance; i < x+maxviableforagedistance; i++) {
				for (int j = y - maxviableforagedistance; j < y + maxviableforagedistance; j++) {
					// If it is an edge habitat then it records this in Map2, if a potential forage habitat then if its close enough it records this in Map1 as a 0, otherwise Map1 gets a 1.
					TTypesOfLandscapeElement tole = m_TheLandscape->SupplyElementType( x+i, y+j );
					switch (tole) {
						default:;
					}
				}
			}
		}
	}

}
//---------------------------------------------------------------------------
void Pipistrelle_Population_Manager::PreProcessTerritories()
{}
//---------------------------------------------------------------------------
