/*
*******************************************************************************************************
Copyright (c) 2016, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Pipistrelle.cpp
\brief <B>The main source code for all predator lifestage and population manager classes</B>
*/
/**  \file Pipistrelle.cpp
Version of  2 November 2012 \n
By Chris J. Topping \n \n
*/

/*
To make a new animal from this code:
1) Copy all four Pipistrelle files to a new directory and rename them e.g. Elephant.h Elephant_Population_Manager.cpp etc
2)Open all four files and replace all occurences of Pipistrelle with Elephant or whatever you chose to use
3) Go to ALMaSS_GUI.cpp and add a new item to the item9Strings.
4) Add a new call to population manager in bool MyDialog::CreatePopulationManager() with the next number in sequence
5) Add the renamed files to the project and compile
*/

#include <iostream>
#include <fstream>
#include<vector>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Pipistrelle/Pipistrelle.h"
#include "../Pipistrelle/Pipistrelle_Population_Manager.h"
#include "../BatchALMaSS/BoostRandomGenerators.h"

extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;
extern MapErrorMsg *g_msg;

using namespace std;

Pipistrelle_Base::Pipistrelle_Base(int a_x, int a_y, unsigned a_cave, Landscape* a_L, Pipistrelle_Population_Manager* a_NPM) : TAnimal(a_x,a_y,a_L)
{
	// Assign the pointer to the population manager
	m_OurPopulationManager = a_NPM;
	m_CurrentPipistrelleState = toPipistrelles_InitialState;
	m_Age = 0;
	m_HomeCave = a_cave;
}

Pipistrelle_Base::~Pipistrelle_Base(void)
{
	;
}

void Pipistrelle_Base::st_Dying( void )
{
	m_CurrentStateNo = -1; // this will kill the animal object and free up space
}

Pipistrelle_Young::Pipistrelle_Young(int a_x, int a_y, unsigned a_cave, Landscape* a_L, Pipistrelle_Population_Manager* a_NPM) : Pipistrelle_Base(a_x, a_y, a_cave, a_L, a_NPM)
{
}

Pipistrelle_Young::~Pipistrelle_Young(void)
{
	;
}

Pipistrelle_Juvenile::Pipistrelle_Juvenile(int a_x, int a_y, unsigned a_cave, Landscape* a_L, Pipistrelle_Population_Manager* a_NPM) : Pipistrelle_Young(a_x, a_y, a_cave, a_L, a_NPM)
{
}

Pipistrelle_Juvenile::~Pipistrelle_Juvenile(void)
{
	;
}

Pipistrelle_Male::Pipistrelle_Male(int a_x, int a_y, unsigned a_cave, Landscape* a_L, Pipistrelle_Population_Manager* a_NPM) : Pipistrelle_Juvenile(a_x, a_y, a_cave, a_L, a_NPM)
{
}

Pipistrelle_Male::~Pipistrelle_Male(void)
{
	;
}

Pipistrelle_Female::Pipistrelle_Female(int a_x, int a_y, unsigned a_cave, Landscape* a_L, Pipistrelle_Population_Manager* a_NPM) : Pipistrelle_Juvenile(a_x, a_y, a_cave, a_L, a_NPM)
{
}

Pipistrelle_Female::~Pipistrelle_Female(void)
{
	;
}

