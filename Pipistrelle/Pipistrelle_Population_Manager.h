/*
*******************************************************************************************************
Copyright (c) 2012, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Pipistrelle_Population_Manager.h 
\brief <B>The main source code for all predator lifestage and population manager classes</B>
*/
/**  \file Pipistrelle_Population_Manager.h
Version of  2 November 2012 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------
#ifndef Pipistrelle_Population_ManagerH
#define Pipistrelle_Population_ManagerH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

class Pipistrelle;

//------------------------------------------------------------------------------

/**
\brief
Used for creation of a new Pipistrelle object
*/
class struct_Pipistrelle
{
 public:
  /** \brief x-coord */
  int x;
  /** \brief y-coord */
  int y;
  /** \brief species ID */
  int species;
  /** \brief Landscape pointer */
  Landscape* L;
  /** \brief Pipistrelle_Population_Manager pointer */
  Pipistrelle_Population_Manager * NPM;
};

/**
\brief
The class to handle all predator population related matters
*/
class Pipistrelle_Population_Manager : public Population_Manager
{
public:
// Methods
   /** \brief Pipistrelle_Population_Manager Constructor */
   Pipistrelle_Population_Manager(Landscape* L);
   /** \brief Pipistrelle_Population_Manager Destructor */
   virtual ~Pipistrelle_Population_Manager (void);
   /** \brief Method for creating a new individual Pipistrelle */
   void CreateObjects(int ob_type, TAnimal *pvo, struct_Pipistrelle* data, int number);
   /** \brief Preprocess the landscape and identify areas of foaraging habitat */
   void PreProcessForageHabitat();
   /** \brief Preprocess the landscape and identify potential territories */
   void PreProcessTerritories();

protected:
// Attributes
// Methods
   /** \brief  Things to do before anything else at the start of a timestep  */
   virtual void DoFirst(){}
   /** \brief Things to do before the Step */
   virtual void DoBefore(){}
   /** \brief Things to do before the EndStep */
   virtual void DoAfter(){}
   /** \brief Things to do after the EndStep */
   virtual void DoLast(){}
};

#endif