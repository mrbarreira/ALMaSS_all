// Version of 30th January 2002
// Lst modified CJT 30th Nov 2006
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
//---------------------------------------------------------------------------


#include <iostream>
#include <fstream>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/positionmap.h"
#include "../Spider/spider_all.h"
#include "../Spider/SpiderPopulationManager.h"

//---------------------------------------------------------------------------
//                         SPIDER CONSTANTS
//---------------------------------------------------------------------------

extern CfgBool cfg_ReallyBigOutput_used;

static CfgInt cfg_SpeciesRef( "SPID_SPECIES_REF", CFG_CUSTOM, 0 ); // default=Erigone
static CfgInt cfg_Max_Egg_Production( "SPID_MAX_EGG_PRODUCTION", CFG_CUSTOM, 100 );
static CfgFloat cfg_NoFoodLevel( "SPID_NOFOODLEVEL", CFG_CUSTOM, 2.5 ); //2.5 because that will give low food after harvest, but no food after ploughing
static CfgFloat cfg_LowFoodLevel( "SPID_LOWFOODLEVEL", CFG_CUSTOM, 10.58 );
static CfgFloat cfg_HighFoodLevel( "SPID_HIGHFOODLEVEL", CFG_CUSTOM, 18.515 );

static CfgInt cfg_DailyEggMort( "SPID_DAILYEGGMORT", CFG_CUSTOM, 40 );
CfgInt cfg_DailyJuvMort( "SPID_DAILYJUVMORT", CFG_CUSTOM, 30 );
static CfgInt cfg_DailyFemaleMort( "SPID_DAILYFEMALEMORT", CFG_CUSTOM, 3 );
static CfgFloat cfg_EggDevelConst2( "SPID_EGGDEVELCONSTTWO", CFG_CUSTOM, 1000 );
static CfgFloat cfg_JuvDevelConst2( "SPID_JUVDEVELCONSTTWO", CFG_CUSTOM, 1000.0 );
static CfgInt cfg_Adult_FireMort( "SPID_ADULT_FIREMORT", CFG_CUSTOM, 500 );


static CfgInt cfg_Juv_FireMort( "SPID_JUVENILE_FIREMORT", CFG_CUSTOM, 500 );
static CfgInt cfg_Egg_FireMort( "SPID_EGG_FIREMORT", CFG_CUSTOM, 500 );
static CfgInt cfg_Adult_GrazingMort( "SPID_ADULT_GRAZINGMORT", CFG_CUSTOM, 40 );
static CfgInt cfg_Juv_GrazingMort( "SPID_JUVENILE_GRAZINGMORT", CFG_CUSTOM, 40 );
static CfgInt cfg_Adult_GrazingBalloon( "SPID_ADULT_GRAZINGBALLOON", CFG_CUSTOM, 40 );
static CfgInt cfg_Juv_GrazingBalloon( "SPID_JUVENILE_GRAZINGBALLOON", CFG_CUSTOM, 40 );
static CfgInt cfg_Egg_GrazingMort( "SPID_EGG_GRAZINGMORT", CFG_CUSTOM, 40 );
// PESTICIDE RESPONSES
static CfgInt cfg_Adult_InsecticideApplication( "SPID_ADULT_INSECTICIDEAPPLICATION", CFG_CUSTOM, 900 );
static CfgInt cfg_Juvenile_InsecticideApplication( "SPID_JUVENILE_INSECTICIDEAPPLICATION", CFG_CUSTOM, 900 );
static CfgInt cfg_Egg_InsecticideApplication( "SPID_EGG_INSECTICIDEAPPLICATION", CFG_CUSTOM, 900 );
static CfgInt cfg_Adult_SynInsecticideApplication( "SPID_ADULT_SYNINSECTICIDEAPPLICATION", CFG_CUSTOM, 900 );
static CfgInt cfg_Juvenile_SynInsecticideApplication( "SPID_JUVENILE_SYNINSECTICIDEAPPLICATION", CFG_CUSTOM, 900 );
static CfgInt cfg_Egg_SynInsecticideApplication( "SPID_EGG_SYNINSECTICIDEAPPLICATION", CFG_CUSTOM, 900 );
static CfgInt cfg_PesticideTrialAdultTreatmentMort( "SPID_PESTICIDETRIALADULTTREATMENTMORT", CFG_CUSTOM, 900 );
static CfgInt cfg_PesticideTrialJuvenileTreatmentMort( "SPID_PESTICIDETRIALJUVENILETREATMENTMORT", CFG_CUSTOM, 900 );
static CfgInt cfg_PesticideTrialJuvenileToxicMort( "SPID_PESTICIDETRIALJUVENILETOXICMORT", CFG_CUSTOM, 900 );
static CfgInt cfg_PesticideTrialAdultToxicMort( "SPID_PESTICIDETRIALADULTTOXICMORT", CFG_CUSTOM, 900 );
//
static CfgFloat cfg_EggPlacementChanceConst( "SPID_EGGPLACEMENTCHANCECONST", CFG_CUSTOM, 6 );
static CfgInt cfg_HatDensityDepMortConst( "SPID_HATDENSITYDEPMORTCONST", CFG_CUSTOM, 2 ); //  Was 3
static CfgInt cfg_BalDensityDepMortConst( "SPID_BALDENSITYDEPMORTCONST", CFG_CUSTOM, 3 ); //  Was 5
static CfgInt cfg_WalDensityDepMortConst( "SPID_WALDENSITYDEPMORTCONST", CFG_CUSTOM, 3 ); //  Was 5
static CfgInt cfg_JuvDensityDepMortConst( "SPID_JUVDENSITYDEPMORTCONST", CFG_CUSTOM, 2 ); //  Was 4

//
static CfgFloat cfg_EggProducConst2( "SPID_EGGPRODUCCONSTTWO", CFG_CUSTOM, 1000 );

static CfgFloat cfg_mediumplantbiomass( "SPID_MEDIUMPLANTBIOMASS", CFG_CUSTOM, 100.0 );
static CfgFloat cfg_lowplantbiomass( "SPID_LOWPLANTBIOMASS", CFG_CUSTOM, 10.0 );
static CfgInt cfg_ExtraCompetionMortality( "SPID_EXTRACOMPETITIONMORTALITY", CFG_CUSTOM, 10 );
static CfgInt cfg_ExtraBadHabMortality( "SPID_EXTRABADHABMORTALITY", CFG_CUSTOM, 500 );
static CfgInt cfg_Adult_PloughMort( "SPID_ADULT_PLOUGHMORT", CFG_CUSTOM, 380 );
static CfgInt cfg_Juv_PloughMort( "SPID_JUV_PLOUGHMORT", CFG_CUSTOM, 380 );
static CfgInt cfg_Egg_PloughMort( "SPID_EGG_PLOUGHMORT", CFG_CUSTOM, 380 );
static CfgInt cfg_Adult_HarrowMort( "SPID_ADULT_HARROWMORT", CFG_CUSTOM, 250 );
static CfgInt cfg_Juv_HarrowMort( "SPID_JUV_HARROWMORT", CFG_CUSTOM, 250 );
static CfgInt cfg_Egg_HarrowMort( "SPID_EGG_HARROWMORT", CFG_CUSTOM, 250 );
static CfgInt cfg_Adult_StriglingMort( "SPID_ADULT_STRIGLINGMORT", CFG_CUSTOM, 370 );
static CfgInt cfg_Juv_StriglingMort( "SPID_JUVENILE_STRIGLINGMORT", CFG_CUSTOM, 370 );
static CfgInt cfg_Egg_StriglingMort( "SPID_EGG_STRIGLINGMORT", CFG_CUSTOM, 370 );
static CfgInt cfg_Adult_HarvestMort( "SPID_ADULT_HARVESTMORT", CFG_CUSTOM, 240 );
static CfgInt cfg_Adult_HarvestBalloon( "SPID_ADULT_HARVESTBALLOON", CFG_CUSTOM, 320 );
static CfgInt cfg_Juv_HarvestMort( "SPID_JUVENILE_HARVESTMORT", CFG_CUSTOM, 240 );
static CfgInt cfg_Juv_HarvestBalloon( "SPID_JUVENILE_HARVESTBALLOON", CFG_CUSTOM, 320 );
static CfgInt cfg_Egg_HarvestMort( "SPID_EGG_HARVESTMORT", CFG_CUSTOM, 240 );


const int ErigoneEggsPerSac[ 2 ] [ 13 ] = {{0, 0, 0, 7, 4, 5, 4, 6, 5, 5, 0, 0, 0},
                                       {0, 0, 0, 7, 4, 5, 4, 6, 5, 5, 0, 0, 0}};
//**per** changed to match fec. paper
int DroughtMort[ 8 ] = {0,0,10,20,40,60,80,100}; // **per** drought mortality lines
const int DispersalChance[ 40 ] = // Depending on days starved
// uses a index up to 40 because m_BadHabiatDays is used as the index
// which can only vary 0->39
  {
     /*  //ballooning motivation
      5,   5,  6,  7,  8,  9, 11, 13, 15, 22,
      49, 61, 76, 80, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82
      */
      // **per** ballooning motivation 
    
	  8,   9, 10, 11, 12, 14, 15, 16, 18, 22,
     29,  49, 61, 65, 67, 69, 71, 72, 74, 75,
     77,  79, 80, 81, 82, 82, 82, 82, 82, 82,
     82,  82, 82, 82, 82, 82, 82, 82, 82, 82
	 /*
	  // 1/8th ballooning motivation
	  1,2,2,2,2,2,2,2,3,3,4,7,8,9,9,9,9,9,10,10,10,10,10,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11
	  */
 };



//---------------------------------------------------------------------------
//                          Spider_BASE
//---------------------------------------------------------------------------


Spider_Base::Spider_Base( int x, int y, Landscape * L, Spider_Population_Manager * SpPM ) : TAnimal( x, y, L ) {
  CurrentSpState = tosps_Initiation;
  m_OurPopulation = SpPM;
}

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//                           Spider_Egg
//---------------------------------------------------------------------------

Spider_Egg::Spider_Egg( int x, int y, Landscape * L, Spider_Population_Manager * SpPM, int Eggs ) :
     Spider_Base( x, y, L, SpPM ) {
       m_DateLaid = L->SupplyDayInYear();
       m_NoEggs = Eggs;
       // **FN**
       m_pesticide_accum = 0.0;
}

//---------------------------------------------------------------------------


void Spider_Egg::BeginStep() {
  CheckManagement();
}

//---------------------------------------------------------------------------


void Spider_Egg::Step() {
  if ( m_StepDone || m_CurrentStateNo == -1 ) return;
#ifdef __CJTDebug_5
  if ( IsAlive() != 0x0DEADC0DE ) DEADCODEError();
#endif
  switch ( CurrentSpState ) {
    case tosps_Initiation: // Initial
      CurrentSpState = tosps_Develop;
    break;
    case tosps_Develop:
      switch ( st_Develop() ) {
        case 0: // Continue developing
            m_StepDone = true;
        break;
        case 1: // Must Hatch
            CurrentSpState = tosps_Hatch;
            m_StepDone = true;
		    break;
        case 2: // Must die
		    CurrentSpState = tosps_Dying;
	        break;
        default:
            m_OurLandscape->Warn( "Spider Egg - unknown return value from develop", NULL );
            exit( 1 );
      };
    break;

    case tosps_Hatch:
      st_Hatch();
	  m_OurPopulation->EggPosMap->ClearMapValue( m_Location_x, m_Location_y );
	  CurrentSpState = tosps_Destroy; //Destroys object
      m_CurrentStateNo = -1;
      m_StepDone = true;
    break;

    case tosps_Dying:
      st_Die();
      m_StepDone = true;
    break;

    default:
      m_OurLandscape->Warn( "Spider Egg - unknown state", NULL );
      exit( 1 );
  }
}

//---------------------------------------------------------------------------

void Spider_Egg::st_Hatch() {
  // Intially spreads the spiders over a EggSacSpread x EggSacSpread area
  // so as to minimise local density dependence

  int Eggs = 0;
  struct_Spider * SS;
  SS = new struct_Spider;
  SS->L = m_OurLandscape;
  SS->SpPM = m_OurPopulation;
  // do this to avoid testing from wrap around
  // - always moves co-ords away from edge
  int DoubleEggSacSpread = m_OurPopulation->SupplyDoubleEggSacSpread();

  int mx=m_Location_x;
  int my=m_Location_y;
  if ( mx < m_OurPopulation->SupplyEggSacSpread() ) mx = m_OurPopulation->SupplyEggSacSpread();
  if ( my < m_OurPopulation->SupplyEggSacSpread() ) my = m_OurPopulation->SupplyEggSacSpread();
  if ( mx + m_OurPopulation->SupplyEggSacSpread() >= m_OurPopulation->SimW )
    mx = m_OurPopulation->SimW - ( m_OurPopulation->SupplyEggSacSpread() + 1 );
  if ( my + m_OurPopulation->SupplyEggSacSpread() >= m_OurPopulation->SimH )
    my = m_OurPopulation->SimH - ( m_OurPopulation->SupplyEggSacSpread() + 1 );

  int tx, ty;
  int tries = 0;
  while ( ( Eggs < m_NoEggs ) & ( tries < 500 ) ) {
    tries++;
    tx = mx + random( DoubleEggSacSpread ) - m_OurPopulation->SupplyEggSacSpread();
    ty = my + random( DoubleEggSacSpread ) - m_OurPopulation->SupplyEggSacSpread();
    TTypesOfLandscapeElement ele = m_OurLandscape->SupplyElementType( tx, ty );
    switch ( ele ) {
      // Edit this list to add more impossible element types
      case tole_SmallRoad:
      case tole_LargeRoad:
	  case tole_Pond:
	  case tole_Freshwater:
      case tole_Saltwater:
      case tole_River:
      case tole_Scrub:
      case tole_Track:
      break;
      default:
        // Must write to the position map
        if ( m_OurPopulation->JuvPosMap->GetMapValue( tx, ty ) == 0 ) {
          if ( !HatchDensityMort( tx, ty ) ) {
            m_OurPopulation->JuvPosMap->SetMapValue( tx, ty );
            SS->x = tx;
            SS->y = ty;
            m_OurPopulation->CreateObjects( 1, this, SS, 1 );
          }
          Eggs++;
        }

    }
  }
  delete SS;
}

//---------------------------------------------------------------------------

bool Spider_Egg::HatchDensityMort( int tx, int ty ) {
/* THIS VERSION WAS NOT FASTER
unsigned range=cfg_HatDensityDepMortConst.value();
	int x1 = tx - range;
	int y1 = ty - range;
	if (x1<0) x1=0;
	if (y1<0) y1=0;
	range=(range<<1); // *2
	return (GetPosMapPositive(x1,y1,range));
}
*/
  int x1, y1, x2, y2;
  // Define extent
  int range=cfg_HatDensityDepMortConst.value();
  x1 = tx - range;
  y1 = ty - range;
  //range=range<<1;
  x2=tx+range;
  y2=ty+range;
  if ( x1 < 0 ) x1 = 0;
  if ( y1 < 0 ) y1 = 0;
  if ( x2 > m_OurPopulation->SupplySimW() ) x2 = m_OurPopulation->SupplySimW();
  if ( y2 > m_OurPopulation->SupplySimW() ) y2 = m_OurPopulation->SupplySimW();
  int sum = 0;
  for ( int i = x1; i < x2; i++ ) {
    for ( int j = y1; j < y2; j++ ) {
      sum += CheckJuvPosMap( i, j );
    }
  }
  if ( sum > __DENDEPCONST ) {
    return true; // Den Dep Killed it
  }
  return false; // all OK
}
//---------------------------------------------------------------------------


bool Spider_Egg::OnFarmEvent( FarmToDo event ) {
  switch ( event ) {
    case sleep_all_day:
    break;
    case autumn_plough:
      if ( random( 1000 ) < cfg_Egg_PloughMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case autumn_harrow:
      if ( random( 1000 ) < cfg_Egg_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case autumn_roll:
      if ( random( 1000 ) < cfg_Egg_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case autumn_sow:
      if ( random( 1000 ) < cfg_Egg_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case winter_plough:
      if ( random( 1000 ) < cfg_Egg_PloughMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case deep_ploughing:
      if ( random( 1000 ) < cfg_Egg_PloughMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case spring_plough:
      if ( random( 1000 ) < cfg_Egg_PloughMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case spring_harrow:
      if ( random( 1000 ) < cfg_Egg_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case spring_roll:
      if ( random( 1000 ) < cfg_Egg_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case spring_sow:
      if ( random( 1000 ) < cfg_Egg_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case fp_npks:
    break;
    case fp_npk:
    break;
    case fp_pk:
    break;
    case fp_liquidNH3:
    break;
    case fp_slurry:
    break;
    case fp_manganesesulphate:
    break;
    case fp_manure:
    break;
    case fp_greenmanure:
    break;
    case fp_sludge:
    break;
    case fa_npk:
    break;
    case fa_pk:
    break;
    case fa_slurry:
    break;
    case fa_ammoniumsulphate:
    break;
    case fa_manure:
    break;
    case fa_greenmanure:
    break;
    case fa_sludge:
    break;
    case herbicide_treat:
    break;
    case growth_regulator:
    break;
    case fungicide_treat:
    break;
    case glyphosate:
	case product_treat:
	  break;
    case insecticide_treat:
      if ( random( 1000 ) < cfg_Egg_InsecticideApplication.value() ) CurrentSpState = tosps_Dying;
    break;
    case syninsecticide_treat:
      if ( random( 1000 ) < cfg_Egg_SynInsecticideApplication.value() ) CurrentSpState = tosps_Dying;
    break;
    case molluscicide:
    break;
    case row_cultivation:
      if ( random( 1000 ) < cfg_Egg_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case strigling:
      if ( random( 1000 ) < cfg_Egg_StriglingMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case hilling_up:
      if ( random( 1000 ) < cfg_Egg_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case water:
    break;
    case swathing:
      if ( random( 1000 ) < cfg_Egg_HarvestMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case harvest:
      if ( random( 1000 ) < cfg_Egg_HarvestMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case cattle_out:
      if ( random( 1000 ) < cfg_Egg_GrazingMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case cut_to_hay:
      if ( random( 1000 ) < cfg_Egg_HarvestMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case cut_to_silage:
      if ( random( 1000 ) < cfg_Egg_HarvestMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case straw_chopping:
      if ( random( 1000 ) < cfg_Egg_HarvestMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case hay_turning:
      if ( random( 1000 ) < cfg_Egg_HarvestMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case hay_bailing:
      if ( random( 1000 ) < cfg_Egg_HarvestMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case flammebehandling:
      if ( random( 1000 ) < cfg_Egg_FireMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case stubble_harrowing:
      if ( random( 1000 ) < cfg_Egg_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case autumn_or_spring_plough:
      if ( random( 1000 ) < cfg_Egg_PloughMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case burn_straw_stubble:
      if ( random( 1000 ) < cfg_Egg_FireMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case mow:
      if ( random( 1000 ) < cfg_Egg_HarvestMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case cut_weeds:
      if ( random( 1000 ) < cfg_Egg_HarvestMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case pigs_out:
      if ( random( 1000 ) < cfg_Egg_GrazingMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case strigling_sow:
      if ( random( 1000 ) < cfg_Egg_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case trial_insecticidetreat:
      if ( random( 1000 ) < cfg_PesticideTrialAdultTreatmentMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case trial_toxiccontrol:
      if ( random( 1000 ) < cfg_PesticideTrialAdultToxicMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case trial_control:
    break;
    default:
    break;
  }
  return true;
}

//---------------------------------------------------------------------------


int Spider_Egg::st_Develop() {
  if ( random( 10000 ) < cfg_DailyEggMort.value() ) return 2; // stochastic mortality go to die
  if ( m_OurPopulation->SupplyEggDevelDegrees( m_DateLaid ) > cfg_EggDevelConst2.value() ) {
    //m_OurPopulation->WriteToTestFile(2, m_OurLandscape->SupplyDayInYear());
    return 1; // go to hatch
  } else
    return 0; // carry on developing
}

//---------------------------------------------------------------------------


void Spider_Egg::st_Die() {
  m_OurPopulation->EggPosMap->ClearMapValue( m_Location_x, m_Location_y );
  m_CurrentStateNo = -1;
  m_StepDone = true;
}

//---------------------------------------------------------------------------

bool Spider_Egg::GetPosMapPositive( unsigned x, unsigned y, unsigned range ) {
  return m_OurPopulation->JuvPosMap->GetMapPositive(x,y,range);
}
//---------------------------------------------------------------------------
/*
inline bool Spider_Egg::GetPosMapPositiveB( unsigned x, unsigned y, unsigned range ) {
  return m_OurPopulation->JuvPosMap->GetMapPositiveB(x,y,range);
}
*/
//---------------------------------------------------------------------------




//---------------------------------------------------------------------------
//                           Spider_Mobile
//---------------------------------------------------------------------------

Spider_Mobile::Spider_Mobile( int x, int y, Landscape * L, Spider_Population_Manager * SpPM ) : Spider_Base( x, y, L, SpPM ) {
  m_BadHabitatDays = 0;
  m_MyDirection = ( char )random( 8 );
}

//---------------------------------------------------------------------------


void Spider_Mobile::BeginStep() {
}

//---------------------------------------------------------------------------


void Spider_Mobile::Step() {
}

//---------------------------------------------------------------------------


void Spider_Mobile::EndStep() {
 // Arbitrary pesticide influence code.

#ifdef __SPIDER_PESTICIDE
  m_pesticide_accum += m_OurLandscape->SupplyPesticide( m_Location_x, m_Location_y, ppp_1 );
  if ( m_pesticide_accum > 1.0 ) {
    //printf( "%4ld %4d %4d\n", m_OurLandscape->SupplyDayInYear(), m_Location_x, m_Location_y );
    CurrentSpState = tosps_Dying;
    //m_CurrentStateNo = -1;
    //m_StepDone = true;
    return;
  }
  m_pesticide_accum = 0;
#endif

}

//---------------------------------------------------------------------------

int Spider_Mobile::AssessFood() {
  int tov = CheckToleTovIndex();
  if ( tov == 3 ) {
    double insects = m_OurLandscape->SupplyInsects( m_Location_x, m_Location_y );
    if ( insects < cfg_NoFoodLevel.value() ) return 0; else if ( insects < cfg_LowFoodLevel.value() ) return 1;
    else if ( insects > cfg_HighFoodLevel.value() ) return 3; else
      return 2;
  } 
  else if ( tov == 2 ) {
    double insects = m_OurLandscape->SupplyInsects( m_Location_x, m_Location_y ) * 0.5;
    if ( insects < cfg_NoFoodLevel.value() ) return 0; else if ( insects < cfg_LowFoodLevel.value() ) return 1;
    else if ( insects > cfg_HighFoodLevel.value() ) return 3; else
      return 2;
  }
  return 0;
}

//---------------------------------------------------------------------------


bool Spider_Mobile::BallooningMortality( int dist ) {
  //Mortality related directly to distance travelled
  if ( random( 100 ) < dist * m_OurPopulation->SupplyBallooningMortalityPerMeter() ) {

    return true;
  }
  return false;
}

//---------------------------------------------------------------------------

int Spider_Mobile::st_Balloon() {
  // Assumes that the distance moved is available in an array 0-99
  // also that the chance of dispersal is 20%+function of days in
  // bad habitat
  // Mortality is given by another array

  /** ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
  *CJT** removed - transfered to BeginStep code // Step 1 should I disperse?
   if (random(100)<(DispersalChance[m_BadHabitatDays])) {
  fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff */
  // Want to disperse
  // Step 2 can I disperse?
  if ( m_OurPopulation->SupplyBTimeToday() > 0 ) {
    // can disperse
    // Step 3 Mortality
    int Distance = ( int )( m_OurPopulation->SupplyDispDist( random( 100 ) ) * m_OurPopulation->SupplyBTimeToday() );
    if ( BallooningMortality( Distance ) ) {
      //m_OurPopulation->WriteToTestFile(4, m_OurLandscape->SupplyDayInYear());
      return 0; // Signal to die
    }
    // Step 4 where to go to
    int Direction = m_OurPopulation->SupplyWindDirection();
    return BalloonTo( Direction, Distance ); // 0-2 Die, Balloon, Stay
  }
  /* } */
  return 1; // carry on
}

//---------------------------------------------------------------------------

int Spider_Mobile::st_Walk() {
  if ( m_OurPopulation->SupplyMinWalkTemp() ) {
    // Step 1 should I disperse?
    if ( random( 100 ) < ( DispersalChance[ m_BadHabitatDays ] ) ) {
      // Want to disperse
      // Step 2 where to go to
      int Direction = m_MyDirection & -1;
      return WalkTo( Direction );
    }
  }
  return 1; // carry on
}

//---------------------------------------------------------------------------



int Spider_Mobile::CheckToleTovIndex() {

	// Alternative version to see if we can increase speed using a movementmap
	int mmapval=m_OurPopulation->m_MoveMap->GetMapValue(m_Location_x, m_Location_y );
	if (mmapval==1) {
		TTypesOfVegetation tov_type = tov_Undefined;
		tov_type = m_OurLandscape->SupplyVegType( m_Location_x, m_Location_y );
        switch ( tov_type ) {
          case tov_PermanentSetaside:
          case tov_Potatoes:
          case tov_PotatoesIndustry:
          case tov_Setaside:
          case tov_OSetaside:
          case tov_OPotatoes:
		  case tov_Heath:
            return 2;
         default:
			  return 3;

		}
	}
  return mmapval;
}

//---------------------------------------------------------------------------

int Spider_Mobile::st_AssessHabitat() {
  if ( m_MustBalloon ) return 1;
  int result = CheckToleTovIndex();
  switch ( result ) {
    case 0:
      return 0; // Code for dying
/*
case 1 removed to case 2, case 1 now a code for special habitat actions in CheckToleTovIndex()
case 1:
      //Extra mortality for trafficed and non-preferred habitats
      if ( random( 10000 ) < cfg_ExtraBadHabMortality.value() ) return 0;
      if ( m_OurPopulation->IsBallooningWeather() ) return 1; // try to balloon
      else
        return 2; // Nothing to do - add to starvation days

		*/
	case 2:
      // Extra Mortality for competition sake
      if ( random( 10000 ) < cfg_ExtraCompetionMortality.value() ) return 0;
      if ( m_OurPopulation->IsBallooningWeather() ) return 1; // try to balloon
      else
        return 3; // Nothing to do - add to starvation days, reproduce if
      // possible
    case 3:
      if ( !m_OurPopulation->CheckHumidity( m_Location_x, m_Location_y ) ) {
        // Is too dry
        if ( m_OurPopulation->IsBallooningWeather() ) return 1; // try to balloon
        else
          return 3; // do assessfood
      } else {
        return 4; // do assessfood
      }
    default:
      m_OurLandscape->Warn( "AssessHabitat - Bad return code", NULL );
      exit( 1 );
  }
}

//---------------------------------------------------------------------------

int Spider_Mobile::BalloonTo( int direction, int distance ) {
  // moves the spider 'distance' metres in 'direction' direction
  // returns 2 if the new location is not lethal otherwise returns 0
  //   m_OurPopulation->WriteToTestFile(8, m_OurLandscape->SupplyDayInYear());
	// For small landscapes ballooning round the world and back can be a problem
	// First clears its positions since it might try to count itself
	int SimW=m_OurPopulation->SupplySimW();
	if (distance>=SimW) distance-=SimW;
	//
  switch ( direction ) {
    case 0: // North
      m_Location_y -= distance;
      if ( m_Location_y < 0 ) m_Location_y += SimW;
    break;
    case 1: // NE
      m_Location_x += distance;
      m_Location_y -= distance;
      if ( m_Location_y < 0 ) m_Location_y += SimW;
      if ( m_Location_x >= SimW)  m_Location_x -= SimW;
    break;
    case 2: // E
      m_Location_x += distance;
      if ( m_Location_x >= SimW ) m_Location_x -= SimW;
    break;
    case 3: // SE
      m_Location_x += distance;
      m_Location_y += distance;
      if ( m_Location_x >= SimW ) m_Location_x -= SimW;
      if ( m_Location_y >= SimW ) m_Location_y -= SimW;
    break;
    case 4: // S
      m_Location_y += distance;
      if ( m_Location_y >= SimW ) m_Location_y -= SimW;
    break;
    case 5: // SW
      m_Location_x -= distance;
      m_Location_y += distance;
      if ( m_Location_x < 0 ) m_Location_x += SimW;
      if ( m_Location_y >= SimW ) m_Location_y -= SimW;
    break;
    case 6: // W
      m_Location_x -= distance;
      if ( m_Location_x < 0 ) m_Location_x += SimW;
    break;
    case 7: // NW
      m_Location_x -= distance;
      m_Location_y -= distance;
      if ( m_Location_x < 0 ) m_Location_x += SimW;
      if ( m_Location_y < 0 ) m_Location_y += SimW;
    break;
  }
  // Now find out if the habitat is lethal
  TTypesOfLandscapeElement ele = m_OurLandscape->SupplyElementType( m_Location_x, m_Location_y );
  switch ( ele ) {
    // Edit to this list to add more lethal element types
	  case tole_Pond:
	  case tole_Freshwater:
    case tole_Saltwater:
    case tole_River:
      return 0; //Die
    default:
      ;
  }
// Define extent
/*
  int x1, y1, x2, y2;
  int range=cfg_BalDensityDepMortConst.value();
  x1 = m_Location_x - range;
  x2 = m_Location_x + range;
  y1 = m_Location_y - range;
  y2 = m_Location_y + range;
  if ( x1 < 0 ) x1 = 0;
  if ( y1 < 0 ) y1 = 0;
  if ( x2 > SimW ) x2 = SimW;
  if ( y2 > SimW ) y2 = SimW;
  int sum = 0;
  for ( int i = x1; i < x2; i++ ) {
    for ( int j = y1; j < y2; j++ ) {
      sum+= CheckPosMap( i, j );
     }
  }
  if ( sum > __DENDEPCONST ) {
    //m_OurPopulation->WriteToTestFile(5, m_OurLandscape->SupplyDayInYear());
    return 0;
  }
*/
// CJT Replaced 14/04/2008
	unsigned int range=cfg_BalDensityDepMortConst.value();
	// NB these two must be signed!
	int x1 = m_Location_x - range;
	int y1 = m_Location_y - range;
	int y2 = m_Location_y + range;
	if (x1<0) x1=0;
	if (y1<0) y1=0;
	if (y2 >= SimW) range -= (++y2-SimW);
	range=(range<<1); // *2
//	if (GetPosMapPositiveB(x1,y1,range)) return 0;
	if (GetPosMapPositive(x1,y1,range)) return 0;
	return 2;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

int Spider_Mobile::WalkTo( int direction ) {
	// moves the spider 1 metre in 'direction' direction
	// returns 2 if the new location is OK otherwise returns 0 for death or 1 for no move
	int tx = m_Location_x;
	int ty = m_Location_y;
	switch (direction) {
		case 0: // North
			ty -= 1;
			if (ty < 0) ty += m_OurPopulation->SupplySimH();
			break;
		case 1: // NE
			tx += 1;
			ty -= 1;
			if (ty < 0) ty += m_OurPopulation->SupplySimH();
			if (tx >= m_OurPopulation->SupplySimW()) tx -= m_OurPopulation->SupplySimW();
			break;
		case 2: // E
			tx += 1;
			if (tx >= m_OurPopulation->SupplySimW()) tx -= m_OurPopulation->SupplySimW();
			break;
		case 3: // SE
			tx += 1;
			ty += 1;
			if (tx >= m_OurPopulation->SupplySimW()) tx -= m_OurPopulation->SupplySimW();
			if (ty >= m_OurPopulation->SupplySimH()) ty -= m_OurPopulation->SupplySimH();
			break;
		case 4: // S
			ty += 1;
			if (ty >= m_OurPopulation->SupplySimH()) ty -= m_OurPopulation->SupplySimH();
			break;
		case 5: // SW
			tx -= 1;
			ty += 1;
			if (tx < 0) tx += m_OurPopulation->SupplySimW();
			if (ty >= m_OurPopulation->SupplySimH()) ty -= m_OurPopulation->SupplySimH();
			break;
		case 6: // W
			tx -= 1;
			if (tx < 0) tx += m_OurPopulation->SupplySimW();
			break;
		case 7: // NW
			tx -= 1;
			ty -= 1;
			if (tx < 0) tx += m_OurPopulation->SupplySimW();
			if (ty < 0) ty += m_OurPopulation->SupplySimH();
			break;
	}
	// Now find out if the habitat is lethal
	TTypesOfLandscapeElement ele = m_OurLandscape->SupplyElementType( tx, ty );
	int sum = 0;
	switch (ele) {
		// Edit this list to add more lethal element types
		case tole_Pond:
		case tole_Freshwater:
		case tole_Saltwater:
		case tole_River:
			return 1;
		default:
			int x1, y1, x2, y2;
			// Define extent
			x1 = m_Location_x - cfg_WalDensityDepMortConst.value();
			x2 = m_Location_x + cfg_WalDensityDepMortConst.value();
			y1 = m_Location_y - cfg_WalDensityDepMortConst.value();
			y2 = m_Location_y + cfg_WalDensityDepMortConst.value();
			if (x1 < 0) x1 = 0;
			if (y1 < 0) y1 = 0;
			if (x2 > m_OurPopulation->SupplySimW()) x2 = m_OurPopulation->SupplySimW();
			if (y2 > m_OurPopulation->SupplySimW()) y2 = m_OurPopulation->SupplySimW();
			for (int i = x1; i < x2; i++) {
				for (int j = y1; j < y2; j++) {
					//if position not empty die (density dependence)
					sum += CheckPosMap( i, j );
				}
			}
			if (sum > __DENDEPCONST) return 0; //die
	}
	/*
		unsigned range=cfg_WalDensityDepMortConst.value();
		int x1 = tx - range;
		int y1 = ty - range;
		if (x1<0) x1=0;
		if (y1<0) y1=0;
		range=(range<<1); // *2
		//if ( GetPosMapDensity(x1,y1,range) > __DENDEPCONST ) {
		if(GetPosMapPositive(x1,y1,range)) {
		//m_OurPopulation->WriteToTestFile(5, m_OurLandscape->SupplyDayInYear());
		return 0;
		}
		}
		*/
	m_Location_x = tx;
	m_Location_y = ty;
	return 2; // Moved OK
}

//---------------------------------------------------------------------------




//---------------------------------------------------------------------------
//                           Spider_Juvenile
//---------------------------------------------------------------------------

Spider_Juvenile::Spider_Juvenile( int x, int y, Landscape * L, Spider_Population_Manager * SpPM ) :
     Spider_Mobile( x, y, L, SpPM ) {
       m_AgeDegrees = 0;
       m_Age = 0;
       // **FN**
       m_pesticide_accum = 0.0;
}

//---------------------------------------------------------------------------


int Spider_Juvenile::CheckPosMap( unsigned x, unsigned y ) {
  return m_OurPopulation->JuvPosMap->GetMapValue( x, y );
}

//---------------------------------------------------------------------------


inline bool Spider_Juvenile::GetPosMapPositive( unsigned x, unsigned y, unsigned range ) {
  return m_OurPopulation->JuvPosMap->GetMapPositive(x,y,range);
}
//---------------------------------------------------------------------------

/*
inline bool Spider_Juvenile::GetPosMapPositiveB( unsigned x, unsigned y, unsigned range ) {
  return m_OurPopulation->JuvPosMap->GetMapPositiveB(x,y,range);
}
*/
//---------------------------------------------------------------------------

int Spider_Juvenile::GetPosMapDensity( unsigned x, unsigned y, unsigned range ) {
  return m_OurPopulation->JuvPosMap->GetMapDensity(x,y,range);
}

//---------------------------------------------------------------------------


int Spider_Egg::CheckJuvPosMap( unsigned x, unsigned y ) {
  return m_OurPopulation->JuvPosMap->GetMapValue( x, y );
}

//---------------------------------------------------------------------------


void Spider_Juvenile::ClearPosMap( unsigned x, unsigned y ) {
  return m_OurPopulation->JuvPosMap->ClearMapValue( x, y );
}

//---------------------------------------------------------------------------

void Spider_Juvenile::SetPosMap( unsigned x, unsigned y ) {
	m_OurPopulation->JuvPosMap->SetMapValue( x, y );
}

//---------------------------------------------------------------------------


void Spider_Juvenile::BeginStep() {
#ifdef __CJTDebug_5
  if ( IsAlive() != 0x0DEADC0DE ) DEADCODEError();
#endif
  CheckManagement(); // This can lead to death, or movement or no change
  // CJT 31-01-2007 if ( ( m_Age++ > 6 ) && ( m_Age < 10 ) ) {
  if ( m_Age++ == 7 )  {
    ClearPosMap( m_Location_x, m_Location_y );
    // Seventh day of life so check for density dependent dispersal
/*
    int x1, y1, x2, y2;
    // Define extent
    x1 = m_Location_x - cfg_JuvDensityDepMortConst.value();
    x2 = m_Location_x + cfg_JuvDensityDepMortConst.value();
    y1 = m_Location_y - cfg_JuvDensityDepMortConst.value();
    y2 = m_Location_y + cfg_JuvDensityDepMortConst.value();
    if ( x1 < 0 ) x1 = 0;
    if ( y1 < 0 ) y1 = 0;
    if ( x2 > m_OurPopulation->SupplySimW() ) x2 = m_OurPopulation->SupplySimW();
    if ( y2 > m_OurPopulation->SupplySimW() ) y2 = m_OurPopulation->SupplySimW();
    int sum = 0;
    int p;
    for ( int i = x1; i < x2; i++ ) {
      for ( int j = y1; j < y2; j++ ) {
        //if position not empty die (density dependence)
        p = CheckPosMap( i, j );
        sum += p;
        //           if (sum>0) break;
      }
      //           if (sum>0) break;
    }
    if ( sum > __DENDEPCONST ) {
      m_CurrentStateNo = -1; // Die
      m_StepDone = true;
	  return;
    }
*/
	unsigned int range=cfg_JuvDensityDepMortConst.value();
	// NB these two must be signed!
	int x1 = m_Location_x - range;
	int y1 = m_Location_y - range;
	int y2 = m_Location_y + range;
	if (y2>m_OurPopulation->SimH) range-=(++y2-m_OurPopulation->SimH);
	if (x1<0) x1=0;
	if (y1<0) y1=0;
	range=(range<<1); // *2
	if (GetPosMapPositive(x1,y1,range)) {
//	if (GetPosMapPositiveB(x1,y1,range)) {
      m_CurrentStateNo = -1; // Die
      m_StepDone = true;
	  return;
	}
  }
  /*
 	unsigned range=cfg_JuvDensityDepMortConst.value();
	int x1 = m_Location_x - range;
	int y1 = m_Location_y - range;
	if (x1<0) x1=0;
	if (y1<0) y1=0;
	range=(range<<1); // *2
	//if ( GetPosMapDensity(x1,y1,range) > __DENDEPCONST ) {
	if(GetPosMapPositive(x1,y1,range)) {
		//m_OurPopulation->WriteToTestFile(5, m_OurLandscape->SupplyDayInYear());
        m_CurrentStateNo = -1; // Die
        m_StepDone = true;
        // Remove my spot from map
		return;
	}
	*/
    SetPosMap( m_Location_x, m_Location_y );
}

//---------------------------------------------------------------------------


void Spider_Juvenile::Step() {
  if ( m_StepDone || m_CurrentStateNo == -1 ) return;
#ifdef __CJTDebug_5
  if ( IsAlive() != 0x0DEADC0DE ) DEADCODEError();
#endif
  switch ( CurrentSpState ) {
    case tosps_Initiation: // Initial
      CurrentSpState = tosps_JDevelop;
    break;

    case tosps_JDevelop:
      // The last thing done each time step
      // next behaviour is either death, maturation, assess habitat or move
      switch ( st_Develop() ) {
        case 0:
          //      m_OurPopulation -> WriteToTestFile(1);
          CurrentSpState = tosps_Dying;
        break;
        case 1:
          //m_OurPopulation->WriteToTestFile(3, m_OurLandscape->SupplyDayInYear());
          CurrentSpState = tosps_Mature;
        break;
        case 2:
          CurrentSpState = tosps_JAssessHabitat;
        break;
      }
      if ( random( 100 ) < ( DispersalChance[ m_BadHabitatDays ] ) ) {
        CurrentSpState = tosps_JMove;
      }
      m_StepDone = true;
    break;

    case tosps_JAssessHabitat:
      // Is the first thing a spider does if not moving or dying or maturing
      // Results in either death or development or movment
      switch ( st_AssessHabitat() ) {
        case 0:
          CurrentSpState = tosps_Dying;
        break;
        case 1:
          m_MustBalloon = true;
          CurrentSpState = tosps_JMove;
        break;
        case 2:
        case 3:
          AddToStarvationDays();
        case 4:
          CurrentSpState = tosps_JDevelop;
        break;
      }
    break;

    case tosps_JMove:
      // Just a switch which determines whether to balloon or walk
      if ( !m_OurPopulation->Walking() ) CurrentSpState = tosps_JBalloon; else
        CurrentSpState = tosps_JWalk;
    break;

    case tosps_JBalloon:
      // Results in either death or to Develop
      // First clear the old position
		this->ClearPosMap( m_Location_x, m_Location_y );
      switch ( st_Balloon() ) {
        case 0:
          CurrentSpState = tosps_Dying;
        break;
        case 1: // Could not move to a new place
          SetPosMap( m_Location_x, m_Location_y );
          CurrentSpState = tosps_JDevelop;
        break;
        case 2: // Moved to a new place
          m_MustBalloon = false;
          SetPosMap( m_Location_x, m_Location_y );
          CurrentSpState = tosps_JDevelop;
        break;
      }
    break;

    case tosps_JWalk:
      // Results in either death or to Develop
      m_OurPopulation->JuvPosMap->ClearMapValue( m_Location_x, m_Location_y );
      switch ( st_Walk() ) {
        case 0:
          CurrentSpState = tosps_Dying;
        break;
        case 1: //Could not move
        case 2: // Moved to a new place
          CurrentSpState = tosps_JDevelop;
          m_OurPopulation->JuvPosMap->SetMapValue( m_Location_x, m_Location_y );
        break;
      }
    break;

    case tosps_Mature:
      Maturation();
      m_OurPopulation->JuvPosMap->ClearMapValue( m_Location_x, m_Location_y );
      m_CurrentStateNo = -1;
      m_StepDone = true;
    break;

    case tosps_Dying:
      m_OurPopulation->JuvPosMap->ClearMapValue( m_Location_x, m_Location_y );
      m_CurrentStateNo = -1;
      m_StepDone = true;
    break;

    default:
      m_OurLandscape->Warn( "Spider Juvenile - unknown state", NULL );
      exit( 1 );
  }
}

//---------------------------------------------------------------------------

void Spider_Juvenile::Maturation() {
  bool done = false;
  struct_Spider * aps;
  aps = new struct_Spider;
  aps->SpPM = m_OurPopulation;
  aps->L = m_OurLandscape;
  int x_ex, y_ex, tx, ty;
  if ( m_Location_x < 5 ) x_ex = m_Location_x; else x_ex = 5;
  if ( m_Location_y < 5 ) y_ex = m_Location_y; else y_ex = 5;
  for ( int a = 0; a < y_ex; a++ ) {
    for ( int b = 0; b < x_ex; b++ ) {
      tx = m_Location_x - b;
      ty = m_Location_y - a;
      TTypesOfLandscapeElement ele = m_OurLandscape->SupplyElementType( tx, ty );
      switch ( ele ) {
        // Edit this list to add more impossible element types
        case tole_Track:
        case tole_SmallRoad:
        case tole_LargeRoad:
		case tole_Pond:
		case tole_Freshwater:
        case tole_Saltwater:
        case tole_River:
        break;
        default:
          // Must write to the position map
          if ( m_OurPopulation->AdultPosMap->GetMapValue( tx, ty ) == 0 ) {
            m_OurPopulation->AdultPosMap->SetMapValue( tx, ty );
            aps->x = tx;
            aps->y = ty;
            m_OurPopulation->CreateObjects( 2, this, aps, 1 );
            done = true;
            // m_OurPopulation->WriteToTestFile(m_OurLandscape->SupplyDayInYear());
          break;
          }
      }
      if ( done == true ) break;
    }
    if ( done == true ) break;
  }
  delete aps;
}

//---------------------------------------------------------------------------

bool Spider_Juvenile::StarvationMort() {
	if (random(10000)< m_OurPopulation->SupplyMort(0,m_BadHabitatDays)) return true;
	return false; // default don't die
}
//---------------------------------------------------------------------------

void Spider_Juvenile::SpecialWinterMort() {
	if (  (m_AgeDegrees/3) < (cfg_JuvDevelConst2.value()/4) ) this->CurrentSpState=tosps_Dying;
}
//---------------------------------------------------------------------------

int Spider_Juvenile::st_Develop() {
  if ( random( 10000 ) < m_OurPopulation->SupplyJuvMort() ) return 0;
  /* Starvation removed because it happens too rarely if (StarvationMort()) return 0; // Dying */

  switch ( AssessFood() ) {
    case 0: // No Food
      // No development
      AddToStarvationDays();
    break;
      // Not much food
    case 1:
      m_AgeDegrees += m_OurPopulation->SupplyJuvDegrees_poor();
      m_BadHabitatDays = 0;
    break;
    case 2:
      m_AgeDegrees += m_OurPopulation->SupplyJuvDegrees_intermediate();
      m_BadHabitatDays = 0;
    break;
    case 3: // Lots of food
      m_AgeDegrees += m_OurPopulation->SupplyJuvDegrees_good();
      m_BadHabitatDays = 0;
    break;
  }
  if ( m_AgeDegrees > cfg_JuvDevelConst2.value() ) {
    return 1; // go to mature
  } else
    return 2; // carry on developing
}
//---------------------------------------------------------------------------


bool Spider_Juvenile::OnFarmEvent( FarmToDo event ) {
  switch ( event ) {
    case sleep_all_day:
    break;
    case autumn_plough:
      if ( random( 1000 ) < cfg_Juv_PloughMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case autumn_harrow:
      if ( random( 1000 ) < cfg_Juv_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case autumn_roll:
      if ( random( 1000 ) < cfg_Juv_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case autumn_sow:
      if ( random( 1000 ) < cfg_Juv_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case winter_plough:
      if ( random( 1000 ) < cfg_Juv_PloughMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case deep_ploughing:
      if ( random( 1000 ) < cfg_Juv_PloughMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case spring_plough:
      if ( random( 1000 ) < cfg_Juv_PloughMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case spring_harrow:
      if ( random( 1000 ) < cfg_Juv_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case spring_roll:
      if ( random( 1000 ) < cfg_Juv_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case spring_sow:
      if ( random( 1000 ) < cfg_Juv_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case fp_npks:
    break;
    case fp_npk:
    break;
    case fp_pk:
    break;
    case fp_liquidNH3:
    break;
    case fp_slurry:
    break;
    case fp_manganesesulphate:
    break;
    case fp_manure:
    break;
    case fp_greenmanure:
    break;
    case fp_sludge:
    break;
    case fa_npk:
    break;
    case fa_pk:
    break;
    case fa_slurry:
    break;
    case fa_ammoniumsulphate:
    break;
    case fa_manure:
    break;
    case fa_greenmanure:
    break;
    case fa_sludge:
    break;
    case herbicide_treat:
    break;
    case growth_regulator:
    break;
    case fungicide_treat:
    break;
    case glyphosate:
	case product_treat:
	  break;
    case insecticide_treat:
      if ( random( 1000 ) < cfg_Juvenile_InsecticideApplication.value() ) CurrentSpState = tosps_Dying;
    break;
    case syninsecticide_treat:
      if ( random( 1000 ) < cfg_Juvenile_SynInsecticideApplication.value() ) CurrentSpState = tosps_Dying;
    break;
    case molluscicide:
    break;
    case row_cultivation:
      if ( random( 1000 ) < cfg_Juv_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case strigling:
      if ( random( 1000 ) < cfg_Juv_StriglingMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case hilling_up:
      if ( random( 1000 ) < cfg_Juv_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case water:
    break;
    case swathing:
      if ( random( 1000 ) < cfg_Juv_HarvestMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case harvest:
      if ( random( 1000 ) < cfg_Juv_HarvestMort.value() ) CurrentSpState = tosps_Dying;
      if ( random( 1000 ) < cfg_Juv_HarvestBalloon.value() ) m_MustBalloon = true;
    break;
    case cattle_out:
      if ( random( 1000 ) < cfg_Juv_GrazingBalloon.value() ) m_MustBalloon = true;
    break;
    case cut_to_hay:
      if ( random( 1000 ) < cfg_Juv_HarvestMort.value() ) CurrentSpState = tosps_Dying;
      if ( random( 1000 ) < cfg_Juv_HarvestBalloon.value() ) m_MustBalloon = true;
    break;
    case cut_to_silage:
      if ( random( 1000 ) < cfg_Juv_HarvestMort.value() ) CurrentSpState = tosps_Dying;
      if ( random( 1000 ) < cfg_Juv_HarvestBalloon.value() ) m_MustBalloon = true;
    break;
    case straw_chopping:
      if ( random( 1000 ) < cfg_Juv_HarvestMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case hay_turning:
      if ( random( 1000 ) < cfg_Juv_HarvestMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case hay_bailing:
      if ( random( 1000 ) < cfg_Juv_HarvestMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case flammebehandling:
      if ( random( 1000 ) < cfg_Juv_FireMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case stubble_harrowing:
      if ( random( 1000 ) < cfg_Juv_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case autumn_or_spring_plough:
      if ( random( 1000 ) < cfg_Juv_PloughMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case burn_straw_stubble:
      if ( random( 1000 ) < cfg_Juv_FireMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case mow:
      if ( random( 1000 ) < cfg_Juv_HarvestMort.value() ) CurrentSpState = tosps_Dying;
      if ( random( 1000 ) < cfg_Juv_HarvestBalloon.value() ) m_MustBalloon = true;
    break;
    case cut_weeds:
      if ( random( 1000 ) < cfg_Juv_HarvestMort.value() ) CurrentSpState = tosps_Dying;
      if ( random( 1000 ) < cfg_Juv_HarvestBalloon.value() ) m_MustBalloon = true;
    break;
    case pigs_out:
      if ( random( 1000 ) < cfg_Juv_GrazingMort.value() ) CurrentSpState = tosps_Dying;
      if ( random( 1000 ) < cfg_Juv_GrazingBalloon.value() ) m_MustBalloon = true;
    break;
    case strigling_sow:
      if ( random( 1000 ) < cfg_Juv_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case trial_insecticidetreat:
      if ( random( 1000 ) < cfg_PesticideTrialJuvenileTreatmentMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case trial_toxiccontrol:
      if ( random( 1000 ) < cfg_PesticideTrialJuvenileToxicMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case trial_control:
    break;
      // **FN**
    default:
    break;
  }
return true;
}

//---------------------------------------------------------------------------




//---------------------------------------------------------------------------
//                           Spider_Adult
//---------------------------------------------------------------------------

Spider_Adult::Spider_Adult( int x, int y, Landscape * L, Spider_Population_Manager * SpPM ) : Spider_Mobile( x, y, L, SpPM ) {
}

//---------------------------------------------------------------------------


int Spider_Adult::GetPosMapDensity( unsigned x, unsigned y, unsigned range ) {
  return m_OurPopulation->AdultPosMap->GetMapDensity(x,y,range);
}

//---------------------------------------------------------------------------


bool Spider_Adult::GetPosMapPositive( unsigned x, unsigned y, unsigned range ) {
  return m_OurPopulation->AdultPosMap->GetMapPositive(x,y,range);
}
//---------------------------------------------------------------------------


/*
inline bool Spider_Adult::GetPosMapPositiveB( unsigned x, unsigned y, unsigned range ) {
  return m_OurPopulation->AdultPosMap->GetMapPositiveB(x,y,range);
}
*/
//---------------------------------------------------------------------------

int Spider_Adult::CheckPosMap( unsigned x, unsigned y ) {
  return m_OurPopulation->AdultPosMap->GetMapValue( x, y );
}

//---------------------------------------------------------------------------

void Spider_Adult::ClearPosMap( unsigned x, unsigned y ) {
  m_OurPopulation->AdultPosMap->ClearMapValue( x, y );
}

//---------------------------------------------------------------------------

void Spider_Adult::SetPosMap( unsigned x, unsigned y ) {
	m_OurPopulation->AdultPosMap->SetMapValue( x, y );
}

//---------------------------------------------------------------------------

void Spider_Adult::BeginStep() {
  // Should never be anything here
}

//---------------------------------------------------------------------------


void Spider_Adult::Step() {
  // Should never be anything here
}

//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//                           Spider_Female
//---------------------------------------------------------------------------

Spider_Female::Spider_Female( int x, int y, Landscape * L, Spider_Population_Manager * SpPM ) : Spider_Adult( x, y, L, SpPM ) {
  m_EggSacDegrees = 0;
  m_lifespan = 0;
  m_EggsProduced = 0;
  // **FN**
  m_pesticide_accum = 0.0;
}

//---------------------------------------------------------------------------


void Spider_Female::BeginStep() {
#ifdef __CJTDebug_5
  if ( IsAlive() != 0x0DEADC0DE ) DEADCODEError();
#endif
  // Can result in death or movement or no change
  // if we run out of life we die
  m_lifespan++;
  if ( m_lifespan == 365 ) {
    m_OurPopulation->AdultPosMap->ClearMapValue( m_Location_x, m_Location_y );
    m_CurrentStateNo = -1;
    m_StepDone = true;
  } else {
    if ( random( 10000 ) < cfg_DailyFemaleMort.value() ) {
      m_OurPopulation->AdultPosMap->ClearMapValue( m_Location_x, m_Location_y );
      m_CurrentStateNo = -1;
      m_StepDone = true;
    } else {
      /* Commented out to because it is never used under DK conditions   - too wet
      float biomass=m_OurLandscape->SupplyVegBiomass(m_Location_x, m_Location_y); int index;
      if (biomass>cfg_mediumplantbiomass.value()) index=2; else if (biomass<=cfg_lowplantbiomass.value()) index=0;
      else index=1; if (m_OurPopulation->SupplyTodaysDroughtSc(index)) { m_droughtCounter++;
      if (random(100)<DroughtMort[m_droughtCounter]) { CurrentSpState=tosps_Dying; m_StepDone=true; } }
      else m_droughtCounter=0; */
      CheckManagement();
    }
  }
}

//---------------------------------------------------------------------------


void Spider_Female::Step() {
  if ( m_StepDone || m_CurrentStateNo == -1 ) return;
#ifdef __CJTDebug_5
  if ( IsAlive() != 0x0DEADC0DE ) DEADCODEError();
#endif
  switch ( CurrentSpState ) {
    case tosps_Initiation: // Initial
      CurrentSpState = tosps_FAssessHabitat;
      m_StepDone = true;
    break;

    case tosps_Reproduce:
      // The last thing done each time step
      // results in either death or AssessHabitat
      switch ( st_Reproduce() ) {
        case 0:
          CurrentSpState = tosps_Dying;
        break;
        case 1:
          CurrentSpState = tosps_FAssessHabitat;
        break;
      }
      m_StepDone = true;
    break;

    case tosps_FAssessHabitat:
      // The first thing done each day unless the CheckManagement says Move
      switch ( st_AssessHabitat() ) {
        case 0:
          CurrentSpState = tosps_Dying;
        break;
        case 1:
          CurrentSpState = tosps_FMove;
        break;
        case 2:
          AddToStarvationDays();
          CurrentSpState = tosps_FMove;
        break;
        case 3:
          AddToStarvationDays();
        case 4:
          CurrentSpState = tosps_Reproduce;
        break;
      }
      if ( random( 100 ) < ( DispersalChance[ m_BadHabitatDays ] ) ) {
        CurrentSpState = tosps_FMove;
      }
    break;

    case tosps_FMove:
      // Just a switch to decide whether to balloon or walk
      // Can arrive here from CheckManagement or AssessHabitat
      if ( !m_OurPopulation->Walking() ) CurrentSpState = tosps_FBalloon; else
        CurrentSpState = tosps_FWalk;
    break;

    case tosps_FBalloon:
      // First clear the old position
      m_OurPopulation->AdultPosMap->ClearMapValue( m_Location_x, m_Location_y );
      switch ( st_Balloon() ) {
        case 0:
          CurrentSpState = tosps_Dying;
          m_StepDone = true;
        break;
        case 1: //Could not move
          CurrentSpState = tosps_Reproduce;
          m_OurPopulation->AdultPosMap->SetMapValue( m_Location_x, m_Location_y );
        break;
        case 2: // Moved to a new place
          m_MustBalloon = false;
          CurrentSpState = tosps_Reproduce;
          m_OurPopulation->AdultPosMap->SetMapValue( m_Location_x, m_Location_y );
        break;
      }
    break;

    case tosps_FWalk:
      m_OurPopulation->AdultPosMap->ClearMapValue( m_Location_x, m_Location_y );
      switch ( st_Walk() ) {
        case 0:
          CurrentSpState = tosps_Dying;
          m_StepDone = true;
        break;
        case 1: //Could not move
        case 2: // Moved to a new place
          CurrentSpState = tosps_Reproduce;
          m_OurPopulation->AdultPosMap->SetMapValue( m_Location_x, m_Location_y );
        break;
      }
    break;

    case tosps_Dying:
      m_OurPopulation->AdultPosMap->ClearMapValue( m_Location_x, m_Location_y );
      m_CurrentStateNo = -1;
      m_StepDone = true;
    break;

    default:
      m_OurLandscape->Warn( "Spider Female - unknown state", NULL );
      exit( 1 );
  }
}

//---------------------------------------------------------------------------



bool Spider_Female::OnFarmEvent( FarmToDo event ) {
  switch ( event ) {
    case sleep_all_day:
    break;
    case autumn_plough:
      if ( random( 1000 ) < cfg_Adult_PloughMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case autumn_harrow:
      if ( random( 1000 ) < cfg_Adult_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case autumn_roll:
      if ( random( 1000 ) < cfg_Adult_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case autumn_sow:
      if ( random( 1000 ) < cfg_Adult_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case winter_plough:
      if ( random( 1000 ) < cfg_Adult_PloughMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case deep_ploughing:
      if ( random( 1000 ) < cfg_Adult_PloughMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case spring_plough:
      if ( random( 1000 ) < cfg_Adult_PloughMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case spring_harrow:
      if ( random( 1000 ) < cfg_Adult_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case spring_roll:
      if ( random( 1000 ) < cfg_Adult_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case spring_sow:
      if ( random( 1000 ) < cfg_Adult_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case fp_npks:
    break;
    case fp_npk:
    break;
    case fp_pk:
    break;
    case fp_liquidNH3:
    break;
    case fp_slurry:
    break;
    case fp_manganesesulphate:
    break;
    case fp_manure:
    break;
    case fp_greenmanure:
    break;
    case fp_sludge:
    break;
    case fa_npk:
    break;
    case fa_pk:
    break;
    case fa_slurry:
    break;
    case fa_ammoniumsulphate:
    break;
    case fa_manure:
    break;
    case fa_greenmanure:
    break;
    case fa_sludge:
    break;
    case herbicide_treat:
    break;
    case growth_regulator:
    break;
    case fungicide_treat:
    break;
    case glyphosate:
	case product_treat:
	  break;
    case insecticide_treat:
      if ( random( 1000 ) < cfg_Adult_InsecticideApplication.value() ) CurrentSpState = tosps_Dying;
    break;
    case syninsecticide_treat:
      if ( random( 1000 ) < cfg_Adult_SynInsecticideApplication.value() ) CurrentSpState = tosps_Dying;
    break;
    case molluscicide:
    break;
    case row_cultivation:
      if ( random( 1000 ) < cfg_Adult_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case strigling:
      if ( random( 1000 ) < cfg_Adult_StriglingMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case hilling_up:
      if ( random( 1000 ) < cfg_Adult_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case water:
    break;
    case swathing:
      if ( random( 1000 ) < cfg_Adult_HarvestMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case harvest:
      if ( random( 1000 ) < cfg_Adult_HarvestMort.value() ) CurrentSpState = tosps_Dying;
      if ( random( 1000 ) < cfg_Adult_HarvestBalloon.value() ) m_MustBalloon = true;
    break;
    case cattle_out:
      if ( random( 1000 ) < cfg_Adult_GrazingBalloon.value() ) m_MustBalloon = true;
    break;
    case cut_to_hay:
      if ( random( 1000 ) < cfg_Adult_HarvestMort.value() ) CurrentSpState = tosps_Dying;
      if ( random( 1000 ) < cfg_Adult_HarvestBalloon.value() ) m_MustBalloon = true;
    break;
    case cut_to_silage:
      if ( random( 1000 ) < cfg_Adult_HarvestMort.value() ) CurrentSpState = tosps_Dying;
      if ( random( 1000 ) < cfg_Adult_HarvestBalloon.value() ) m_MustBalloon = true;
    break;
    case straw_chopping:
      if ( random( 1000 ) < cfg_Adult_HarvestMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case hay_turning:
      if ( random( 1000 ) < cfg_Adult_HarvestMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case hay_bailing:
      if ( random( 1000 ) < cfg_Adult_HarvestMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case flammebehandling:
      if ( random( 1000 ) < cfg_Adult_FireMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case stubble_harrowing:
      if ( random( 1000 ) < cfg_Adult_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case autumn_or_spring_plough:
      if ( random( 1000 ) < cfg_Adult_PloughMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case burn_straw_stubble:
      if ( random( 1000 ) < cfg_Adult_FireMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case mow:
      if ( random( 1000 ) < cfg_Adult_HarvestMort.value() ) CurrentSpState = tosps_Dying;
      if ( random( 1000 ) < cfg_Adult_HarvestBalloon.value() ) m_MustBalloon = true;
    break;
    case cut_weeds:
      if ( random( 1000 ) < cfg_Adult_HarvestMort.value() ) CurrentSpState = tosps_Dying;
      if ( random( 1000 ) < cfg_Adult_HarvestBalloon.value() ) m_MustBalloon = true;
    break;
    case pigs_out:
      if ( random( 1000 ) < cfg_Adult_GrazingMort.value() ) CurrentSpState = tosps_Dying;
      if ( random( 1000 ) < cfg_Adult_GrazingBalloon.value() ) m_MustBalloon = true;
    break;
    case strigling_sow:
      if ( random( 1000 ) < cfg_Adult_HarrowMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case trial_insecticidetreat:
      if ( random( 1000 ) < cfg_PesticideTrialAdultTreatmentMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case trial_toxiccontrol:
      if ( random( 1000 ) < cfg_PesticideTrialAdultToxicMort.value() ) CurrentSpState = tosps_Dying;
    break;
    case trial_control:
    break;
      // **FN**
    default:
    break;
  }
  return true;
}

//---------------------------------------------------------------------------


int Spider_Female::st_Reproduce() {
  /* Starvation removed because it happens too rarely if (StarvationMort()) return 0; // Die */
  //Get EggSac Degrees
  switch ( AssessFood() ) {
    case 0:
      AddToStarvationDays();
    break;
    case 1:
      m_EggSacDegrees += m_OurPopulation->SupplyEggDegreesPoor();
      m_BadHabitatDays = 0;
    break;
    case 2:
      m_EggSacDegrees += m_OurPopulation->SupplyEggDegreesInt();
      m_BadHabitatDays = 0;
    break;
    case 3:
      m_EggSacDegrees += m_OurPopulation->SupplyEggDegreesGood();
      m_BadHabitatDays = 0;
    break;
  }
  if ( m_EggSacDegrees > cfg_EggProducConst2.value() ) {
    //m_OurPopulation->WriteToTestFile(1, m_OurLandscape->SupplyDayInYear());
    if ( ProduceEggSac() ) {
      ZeroEggSacDegrees();
      if ( m_EggsProduced > cfg_Max_Egg_Production.value() ) {
        return 0; // Die
      }
    }
  }
  return 1;
}

//---------------------------------------------------------------------------

//**per_fjern**
/* bool Spider_Female::StarvationMort() { if (random(10000)< m_OurPopulation->SupplyMort(1,m_BadHabitatDays)) return true;
return false; // default don't die } */
//---------------------------------------------------------------------------


bool Spider_Female::ProduceEggSac() {
  int NoEggs = ErigoneEggsPerSac[ cfg_SpeciesRef.value() ] [ m_OurPopulation->SupplyTodaysMonth() ];
	if ( NoEggs > 0 ) {
		if ( m_OurPopulation->EggPosMap->GetMapValue( m_Location_x, m_Location_y ) ) {
			// /* This section has been deleted because this was not included in the original runs
			//Hence density dependent mort is at a maximum. Future versions should uncomment these lines:
			int tx=m_Location_x;
			int ty=m_Location_y; // Central position filled, check others
			for (int h=0; h<cfg_EggPlacementChanceConst.value(); h++)
			{
				tx=m_Location_x-random(5);
				ty=m_Location_y-random(5);
				if (tx<0) tx=0;
				if (ty<0) ty=0;
				if (!m_OurPopulation->EggPosMap->GetMapValue(tx,ty)) {
					struct_Spider * SS;
					SS = new struct_Spider;
					SS->x     = tx;
					SS->y     = ty;
					SS->L     = m_OurLandscape;
					SS->SpPM  = m_OurPopulation;
					SS->noEggs = NoEggs;
					m_OurPopulation->CreateObjects(0,this,SS,1); m_EggsProduced+=NoEggs;
					m_OurPopulation->EggPosMap->SetMapValue(tx,ty);
					delete SS;
					return true;
				}
			}
		 //*/
			m_EggsProduced += NoEggs; // They are dead but she produced them
			return false;
		} else {
			struct_Spider * SS;
			SS = new struct_Spider;
			SS->x = m_Location_x;
			SS->y = m_Location_y;
			SS->L = m_OurLandscape;
			SS->SpPM = m_OurPopulation;
			SS->noEggs = NoEggs;
			m_OurPopulation->CreateObjects( 0, this, SS, 1 );
			m_MustBalloon=true; //**per** line to prevent females to produce more than one eggsac in one spot
			m_EggsProduced += NoEggs;
			m_OurPopulation->EggPosMap->SetMapValue( m_Location_x, m_Location_y );
			delete SS;
			return true;
		}
	}
	return false;
}
//---------------------------------------------------------------------------


void Spider_Mobile::CopyMyself(int a_sptype) {
  bool done=false;
  struct_Spider * aps;
  aps = new struct_Spider;
  aps->SpPM = m_OurPopulation;
  aps->L = m_OurLandscape;
  int x_ex, y_ex, tx, ty;
  if ( m_Location_x < 5 ) x_ex = m_Location_x; else x_ex = 5;
  if ( m_Location_y < 5 ) y_ex = m_Location_y; else y_ex = 5;
  for ( int a = 0; a < y_ex; a++ ) {
    for ( int b = 0; b < x_ex; b++ ) {
      tx = m_Location_x - b;
      ty = m_Location_y - a;
      TTypesOfLandscapeElement ele = m_OurLandscape->SupplyElementType( tx, ty );
      switch ( ele ) {
        // Edit this list to add more impossible element types
        case tole_Track:
        case tole_SmallRoad:
        case tole_LargeRoad:
		case tole_Pond:
		case tole_Freshwater:
        case tole_Saltwater:
        case tole_River:
        break;
        default:
          // Must write to the position map
          if ( m_OurPopulation->AdultPosMap->GetMapValue( tx, ty ) == 0 ) {
            m_OurPopulation->AdultPosMap->SetMapValue( tx, ty );
            aps->x = tx;
            aps->y = ty;
            m_OurPopulation->CreateObjects( a_sptype, this, aps, 1 );
            done = true;
            // m_OurPopulation->WriteToTestFile(m_OurLandscape->SupplyDayInYear());
          break;
          }
      }
      if ( done == true ) break;
    }
    if ( done == true ) break;
  }
  delete aps;
}
//---------------------------------------------------------------------------


void Spider_Egg::CopyMyself(int) {
	for (int tx=m_Location_x-5; tx<=m_Location_x; tx++) {
  	  for (int ty=m_Location_y-5; ty<=m_Location_y; ty++) {
		if (tx<0) tx=0;
		if (ty<0) ty=0;
        if (!m_OurPopulation->EggPosMap->GetMapValue(tx,ty)) {
			struct_Spider * SS = new struct_Spider;
			SS->x     = tx;
			SS->y     = ty;
			SS->L     = m_OurLandscape;
			SS->SpPM  = m_OurPopulation;
			SS->noEggs = m_NoEggs;
			m_OurPopulation->CreateObjects(0,this,SS,1);
			m_OurPopulation->EggPosMap->SetMapValue(tx,ty);
			delete SS;
			return;
		}
	  }
	}
}
//-----------------------------------------------------------------------------







