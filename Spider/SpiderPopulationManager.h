/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
//---------------------------------------------------------------------------
#ifndef SpiderPopulationManagerH
#define SpiderPopulationManagerH
//---------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Forward Declarations

class struct_Spider;
class SimplePositionMap;
class MovementMap;

//---------------------------------------------------------------------------

/**
Enumerator for spider object types
*/
typedef enum {
  spob_Egg, 
  spob_Juvenile, 
  spob_Female 
}
Spider_Object;


class Spider_Population_Manager: public Population_Manager
{
    // Attributes
 protected:
    int m_DailyJuvMort;
    int DispersalDistances [10000];
    int StarvationMortChance[2][40];
    double BallooningHrs[52*7];
    int m_DaysSinceRain;
    int m_TodaysMonth;
    int m_TodaysDroughtScore [3];
    double m_TodaysBallooningTime;
    int m_EggSacSpread;
    int m_DoubleEggSacSpread;
    double m_BallooningMortalityPerMeter;
    bool EggProdThresholdPassed;
    int m_WindDirection;
    bool m_BallooningWeather;
    double m_EggDegrees[365];
    double m_JuvDegreesGood;
    double m_JuvDegreesIntermediate;
    double m_JuvDegreesPoor;
    double m_EggProdDDegsGood;
    double m_EggProdDDegsInt;
    double m_EggProdDDegsPoor;
    bool m_WalkingOnly;
    bool m_MinWalkTemp;
	// Methods
	void Catastrophe();
 public:
    MovementMap* m_MoveMap;
    inline int SupplyEggSacSpread() {return m_EggSacSpread;}
    inline int SupplyDoubleEggSacSpread() {return m_DoubleEggSacSpread;}
    double SupplyBallooningMortalityPerMeter()
                                      {return m_BallooningMortalityPerMeter;}
    int SupplyDaysSinceRain(){return m_DaysSinceRain;}
    int SupplyTodaysMonth(){return m_TodaysMonth;}
    int SupplyTodaysDroughtSc(int index) { return m_TodaysDroughtScore [index]; }
    int SupplyMort(int type, int days)
    {
      return StarvationMortChance[type][days];
    }
	int SupplyJuvMort() { return m_DailyJuvMort; }
	int SupplyDispDist(int chance)
    {
      return DispersalDistances[chance];
    }
    double SupplyBTime(int day)
    {
      return BallooningHrs[day];
    }
    Spider_Population_Manager(Landscape* p_L);
    virtual void Init (void);
    virtual void DoFirst (void);
	virtual ~Spider_Population_Manager();
    void CreateObjects(int ob_type, TAnimal *pvo, struct_Spider * data,int number);
  // Other interface functions
	virtual void TheAOROutputProbe();
    virtual void TheRipleysOutputProbe(FILE* a_prb);
    bool InSquare(int p_x, int p_y, int p_sqx, int p_sqy, int p_range);
    double SupplyBTimeToday() {return m_TodaysBallooningTime;}
    int SupplyWindDirection() {return m_WindDirection;}
    bool IsBallooningWeather() {return m_BallooningWeather;};
    double SupplyEggDegreesPoor() {return m_EggProdDDegsPoor;}
    double SupplyEggDegreesInt() {return m_EggProdDDegsInt;}
    double SupplyEggDegreesGood() {return m_EggProdDDegsGood;}
    double SupplyJuvDegrees_good() {return m_JuvDegreesGood;}
    double SupplyJuvDegrees_intermediate() {return m_JuvDegreesIntermediate;}
    double SupplyJuvDegrees_poor() {return m_JuvDegreesPoor;}
    double SupplyEggDevelDegrees(int day) {return m_EggDegrees[day];}
    bool CheckHumidity(int /* x */ , int /* y */) {return true; } // TODO
    bool Walking() {return m_WalkingOnly;}
    bool SupplyMinWalkTemp() {return m_MinWalkTemp;}
  //Attributes
    SimplePositionMap* EggPosMap;
    SimplePositionMap* JuvPosMap;
    SimplePositionMap* AdultPosMap;
protected:
// Special functionality
#ifdef __RECORD_RECOVERY_POLYGONS
   /** \brief Special pesticide recovery code */
   void RecordRecoveryPolygons();
   int m_RecoveryPolygons[101];
   int m_RecoveryPolygonsC[101];
#endif
};
//---------------------------------------------------------------------------



#endif
