/*
*******************************************************************************************************
Copyright (c) 2012, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file NEWANIMAL.h
\brief <B>The main source code for all predator lifestage and population manager classes</B>
*/
/**  \file NEWANIMAL.h
Version of  2 November 2012 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------
#ifndef NEWANIMALH
#define NEWANIMALH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

class NEWANIMAL;
class NEWANIMAL_Population_Manager;

//------------------------------------------------------------------------------
/**
Used for the population manager's list of NEWANIMAL
*/
//typedef vector<NEWANIMAL*> TListOfNEWANIMAL;
//---------------------------------------------------------------------------

/**
NEWANIMAL like other ALMaSS animals work using a state/transition concept.
These are the NEWANIMAL behavioural states, these need to be altered, but some are here just to show how they should look.
*/
typedef enum
{
      toNEWANIMALs_InitialState=0,
      toNEWANIMALs_Develop,
      toNEWANIMALs_Move,
      toNEWANIMALs_Die
} TTypeOfNEWANIMALState;


class NEWANIMAL : public TAnimal
{
   /**
   A NEWANIMAL must have some simple functionality:
   Inititation
   Movement
   Dying

   Inherits m_Location_x, m_Location_y, m_OurLandscape from TAnimal
   NB All areas are squares of size length X length
   */

protected:
   /** \brief Variable to record current behavioural state */
   TTypeOfNEWANIMALState m_CurrentNAState;
   /** \brief A unique ID number for this species */
   unsigned m_SpeciesID;
   /** \brief A typical member variable - this one determines the max dispersal distance */
   unsigned m_DispersalMax;
   /** \brief This is a time saving pointer to the correct population manager object */
   NEWANIMAL_Population_Manager*  m_OurPopulationManager;
public:
   /** \brief NEWANIMAL constructor */
   NEWANIMAL(int p_x, int p_y,Landscape* p_L, NEWANIMAL_Population_Manager* p_NPM);
   /** \brief NEWANIMAL destructor */
   ~NEWANIMAL();
   /** \brief Behavioural state development */
   TTypeOfNEWANIMALState st_Develop( void );
   /** \brief Behavioural state movement */
   TTypeOfNEWANIMALState st_Movement( void );
   /** \brief Behavioural state dying */
   void st_Dying( void );
   /** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
   virtual void BeginStep(void) {} // NB this is not used in the NEWANIMAL code
   /** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
   virtual void Step(void);
   /** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
   virtual void EndStep(void) {} // NB this is not used in the NEWANIMAL code
   /** \brief A typical interface function - this one returns the SpeciesID number as an unsigned integer */
   unsigned GetSpeciesID() { return m_SpeciesID; }
   /** \brief A typical interface function - this one returns the SpeciesID number as an unsigned integer */
   void SetSpeciesID(unsigned a_spID) { m_SpeciesID = a_spID; }
};

#endif
