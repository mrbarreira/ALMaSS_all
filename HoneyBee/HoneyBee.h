/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file HoneyBee.h
\brief <B>The main header file for all HoneyBee lifestage classes</B>
Version of  10th May 2017 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------
#ifndef HONEYBEEH
#define HONEYBEEH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

//class HoneyBee_Base;
//class struct_HoneyBee;
//class BeeVirusInfectionLevels;
#include "honeybeetypes.h"
//#include "BeeVirusInfectionLevels.h"
#include "HoneyBee_Colony.h"
#include "hive.h"

//#include "../BatchALMaSS/PopulationManager.h"
//class Hive;


/**
HoneyBee like other ALMaSS animals work using a state/transition concept.
These are the HoneyBee behavioural states.
*/
typedef enum
{
	toHoneyBeeS_InitialState = 0,
	toHoneyBeeS_Develop,
    toHoneyBeeS_Progress,
    toHoneyBeeS_Decide,
    toHoneyBeeS_Move,
    toHoneyBeeS_Die,
    toHoneyBeeS_ForagePollen,
    toHoneyBeeS_StorePollen,
    toHoneyBeeS_ForageNectar,
    toHoneyBeeS_StoreNectar,
    toHoneyBeeS_foobar
} TTypeOfHoneyBeeState;

/** \brief A small extendable class to hold an individual's virus infection load. */

class HoneyBee_Base : public TAnimal
{
	/**
	A HoneyBee_Base must have some simple functionality:
	Inititation
	Develop
	Dying

	Inherits m_Location_x, m_Location_y, m_OurLandscape from TAnimal
	NB All areas are squares of size length X length

	This class should never actually be used to create an object, it is a base class from which all other bee types are defined.
	*/

protected:
	// Attributes - 
	// Static attributes
    int m_Location_z;

	/** \brief static attribute to record colony time on a 24 hour clock in cfg_BeeMinimumTimeStep minute timesteps*/
	static unsigned m_ColonyTime;
	/** \brief Variable to record current behavioural state */
	TTypeOfHoneyBeeState m_CurrentHBState;
	/** \brief This is a time saving pointer to the correct population manager object */
	HoneyBee_Colony*  m_OurPopulationManager;
	/** \brief Holds the age of the bee in days */
	unsigned m_Age;
	// Standard Attributes
    //BeeVirusInfectionLevels m_OurVirusInfection;
    unsigned long id;
    int developmentTime;

public:
	// Methods
	/** \brief HoneyBee_Base constructor */
    HoneyBee_Base(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
	/** \brief HoneyBee_Base destructor */
    /** \brief ReInit for object pool */
    void ReInit(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);

	virtual ~HoneyBee_Base();
    virtual void nextStage() {}
    float mortality() {return 0.0;}
    /** \brief Behavioural state develop */
    TTypeOfHoneyBeeState st_Develop();
    /** \brief Behavioural state develop */
    TTypeOfHoneyBeeState st_Progress(void);
    /** \brief Behavioural state progressing */
	void st_Dying(void);
	/** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	virtual void BeginStep(void) {} // NB this is not used in the HoneyBee_Base code
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
    virtual void Step();
	/** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	virtual void EndStep(void) {} // NB this is not used in the HoneyBee_Base code
	/** \brief Returns the age in days */
	unsigned GetAge() { return m_Age; }
	/** \brief Returns the colony time in minutes since midnight */
	unsigned GetColonyTime() { return m_ColonyTime; }
	/** \brief Sets the colony time in minutes since midnight */
	void SetColonyTime(unsigned a_time) { m_ColonyTime = a_time; }
	/** \brief Adds time to the colony time */
	void AddColonyTime(unsigned a_time) { m_ColonyTime += a_time; m_ColonyTime = m_ColonyTime % (24 * 60); }
	/** \brief For filling in a data structure ready to transfer the objects data to a new object, e.g. when an egg hatches to make a larva */
	void FillHBdata(struct_HoneyBee* a_data);
	/** \brief Set the DWV infection state */
//	void SetDWVInfection(bool a_value) { m_OurVirusInfection.SetDWV(a_value); }
	/** \brief Get the DWV infection state */
//	bool GetDWVInfection() { return m_OurVirusInfection.GetDWV(); }
	/** \brief Set the ABPV infection state */
//	void SetABPVInfection(bool a_value) { m_OurVirusInfection.SetDWV(a_value); }
	/** \brief Get the ABPV infection state */
//	bool GetABPVInfection() { return m_OurVirusInfection.GetABPV(); }
    static const int myID=-1;
    bool inHive;

    void SetZ(int a_z)
    {
      m_Location_z = a_z;
    }

    int Supply_m_Location_z()
    {
      return m_Location_z;
    }

    int getX()
    {
        return Supply_m_Location_x();
    }

    int getY()
    {
        return Supply_m_Location_y();
    }

    int getZ()
    {
        return Supply_m_Location_z();
    }

    Hive * getHive();
};

class HoneyBee_WorkerLarva;
class HoneyBee_WorkerPupa;
class HoneyBee_Worker;

class HoneyBee_WorkerEgg : public HoneyBee_Base
{
	/**
	Extends the HoneyBee_Egg for specialisms related to being a worker egg
	*/
public:
	/** \brief HoneyBee_Egg constructor */
    HoneyBee_WorkerEgg(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
	/** \brief ReInit for object pool */
    //void ReInit(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
	/** \brief Behavioural state for egg hatch */
    //virtual TTypeOfHoneyBeeState st_Hatch(void);
    static const int myID=0;
    static const CellType cellType=CellType::Worker_Egg;

    virtual void nextStage() {
      m_OurPopulationManager->add<HoneyBee_WorkerEgg, HoneyBee_WorkerLarva>(*this);
    }
};


class HoneyBee_WorkerLarva : public HoneyBee_Base
{
	/**
	Extends the HoneyBee_Larva for specialisms related to being a worker larva
	*/
public:
	/** \brief HoneyBee_WorkerLarva constructor */
    HoneyBee_WorkerLarva(const int a_x, const int a_y,const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
	/** \brief ReInit for object pool */
    void ReInit(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
	/** \brief Behavioural state for pupation */
    //virtual TTypeOfHoneyBeeState st_Pupate(void);
    /** \brief Behavioural state for larval development - this overrides the egg st_Develop */
    //virtual TTypeOfHoneyBeeState st_Develop(void);
    //virtual void Step();
    static const int myID=1;
    static const CellType cellType=CellType::Worker_Larva;

    virtual void nextStage() {
      m_OurPopulationManager->add<HoneyBee_WorkerLarva,HoneyBee_WorkerPupa>(*this);
    }
};


class HoneyBee_WorkerPupa : public HoneyBee_Base
{
    /**
    * Extends the HoneyBee_Egg for specialisms related to being a larva.
    * The main differences between eggs and larvae is that the larvae needs additional information on construction w.r.t. viruses, and we need pupation not hatch.
    */
public:
    /** \brief HoneyBee_WorkerPupa constructor */
    HoneyBee_WorkerPupa(int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
    /** \brief ReInit for object pool */
    void ReInit(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
    /** \brief Behavioural state for pupation */
    //virtual TTypeOfHoneyBeeState st_Emerge(void); //{ return toHoneyBeeS_foobar; }
    //virtual TTypeOfHoneyBeeState st_Develop(void);
    //virtual void Step();
    static const int myID=2;
    static const CellType cellType=CellType::Worker_Pupa;

    virtual void nextStage() {
      m_OurPopulationManager->add<HoneyBee_WorkerPupa,HoneyBee_Worker>(*this);
    }

};


class HoneyBee_Worker : public HoneyBee_Base
{
    /**
    * Extends the HoneyBee_Egg for specialisms related to being a larva.
    * The main differences between eggs and larvae is that the larvae needs additional information on construction w.r.t. viruses, and we need pupation not hatch.
    */
    int moveSteps;
    int stepCount;
    int forageCounter;
    double forageAmount;

    // Metabolic functions
    double metabolicRate;
    double sugar;
    double protein;

public:
    /** \brief HoneyBee_WorkerPupa constructor */
    HoneyBee_Worker(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
    /** \brief ReInit for object pool */
    void ReInit(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
    virtual void Step();
    virtual void BeginStep();

    TTypeOfHoneyBeeState st_MoveRandom();
    TTypeOfHoneyBeeState st_Decide();
    TTypeOfHoneyBeeState st_ForagePollen();
    TTypeOfHoneyBeeState st_StorePollen();
    TTypeOfHoneyBeeState st_ForageNectar();
    TTypeOfHoneyBeeState st_StoreNectar();

    // Metabolic functions
    void metaboliseAndEat();
    double pollenDemand();
    double sugarDemand();

    static const int myID=3;
    static const CellType cellType=CellType::Empty;
};



///////////////////////////// DRONE ////////////////////////////

class HoneyBee_DroneEgg : public HoneyBee_WorkerEgg
{
    /**
    Extends the HoneyBee_Egg for specialisms related to being a drone egg
    */
public:
    /** \brief HoneyBee_Egg constructor */
    HoneyBee_DroneEgg(const int a_x , const int a_y, const int a_z,  Landscape* p_L, HoneyBee_Colony* p_BPM);
    /** \brief ReInit for object pool */
    void ReInit(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM);
    /** \brief Behavioural state for egg hatch */
    virtual TTypeOfHoneyBeeState st_Hatch(void);
    static const int myID=4;
};


#endif
