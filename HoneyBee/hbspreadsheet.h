#ifndef HB_SPREADSHEET_H
#define HB_SPREADSHEET_H

#include "hbtools.h"
#include <memory>

using FArray2D = blitz::Array<double,2>;
using FArray1D = blitz::Array<double,1>;
using IArray2D = blitz::Array<int,2>;
using IArray1D = blitz::Array<int,1>;

const int NUMBER_OF_JOBS = 17;

enum Jobs
{
  /* Must not contain specific values except for initial zero.
     Used for array acces.*/
  CLEANCELLS = 0,
  CAPBROOD,
  FEEDBROOD,
  WARMBROOD,
  RECEIVENECTAR,
  DEPOSITNECTAR,
  RIPENNECTAR,
  CAPHONEY,
  PACKPOLLEN,
  BUILDCOMB,
  VENTILATE,
  GUARD,
  CLEAN,
  TESTFLIGHTS,
  FORAGENECTAR,
  FORAGEPOLLEN,
  UNCAPHONEY
};

enum Constants
{
  /* Must not contain specific values except for initial zero.
     Used for array acces.*/
  LOWEST_TEMP_FOR_FLYING = 0,
  SUGAR_POOL_PER_BEE,
  WAX_PEAK_PRODUCTION,
  HPG_PRODUCTION,
  SUGAR_POOL_EXTRA_CAPACITY,
  POLLEN_CELLS_PER_BROOD_CELLS,
  HONEY_CELLS_PER_ADULT,
  HPG_PER_FEED,
  WAX_PER_JOB
};

struct JobData
{
  int age;
  bool isSummer;
  bool isDaytime;
  double ambientTemp;
  int numberOfBees;
  int sizeOfBrood;
  float sugarPoolVolume;
  int nHoneyCells;
  int nPollenCells;
  int nBeesExternal;
  int nFreeCells;
  float beeHPGAvailable;
  float beeWaxAvailable;
  Jobs localFlag;
};

class Spreadsheet
{
protected:
  IArray2D * _workerPriority;
  IArray2D * _colonyPriority;
  FArray2D * _constants;

 public:
  Spreadsheet() {}
    
  ~Spreadsheet()
  {
    delete(_workerPriority);
    delete(_colonyPriority);
  }
  void load();
  IArray2D  workerPriority();
  IArray2D  colonyPriority();
  IArray1D getWorkerPriorityLine(int age);
  IArray1D getColonyPriorityLine(int l);
  double getConstant(Constants constant);
  void setGlandularFlags(IArray1D& glandularFlags, const JobData& jobdata);
  void setColonyFlags(IArray1D& colonyFlags, const JobData& jobdata);
  void setLocalFlags(IArray1D& localFlags, const JobData& jobdata);
  int getJobNumber(const JobData& jobdata);
};

#endif
