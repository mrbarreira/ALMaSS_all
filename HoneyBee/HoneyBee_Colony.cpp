/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file HoneyBee_Colony.cpp
Version of  10th May 2017 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------

#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../HoneyBee/BeeVirusInfectionLevels.h"
#include "../HoneyBee/HoneyBee.h"
#include "../HoneyBee/HoneyBee_Colony.h"
#include "blitz/array.h"

//---------------------------------------------------------------------------------------
// Externals
extern MapErrorMsg *g_msg;

//---------------------------------------------------------------------------------------

// Configuration inputs - parameter default values
/** \brief The minimum time step considered for any bees */
CfgInt cfg_BeeMinimumTimeStep("HONEYBEE_MINTIMESTEP", CFG_CUSTOM, 30);
/** \brief The minimum time step considered for any bees */
CfgFloat cfg_EggDailyMortality("HONEYBEE_EGGDAILYMORT", CFG_CUSTOM, 0.001);
/** \brief The minimum time step considered for any bees */
CfgFloat cfg_LarvaDailyMortality("HONEYBEE_LARVADAILYMORT", CFG_CUSTOM, 0.001);

//---------------------------------------------------------------------------------------
// Assign default static member values (these will be changed later).

unsigned HoneyBee_Base::m_ColonyTime = 0;
//double HoneyBee_WorkerEgg::m_EggDailyMortality = 0;
//double HoneyBee_WorkerEgg::m_LarvaDailyMortality = 0;

//---------------------------------------------------------------------------

HoneyBee_Colony::~HoneyBee_Colony (void)
{
    delete hive;
   // Should all be done by the Population_Manager destructor
}
//---------------------------------------------------------------------------

HoneyBee_Colony::HoneyBee_Colony(Landscape* L) : Population_Manager(L)
{
    // Load List of Animal Classes
    m_ListNames[0] = "Worker Egg";
    m_ListNames[1] = "Worker Larva";
    m_ListNames[2] = "Worker Pupa";
    m_ListNames[3] = "Drone Egg";
    m_ListNames[4] = "Drone Larva";
    m_ListNames[5] = "Drone Pupa";
    m_ListNames[6] = "Worker Adult";
    m_ListNames[7] = "Forager";

    m_ListNameLength = 8;
    strcpy(m_SimulationName, "HoneyBee Simulation");
	
    currentID=0;
    hive = new Hive(80,25,10,5.0,20.0,3.0);

    blitz::Array<float,3> xx = getHive()->getX();
    int xrange=xx.shape()[0];
    int yrange=xx.shape()[1];
    int zrange=xx.shape()[2];
    HoneyBee_Worker bee(0,0,0,m_TheLandscape,this);

    for (int i=0; i< 3000; i++) // This will need to be an input variable (config)
	{
        bee.SetX(random(xrange));
        bee.SetY(random(yrange));
        bee.SetZ(random(zrange));
        add<HoneyBee_Worker,HoneyBee_Worker>(bee);
    }
    spreadsheet.load();
}


void HoneyBee_Colony::DoFirst()
{
	/**
	* This method does all housekeeping required by the population manager before the start of the next time-step.
	*
	* Currently this is:\n
	* - Updating colony time
	*/
    //dynamic_cast<HoneyBee_WorkerEgg*>(TheArray[ttohb_Worker][0])->AddColonyTime(cfg_BeeMinimumTimeStep.value());
    layEggs();
    getHive()->step();
    //std::cout << getHive()->dayOfYear() << std::endl;
}

void HoneyBee_Colony::layEggs()
{
    blitz::Array<float,3> temp;
    temp.resize(hive->getTemperatureArray().shape());
    temp=blitz::where(hive->getCellTypeArray()==(int)CellType::Empty, hive->getTemperatureArray(), 0);

    int N=10;
    int x,y,z;

    struct_HoneyBee sp;
    sp.BPM = this;
    sp.L = m_TheLandscape;

    //sp->BPM = this;
    //sp->L = m_TheLandscape;

    HoneyBee_WorkerEgg egg(0,0,0,m_TheLandscape,this);
    for (int i=0,j=0; (i < N) && (j < 1000);j++)
    {
        blitz::TinyVector<int,3> inds;
        inds=blitz::maxIndex(temp);
        x=inds[0];
        y=inds[1];
        z=inds[2];
        //std::cout << "i,j: " << i << ", " << j << ", (x,y,z) = " << "(" << x << "," << y << "," << z << ")" << std::endl;


        if (hive->isFree(x,y,z))
        {
            egg.SetX(x);
            egg.SetY(y);
            egg.SetZ(z);
            //std::cout << "i,j: " << i << ", " << j << ", (x,y,z) = " << "(" << x << "," << y << "," << z << ") *being made*" << std::endl;
            //CreateObjects(ttohb_WorkerEgg,NULL,&sp,1);

            add<HoneyBee_WorkerEgg, HoneyBee_WorkerEgg>(egg);
            temp(x,y,z)=0.0;
            ++i;
        }
    }
    //std::cout << std::endl;
}

/*
blitz::TinyVector<int,3> HoneyBee_Colony::warmest(CellType celltype)
{
    blitz::Array<float,3> temp;
    temp.resize(hive->getTemperatureArray().shape());
    temp=blitz::where(hive->getCellTypeArray()==(int)celltype, hive->getTemperatureArray(), 0);
    blitz::TinyVector<int,3> inds;
    inds=blitz::maxIndex(temp);
    return inds;
}

*/
