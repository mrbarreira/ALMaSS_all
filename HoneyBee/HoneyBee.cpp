/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file HoneyBee.cpp
\brief <B>The main source code for all HoneyBee lifestage classes</B>
Version of  10th May 2017 \n
By Chris J. Topping \n \n
*/

#include <iostream>
#include <fstream>
#include <vector>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../HoneyBee/BeeVirusInfectionLevels.h"
#include "../HoneyBee/HoneyBee.h"
#include "../HoneyBee/HoneyBee_Colony.h"
#include "../BatchALMaSS/BoostRandomGenerators.h"
#include "honeybeeconfigs.h"
#include "hbincludes.h"

extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;
extern MapErrorMsg *g_msg;

//using namespace std;

// Set to 144 for real simulation.
// Set smaller to make stage progression faster for testing
const int TEST_STEP_MULT=144;

inline void HoneyBee_Base::FillHBdata(struct_HoneyBee* a_data) {
    a_data->BPM = m_OurPopulationManager;
    a_data->L = m_OurLandscape;
    a_data->age = m_Age;
    a_data->x = m_Location_x;
    a_data->y = m_Location_y;
    a_data->z = m_Location_z;
   // a_data->BVI = &m_OurVirusInfection;
}

//******************************************************************************************************
//******************************* START HoneyBee_Base Class ********************************************
//******************************************************************************************************

HoneyBee_Base::HoneyBee_Base(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
    : TAnimal( a_x ,  a_y , p_L)
{
	// Assign the pointer to the population manager
    SetZ(a_z);
    developmentTime=999;
    id= m_OurPopulationManager->nextID();
	m_OurPopulationManager = p_BPM;
	m_CurrentHBState = toHoneyBeeS_InitialState;
    getHive()->incBeeCount(a_x,a_y,a_z);
    inHive=true;
}


void HoneyBee_Base::ReInit(const int  a_x, const int  a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
    TAnimal::ReinitialiseObject(a_x,a_y,p_L);
    SetZ(a_z);
    m_Age = 0;
    m_CurrentStateNo=toHoneyBeeS_InitialState;
    m_CurrentHBState=toHoneyBeeS_InitialState;
    id = m_OurPopulationManager->nextID();
}

HoneyBee_Base::~HoneyBee_Base(void)
{
    getHive()->decBeeCount(getX(),getY(),getZ());
}

void HoneyBee_Base::Step(void)
{
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentHBState)
	{
	case toHoneyBeeS_InitialState: // Initial state always starts with develop
        m_CurrentHBState = toHoneyBeeS_Develop;
		break;        
    case toHoneyBeeS_Develop:
        m_CurrentHBState=st_Develop(); // Will return movement or die
        m_StepDone=true;
        break;
    case toHoneyBeeS_Progress:
        m_CurrentHBState = st_Progress();
        m_StepDone = true;
        break;
	case toHoneyBeeS_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone = true;
		break;
	default:
		m_OurLandscape->Warn("HoneyBee_Base::Step()", "unknown state - default");
		exit(1);
	}
}

TTypeOfHoneyBeeState HoneyBee_Base::st_Develop()
{

    if (g_rand_uni() < mortality())
        return toHoneyBeeS_Die;
    else
    {
        m_Age++;
        if (m_Age >= developmentTime*TEST_STEP_MULT) // Note: just for debug
            return toHoneyBeeS_Progress;
        else
            return toHoneyBeeS_Develop;
    }
    return toHoneyBeeS_Develop;
}

TTypeOfHoneyBeeState HoneyBee_Base::st_Progress(void)
{
  /**
     This creates a data object to pass on to the population manager to create a worker larva, then signals that this object should be destroyed.
  */
  nextStage();
  m_CurrentStateNo = -1;
  m_StepDone = true;
  return toHoneyBeeS_Die; // Not necessary, but for neatness
}

void HoneyBee_Base::st_Dying(void)
{
	m_CurrentStateNo = -1; // this will kill the animal object and free up space
    getHive()->setCellType(getX(),getY(),getZ(),(int)CellType::Empty);
}

Hive * HoneyBee_Base::getHive()
{
    return m_OurPopulationManager->getHive();
}

//******************************************************************************************************
//***************************** START HoneyBee_WorkerEgg Class *****************************************
//******************************************************************************************************

HoneyBee_WorkerEgg::HoneyBee_WorkerEgg(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
    : HoneyBee_Base(a_x, a_y, a_z, p_L, p_BPM)
{
    developmentTime=3;
}



//******************************************************************************************************
//*************************** START HoneyBee_WorkerLarva Class *****************************************
//******************************************************************************************************
HoneyBee_WorkerLarva::HoneyBee_WorkerLarva(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
    : HoneyBee_Base(a_x, a_y, a_z, p_L, p_BPM)
{
    developmentTime=9;
}

void HoneyBee_WorkerLarva::ReInit(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
    HoneyBee_Base::ReInit(a_x, a_y, a_z, p_L, p_BPM);
}

/**
* This is the development code for all honey bee worker larvae.
* It has to:
* - take care of daily mortality rates
* - increase the physiological age based on hive temperature and humidity and food supply
* - if large enough it must pupate
*
* \param[in] nothing
* \return either a signal to pupate or to continue developing, or to die
*/

HoneyBee_WorkerPupa::HoneyBee_WorkerPupa(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
    : HoneyBee_Base(a_x, a_y, a_z, p_L, p_BPM)
{
    developmentTime=10;
}

void HoneyBee_WorkerPupa::ReInit(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
    HoneyBee_Base::ReInit(a_x, a_y, a_z, p_L, p_BPM);
}


HoneyBee_Worker::HoneyBee_Worker(const int a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
    : HoneyBee_Base(a_x, a_y, a_z, p_L, p_BPM),
      moveSteps(10),stepCount(0)
{
}

void HoneyBee_Worker::ReInit(const int  a_x, const int a_y, const int a_z, Landscape* p_L, HoneyBee_Colony* p_BPM)
{
    HoneyBee_Base::ReInit(a_x, a_y, a_z, p_L, p_BPM);
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_MoveRandom()
{    
    std::array<int,3> p = getHive()->randomNeighbour(getX(),getY(),getZ());
    getHive()->decBeeCount(getX(),getY(),getZ());
    SetX(p[0]);
    SetY(p[1]);
    SetZ(p[2]);
    getHive()->incBeeCount(p[0],p[1],p[2]);
    stepCount++;
    if (stepCount >= moveSteps)
    {
        m_StepDone=true;
    }
    return toHoneyBeeS_Decide;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_Decide()
{
    m_StepDone=true;
    return toHoneyBeeS_ForagePollen;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_ForagePollen()
{
    TestLandscape& tl=m_OurPopulationManager->testlandscape;
    if (forageCounter==0)
    {
        forageCounter=10;
        inHive=false;
    }

    forageCounter--;
    m_StepDone=true;

    if (forageCounter==0)
    {
        int day=m_OurLandscape->SupplyDayInYear();
        forageAmount=tl.decPollen;
        return toHoneyBeeS_StorePollen;
    }
    else
    {
        return toHoneyBeeS_ForagePollen;
    }
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_StorePollen()
{
    getHive()->incSugarPool(forageAmount);
    m_StepDone=true;
    inHive=true;
    return toHoneyBeeS_Decide;
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_ForageNectar()
{
    TestLandscape& tl=m_OurPopulationManager->testlandscape;

    if (forageCounter==0)
    {
        forageCounter=tl.waitLength();
        inHive=false;
    }

    forageCounter--;
    m_StepDone=true;

    if (forageCounter==0)
    {
        int day=m_OurLandscape->SupplyDayInYear();
        double forageAmount=tl.decHoney;
        double forageConcentration=0.15;
        getHive()->incSugarPool(forageAmount,forageConcentration);
        return toHoneyBeeS_Decide;
    }
    else
    {
        return toHoneyBeeS_ForageNectar;
    }
}

TTypeOfHoneyBeeState HoneyBee_Worker::st_StoreNectar()
{
    getHive()->decSugarPool(forageAmount);
    m_StepDone=true;
    inHive=true;
    return toHoneyBeeS_Decide;
}

void HoneyBee_Worker::Step(void)
{
    if (m_StepDone || m_CurrentStateNo == -1) return;
    switch (m_CurrentHBState)
    {
    case toHoneyBeeS_InitialState: // Initial state always starts with develop
        m_CurrentHBState = toHoneyBeeS_Move;
        break;
    case toHoneyBeeS_Move:
        m_CurrentHBState=st_MoveRandom(); // Will return movement or die
        break;
    case toHoneyBeeS_Decide:
        m_CurrentHBState=st_Decide();
        break;
    case toHoneyBeeS_ForageNectar:
        m_CurrentHBState=st_ForageNectar();
        break;
    case toHoneyBeeS_StoreNectar:
        m_CurrentHBState=st_StoreNectar();
        break;
    case toHoneyBeeS_ForagePollen:
        m_CurrentHBState=st_ForagePollen();
        break;
    case toHoneyBeeS_StorePollen:
        m_CurrentHBState=st_StorePollen();
        break;
    default:
        m_OurLandscape->Warn("HoneyBee_Worker::Step()", "unknown state - default");
        exit(1);
    }
}

void HoneyBee_Worker::BeginStep()
{
    metaboliseAndEat();
}

void HoneyBee_Worker::metaboliseAndEat()
{

    // Consume sugar
    double sugarConsumed=metabolicRate*cfg_hb_sugar_per_metabolic_rate.value();
    sugar-=sugarConsumed;
    // Eat sugar from pool
    getHive()->decSugarPool(sugarConsumed);

    //protein-=currentProteinUsage;
    // getHive()->dec

}
