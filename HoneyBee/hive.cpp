#include "hive.h"
#include "honeybeeconfigs.h"
#include "hbincludes.h"

//#include "../Landscape/configurator.h"

//static CfgFloat cfg_hb_honeycell_volume("HB_HONEYCELL_VOLUME",CFG_CUSTOM,0.001);


// Returns a 2D array for one side of a frame
template<class T>
blitz::Array<T,2> getSide(HiveCoords<T> A,const int frame, const FrameSide side)
{
  return A.z(blitz::Range::all(),blitz::Range::all(),(frame*2) + (int)side);
}

// Initialises a Hive as indices.
template <class T>
void setAsIndex(HiveCoords<T> A)
{
  A.x=A.i;
  A.y=A.j;
  A.z=A.k;
}

// Initialises a Hive as positions.
template <class T>
void setAsPosition(HiveCoords<T> A,
   const double cellRadius,
   const double frameSpacing,
   const double sideOffset)
{
  double r=cellRadius;
  double xs=sqrt(r*r-pow((r/2),2));
  double ys=r-xs/4.0;
  A.x = A.i * xs + (A.j%2)*(xs/2.0);
  A.y = A.j*ys;
  A.z = A.k*frameSpacing - sideOffset;
  A.z += (A.k%2)*sideOffset*2;
}

// Calculates an array of distance from a point
template <class T>
blitz::Array<T,3> getDistances(HiveCoords<T> A, const T x, const T y, const T z)
{
  blitz::Array<T,3> B = A.x.copy();
  B=sqrt(blitz::pow2(A.x-x) + blitz::pow2(A.y-y) + blitz::pow2(A.z-z));
  return B;
}


Hive::Hive(unsigned x, unsigned y, unsigned z, const float cellRadius, const float frameSpacing, const float sideOffset)
{
    initialise(x,y,z,cellRadius,frameSpacing,sideOffset);
}

void Hive::makeSomeHoney()
{
    celltype=blitz::where((temperature < (273.0+10.0)) && (position.y>40),4,0);
    celltype=blitz::where((temperature < (273.0)+10.0) && (temperature>=(273.0+5.0)),5,celltype);
}


void Hive::initialise(unsigned x, unsigned y, unsigned z, const float cellRadius, const float frameSpacing, const float sideOffset)
{
    position.reshape(x,y,z);
    beeCount.resize(position.x.shape());
    celltype.resize(position.x.shape());
    honey.resize(position.x.shape());
    pollen.resize(position.x.shape());

    setAsPosition(position,cellRadius,frameSpacing,sideOffset);
    xrange=position.x.shape()[0]-1;
    yrange=position.x.shape()[1]-1;
    zrange=position.x.shape()[2]-1;
    setTemperatureGauss(30.0,273.0,50.0,200.0,65.0,99.9);
    makeSomeHoney();
    pollenStore={-1,-1,-1};
    //nectarStore={-1,-1,-1};

    pollenStoreFullWeight=1.0;
    nectarStoreFullVolume=1.0;

    sugarPool=0.000005*3000; // How much a bee can hold * N bees. Rough init value
    sugarPoolConcentration=.15;
}

blitz::Array<float,3>  Hive::getDist(const float x, const float y, const float z)
{
    blitz::Array<float,3> B;
    B.resize(position.x.shape());
    B=blitz::sqrt(blitz::pow2(position.x-x) + blitz::pow2(position.y-y) + blitz::pow2(position.z-z));
    return B;
}

blitz::Array<float,2> Hive::getFrameDist(const int frame, const FrameSide side)
{
    blitz::Array<float,3> D = getDist(0.0,0.0,0.0);
    return D(blitz::Range::all(),blitz::Range::all(),(frame*2) + (int)side);
}


void Hive::setTemperatureGauss(float amplitude, float base, float sig, const float x, const float y, const float z)
{
  temperature.resize(position.x.shape());
  blitz::Array<float,3> D;
  D.resize(position.x.shape());
  D=getDist(x,y,z);
  //      D=getDistances<float>(position,x,y,z);
  temperature=amplitude * blitz::exp(-(D*D)/(2*sig*sig)) + base;
}

void Hive::makeTypesFromTemp()
{
    celltype=0;
    celltype=blitz::where(temperature>275.0,1,celltype);
    celltype=blitz::where(temperature>280.0,2,celltype);
    celltype=blitz::where(temperature>285.0,3,celltype);
    celltype=blitz::where(temperature>290.0,4,celltype);
    celltype=blitz::where(temperature>295.0,5,celltype);
    celltype=blitz::where(temperature>300.0,6,celltype);
}

void Hive::clearCellTypeArray()
{
    celltype.resize(position.x.shape());
    celltype=0;
}

// Change this to return blitz::TinyVector<int,3>
std::array<int,3> Hive::randomNeighbour(const int x, const int y, const int z)
{
    std::array<int,3> p;

    int xx=x;
    int yy=y;
    int zz=z;
    int rn;

    rn=rand() % 3-1;
    if (x+rn > (int)xrange)
    {
        rn=rand() % 3-1;
        if (!(z+rn > (int)zrange))
            zz+=rn;
        if (!(z+rn < 0))
            zz+=rn;
    }
    else
    {
        xx+=rn;
    }

    rn=rand() % 3-1;
    if (y+rn > (int)yrange)
    {
        rn=rand() % 3-1;
        if (!(z+rn > (int)zrange))
            zz+=rn;
        if (!(z+rn < 0))
            zz+=rn;
    }
    else
    {
        yy+=rn;
    }

    p[0]=xx;
    p[1]=yy;
    p[2]=zz;

    //std::cout << "Old Location: " << x << " " << y << " " << z << std::endl;
    //std::cout << "New Location: " << p[0] << " " << p[1] << " " << p[2] << std::endl;
    return p;
}

/*
void Hive::storeNectar(float volume)
{
    if(nectarStore.x==-1) // We have no cell currently open for storing nectar.
    {
        //nectarStore={1,1,1};
    }
}
*/

blitz::TinyVector<int,3> Hive::warmest(CellType cellType)
{
    blitz::Array<float,3> temp;
    temp.resize(getTemperatureArray().shape());
    temp=blitz::where(getCellTypeArray()==(int)cellType, getTemperatureArray(), 0);
    blitz::TinyVector<int,3> inds;
    inds=blitz::maxIndex(temp);
    return inds;
}

blitz::TinyVector<int,3> Hive::randomCell()
{
    blitz::TinyVector<int,3> shp=shape();
    blitz::TinyVector<int,3> r={rand()%shp(0),rand()%shp(1),rand()%shp(2)};
    return r;
}

// Maybe not the fastest algorithm.
blitz::TinyVector<int,3> Hive::randomCell(CellType cellType)
{
    blitz::TinyVector<int,3> r=randomCell();
    while (getCellType(r) != cellType)
    {
        r=randomCell();
    }
    return r;
}


void Hive::openWarmestPollenCell()
{
    //blitz::TinyVector<int,3> wm;
    pollenStore=warmest(CellType::MarkedPollen);
    setCellType(pollenStore,CellType::Pollen);
    setPollen(pollenStore,0.0);
}

void Hive::storePollen(const double weight)
{
    if (isPollenStoreClosed())
    {
        openWarmestPollenCell();
    }
    incPollenStore(weight);
    if (isPollenStoreFull())
    {
        closePollenStore();
    }
}

void Hive::openWarmestNectarCell()
{
    //blitz::TinyVector<int,3> wm;
    nectarStore=warmest(CellType::MarkedNectar);
    setCellType(nectarStore,CellType::Nectar);
    setNectar(nectarStore,0.0);
}

void Hive::storeNectar(const double volume)
{
    if (isNectarStoreClosed())
    {
        openWarmestNectarCell();
    }
    incNectarStore(volume);
    if (isNectarStoreFull())
    {
        closeNectarStore();
    }
}

void Hive::incSugarPool(double quantity, double fromConcentration)
{
    sugarPool+=quantity*fromConcentration/sugarPoolConcentration;
}

void Hive::decSugarPool(double quantity, double toConcentration)
{
    sugarPool+=quantity*toConcentration/sugarPoolConcentration;
}

void Hive::storePoolInHoneyCell(blitz::TinyVector<int,3> cell)
{
    setCellType(cell,CellType::Honey);
    decSugarPool(cfg_hb_honeycell_volume.value(),cfg_hb_honey_concentration.value());
}

void Hive::storePoolInHoneyCell()
{
    blitz::TinyVector<int,3> cell;

    storePoolInHoneyCell(cell);
}

void Hive::takeFromHoneyCellToPool(blitz::TinyVector<int,3> cell)
{
    setCellType(cell,CellType::Empty);
    incSugarPool(cfg_hb_honeycell_volume.value(),cfg_hb_honey_concentration.value());
}

void Hive::takeFromHoneyCellToPool()
{
    blitz::TinyVector<int,3> cell = randomCell(CellType::Honey);
    setCellType(cell,CellType::Empty);
    incSugarPool(cfg_hb_honeycell_volume.value(),cfg_hb_honey_concentration.value());
}


/*
bool openWarmestPollenCell();
void closePollenStore() {pollenStore(0)=-1;}
bool isPollenStoreOpen() {return pollenStore(0) != -1;}
bool isPollenStoreClosed() {return pollenStore(0) == -1;}
void storePollen(double volume);
void incPollenStore(double amount);
bool pollenStoreFull();
*/


//#define __TESTMAIN
#if defined __TESTMAIN
int main()
{
  Hive * hive = new Hive(4,3,2,5.0,20.0,3.0);
  hive->setTemperatureGauss(1,5,5,3,3);
  
  //  std::cout << hive->getX() << std::endl;
  // std::cout << hive->getDist(1,1,1) << std::endl;
  // std::cout << hive->getTemperature() << std::endl;
  // std::cout << hive->getFrameDist(1,FrameSide::Front) << std::endl;
  std::cout << hive->getFrameTemp(1,FrameSide::Front) << std::endl;
  delete hive;
}
#endif
