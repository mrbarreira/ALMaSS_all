#ifndef HBTOOLS_H
#define HBTOOLS_H
#include <string>
#include <vector>
#include <blitz/array.h>
#include <memory>

bool isFloat(std::string myString);
std::vector<std::string> split(std::string input, char delimiter=',');
std::vector<std::vector<std::string> > loadCSV(std::string filename);
blitz::Array<double,2> * blitzLoadCSVdouble(std::string filename, double nodataValue=-9999.0);
blitz::Array<int,2> * blitzLoadCSVint(std::string filename, int nodataValue=-9999);
#endif // HBTOOLS_H

