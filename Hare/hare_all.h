/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\brief
\file
Hare_all.h - contains the headers for all hare related classes\n
*/
/**
\file
\n
By C.J.Topping  May 2008\n
\n
*/

//---------------------------------------------------------------------------
#ifndef HaresH
#define HaresH

//---------------------------------------------------------------------------

class THare;
class THare_Population_Manager;
class Hare_Female;

//------------------------------------------------------------------------------
/**
Type definition for a list of hares
*/
typedef vector<THare*> TListOfHares;
//---------------------------------------------------------------------------

/**
Enumerator for hare object types
*/
typedef enum {
	hob_Infant,
	hob_Young,
	hob_Juvenile,
	hob_Male,
	hob_Female,
	hob_Foobar
} Hare_Object;

/**
	\brief
	Enumerator for hare behavioural states
*/
typedef enum
{
      tohs_InitialState=0,
      tohs_NextStage,
      tohs_Developing,
      tohs_Dispersal,
      tohs_Foraging,
      tohs_Resting,
      tohs_ReproBehaviour,
      tohs_Running,
      tohs_Dying,
      tohs_DestroyObject  // Used to signal that all necessary behaviour is
                          // done, safe to remove the object
} TTypeOfHareState;

/**
	\brief
	Enumerator for hare activities
*/
typedef enum
{
  activity_Resting=0,
  activity_Running,
  activity_Foraging,
  activity_Walking,
  activity_Dispersal,
  activity_Recovery,
  activity_oestrouscycle,
  activity_inoestrous,
  activity_gestation,
  activity_givebirth,
  activity_lactation,
  activity_nonAdult
}TTypeOfActivity;

/**
\brief
Data entry for mark release recapture data MRR_Data
*/
struct MRR_Entry{
	int m_HareRefNum;
	int m_trappings1;
	int m_trappings2;
};

/**
\brief
Class for running mark-release-recapture experiments
*/
class MRR_Data {
public:
    MRR_Data();
	void AddEntry(int a_RefNum);
	void OutputToFile();
	void IncTrapping() { m_currenttrapping++; }
protected:
	// Class for the MRR data before it is written to file-
	int m_currenttrapping;
	vector <MRR_Entry> m_Entries;
};

/**
\brief
Class used to pass hare information to CreateObjects
*/
class struct_Hare
{
 public:
  int x;
  int y;
  int age;
  Landscape* L;
  THare_Population_Manager * HM;
  Hare_Female* Mum;
  int RefNum;
  double weight;
};

/*-------------------------------------------------------------------------------------------
--------------------------------------    THARE    ------------------------------------------
---------------------------------------------------------------------------------------------*/
/**
\brief
The base class for all hare classes
*/
class THare : public TAnimal
{
/**
   Inherits m_Location_x, m_Location_y, m_OurLandscape and general step behaviour from TAnimal
*/
protected:
   // Attributes
	/**
	\brief
	Defines the current activity
	*/
	TTypeOfHareState m_CurrentHState;
	/**
	\brief
	State variale - hare age
	*/
	int m_Age;
	/**
	\brief
	State variale - the type of hare
	*/
	Hare_Object m_Type;
	/**
	\brief
	State variale - hare weight g
	*/
	double m_weight;
	/**
	\brief
	State variale - last hare weight
	*/
	double m_old_weight;
	/**
	\brief
	Pointer to the hare's mum 
	*/
	Hare_Female* m_MyMum;
	/**
	\brief
	Pointer to the hare population manager */
	THare_Population_Manager* m_OurPopulationManager;
	/**
	\brief
	Physiolocal lifespan, assuming nothing else kills the hare (unlikely to reach this age) */
	int m_Lifespan;
	/**
	\brief
	Minutes of potential activity time per day
	*/
	int m_ActivityTime;
	/**
	\brief
	State variable -  the number of consecutive days in negative energy balance
	*/
	int m_StarvationDays;
	/**
	\brief
	State variable -  the energy reserve of the hare
	*/
	double m_fatReserve;
	/**
	\brief
	State variable -  the amount of energy available today, can be in deficit
	*/
	double m_TodaysEnergy;
	/**
	\brief
	State variable -  the amount of energy it is possible to eat as a multiplyer or RMR
	*/
	double m_EnergyMax;
	/**
	\brief
	KJ/m cost of running per kg hare
	*/
	double m_KJRunning;  // KJ/metre
	/**
	\brief
	KJ/m cost of walking per kg hare
	*/
	double m_KJWalking;  // KJ/metre
	/**
	\brief
	KJ/m cost of foraging per kg hare
	*/
	double m_KJForaging; // KJ/minute
	/**
	\brief
	m/min speed of  running per kg hare
	*/
	double m_SpeedRunning;  // metre/min
	/**
	\brief
	m/min speed of  walking per kg hare
	*/
	double m_SpeedWalking;  // metre/min
	/**
	\brief
	Energy obtained from foraging/feeding */
	double m_foragingenergy;
	/**
	\brief
	peg x-coordinate */
	int m_peg_x;
	/**
	\brief
	peg y-coordinate */
	int m_peg_y;
	/**
	\brief
	Unique hare reference number, also functions as sex flag
	*/
	int m_RefNum;

	/**
	\brief
	State variable used in alternative density-dependent configurations
	*/
	int m_experiencedDensity;
	/**
	\brief
	State variable used in alternative density-dependent configurations
	*/
	int m_lastYearsDensity;
	/**
	\brief
	State variable used in alternative density-dependent configurations
	*/
	int m_ddindex;
	/**
	\brief
	State variable used in alternative density-dependent configurations
	*/
	int m_expDensity[365];
	/**
	\brief
	State variable used in alternative density-dependent configurations
	*/
	int m_DensitySum;
	/**
	\brief
	flag for sickness - used in conjunction with disease configurations */
	bool m_IamSick;

	/**
	\brief
	State variable used to hold the current body-burden of pesticide
	*/
	double m_pesticide_burden;
	/**
	\brief
	State variable used to hold the daily degredation rate of the pesticide in the body
	*/
	double m_pesticidedegradationrate;
	/**
	\brief
	Flag to indicate pesticide effects (e.g. can be used for endocrine distruptors with delayed effects until birth).
	*/
	bool m_pesticideInfluenced1;

public:
	/**
	\brief
	Will hold and array of palatability for hare for each tov type. Most are 1, but unpalatable vegetation can be specified here
	*/
	static double* m_vegPalatability;
	// Methods
	/**
	\brief
	Constructor
	*/
	THare(int p_x, int p_y, Landscape* p_L, THare_Population_Manager* p_PPM);
	/**
	\brief
	Object Initiation
	*/
	void THareInit(int p_x, int p_y, THare_Population_Manager* p_PPM);
	/**
	\brief
	Destructor
	*/
	virtual ~THare();
	/**
	\brief
	Base implementation only - reimplemented
	*/
	virtual TTypeOfHareState st_Dispersal();
	/**
	\brief
	Base implementation only - reimplemented
	*/
   virtual void BeginStep       (void) {}
	/**
	\brief
	Base implementation only - reimplemented
	*/
   virtual void Step            (void) {}
	/**
	\brief
	Base implementation only - reimplemented
	*/
	virtual void EndStep         (void) { MovePeg(); }

// Interface Functions
	/**
	Provide the weight of the hare.
	*/
   double GetWeight() {
	   return m_weight;
   }
   /**
	\brief
	Provide the wet weight of the hare.
	*/
	/**
	Uses a standard multiplier of 3.8 to convert dry to wet weight, then adds the fat (no water here).
	*/
	double GetTotalWeight() {
		return m_weight*3.8+m_fatReserve;
	}
	/**
	Provide the age of the hare
	\brief
	*/
	int GetAge() {
	   return m_Age;
	}
	/**
	\brief
	Inform Mum that we are dead
	*/
	/**
	Really a Hare_Infant method. Implemented here because it is needed only for debug.
	*/
	void ON_MumDead(Hare_Female* a_Mum) {
     if (a_Mum != m_MyMum)   {
       m_OurLandscape->Warn("Hare_Infant::On_MumDead  - not my mum!!",NULL);
       exit(1);
     }
     // I am now an orphan
     m_MyMum=NULL;
   }
	/**
	\brief
	Set the mother pointer. Reimplemented in Hare_Infant
	*/
   void SetMum(Hare_Female* /*a_af*/) { ; }
	/**
	\brief
	Get the mother pointer
	*/
   Hare_Female* GetMum() { return m_MyMum; }
	/**
	\brief
	Get todays RMR
	*/
   double GetRMR();
	/**
	\brief
	Test for predation
	*/
   virtual bool WasPredated();
	/**
	\brief
	Get the refnum for this hare
	*/
   int GetRefNum() { return m_RefNum; }
   /**
   \brief
   Mortality - overridden in descendent classes
   */
   virtual void ON_Dead( void ) {
	   ;
   }
   /**
   \brief
   Loads static member m_vegPalatability with data
   */
   void loadVegPalatability( void );
protected:
	/**
	\brief
	Run a distance in a direction
	*/
	bool Run(int a_dist, int a_direction);
 	/**
	\brief
	Adjust energy balance for an activity
	*/
	void EnergyBalance(TTypeOfActivity a_activity, int dist);
 	/**
	\brief
	Adjust time budger for an activity
	*/
	void TimeBudget(TTypeOfActivity a_activity, int dist);
 	/**
	\brief
	Tidy up before removing the object on death
	*/
	void st_Dying();
 	/**
	\brief
	Run
	*/
	virtual void Running(int a_max_dist);
 	/**
	\brief
	Walking
	*/
	void Walking(int a_dist, int a_direction);
	/**
	\brief
	Foraging
	*/
	double Forage(int &time); // Returns energy obtained
	/**
	\brief
	Foraging but also incorporating pesticide exposure.
	*/
	double ForageP(int &time); // Returns energy obtained
	/**
	\brief
	Handles internal effects of pesticide exposure. If any effects are needed this method must be re-implemented by descendent classes.
	*/
	virtual void InternalPesticideHandlingAndResponse();
	/**
	\brief
	Handles internal effects of endocrine distrupter pesticide exposure. If any effects are needed this method must be re-implemented by descendent classes.
	*/
	virtual void GeneralEndocrineDisruptor(double /* a_pesticide_dose */) { ; }
	/**
	\brief
	Handles internal effects of organophosphate pesticide exposure. If any effects are needed this method must be re-implemented by descendent classes.
	*/
	virtual void GeneralOrganoPhosphate(double /* a_pesticide_dose */) { ; }
	/**
	\brief
	Forage from an area
	*/
	double ForageSquare(int a_x, int a_y);
	/**
	\brief
	Forage from an area and resturn pesticide exposure as well as food
	*/
	double ForageSquareP(int a_x, int a_y, double *a_pestexposure);
 	/**
	\brief
	Get peg distance
	*/
	int GetPegDistance();
 	/**
	\brief
	Get attractive force of peg
	*/
	int GetPegPull();
 	/**
	\brief
	Get direction of peg
	*/
	int GetPegDirection();
 	/**
	\brief
	Move the peg according to attraction forces
	*/
	void MovePeg();
	/**
	\brief
	Do we require a response to a farm event
	*/
	bool OnFarmEvent( FarmToDo event );
};

/**
\brief
The class that handles all the population lists for hares.
*/
class THare_Population_Manager : public Population_Manager
{
public:
// Methods
   THare_Population_Manager(Landscape* L);
   virtual ~THare_Population_Manager (void);
   	/** \brief Output method */
   virtual void TheAOROutputProbe();
   void CreateObjects(int ob_type, TAnimal *pvo,void* null ,
                                         struct_Hare* data,int number);
	/**
	\brief
	Returns the RMR given a specific age and mass
	*/
	double GetRMR(int a_age, double a_size);
	/**
	\brief
	Get the next ID number available
	*/
	int GetHareRefNum() { return ++m_RefNums; }
    /**
	Get the maximum daily energy needed for growth for this a_age
	*/
    double GetMaxDailyGrowthEnergy(int a_age) {
#ifdef __BOUNDSCHECK
	   if ((a_age<0) || (a_age>5000)) {
		     m_TheLandscape->Warn( "Hare GetMaxDailyGrowthEnergy a_age out of bounds", NULL );
			 exit( 1 );
	   }
#endif
		return m_MaxDailyGrowthEnergy[a_age];
   }
	/**
	\brief
	Get the maximum daily energy needed for growth for this a_age for use in protein construction
	*/
    double GetMaxDailyGrowthEnergyP(int a_age) {
#ifdef __BOUNDSCHECK
	   if ((a_age<0) || (a_age>5000)) {
		     m_TheLandscape->Warn( "Hare GetMaxDailyGrowthEnergyP a_age out of bounds", NULL );
			 exit( 1 );
	   }
#endif
		return m_MaxDailyGrowthEnergyP[a_age];
	}
	/**
	\brief
	Get the maximum daily energy needed for growth for this a_age for use in fat construction
	*/
    double GetMaxDailyGrowthEnergyF(int a_age) {
#ifdef __BOUNDSCHECK
	   if ((a_age<0) || (a_age>5000)) {
		     m_TheLandscape->Warn( "Hare GetMaxDailyGrowthEnergyF a_age out of bounds", NULL );
			 exit( 1 );
	   }
#endif
		return m_MaxDailyGrowthEnergyF[a_age]; }
	/**
	\brief
	Get the growth efficiency for this a_age
	*/
   double GetGrowthEfficiency(int a_age) {
#ifdef __BOUNDSCHECK
	   if ((a_age<0) || (a_age>5000)) {
		     m_TheLandscape->Warn( "Hare GetGrowthEfficiency a_age out of bounds", NULL );
			 exit( 1 );
	   }
#endif
		return m_GrowthEfficiency[a_age]; }
	/**
	\brief
	Get the growth efficiency for this a_age for creating protein
	*/
   double GetGrowthEfficiencyP(int a_age) {
#ifdef __BOUNDSCHECK
	   if ((a_age<0) || (a_age>5000)) {
		     m_TheLandscape->Warn( "Hare GetGrowthEfficiencyP a_age out of bounds", NULL );
			 exit( 1 );
	   }
#endif
		return m_GrowthEfficiencyP[a_age]; }
	/**
	\brief
	Get the growth efficiency for this a_age for creating fat
	*/
	double GetGrowthEfficiencyF(int a_age) {
#ifdef __BOUNDSCHECK
	   if ((a_age<0) || (a_age>5000)) {
		     m_TheLandscape->Warn( "Hare m_GrowthEfficiencyF a_age out of bounds", NULL );
			 exit( 1 );
	   }
#endif
		return m_GrowthEfficiencyF[a_age]; }
	/**
	\brief
	Get the cost of moving 1m in KJ dependent upon mass (
	*/
	double GetKJperM(int a_size) {
#ifdef __BOUNDSCHECK
	   if ((a_size<0) || (a_size>7000)) {
		   char sz[255];
		   _itoa(a_size,sz,10);
		   m_TheLandscape->Warn( "Hare GetKJperM a_size out of bounds", sz );
		   exit( 1 );
	   }
#endif
		return m_KJperM[a_size]; }
	int GetLitterSize(int noLitters);
	/**
	\brief
	Density function - adds one to the density in the square containing by x,y. Each square is 256x256m in size
	*/
	void AddHareDensity(int x, int y, Hare_Object a_type)   { m_DensityMap[a_type][x >> __DENSITYSHIFT][y >> __DENSITYSHIFT]++; }
	/**
	\brief
	Density function - subtracts one from the density in the square containing by x,y. Each square is 256x256m in size
	*/
	void SubtractHareDensity(int x, int y, Hare_Object a_type)   { m_DensityMap[a_type][x >> __DENSITYSHIFT][y >> __DENSITYSHIFT]--; }
	/**
	\brief
	Density function - returns the density of infants in the square containing by x,y. Each square is 256x256m in size
	*/
	int GetInfantDensity(int x, int y)   { return m_DensityMap[hob_Infant][x >> __DENSITYSHIFT][y >> __DENSITYSHIFT]; }
	/**
	\brief
	Density function - returns the density of young in the square containing by x,y. Each square is 256x256m in size
	*/
	int GetYoungDensity(int x, int y)    { return m_DensityMap[hob_Young][x>>__DENSITYSHIFT][y>>__DENSITYSHIFT]; }
    /**
	\brief
	Density function - returns the density of juveniles in the square containing by x,y. Each square is 256x256m in size
	*/
	int GetJuvenileDensity(int x, int y) { return m_DensityMap[hob_Juvenile][x>>__DENSITYSHIFT][y>>__DENSITYSHIFT]; }
    /**
	\brief
	Density function - returns the density of males in the square containing by x,y. Each square is 256x256m in size
	*/
	int GetMaleDensity(int x, int y)     { return m_DensityMap[hob_Male][x>>__DENSITYSHIFT][y>>__DENSITYSHIFT]; }
    /**
	\brief
	Density function - returns the density of females in the square containing by x,y. Each square is 256x256m in size
	*/
	int GetFemaleDensity(int x, int y)   { return m_DensityMap[hob_Female][x>>__DENSITYSHIFT][y>>__DENSITYSHIFT]; }
    /**
	\brief
	Density function - returns the density of all hares in the square containing by x,y. Each square is 256x256m in size
	*/
	int GetTotalDensity(int x, int y)    { return m_DensityMap[5][x>>__DENSITYSHIFT][y>>__DENSITYSHIFT]; }
    /**
	\brief
	Density function - returns the density of adults in the square containing by x,y. Each square is 256x256m in size
	*/
	int GetAdultDensity(int x, int y)    
	{ 
		return (m_DensityMap[hob_Male][x>>__DENSITYSHIFT][y>>__DENSITYSHIFT] + m_DensityMap[hob_Female][x>>__DENSITYSHIFT][y>>__DENSITYSHIFT]); 
	}
    /**
	\brief
	Density function - returns the density of adults in the square containing by x,y, but for last year at this time. Each square is 256x256m in size
	*/
	int GetDelayedAdultDensity(int x, int y)    {
		return m_DensityMap[6][x>>__DENSITYSHIFT][y>>__DENSITYSHIFT];
	}
	/**
	\brief
	Return the proportion of time used in communicating with con-specifics.
	*/
	/**
	Based on:\n
	          m_Interference[i]=exp(i*cfg_HareInterferenceConstant)
	*/
	double GetInterference(int h) {
#ifdef __BOUNDSCHECK
	   if ((h<0) || (h>2499)) {
		     m_TheLandscape->Warn( "Hare Interference range out of bounds", NULL );
			 exit( 1 );
	   }
#endif
		return m_Interference[h]; //[m_variableDD];
	}
	/**
	\brief
	Get stored polygon food quality
	*/
	double GetPolyFood(int a_poly) { return m_PolyFood[a_poly]; }
	/**
	\brief
	Set polygon food quality
	*/
	void SetPolyFood(int a_poly, double a_value ) { m_PolyFood[a_poly]=a_value; }
	/** 	
	\brief
	BodyBurden output 
	*/
	void BodyBurdenOut(int a_year, int a_day, double a_bb, double a_mgkg) {
		fprintf(BodyBurdenPrb, "%d\t%d\t%g\t%g\n", a_year, a_day, a_bb, a_mgkg);
	}

	/**
	\brief	Input variable - Juvenile mortality rate
	*/
	double m_JuvMortRate;
	/**
	\brief	Input variable - Adult mortality rate
	*/
	double m_AdultMortRate;
	/**
	\brief	Input variable - Young mortality rate
	*/
	double m_YoungMortRate;
	/**
	\brief	Variable for adding stochasticity
	*/
	float m_GoodYearBadYear;
	/**
	\brief	Array for storing minimum acceptable size with age
	*/
	double m_DMWeight[5001];
	/**
	\brief	Input variable - Threshold density dependence level
	*/
	int m_HareThresholdDD;
protected:
// Attributes
	/**	\brief	The last hare ID used	*/
	int m_RefNums;
	/** \brief Hunting Bag */
	int m_shot;
	/**	\brief	Stochasticity around mortality parameters	*/
	double m_MortStochast;
	/**	\brief	Array storing density-dependence effect with density	*/
	double m_Interference[2500]; //[21]; // this limit should never be reached, but it may be under debugging tests so watch out.
	/**	\brief	Array storing densities measured in different ways	*/
	int m_DensityMap[8][200][200];//NB maps larger than 50km in any one dimension -> memory overun
	/**	\brief	Used to vary the density dependence each year	*/
	int m_variableDD;
	/**	\brief	Storage for litter size distributions (not used in default config)	*/
	int m_LitterSize[12][7];
	/**	\brief	Temporary storage	*/
	double* m_PolyFood;
	// Energetic constants will go into these arrays
	// They are filled by the Init method
	/**	\brief	Precalculated RMR with age	*/
	double m_RMR[2][366]; // Will also needs bounds checking
	/**	\brief	Precalculated max growth energy requirement with size	*/
	double m_MaxDailyGrowthEnergy[5001]; // Will also needs bounds checking
	/**	\brief	Precalculated growth efficiency with size	*/
	double m_GrowthEfficiency[5001]; // Will also needs bounds checking
	/**	\brief	Precalculated max growth energy requirement with size for protein	*/
	double m_MaxDailyGrowthEnergyP[5001]; // Will also needs bounds checking
	/**	\brief	Precalculated max growth energy requirement with size for fat	*/
	double m_MaxDailyGrowthEnergyF[5001]; // Will also needs bounds checking
	/**	\brief	Precalculated growth efficiency for protein with size	*/
	double m_GrowthEfficiencyP[5001]; // Will also needs bounds checking
	/**	\brief	Precalculated growth efficiency for fat with size	*/
	double m_GrowthEfficiencyF[5001]; // Will also needs bounds checking
	/**	\brief	Precalculated cost of locomotion - KJ per m per kg	*/
	double m_KJperM[7001];
	/**	\brief	Data structure for MarkReleaseRecapture expts	*/
	MRR_Data* m_OurMRRData;
	/** Probe for pestidcide output/testing */
	FILE* BodyBurdenPrb;
// Methods
	void Init();
	void CalcLitterSize(double mean, double SD, int litter);
//   virtual bool StepFinished();
	virtual void DoFirst();
//   virtual void DoBefore(){}
//   virtual void DoAfter(){}
	virtual void DoLast();
	virtual void DoAlmostLast();
	void POMOutputs();
	void MRROutputs();
	/**	\brief	Standard spatial output	*/
    virtual void TheRipleysOutputProbe(FILE* a_prb);
	/**	\brief	An extra global mortality	*/
	void ExtraPopMort( void );
	void Hunting( void );
	void HuntingGrid( void );
	void HuntingDifferentiatedBeetleBankArea( void );
	virtual void Catastrophe();
};


/**
\brief
Class for infant hares (stationary, only milk inputs)
*/
class Hare_Infant : public THare
{
public:
	virtual void BeginStep       (void);
	virtual void Step            (void);
	virtual void EndStep         (void);
	/**
	\brief
	Hare infant constructor
	*/
	Hare_Infant(int p_x, int p_y, Landscape * p_L,THare_Population_Manager * p_PPM);
	/** \brief Infant object reinitiation*/
	void ReInit(struct_Hare a_data);
	/** \brief Object initiation */
	void Init();
	/**
	\brief
	Hare infant destructor
	*/
	virtual ~Hare_Infant();
	// Interface Functions
	/**
	\brief
	This hare has been killed
	*/
	virtual void ON_Dead();
	void ON_BeingFed(double a_someMilk);
	/**
	\brief
	Set the weight.
	*/
	/**
	This is only used when a hare is born, so no need to have it in the base class
	*/
	void SetWeight(double w) { m_weight=w; }
	/**
	\brief
	Set the mother pointer.
	*/
	void SetMum(Hare_Female* a_af) { m_MyMum=a_af; }
protected:
	TTypeOfHareState st_Developing();
	void st_NextStage();
	/**
	\brief
	Do we require a response to a farm event
	*/
	bool OnFarmEvent( FarmToDo event );
	// Attributes
};

/**
\brief
Class for young hares (low mobility, milk and solid food inputs)
*/
class Hare_Young : public Hare_Infant
{
public:
   virtual void BeginStep       (void);
   virtual void Step            (void);
   virtual void EndStep         (void);
   Hare_Young(int p_x, int p_y, Landscape * p_L,
                    THare_Population_Manager * p_PPM, double p_weight);
   /** \brief Young object reinitiation*/
   void ReInit(struct_Hare a_data);
   /** \brief Object initiation */
   void Init(double p_weight);
   virtual ~Hare_Young();
   // Interface
	/**
	\brief
	This hare has been killed
	*/
   virtual void ON_Dead();
protected:
  TTypeOfHareState st_Developing();
	/**
	\brief
	Not used
	*/
  TTypeOfHareState st_Dispersal();
	/**
	\brief
	Young foraging
	*/
  TTypeOfHareState st_Foraging();
	/**
	\brief
	Maturation to Hare_Juvenile
	*/
  void st_NextStage();
	/**
	\brief
	Resting
	*/
  TTypeOfHareState st_Resting();
  /**
  \brief
  Response to farm actions
  */
  bool OnFarmEvent( FarmToDo event );
};

/**
\brief
Class for juvenile hares (after 5 weeks old, fully mobile)
*/
class Hare_Juvenile : public THare
{
public:
   virtual void BeginStep       (void);
   virtual void Step            (void);
   virtual void EndStep         (void);
   Hare_Juvenile(int p_x, int p_y, Landscape * p_L,THare_Population_Manager *
                                                        p_PPM, double p_weight);
   /** \brief Juvenile object reinitiation*/
   void ReInit(struct_Hare a_data);
   /** \brief Object initiation */
   void Init(double p_weight);
   virtual ~Hare_Juvenile();
   // Interface
   virtual void ON_Dead();
protected:
 // States
	TTypeOfHareState st_Developing();
	/**
	\brief
	Juvenile Dispersal
	*/
	TTypeOfHareState st_Dispersal();
	/**
	\brief
	Juvenile foraging
	*/
	TTypeOfHareState st_Foraging();
	/**
	\brief
	Maturation to Hare_Male or Hare_Female
	*/
	void st_NextStage();
	/**
	\brief
	Juvenile Resting
	*/
	TTypeOfHareState st_Resting();
// Methods
	/**
	\brief
	Test for maturation
	*/
	bool ShouldMature();
	/** \brief Test for mortality */
	virtual bool WasPredated();
};

/**
\brief
Class for male hares
*/
class Hare_Male : public Hare_Juvenile
{
public:
   virtual void BeginStep       (void);
   virtual void Step            (void);
   virtual void EndStep         (void);
	/**
	\brief
	Constructor
	*/
	Hare_Male(int p_x, int p_y, Landscape * p_L,THare_Population_Manager * p_PPM,
                                                     double p_weight, int a_age, int a_Ref);
	/** \brief Male object reinitiation*/
	void ReInit(struct_Hare a_data);
	/** \brief Object initiation */
	void Init(double p_weight, int a_age, int a_Ref);
	/**
	\brief
	Destructor
	*/
	virtual ~Hare_Male();
// Interface
	virtual void ON_Dead();
protected:
	// States
	/**
	\brief
	Male Development
	*/
	TTypeOfHareState st_Developing();
	/**
	\brief
	Male Foraging
	*/
	TTypeOfHareState st_Foraging();
	/**
	\brief
	Male Resting
	*/
	TTypeOfHareState st_Resting();
	/**
	\brief
	Currently Unused
	*/
	TTypeOfHareState st_ReproBehaviour();
   // Methods
	/**	\brief	Handles internal effects of pesticide exposure. If any effects are needed this method must be re-implemented by descendent classes.	*/
	virtual void InternalPesticideHandlingAndResponse();
	/**
	\brief
	Handles internal effects of endocrine distrupter pesticide exposure. If any effects are needed this method must be re-implemented by descendent classes.
	*/
	virtual void GeneralEndocrineDisruptor(double /* a_pesticide_dose */);
	/**
	\brief
	Handles internal effects of organophosphate pesticide exposure. If any effects are needed this method must be re-implemented by descendent classes.
	*/
	virtual void GeneralOrganoPhosphate(double /* a_pesticide_dose */);
};

/**
\brief
Class for female hares
*/
class Hare_Female : public Hare_Juvenile
{
protected:
	// Attributes
	/** \brief State variable - current litter size*/
	int m_NoYoung;
	/** \brief State variable - current litter number */
	int m_litter_no;
	/** \brief State variable - is/not sterile */
	bool m_sterile;
	//double m_SurplusEnergy;
	/** \brief  State variable - Days in oestrous */
	int m_OestrousCounter;
	/** \brief  State variable - Days in gestation */
	int m_GestationCounter;
	/** \brief  State variable - Mass of foetal material */
	double m_LeveretMaterial;
	/** \brief  State variable - current reproductive state */
	TTypeOfActivity m_reproActivity;
	/** \brief Pointer to litter */
	TListOfHares m_MyYoung;

	/** \brief Update oestrous counter */
	void UpdateOestrous();
	/** \brief Update gestation counter */
	void UpdateGestation();
	/** \brief Produce a litter */
	void GiveBirth();
	/** \brief Find somewhere nice for the babies to hide */
	APoint PlaceYoung();
	/** \brief Lactation */
	void DoLactation();
	/** \brief Mate */
	void Mating();
public:
	/** \brief Female BeginStep */
	virtual void BeginStep(void);
	/** \brief Female Step */
	virtual void Step(void);
	/** \brief Female EndStep */
	virtual void EndStep(void);
	/** \brief Female Constructor */
	Hare_Female(int p_x, int p_y, Landscape * p_L, THare_Population_Manager *p_PPM, double p_weight, int a_age, int a_Ref);
	/** \brief Female object reinitiation*/
	void ReInit(struct_Hare a_data);
	/** \brief Object initiation */
	void Init(double p_weight, int a_age, int a_Ref);
	/** \brief Female Destructor */
	virtual ~Hare_Female();
	// Interface functions
	/** \brief Used to record energetic status */
	void dumpEnergy();
	/** \brief Female is sterile */
	void SetSterile() { m_sterile = true; }
	/** \brief The female is dead */
	virtual void ON_Dead();
	/** \brief  Swap a young list pointer*/
	bool UpdateYoung(THare* a_old, THare* a_new);
	/** \brief Add a leveret to the list of kids */
	void AddYoung(THare* a_new);
	/** \brief A leveret has been killed */
	void ON_YoungKilled(THare* a_young);
	/** \brief A leveret has matured */
	void ON_RemoveYoung(THare* a_young);
	/** \brief Last leveret predated */
	void AllYoungKilled();
	/** \brief No more young to look after */
	void AllYoungMatured();
	// *** DeBugging Methods ***
	/** \brief Debug function */
	bool ON_AreYouMyMum(THare* a_young);
	/** \brief Debug function */
	bool SanityCheckYoungList();
   // *** End Debug ***

protected:
   // States
	/**
	\brief
	Female Dispersal
	*/
    TTypeOfHareState st_Dispersal();
	/**
	\brief
	Female Developing
	*/
    TTypeOfHareState st_Developing();
	/**
	\brief
	Female Foraging
	*/
    TTypeOfHareState st_Foraging();
	/**
	\brief
	Resting
	*/
    TTypeOfHareState st_Resting();
	/**
	\brief
	Reproductive behaviour control
	*/
    TTypeOfHareState st_ReproBehaviour();
//Methods
	/**	\brief	Handles internal effects of pesticide exposure. If any effects are needed this method must be re-implemented by descendent classes.	*/
	virtual void InternalPesticideHandlingAndResponse();
	/**
	\brief
	Handles internal effects of endocrine distrupter pesticide exposure for female.
	*/
	virtual void GeneralEndocrineDisruptor(double a_pesticide_dose);
	/**
	\brief
	Handles internal effects of organophosphate pesticide exposure for female.
	*/
	virtual void GeneralOrganoPhosphate(double a_pesticide_dose);
};
//---------------------------------------------------------------------------
#endif
