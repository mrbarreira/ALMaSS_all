/**
\mainpage ALMaSS Hare ODDox Documentation

\htmlonly 
<style type="text/css">
body {
    color: #000000
    background: white;
  }
h2  {
    color: #0022aa
    background: white;
  }
</style>
  <h1>
<small>
<small>
<small>
<small>
Created by<br>
<br>
</small>
</small>
Chris J. Topping<br>
<small>Department of Wildlife Ecology<br>
National Environmental Research Institute, University of Aarhus<br>
Grenaavej 14<br>
DK-8410 Roende<br>
Denmark<br>
<br>
1st September 2008<br>
</small>
</small>
</small>
</h1>
<br>
\endhtmlonly
\n
<HR> 
\n
\htmlonly
<h3>A note about ODDox</h3>
\endhtmlonly
This model description follows broadly the ODD protocol for describing individual- and agent-based models (<a href="page0.html#ref4">
Grimm et al. 2006</a>) and consists of three main elements. 
The first provides an (O)verview, the second explains general concepts underlying 
the models (D)esign, the last, (D)etail, provides the information about the various model constructs in detail. The Detail section 
presented has been implemented by using the Doxygen program on the commented source code, creating and html-based documentation. 
In this way the source can be viewed and navigated through in an efficient and manageable manner. This has the advantage that all links 
between objects, methods and functions are highlighted and can be followed easily together with the source code, even without more than 
a very basic understanding of programming.\n
\n
In addtion to the change of Detail to using Doxygen, the section originally entitled 'Submodels' in ODD has been alterned to 'Interconnections'.
The reason for this is that submodels will be described under the classes that are covered in the doxygen part of the ODDox and that classes
with connectsion to the classes described are not submodels but entities existing at the same hierarchical level. The section 'Interconnections'
is therefore used to highlight important linkages to the main classes described in any one section of the ODDox. Note also that the ODDox for 
ALMaSS consists of many individual documentation sections.  Interconnections allows you to navigate to those descriptions that are of primary 
relevance.\n
\n
A note about code structure: This program was written in C++ code which is an object-oriented language. Objects (e.g. a male hare) are 
termed 'instances' of a 'class' (e.g. Hare_Male). Classes are arranged in a hierarchy whereby information contained in a base class is 
available to descendent classes. For an example see the class diagram at the top of TALMaSSObject. This diagram shows how TAnimal is 
descended from TALMaSSObject, and subsequently THare is descended from TAnimal, and Hare_Male from THare. Understanding this basic 
concept and terminology will be an advantage when traversing through the code using the hyperlinks since methods (functions, behaviours) 
and attributes that are not modified in the descendent class need to be viewed in the base class.\n
This method of programming reduces code size and eases code development. An example of this approach can be seen by comparing Hare_Juvenile::st_Dispersal  with Hare_Female::st_Dispersal. In this case the female dispersal uses the juvenile code, then adds special female behaviour afterwards.\n
\n
<HR> 
\n
\htmlonly 
<h2><big>Overview</big></h2>
\endhtmlonly

Hare model description following ODDox protocol
The hare model presented here forms part of the ALMaSS framework of models (<a href="page0.html#ref1">Topping et al., 2003</a>). ALMaSS integrates a detailed simulation of the landscape together with agent-based models of individual animals to elucidate impacts of changes in environmental factors including man's management on animal population dynamics. \n
\n

\htmlonly <h2>  1. Purpose </h2>
\endhtmlonly


Since the 1960s hare population have undergone severe declines in Denmark and the rest of Europe (Figure 1). Many factors have been suggested as possible causes and there is a clear correlation with intensification of agriculture and population decline. However many of the potential causal factors co-vary in time and space and as such analyses seeking to attribute the causes of declines to simple factors have not been successful. This model was designed to integrate the knowledge we have on hare ecology and behaviour to better understand hare population regulating factors.\n
\n
\htmlonly <h2>  2. State variables and scales </h2>
\endhtmlonly

\n
\htmlonly <h3>  2.a. Process Overview and Scheduling</h3>
\endhtmlonly

\n
<b>2.a.i	Hares </b> <br>
\n
Individual hares had a set of common state variables each with its own value for the individual hare. These were:\n
\n
Type of hare:\n
&nbsp;&nbsp;&nbsp;&nbsp;Infant (0-10 days) see Hare_Infant\n
&nbsp;&nbsp;&nbsp;&nbsp;Young (11-35 days) see Hare_Young\n
&nbsp;&nbsp;&nbsp;&nbsp;Juvenile (36 days to adult) see Hare_Juvenile\n
&nbsp;&nbsp;&nbsp;&nbsp;Male see Hare_Male\n
&nbsp;&nbsp;&nbsp;&nbsp;Female see Hare_Female\n
Age in days (see THare::m_Age)\n
Location: x- y-coordinate (see TAnimal::m_Location_x TAnimal::m_Location_y)\n
Weight: weight of the hare in g dry-weight (see THare::m_weight)\n
Current Behaviour: A varying behavioural state (see THare::m_CurrentHState)\n
Daily energy balance:  kJ (see THare::m_TodaysEnergy)\n
Energy Depot: g fat (see THare::m_fatReserve)\n
Physiological Lifespan (see THare::m_Lifespan)\n
Days spent in negative energey balance (see THare::m_StarvationDays)\n
Knowledge of their mother (for young stages) (see THare::m_MyMum)\n
The number of minutes available for daily activity (see THare::m_ActivityTime)\n
Energy input from foraging (see THare::m_foragingenergy)\n
Peg location (see THare::m_peg_x & THare::m_peg_y)\n
Unique indentification number, and sex flag (see THare::m_RefNum)\n
Below are all used in alternative configurations:\n
&nbsp;&nbsp;&nbsp;&nbsp;   THare::m_experiencedDensity  Used in delayed density dependent configurations\n
&nbsp;&nbsp;&nbsp;&nbsp;   THare::m_lastYearsDensity  Used in delayed density dependent configurations\n
&nbsp;&nbsp;&nbsp;&nbsp;   THare::m_ddindex  Used in delayed density dependent configurations\n
&nbsp;&nbsp;&nbsp;&nbsp;   THare::m_expDensity[365]  Used in delayed density dependent configurations\n
&nbsp;&nbsp;&nbsp;&nbsp;   THare::m_DensitySum  Used in delayed density dependent configurations\n
&nbsp;&nbsp;&nbsp;&nbsp;   THare::m_IamSick  flag for sickness - used in conjunction with disease configurations\n
\n
In addition Hare_Female has another set of state variables related to reproduction:\n
Counter for oestrous cycle (see Hare_Female::m_OestrousCounter)\n
Counter for gestation days (see Hare_Female::m_GestationCounter)\n
Current mass of foetal material (see Hare_Female::m_LeveretMaterial)\n
Reproductive State: Whether in oestrous, lactating, pregnant, or not in reproductive mode (see Hare_Female::m_reproActivity)\n
Number of current young and a list of pointers to them (see Hare_Female::m_MyYoung and Hare_Female::m_NoYoung)\n
Output variable counting the litters produced so far this season (see Hare_Female::m_litter_no)\n
Variable used in disease configuration to signify sterility (see Hare_Female::m_sterile)\n
\n
\n
\n
<b>2.a.ii	Environment </b><br>
<br>
ALMaSS is spatially explicit, requiring a detailed two-dimensional map of the landscape and having a time-step of 24 hours. 
The extent of the model landscape was variable and was a square raster representation of a landscape (e.g. 10 km x 10 km; 5 km x 5 km etc.) 
at 1 m<SUP>2</SUP> resolution, with wrap around. State variables associated with each raster map were:\n
Id: Identification number associated with each unique polygon\n
Habitat:  Land cover code for each polygon\n
Farm unit:  If the land cover code was a field then it also had an owner associated with it. The owner represented the farmer who carried out farm management on their farm unit. \n
Vegetation Type - Vegetation type for the polygon (if any vegetation grows in the polygon. \n
\n
The environment model in ALMaSS was weather dependent and the state variables associated with the weather sub-model were:\n
Temp: mean temperature on the given day (Celsius)\n
Wind: mean wind speed on the given day (m)\n
Rain:  total precipitation on given day (mm)\n
\n
ALMaSS is a complex ecological simulation system (<a href="page0.html#ref1">Topping et al. 2003</a>) with a multitude of sub-models, each with its unique set of state 
variables. E.g. Farm management is a crucial part of ALMaSS, consisting of 3 elements: farm units, farm types, and crop husbandry plans. 
There are 74 possible crops types in ALMaSS each with its own crop husbandry plan (e.g. WinterWheat) see <A HREF="page1.html"> Crop Classes</A>. Each crop type is a sub-model and has its own set 
of state variables. Documentation of the environment section is an ongoing process but the classes indicated above provide useful starting points.
Readers may refer to <a href="page0.html#ref1">Topping et al. (2003)</a> for more information on farm units, crop types, crop rotations, and farm types in ALMaSS. 
\n
The original ALMaSS model was augmented for the purposes of including the hare model by the addition of a digestibility measure to the 
vegetation growth model. Digestibility was modelled as being between 0.5-0.8, as 0.5 plus the ratio of new growth <14 days old to green 
biomass >14 days old. If digestibility exceeded 0.8 it was capped at 0.8.\n
\n
\htmlonly <h3>  2.b	Process overview</h3>
\endhtmlonly

The hare model consisted of five life-stage classes, infant (up to 10 days old at which time the hares start having an increasing portion of their diet consisting of solid food); young (up to 35 days, which is the normal weaning age); juvenile (from 35 days to adult, here defined as being a minimum of 180 days old before September 1st); male; and female. Individual hares passed through each life-stage until either becoming adult males or females.\n
\n
Each life-stage had a set of potential behavioural states linked together by conditional transitions. For example, a hare finding itself in a negative energy situation will make a transition to the behavioural state dispersal.\n
\n
Most of the key processes in the hare model had an energetic basis. Energy supply and demand determines the dispersal patterns of the hares, their rate of development and reproductive success. The key processes are as follows:\n
\n
1)	Movement - if the hares cannot obtain enough energy to cover their daily energy budget then they will move to a new area, 
once sufficient energy is available again dispersal stops.\n
2)	The size of the hare is determined by it's energetic history. 
Hares that do not get maximal energetic inputs will grow at a reduced rate, hence the final adult weight is determined throughout 
the growth period of the hare on a daily basis.\n
3)	The period with the highest energetic demand for any hare life-stage is lactation. 
If the female hare does not have enough energy during this period she will be unable to produce enough milk and her young will either develop more slowly or in extreme situations die. Fat reserves are assumed to be needed for successful rearing of a litter, hence poor energy balance at the start of breeding will also hinder the female from attempting to breed.\n
4)	In situations of extreme energy shortage death from starvation will result. 
This is quite common for young hares, but rare in adults.\n
\n
\htmlonly <h3> 2.c	Basic process for all hare stages</h3>
\endhtmlonly
\n
The hare life-stage classes are organised following a state-transition concept such that at any time each hare object is in a specific behavioural 
state (denoted by st_StateName) and exhibiting behaviour associated with that state. As a result of this behaviour or external stimuli a 
transition may occur to another state. In the descriptions below, behavioural states are described together with some general functionality
common to more that one life-stage.\n

There are a number of functions that are common to all or most of the hare objects, these are listed here to avoid repetition below.\n
\n
<b>Begin Step:</b> One of three structural controls within the model time step of one day. The BeginStep was called once for each hare object each day. When all objects had completed this step, the Step function was called. The BeginStep function was responsible for generating mortality and disturbance events, ageing and checking for physiological death. (See Hare_Infant::BeginStep, Hare_Young::BeginStep, Hare_Juvenile::BeginStep, Hare_Male::BeginStep, & Hare_Female::BeginStep)\n
\n
<b>Step:</b> This function was responsible for calling the behavioural state code for each object. Each hare object would have its Step function called at least once per time step. Hierarchical execution was used to ensure that all infants completed their Step function for the time step before Young were called, then Juveniles, Males and Females. Each object in the list for that life stage (e.g. female), would execute the Step function. This would result in a return parameter denoting the behavioural state the hare was currently in together with a Boolean value to signal whether activity had been completed for that time step. If activity was not completed then Step would be called again on the next pass through the list. When all objects have finished the Step code, then the EndStep function was called. (See Hare_Infant::Step, Hare_Young::Step, Hare_Juvenile::Step, Hare_Male::Step, & Hare_Female::Step)\n
\n
<b>End Step:</b> Like the BeginStep this function was called only once each time step for each object. Its role was to move the home range peg (see below) for juveniles and adults, but had the role of calling stDevelopment for the young and infants. In this way it was certain that all activity and energy inputs and outputs had been completed before the young hares grew that day. (See Hare_Infant::EndStep, Hare_Young::EndStep, Hare_Juvenile::EndStep, Hare_Male::EndStep, & Hare_Female::EndStep)\n
\n
<b>Movement:</b> Movement requires the  calculation of the cost of movement in time and energy and subtraction of these from the appropriate budgets (see THare::TimeBudget & THare::EnergyBalance). Movement was used both in foraging and dispersal. \n
\n
<b> Foraging:</b> The state is common to all but the infant hare and is assumed to use up to two thirds of the daily time budget, less that time used for interactions with other hares and for dispersal. Foraging functions by allowing the model hare to search its local area in grid units of 10 x 10 m, and to extract resources from these areas at a constant rate which was modified by the structure of the vegetation. At the start of each day's foraging activity the hare would search the grid units it was located in and extract food. If the food extracted less than the maximum the hare could use, and there was still time in the time budget, the hare then searched the surrounding 8 grid units, selecting the one that had the best forage and moved there to feed. If two or more units were tied for best the choice was random. This process was continued until either enough food was found (i.e. RMR, growth, locomotion, reproduction, and replenishing fat reserve costs were covered), or there was no more time. It was not possible for a hare to feed from the same grid unit twice in one day. Movement between cells during searching resulted in the use of energy via locomotion. The resulting pattern of foraging is a random walk in a uniform area, but directed movement in patchy conditions. However, the observed pattern of movement of real hares does not match this closely in that real hares tend to stay in local areas for an extended period of time, i.e. have flexible home ranges. The following solution was found to simulate this. See Hare_Young::st_Foraging, Hare_Juvenile::st_Foraging, Hare_Male::st_Foraging, & Hare_Female::st_Foraging.\n
\n
The centre of the hares activity range is identified (the 'peg'). The peg exerts a attractive force on the hare that increases with the square of the distance between the hare's location and the peg. At each movement choice there is a probability of moving in the direction of the peg rather than the optimum feeding choice of    where d is the distance in m from the peg. This has the property of strongly reducing the 'drift' of an individual across the landscape, but still allowing almost optimal foraging. This alone would have had the undesirable effect of creating permanent home ranges for hares; hence the peg was also allowed to move at the end of each day. The peg movement was in the direction of the final daily forage location and was a constant proportion (pd) of the distance between this and the current peg location. pd was set to be 0.1. These parameter values were fitted by evaluating the behaviour of model hares and choosing values that resulted in long-term stable home ranges in homogenous areas, but the property of being able to switch forage locations under changing conditions (tested as the ability to move between crop fields of different quality as the season progresses).\n
\n
Evaluation of forage quality was based on a combination of vegetation type, vegetation age, and vegetation structure.
Table 3 shows the food qualities for each vegetation type. 
Overall forage quality was determined by calculating the digestibility, and then multiplying this by an accessibility factor. 
Accessibility was determined by the height and structure of the vegetation. 
If the vegetation was denoted as 'patchy', meaning not uniformly dense (e.g. as a modern farm crop) and allowing free access to the hare, 
then accessibility was 1.0 up to 0.5 m height, decreasing by 0.00125 for each 0.01 m above 0.5 m. 
This simulates the availability of food growing in patches between the tall vegetation. 
If the vegetation was not patchy then accessibility decreased by 0.0125, resulting in and accessibility of zero at 1.3 m.\n
\n
<TABLE>
<TR>
<TD><B>Habitat Types</B></TD>	<TD><B>Accessibilty</B></TD>  <TD><B>Digestability</B></TD>
<TR>
<TD>Building, Freshwater, River, Saltwater, Coastline</TD>	<TD>-1</TD><TD>NA</TD>
<TR>
<TD>Coniferous Forest, Urban, Metalled Road, Excavations (e.g. Working gravel pits)</TD> <TD>1</TD><TD>0</TD>	
<TR>
<TD>DeciduousForest, MixedForest, RiversidePlants, RiversideTrees, Garden/Park, Track, Stone Wall, Hedges, Marsh, Pit Disused, Roadside Verge, Railway, Scrub </TD> <TD>0.5</TD><TD>0.5</TD>
<TR>
<TD>Fields, Managed grass areas, Hedgebanks, Semi-natural grass	Dependent on vegetation structure and height</TD>
<TD>0.0-1.0</TD>	<TD>Dependent on new:old growth ratio: 0.5-0.8</TD>
</TABLE>
\n
When a grid unit was foraged, the return in energy was given as the extraction rate (Hare_all.cpp::cfg_hare_ExtEff) multiplied by the digestibility and accessibility.\n
\n
<b>Dispersal:</b> The hare needs to move from its current location, and since the cause of this will be food shortage (unless at a later 
stage we introduce mate selection) then the best cue is food. This method tests food availability in all 8 directions at 100 random 
distances up to max_dispersal distance. It then picks the best one and moves there. The energy used in movement is assumed only to be 
for the one move - not the testing, but this is the way I decided to simulate eye-sight. (see Hare_Juvenile::st_Dispersal, 
Hare_Male::st_Dispersal & Hare_Female::st_Dispersal)\n
\n
THare::st_Dying: A necessary book-keeping state used to signal to the population manager, and possibly the mother, that the hare is dying. 
Subsequent communication with this object is rendered impossible prior to recovery of memory resources.\n
\n

\htmlonly <h3> 2.c.i Hare_Infant</h3>
\endhtmlonly

Hare_Infant::st_Developing: Growth occurred based upon the amount of milk supplied by the mother after subtracting metabolic requirements. 
If during this state the infant had a negative energy balance during four consecutive days, then the infant object transitioned to st_Dying. If the age of the object was >10days then there was a transition to Hare_Infant::st_NextStage.\n
Hare_Infant::st_NextStage: This state simply replaced this infant object with a 'young' object with the same state parameters as the infant 
(location, age, size, mother, consecutive days of negative energy).\n
\n
\htmlonly <h3> 2.c.ii	Hare_Young</h3>
\endhtmlonly

Hare_Young::st_Developing: See infant. Energy input comes from both the mother and foraging.\n
Hare_Young::st_NextStage: The young object is replaced with a juvenile object with the same state variable values.\n
Hare_Young::st_Foraging: The proportion of the daily maximum energetic requirement from forage is calculated by interpolation of the values 
given by <a href="page0.html#ref3">Hacklander et al (2002)</a>. The standard forage fuction (see THare::Forage) was called each day, allowing the young hare to forage up to a maximum energetic input, should time and conditions allow.
Foraging used time from the daily time budget of 1440 minutes, and added to energetic costs.\n
Hare_Young::st_Resting: This state is instantaneous and is called at the end of the day, before Hare_Young::st_Developing. 
It calculates the RMR and balances the daily time budget to 1440 minutes by assuming unused time was used for resting.\n
\n
\htmlonly <h3> 2.c.iii	Hare_Juvenile</h3>
\endhtmlonly

Hare_Juvenile::st_Developing: Energy input from foraging was converted to growth as described in section 1.3.1. Transtion to stNextStage occurred if the object reached the age of 180 days old during the breeding season, or 1yr old. This provided the possibility of early young breeding late in the autumn. Negative energy situations resulted in a transition to stDispersal. The starvation criterium was 14 consecutive days of negative energy balance in excess of 0.25% of RMR. If this occurred then there was a transition to THare::st_Dying.\n
Hare_Juvenile::st_NextStage: The juvenile object was replaced with a adult object with the same state parameter values. Whether the resulting adult was male or female was stochastic with a probability of 50% male.\n
Hare_Juvenile::st_Foraging: Foraging activity filled 66.6% of the daily activity period, less time spent interacting with other hares (see 2.5.3), or avoiding enemies. Foraging used time from the daily time budget of 1440 minutes, and added to energetic costs as well as inputs.\n
Hare_Juvenile::st_Resting: See young.\n
sHare_Juvenile::st_Dispersal: This state results in the movement of the hare a random distance between  100m and 1000m in one of 8 directions (N,NW,W etc.). All 8 locations are evaluated for forage quality and the best location chosen. The cost of locomotion between the two locations was calculated into the daily energy budget.\n
\n
\htmlonly <h3> 2.c.iv	Hare_Male</h3>
\endhtmlonly

Hare_Male::st_Developing: Energy input from foraging, less activity costs, were converted to growth. The starvation criterium was a fixed 
number of consecutive days of negative energy balance. If this occurred then there was a transition to THare::st_Dying. If the age of the 
hare reached its pre-determined physiological death point, then the hare object transitioned to THare::st_Dying.\n
Hare_Male::st_Foraging: see juvenile\n
Hare_Male::st_Resting: see young.\n
Hare_Male::st_Dispersal: see juvenile\n
\n
\htmlonly <h3> 2.c.v	Hare_Female</h3>
\endhtmlonly

Hare_Female::st_Developing: see male.\n
Hare_Female::st_Foraging: see juvenile\n
Hare_Female::st_Resting: see young.\n
Hare_Female::st_Dispersal: see juvenile\n
Hare_Female::st_ReproBehaviour: This state acted as a structural program control and was called once each day after st_Developing and was 
responsible for controlling oestrous, mating, gestation, giving birth, and lactation via the following states:\n
Hare_Female::UpdateOestrous: During the breeding season, an oestrous counter was incremented each day. On reaching 20 there was a 
transition to Hare_Female::Mating.\n
Hare_Female::Mating: If a male hare was within 1000m of the females location it was assumed that she was mated.
Hare_Female::UpdateGestation: This state was called for the 41 days following mating, after which HareFemale::GiveBirth was called. 
During gestation energetic costs of development of foetal mass were added to the daily energy budget.\n
Hare_Female::GiveBirth: One infant object is created for each leveret to be born. A counter for the number of days of lactation is set to 
zero and the state stLactation is called. The number of Hare_Infant objects born and their size is determined by the amount of foetal mass 
created up to this point. \n
Hare_Female::DoLactation: The maximum growth energy required by each infant or young from milk is communicated to the female. If the female 
has sufficient energy in her fat reserves or free energy foraged that day, then each leveret receives its maximum growth energy requirement. 
If the hare does not have enough spare energy for this, then that that she does have is equally shared between all leverets. The amount of 
energy received by the leveret is determined by milk energy/conversion and absorption factors.
\n

\htmlonly <h3>  2.d		Scheduling</h3>
\endhtmlonly

Hare objects interact with each other and their environment during the simulation run. Since the time-step of the model is one day, 
this means that interactions needs to be integrated into a single daily time-step. 
This gives rise to potential concurrency problems that were solved using a hierarchical structure to execution. 
This structure ensures that objects receive information and interact in a sensible order. 
This was particularly important in the case of lactating female hares who must first obtain enough energy to produce milk before the 
young are fed. Outside of the need for this kind of interaction control the order of execution of individual objects was random. 
Random execution order prevents the generation of bias between individuals, e.g. where the same individual would always obtain food first 
just by virtue of its position in a list of individuals. see <a href="#sb13">Interconnections</a> for more information.\n
\n

<HR> 
\htmlonly 
<h2><big>Design</big></h2>
\endhtmlonly

\htmlonly <h2> 3. Design Concepts</h2>
\endhtmlonly


\htmlonly <h3>  3.a Emergence</h3>
\endhtmlonly

Emergent properties were patterns of spatial distribution of hares as a result of exploitation of available resources and social interactions; demographic parameters, (hare population age structure, age-specific survival rates, population size and fluctuations); hare reproductive output (seasonal and annual); and individual hare weights.\n
\htmlonly <h3>  3.b. Adaptation</h3>
\endhtmlonly

Model hares show many adaptive traits. The primary emergent trait was associated with maintaining a favourable energetic status and involved triggered dispersal to find better feeding locations.\n
\n
Explicitly incorporated adaptations were:\n
&nbsp;&nbsp;&nbsp;&nbsp;   Restriction of the onset of breeding behaviour to the period March to September inclusive, preventing unnecessary use of resources for futile reproductive attempts.\n
&nbsp;&nbsp;&nbsp;&nbsp;   Restriction of breeding by females if energy depots were too low (< 3% bw consisting of fat reserves).\n
&nbsp;&nbsp;&nbsp;&nbsp;   Cessation of lactation and abandonment of young by the female if energetic requirements require the utilisation of body protein as an energy source.\n
\n
\htmlonly <h3> 3.c Fitness</h3>
\endhtmlonly

Fitness is based on the survial and reproduction of the hare. It is ultimatly an energetic concept.\n
\htmlonly <h3>  3.d Prediction</h3>
\endhtmlonly

Prediction is not used in the hare model.\n
\htmlonly <h3>  3.e Sensing</h3>
\endhtmlonly

Sensing/knowing mechanisms were modelled implicitly on the basis of rules; therefore, individuals were able to sense/know each variable accurately. Variables considered by hare objects in their adaptive decisions were:\n
\n
&nbsp;&nbsp;&nbsp;&nbsp;   Day of the year.\n
&nbsp;&nbsp;&nbsp;&nbsp;   Daily temperature\n
&nbsp;&nbsp;&nbsp;&nbsp;   Density of other hares within 256m\n
&nbsp;&nbsp;&nbsp;&nbsp;   Forage digestability and structure\n
&nbsp;&nbsp;&nbsp;&nbsp;   Available fat reserves\n
&nbsp;&nbsp;&nbsp;&nbsp;   Number of days of negative energy conditions\n
&nbsp;&nbsp;&nbsp;&nbsp;   Female hares <i>knew</i> the number and location of their own young during lactation\n
&nbsp;&nbsp;&nbsp;&nbsp;   Infant and young hares <i>knew</i> their daily requirements for milk energy based upon the pre-calculated values for age and size\n
&nbsp;&nbsp;&nbsp;&nbsp;   All hares were subject to the energetic calculations determining daily energetic costs from body size, temperature, and activity\n
\n
\htmlonly <h3>  3.f Interaction</h3>
\endhtmlonly

Interactions between model hares fell into two types, these being care of young and interference:\n
\n
\htmlonly <h3>   3.f.i Care of young </h3> \endhtmlonly
Care of young interactions were related to feeding of the young hares by the female during lactation. Young hares demanded their energetic requirements from the female at feeding, and the female supplied milk up to this requirement assuming she had the necessary energetic resources. If resources were limiting then the available resources were equally distributed between the young. If either the female or one of her young died they informed the others allowing them to take action accordingly (no longer waiting to be fed or not attempting to feed the dead young).\n
\n
\htmlonly <h3>    3.f.ii Interference  </h3> \endhtmlonly
Interference interactions occurred as a density-related effect based on the total number of hares within a 256 m diameter of the hare's precise location (256 was chosen for ease of binary operations). It was assumed that hares inside this area spent time interacting with each other, and therefore reduced the total amount of time available for other activities (feeding, resting). The amount of time taken was given by:
\f$I = 1 - e^{d.s}\f$
, where I is the proportion of time used interacting, d is the number of hares within 256m and s is a constant fitted as a fitting parameter. This relationship has the property of increasingly penalising hares as the local density increases.\n
\n
Interactions between hares and their environment were on the basis of information flow from the environment to the hare. The information content was of two types either (i) habitat type, vegetation structure (height and density), vegetation type, palatability, or (ii) it was information on management, e.g. if a field was cut for silage. The first type of information was used in determining movement and feeding behaviour, the second resulted in a probabilistic mortality dependent upon age and type of management. It was assumed that all juvenile and adult hares could escape from mechanical disturbance, whereas 50% of young and 100% of infants would be killed by any activity such as agricultural harvest, mowing or soil cultivation. Other operations that did not disturb the soil or vegetation (e.g. spraying, fertilizer treatment) were not considered as mortality sources. This relatively simple representation of agricultural mortality could easily be improved as more information becomes available since all activities are handled separately.\n
\htmlonly <h3> 3.g Stochasticity</h3>
\endhtmlonly

Stochasticity was used extensively within the model for applying probabilities:\n
&nbsp;&nbsp;&nbsp;&nbsp;   Initial locations of hares at simulation start.\n
&nbsp;&nbsp;&nbsp;&nbsp;   Daily chance of predation/death by other factors not modelled explicitly but controlled by two stage specific parameters for infant/young/juvenile, & adults. \n
&nbsp;&nbsp;&nbsp;&nbsp;   Foraging movement within homogenous areas and the probability of returning towards the centre of the current home range.\n
&nbsp;&nbsp;&nbsp;&nbsp;   Selection of a litter size from a distribution of litter sizes.\n
&nbsp;&nbsp;&nbsp;&nbsp;   The probability of female sterility.\n
&nbsp;&nbsp;&nbsp;&nbsp;   Determining the sex of a newborn individual.\n
&nbsp;&nbsp;&nbsp;&nbsp;   The probability of having to 'escape' a threat, and therefore use energy on running.\n
&nbsp;&nbsp;&nbsp;&nbsp;   Determining the daily distance dispersed, should dispersal behaviour be initiated.\n
&nbsp;&nbsp;&nbsp;&nbsp;   Determining variation in physiological life-span.\n
\n
\htmlonly <h3>  3.h Collectives</h3>
\endhtmlonly

All hares were managed as lists of objects by the THare_Population_Manager. This object was responsible for initiating daily activity for all hares, and for output of population information as required.\n
\htmlonly <h3>  3.i Observation</h3>
\endhtmlonly

Since each hare object is managed through the Hare Population Manager, it is relatively easy to extract information from a single hare, or any set of hares. Information is collected by writing C++ probes that simply sort through the list of hares and extract information from those matching given criteria, e.g. output of the weights and ages of all female hares. Hence information is easily available at the population to individual level.\n
\n
\htmlonly <h2> 4. Initialisation</h2>
\endhtmlonly

The model was initiated with a starting population of 200 hares per km<SUP>2</SUP> each randomly placed inside the landscape in terrestrial habitats. The hares were aged 1 year old and were given the mean expected weight of a hare of that age. The sex ratio was 1:1. The initial allocation of crops to fields varied between runs of the same simulation but always followed the farm rotation rules. Simulations were started on the 1st of January each year.\n
\htmlonly <h2> 5. Inputs </h2>
\endhtmlonly

Primary inputs to the model are the landscape structure, weather and farming practices as detailed in section 1.b.ii. Values for hare model parameters are either detailed in the model descriptions, or were a result of fitting during model testing\n


<a name="sb13">
\htmlonly <h2> 6. Interconnections</h2>
\endhtmlonly
Apart from connections to the Landscape, Farm and Crop classes, in common with all other ALMaSS models the hare classes are administered by a
population manager class, in this case THare_Population_Manager. 
The THare_Population_Manager is descended from Population_Manager and performs the role of an auditor in the simulation. 
\n
\htmlonly <h3>  6.a	The Hare Population Manager</h3>
\endhtmlonly
The population manager keeps track of individual hare objects (infant, young, juvenile, male, and female) in the model and initiates their 
daily behaviour via management of lists. The Population_Manager::Run method is the main daily behaviour control functions splitting each 
day into 5 steps as follows: \n
1. At the start of each time step all hare objects within each list are randomised to avoid concurrency problems. \n
2. After randomisation the BeginStep of each object in each list was called. 
Hierarchical execution started with the list of infant objects and ended with the execution of female objects. \n
3. The  Step function for each object in the same way as the BeginStep. The Step code of hares differs from BeginStep 
and EndStep in that it is repeatedly called by the population manager until all hares signal that they have finished Step behaviours 
for that day. This allows linking of behaviours and more flexibility in terms of daily activities.\n
4. When all objects had completed their Step activity, the EndStep function was called for all objects. Behaviours here are those that may depend on other objects or self carrying out activities earlier in the day. The best example is 
development of young hares happens here after it is certain that if they are going to be, then they have been fed by the female in Step (see Hare_Infant::EndStep). \n
5. Any hare objects representing hares that died were removed from the simulation and any output requests handled.\n
\n
Hare that were born in the simulation during these steps were initiated by the population manager and their initial parameter values were set.
The physiological life-span was set at this time using a range of 8-12.5 years, based on <a href="page0.html#ref2">Pielowski (1971)</a>.\n
\n
Specialist output of results is primarily done by this class in THare_Population_Manager::DoFirst & THare_Population_Manager::DoLast, calling 
THare_Population_Manager::POMOutputs & THare_Population_Manager::MRROutputs. Standard output is done by the Population_Manager called from
main simulation loop Population_Manager::Run. 

THare_Population_Manager is also responsible for precalcuation of many of 
the energetic or density-dependent pre-requisites for the simulation in THare_Population_Manager::Init.\n
\n

*/