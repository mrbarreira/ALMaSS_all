/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
//---------------------------------------------------------------------------

#include <cmath>
#include <iostream>
#include <fstream>
#ifdef __UNIX
#undef max
#endif
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Hare/hare_all.h"

#include "../BatchALMaSS/BoostRandomGenerators.h"
extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;

//---------------------------------------------------------------------------

extern CfgFloat cfg_hare_ExtEff;
extern double g_hare_peg_inertia;
extern double g_FarmIntensiveness;

CfgInt cfg_hare_pegmoveto_chance("HARE_PEGMOVETOCHANCE", CFG_CUSTOM, 250000);
CfgFloat cfg_hare_pesticide_contact_exposure_rate("HARE_PESTICIDE_CONTACT_RATE", CFG_CUSTOM, 0.0);
CfgFloat cfg_hare_pesticide_ingestion_exposure_rate("HARE_PESTICIDE_INGESTION_RATE", CFG_CUSTOM, 0.0);
#define __FSQRS 10

//---------------------------------------------------------------------------
//       THARE CODE
//---------------------------------------------------------------------------

double THare::Forage(int &a_time)
{
	/**
	* This could be implemented in each hare's st_Foraging,  but is implemented as a common method because all the hares do it the same way. This method determine how much energy can be foraged from the current location in the time available (a_time). \n
	* It is important that energy used for foraging is deducted from the energybudget calculations
	* before calculating the target - otherwise growth will be affected. \n
	* Efficiency can be roughly calculated for optimal forage by calculating the number squares and the cost of
	* foraging together with the return.  e.g. 100m2 -> 10m2 forage with ext of 5.0 = 50KJ at a cost of 100*m_KJForaging. 	 
	* Hence return can be calculated and the target adjusted accordingly\n
	*/
	#define __SQRS 100 // __SQRS is the number of m2 foraged per forage square
	int y_add[8] = { -10, -10, 0, 10, 10, 10, 0, -10 }; // N,NE,E,SE,S,SW,W,NW
	int x_add[8] = {  0,  10, 10, 10, 0, -10, -10, -10 }; // N,NE,E,SE,S,SW,W,NW
	unsigned next=0;
	double food[8];
	int bestsquare = 0;
	/**
	* m_EnergyMax is the max amount of energy possible in one day (intake/cfg_hare_ExtEff to reduce calculations). This needs to be no more than they ever need,
	* the closer to this limit the faster the code will run due to avoiding unnecessary searching, but also will prevent 
	* high energy usage being offset by too high intake.
	* m_EnergyMax is set at the beginnng of each day in BeginStep.
	*/
	
	// Get the top corner of our square
	int x=m_Location_x;
	int y=m_Location_y;
	// Search central square
	double TotalFood = ForageSquare(x,y); // this calls walking automatically
	a_time -= __SQRS;
	EnergyBalance(activity_Foraging, __SQRS * 5); // Assume a 20cm swath, therefore distance = 5 * m2
	if ((a_time - __SQRS)<0)
	{
		return TotalFood;
	}
	// Set up searching for the next 8 squares around this one
	food[0]=0;
	int equal=0;
	double best=-1;
	for (int i=0; i<8; i++) 
	{
		x = m_Location_x+x_add[i];
		y = m_Location_y+y_add[i];
		food[i]=ForageSquare(x,y);
		if (food[i]>best) {
			best=food[i];
			equal=0;
			bestsquare=i;
		} else if (food[i]==best) 
		{
			equal++;
		}
	}
	if (equal>0) 
	{ 
		// No real direction to move in, so go towards our peg if this is OK
		unsigned pd=(GetPegDirection());
		// Our normal directions are 0-7 N,NE,E,SE,S,SW,W,NW		//
		if (food[pd]== best) bestsquare=pd;
		else {
			pd= (pd-1) & 0x07;
			if (food[pd]== best) bestsquare=pd;
			else {
				// First check the directions
				bestsquare=-1;
				while (bestsquare==-1) 
				{
					int t=random(8);
					if (food[t]==best) bestsquare=t;
				}
			}
		}
	}
	// Now move to the next square, also the best in food return
	a_time -= __SQRS;
	EnergyBalance(activity_Foraging, __SQRS * 5);
	Walking(10, bestsquare);
	TotalFood+=food[bestsquare];
	if (TotalFood>=m_EnergyMax) 
	{
		return TotalFood;
	}
	// OK now dependent upon the direction bestsquare, we need to check 3 or 5 new squares for food
	while (a_time>0) 
	{
		int nsqs;
		int subtract=1+(bestsquare & 1);
		if ( subtract == 1) nsqs = 3; else nsqs = 5;
		for (int j=0; j<nsqs; j++) {
			next=((bestsquare-subtract)+j) & 7;
			x = m_Location_x+x_add[next];
			y = m_Location_y+y_add[next];
			food[j]=ForageSquare(x,y);
			TotalFood+=food[j];
			a_time -= __SQRS;
			EnergyBalance(activity_Foraging, __SQRS*5);
		}
		// Choose the best
		int found=0;
		//bestsquare=0;
		for (int j=1; j<nsqs; j++) {
			if (food[j]>food[found]) found=j;
			else if (food[j]==food[found]) {
			  if (random(3)==0) {
				  found=j;
			  }
		  }
		}
		bestsquare=((bestsquare-subtract)+found) & 7;
		if (GetPegPull()> random(cfg_hare_pegmoveto_chance.value())) 
		{
			bestsquare=(GetPegDirection());
			// Our normal directions are 0-7 N,NE,E,SE,S,SW,W,NW
		}
		//
		Walking(10,bestsquare);
		if (TotalFood>m_EnergyMax) {
			return TotalFood;
		}
	}
	if (TotalFood > m_EnergyMax)
	{
		TotalFood = m_EnergyMax;
	}
	return TotalFood;
}
//---------------------------------------------------------------------------

double THare::ForageP(int &a_time)
{
	/**
	* This could be implemented in each hare's st_Foraging,  but is implemented as a common method because all the hares do it the same way. 
	* This method determine how much energy can be foraged from the current location in the time available (a_time). \n
	* It is important that energy used for foraging is deducted from the energybudget calculations
	* before calculating the target - otherwise growth will be affected. \n
	* Efficiency can be roughly calculated for optimal forage by calculating the number squares and the cost of
	* foraging together with the return.  e.g. 100m2 -> 10m2 forage with ext of 5.0 = 50KJ at a cost of 100*m_KJForaging.
	* Hence return can be calculated and the target adjusted accordingly\n
	* The only difference between this method and THare::Forage is that this one calls the version of ForageSquare that also calculates pesticide exposure.
	*/
	
	#define __SQRS 100 // __SQRS is the number of m2 foraged per forage square
	int y_add[8] = { -10, -10, 0, 10, 10, 10, 0, -10 }; // N,NE,E,SE,S,SW,W,NW
	int x_add[8] = { 0, 10, 10, 10, 0, -10, -10, -10 }; // N,NE,E,SE,S,SW,W,NW
	unsigned next = 0;
	double food[8];
	double pesticide[8];
	int bestsquare = 0;
	/**
	* m_EnergyMax is the max amount of energy possible in one day. This needs to be no more than they ever need,
	* the closer to this limit the faster the code will run due to avoiding unnecessary searching, but also will prevent
	* high energy usage being offset by too high intake.
	* m_EnergyMax is set at the beginnng of each day in BeginStep.
	*/

	// Get the top corner of our square
	int x = m_Location_x;
	int y = m_Location_y;
	// Search central square
	double PesticideExposure = 0.0;
	double TotalFood = ForageSquareP(x, y, &PesticideExposure); // this calls walking automatically
	a_time -= __SQRS;
	EnergyBalance(activity_Foraging, __SQRS * 5); // Assume a 20cm swath, therefore distance = 5 * m2
	if ((a_time - __SQRS)<0)
	{
		m_pesticide_burden += PesticideExposure;
		return TotalFood;
	}
	// Set up searching for the next 8 squares around this one
	food[0] = 0;
	int equal = 0;
	double best = -1;
	double tempP;
	for (int i = 0; i<8; i++)
	{
		x = m_Location_x + x_add[i];
		y = m_Location_y + y_add[i];
		tempP = 0.0;
		food[i] = ForageSquareP(x, y, &tempP);
		pesticide[i] = tempP;
		if (food[i]>best) {
			best = food[i];
			equal = 0;
			bestsquare = i;
		}
		else if (food[i] == best)
		{
			equal++;
		}
	}
	if (equal>0)
	{
		// No real direction to move in, so go towards our peg if this is OK
		unsigned pd = (GetPegDirection());
		// Our normal directions are 0-7 N,NE,E,SE,S,SW,W,NW		//
		if (food[pd] == best) bestsquare = pd;
		else {
			pd = (pd - 1) & 0x07;
			if (food[pd] == best) bestsquare = pd;
			else {
				// First check the directions
				bestsquare = -1;
				while (bestsquare == -1)
				{
					int t = random(8);
					if (food[t] == best) bestsquare = t;
				}
			}
		}
	}
	// Now move to the next square, also the best in food return
	Walking(10, bestsquare);
	a_time -= __SQRS;
	EnergyBalance(activity_Foraging, __SQRS * 5);
	TotalFood += food[bestsquare];
	PesticideExposure += pesticide[bestsquare];
	if (TotalFood >= m_EnergyMax)
	{
		m_pesticide_burden += PesticideExposure;
		return TotalFood;
	}
	// OK now dependent upon the direction bestsquare, we need to check 3 or 5 new squares for food
	while (a_time>0)
	{
		int nsqs;
		int subtract = 1 + (bestsquare & 1);
		if (subtract == 1) nsqs = 3; else nsqs = 5;
		for (int j = 0; j<nsqs; j++) {
			next = ((bestsquare - subtract) + j) & 7;
			x = m_Location_x + x_add[next];
			y = m_Location_y + y_add[next];
			food[j] = ForageSquareP(x, y, &PesticideExposure);
			TotalFood += food[j];
			a_time -= __SQRS;
			EnergyBalance(activity_Foraging, __SQRS * 5);
		}
		// Choose the best
		int found = 0;
		//bestsquare=0;
		for (int j = 1; j<nsqs; j++) {
			if (food[j]>food[found]) found = j;
			else if (food[j] == food[found]) {
				if (random(3) == 0) {
					found = j;
				}
			}
		}
		bestsquare = ((bestsquare - subtract) + found) & 7;
		if (GetPegPull()> random(cfg_hare_pegmoveto_chance.value()))
		{
			bestsquare = (GetPegDirection());
			// Our normal directions are 0-7 N,NE,E,SE,S,SW,W,NW
		}
		//
		Walking(10, bestsquare);
		if (TotalFood>m_EnergyMax) {
			m_pesticide_burden += PesticideExposure;
			return TotalFood;
		}
	}
	if (TotalFood > m_EnergyMax)
	{
		TotalFood = m_EnergyMax;
	}
	m_pesticide_burden += PesticideExposure;
	return TotalFood;
}
//---------------------------------------------------------------------------

double THare::ForageSquare(int a_x, int a_y)
{
	int polyref;
	double food=0.0;
	int sw=m_OurLandscape->SupplySimAreaWidth();
	int sh=m_OurLandscape->SupplySimAreaHeight();
	// Do the search and return the value
	// Now we need the irritating boundary check
	if ((a_x >= sw-10) || (a_y > sh-10) || (a_x < 0 ) || (a_y < 0)) 
	{
		// We check __FSQRS% of the cell and assume that the hare can find forage matching the best square
		for (int i=0; i<__FSQRS; i++) 
		{
			// Here is the boundary check
			int x = (sw+a_x+random(10)) % sw;
			int y = (sh+a_y+random(10)) % sh;
			// All OK now, so get the polygon information and food
			polyref=m_OurLandscape->SupplyPolyRef(x,y);
			double f=m_OurPopulationManager->GetPolyFood(polyref);
			if (f==-1.0) 
			{
				f = m_OurLandscape->GetHareFoodQuality(polyref)* m_vegPalatability[m_OurLandscape->SupplyVegType(polyref)];
				m_OurPopulationManager->SetPolyFood(polyref,f);
			}
			 food+=f; //Changed foraging methods - now find the best sample cell and assume the whole square is this good - allows focus in foraging
			//if (f > food) food = f;

		}
	} else 
	{
		for (int i=0; i<__FSQRS; i++) 
		{
			int x = a_x+random(10);
			int y = a_y+random(10);
			polyref=m_OurLandscape->SupplyPolyRef(x,y);
			double f=m_OurPopulationManager->GetPolyFood(polyref);
			if (f==-1.0) 
			{
				f = m_OurLandscape->GetHareFoodQuality(polyref)* m_vegPalatability[m_OurLandscape->SupplyVegType(polyref)];
				m_OurPopulationManager->SetPolyFood(polyref,f);
			}
			food+=f; //Changed foraging methods - now find the best sample cell and assume the whole square is this good - allows focus in foraging
			//if (f > food) food = f;
		}
	}
	return food * 10 * cfg_hare_ExtEff.value(); // Assume they eat from all 100 m2 at this rate
}
//---------------------------------------------------------------------------


double THare::ForageSquareP(int a_x, int a_y, double * a_pesticideexposure)
{
	int polyref;
	double somepesticide = 0.0;
	double food = 0.0;
	int sw = m_OurLandscape->SupplySimAreaWidth();
	int sh = m_OurLandscape->SupplySimAreaHeight();
	// Do the search and return the value
	// Now we need the irritating boundary check
	if ((a_x >= sw - 10) || (a_y > sh - 10) || (a_x < 0) || (a_y < 0))
	{
		// We check __FSQRS% of the cell and assume that the hare can find forage matching the best square
		for (int i = 0; i<__FSQRS; i++) 
		{
			// Here is the boundary check
			int x = (sw + a_x + random(10)) % sw;
			int y = (sh + a_y + random(10)) % sh;
			// All OK now, so get the polygon information and food
			polyref = m_OurLandscape->SupplyPolyRef(x, y);
			somepesticide += m_OurLandscape->SupplyPesticideP(x, y, ppp_1);
			double f = m_OurPopulationManager->GetPolyFood(polyref);
			if (f == -1.0) 
			{
				f = m_OurLandscape->GetHareFoodQuality(polyref)* m_vegPalatability[m_OurLandscape->SupplyVegType(polyref)];
				m_OurPopulationManager->SetPolyFood(polyref, f);
			}
			food += f; //Changed foraging methods - now find the best sample cell and assume the whole square is this good - allows focus in foraging
		}
	}
	else
	{
		for (int i = 0; i<__FSQRS; i++)
		{
			int x = a_x + random(10);
			int y = a_y + random(10);
			polyref = m_OurLandscape->SupplyPolyRef(x, y);
			somepesticide += m_OurLandscape->SupplyPesticideP(x, y, ppp_1);
			double f = m_OurPopulationManager->GetPolyFood(polyref);
			if (f == -1.0) 
			{
				f = m_OurLandscape->GetHareFoodQuality(polyref)* m_vegPalatability[m_OurLandscape->SupplyVegType(polyref)];
				m_OurPopulationManager->SetPolyFood(polyref, f);
			}
			food += f; 
		}
	}
	// somepesticide now has the pesticide exposure in units per 10m2, but we need 100m and there are two routes with different rates of intake, so first X10
	somepesticide *= 10;
	// contact exposure
	(*a_pesticideexposure) += somepesticide * cfg_hare_pesticide_contact_exposure_rate.value();
	// ingestion exposure
	(*a_pesticideexposure) += somepesticide * cfg_hare_pesticide_ingestion_exposure_rate.value();
	return food * 10 * cfg_hare_ExtEff.value(); // Assume they eat from all 100 m2 at this rate
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//                          Peg Related Code
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

inline int THare::GetPegDistance() 
{
/**
* Daily movement is limited by a peg which does not really want to move, but can be dragged slowly. 
* This requires a set of rapid functions because they will be used a lot. NB dispersal takes no account of the peg.\n
* Calculates the distance from our current x,y to the peg
* We must avoid sqrt and the like so our distance is in rectangular co-ordinates. 
* This is complicated by the fact that the peg and hare may be on opposite sides of the world due to wrap around.
*/
	  int d1=abs(m_Location_x-m_peg_x);
	  if (d1 > (int) m_OurPopulationManager->SimWH) d1 = abs(d1-m_OurPopulationManager->SimW);
	  int d2=abs(m_Location_y-m_peg_y);
	  if (d2 > (int) m_OurPopulationManager->SimHH) d2 = abs(d2-m_OurPopulationManager->SimH);
	  return d1+d2;
}

inline int THare::GetPegPull() 
{
	/**
	* Returns the squared rectangular distance from the peg
	*/
	int d = GetPegDistance();
		return d*d;
}

inline int THare::GetPegDirection() 
{
	/**
	Daily movement is limited by a peg which does not really want to move, but can be dragged slowly. This requires a set of rapid functions because they will be used a lot. NB dispersal takes no account of the peg.\n
	Figures out the closest direction to peg.\n
	1 NE, 3 SE, 5 SWest & 7 NW\n
	only uses 4 returns because this is very fast and relatively simple - 8 directions would be quite difficult to implement fast\n
	This is also complicated by the fact that the peg can be at the other end of the world due to wrap around.\n
	*/
	int d1 = m_Location_x - m_peg_x;
	  int d2 = m_Location_y - m_peg_y;
	  if ((d1 == 0) && (d2 == 0)) return random(8);
	  if (abs(d1) > (int)m_OurPopulationManager->SimWH)
	  {
		  if (d1<0) 
		  {
			  if ( abs(d2) > (int) m_OurPopulationManager->SimHH) 
			  {
				  if (d2<0) return 7; //NW;
				  else return 5; //SW;
			  }
			  else 
			  {
				  if (d2<0) return 5; //SW;
				  else return 7; //NW;
			  }
		  }
		  else 
		  {
			  if ( abs(d2) > (int) m_OurPopulationManager->SimHH) {
				  if (d2<0) return 1; //NE;
				  else return 3; //SE;
			  }
			  else {
				  if (d2<0) return 3; //SE;
				  else return 1; //NE;
			  }
		  }
	  }
	  else 
	  {
		  if (d1<0) 
		  {
			  if ( abs(d2) > (int) m_OurPopulationManager->SimHH) 
			  {
				  if (d2<0) return 1; //NE;
				  else return 3; //SE;
			  }
			  else {
				  if (d2<0) return 3; //SE;
				  else return 1; //NE;
			  }
		  }
		  else {
			  if ( abs(d2) > (int) m_OurPopulationManager->SimHH) {
				  if (d2<0) return 7; //NW;
				  else return 5; //SW;
			  }
			  else {
				  if (d2<0) return 5; //SW;
				  else return 7; //NW;
			  }
		  }
	  }
}


  void THare::MovePeg() 
{
	  /**
	  Daily movement is limited by a peg which does not really want to move, but can be dragged slowly. This requires a set of rapid functions because they will be used a lot. NB dispersal takes no account of the peg.\n
	  Moves the peg towards the location, the speed of movement is dependent upon an inertia parameter.\n
	  Once again we have the problem of wrap around.\n
	  */
	  //
	  // Find the distance between the peg & x,y and add a fixed proportion to move peg
	  int d1=(m_Location_x-m_peg_x);
	  if (abs(d1)< (int) m_OurPopulationManager->SimWH) m_peg_x += int(d1*g_hare_peg_inertia); // Global used for speed
	  else {
		  if (d1<0) d1+=m_OurPopulationManager->SimWH; else d1-=m_OurPopulationManager->SimWH;
		  m_peg_x += int(d1*g_hare_peg_inertia);
	  }
	  d1=(m_Location_y-m_peg_y);
	  if (abs(d1)< (int) m_OurPopulationManager->SimHH) m_peg_y += int(d1*g_hare_peg_inertia);
	  else {
		  if (d1<0) d1+=m_OurPopulationManager->SimHH; else d1-=m_OurPopulationManager->SimHH;
		  m_peg_y += int(d1*g_hare_peg_inertia);
	  }
  }
  //-------------------------------------------------------------------------------------------------------------------------

  void THare::InternalPesticideHandlingAndResponse()
  {
	  /**
	  * This method needs to be re-implemented for any class which has pesticide response behaviour. e.g. see Hare_Female::InternalPesticideHandlingAndResponse
	  */
	  m_pesticide_burden *= m_pesticidedegradationrate; // Does nothing by default except internal degredation of the pesticide
  }
  //-------------------------------------------------------------------------------------------------------------------------

