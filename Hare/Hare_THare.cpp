/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
//---------------------------------------------------------------------------

#include <cmath>
#include <iostream>
#include <fstream>
#ifdef __UNIX
#undef max
#endif
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Hare/hare_all.h"


#include "../BatchALMaSS/BoostRandomGenerators.h"
extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;


//---------------------------------------------------------------------------

extern CfgInt cfg_Hare_Recovery_Time;
// Physiological life-span
extern CfgInt cfg_hare_max_age;
extern CfgInt cfg_hare_max_age_var;
// extern CfgFloat cfg_Hare_StdKJWalking;
// extern CfgFloat cfg_Hare_StdKJForaging;
extern CfgInt cfg_hare_escape_dist;
extern CfgFloat cfg_Hare_StdSpeedWalking;
extern CfgFloat cfg_Hare_StdSpeedRunning;
extern CfgFloat cfg_ForageRestingRatio;
extern CfgFloat cfg_hare_ExtEff;
extern CfgInt cfg_hare_adult_predation;
extern CfgInt cfg_hare_minimum_breeding_weight;
static CfgFloat cfg_hare_lowfatextramort("HARE_LOWFATEXTRAMORT", CFG_CUSTOM, 10.0);
static CfgFloat cfg_hare_pesticidedegradationrate("HARE_PESTICIDEDEGRADATIONRATE", CFG_CUSTOM, 0.0);


//---------------------------------------------------------------------------
//       THARE CODE
//---------------------------------------------------------------------------
/**
THare constructor code
*/
THare::THare(int p_x, int p_y,
	Landscape* p_L, THare_Population_Manager* p_PPM) : TAnimal(p_x, p_y, p_L)
{
	THareInit(p_x, p_y, p_PPM);
}
//---------------------------------------------------------------------------
void THare::THareInit(int p_x, int p_y, THare_Population_Manager* p_PPM)
{
  // The THare constructor - only needs to know about the Hare Population
  // Manager
  m_OurPopulationManager=p_PPM;
  m_CurrentHState=tohs_InitialState;
  // Set the lifespan
  m_Lifespan=cfg_hare_max_age.value()+(random(cfg_hare_max_age_var.value()*2)-cfg_hare_max_age_var.value());
  // Energetic values - these can be overwritten by the constructor of
  // descendent classes
  m_KJRunning=0;
  m_KJWalking=0;
  m_KJForaging=0;
  m_SpeedRunning=cfg_Hare_StdSpeedRunning.value();
  m_SpeedWalking=cfg_Hare_StdSpeedWalking.value();
  m_fatReserve=0;
  m_StarvationDays=0;
  m_TodaysEnergy=0;
  //m_in_dispersal = false;
  //m_TimePerForageSquare= 13 * 13; // 24 squares
  m_ActivityTime=1440; // In the case of object creation, this cannot be set in the begin step
  m_foragingenergy=0.001; // a bit more than 0
  m_peg_x=p_x;
  m_peg_y=p_y;
  m_lastYearsDensity=0;
  m_experiencedDensity=0;
  for (int i=0; i<365; i++) m_expDensity[i]=0;
  m_DensitySum=0;
  m_ddindex=0;
  m_RefNum=p_PPM->GetHareRefNum();
  m_pesticideInfluenced1 = false;
  m_pesticide_burden = 0.0;
  m_pesticidedegradationrate = cfg_hare_pesticidedegradationrate.value(); // default of 0.0 will remove all body burden pesticide at the end of each day
}
//---------------------------------------------------------------------------

/**
THare destructor
*/
THare::~THare()
{
}
//---------------------------------------------------------------------------

void THare::loadVegPalatability()
{
	m_vegPalatability = new double[ tov_Undefined ];
	// First fill in default value
	for (int i = 0; i < tov_Undefined; i++) m_vegPalatability[ i ] = 1.0;
	// Now set any special values
	m_vegPalatability[ tov_Potatoes ]			= 0.1;
	m_vegPalatability[ tov_PotatoesIndustry ]	= 0.1;
	m_vegPalatability[ tov_PlantNursery ]		= 0.1;
	m_vegPalatability[ tov_NoGrowth ]			= 0.0;
	m_vegPalatability[ tov_None ]				= 0.0;
	m_vegPalatability[ tov_OPotatoes ]			= 0.1;
	m_vegPalatability[ tov_Carrots ]			= 1.5;
	m_vegPalatability[ tov_OCarrots ] 			= 1.5;
	/*
	m_vegPalatability[tov_FodderBeet] = 1.25;
	m_vegPalatability[tov_SugarBeet] = 1.25;
	m_vegPalatability[tov_OFodderBeet] = 1.25;
	*/
}
//---------------------------------------------------------------------------

TTypeOfHareState THare::st_Dispersal()
{
  /**
  This function is really a dummy - but it needs to be here. \n
  The return values is meaningless, but stops the compiler complaining
  */
  return tohs_InitialState;
}
//---------------------------------------------------------------------------
/**
Moves the hare and alters the energy and time budget to simulate running
 /n
 All proximity alerts or other causes of running for all hares are sent via this code - the code then calls a movement code and energetic code. However these are may not the same for all hare types, so if there is a specific routine for a type, that is called, if not the default is used - there is no need to worry about this as the calling program.
*/
void THare::Running(int a_max_dist)
{
   int dist;
   bool b;
   // The length of this loop can cause problems, hence in very small landscapes max_dist has to be small. A minimum distance was not used because this invites infinite looping.
   do {
    // Choose a distance......
	dist=random(a_max_dist);
	// and see if it is possible to go there in a random direction
	b=Run(dist, random(8));
   } while (b==false);
   // Now adjust our energy balance
   EnergyBalance(activity_Running, dist);
   TimeBudget(activity_Running, dist);
   TimeBudget(activity_Recovery, cfg_Hare_Recovery_Time.value());
}
//---------------------------------------------------------------------------
/**
Do the housekeeping necessary to die
*/
void THare::st_Dying() {
	if (m_MyMum) m_MyMum->ON_YoungKilled(this);
	m_CurrentHState=tohs_DestroyObject;
	m_CurrentStateNo=-1;
	m_MyMum=NULL;
	m_StepDone=true;
}
//---------------------------------------------------------------------------
/**
Moves the hare and alters the energy budget to simulate walking. \n
*/
void THare::Walking(int a_dist, int a_direction)
{
	while (!Run(a_dist, a_direction)) {
		a_direction=random(8);
		//a_dist=random(500); // This is just for cases like Illum� where it is possible to travel too far in all directions
		a_dist--; // This is because hares can get stuck otherwise.
	}
}
//---------------------------------------------------------------------------

bool THare::Run(int a_dist, int a_direction)
{
  // Need to move a_dist in direction a_direction
  int Width=m_OurPopulationManager->SimW;
  int Height=m_OurPopulationManager->SimH;
  int tx=m_Location_x;
  int ty=m_Location_y;
  switch(a_direction)
  {
    case 0: // North
      ty=m_Location_y-a_dist;
      if (ty<0) ty+=Height;
      break;
    case 1: // NorthWest
      // OK, OK I know, I should reduce the distance for the diagonal - but it
      // costs two multiplications if I do, and this is used a lot
      ty=m_Location_y-a_dist;
      tx=m_Location_x+a_dist;
      if (ty<0) ty+=Height;
      if (tx>=Width) tx-=Width;
      break;
    case 2: // West
      tx=m_Location_x+a_dist;
      if (tx>=Width) tx-=Width;
      break;
      case 3: // SouthWest
        ty=m_Location_y+a_dist;
        tx=m_Location_x+a_dist;
        if (tx>=Width) tx-=Width;
        if (ty>=Height) ty-=Height;
        break;
    case 4: // South
      ty=m_Location_y+a_dist;
      if (ty>=Height) ty-=Height;
      break;
    case 5: // SouthEast
      ty=m_Location_y+a_dist;
      tx=m_Location_x-a_dist;
      if (tx<0) tx+=Width;
      if (ty>=Height) ty-=Height;
      break;
    case 6: // East
      tx=m_Location_x-a_dist;
      if (tx<0) tx+=Width;
      break;
    case 7: // NorthEast
      ty=m_Location_y-a_dist;
      tx=m_Location_x-a_dist;
      if (tx<0) tx+=Width;
      if (ty<0) ty+=Height;
      break;
	default: // No direction
	  break;

  }
  // Now just before we do anything, check that we can go to tx,ty
  	  TTypesOfLandscapeElement habitat = m_OurLandscape->SupplyElementType(tx,ty);
	  switch (habitat) {
		// Impossible stuff
		case tole_Building:
		case tole_Pond:
		case tole_Freshwater:
		case tole_River:
		case tole_Saltwater:
		case tole_Coast:
		case tole_BareRock:
		case tole_UrbanNoVeg:
		case tole_UrbanPark:
		case tole_SandDune:
		case tole_Stream:
			return false;
			break;
		default:
			m_Location_x=tx;
			m_Location_y=ty;
			return true;
			break;
	  }
  // I know I can save code by only testing for boundary conditions
  // at the end, but 50% of the time this will involve an unnecessary test and
  // speed usually being the problem I think it is better this way
}
//---------------------------------------------------------------------------

void THare::EnergyBalance(TTypeOfActivity a_activity, int dist)
{
  /** This method calculates energy used and subtracts this from the fat store. \n
   The fat store is KJ so no conversion is required here.
   The fat store can only be a maximum X% of the body weight
  */
  switch (a_activity)
  {
    case activity_Resting:
      m_TodaysEnergy-=m_OurPopulationManager->GetRMR(m_Age,GetTotalWeight());
      break;
    case activity_Running:
      // Note here it is the fatReserve that is altered
      // This is because this activity could be called at anytime, and we
      // do not know if m_TodaysEnergy is set to anything sensible. fatReserve is
      // always sensible though. Note this will potentially influence behaviour
      // if running causes a low fatReserve.
      m_fatReserve-=dist*m_KJRunning;    // dist in metres
      break;
    case activity_Dispersal:
      m_TodaysEnergy-=dist*m_KJWalking;  // dist in metres
      break;
    case activity_Walking:
      m_TodaysEnergy-=dist*m_KJWalking;  // dist in metres
      break;
    case activity_Foraging:
      m_TodaysEnergy-=dist*m_KJForaging; // dist in minutes
	  // Checking code
	  m_foragingenergy+=dist*m_KJForaging;
      break;
    default:
      m_OurLandscape->Warn("THare::EnergyBalance - unknown activity",NULL);
      exit(1);
  }
}
//---------------------------------------------------------------------------

void THare::TimeBudget(TTypeOfActivity a_activity, int dist)
{
  /**
  This method calculates time used for each activity and subtracts this from the day
  */
  switch (a_activity)
  {
    case activity_Resting:
      m_ActivityTime-=dist; // dist is in time here (minutes)
      break;
    case activity_Running:
      m_ActivityTime-=(int)(dist/m_SpeedRunning);  // dist in metres
      break;
    case activity_Walking:
      m_ActivityTime-=(int)(dist/m_SpeedWalking); // dist in metres
      break;
    case activity_Foraging:
      m_ActivityTime-=dist; // dist in minutes
      break;
    case activity_Dispersal:
		m_ActivityTime -= (int)(dist / m_SpeedWalking); // dist in metres 
      break;
    case activity_Recovery:
      m_ActivityTime-=dist; // dist in minutes
      break;
    default:
      m_OurLandscape->Warn("THare::TimeBudget - unknown activity",NULL);
      exit(1);
  }
}
//---------------------------------------------------------------------------

/**
Gets the hare RMR based on age and mass
*/
double THare::GetRMR() {
	   return m_OurPopulationManager->GetRMR(m_Age,GetTotalWeight());
}
//---------------------------------------------------------------------------

/**
A mortality test. Also include optional code for size and low fat reserve increased mortality - tested under the POM
*/
bool THare::WasPredated() {
	// This is only called by adult states, so don't call it from any other hare class
#ifdef __SIZERELATEDDEATH2
	if (m_weight<cfg_hare_minimum_breeding_weight.value()) test*=2;
#endif
#ifdef __LOWFATRELATEDDEATH
	if (m_Age>365) if (m_fatReserve<1.0) {
		test= (int)(cfg_hare_lowfatextramort.value() * test); // Increase the chance of dying if fat reserves drop to low levels
	}
#endif
	if (g_rand_uni()<m_OurPopulationManager->m_AdultMortRate) return true;
	return false;
}
//---------------------------------------------------------------------------

bool THare::OnFarmEvent( FarmToDo event )
/**
Checks to see if any nasty farm event has caused 
any behaviour. Few do directly, most work by changes in vegetation or forage, but if they do
they can be added here */
{
	switch (event)
	{
		case cattle_out:
		case pigs_out:
			// In these cases we want to move.
			Running(cfg_hare_escape_dist.value());
			return true; // This will ensure that the next event checked is sensible.
		default:
			break;
	}
	return false;
}
//---------------------------------------------------------------------------