/*
*******************************************************************************************************
Copyright (c) 2016, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Newt.h
\brief <B>The header file for all newt lifestages</B>
*
Version of  1 January 2016 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------
#ifndef NewtH
#define NewtH
//---------------------------------------------------------------------------

#define  __NewtDebug

//---------------------------------------------------------------------------

class Newt_Base;
class Newt_Population_Manager;

//------------------------------------------------------------------------------
/**
Used for the population manager's list of Newt types
*/
enum TTypesOfNewt
{
	tton_Egg = 0,
	tton_Larva,
	tton_Juvenile,
	tton_Male,
	tton_Female,
	tton_Foobar
};

//---------------------------------------------------------------------------

/** \brief Newt behavioural states */
enum TTypeOfNewtState
{
	/**
	Newts like other ALMaSS animals work using a state/transition concept.
	These are the Newt behavioural state.
	*/
	toNewts_InitialState = 0,
      toNewts_Develop,
	  toNewts_EvaluateLocation,
	  toNewts_NextStage,
	  toNewts_Remove,
	  toNewts_Dispersal,
	  toNewts_Migrate,
	  toNewts_Breed,
	  toNewts_Overwinter,
      toNewts_Die,
	  toNewts_foobar
};

/** \brief Newt types of movement */
enum TTypeDirectedWalk {
	directedwalk_totally = 1,
	directedwalk_high = 3,
	directedwalk_medium = 5,
	directedwalk_low = 7,
	directedwalk_random = 8
};

/** \brief The base class for all newts containing common attributes and behaviour for descendent types */
class Newt_Base : public TAnimal {
	/**
	* ALL newts have to know which pond they were born in, as well as their age, state, and the information inherited from TAnimal. Newt_Base also defines static members used by all descendent classes.
	* These are parameter values and therefore constants for a single run.\n
	* Attributes introduced here are:\n
	* - #m_Age the age of the newt in days from egg laying
	* - #m_pondlist which is a list of the ponds that the newt has located since leaving the pond of birth
	*/
public:
	// Attributes
	static double m_EggDevelopmentDDTotal;
	static double m_EggDevelopmentDDParameter;
	static double m_JuvenileDevelopmentSize;
	static double m_EggMortalityChance;
	static double m_JuvenileMortalityChance;
	static double m_AdultMortalityChance;
	/** \brief Flags to record whether we are in pesticide testing mode */
	static bool m_test_pesticide_egg;
	static bool m_test_pesticide_larva;
	static bool m_test_pesticide_terrestrial;

protected:
	// Attributes
	/** \brief Variable to record current behavioural state */
	TTypeOfNewtState m_CurrentNewtState;
	/** The age of the newt in days */
	unsigned m_Age;
	/** /brief The list of pond locations found by the newt, the first value being the pond of birth - NB this holds the index to the polygon array */
	vector<unsigned> m_pondlist;
	/** \brief This is a time saving pointer to the correct population manager object */
	Newt_Population_Manager*  m_OurPopulationManager;
	/** \brief The current PPP body burden for use in pesticide testing mode */
	double m_body_burden;
	/** \brief A flag to indicate environmentally induced reproductive inhibition (value is inherited by descendent classes) */
	bool m_reproductiveinhibition;
public:
	// Methods
	/** \brief Newt constructor */
	Newt_Base(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, bool a_reproinhib);
	/** \brief Intitialise object */
	void Init(vector<unsigned> a_pond, Newt_Population_Manager* a_NPM, bool a_reproinhib);
	/** \brief ReInit for object pool */
	void ReInit(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, bool a_reproinhib);
	/** \brief Newt destructor */
	~Newt_Base();
	/** \brief Returns the state number for display purposes */
	virtual int WhatState() {
		return m_CurrentNewtState;
	}
	/** \brief Behavioural state development - does nothing in the base class*/
	TTypeOfNewtState st_Develop( void ) {
		return TTypeOfNewtState(-1);
	}
	/** \brief Behavioural state movement  - does nothing in the base class*/
	TTypeOfNewtState st_Movement( void ) {
		return TTypeOfNewtState(-1);
	}
	/** \brief Behavioural state dying */
	void st_Dying( void );
	/** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	virtual void BeginStep( void ) {
		;
	}// NB this is not used in the Newt_Base code
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	virtual void Step( void ) {
		;
	}
	/** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	virtual void EndStep( void ) {
		;
	}// NB this is not used in the Newt_Base code
	/** \brief A typical interface function - this one returns the Age as an unsigned integer */
	unsigned GetAge() {
		return m_Age;
	}
	/** \brief A typical interface function - this one sets the Age as an unsigned integer */
	void SetAge( unsigned a_age ) {
		m_Age = a_age;
	}
	/** \brief A typical interface function - this one returns the home pond reference as an unsigned integer */
	unsigned GetHomePond() {
		return m_pondlist[ 0 ];
	}
	/** \brief A typical interface function - this one sets the home pond reference as an unsigned integer */
	void SetHomePond( unsigned a_pond ) {
		m_pondlist[ 0 ] = a_pond;
	}
	/** \brief For handlng of class-specific pesticide effects */
	void InternalPesticideHandlingAndResponse(double) {};
};

/** \brief The newt egg class - in pond */
/**
* Class Newt_Egg represents the egg stage of the newt in ponds. Each egg has a location in a pond polygon represented by
* it's home pond polygon reference (first entry in m_pondlist). The egg starts life at zero days old with zero day degrees. It ages each day ( see Newt_Egg::st_Develop ),
* and the temperature experiences is summed each day to create a cumulative day-degree sum. When this sum reaches a pre-defined
* number of day-degrees the egg hatches to produce a larva.
*/
class Newt_Egg : public Newt_Base
{
protected:
	// Attributes
	/** A internal variable used to sum up the day-degrees experienced by the egg/larva */
	double m_AgeDegrees;
public:
	// Attributes
	/** \brief  The threshold for effect of PPP on eggs*/
	static double m_EggPPPThreshold;
	/** \brief  The daily proportion of PPP body burden that is not eliminated for eggs */
	static double m_EggPPPElimRate;
	/** \brief  The daily probability of effect if above m_EggPPPThreshold for eggs */
	static double m_EggPPPEffectProbability;
	// Methods
	/** \brief Newt_Egg constructor */
	Newt_Egg(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, bool a_reproinhib);
	/** \brief ReInit for object pool */
	void ReInit(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, bool a_reproinhib);
	/** \brief Newt destructor */
	~Newt_Egg();
	/** \brief Newt_Egg Initialisation */
	virtual void Init();
	/** \brief Newt_Egg Step code. This is called continuously until all animals report that they are 'DONE'. */
	virtual void Step( void );
	/** \brief For handlng of class-specific pesticide effects */
	void InternalPesticideHandlingAndResponse();
protected:
	// Methods
	/** \brief Behavioural state development */
	virtual TTypeOfNewtState st_Develop( void );
	/** \brief Egg hatch */
	virtual TTypeOfNewtState st_NextStage( void );
};

/** \brief The newt larva class - in pond */
class Newt_Larva : public Newt_Egg
{
	/**
	* Class Newt_Larva represents the larval stage of the newt in ponds. Each larva has a location in a pond polygon represented by
	* it's home pond polygon reference (in m_pondlist). 
	* 
	* Death can occur by daily mortality probability or due to failing to get daily food intake.\n
	* In addition to inherited attributes from Newt_Egg, each larva has:
	* - #m_LarvalFoodProportion Holds the parameter value for the proportion of food eaten per day relative to weight
	*
	*/
public:
	// Attributes
	/** \brief  The threshold for effect of PPP on larvae*/
	static double m_LarvaPPPThreshold;
	/** \brief  The daily proportion of PPP body burden that is not eliminated for larvae */
	static double m_LarvaPPPElimRate;
	/** \brief  The daily probability of effect if above m_LarvaPPPThreshold for larvae */
	static double m_LarvaPPPEffectProbability;
	/** \brief Holds the parameter value for the proportion of food eaten per day relative to size */
	static double m_LarvalFoodProportion;
	/** \brief The upper size at which larvae will undergo metamorphosis */
	static double m_LarvaDevelopmentUpperSz;
	/** \brief The lower size below which larvae cannot undergo metamorphosis */
	static double m_LarvaDevelopmentLowerSz;
	/** \brief The time before a larva can undergo metamorphosis */
	static double m_LarvaDevelopmentTime;
	/** \brief The daily growth increment if a larva survives to grow that day */
	static double m_NewtLarvaDailyGrowthIncrement;
	/** \brief The daily probability of death from unspecified causes for a larva */
	static double m_LarvaMortalityChance;
	// Methods
	/** \brief Newt_Larva constructor */
	Newt_Larva(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib);
	/** \brief ReInit for object pool */
	void ReInit(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib);
	/** \brief Newt_Larva destructor */
	~Newt_Larva();
	/** \brief Newt_larva Initialisation */
	virtual void Init(int a_age);
	/** \brief Newt_Larva Step code. This is called continuously until all animals report that they are 'DONE'. */
	virtual void Step( void );
	/** \brief For handlng of class-specific pesticide effects */
	void InternalPesticideHandlingAndResponse();
protected:
	// Attributes
	/** \brief Holds the size of the larva, we are assuming length is the critical measure as with the adults */
	double m_LarvalSize;
	// Methods
	/** \brief Larva development state */
	virtual TTypeOfNewtState st_Develop( void );
	/** \brief Larva metamorphosis */
	virtual TTypeOfNewtState st_NextStage( void );
};

/** \brief The newt juvenile class - free living but not reproductive */
class Newt_Juvenile : public Newt_Base
{
	/**
	* Class Newt_Juvenile represents the juvenile stage of the newt. \n
	* In addition to inherited attributes from Newt_Larva, each juvenile has:
	* - #m_weight which is the weight of the newt in g
	* - #m_SimW - the width of the simulation map, a static variable used for speed
	* - #m_SimH - the height of the simulation map, a static variable used for speed
	* - #m_CurrentHabitat - the current habitat type the newt is located in
	* - #m_InPond is the polyrefindex for the pond the newt is in, or -1 if not in a pond
	* - #m_SimW The width of the simulation map, stored for fast access
	* - #m_SimH The height of the simulation map, stored for fast access
	* - #m_OurVector The last direction the newt moved in
	*
	* Other new attributes are included here for efficient storage of input parameters
	* - #m_JuvenileDailyWeightGain Used in determining daily growth, this is the daily increment.
	* - #m_roadmortalityprob  The probability of death when crossing a road, this is per movement (1 x #m_newtwalkspeed).
	* - #m_newtwalkspeed  The max walking speed of a newt per movement (1 day)
	* - #m_newtwalkstepsize  The size of a step when evaluating habitat during walking - used to speed up movement if necessary, default is 1m
	* - #m_goodhabitatdispersalprob  Probability of dispersal in good habitat
	* - #m_poorhabitatdispersalprob  Probability of dispersal in poor habitat
	* - #m_NewtDormancyTemperature  Temperature in degrees that the newts become dormant and stop moving or breeding (summed over previous 5 days) Default value is 22.5 based on Langton et al (2001) who state 4-5 degrees.
	*
	* New behaviours are introduced here:\n
	* - #st_Disperse this moves the newts out of and away from the pond
	* - #st_Overwinter this is where the newt enters a semi-torpid state over winter (ie does not move).
	*/

public:
	// Attributes
	/** \brief  The minimum threshold for effect of PPP on juveniles*/
	static double m_JuvenilePPPThreshold_Min;
	/** \brief  The environmental concentration threshold for effect of PPP on juveniles*/
	static double m_JuvenilePPPThreshold_EnvConc;
	/** \brief  The overspray threshold for effect of PPP on juveniles*/
	static double m_JuvenilePPPThreshold_Overspray;
	/** \brief  The daily proportion of PPP body burden that is not eliminated for juveniles */
	static double m_JuvenilePPPElimRate;
	/** \brief  The daily probability of effect if above m_JuvenilePPPThreshold for juveniles for environmental concentration */
	static double m_JuvenilePPPEffectProbability_EnvConc;
	/** \brief  The daily probability of effect if above m_JuvenilePPPThreshold for juveniles for overspray */
	static double m_JuvenilePPPEffectProbability_Overspray;
	/** \brief Used in determining daily growth */
	static double m_JuvenileDailyWeightGain;
	/** \brief The width of the simulation map, stored for fast access */
	static int m_SimW;
	/** \brief The height of the simulation map, stored for fast access */
	static int m_SimH;
	/** \brief The probability of death when crossing a road */
	static double m_roadmortalityprob;
	/** \brief The max walking speed of a newt */
	static int m_newtwalkspeed;
	/** \brief The size of a step when evaluating habitat during walking */
	static int m_newtwalkstepsize;
	/** \brief Probability of dispersal in good habitat */
	static double m_goodhabitatdispersalprob;
	/** \brief Probability of dispersal in poor habitat */
	static double m_poorhabitatdispersalprob;
	/** \brief Temperature in degrees that the newts become dormant */
	static double m_NewtDormancyTemperature;
	/** \brief Temperature in degrees that the newts become dormant */
	static double m_NewtDormancyHumidity;
	// Methods
	/** \brief Newt_Juvenile constructor */
	Newt_Juvenile(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib);
	/** \brief Newt_Juvenile destructor */
	~Newt_Juvenile();
	/** \brief Used to re-use an object - must be implemented in descendent classes */
	virtual void ReInit(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib);
	/** \brief Newt_Juvenile Step code. This is called continuously until all animals report that they are 'DONE'. */
	virtual void Step( void );
	/** \brief Get method for m_weight */
	double GetWeight() {
		return m_weight;
	}
	/** \brief Set method for m_weight */
	void SetWeight( double a_weight ) {
		m_weight = a_weight;
	}
	/** \brief For handlng of class-specific pesticide effects */
	void InternalPesticideHandlingAndResponse();
protected:
	// Attributes
	/** \brief The polyrefindex for the pond the newt is in, or -1 if not in a pond */
	int m_InPond;
	/** /brief The weight of the newt in grammes */
	double m_weight;
	/** \brief The last direction we moved in */
	unsigned m_OurVector;
	/** \brief The current habitat type the newt is in */
	TTypesOfLandscapeElement m_CurrentHabitat;
	/** \brief A flag to force dispersal from the breeding ponds */
	bool m_forcedisperse;
	// Methods
	/** \brief Juvenile development state*/
	virtual TTypeOfNewtState st_Develop( void );
	/** \brief Juvenile evaluate habitat behaviour*/
	virtual TTypeOfNewtState st_EvaluateHabitat( void );
	/** \brief Juvenile dispersal state */
	virtual TTypeOfNewtState st_Disperse( void );
	/** \brief Juvenile overwintering state */
	virtual TTypeOfNewtState st_Overwinter( void );
	/** \brief Juvenile maturation */
	virtual TTypeOfNewtState st_NextStage( void );
	/** \brief Top level control of newt movement */
	void MoveTo( TTypeDirectedWalk a_directedness, int a_stepsize, int a_steps );
	/** \brief  This method moves the newt through the landscape */
	void NewtDoWalking( int &a_vector, int a_stepsize, int a_steps, int &vx, int &vy );
	/** \brief  This method moves the newt through the landscape using wrap around corrects*/
	void NewtDoWalkingCorrect( int &a_vector, int a_stepsize, int a_steps, int &vx, int &vy );
	/** \brief Tests the habitat quality for a square to move to */
	int NewtMoveQuality( int a_x, int a_y, TTypesOfLandscapeElement & a_tole);
};

/** \brief The newt adult class - contains behaviour and attributes common to adults but not juveniles */
class Newt_Adult : public Newt_Juvenile {
	/**
	* Newt_Adult adds the following attributes:\n
	* - #m_AdultLifespan which is the maximum lifespan of a newt, set a 14 years following Francillon-Vieillot (1990)
	* - #m_targetpondx which is the x-coordinate of the pond the newt decides to head to in migration
	* - #m_targetpondy which is the y-coordinate of the pond the newt decides to head to in migration
	*
	* Four behavioural states are defined for the adult:\n
	* - #st_Develop which redefines the development state to take account of the adult rather than juvenile parameters
	* - #st_Overwinter which redefines the dormant state to use adult daily mortality
	* - #st_EvaluateHabitat which adds a breeding season test and the possibility to switch to migration or breeding behaviour
	* - #st_Migrate which returns the newt to its home pond or another on its list of ponds found if any
	*
	*/
public:
	// Attributes
	/** \brief  The threshold for effect of PPP on adults*/
	static double m_AdultPPPThreshold;
	/** \brief  The daily proportion of PPP body burden that is not eliminated for adults */
	static double m_AdultPPPElimRate;
	/** \brief  The daily probability of effect if above m_AdultPPPThreshold for adults */
	static double m_AdultPPPEffectProbability;
	/** The lifespan of the adult */
	static unsigned m_AdultLifespan;
	/** \brief The max walking speed of a newt */
	static int m_newtadultwalkspeed;
	// Methods
	/** \brief Newt_Adult constructor */
	Newt_Adult(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib);
	/** \brief Newt_Male destructor */
	~Newt_Adult();
	/** \brief Initialise object */
	virtual void Init(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age);
	/** \brief Used to re-use an object - must be implemented in descendent classes */
	virtual void ReInit(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib);
	/** \brief Returns the current pond status (-1 if not in pond else the pond ref is returned) */
	int GetInPond() {
		return m_InPond;
	}
	/** \brief Forces setting the in pond status to the first pond ref - used only on start-up */
	void SetInPond() {
		m_InPond = m_pondlist[0];
	}
protected:
	// Attributes
	/** \brief x-coord of a target pond */
	int m_targetpondx;
	/** \brief y-coord of a target pond **/
	int m_targetpondy;
	// Methods
	/** \brief Calculates the direction needed to move in to get to the target pond */
	void CalcMovementVector(void);
	/** \brief Ages the adult and applies a daily mortality probability */
	bool AgeMortTest(void);
	/** \brief Adult development state*/
	virtual TTypeOfNewtState st_Develop(void);
	/** \brief Adult development state*/
	virtual TTypeOfNewtState st_Disperse(void);
	/** \brief Adult dormant state*/
	virtual TTypeOfNewtState st_Overwinter(void);
	/** \brief Adult migration to pond state*/
	virtual TTypeOfNewtState st_Migrate(void);
	/** \brief Adult evaluate habitat behaviour*/
	virtual TTypeOfNewtState st_EvaluateHabitat(void);
};

/** \brief The newt male class */
class Newt_Male : public Newt_Adult {
	/**
	* Newt_Male adds no new attributes to Newt_Adult.\n
	* The only special behaviour defined here is the Step code.
	*
	*/
public:
	/** \brief Newt_Male constructor */
	Newt_Male(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib);
	/** \brief Newt_Male destructor */
	~Newt_Male();
	/** \brief Used to re-use an object - must be implemented in descendent classes */
	virtual void ReInit(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib);
	/** \brief Newt_Male BeingStep code. Called once per timestep, this only records pond presence. */
	virtual void BeginStep(void);
	/** \brief Newt_Male Step code. This is called continuously until all animals report that they are 'DONE'. */
	virtual void Step(void);
	/** \brief Male development state*/
	virtual TTypeOfNewtState st_Develop(void);
	/** \brief For handlng of class-specific pesticide effects */
	void InternalPesticideHandlingAndResponse();
protected:
	/** \brief Adult evaluate habitat behaviour*/
	virtual TTypeOfNewtState st_EvaluateHabitat(void);
};

/** \brief The newt female class */

class Newt_Female : public Newt_Adult {
	/**
	* Newt_Female adds the following attributes to Newt_Adult:\n
	* - #m_eggproductionvolume  Parameter - holds the max number of eggs that can be produced during breeding
	* - #m_eggdailyproductionvolume  Parameter - holds the number of eggs produced per day during breeding
	* - #m_eggsproduced is used to keep track of the actual number of eggs produced
	* - #m_mated which is used to record whether the newt is mated or not this season
	*
	* The only new behaviour added to the Newt_Female class is #st_Breed which is responsible for carrying out the breeding behaviour when the newt is in the pond
	*/
public:
	/** \brief Parameter - holds the max number of eggs that can be produced during breeding */
	static unsigned m_eggproductionvolume;
	/** \brief Parameter - holds the number of eggs produced per day during breeding */
	static unsigned m_eggdailyproductionvolume;
	/** \brief Newt_Female constructor */
	Newt_Female(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib);
	/** \brief Newt_Female destructor */
	~Newt_Female();
	/** \brief Used to re-use an object - must be implemented in descendent classes */
	virtual void ReInit(int a_x, int a_y, vector<unsigned> a_pond, Landscape* a_L, Newt_Population_Manager* a_NPM, unsigned a_age, bool a_reproinhib);
	/** \brief Newt_Female Step code. This is called continuously until all animals report that they are 'DONE'. */
	virtual void Step(void);
	/** brief Sets the eggs produced to zero */
	void ResetEggProduction() {
		m_eggsproduced = 0;
	}
	/** \brief For handlng of class-specific pesticide effects */
	void InternalPesticideHandlingAndResponse();
protected:
	/** \brief Holds the number of eggs produced during breeding */
	unsigned m_eggsproduced;
	/** \brief Flag showing mating status, true means mated this season */
	bool m_mated;
	/** \brief Male development state*/
	virtual TTypeOfNewtState st_Develop(void);
	/** \brief Female breeding in pond */
	virtual TTypeOfNewtState st_Breed(void);
	/** \brief Adult evaluate habitat behaviour - augmenting the adult behaviour*/
	virtual TTypeOfNewtState st_EvaluateHabitat(void);
};

#endif
