# Makefile for ALMaSS
# David Wallis, 2018

CXX = g++
#CXX_FLAGS = -Wfatal-errors -Wall -Wextra -Wpedantic -Wconversion -Wshadow -std=c++11
#CXX_FLAGS  = -g -Wall -Werror=return-type -std=c++11  `wx-config --cxxflags` -D'__UNIX' #-D'__UNIX__'
CXX_FLAGS  = -g -Wall -std=c++11  `wx-config --cxxflags` -D'__UNIX' #-D'__UNIX__'

#CXX_FLAGS  = -g -Wall -std=c++11  -D'__UNIX' #-D'__UNIX__' 

BIN_BATCH = almass
BIN_GUI = almass_visual

BUILD_DIR_BATCH = ./build_batch
BUILD_DIR_GUI = ./build_gui
#TARGET_DIR = ~/Dropbox/aarhus/data/TestingRunDir3
TARGET_DIR = ./TestingRunDir

# These are all the c++ files
CPP = $(wildcard Bembidion/*.cpp) $(wildcard Dormouse/*.cpp) $(wildcard GooseManagement/*.cpp) $(wildcard Hare/*.cpp) $(wildcard HoneyBee/*.cpp) $(wildcard Hunters/*.cpp) $(wildcard Landscape/*.cpp) $(wildcard MarshFritillary/*.cpp) $(wildcard Newt/*.cpp) $(wildcard Osmia/*.cpp) $(wildcard Partridge/*.cpp) $(wildcard Pipistrelle/*.cpp) $(wildcard Rabbit/*.cpp) $(wildcard RodenticideModelling/*.cpp) $(wildcard RoeDeer/*.cpp) $(wildcard Skylark/*.cpp) $(wildcard Spider/*.cpp) $(wildcard Vole/*.cpp) $(wildcard Landscape/cropprogs/*.cpp) $(wildcard OliveMoth/*.cpp) 

# For batch, we add cpp files in BatchALMaSS
CPP_BATCH = $(wildcard BatchALMaSS/*.cpp) $(CPP)

# For GUI we add cpp files in GUI and all the files in BatchALMaSS *except* BatchALMaSS.cpp
CPP_GUI = $(wildcard GUI/*.cpp) $(CPP) BatchALMaSS/AOR_Probe.cpp BatchALMaSS/BinaryMapBase.cpp BatchALMaSS/CurveClasses.cpp BatchALMaSS/MovementMap.cpp BatchALMaSS/PopulationManager.cpp BatchALMaSS/PositionMap.cpp


OBJ_BATCH = $(CPP_BATCH:%.cpp=$(BUILD_DIR_BATCH)/%.o)
OBJ_GUI = $(CPP_GUI:%.cpp=$(BUILD_DIR_GUI)/%.o)
DEP_BATCH = $(OBJ_BATCH:%.o=%.d)
DEP_GUI = $(OBJ_GUI:%.o=%.d)

all : $(BIN_GUI)

$(BIN_BATCH) : $(BUILD_DIR_BATCH)/$(BIN_BATCH)
$(BIN_GUI) : $(BUILD_DIR_GUI)/$(BIN_GUI)

# This links the batch version
$(BUILD_DIR_BATCH)/$(BIN_BATCH) : $(OBJ_BATCH)
	mkdir -p $(@D)
	$(CXX) $(CXX_FLAGS) $^ -o $@

# To link the gui version we need to add the wx libraries - uses wx-config.
$(BUILD_DIR_GUI)/$(BIN_GUI) : $(OBJ_GUI)
	mkdir -p $(@D)
	$(CXX) $(CXX_FLAGS)  $^ `wx-config --libs` -o $@

#-include $(DEP_BATCH)
#-include $(DEP_GUI)

# This compiles the batch version
$(BUILD_DIR_BATCH)/%.o : %.cpp
	mkdir -p $(@D)
	$(CXX) $(CXX_FLAGS) -MMD -c $< -o $@

# For the gui version we add the env variable __ALMASS_VISUAL
# This adds a pointer to the gui canvas in the pop manager
$(BUILD_DIR_GUI)/%.o : %.cpp
	mkdir -p $(@D)
	$(CXX) $(CXX_FLAGS) -D'__ALMASS_VISUAL' -MMD -c $< -o $@

# Removes all files under the batch build directory
.PHONY : batchclean
batchclean :
	-rm $(BUILD_DIR_BATCH)/$(BIN_BATCH) $(OBJ_BATCH) $(DEP_BATCH)
#	-rm $(BUILD_DIR_BATCH)/$(BIN_GUI) $(OBJ_GUI) $(DEP_GUI)

# Removes all files under the gui build directory
.PHONY : guiclean
guiclean :
#	-rm $(BUILD_DIR_GUI)/$(BIN_BATCH) $(OBJ_BATCH) $(DEP_BATCH)
	-rm $(BUILD_DIR_GUI)/$(BIN_GUI) $(OBJ_GUI) $(DEP_GUI)

# Copies the binary to the working directory
.PHONY : install
install : $(BIN_BATCH)
	cp $(BUILD_DIR_BATCH)/$(BIN_BATCH) $(TARGET_DIR)/

# Copies the gui binary to the working directory
.PHONY : installgui
installgui : $(BIN_GUI)
	cp $(BUILD_DIR_GUI)/$(BIN_GUI) $(TARGET_DIR)/

