//
// Roe_constants.cpp
//
#include <string.h>
#include <vector>
#include <iostream>
#include <fstream>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "Roe_constants.h"
#include "Roe_all.h"



extern const int Vector_x[8];
extern const int Vector_y[8];
//grid constants
const int Cell_Size = 20;  //cells are 20x20 m
const double InvCell_Size = 0.05;
const int Cell_HalfSize = 10;
const int MinRange = 200;
const int MaxRange = 400;
const int MaxDistToMum = 200;
const int MinDispDistance = 300;
const int MaxNoGroups = 5000;
const int MaxNoDeer = 5000;  //at any one time - for the fixlist

//cannot set up range if present density is more than this
const unsigned MaleDensityThreshold = 1;

const unsigned FemaleDensityThreshold = 1;

//average range quality based on this many areas
const int NoRandomAreas = 1000;

const float DivideRandomAreas = (float) 0.001; //= 1 / NoRandomAreas
const int MinRangeQuality = 3900;  //all-year minimum
const int AverageRecoveryTime=8;  //timesteps, data from CRO
const int MaxRecoveryTime=12;      //data from CRO
const int CoverThreshold1 = 1;   //> 1 = medium (= open-summer/forest-winter)
const int CoverThreshold2 = 2;   //> 2 = good (= forest-summer)

//g dw/minute Drozdz 1979. Values here are multiplied by 10 to get time step values

//Intake -10%
/*const float IntakeRate[12] = {
  7.9,10.3,10.8,15.3,22.5,25.2,25.2,18.0,17.1,15.3,13.5,9.9
};  */

//Intake +10%
/*const float IntakeRate[12] = {
  9.6,12.7,13.2,18.7,27.5,30.8,30.8,22.0,20.9,18.7,16.5,12.1
};
*/
//Intake Drozdz 1979
const float IntakeRate[12] = {
  8.7f,8.9f,11.7f,13.2f,31.8f,33.0f,24.7f,23.8f,22.4f,18.8f,11.9f,9.5f
}; 


//will not feed if gain for 10 minutes feeding is less than this
const int MinNutri =  8000;

//TRAFFIC, width in meter and traffic load in no. of vehicles per hour.
const int MaxRoadWidth = 12;   //max road width in area
const int MaxTraffic = 513; //In this area

// ENERGETICS
// Resting met. rate in cal/timestep/kg.
// RMR[0]=non-pregnant, RMR[1]=pre-partum 240-300,
// RMR[2] post-pertum 1-15, RMR[3]=postp 16-30,
// RMR[4]=postp 31-60, RMR[5]=postp 61-90
// RMR[6]=postp 91-120
const float FemaleRMR[7]={358.35f,414.93f,452.65f,437.56f,414.93f,396.07f,377.21f};
const float FawnRMR = (float) 526.2;  //cal/timestep/kg
const float MaleRMR = (float) 358.35; //same as non-pregnant female

// For fawns lower critical bw follows age, not month in year.
// [0]:0-9 days, [1]:10-60 days,[2]: 2-4 months,
// [3]:5-10 months, [4]: 11-12 months
const int MinWeightFawn[5]={1500,3000,6000,7000,8000};

const int prop_femalefawns = 50;  //sexratio at birth, of 100 fawns

//No of feeding bouts per 24 hrs
//in updateE
const int Male_FeedBouts[12]={
  9,8,8,13,14,13,17,14,11,13,9,9
};

const int Female_FeedBouts[12]={
  8,9,9,12,14,13,12,13,11,11,8,8
};

//COST; additional costs for different types of activity. Corresponds to the
//m_MinInState-array. In cal/timestep/kg.
//Based on values for 1:rest,2:standing/foraging,
//3:walking and 4:running. Activities that are assumed to be costly, but not
//taking up real time in the model (e.g. giving birth) is given a value of 7000
//Females: Cost[0]=Disperse,[1]=InHeat,[2]=GiveB,[3]=Care,[4]=Run,
//[5]=Recov,[6]=Ignore,[7]=Evade,[8]=Rumi,[9]=Feed,[10]=Mate,[11]=EstablR
//Males:[0]=Establ.R,[1]=establ.T,[2]=Disperse,[3]=Run,[4]=Recov,[5]Evade,
//[6]=Ignore,[7]=Fight,[8]=AttendF,[9]=Rumi,[10]=Feed,[11]=Mate
//Fawns:[0]=Run,[1]=Recov,[2]=Hide,[3]=Rumi,[4]=Feed,[5]=Suckle
//Value "1" for resting, will be multiplied by RMR
const int CostFemaleAct[12]={
  467,467,7000,500,2530,467,467,467,1,467,7000,7000
};
const int CostMaleAct[12]={
  7000,7000,467,2530,467,467,467,700,2000,1,467,7000
};
const int CostFawnAct[6]={
  2530,1,1,1,684,684        //[4] and [5] = 1.3 * RMR (same proportional increase
                            //as in adults)    
};

//Ana. and catab. gives the cost and gain of storing vs. metabolising tissue
//In cal/g tissue (i.e. fat). Data from farmed deer.
const float Anabolic = (float) 13186;
const float Anabolic_Inv = (float) (1.0/Anabolic);
// Compileren forudberegner denne v�rdi.
const float Catabolic = (float) 9400;
const float Catabolic_Inv = (float) (1.0/Catabolic);

//BACKGROUND MORTALITY, adults
//DailyMortRate (DMR)=1-(sqrt(survivalrate,365))
//(e.g. FDMR[0]= 1-(365sgrt of 0.77)
//In a year DMR is proportional to annual mortality rates from Gaillard et al.
//1993. All values are multiplied by 100.000 to get int values!!
//DMR[0]=1-2 yrs, DMR[1]=2-7 yrs, DMR[2]=7-10 yrs, DMR[3]=>10 yrs. From
//capture-recapture data
//Floaters are given an increased annual mortality
const int FloatMort = 180;
const float FemaleDMR[4]={
  24,3,35,220
};
const float MaleDMR[4]={
  27,14,28,220
};

//MORTALITY fawns
//FDMR[0]=0-6 months, FDMR[1]=6-12 months. Includes all kinds of mortality
//must be revised when predation is specifically included in model
const float FawnDMR[2]={ 68,16 };

//NURSING, female+fawn
//Minhrcare=minimum care per hour in timesteps.
//Duration_care=duration of each care
//period. Depends on fawn age in days:[0]=0-9,[1]=10-60
const int Minhrcare[2]={2,1};
const int Flush_dist[2]={1,7}; //fawn flush dist when approached
const int MalePrime = 2555; //age in days (=7 yrs) above which senescence is
                            //assumed to start (age is a disadvantage in ranks)    
const int GestationPeriod = 300;  //days
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

