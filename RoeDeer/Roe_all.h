//---------------------------------------------------------------------------
#ifndef Roe_allH
#define Roe_allH

//---------------------------------------------------------------------------
#include <math.h>

#include <vector>
#include <algorithm>
#include <string>


//---------------------------------------------------------------------------

using namespace std;



//---------------------------------------------------------------------------

class Roe_Base;
class Roe_Male;
class Roe_Female;
class Roe_Fawn;
class Roe_Adult_Base;
class RoeDeer_Population_Manager;
class TTypesOfElement;
class Grid;
class GridCell;
class MovementMap;
class probe_data;

//---------------------------------------------------------------------------

typedef vector<Roe_Fawn *> Roe_FawnList;
typedef vector<Roe_Male *> Roe_MaleList;
typedef vector<Roe_Female *> Roe_FemaleList;
// the arrays
typedef vector< Roe_Male *>    ListOfMales;
typedef vector< Roe_Female *>  ListOfFemales;
typedef vector< Roe_Base *>    ListOfDeer;

//---------------------------------------------------------------------------

typedef enum {
                rds_Initialise = 0,
                rds_FOnMature,
                rds_MOnMature,
                rds_FUpdateGestation,
                rds_FUpdateEnergy,
                rds_MUpdateEnergy,
                rds_FAUpdateEnergy,
                rds_FEstablishRange,
                rds_MEstablishRange,
                rds_MEstablishTerritory,
                rds_FDisperse,
                rds_MDisperse,
                rds_FOnNewDay,
                rds_MOnNewDay,
                rds_FAOnNewDay,
                rds_FFormGroup,
                rds_FInHeat,
                rds_FGiveBirth,
                rds_FCareForYoung,
                rds_FRun,
                rds_MRun,
                rds_FARunToMother,
                rds_FRecover,
                rds_MRecover,
                rds_FARecover,
                rds_FIgnore,
                rds_MIgnore,
                rds_FEvade,
                rds_MEvade,
                rds_FAHide,
                rds_MFight,
                rds_MAttendFemale,
                rds_FRuminate,
                rds_MRuminate,
                rds_FARuminate,
                rds_FFeed,
                rds_MFeed,
                rds_FAFeed,
                rds_FASuckle,
                rds_FMate,
                rds_MMate,
                rds_FDie,
                rds_MDie,
                rds_FADie,
                rds_FDeathState,
                rds_MDeathState,
                rds_FADeathState,
                rds_FAInit,
                rds_RUBBISH
             }
             TRoeDeerStates;
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

class Roe_Base : public TAnimal
{
 public:
 TRoeDeerStates CurrentRState;
 RoeDeer_Population_Manager * m_OurPopulation;
 int m_Age; //in days
 int m_Size;   //body weight in gram
 int m_RangeCentre_x;
 int m_RangeCentre_y;
 int m_Reserves;
 double m_HomeRange;
 int m_OldRange_x;    //stores roes own rangecentre while in group or dispersing
 int m_OldRange_y;
 int m_SearchRange;
 bool m_float;
 bool m_Disperse;
 bool m_HaveRange;
 Roe_Female * Mum;
 int m_ID; //unique ID number
 int m_FixlistNumber;
 bool m_Sex;   //true=male
 bool m_IsDead;
 unsigned Alive;
 unsigned IsAlive() {return Alive;}

 int SupplyReserves() {return m_Reserves;}
 private:
 protected:
 int timestep;  //current timestep in this animals life

 int m_Cross_x;
 int m_Cross_y;
 double m_EnergyGained;    //counter that keeps track of energy gained from feeding
 int m_RecovCount;  //counts time spend in recov. Set=0 when state is left
 int m_RumiCount;   //counts time spend ruminating. Set=0 when state is left
 int m_FeedCount;  //counts time spend feeding since end of last ruminate period
 int m_LengthFeedPeriod;//calculated daily in UpdateEnergy, depends on date and body weight
 int m_LengthRuminate;
 int m_LastState;  //holds the last state in before disturbance or NewDay
 int m_danger_x; // co-ordinates to a source of danger - set by an
 int m_danger_y; // ApproachOfDanger message
 //for obstacle avoidance during very directed movement:
 int m_distrc; //stores the last dist to rc
 int m_weightedstep; //this many steps with weight during last hour
 int SimW;
 int SimH;
 bool Running (int x,int y);
 void Ignore (int p_To_x, int p_To_y);

 int DistanceTo   (int p_x,int p_y, int p_To_x, int p_To_y);
 int DistanceToCC (int p_x,int p_y, int p_To_x, int p_To_y)
   {
     m_OurLandscape->CorrectCoords( p_x, p_y );
     m_OurLandscape->CorrectCoords( p_To_x, p_To_y );
     return DistanceTo( p_x, p_y, p_To_x, p_To_y );
  }

 int DirectionTo(int p_x,int p_y, int p_To_x, int p_To_y);
 int DirectionToCC(int p_x,int p_y, int p_To_x, int p_To_y)
   {
     m_OurLandscape->CorrectCoords( p_x, p_y );
     return DirectionTo( p_x, p_y, p_To_x, p_To_y );
   }

 int NextStep (int weight,int dir, int recurselevel);
 int AssessHabitat (int polyref);
 int AssessHabitat (int p_x, int p_y);

 int LegalHabitat(int p_x,int p_y);
 /**
 LegalHabitatCC -calls LegalHabitat witth coordinates corrected for wrap-around
 */
 int LegalHabitatCC(int p_x,int p_y)
   {
     m_OurLandscape->CorrectCoords( p_x, p_y );
     return LegalHabitat( p_x, p_y );
   }


 /** \brief Calls the cover calculation for a given polyref */
 int Cover(int polyref);
 /** \brief Calls the cover calculation for a given x,y location */
 int Cover(int p_x, int p_y);
 /** \brief The cover calculation for a given element type */
 int CalcCover(TTypesOfLandscapeElement a_ele, int a_poly);
 /**
 corrects coordinates (for cover) for wrap around landscape
 */
 int CoverCC(int p_x, int p_y) // Corrects coordinates, inline. **FN**
   {
     m_OurLandscape->CorrectCoords( p_x, p_y );
     return Cover( p_x, p_y );
   }

 int NutriValue(int polyref);
 int NutriValue(int p_x, int p_y);
 int NutriValueCC(int p_x, int p_y)
   {
     m_OurLandscape->CorrectCoords( p_x, p_y );
     return NutriValue( p_x, p_y );
   }

 int Feeding(bool p_Disperse);
 void SeekCover(int threshold);
 void SeekNutri(int p_threshold);
 int ProbRoadCross (int p_x, int p_y, int p_width);
 double CalculateRoadMortality (int p_x,int p_y,int p_width);

 void WalkTo(int pos_x, int pos_y);
 void WalkToCC(int pos_x, int pos_y)
   {
     m_OurLandscape->CorrectCoords( pos_x, pos_y );
     WalkTo( pos_x, pos_y );
   }

public:
 virtual void On_MumAbandon(Roe_Female* /* mum */) {}
 virtual void On_IsDead(Roe_Base* /* base */, int /* whos_dead */, bool /* fawn */){}
 virtual void On_EndGroup() {}
 virtual void Send_IsDead() {}
 virtual void On_ChangeGroup(int /* newgroup */) {}
 virtual void On_UpdateGroup(int /* p_group_x */,int /* p_group_y */) {}
 RoeDeerInfo SupplyInfo();
 virtual void BeginStep (void);
 virtual void Step      (void);
 virtual void EndStep   (void);
 virtual int WhatState() {return (int) CurrentRState;}
 Roe_Base(int x, int y, Landscape* L,RoeDeer_Population_Manager* RPM);
 virtual ~Roe_Base();
};
//---------------------------------------------------------------------------

class Roe_Adult_Base : public Roe_Base
{
public:

protected:
 bool m_DestinationDecided;  //know where to go after OnNewDay
 int m_NewState;    //go to this state after OnNewDay
 int m_DispCount;
 int m_float_x;
 int m_float_y;
 
public:
 virtual void BeginStep (void);
 virtual void Step      (void);
 virtual void EndStep   (void);

 Roe_Adult_Base(int x, int y, int size,int age, Landscape* L,RoeDeer_Population_Manager* RPM);
 virtual ~Roe_Adult_Base();
};
//---------------------------------------------------------------------------

class Roe_Fawn : public Roe_Base
{
private:
  //states
  int FAInit(void);
  int FASuckle(void);
  int FAFeed(void);
  int FARuminate(void);
  int FAHide(void);
  int FARecover(void);
  int FARunToMother(void);
  int FAOnNewDay(void);
  int FAUpdateEnergy(void);
  int FADie(void);

protected:
 Roe_Base* m_MySiblings[3];  //same age siblings
 int BedSiteList[15][2];
 int m_Agegroup;
 bool m_Mature;
 int m_MinHourlyCare;
 bool m_Orphan;
 bool m_InHide;
 int m_Bedsite_x;
 int m_BedSite_y;
 unsigned m_Count; //counts every timestep for the first 60 days of fawns life (8640 timesteps)
 int m_CareLastHour[6];
 //dynamic list that counts last 6 timesteps as '1' if
                        //fawn has received the required care and '0' if not.

 int m_memorypointer;  //no. of latest bedsite on bedsite list
 void CareCounter(int state); //counts hourly care
 void SelectBedSite ();
 int RunTo(int mum_x, int mum_y, bool p_IsSafe);
 void Mature();
 void Send_InitCare();
 void On_UpdateGroup(int p_group_x,int p_group_y);
 void On_ApproachOfDanger(int p_To_x,int p_To_y);
public:
  int m_MyGroup;
  int m_MinInState[6]; //for energy calc., minuts spend in different states
 //[0]=Run,[1]=Recov,[2]=Hide,[3]=Rumi,[4]=Feed,[5]=Suckle
public:
 virtual void On_IsDead(Roe_Base* Someone, int whos_dead, bool fawn);
 virtual void On_ChangeGroup(int newgroup);
 virtual void On_MumAbandon(Roe_Female* mum);
 virtual void BeginStep (void);
 virtual void Step      (void);
 virtual void EndStep   (void);
 virtual void On_EndGroup();
 Roe_Fawn(int x, int y,int size,int group,bool sex,Landscape* L,RoeDeer_Population_Manager* RPM);
 virtual ~Roe_Fawn();
};
//---------------------------------------------------------------------------

class Roe_Female : public Roe_Adult_Base
{
public:
 Roe_Base* m_MyYoung[20];  //an array of pointers to all offspring
 int m_MinInState[12]; //for energy calc., minuts spend in different states
 //[0]=Disperse,[1]=InHeat,[2]=GiveB,[3]=Care,[4]=Run,[5]=Recov,[6]=Ignore,
 //[7]=Evade,[8]=Rumi,[9]=Feed,[10]=Mate,[11]=EstablR
 int m_MyGroup;     //number of mygroup in grouplist
 int SupplyIsPregnant() {if (m_Pregnant) return 1; else return 0;}
 int SupplyYoung() {return m_NoOfYoung;}

// STATES
private:
  int FOnMature(void);
  int FDie(void);
  int FDisperse(void);
  int FEstablishRange(void);
  int FOnNewDay(void);
  int FUpdateGestation(void);
  int FGiveBirth(void);
  int FFormGroup(void);
  int FInHeat(void);
  int FFeed(void);
  int FRun();
  int FRecover(void);
  int FEvade();
  int FIgnore();
  int FRuminate(void);
  int FCareForYoung(void);
  int FMate(void);
  int FUpdateEnergy(void);
protected:
 bool m_Care;
 int m_Gestationdays;
 bool m_Pregnant;
 Roe_Male* m_My_Mate;
 int m_NoOfYoung; //this years fawns
 int m_DaysSinceParturition;
 bool m_WantGroup;
 bool m_NatalGroup;
 vector<int> * m_GroupList;

 int m_OptGroupSize;
 bool m_HaveGroup;
 bool m_GroupUpdated;
 int m_HeatCount;
 bool m_WasInHeat;

//FUNCTIONS
 int  On_CanJoinGroup(Roe_Female* Female);
 void On_UpdateGroup(int p_group_x,int p_group_y);
 void On_ApproachOfDanger(int p_To_x,int p_To_y);
 void Send_EndGroup();
 virtual void On_EndGroup();

public:
 void         Init      (void);
 virtual void BeginStep (void);
 virtual void Step      (void);
 virtual void EndStep   (void);
 virtual void On_IsDead(Roe_Base* Someone, int whos_dead, bool fawn);
 bool On_InitCare(Roe_Fawn* Fawn);
 void On_EndCare(Roe_Fawn* Fawn);
 int SupplyGroupNo() {return m_MyGroup;}   
 void AddFawnToList(Roe_Fawn* fawn);

 Roe_Female(int x, int y, int size, int age,int group,Roe_Female* mum, Landscape* L,RoeDeer_Population_Manager* RPM);
 virtual ~Roe_Female();
};
//---------------------------------------------------------------------------

class Roe_Male : public Roe_Adult_Base
{
private: //male states

protected:
 Roe_Female* m_My_Mate;   //points to the female he is attending

 bool m_HaveTerritory;
 int m_NoOfMatings;
 int m_NumberOfFights;
 int m_MyFightlist;   //this males record of encounters on global fightlist
 Roe_Male* m_MyTMale;  //young satellite males need a pointer to their "host"
 Roe_Male* m_MySatellites[50];

  int MOnMature(void);
  int MDie(void);
  int MDisperse(void);
  int MEstablishRange(void);
  int MOnNewDay(void);
  int MEstablishTerritory(void);
  int MFight(void);
  int MAttendFemale(void);
  int MFeed(void);
  int MRun();
  int MRecover(void);
  int MEvade();
  int MIgnore();
  int MRuminate(void);
  int MMate(void);
  int MUpdateEnergy(void);


public:
 int m_MinInState[12];  //for energy calc., minuts spend in different states
 //[0]=Establ.R,[1]=establ.T,[2]=Disperse,[3]=Run,[4]=Recov,[5]Evade,[6]=Ignore,
 //[7]=Fight,[8]=AttendF,[9]=Rumi,[10]=Feed,[11]=Mate
 void On_Expelled(Roe_Male* rival);
 void On_NewHost(Roe_Male* host);
 void On_ApproachOfDanger(int p_To_x,int p_To_y);
 bool On_InHeat (Roe_Female* female,int p_x,int p_y);
 void ClearFightlist();
 bool SupplyHaveTerritory() {return m_HaveTerritory;}
 Roe_Male* SupplyHost() {return m_MyTMale;}
 bool On_Rank(Roe_Male* rival,double size,int age,int matings,bool where,bool range);
 virtual void On_IsDead(Roe_Base* Someone, int whos_dead, bool fawn);
 void AddSatellite(Roe_Male* sat);
 void RemoveSatellite(Roe_Male* sat);
 void         Init      (void);
 virtual void BeginStep (void);
 virtual void Step      (void);
 virtual void EndStep   (void);

 Roe_Male(int x, int y,int size, int age,Roe_Female* mum,Landscape* L,RoeDeer_Population_Manager* RPM);
 virtual ~Roe_Male();
};
//---------------------------------------------------------------------------

class Deer_struct
{
public:
   int x;
   int y;
   bool sex;
   Landscape* L;
   RoeDeer_Population_Manager* Pop;
   Roe_Female* mum;
   int group;
   int size;
   int age;
   int ID;
};
//---------------------------------------------------------------------------

//-----------------------------------------------------------------------------
class TRoeGroup
{
   public:
   Roe_Base* GroupList[30];
   int Size;
   unsigned group_x;
   unsigned group_y;
};

//----------------------------------------------------------------------------

class Roe_Grid
{
   public:
   Roe_Grid(Landscape* L);
   ~Roe_Grid();
   bool AddGridValue(int x,int y, int value);
   int Supply_GridValue(int g_x,int g_y);
   int Supply_GridCoord(int p_coor);
   protected:
   Landscape* m_LS;
   int m_SizeOfCells;
   int m_extent_x;
   int m_extent_y;
   int m_maxcells;
   int* m_grid;  //List of all cells in grid, min. cell size = 20 m
};
//----------------------------------------------------------------------------

class RoeDeer_Population_Manager : public Population_Manager
{
    // Attributes. 
 protected:
    Roe_Grid* m_TheGrid;
    ListOfDeer* m_GroupList[5000];//list of all groups in area
    int m_NoGroups;
    double m_AverageRangeQuality;
    int m_FixCounter [5000];
    ListOfMales* m_Fightlist [5000]; //keep fightlist for up to 5000 males
	long m_totalarea;
    int Turnover;
    int m_FixList[5000][20][2]; //for each animal keep up to 20 x,y positions
    unsigned m_UpperList[100][2];
    unsigned m_LowerList[100][2];
	/** \brief The number of cells wide in the qual grid */
	int m_gridextentx;
	/** \brief The number of cells tall in the qual grid */
	int m_gridextenty;
public:
	 /**   \brief Map of suitability for movement */
   MovementMap* m_MoveMap; //**ljk inserted on 25/7-2012
   int DeadList[2000][2]; //animals that died within this year.
   long StepCounter;  //timestep in this simulation
   int m_MaleDispThreshold;
   int m_FemaleDispThreshold;
   int m_MinWeight_Males;
   int m_MinWeight_Females;
   int m_CriticalWeight_Males;
   int m_CriticalWeight_Females;
   int CreateNewGroup(Roe_Base* p_deer);
   size_t Supply_GroupSize(int Group) {return m_GroupList[Group]->size();}
   double Supply_AverageRangeQuality() {return m_AverageRangeQuality;}
   double RangeQuality(int p_x,int p_y,int p_range);
   double PercentForest(int p_x,int p_y,int p_range);
   bool AddToGroup(Roe_Base* p_deer,int TheGroup);
   bool RemoveFromGroup(Roe_Base* p_deer,int TheGroup);
   void RemoveFromFightlist(Roe_Base* deer);
   ListOfDeer* Supply_RoeGroup(int Group) {return m_GroupList[Group];}
   bool InSquare(int p_x, int p_y,int p_sqx,int p_sqy, int p_range);
   void CalculateArea(int p_a, int p_b,int ID);
   int ScanGrid(int p_x, int p_y, bool avq);
   int AddToArea(int g_x,int g_y, int p_range);
   int AddToAreaCC(int g_x,int g_y, int p_range);
   void Sort(int ID, int p_a, int p_b);
   void Divide1(int ID, int p_a, int p_b);
   void Divide2(int p_a, int p_b, int p_c,bool p_upper);
   int SimW;
   int SimH;
   ListOfMales* SupplyFightlist(int ListNo) {if (ListNo!=-1) return m_Fightlist[ListNo];
                                             else return NULL;}
   void DeleteFightlist(int TheList);
   int AddToFightlist(Roe_Male* rival, int ListNo);
   int GetFixList();
   void RemoveFixList(int listno);
   void PreProcessLandscape();
   void UpdateRanges();  
	// Other interface functions  //ljk added 22/8-2012
    virtual void TheAOROutputProbe();
    virtual void TheRipleysOutputProbe(FILE* a_prb);	
 protected:
    // Methods
    void DoFirst();
    void RunFirst();
    void RunBefore();
    void RunAfter();
    void RunLast();

 public:
    RoeDeer_Population_Manager(Landscape* L);
    ~RoeDeer_Population_Manager();
   virtual void Init (void);
   void CreateObjects(int, TAnimal *pvo,void* null ,
                                         Deer_struct * data,int number);
   ListOfMales* SupplyMales(int p_x,int p_y,int SearchRange);
   ListOfMales* SupplyTMales(int p_x,int p_y,int SearchRange);
   ListOfMales* SupplyMaleRC(int p_x,int p_y,int SearchRange);
   ListOfFemales* SupplyFemales(int p_x,int p_y,int SearchRange);
   ListOfFemales* SupplyFemaleRC(int p_x,int p_y,int SearchRange);
   //void WriteToHRFile(std::string name, int month, int year);
   //void WriteRandHR(std::string name, int total);
   //void WriteToFixFile(Roe_Base*deer);
   //void WriteFloatFile(std::string name, int male_f, int female_f);
   //void WriteIDFile(std::string name, int p_ID, bool p_sex);
   //void WriteDeadFile(std::string name, int p_year);
   //void WriteAgeStructFile(std::string name, int year);
   //void WriteReproFile(std::string name, int id, int young);    //for Lene
   void LOG(int day, int year);  //daily log for debug purposes


};

#endif
