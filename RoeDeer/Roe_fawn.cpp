//
// roe_fawn.cpp
//


#include <iostream>
#include <fstream>
#include "../Landscape/ls.h"
#include "../ALMaSSDefines.h"
#pragma hdrstop

#include "../BatchALMaSS/PopulationManager.h"


#include "Roe_all.h"
#include "Roe_constants.h"

extern int FastModulus( int a_val, int a_modulus );

//---------------------------------------------------------------------------
//                              Roe_Fawn
//---------------------------------------------------------------------------
/**
Roe_Fawn::FAInit - initial state of fawn object. Keeps track of mother and siblings.
Returns suckling state.
Calls SupplyInfo().
*/
int Roe_Fawn::FAInit()  //initial state
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Fawn:Init():Deadcode warning!","");
    exit( 1 );
  }
  #endif
  int count=0;
  for (int i=0;i<20;i++)
  {
    #ifdef JUJ__Debug3
      if(Mum->IsAlive()!=0x0DEADC0DE)
      {
        m_OurLandscape ->Warn("Roe_Fawn:Init():Deadcode warning, Mum!","");
        exit( 1 );
      }
    #endif
    if((Mum->m_MyYoung[i]!=NULL)&&(Mum->m_MyYoung[i]!=this))
    {
       #ifdef JUJ__Debug3
       if(Mum->m_MyYoung[i]->IsAlive()!=0x0DEADC0DE)
       {
         m_OurLandscape ->Warn("Roe_Fawn:Init():Deadcode warning, Mum->m_MyYoung[i]!","");
         exit( 1 );
       }
       #endif
      RoeDeerInfo info = Mum->m_MyYoung[i]->SupplyInfo();
      if(info.m_Age==m_Age)
      {
        m_MySiblings[count]=Mum->m_MyYoung[i];
        count++;
      }
    }
  }
  return 38; //FASuckle
}

//---------------------------------------------------------------------------
/**
Roe_Fawn::FAFeed - Determines possible energy gain per minute feeding of fawn object by
looking at nutritional value of habitat, and thuis determines if habitat is suited for
feeding. If so adds nutritional value to the energy gained by the fawn object.
If the fawn is not an orphan fawn is weighted back towards mother (via a simple semi-random
walk) if too far away. Orphans are weighted back towards their rangecentre instead.
If the feeding time of the fawn object is long enough state is set to ruminate, else 
to stay in feeding state. If fawn object is less than 60 days old, it may initiate a
suckle period or set state to hide.
Calls functions NutriValue(), SupplyPosition(), DistanceTo(), On_InitCare(), DirectionTo()
*/
int Roe_Fawn::FAFeed()

{
  //a simple semirandom walk. Fawn is weighted back toards mother if too far away
  //Orphans are weighted back towards their rangecentre instead
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Fawn:FFeed():Deadcode warning!","");
    exit( 1 );
  }
  #endif
  #ifdef JUJ__Debug1
  if((Mum==NULL)&&(m_Orphan==false))
  {
    m_OurLandscape ->Warn("Roe_Fawn:FFeed():Warning!","");
    exit( 1 );
  }
  #endif

  m_MinInState[4]++; //add 1 timestep to time spend in this state
  m_FeedCount++;   //add 1 to count for this feeding bout
  m_LastState=37;  //possible to return to this state after disturbance
  int nutri=NutriValue(m_Location_x,m_Location_y);  //possible energy gain per minut

  if(nutri>MinNutri)  //can feed here
  {
    m_EnergyGained+=nutri;   //add this to total count for today
  }
  int weight=-1;
  if(m_Orphan==false)
  {
    #ifdef JUJ__Debug3
    if(Mum->IsAlive()!=0x0DEADC0DE)
    {
      m_OurLandscape ->Warn("Roe_Fawn:FFeed():Deadcode warning, Mum!","");
      exit( 1 );
    }
    #endif
    //set care counter right if fawn is < 60 days
    if (m_Age<60) CareCounter(0); //no care received during this timestep
    //get mothers position
    AnimalPosition xy = Mum->SupplyPosition();
    //get distance to mother
    int dist=DistanceTo(m_Location_x,m_Location_y,xy.m_x,xy.m_y);
    if (dist<=MaxDistToMum)    //OK, can walk in any direction
    {
        NextStep(0,0,0);   //no weight, no directio
    }
    else // too far away, so find appropriate weight
    {
      if (dist<=(MaxDistToMum+25)) weight=1;
        else if ((dist>MaxDistToMum+25)&&(dist<=MaxDistToMum+50)) weight=2;
          else if ((dist>MaxDistToMum+50)&&(dist<=MaxDistToMum+75)) weight=3;
            else if (dist>MaxDistToMum+75)  weight=4;

      //get direction to mother
      int dir=DirectionTo(m_Location_x,m_Location_y,xy.m_x,xy.m_y);
      {
        NextStep(weight,dir,0);
      }
    }
    //decide where to go to from here
    if (m_Age<60)
    {
      int care=0;
      for (int j=0; j<6; j++)  //last 6 timesteps
      {
        if (m_CareLastHour[j]==1)  care++;
      }
      if (care<Minhrcare[m_Agegroup])    //is hungry
      {
        bool CanSuckle=Mum->On_InitCare(this);
        if(CanSuckle==true) return 38; //Suckle
        else return 37;  //Feed
      }
      else
      {
        m_FeedCount=0;
        return 29; //Hide
      }
    }
    else  // > 60 days old
    {
      if (m_FeedCount<=m_LengthFeedPeriod)  //stay in feed
      {
        return 37;
      }
      else  //ruminate
      {
        //for how long do you need to ruminate?
        int month= m_OurLandscape->SupplyMonth();
        m_LengthRuminate=(int) (m_FeedCount*IntakeRate[month-1]*0.1);
        m_FeedCount=0;   //leaves state, so set counter to zero
        return 34; //Ruminate
      }
    }
  }
  else //m_Orphan==true, fawn checks for distance to rangecentre instead
  {
    //get distance to rangecentre
    int dist=DistanceTo(m_Location_x,m_Location_y,m_RangeCentre_x,m_RangeCentre_y);
    if (dist<=200)    //OK, so walk in any direction
    {
        NextStep(0,0,0);   //no weight, no direction
    }
    else // too far away, so find appropriate weight
    {
      if (dist<=(225)) weight=1;
        else if ((dist>225)&&(dist<=250)) weight=2;
          else if ((dist>250)&&(dist<=275)) weight=3;
            else if (dist>275)  weight=4;

      //get direction to range centre
      int dir=DirectionTo(m_Location_x,m_Location_y,m_RangeCentre_x,m_RangeCentre_y);
      {
          NextStep(weight,dir,0);
      }
    }
    //whereto next
    if (m_FeedCount<m_LengthFeedPeriod)  //stay in feed
    {
      return 37;
    }
    else
    {
      m_FeedCount=0;   //leaves state, so set counter to zero
      return 34; //Ruminate
    }
  }
}
//----------------------------------------------------------------------------
/**
Roe_Fawn::FASuckle - Sets energy gain from suckling as 75000 per 10 minutes. The fawn 
object can only be in this state if CanSuckle=true. A careperiod cannot last more than 
1 timestep. Sets care counter (if fawns is < 60 days) and determines whether fawn want 
to initiate another careperiod and whether or not it is still hungry (dependent on 
minimum care time for age group) and wants to initiate more suckling ( which may be 
refused by mother). Either returns states suckling, feed (if mother refused suckling), 
or hide if fawn is less than 14 days.
Calls functions On_InitCare(), SupplyPosition(), DistanceTo(), WalkTo(), On_EndCare()

*/
int Roe_Fawn::FASuckle()
//fawn can only be in this state if CanSuckle=true. A careperiod cannot last
//more than 1 timestep
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
      m_OurLandscape ->Warn("Roe_Fawn:FFeed():Deadcode warning!","");
      exit( 1 );
  }
  if(Mum->IsAlive()!=0x0DEADC0DE)
  {
      m_OurLandscape ->Warn("Roe_Fawn:FFeed():Deadcode warning, Mum!","");
      exit( 1 );
  }
  #endif
  #ifdef JUJ__Debug1
  if(Mum==NULL)
  {
      m_OurLandscape ->Warn("Roe_Fawn:FFeed():NULL pointer,Mum!","");
      exit( 1 );
  }
  #endif
  m_MinInState[5]++; //add 1 to time spend in this state
  m_LastState=38;

  m_EnergyGained += 75000; //during 10 minutes
  //set care counter right if fawn is < 60 days
  CareCounter(1);
  //Does fawn want to initiate another careperiod?
  int care=0;
  for (int j=0; j<6; j++)  //last 6 timesteps
  {
    if (m_CareLastHour[j]==1)  care++;
  }
  #ifdef JUJ__Debug2
  if(m_Agegroup>1)
  {
      m_OurLandscape ->Warn("Roe_Fawn:FFeed():m_AgeGroup > 1!","");
      exit( 1 );
  }
  #endif

  if (care<Minhrcare[m_Agegroup])    //is still hungry
  {
    bool CanSuckle=Mum->On_InitCare(this);
    if(CanSuckle==true)
    {
      AnimalPosition xy=Mum->SupplyPosition();
     //get distance to mum
      if(DistanceTo(m_Location_x,m_Location_y,xy.m_x,xy.m_y)>2)
      {
        WalkTo(xy.m_x,xy.m_y);
      }
      return 38; //Suckle
    }
    else return 37;  //mother refused, so go to feed
  }
  else     //fawn is satiated
  {
    Mum->On_EndCare(this); //end this careperiod
  }
  if (m_Age<14) return 29;  //too young to feed so hide
  else return 37;
}
//----------------------------------------------------------------------------
/**
Roe_Fawn::FARuminate - Function for ruminating. When fawns are older than 2 month 
their ruminating activity is equal to an adult. Checks for good spot to rest and amount of time 
spent ruminating. Ruminate activity cannot be seperated from other kinds of inactivity. 
It is also assumed that ruminating takes place while recovering after disturbance. 
Need to count amount of time spend in this state during this period of ruminating and in total last 24 hrs. 
Returns values for either stay ruminating, feed or dispersal (or die).
Calls function Cover().
*/
int Roe_Fawn::FARuminate()
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
      m_OurLandscape ->Warn("Roe_Fawn:FARuminate():Deadcode warning!","");
      exit( 1 );
  }
  #endif
  //When fawns are older than 2 month their ruminating activity is equal to an adult
  //Ruminating cannot be separated from other kinds of inactivity.
  //It is also assumed that ruminating takes place while recovering after
  //disturbance. Need to count amount of time spend in this state during
  //this period of ruminating and in total last 24 hrs
  m_MinInState[3]++; //add 1 to time spend in this state
  int min;

  //allow roe the opportunity to return to this state after disturbance
  m_LastState=34;  //Ruminate
  if (m_Age < 60) CareCounter(0);
  if (m_RumiCount==0)   //first step so make sure this is a good spot to rest
  {
    int cover=Cover(m_Location_x,m_Location_y);
    //get month
    int month=m_OurLandscape->SupplyMonth();
    if((month>=5)&&(month<=9)) min=CoverThreshold2; //summer
    else min=CoverThreshold1; //winter
    if (cover<min)// habitat unsuitable or cover very poor, so look for better spot
    {
       SeekCover(2);
       if(m_IsDead) return 43; //Die
       else
       {
         m_RumiCount++;
         return 34;
       }
    }
    else //habitat OK, so no need to do anything
    {
      m_RumiCount++;       //add 1 to counter
      return 34;   //return to this state next time step
    }
  }
  else if(m_RumiCount<m_LengthRuminate)   //still less than average
  {
    m_RumiCount++;  //add 1 to counter
    return 34;     //stay
  }
  else //return to feed
  {
    m_RumiCount=0;
    return 37; //Feed
  }
}
//---------------------------------------------------------------------------
/**
Roe_Fawn::FAHide - only fawn objects less than 60 days can be in the hide state.
Sets fawn to hide and select hiding spot. The fawn will leave hide only if flushed 
or hungry. If fawns has been in hiding for more than 1 time step it will check if
it is in need of care and is hungry. If so it may initiate a suckling session (if mother
accepts) or if old enough to feed a feeding session. Checks if mother is dead, and if so
the fawn will also die.
Calls functions SelectBedSite(), On_InitCare(), 
*/
int Roe_Fawn::FAHide() //only in this state if < 60 days
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
      m_OurLandscape ->Warn("Roe_Fawn:FAHide():Deadcode warning!","");
      exit( 1 );
  }
  #endif
  m_MinInState[2]++; //add 1 to time spend in this state
  //set care counter right
  CareCounter(0);
  m_LastState=29;
  m_InHide=true;
  if(m_InHide==false) //first timestep in this state
  {
    SelectBedSite(); //will leave hide only if flushed or hungry
    if(m_IsDead)  return 43; //Die
  }
  else
  {
    //do fawn want to stay in this state?
    int care=0; //sum of care last hour
    for (int j=0; j<6; j++)
    {
      care += m_CareLastHour[j];
    }
    #ifdef JUJ__Debug1
    if (Mum==NULL)
    {
      m_OurLandscape ->Warn("Roe_Fawn:FARuminate():NULL pointer,Mum!","");
      exit( 1 );
    }
    #endif
    if (care<Minhrcare[m_Agegroup])    //is hungry
    {
      bool CanSuckle=Mum->On_InitCare(this);
      care=0;
      if(CanSuckle==true)
      {
        m_InHide=false;
        return 38;  //suckle
      }
      else
      {
        if (m_Age>=14) //old enough to feed
        {
          m_InHide=false;
          return 37;   //feed
        }
        else return 29;
      }
    }
  }
  return 29; //stay
}


//---------------------------------------------------------------------------
/**
Roe_Fawn::FARecover - Recovery from reaction to a disturbance. Fawn may return to 
last state, a new state such as suckling, feeding or hiding (if less than 14 days old) or 
may stay in the recovery state.
Calls function On_InitCare()
*/
int Roe_Fawn::FARecover()
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
      m_OurLandscape ->Warn("Roe_Fawn:FARécover():Deadcode warning!","");
      exit( 1 );
  }
  #endif
  //Need to count amount of time in state during the present
  //period of recov. (m_RecovCount) and set this to 0 when state is left.
  m_MinInState[1]++; //add 1 to time spend in this state
  //set care counter right if fawn is < 60 days
  if (m_Age<60) CareCounter(0);

  m_LastState=24; //possible to return to this state after disturb. or newday
  if (m_RecovCount<=AverageRecoveryTime)
  {
    m_RecovCount++;   //add 1 to time spend in recover and stay here
    return 24;
  }
  else if ((m_RecovCount>AverageRecoveryTime)&&(m_RecovCount<MaxRecoveryTime))
  //return to feed or suckle with certain probability
  {
    int stop = random(100);
    if (stop>10) //stay in recover
    {
      m_RecovCount++;   //add 1 to time spend in this state
      return 24; //recover
    }
  }
  else if (m_RecovCount>=MaxRecoveryTime)   //go to either feed or suckle
  {
    m_RecovCount=0;    //leaves state, so set counter=0
    if (m_Age<60)   //young fawn, ask mum for careperiod
    {
      #ifdef JUJ__Debug1
      if(Mum==NULL)
      {
        m_OurLandscape ->Warn("Roe_Fawn:FARecover():NULL pointer, Mum!","");
        exit( 1 );
      }
      #endif
      #ifdef JUJ__Debug3
      if(Mum->IsAlive()!=0x0DEADC0DE)
      {
        m_OurLandscape ->Warn("Roe_Fawn:FARecover():Deadcode warning, Mum!","");
        exit( 1 );
      }
      #endif
      bool CanSuckle=Mum->On_InitCare(this);
      if(CanSuckle==true)
      {
        return 38;
      }
      else if ((CanSuckle==false)&&(m_Age<14))//not old enough to feed, so go to hide
      {
        return 29;   //Hide
      }
      else   //go to feed
      {
        m_FeedCount=0;
        return 37;
      }
    }
  }
  return 37;  // else feed
}

//---------------------------------------------------------------------------
/**
Roe_Fawn::FARunToMother - the fawn object gets it's mothers position and will run 
to her avoiding non-habitat. 
Calls SupplyPosition(), RunTo()
*/
int Roe_Fawn::FARunToMother()
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Fawn:FARunToMother():Deadcode warning!","");
    exit( 1 );
  }
  #endif
  #ifdef JUJ__Debug1
  if(Mum==NULL)
  {
    m_OurLandscape ->Warn("Roe_Fawn:FARecover():NULL pointer,Mum","");
    exit( 1 );
  }
  #endif
  m_MinInState[0]++; //add 1 to time spend in this state
  //set care counter right if fawn is < 60 days
  if (m_Age<60) CareCounter(0);

  bool p_IsSafe=false;
  //Get mothers position and run to her avoiding non-habitat
  AnimalPosition xy=Mum->SupplyPosition();

  RunTo(xy.m_x,xy.m_y,false);
  if(m_IsDead) return 43; //Die
  else if (p_IsSafe) return 24;  //Recover
  else return 21;  //RunToMother
}
//---------------------------------------------------------------------------

/**
Roe_Fawn::FAOnNewDay - updates age of fawn object (+ 1 day), if age = 365 days, fawns is set to mature
(creates an adult object) and fawn object dies. Checks for fawn mortality and updates age group of fawn.
Calls functions Mature().
*/

int Roe_Fawn::FAOnNewDay()       //instant
{
   #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Fawn:FAOnNewDay():Deadcode warning!","");
    exit( 1 );
  }
  #endif

  m_Age++;
  if(m_Age>=365)  //adult
  {
    Mature();
    return 43;  //die
  }
  //Else find out if life expectancy has been reached
  int mort;
  long int die=random(100000);  //since mort rates are multiplied with 100000 to
                                //produce int values
  if (m_Age<180)
    mort=(int)FawnDMR[0];
  else
    mort=(int)FawnDMR[1];
  if (die<mort)
  {
    return 43;  //Die
  }
  else   //still alive
  {
    //set current agegroup
      if(m_Age<10) m_Agegroup=0;
      else if(m_Age<=60) m_Agegroup=1;
      else if (m_Age<=120) m_Agegroup=2;
      else if (m_Age<=300) m_Agegroup=3;
      else m_Agegroup=4;
  }
 return 6; //Update energy
}
//---------------------------------------------------------------------------
/**
Roe_Fawn::Mature - maturation of fawn into adult. Creates new adult object (male of female)
in same geographical place as fawn and sets adult properties.
Calls CreateObjects().
*/
void Roe_Fawn::Mature()
{
   m_Mature=true;
   //A new adult object is created
   Deer_struct* ads;
   ads=new Deer_struct;
   int objecttype;

   if (m_Sex==true) objecttype=1;  //male
   else objecttype=2; //female
   //call CreateObjects
   ads->Pop = m_OurPopulation;
   ads-> L = m_OurLandscape;
   ads->x = m_Location_x;
   ads->y = m_Location_y;
   ads->mum = Mum;
   ads->size = m_Size;
   ads->age = m_Age;
   ads->group = m_MyGroup;
   //debug
   
   ads->ID = m_ID;
   m_OurPopulation->CreateObjects(objecttype,this,NULL,ads,1);
   delete ads;
}
//---------------------------------------------------------------------------
/**
Roe_Fawn::FAUpdateEnergy - Calculates how much energy is used (in cal/minute) in different activities.
Calculates net energy (gained-used) and determines whether excess enery is added to reserves (fawns do not
have reserves the first 2 months, if negative energy balance they immediately loose weight) or is used to
put on weight. A negative net energy causes the fawn object to loose weight - if fawn weight falls
below a minimum value, it will die. If the fawn is in very bad condition it will increase 
forage time. Returns states such as feed or die.
*/

int Roe_Fawn::FAUpdateEnergy()
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Fawn:FAUpdateEnergy():Deadcode warning!","");
    exit( 1 );
  }
  #endif
  //all energetic calculations in cal.
  //Calculate how much energy used in different activities
  float EnergyUsed=0;
  int month=m_OurLandscape->SupplyMonth();
  for (int i=0; i<6; i++)
  {
    float cost;
    if (CostFawnAct[i]==1)   cost=FawnRMR;
    else cost=(float) CostFawnAct[i];
    EnergyUsed += (float) (cost * m_MinInState[i] * (m_Size*0.001));
  }
  //Note: Fawns do not have reserves the first 2 months. If negative energybalance,
  //they immediately loose weight.
  float result = m_EnergyGained - EnergyUsed;     //in cal
  if (result>0)
  {
    if((m_Age>60)&&(m_Reserves<5)) m_Reserves++;   //increase reserves
    else
    {
      m_Size += (int)floor(result * Anabolic_Inv);  //put on weight
    }
  }
  else if (result<0)
  {
    if((m_Age>60)&&(m_Reserves>0))   m_Reserves--;   //loose reserves
    else
    {
      m_Size += (int)floor(result*Catabolic_Inv);   //loose weight
    }
  }
  //get length of feeding period
  if (m_Age>=60)
  {
      //adjust lenght of feeding bouts
      m_LengthFeedPeriod = (int)(7*(m_Size*0.001)/
				      Female_FeedBouts[month-1]);
      //if approaching lower critical bodyweight, increase feed period
      if((m_Size < MinWeightFawn[m_Agegroup]+1500)
                                       &&(m_Size >= MinWeightFawn[m_Agegroup]))
      {
        m_LengthFeedPeriod+=2;
      }
      //If m_Size < MinSize roe dies
      else if (m_Size < MinWeightFawn[m_Agegroup])
      {
        return 43;
      }
  }
  //set counter of daily energy gain to zero
  m_EnergyGained=0;
  for(int i=0;i<6;i++)
  {
    m_MinInState[i]=0;
  }
  return m_LastState;
}
//---------------------------------------------------------------------------
/**
Roe_Fawn::FADie - function for managing death of a fawn object. The fawn object
sends message to siblings and mom. The fawn adds itself to list of dead animals. 
Function returns dead state.
Calls functions On_IsDead().
*/

int Roe_Fawn::FADie(void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Fawn:FADie():Deadcode warning!","");
    exit( 1 );
  }
  #endif
 
  //add yourself to list of dead animals
  int sex;
  if(m_Sex) sex=1;
  else sex=2;
  for(int i=0;i<2000;i++)
  {
    if(m_OurPopulation->DeadList[i][0] == 0) //empty entrance
    {
      m_OurPopulation->DeadList[i][0] = sex;  //'1' for male, '2' for female
      m_OurPopulation->DeadList[i][1] = m_Age; //age in days
      break;
    }
  }
  //tell siblings
  for (int i=0; i<3; i++)
  {
      #ifdef JUJ__Debug3
      if((m_MySiblings[i]!=NULL)&&(m_MySiblings[i]->IsAlive()!=0x0DEADC0DE))
      {
        m_OurLandscape ->Warn("Roe_Fawn:FADie():Bad pointer to m_MySiblings[i]!","");
        exit( 1 );
      }
      #endif
     if (m_MySiblings[i] != NULL)
     {
       m_MySiblings[i]->On_IsDead(this,4,true);  //'4' indicates siblings
     }
  }

  if(Mum!=NULL) //tell mum
  {
    #ifdef JUJ__Debug3
    if(Mum->IsAlive()!=0x0DEADC0DE)
    {
       m_OurLandscape ->Warn("Roe_Fawn:FADie():Deadcode warning, Mum!","");
       exit( 1 );
    }
    #endif
     //send to mother
     Mum->On_IsDead(this,3,true);   //'3' indicates offspring
  }

  m_CurrentStateNo=-1;
  return 46;  //DeathState
}
//---------------------------------------------------------------------------
/**
Roe_Fawn::BeginStep - checks if fawn object is dead
*/
void Roe_Fawn::BeginStep (void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Fawn:BeginStep():Deadcode warning!","");
    exit( 1 );
  }
  #endif
 if (CurrentRState==rds_FADeathState) return;
}
//---------------------------------------------------------------------------
/**
Roe_Fawn::Step - function called every time step. The functions checks if done with a given state (m_StepDone)
and governs states and transitions to other states. Calls functions: FAInit(), FADie(), FAOnNEwDay(),
FAUpdateEnergy(), FASuckle(), FAFeed(), FARuminate(), FARunToMother(), FARecover(), FAHide().
*/
void Roe_Fawn::Step (void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Fawn:Step():Deadcode warning!","");
    exit( 1 );
  }
  #endif
 if (m_StepDone || (CurrentRState==rds_FADeathState)) return;

 switch (CurrentRState)
 {
   case rds_Initialise:
     CurrentRState=rds_FAInit;
     break;
   case rds_FAInit:
     switch(FAInit())
     {
       case 38:
         CurrentRState=rds_FASuckle;
         break;
       default:
         m_OurLandscape ->Warn("Roe_Fawn:Step():rds_FAInit: no matching case!","");
         exit( 1 );
         break;
     }
     m_StepDone=true;
     break;
   case rds_FADie:
    switch (FADie())
    {
      case 46:
        CurrentRState=rds_FADeathState;
        break;
      default:
        m_OurLandscape ->Warn("Roe_Fawn:Step():rds_FADie: no matching case!","");
        exit( 1 );
        break;
    }
    m_StepDone=true;
    break;

   case rds_FAOnNewDay:
    switch (FAOnNewDay())
    {
      case 6: //update energy
        CurrentRState=rds_FAUpdateEnergy;
        m_StepDone=true;
        break;
      case 43: //died or matured
        CurrentRState=rds_FADie;
        break;
      default:
        m_OurLandscape ->Warn("Roe_Fawn:Step():rds_FAOnNewDay: no matching case!","");
         exit( 1 );
        break;
    }
    break;

   case rds_FAUpdateEnergy:
    switch (FAUpdateEnergy())
    {
      case 24: //rds_FARecover
        CurrentRState=rds_FARecover;
        m_StepDone=true;
        break;
      case 29:
        CurrentRState=rds_FAHide;
        m_StepDone=true;
        break;
      case 34: //rds_FARuminate
        CurrentRState=rds_FARuminate;
        m_StepDone=true;
        break;
      case 37: //rds_FAFeed
        CurrentRState=rds_FAFeed;
        m_StepDone=true;
        break;
      case 38: //suckle
        CurrentRState=rds_FASuckle;
        m_StepDone=true;
        break;
      case 43: //rds_FADie:
        CurrentRState=rds_FADie;
        break;
      default:
        m_OurLandscape ->Warn("Roe_Fawn:Step():rds_FAUpdateEnergy: no matching case!","");
         exit( 1 );
        break;
    }
    break;
   case rds_FASuckle:
    switch (FASuckle())
    {
      case 37: //rds_FAFeed:
        CurrentRState=rds_FAFeed;
        break;
      case 29: //rds_FAHide:
        CurrentRState=rds_FAHide;
        break;
      case 38: //suckle
        CurrentRState=rds_FASuckle;
        break;
      default:
        m_OurLandscape ->Warn("Roe_Fawn:Step():rds_FASuckle: no matching case!","");
         exit( 1 );
        break;
    }
    m_StepDone=true;
    break;

   case rds_FAFeed:
    switch (FAFeed())
    {
      case 29: //rds_FAHide:
        CurrentRState=rds_FAHide;
        break;
      case 37: //rds_FAFeed:
        CurrentRState=rds_FAFeed;
        break;
      case 38: //rds_FASuckle:
        CurrentRState=rds_FASuckle;
        break;
      case 34: //rds_FARuminate:
        CurrentRState=rds_FARuminate;
        break;
      default:
        m_OurLandscape ->Warn("Roe_Fawn:Step():rds_FAFeed: no matching case!","");
         exit( 1 );
        break;
    }
    m_StepDone=true;
    break;

   case rds_FARuminate:
    switch (FARuminate())
    {
      case 37: //rds_FAFeed:
        CurrentRState=rds_FAFeed;
        m_StepDone=true;
        break;
      case 34: //rds_FARuminate:
        CurrentRState=rds_FARuminate;
        m_StepDone=true;
        break;
      case 43:
        CurrentRState=rds_FADie;
        break;
      default:
        m_OurLandscape ->Warn("Roe_Fawn:Step():rds_FARuminate: no matching case!","");
         exit( 1 );
        break;
    }
    break;

   case rds_FARunToMother:
    switch (FARunToMother())
    {
      case 24: //rds_FARecover:
        CurrentRState=rds_FARecover;
        break;
      case 21: //rds_FARunToMother
        CurrentRState=rds_FARunToMother;
        break;
      default:
        m_OurLandscape ->Warn("Roe_Fawn:Step():rds_FARunToMother: no matching case!","");
         exit( 1 );
        break;
    }
    m_StepDone=true;
    break;

   case rds_FARecover:
    switch (FARecover())
    {
      case 37: //rds_FAFeed:
        CurrentRState=rds_FAFeed;
        break;
      case 38: //rds_FASuckle:
        CurrentRState=rds_FASuckle;
        break;
      case 24: //rds_FARecover:
        CurrentRState=rds_FARecover;
        break;
      default:
        m_OurLandscape ->Warn("Roe_Fawn:Step():rds_FARecover: no matching case!","");
         exit( 1 );
        break;
    }
    m_StepDone=true;
    break;
  case rds_FAHide:
   switch (FAHide())
   {
     case 37: //rds_FAFeed:
       CurrentRState=rds_FAFeed;
       break;
     case 38: //rds_FASuckle:
       CurrentRState=rds_FASuckle;
       break;
     case 29: //rds_FAHide
        CurrentRState=rds_FAHide;
        break;
     default:
       m_OurLandscape ->Warn("Roe_Fawn:Step():rds_FAHide: no matching case!","");
         exit( 1 );
       break;
   }
   m_StepDone=true;
   break;
  default:
    m_OurLandscape ->Warn("Roe_Fawn:Step(): no matching case!","");
         exit( 1 );
 }
}
//---------------------------------------------------------------------------
/**
Roe_Fawn::EndStep - checks if fawn object is dead and adds to number of timesteps.
If 1 whole day has passed, current state is set to FOnNewDay
*/
void Roe_Fawn::EndStep ()
{
 #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Fawn:EndStep():Deadcode warning!","");
         exit( 1 );
  }
  #endif
 if (CurrentRState==rds_FADeathState) return;

 timestep++;
 if (m_OurPopulation->StepCounter %144==0) //1 day has passed
 {
    m_weightedstep=0;
    CurrentRState=rds_FAOnNewDay;
 }
}
//---------------------------------------------------------------------------
/**
Roe_Fawn::CareCounter - is called at the beginning of every timestep and keeps track of
dynamic list that counts last 6 timesteps as '1' if fawn has received the required care 
and '0' if not.
*/
void Roe_Fawn::CareCounter(int state) //called at the beginning of every timestep
{
  m_Count %= 6;
  #ifdef JUJ__Debug2
  if (m_Count>5)
  {
    m_OurLandscape ->Warn("Roe_Fawn:CareCounter: variable out of range!","");
         exit( 1 );
  }
  #endif
  
  m_CareLastHour[m_Count]=state;
  m_Count++;
}
//---------------------------------------------------------------------------
/**
Roe_Fawn::RunTo - directed movement towards mothers position, all habitat except lethal
habitat is used. Roads can be crossed and probability of dying is calculated.
Calls functions  DirectionTo(), DistanceTo(), SupplyElementType(), SupplyRoadWidth(), 
CalculateRoadMortality(), 
*/
int Roe_Fawn::RunTo(int mum_x, int mum_y, bool /* p_IsSafe */)
{
  //directed movement towards mothers position, all habitat except lethal
  //habitat is used. Roads can be crossed
  int t[8];
  int q[8];
  int thisfar=0; //run this far in one timestep
  int x=m_Location_x;
  int y=m_Location_y;
  //First get direction and distance to Mum
  int dir=DirectionTo(x,y,mum_x,mum_y);
  int dist=DistanceTo(x,y,mum_x,mum_y);
  //and direction to threat
  int threat_dir=DirectionTo(x,y,m_danger_x,m_danger_y);
  if (dist<100) thisfar=dist;
  else thisfar=100;
  //Is it possible to go in that direction
  for (int i=0; i<thisfar; i++)
  {
    t[0]=dir;
    t[0]=(t[0]+8) & 7;
    q[0]=m_OurLandscape->SupplyElementType(x+Vector_x[t[0]],y+Vector_y[t[0]]);
    if ((q[0]<tole_SmallRoad)&&(t[0]!=threat_dir)) //OK
    {
      //change coordinates
      x+=Vector_x[t[0]];
      y+=Vector_y[t[0]];
    }
    else  //not possible, so check all other directions except dir+4
    {
      t[1]=dir-1;
      t[1]=(t[1]+8) & 7;
      t[2]=dir+1;
      t[2]=(t[2]+8) & 7;
      t[3]=dir-2;
      t[3]=(t[3]+8) & 7;
      t[4]=dir+2;
      t[4]=(t[4]+8) & 7;
      t[5]=dir+3;
      t[5]=(t[5]+8) & 7;
      t[6]=dir-3;
      t[6]=(t[6]+8) & 7;
      q[1]=m_OurLandscape->SupplyElementType(x+Vector_x[t[1]],y+Vector_y[t[1]]);
      q[2]=m_OurLandscape->SupplyElementType(x+Vector_x[t[2]],y+Vector_y[t[2]]);
      q[3]=m_OurLandscape->SupplyElementType(x+Vector_x[t[3]],y+Vector_y[t[3]]);
      q[4]=m_OurLandscape->SupplyElementType(x+Vector_x[t[4]],y+Vector_y[t[4]]);
      q[5]=m_OurLandscape->SupplyElementType(x+Vector_x[t[5]],y+Vector_y[t[5]]);
      q[6]=m_OurLandscape->SupplyElementType(x+Vector_x[t[6]],y+Vector_y[t[6]]);
      for (int j=1; j<6;j++)
      {
        if ((q[j]<tole_SmallRoad)&&(t[j]!=threat_dir))
        {
          x+=Vector_x[t[j]];
          y+=Vector_y[t[j]];
          break;
        }
        else if (q[j]==int (tole_SmallRoad||tole_LargeRoad))   //only option available is road habitat
        {
          //get width of road
          int width=m_OurLandscape->SupplyRoadWidth(x+Vector_x[t[j]],y+Vector_y[t[j]]);
          float mort=CalculateRoadMortality(x+Vector_x[t[j]],y+Vector_y[t[j]],width);
          int die=random(100);
          if (die<mort)  //fawn dies
          {
            m_IsDead=true;
          }
          else
          {
            x+=Vector_x[t[j]];
            y+=Vector_y[t[j]];
            break;
          }
        }
      }
    }
    //change roes position
    m_Location_x=x;
    m_Location_y=y;

    //finished running 100 steps so check if safe
    int threat_dist = DistanceTo(m_Location_x,m_Location_y,m_danger_x,m_danger_y);
    if (threat_dist > 100)
    {
      return 1;     //doesn't matter how far from mum, is safe
    }
    else  //get new dist to see if with mum
    {
      dist=DistanceTo(x,y,mum_x,mum_y);
      if (dist > 2)  //not safe
      {
        return 0;
      }
    }
  }
  return 1;
}
//----------------------------------------------------------------------------
/**
Roe_Fawn::SelectBedSite - function for selecting a good spot to hide. Fawn ob ject
checks habitat type at location. Good habitat is forest type vegetation, or open land
if vegetation is high enough. New bed site is added to bed site list. If not able to find 
new bed site search bed site list and move to closest bed site.
Calls SupplyElementType(), SupplyVegHeight(), DistanceTo(), WalkTo(), SeekCover().
*/
void Roe_Fawn::SelectBedSite() //Locates a good spot to hide
{
   TTypesOfLandscapeElement Elem=m_OurLandscape->
                                SupplyElementType(m_Location_x,m_Location_y);
   //forest type vegetation
   if ((Elem >= tole_PitDisused)&&(Elem < tole_StoneWall)||(Elem==tole_Hedges))
   {
     // add new bedsite to bedsite list
     m_memorypointer ++;
     if (m_memorypointer>=15) {
       m_memorypointer = 0;
     }
     // **FN**
     // A test is much faster than a modulus, which is a division, of
     // course.
     //m_memorypointer = m_memorypointer % 15;
     #ifdef JUJ__Debug2
     if(m_memorypointer>=15)
     {
       m_OurLandscape ->Warn("Roe_Fawn:SelectBedSite():Variable out of range,m_memorycounter!","");
         exit( 1 );
     }
     #endif
     BedSiteList[m_memorypointer][0]=m_Location_x;
     BedSiteList[m_memorypointer][1]=m_Location_y;
   }
   else if ((Elem > tole_Railway)&&(Elem < tole_PitDisused)) //open land
   {
     //get VegHeight
     float VegHeight=(float) m_OurLandscape->SupplyVegHeight(m_Location_x,
						     m_Location_y);
     if (VegHeight>70)      //OK hide on spot
     {
       // add new bedsite to bedsite list
       m_memorypointer ++;
       if(m_memorypointer>=15) {
	 m_memorypointer = 0;
       }
       //m_memorypointer = m_memorypointer %  15;
        #ifdef JUJ__Debug2
       if(m_memorypointer>=15)
       {
         m_OurLandscape ->Warn("Roe_Fawn:SelectBedSite():Variable out of range,m_memorycounter!","");
         exit( 1 );
       }
       #endif
       BedSiteList[m_memorypointer][0]=m_Location_x;
       BedSiteList[m_memorypointer][1]=m_Location_y;
     }
   }
   else   //not good here, check bedsitelist
   {
       int pick=0;
       if (BedSiteList[0][0] == -1) pick = -1; //list empty
       else
       {
         for(int i=0; i<15; i++)
         {
           int best=100;
           int dist= DistanceTo(m_Location_x,m_Location_y,
                    BedSiteList[i][0],BedSiteList[i][1]);
           if(dist<best)
           {
             pick=i;
             break;
           }
           else pick=-1;  //no bedsites nearby
        }
       }
       if (pick != -1)
       {
         #ifdef JUJ__Debug2
         if(pick>=15)
         {
           m_OurLandscape ->Warn("Roe_Fawn:SelectBedSite():Variable out of range,pick!","");
           exit( 1 );
         }
         #endif
         WalkTo(BedSiteList[pick][0],BedSiteList[pick][1]);
         //up to 150 steps, so fawn will always get there in 1 timestep
       }
       else   //nothing found
       {
         SeekCover(2);
         // add new bedsite to bedsite list
         m_memorypointer ++;
	 if(m_memorypointer>=15) {
	   m_memorypointer = 0;
	 }

         // m_memorypointer = m_memorypointer %  15;
         #ifdef JUJ__Debug2
         if(m_memorypointer>=15)
         {
           m_OurLandscape ->Warn("Roe_Fawn:SelectBedSite():Variable out of range,m_memorycounter!","");
           exit( 1 );
         }
         #endif
         BedSiteList[m_memorypointer][0]=m_Location_x;
         BedSiteList[m_memorypointer][1]=m_Location_y;
       }
   }
}
//---------------------------------------------------------------------------
/**
Roe_Fawn::Roe_Fawn - constructor for fawn object. Sets all attributes. 

*/

Roe_Fawn::Roe_Fawn (int x, int y,int size,int group, bool sex, Landscape* L,RoeDeer_Population_Manager* RPM)
   :  Roe_Base (x,y,L,RPM)
{
  m_Age = 0;
  m_Sex=sex;
  m_Mature=false;
  m_IsDead=false;
  m_Orphan=false;
  m_InHide=false;
  m_Agegroup = 0;
  m_MinHourlyCare=0;
  m_Bedsite_x=0;
  m_BedSite_y=0;
  m_LastState=37; //Feed. Could be any safe state
  m_LengthFeedPeriod=0;
  m_memorypointer=-1;
  m_SearchRange=200;  //same for all fawns
  m_Count=0;
  //set pointers to NULL
  m_Size=size;
  m_MyGroup=group;
  for(int i=0;i<3;i++) m_MySiblings[i] = NULL;

  for(int i=0;i<6;i++) m_CareLastHour[i] = 0;
  for (int i=0;i<6;i++) m_MinInState[i] = 0;
  for (int i=0;i<15;i++)
  {
    BedSiteList[i][0]=-1;
    BedSiteList[i][1]=-1;
  }
  m_Reserves=5;
}
//---------------------------------------------------------------------------
Roe_Fawn::~Roe_Fawn ()
{
  //remove yourself from group
  
    m_OurPopulation->RemoveFromGroup(this,m_MyGroup);
    m_MyGroup=-1;

    for(int i=0;i<3;i++)
    {
      if(m_MySiblings[i]!=NULL)
      {
        m_MySiblings[i]=NULL;
      }
    }
}
//---------------------------------------------------------------------------
/**
Roe_Fawn::On_IsDead - governs a fawn objects "response" to death of another object.
If dead object is mother and fawn is less than 60days, the fawn will die. Else it is set to
be an orphan. If same-age sibling is dead, sibling is removed from sibling list.

*/
void Roe_Fawn::On_IsDead(Roe_Base* Someone, int whos_dead, bool /* fawn */)
{
  #ifdef JUJ__Debug3
  if(Someone->IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Fawn:On_IsDead():Deadcode warning, someone!","");
    exit( 1 );
  }
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Fawn:On_IsDead(): Deadcode warning","");
    exit( 1 );
  }
  #endif
   if (whos_dead==0)   //mother is dead
   {
     Mum=NULL;  //remove pointer to mother
     if (m_Age <= 60)  //fawn will not survive alone
     {
       m_IsDead=true;
       CurrentRState=rds_FADie;
     }
     else
     {
       m_Orphan=true;
     }
   }
   else if (whos_dead==4)   //same-age sibling
   {
     //remove from m_MySiblings[]
     for (int i=0; i<3; i++)
     {
       #ifdef JUJ__Debug3
       if(m_MySiblings[i]!=NULL) if(m_MySiblings[i]->IsAlive()!=0x0DEADC0DE)
       {
         m_OurLandscape ->Warn("Roe_Fawn:On_IsDead(): Bad pointer, m_MySiblings","");
         exit( 1 );
       }
       #endif
       if (m_MySiblings[i]==Someone)
       {
         m_MySiblings[i]=NULL;  //remove pointer to that sibling
       }
     }
   }
}
//--------------------------------------------------------------------------
/**
Roe_Fawn::On_MumAbandon - fawn response to mother abandoning it. Mum is deleted,
if fawn is less than 2 months it will die, otherwise it is set as orphaned.
*/
void Roe_Fawn::On_MumAbandon(Roe_Female* /* mum */)
{
  #ifdef JUJ__Debug1
  if((mum!=Mum)||(Mum==NULL))
  {
    m_OurLandscape ->Warn("Roe_Fawn:On_MumAbandon: Bad pointer, Mum","");
    exit( 1 );
  }
  #endif
  Mum=NULL; //don't need to tell mum because she already deletes her fawn list
  if(m_Age<=60)
  {
    m_IsDead=true;  //fawn will not survive
    CurrentRState=rds_FADie;
  }
  else
  {
    m_Orphan=true;
  }
}


//----------------------------------------------------------------------------
/**
Roe_Fawn::On_ApproachOfDanger - determines fawn behavior when exposed to a threath.
When fawn < 60 days it either hides or runs to mother depending on flush distance (this
is again depending on age group of fawn). 
When > 60 days it always runs either towards mother (if not an orphan) or away from threath.
Calls DistanceTo(), Running().
*/
void Roe_Fawn::On_ApproachOfDanger(int p_To_x,int p_To_y)
{
  //when fawn < 60 days it either hides or runs to mother. When > 60 days it
  //always runs
  if (m_Age<60)
  {
    //get distance to threat
    if (DistanceTo(m_Location_x,m_Location_y,p_To_x,p_To_y)>=Flush_dist[m_Agegroup])
    {
      CurrentRState=rds_FAHide;
    }
    else CurrentRState=rds_FARunToMother;
  }
  else //more than 2 months old, never hides
  {
    if (m_Orphan==false)  CurrentRState=rds_FARunToMother;
    else
    {
      Running(p_To_x,p_To_y);
      if(m_IsDead) CurrentRState=rds_FADie;
      else CurrentRState=rds_FARecover;
    }
  }
}
//---------------------------------------------------------------------------
/**
Roe_Fawn::Send_InitCare - sends message to mother asking if it can feed. If message 
returns true, the fawn will be in the suckle state, otherwise feeding state.
*/
void Roe_Fawn::Send_InitCare()
{
  #ifdef JUJ__Debug1
  if(Mum==NULL)
  {
    m_OurLandscape ->Warn("Roe_Fawn:Send_InitCare():NUL pointer","");
    exit( 1 );
  }
  #endif

  bool CanFeed = Mum->On_InitCare(this);   //send message to mum and get answer
  if (CanFeed==true)
  {
    CurrentRState=rds_FASuckle;
  }
  else
  {
    CurrentRState=rds_FAFeed;
  }
}
//---------------------------------------------------------------------------


void Roe_Fawn::On_UpdateGroup(int /* p_group_x */,int /* p_group_y */)
{
  //fawns do not need to know about where the rangecentre is, since they always
  //position themselves according to their mothers position
}
//---------------------------------------------------------------------------

void Roe_Fawn::On_EndGroup()
{

}
//----------------------------------------------------------------------------
/**
Roe_Fawn::On_ChangeGroup - removes fawn object from old group and add it to new group.
Calls RemoveFromGroup(), AddToGroup().
*/
void Roe_Fawn::On_ChangeGroup(int newgroup)
{
  //remove yourself from your old group
  m_OurPopulation->RemoveFromGroup(this,m_MyGroup);
  //change groupnumber
  m_MyGroup=newgroup;
  //add yourself to the new group
  m_OurPopulation->AddToGroup(this,m_MyGroup);
}
//---------------------------------------------------------------------------



