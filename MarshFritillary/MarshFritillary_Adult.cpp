#include <iostream>
#include <fstream>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../Landscape/ls.h"
#include "../MarshFritillary/MarshFritillaryHdrs.h"

extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;

// PARAMETERS
CfgFloat cfg_MarshFrit_AdultLifespanDD("CFG_MARSHFRIT_ADULTLIFESPANDD",CFG_CUSTOM,100);
CfgFloat cfg_MarshFrit_AdultLifespanThresh("CFG_MARSHFRIT_ADULTLIFESPANTRESH",CFG_CUSTOM,10);
CfgFloat cfg_MarshFrit_AdultMortRate("CFG_MARSHFRIT_ADULTMORTRATE",CFG_CUSTOM,0.05);
// END PARAMETERS

	double MarshFritillary_Adult::m_adult_mortrate = cfg_MarshFrit_AdultMortRate.value();
	double MarshFritillary_Adult::m_adult_lifespanThresh = cfg_MarshFrit_AdultLifespanThresh.value();
	double MarshFritillary_Adult::m_adult_lifespanTarget = cfg_MarshFrit_AdultLifespanDD.value();

void MarshFritillary_Adult::SetParameters()
{
	m_adult_mortrate = cfg_MarshFrit_AdultMortRate.value();
	m_adult_lifespanThresh = cfg_MarshFrit_AdultLifespanThresh.value();
	m_adult_lifespanTarget = cfg_MarshFrit_AdultLifespanDD.value();
}

MarshFritillary_Adult::MarshFritillary_Adult( int a_x, int a_y, MarshFritillary_Population_Manager* a_PopulationManager, Landscape* a_Landscape)
	: MarshFritillary_Base( a_x, a_y, a_PopulationManager, a_Landscape)
{
	;
}


MarshFritillary_Adult::~MarshFritillary_Adult(void)
{
	;
}


bool MarshFritillary_Adult::Mortality()
{	
	/**
	* Simple daily mortality chance.\n
	*/
	if (g_rand_uni() < m_adult_mortrate)
	{
			return true;
	}
	return false;
}

ButterflyBehaviour MarshFritillary_Adult::DailyActivity()
{	
	if (Mortality())
	{
		m_StepDone = true;
		return abb_dead;
	}
	double temptoday = m_OurLandscape->SupplyTemp();
	double dd = temptoday - m_adult_lifespanThresh;
	if (dd<0) dd = 0;
	m_DayDegrees+=dd;
	if (m_DayDegrees > m_adult_lifespanTarget) return abb_dead;
	return abb_moving;
}


