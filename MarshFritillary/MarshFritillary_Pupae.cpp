#include <iostream>
#include <fstream>
#include "../Landscape/ls.h"
#include "../MarshFritillary/MarshFritillaryHdrs.h"

extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;

//  PARAMETERS
CfgFloat cfg_MarshFrit_PupalDevelTarget("CFG_MARSHFRIT_PUPALDEVELTARGET",CFG_CUSTOM,180.0);
CfgFloat cfg_MarshFrit_PupalDevelThreshold("CFG_MARSHFRIT_PUPALDEVELTHRESHOLD",CFG_CUSTOM,3.1);
CfgFloat cfg_MarshFrit_PupalMortRate("CFG_MARSHFRIT_PUPALMORTRATE",CFG_CUSTOM,0.01);
// END PARAMETERS

	double MarshFritillary_Pupae::m_DevelopmentThreshold = 0;
	double MarshFritillary_Pupae::m_DevelopmentTarget = 0;
	double MarshFritillary_Pupae::m_PupalMortRate = 0;

void MarshFritillary_Pupae::SetParameters()
{
	m_DevelopmentThreshold = cfg_MarshFrit_PupalDevelThreshold.value();
	m_DevelopmentTarget = cfg_MarshFrit_PupalDevelTarget.value();
	m_PupalMortRate = cfg_MarshFrit_PupalMortRate.value();
}

MarshFritillary_Pupae::MarshFritillary_Pupae( int a_x, int a_y, MarshFritillary_Population_Manager* a_PopulationManager, Landscape* a_Landscape, int a_number)
	: MarshFritillary_EggMass( a_x, a_y, a_PopulationManager, a_Landscape, a_number)
{
	;
}


MarshFritillary_Pupae::~MarshFritillary_Pupae(void)
{
	;
}

void MarshFritillary_Pupae::Step(void)
{
	if (m_CurrentStateNo == abb_killthis || m_StepDone ) return;
	switch ((ButterflyBehaviour) m_CurrentStateNo)
	{
	  case abb_dead:
		m_CurrentStateNo = abb_killthis;
		m_StepDone = true;
		break;
	  case abb_initiation:
		  m_CurrentStateNo = abb_developing;
		  break;
	  case abb_developing:
		  m_CurrentStateNo = Develop();
		  m_StepDone = true;
		  break;
	  case abb_nextstage:
		  m_CurrentStateNo = Emerge();
		  m_StepDone = true;
		  break;
	  default:
		  m_OurLandscape->Warn("MarshFritillary_EggMass::SubStep0- unknown behavioural state",NULL);
		  exit(1);
   }
}


/** 
* This code is identical to that of the EggMass and Larvae, but is needed because the developmental parameters are static members
* which means that any base class will be unable to access them. The alternative is pass them as parameters, but then that 
* will cost more time and the use of static is to save time and space.
*/
ButterflyBehaviour MarshFritillary_Pupae::Develop(void)
{
/**
* The pupa is formed head down, attached to a twig or plant stem by the cremaster. 
* This stage lasts between 2 and 4 weeks, depending on temperature.
*/
	if (g_rand_uni() < m_PupalMortRate) return abb_dead;
	double temptoday = m_OurLandscape->SupplyTemp();
	double dd = temptoday - m_DevelopmentThreshold;
	if (dd<0) dd = 0;
	m_DayDegrees+=dd;
	if (m_DayDegrees < m_DevelopmentTarget) return abb_developing;
	return abb_nextstage;
}


ButterflyBehaviour MarshFritillary_Pupae::Emerge(void)
{
	struct_MFritillary amf;
	amf.x = m_Location_x;
	amf.y = m_Location_y;
	amf.number = 1;
	amf.L = m_OurLandscape;
	int males = random(m_numberofeggs);
	int females = m_numberofeggs - males;
//	for (int i=0; i<males; i++) m_OurPopulationManager->CreateObjects(atomf_Male,this,&amf,1); // males
	for (int i=0; i<females; i++) m_OurPopulationManager->CreateObjects(atomf_Female,this,&amf,1); // females	
	return abb_dead;
}

