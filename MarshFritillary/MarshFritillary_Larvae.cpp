#include <iostream>
#include <fstream>
#include "../Landscape/ls.h"
#include "../MarshFritillary/MarshFritillaryHdrs.h"

extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;

//  PARAMETERS
/** \brief Daydegrees needed after 1st January before emergence */
CfgFloat cfg_MarshFrit_HibernationTarget("CFG_MARSHFRIT_HIBERNATIONTARGET",CFG_CUSTOM,March);
/** \brief Threshold for day degrees for emergence after 1st Jan */
CfgFloat cfg_MarshFrit_HibernationThreshold("CFG_MARSHFRIT_HIBERNATIONTHRESHOLD",CFG_CUSTOM,March);
/** \brief Day-degrees target for hibernation */
CfgFloat cfg_MarshFrit_LarvaeDevelTarget("CFG_MARSHFRIT_LARVAEDEVELTARGET",CFG_CUSTOM,1020.0);
/** \brief Day-degrees threshold temperature at which there is zero development */
CfgFloat cfg_MarshFrit_LarvaeDevelThreshold("CFG_MARSHFRIT_LARVAEDEVELTHRESHOLD",CFG_CUSTOM,3.10);
/** \brief Daily individual-based mortality probability for larvae */
CfgFloat cfg_MarshFrit_LarvalIMortRate("CFG_MARSHFRIT_LARVALIMORTRATE",CFG_CUSTOM,0.02);
/** \brief Daily total colony mortality probability for larvae */
CfgFloat cfg_MarshFrit_LarvalTMortRate("CFG_MARSHFRIT_LARVALTMORTRATE",CFG_CUSTOM,0.075);
// END PARAMETERS

	double MarshFritillary_Larvae::m_DevelopmentThreshold = cfg_MarshFrit_LarvaeDevelThreshold.value();
	double MarshFritillary_Larvae::m_DevelopmentTarget = cfg_MarshFrit_LarvaeDevelTarget.value();
	double MarshFritillary_Larvae::m_larval_Imortrate = cfg_MarshFrit_LarvalIMortRate.value();
	double MarshFritillary_Larvae::m_larval_Tmortrate = cfg_MarshFrit_LarvalTMortRate.value();
	double MarshFritillary_Larvae::m_HibernationTarget = cfg_MarshFrit_HibernationTarget.value();
	double MarshFritillary_Larvae::m_HibernationThreshold = cfg_MarshFrit_HibernationThreshold.value();


void MarshFritillary_Larvae::SetParameters(void)
{
	m_DevelopmentThreshold = cfg_MarshFrit_LarvaeDevelThreshold.value();
	m_DevelopmentTarget = cfg_MarshFrit_LarvaeDevelTarget.value();
	m_larval_Imortrate = cfg_MarshFrit_LarvalIMortRate.value();
	m_larval_Tmortrate = cfg_MarshFrit_LarvalTMortRate.value();
	m_HibernationTarget = cfg_MarshFrit_HibernationTarget.value();
	m_HibernationThreshold = cfg_MarshFrit_HibernationThreshold.value();
}	
	
MarshFritillary_Larvae::MarshFritillary_Larvae( int a_x, int a_y, MarshFritillary_Population_Manager* a_PopulationManager, Landscape* a_Landscape, int a_number)
	: MarshFritillary_EggMass( a_x, a_y, a_PopulationManager, a_Landscape, a_number)
{
	/** 
	* Using RegisterLocation is the primary way in which density dependence is used in the model. Two larval colonies cannot be in the same 1 m2. \n
	*/
	if (!RegisterLocation())
	{
		m_CurrentStateNo = abb_killthis;
		m_StepDone = true; // We cannot return abb_dead here because this will remove a larva from the position grid and it won't be this one!
	}
	m_hibernating = false;
}


MarshFritillary_Larvae::~MarshFritillary_Larvae(void)
{
	;
}

bool MarshFritillary_Larvae::RegisterLocation(void)
{
	if (!m_OurPopulationManager->PosMapGet(m_Location_x, m_Location_y))
	{
		m_OurPopulationManager->PosMapSet(m_Location_x, m_Location_y) ;
		return true;
	}
	return false;
}

void MarshFritillary_Larvae::BeginStep(void)
{
	;
}
void MarshFritillary_Larvae::Step(void)
{
	if (m_CurrentStateNo == abb_killthis || m_StepDone ) return;
	switch ((ButterflyBehaviour) m_CurrentStateNo)
	{
	  case abb_dead:
		m_OurPopulationManager->PosMapClear(m_Location_x, m_Location_y) ;
		m_CurrentStateNo = abb_killthis;
		m_StepDone = true;
		break;
	  case abb_initiation:
		  m_CurrentStateNo = abb_developing;
		  break;
	  case abb_developing:
		  m_CurrentStateNo = Develop();
		  m_StepDone = true;
		  break;
	  case abb_hibernating:
		  m_CurrentStateNo = Hibernate();
		  m_StepDone = true;
		  break;
	  case abb_nextstage:
		  m_CurrentStateNo = Pupate();
		  m_OurPopulationManager->PosMapClear(m_Location_x, m_Location_y) ;		
		  m_CurrentStateNo = abb_killthis;
		  m_StepDone = true;
		  break;
	  default:
		  m_OurLandscape->Warn("MarshFritillary_EggMass::Step- unknown behavioural state",NULL);
		  exit(1);
   }
}

ButterflyBehaviour MarshFritillary_Larvae::Develop()
{	
/**
* On emerging from their eggs the larvae spin a silk web, by binding together leaves of the foodplant, in which they live and feed. 
* Larvae build new webs as they grow and even move to a new plant if necessary. In later instars, the webs can be quite conspicuous 
* on the foodplant. Larvae will also bask on the outside of the tent absorbing the sun's rays, where their increased temperature 
* aids digestion. \n
* After the third moult the larvae build a dense nest of silk low down in vegetation in which they hibernate. 
* Larvae will emerge from their nest with the onset of spring and can be seen basking in warm sun as early as February. 
* Larvae eventually split into smaller groups, continuing to build silk webs where they bask together to keep their body 
* temperature relatively high, even on cool days. More-mature larvae tend to feed alone and are often found wandering across 
* open ground looking for their next meal or, eventually, a pupation site. If there is a shortage of foodplant, the larva is known
* to feed on alternative food sources, such as Honeysuckle growing in hedgerows. There are 5 moults in total.\n
* The caterpillars are liable to be attacked by the parasitoid wasp Apanteles bignellii, especially in warm spring weather.\n
* 
* Until more information is available we assume that the 4th instar laval stage is reached at 67% of the total day degrees required for development, and 
* that food is not limiting.
* This requires the use of both a flag to indicate hibernation and a spring test to initiate development again. Intially this test is based on date, 
* introducing a new parameter.\n
*/
	if (Mortality()) 
	{
		return abb_dead;
	}
	double temptoday = m_OurLandscape->SupplyTemp();
	double dd = temptoday - m_DevelopmentThreshold;
	if (dd<0) return abb_developing; // No development possible, nothing to do this time
	m_DayDegrees+=dd;
	if (m_DayDegrees < m_DevelopmentTarget) return abb_developing;
	else 
	{
		m_hibernating = true;
		return abb_hibernating;
	}
}

ButterflyBehaviour MarshFritillary_Larvae::Hibernate()
{
	if (m_OurLandscape->SupplyDayInYear() < March) m_hibernating=false;
    if (!m_hibernating) 
	{
		if (g_rand_uni() < m_larval_Tmortrate)
		{
			return abb_dead;
		}
		double temptoday = m_OurLandscape->SupplyTemp();
		double dd = temptoday - m_DevelopmentThreshold;
		if (dd<0) return abb_hibernating; // No development possible, nothing to do this time
		m_DayDegrees+=dd;
		if (m_DayDegrees < m_DevelopmentTarget) return abb_hibernating;
		return abb_nextstage;
	}
	return abb_hibernating;
}

ButterflyBehaviour MarshFritillary_Larvae::Pupate()
{	
	struct_MFritillary amf;
	amf.x = m_Location_x;
	amf.y = m_Location_y;
	amf.number = m_numberofeggs;
	amf.L = m_OurLandscape;
	m_OurPopulationManager->CreateObjects(atomf_Pupa,this,&amf,1); // pupae
	return abb_dead;
}

bool MarshFritillary_Larvae::Mortality()
{	
	/**
	* The caterpillars are liable to be attacked by the parasitoid wasp Apanteles bignellii, especially in warm spring weather.\n
	* If the weather in early spring is dominated by clear skies, larvae of the Marsh Fritillary Euphydryas aurinia can warm themselves up by basking in the
	* sunshine, and can feed and develop quickly. Their parasitoid however, the Braconid wasp Apanteles bignellii spends the early spring as a pupa, hidden
	* in the shade amongst grasses where the temperature is lower. Consequently the aurinia larvae develop more rapidly than the Apanteles pupae, and have 
	* pupated themselves before the adult wasps emerge. A few weeks later large numbers of Marsh Fritillaries emerge and breed.\n
	* On the other hand, if the spring weather is predominantly cloudy, the wasps have the advantage. Their pupae develop at much the same speed as 
	* in a sunny spring because in both situations they metamorphose in cool shady situations. The aurinia larvae however feed and grow more slowly, 
	* as they need sunshine to make them active. Consequently the wasps emerge early in relation to their hosts, and find large numbers of half-grown 
	* caterpillars to attack, so the number of adult butterflies which ultimately emerge is much lower.\n
	*/
	/**
	* Here we need to consider sources of total mortality and those that reduce the numbers of larvae
	*/
	int reduc = (int) floor(m_larval_Imortrate * g_rand_uni() + 0.5);
	m_numberofeggs -= reduc; 
	if (m_numberofeggs<1) return true;
	if (g_rand_uni() < m_larval_Tmortrate) return true;
	return false;
}

