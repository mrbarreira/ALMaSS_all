#pragma once
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

class TAnimal;
class MarshFritillary_Population_Manager;

typedef enum
{
/**
	* This enumeration is used to avoid errors with array indices and for readability. Its primary use is to ensure the correct 
	* use of the array Population_Manager::TheArray
*/
	atomf_Egg = 0,
	atomf_Larva,
	atomf_Pupa,
	atomf_Male,
	atomf_Female
} ATypeOfMarshFritillary;

typedef enum
{
	abb_killthis = -1,
	abb_initiation = 0,
	abb_dead,
	abb_mortalitytest,
	abb_hibernating,
	abb_nextstage,
	abb_feeding,
	abb_developing,
	abb_ageing,
	abb_laying,
	abb_moving,
	abb_dailyactivity
} ButterflyBehaviour;


class MarshFritillary_Base : public TAnimal
{
public:
	// Methods
	/** \brief The MarshFritillary Constructor */
	MarshFritillary_Base( int a_x, int a_y, MarshFritillary_Population_Manager* a_PopulationManager, Landscape* a_Landscape);
	/** \brief The MarshFritillary Destructor */
	virtual ~MarshFritillary_Base(void);
	/** \brief The only sub-step needed for the AnAntiBass class */
	virtual void BeginStep(void) { ; }
    /** \brief used to set external parameter value to static members */
	virtual void SetParameters(void) { ; }
	virtual void Step(void) { ; }
	virtual void EndStep(void) { ; }
protected:
	// Attributes
	/** \brief This is a pointer to the actual population manager used. It must be defined at this level in the hierarchy. */
	MarshFritillary_Population_Manager *m_OurPopulationManager;
	double m_DayDegrees;
};

class MarshFritillary_EggMass :
	public MarshFritillary_Base
{
public:
	// Methods
	/** \brief The MarshFritillary_EggMass Constructor */
	MarshFritillary_EggMass( int a_x, int a_y, MarshFritillary_Population_Manager* a_PopulationManager, Landscape* a_Landscape, int a_number  );
	/** \brief The MarshFritillary_EggMass Destructor */
	virtual ~MarshFritillary_EggMass(void);
	virtual void BeginStep(void) { ; }
	virtual void Step();
	virtual void EndStep(void) { ; }
	/** \brief get the current number of eggs */
	int GetNumberOfEggs() { return m_numberofeggs; }
    /** \brief used to set external parameter value to static members */
	virtual void SetParameters(void);
protected:
	// Attributes
	int m_numberofeggs;
	// Methods
	/** \brief The MarshFritillary_EggMass development behaviour */
	virtual ButterflyBehaviour Develop();
	/** \brief The MarshFritillary_EggMass hatching behaviour */
	virtual ButterflyBehaviour Hatch();
private:
	// Attributes
	static double m_DevelopmentThreshold;
	static double m_DevelopmentTarget;
	static double m_EggMortRate;
};

class MarshFritillary_Larvae :
	public MarshFritillary_EggMass
{
public:
	/** \brief The MarshFritillary_Larvae Constructor */
	MarshFritillary_Larvae( int a_x, int a_y, MarshFritillary_Population_Manager* a_PopulationManager, Landscape* a_Landscape, int a_number );
	/** \brief The MarshFritillary_Larvae Destructor */
	virtual ~MarshFritillary_Larvae(void);
	virtual void BeginStep(void);
	virtual void Step(void);
	virtual void EndStep(void) { ; }
	/** \brief Used to start larval development in the simulation off in a sensible stage */
	void InitiateDDStartOfSim() { 
		m_DayDegrees = (int) (m_DevelopmentTarget*0.91); 
		m_hibernating = true; 
		m_CurrentStateNo=abb_hibernating; 
	}
    /** \brief used to set external parameter value to static members */
	virtual void SetParameters(void);
protected:
	/** \brief The MarshFritillary_Larvae development behaviour */
	virtual ButterflyBehaviour Develop();
	/** \brief The MarshFritillary_Larvae hibernate and subsequent development behaviour */
	virtual ButterflyBehaviour Hibernate();
	/** \brief The MarshFritillary_Larvae pupation behaviour */
	virtual ButterflyBehaviour Pupate();
	/** brief Daily test for mortality */
	virtual bool Mortality();
	/** brief Record our position if possible */
	bool RegisterLocation();
private:
	static double m_HibernationTarget;
	static double m_HibernationThreshold;
	static double m_DevelopmentThreshold;
	static double m_DevelopmentTarget;
	static double m_larval_Imortrate;
	static double m_larval_Tmortrate;
	bool m_hibernating;
};

class MarshFritillary_Pupae :
	public MarshFritillary_EggMass
{
public:
	MarshFritillary_Pupae( int a_x, int a_y, MarshFritillary_Population_Manager* a_PopulationManager, Landscape* a_Landscape, int a_number );
	virtual ~MarshFritillary_Pupae(void);
	virtual void BeginStep(void) { ; }
	virtual void Step();
	virtual void EndStep(void) { ; }
    /** \brief used to set external parameter value to static members */
	virtual void SetParameters(void);
protected:
	virtual ButterflyBehaviour Develop(void);
	virtual ButterflyBehaviour Emerge(void);
private:
	static double m_DevelopmentThreshold;
	static double m_DevelopmentTarget;
	static double m_PupalMortRate;
};

class MarshFritillary_Adult : public MarshFritillary_Base
{
/**
* As for most butterfly species, the males emerge a few days before the females and set up small territories centred on a particular 
* plant or flower. They will dart up to investigate any passing butterfly flying nearby. They will also patrol suitable areas, 
* in the hope of finding a newly-emerged female. Once a female is found, the male flutters around her for a short while before 
* mating takes place. Before separating, the male seals the genital opening in the female with a substance that prevents another 
* male from mating with her - essentially providing a "chastity belt". Both adults are avid nectar feeders and will feed from a 
* variety of flowers, favourites including Buttercups and Thistles.\n
*
* The female will search out large foodplants when egg-laying, typically choosing one of the larger leaves on which to lay. 
* She is quite conspicuous as she makes her slow flight looking for suitable plants on which to lay, no doubt weighed down by her 
* load of eggs.\n
*
* Neither sex wanders far from where it emerged, although those emerging later in the flight season are often seen some distance 
* from the main breeding grounds; this dispersal may be a mechanism by which this species colonises new sites.\n
*/
public:
	MarshFritillary_Adult( int a_x, int a_y, MarshFritillary_Population_Manager* a_PopulationManager, Landscape* a_Landscape );
	virtual ~MarshFritillary_Adult(void);
    /** \brief used to set external parameter value to static members */
	virtual void SetParameters(void);
protected:
	static double m_adult_mortrate;
	static double m_adult_lifespanTarget;
	static double m_adult_lifespanThresh;
	virtual bool Mortality();
	/** \brief Common adult daily behaviour */
	virtual ButterflyBehaviour DailyActivity();
};

class MarshFritillary_Male : public MarshFritillary_Adult
{
public:
	MarshFritillary_Male( int a_x, int a_y, MarshFritillary_Population_Manager* a_PopulationManager, Landscape* a_Landscape );
	virtual ~MarshFritillary_Male(void);
	virtual void BeginStep(void) { ; }
	virtual void Step(void);
	virtual void EndStep(void) { ; }
protected:
	ButterflyBehaviour Moving(void);
};

class MarshFritillary_Female : public MarshFritillary_Adult
{
public:
	/** \brief The MarshFritillary_Female Constructor */
	MarshFritillary_Female( int a_x, int a_y, MarshFritillary_Population_Manager* a_PopulationManager, Landscape* a_Landscape );
	/** \brief The MarshFritillary_Female destructor */
	virtual ~MarshFritillary_Female(void);
	virtual void BeginStep(void) { ; }
	/** \brief Main step code */
	virtual void Step(void);
	virtual void EndStep(void) { ; }
protected:
	// Attributes
	/** \brief How many eggs left */
	int m_Eggs;
	/** \brief When was the last laying event */
	int m_lastlayingdate;
	// Methods
	/** \brief Determine individual egg mass size */
	int DetermineEggNumber(void);
	/** \brief Determine lifetime egg potential */
	void DetermineTotalEggs(void);
	/** \brief Egg laying behaviour */
	ButterflyBehaviour EggLaying();
	/** \brief Moving behaviour */
	ButterflyBehaviour Moving(void);
};

