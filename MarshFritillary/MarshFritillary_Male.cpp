#include <iostream>
#include <fstream>
#include "../Landscape/ls.h"
#include "../MarshFritillary/MarshFritillaryHdrs.h"

extern CfgInt cfg_MarshFrit_AdultLifespan;

MarshFritillary_Male::MarshFritillary_Male( int a_x, int a_y, MarshFritillary_Population_Manager* a_PopulationManager, Landscape* a_Landscape)
	: MarshFritillary_Adult( a_x, a_y, a_PopulationManager, a_Landscape)
{
	;
}


MarshFritillary_Male::~MarshFritillary_Male(void)
{
	;
}

void MarshFritillary_Male::Step(void)
{
	if (m_CurrentStateNo == abb_killthis || m_StepDone ) return;
	switch ((ButterflyBehaviour) m_CurrentStateNo)
	{
	  case abb_dead:
		m_CurrentStateNo = abb_killthis;
		m_StepDone = true;
		break;
	  case abb_initiation:
		  m_CurrentStateNo = abb_moving;
		  break;
	  case abb_dailyactivity:
		  m_CurrentStateNo = DailyActivity();
		  break;
	  case abb_moving:
		  m_CurrentStateNo = Moving();
		  m_StepDone = true;
		  break;
	  default:
		  m_OurLandscape->Warn("MarshFritillary_EggMass::SubStep0- unknown behavioural state",NULL);
		  exit(1);
   }
}

ButterflyBehaviour MarshFritillary_Male::Moving(void)
{
	/** Uses movement rules to determine the next position, then changes m_Location_x & m_Location_y */
	/** Current version is random movement */
	int d2 = 128;
	int d1 = d2*2+1;
	m_Location_x += (random(d1)-d2);
	m_Location_y += (random(d1)-d2);
	CorrectWrapRound();
	return abb_dailyactivity;
}

