#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "mwhivetool.h"

#include <QMessageBox>
#include <QDebug>
#include <QMouseEvent>

#include "stdio.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <string>

// #include <qwt_plot.h>
// #include <qwt_plot_barchart.h>

#include "../ALMaSSDefines.h"
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../Landscape/ls.h"

#include "../BatchALMaSS/BinaryMapBase.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Vole/GeneticMaterial.h"
#include "../Skylark/skylarks_all.h"
#include "../Partridge/Partridge_All.h"
#include "../Partridge/Partridge_Population_Manager.h"
#include "../Vole/vole_all.h"
#include "../Vole/VolePopulationManager.h"
#include "../Vole/Predators.h"
#include "../Bembidion/bembidion_all.h"
#include "../Hare/hare_all.h"
#include "../Spider/spider_all.h"
#include "../Spider/SpiderPopulationManager.h"
#include "../GooseManagement/GooseMemoryMap.h"
#include "../GooseManagement/Goose_Base.h"
#include "../BatchALMaSS/CurveClasses.h"
#include "../Hunters/Hunters_all.h"
#include "../GooseManagement/Goose_Population_Manager.h"
#include "../RodenticideModelling/RodenticidePredators.h"
#include "../RoeDeer/Roe_all.h"
#include "../RoeDeer/Roe_pop_manager.h"
#include "../Rabbit/Rabbit.h"
#include "../Rabbit/Rabbit_Population_Manager.h"
#include "../Newt/Newt.h"
#include "../Newt/Newt_Population_Manager.h"
#include "../OliveMoth/olivemoth.h"
#include "habitatmap.h"
#include "../HoneyBee/HoneyBee.h"
#include "../HoneyBee/HoneyBee_Colony.h"

extern CfgBool l_pest_enable_pesticide_engine;
extern CfgInt l_pest_NoPPPs;
extern CfgBool cfg_rodenticide_enable;
extern CfgBool cfg_fixed_random_sequence;
static CfgBool cfg_animate( "G_ANIMATE", CFG_CUSTOM, true );
static CfgBool cfg_dumpmap( "G_DUMPMAP", CFG_CUSTOM, true ); // Produces a set of bitmaps using the next three cfgs to determine start, range and interval
static CfgInt cfg_dumpmapstart( "G_DUMPMAPSTART", CFG_CUSTOM, 366  );
static CfgInt cfg_dumpmapend( "G_DUMPMAPEND", CFG_CUSTOM, 730 );
static CfgInt cfg_dumpmapstep( "G_DUMPMAPSTEP", CFG_CUSTOM, 5 );

extern PopulationManagerList g_PopulationManagerList;



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    habMap.mainWindow=this;
    g_msg = new MapErrorMsg("ErrorFile.txt");
    g_msg->SetWarnLevel(WARN_ALL);

    //hivetool=new MWHiveTool(this);

    // Configurator instantiation is automatic.
    g_cfg->ReadSymbols("TIALMaSSConfig.cfg");
    currentSpecies=0;
    //g_cfg->DumpAllSymbolsAndExit( "allsymbols.cfg" );
    ui->setupUi(this);
    //showFullScreen();
    setWindowState(Qt::WindowMaximized);
    viewGroup = new QActionGroup(this);
    viewGroup->addAction(ui->actionMap);
    viewGroup->addAction(ui->actionVeg_Type);
    viewGroup->addAction(ui->actionBiomass);
    viewGroup->addAction(ui->actionFarm_ownership);
    viewGroup->addAction(ui->actionPesticide_Load);
    viewGroup->addAction(ui->actionGoose_Numbers);
    viewGroup->addAction(ui->actionGoose_Food_Resource);
    viewGroup->addAction(ui->actionGoose_Grain_Resource);
    viewGroup->addAction(ui->actionGoose_Grazing_Resource);
    viewGroup->addAction(ui->actionSoil_Type_Rabbits);
    viewGroup->addAction(ui->actionHunters_Locations);
    viewGroup->addAction(ui->actionPollen);
    viewGroup->addAction(ui->actionNectar);

    ui->stepSizeCB->setCurrentIndex(3);
    ui->stepSizeCB->setEnabled(false);

    currentSpecies=ui->animalComboBox->currentIndex();
    // Only want the hive tool pressable if ApisRAM
    if (currentSpecies==TOP_ApisRAM) {
        ui->stepSizeCB->setCurrentIndex(2);
        ui->stepSizeCB->setEnabled(true);
    }
//    else
//        ui->actionHive_Tool->setEnabled(false);

    // This is how to set background colour of pop labels
    //ui->popLab1->setStyleSheet("background:rgb(46,204,113);");


    connect(ui->map,SIGNAL(clicked()),this,SLOT(on_map_clicked2()));
    connect(ui->map,SIGNAL(clicked(QMouseEvent*)),this,SLOT(on_map_clicked(QMouseEvent*)));


    clickGroup = new QActionGroup(this);
    clickGroup->addAction(ui->actionZoom);
    clickGroup->addAction(ui->actionInfo);
    m_Paused=false;
    drawingMap=true;
    hivetool=NULL;
//    QwtPlotBarChart* popBar1=new QwtPlotBarChart("Pop");
   // QwtPlotBarChart popBar2("Pop");
    //popBar=new QChart();
    DataInit();

    ui->map->setScaledContents(true);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete viewGroup;
    if (hivetool)
        delete hivetool;
    //delete popBar;
}

void MainWindow::setUpForm()
{
    // Give the species checkboxes proper names, taken from SupplyListName()
    QCheckBox* spboxes[14]={ui->spBox1,ui->spBox2,ui->spBox3,ui->spBox4,ui->spBox5,ui->spBox6,ui->spBox7,
                           ui->spBox8,ui->spBox9,ui->spBox10,ui->spBox11,ui->spBox12,ui->spBox13,ui->spBox14};

    QLabel* splabs[14]={ui->popLab1,ui->popLab2,ui->popLab3,ui->popLab4,ui->popLab5,ui->popLab6,ui->popLab7,
                       ui->popLab8,ui->popLab9,ui->popLab10,ui->popLab11,ui->popLab12,ui->popLab13,ui->popLab14};

    if (m_AManager->SupplyListNameLength() > 0)
    {
        for (int i=0; i < m_AManager->SupplyListNameLength();++i)
        {
            spboxes[i]->setEnabled(true);
            spboxes[i]->setChecked(true);
            spboxes[i]->setText(m_AManager->SupplyListName(i));
            splabs[i]->setText(m_AManager->SupplyListName(i));
        }
    }
}

void MainWindow::DataInit()
{
  // Application flags and data
  m_ProbesSet = false;
  m_NoProbes = 0;
  m_time = 0;
  m_Year = 0;
  m_Steps = 0;
  m_SimInitiated = false;
  // End application data & flags
}

int MainWindow::getSubValue() {
    return ui->subSpinBox->value();
}

int MainWindow::getCurrentSpecies() {
    return currentSpecies;
}

Population_Manager* MainWindow::getPopManager() {
    return m_AManager;
}

// To get these functions, right click in action editor in
// embedded designer (click mainwindow.ui file in projects browser)
// Then pick go to slot. Creator will add a function stub.
void MainWindow::on_actionStart_triggered()
{
    m_BatchRun = false;
    ui->animalComboBox->setEnabled(false);
    /*
    ui->stepSizeCB->setEnabled(false);
    ui->stepSpinBox->setEnabled(false);
    ui->itComboBox->setEnabled(false);
    */
    if (currentSpecies==TOP_ApisRAM)
          ui->actionHive_Tool->setEnabled(true);
    Go();
}

void MainWindow::Go()
{
  std::cout << "start triggered\n";
  // Go figures out whether the simulation is already running, and if not
  // starts a new one, otherwise it runs for however long the form says
  if ( !m_SimInitiated )
  {
    // Randomize the random generator.
    if (cfg_fixed_random_sequence.value())
    {
      srand(0);
    }
    else
    {
      srand( ( int )time( NULL ) );
    }
    m_torun = GetTimeToRun();
    if ( m_torun > 0 )
    {
      if ( !ReadBatchINI() )
      {
	return;
      }
      ui->statusBar->showMessage("Creating the landscape. This may take a while...");
      QCoreApplication::processEvents();

      CreateLandscape();
      ui->statusBar->showMessage("Creating the Population Manager.");
      QCoreApplication::processEvents();

      if (!CreatePopulationManager())
      {
	return; // Let out if for some reason the population manager was not created
      }
      // dww. Try and get rid of this
      //                m_AManager->m_MainForm = this;
      m_ALandscape->SetThePopManager(m_AManager);
      
      setUpForm();
      // Now got to get the probe files read in
      GetProbeInput_ini();
      // Ready to go
      m_time = 0;
      m_SimInitiated = true;
      m_StopSignal = false;
      //habMap.DrawLandscape();
    }
  }
  else
  {
    m_torun = GetTimeToRun();
  }
  if ( m_SimInitiated )
  {
    RunTheSim();
  }
}

bool MainWindow::ReadBatchINI() {
  // Must read the TIBatch.INI
  // Read the INI file
  ifstream Fi;
  //int answer = wxYES;
  //wxString filestr = m_IniFileEdit->GetLineText( 0 );
  //Fi.open(filestr.char_str());
  Fi.open("TI_inifile.ini");
  while ( !Fi.is_open() )
  {
      g_msg->Warn( "TI_inifile.ini file not found","" );
      // Issue and error warning
      //wxString amsg = _T("INI File Missing: TI_inifile.ini");
      //amsg += filestr;
      //answer = wxMessageBox( amsg, _T("Try Again?"), wxYES_NO | wxCANCEL, this );
      QMessageBox::StandardButton reply;
      reply = QMessageBox::question(this, "Try Again", "Quit?",
                                      QMessageBox::Yes|QMessageBox::No);
    if ( reply != QMessageBox::Yes ) return false;
//	Fi.open(filestr.char_str());
    Fi.open("TI_inifile.ini");
  }
  char Data[ 255 ];
  Fi >> m_NoProbes;
  Fi.getline(Data, 255);
  for ( int i = 0; i < m_NoProbes; i++ ) {
    m_files[ i ] = new char[ 255 ];
    Fi.getline(m_files[ i ], 255);
  }
  Fi.getline(m_ResultsDir, 255);
  // Read the PredBatch.INI file
  ifstream Fi2;

  QMessageBox::StandardButton reply;
  while ( !Fi2.is_open() )
  {
    Fi2.open( "VoleToxPreds.ini");
    if ( !Fi2.is_open() ) {
      // Issue and error warning
      QString amsg = ("Predator Batch File Missing: ");
      // dww need to add the following line in qt too.
//      amsg += wxString::Format( _T("%s"), _T("VoleToxPreds.ini") );
      //answer = wxMessageBox( amsg, _T("Try Again?"), wxYES_NO | wxCANCEL, this );
      reply = QMessageBox::question(this, "Try Again", "Quit?",
                                      QMessageBox::Yes|QMessageBox::No);
    //}
    if (reply != QMessageBox::Yes ) return false;
  }
  }
  Fi2 >> m_NoPredProbes;
  Fi2.getline(Data, 255);
  for ( int i = 0; i < m_NoPredProbes; i++ ) {
    m_Predfiles[ i ] = new char[ 255 ];
    Fi2.getline(m_Predfiles[ i ], 255);
  }
  Fi2.getline(m_PredResultsDir, 255 );
  Fi2.close();
  if ( m_BatchRun ) {
      Fi >> m_torun;
      m_torun *= 365; // the number of years and multiplied by 365 to get days
  }
  Fi.close();
  return true;
}

void MainWindow::GetProbeInput_ini() {
  char Nme[ 511 ];
  ofstream* AFile = NULL;
  for ( int NProbes = 0; NProbes < m_NoProbes; NProbes++ ) {
    // Must read the probe from a file
    m_AManager->TheProbe[ NProbes ] = new probe_data;
    m_AManager->ProbeFileInput( ( char * ) m_files[ NProbes ], NProbes );
    char NoProbesString[ 32 ];
    sprintf( NoProbesString, "%sProbe%d.res", m_ResultsDir, NProbes );
    if ( NProbes == 0 ) {
      AFile = m_AManager->TheProbe[ NProbes ]->OpenFile( NoProbesString );
      if ( !AFile->is_open() ) {
        m_ALandscape->Warn( "BatchALMSS - cannot open Probe File", NULL );
        exit( 1 );
      }
    } else m_AManager->TheProbe[ NProbes ]->SetFile( AFile );
  }

  if ( currentSpecies == 1 ) {
    for ( int NProbes = 0; NProbes < m_NoPredProbes; NProbes++ ) {
      m_PredatorManager->TheProbe[ NProbes ] = new probe_data;
      m_PredatorManager->ProbeFileInput( ( char * ) m_Predfiles[ NProbes ], NProbes );
      sprintf( Nme, "%sPredProbe%d.res", m_ResultsDir, NProbes + 1 );
      //      strcpy( Nme, m_PredResultsDir );
      if ( NProbes == 0 ) {
        AFile = m_PredatorManager->TheProbe[ NProbes ]->OpenFile( Nme );
        if ( !AFile ) {
          m_ALandscape->Warn( "BatchALMSS - cannot open Probe File", Nme );
          exit( 1 );
        }
      } else m_PredatorManager->TheProbe[ NProbes ]->SetFile( AFile );
    }
  }
}


void MainWindow::setUIMapSize()
{
    ui->map->setFixedHeight(__MAPSIZE);
    ui->map->setFixedWidth(__MAPSIZE);
}

void MainWindow::CreateLandscape()
{
  // Create the landscape
  m_ALandscape = new Landscape();
  // dww need to set these in a qt way

  habMap.SetSimExtent( m_ALandscape->SupplySimAreaWidth(), m_ALandscape->SupplySimAreaHeight() );
  habMap.SetLandscape( m_ALandscape );
  habMap.SetUpMapImage();
  setUIMapSize();
}


bool MainWindow::CreatePopulationManager() {
  // SET UP THE ANIMAL POPULATION
  // THE LANDSCAPE MUST BE SETUP BEFORE THE CALL HERE
  if ( !m_ALandscape ) {
      QMessageBox::StandardButton reply;
      reply = QMessageBox::question(this, "Landscape not created", "Quit?",
                                      QMessageBox::Yes|QMessageBox::No);
    //wxMessageBox( _T("ALMaSSGUI::CreatePopulationManager: Landscape Not Created"), _T("ERROR"), wxOK, this );
    return false;
  }


  if ( currentSpecies == TOP_Skylark) {
    Skylark_Population_Manager * skMan = new Skylark_Population_Manager( m_ALandscape );
    m_AManager = skMan;
    m_AManager->OpenTheBreedingPairsProbe();
    m_AManager->OpenTheBreedingSuccessProbe();
    m_AManager->OpenTheFledgelingProbe();
    g_PopulationManagerList.SetPopulation(m_AManager, TOP_Skylark);
  }

  if ( currentSpecies == TOP_Vole) {
    Vole_Population_Manager * vMan = new Vole_Population_Manager( m_ALandscape );
    m_AManager = vMan;
    g_PopulationManagerList.SetPopulation(m_AManager, TOP_Vole);
    m_PredatorManager = new TPredator_Population_Manager( m_ALandscape, vMan );
    m_PredatorManager->SetNoProbes(m_NoPredProbes);
    g_PopulationManagerList.SetPopulation(m_PredatorManager, TOP_Predators);

  }

  if ( currentSpecies == TOP_Spider) {
    Spider_Population_Manager * spMan = new Spider_Population_Manager( m_ALandscape );
    m_AManager = spMan;
    g_PopulationManagerList.SetPopulation(m_AManager, TOP_Spider);
  }

  if ( currentSpecies == TOP_Beetle) {
    Bembidion_Population_Manager * bMan = new Bembidion_Population_Manager( m_ALandscape );
    m_AManager = bMan;
    g_PopulationManagerList.SetPopulation(m_AManager, TOP_Beetle);
  }

  if ( currentSpecies == TOP_Hare) { // Hare
    THare_Population_Manager * hMan = new THare_Population_Manager( m_ALandscape );
    m_AManager = hMan;
    g_PopulationManagerList.SetPopulation(m_AManager, TOP_Hare);
  }

  if ( currentSpecies == 5 ) {
    Partridge_Population_Manager * pMan = new Partridge_Population_Manager( m_ALandscape );
    m_AManager = pMan;
    g_PopulationManagerList.SetPopulation(m_AManager, TOP_Partridge);
  }

  if ( currentSpecies == TOP_Goose) {
    Goose_Population_Manager * gMan = new Goose_Population_Manager( m_ALandscape );
    m_AManager = gMan;
    g_PopulationManagerList.SetPopulation(m_AManager, TOP_Goose);
    m_Hunter_Population_Manager = new Hunter_Population_Manager( m_ALandscape );
    g_PopulationManagerList.SetPopulation(m_Hunter_Population_Manager, TOP_Hunters);
  }

  if (currentSpecies == TOP_RoeDeer)
  {
    RoeDeer_Population_Manager * rMan = new RoeDeer_Population_Manager( m_ALandscape );
    m_AManager = rMan;
    g_PopulationManagerList.SetPopulation( m_AManager, TOP_RoeDeer );
  }

  if (currentSpecies == TOP_Rabbit)
  {
    Rabbit_Population_Manager * rbMan = new Rabbit_Population_Manager( m_ALandscape );
    m_AManager = rbMan;
    g_PopulationManagerList.SetPopulation( m_AManager, TOP_Rabbit );
  }

  if (currentSpecies == TOP_Newt)
  {
      Newt_Population_Manager * nMan = new Newt_Population_Manager(m_ALandscape);
      m_AManager = nMan;
      g_PopulationManagerList.SetPopulation(m_AManager, TOP_Newt);
  }

  if (currentSpecies == TOP_Osmia)
  {
    Newt_Population_Manager * oMan = new Newt_Population_Manager(m_ALandscape);
    m_AManager = oMan;
    g_PopulationManagerList.SetPopulation(m_AManager, TOP_Osmia);
  }

  if (currentSpecies == TOP_ApisRAM)
  {
      HoneyBee_Colony *hMan = new HoneyBee_Colony(m_ALandscape);
      m_AManager = hMan;
      g_PopulationManagerList.SetPopulation(m_AManager, TOP_ApisRAM);
  }

  if (currentSpecies == TOP_OliveMoth)
  {
    cout << "Making Olive Moth<n";
    OliveMoth_Population_Manager * omMan = new OliveMoth_Population_Manager(m_ALandscape);
    m_AManager = omMan;
    g_PopulationManagerList.SetPopulation(m_AManager, TOP_OliveMoth);
  }


#ifdef __marshfritillary
  if (currentSpecies == TOP_MarshFritillary) {
    MarshFritillary_Population_Manager * mMan = new  MarshFritillary_Population_Manager(m_ALandscape);
    m_AManager = mMan;
    g_PopulationManagerList.SetPopulation(m_AManager, TOP_MarshFritillary);
  }
#endif
#ifdef __dormouse
  if (currentSpecies == TOP_Dormouse
  {
    Dormouse_Population_Manager * dMan = new Dormouse_Population_Manager(m_ALandscape);
    m_AManager = dMan;
    g_PopulationManagerList.SetPopulation(m_AManager, TOP_Dormouse);
  }
#endif

  m_AManager->SetNoProbes(m_NoProbes);
  return true;
}




/*
   cout << "Making Olive Moth<n";
   OliveMoth_Population_Manager * omMan = new OliveMoth_Population_Manager(m_ALandscape);
   m_AManager = omMan;
   g_PopulationManagerList.SetPopulation(m_AManager, TOP_OliveMoth);


  m_AManager->SetNoProbes(m_NoProbes);
  return true;
}
*/

//------------------------------------------------------------------------------

int MainWindow::GetTimeToRun() {
  // Get the time to run in days
  const int daysinmonths[ 13 ] = {0,31,28,31,30,31,30,31,31,30,31,30,31};
  int torun = 0;
  int month;
  // First the number of whatever
//  int anum = item7->GetValue();
  int anum = ui->stepSpinBox->value();
  // Now figure out whatever
//  switch ( item8->GetSelection() ) {
  switch (ui->itComboBox->currentIndex()) {
    case 0: // Year
      torun = anum * 365;
    break;
    case 1: // Month
      if ( m_SimInitiated ) month = m_ALandscape->SupplyMonth(); else
        month = 1; // if we have not started it must be January
      for ( int i = 0; i < anum; i++ ) {
        torun += daysinmonths[ month ];
        if ( ++month == 13 ) month = 1;
      }
    break;
    case 2: // Day
      torun = anum;
    break;
    case 3: // TimeStep
      torun = anum;
    break;
    case - 1:
      // No selection
    break;
  }
  return torun;
}

void MainWindow::CloseDownSim()
{
  if ( m_SimInitiated )
  {
    m_ALandscape->SimulationClosingActions();
    if ( m_AManager ) {
      // Close the probe file
      if ( m_AManager->TheProbe[ 0 ] != NULL ) m_AManager->TheProbe[ 0 ]->CloseFile();
      // delete all probes
      for ( int i = 0; i < m_NoProbes; i++ ) delete m_AManager->TheProbe[ i ];
      delete m_AManager;
      if ( currentSpecies == 1 ) {
        delete m_PredatorManager;
      }

    }
    delete m_ALandscape;
    m_time = 0;
    //item9->Enable( true );
  }
  m_Year = 0;
  m_SimInitiated = false; // so we can run another if we want to later
  //m_StartButton->Enable( true );
  //m_BatchButton->Enable( true );
}


void MainWindow::RunTheSim() {
    ui->statusBar->showMessage("Running the simulation.");
    QCoreApplication::processEvents();
  //cout << "Running Sim. Species number:  " << currentSpecies << std::endl;
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(m_torun);
 // m_bmpout = (_T("ALMaSSbmpOut_0000.bmp"));
    for ( int i = 0; i < m_torun;i++) {
        if (m_Paused) {
           QCoreApplication::processEvents();
          i--;
        }
        else
        {            
            QCoreApplication::processEvents();
            ui->statusBar->showMessage("Turning the world.");
            QCoreApplication::processEvents();
            m_ALandscape->TurnTheWorld();
            QCoreApplication::processEvents();

            m_time++;
            int day = m_ALandscape->SupplyDayInMonth();
            int month = m_ALandscape->SupplyMonth();
            if ( ( day == 1 ) && ( month == 1 ) ) m_Year++;

            QDate date = QDate(m_Year+2018, month,day);
            ui->dateTimeEdit->setDate(date);

            int stepsize = 1;
            if (ui->stepSizeCB->isEnabled())
            {
                if (ui->stepSizeCB->currentIndex()==1)
                    stepsize = 24;

                if (ui->stepSizeCB->currentIndex()==2)
                    stepsize = 144;
            }

            if (currentSpecies==1)
            {
                ui->statusBar->showMessage("Running the predator manager.");
                QCoreApplication::processEvents();

                m_PredatorManager->Run(1);
            }
            ui->statusBar->showMessage("Running the main model.");
            QCoreApplication::processEvents();

            if (currentSpecies == 6)
            {
                for (int tenmin=0; tenmin<144; tenmin++)
                {
                    m_time++;
                    QCoreApplication::processEvents();
                    m_AManager->Run( 1 ); // Goose Model
                    QCoreApplication::processEvents();
                    m_Hunter_Population_Manager->Run(1);
                    QCoreApplication::processEvents();
                    ui->progressBar->setValue(i+1);
                    ui->stepLCD->display((i+tenmin)+1);
                }
            }
            else
            {
                for (int times=0; times < stepsize; ++ times)
                {
                    QCoreApplication::processEvents();
                    m_AManager->Run(1);

                    if (drawingMap)
                    {
                        habMap.DrawLandscape();
                        ShowAnimals();

                        QImage img(habMap.imageData(), habMap.mapSize(),habMap.mapSize(),QImage::Format_RGB888);
                        ui->map->setPixmap(QPixmap::fromImage(img));
                    }

                    if (hivetool)
                    {
                        hivetool->doDraw();
                    }

                    QLCDNumber* popNums[14]={ui->popNum1,ui->popNum2,ui->popNum3,ui->popNum4,ui->popNum5,ui->popNum6,ui->popNum7,
                        ui->popNum8,ui->popNum9,ui->popNum10,ui->popNum11,ui->popNum12,ui->popNum13,ui->popNum14};

                    for (int ac=0; ac < 12; ++ac)
                    {
                        auto ssr = m_AManager->SpeciesSpecificReporting(currentSpecies, m_time);
                        int nAnimals = m_AManager->GetLiveArraySize(ac);
                        popNums[ac]->display(nAnimals);

                    }
                    //m_time++;
                    ui->progressBar->setValue(i+1);
                    //ui->stepLCD->display((i+times)+1);
                    ui->stepLCD->display(m_time);
                }
            }

    // NEED THESE TWO !!!!!!!!!!!!!!!!
//    if (cfg_animate.value()) habMap.DrawLandscape();
 //   ShowAnimals();
    // If we want to dump the bmp map to a file then we need to do it here.


    //if (cfg_dumpmap.value()) DumpMap(i);

  //  while ( wxTheApp->Pending() ) wxTheApp->Dispatch();

    /*habMap.ShowBitmap2();*/
            if ( currentSpecies == 1 ) m_PredatorManager->ProbeReport( m_time );

    /*
    wxString astr(m_AManager->SpeciesSpecificReporting(currentSpecies, m_time), wxConvUTF8);
    if (astr.length()>0)
    m_ProbesBox->InsertItems( 1, &astr, 0 );
            */

            m_ALandscape->DumpVegAreaData( m_time );
  //  Update();
            if ( m_StopSignal )
            {
                m_torun = -1;
                CloseDownSim();
                ui->map->clear();
                ui->progressBar->setValue(0);
            }

        }
    }
  }


void MainWindow::ShowAnimals() {
  int x, y;
  if (currentSpecies == 1)
  {
    /*
    // Special code for genetic demo
    */
    if (ui->spBox6->isChecked() )
    {
        Vole_Population_Manager * VPM;
        VPM = dynamic_cast<Vole_Population_Manager*>(m_AManager);
        for ( int lstages=0; lstages<4; lstages++)
        {
            unsigned noInList = m_AManager->GetLiveArraySize(lstages);
            for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ )
            {
                VPM->SupplyLocXY( lstages, anAnimalRef, x, y );
                // Get the genetic colour
                unsigned gcolour = 3;
                unsigned gcolour1 = VPM->GetVoleAllele(lstages, anAnimalRef, 3, 0);
                unsigned gcolour2 = VPM->GetVoleAllele(lstages, anAnimalRef, 3, 1);
                if ((gcolour1 == 1) && (gcolour2 ==1))  gcolour=4; else if ((gcolour1 == 1) || (gcolour2 ==1)) gcolour = 2;
                habMap.Spot( gcolour, x, y );
            }
        }
        return;
    }
  }
  else
  {
      if (ui->spBox6->isChecked())
      {
          unsigned noInList = m_AManager->GetLiveArraySize(5);
        for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ )
        {
          m_AManager->SupplyLocXY( 5, anAnimalRef, x, y );
          habMap.Spot( 5, x, y );
        }
      }
      //return;
  }

    if (currentSpecies==TOP_ApisRAM)
    {
        int i = 4;
        if (isAnimalChecked(i))
        {
            unsigned noInList = m_AManager->GetLiveArraySize(i);
            for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ )
            {
                m_AManager->SupplyLocXY( i, anAnimalRef, x, y );
                HoneyBee_Colony * pm = dynamic_cast<HoneyBee_Colony*>(m_AManager);
                if (pm->isInHive<HoneyBee_Worker>(i,anAnimalRef))
                {
                    habMap.Spot( i, x, y );
                }
            }
        }
    return;
    }

    for (int i = 0; i < 12; ++i)
    {
        if (isAnimalChecked(i))
        {
            unsigned noInList = m_AManager->GetLiveArraySize(i);
            for ( unsigned anAnimalRef = 0; anAnimalRef < noInList; anAnimalRef++ )
            {
                m_AManager->SupplyLocXY( i, anAnimalRef, x, y );
                habMap.Spot( i, x, y );
            }
        }
    }
}

bool MainWindow::isAnimalChecked(const unsigned i) {
    QCheckBox* spboxes[14]={ui->spBox1,ui->spBox2,ui->spBox3,ui->spBox4,ui->spBox5,ui->spBox6,ui->spBox7,
                                 ui->spBox8,ui->spBox9,ui->spBox10,ui->spBox11,ui->spBox12,ui->spBox13,ui->spBox14};
    return spboxes[i]->isChecked();
}

// ////////////////////////// ACTIONS //////////////////////////

void MainWindow::on_actionPause_triggered()
{
//    std::cout << "pause triggered\n";
}

void MainWindow::on_actionContinue_triggered()
{
    std::cout << "Continue triggered\n";
}

void MainWindow::on_actionStop_triggered()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Stop The Simulation", "Really stop? You won't be able to restart.",
                                          QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        std::cout << "Stop triggered\n";
        m_StopSignal=true;
        ui->animalComboBox->setEnabled(true);
        ui->stepSizeCB->setEnabled(true);
        ui->stepSpinBox->setEnabled(true);
        ui->itComboBox->setEnabled(true);
    }
}

void MainWindow::on_stepSpinBox_valueChanged(int arg1)
{
 m_torun=arg1*144;
}

void MainWindow::on_stepSpinBox_valueChanged(const QString &arg1)
{

}

void MainWindow::on_actionMap_toggled(bool arg1)
{
    if (arg1) {
        habMap.currentView=0;
        habMap.DrawLandscape();
        ShowAnimals();
    }
}

void MainWindow::on_actionVeg_Type_toggled(bool arg1)
{
    if (arg1) {
        habMap.currentView=1;
        habMap.DrawLandscape();
        ShowAnimals();
    }
}

void MainWindow::on_actionBiomass_toggled(bool arg1)
{
    habMap.currentView=2;
}

void MainWindow::on_actionFarm_ownership_toggled(bool arg1)
{
    habMap.currentView=3;
}

void MainWindow::on_actionPesticide_Load_toggled(bool arg1)
{
    habMap.currentView=4;
}

void MainWindow::on_actionGoose_Numbers_toggled(bool arg1)
{
    habMap.currentView=5;
}

void MainWindow::on_actionGoose_Food_Resource_toggled(bool arg1)
{
    habMap.currentView=6;
}

void MainWindow::on_actionGoose_Grain_Resource_toggled(bool arg1)
{
    habMap.currentView=7;
}

void MainWindow::on_actionGoose_Grazing_Resource_toggled(bool arg1)
{
    habMap.currentView=8;
}

void MainWindow::on_actionSoil_Type_Rabbits_toggled(bool arg1)
{
    habMap.currentView=9;
}

void MainWindow::on_actionHunters_Locations_toggled(bool arg1)
{
    habMap.currentView=10;
}

void MainWindow::on_actionPollen_toggled(bool arg1)
{
    habMap.currentView=11;
}

void MainWindow::on_actionNectar_toggled(bool arg1)
{
    habMap.currentView=13;
}

void MainWindow::on_animalComboBox_currentIndexChanged(int index)
{
   currentSpecies=index;
   if (currentSpecies == TOP_ApisRAM)
   {
    ui->stepSizeCB->setCurrentIndex(2);
    ui->stepSizeCB->setEnabled(true);
   }
   else
   {
    ui->stepSizeCB->setCurrentIndex(4);
    ui->stepSizeCB->setEnabled(false);
   }
}

void MainWindow::on_actionMap_triggered()
{

}

void MainWindow::on_map_clicked(QMouseEvent * e)
{
  if (!GetSimInitiated()) return;
  QPoint p = e->pos();
  //std::cout << "Zoom: " << p.x() << " " << p.y() << std::endl;
  if (ui->actionZoom->isChecked()) {  
    if (e->buttons()==Qt::LeftButton) {
        habMap.ZoomIn(p.x(),p.y());
    }
    else {
        habMap.ZoomOut(p.x(),p.y());
    }
  }
  if (ui->actionInfo->isChecked()) {
    if (e->buttons()==Qt::LeftButton) {
        DisplayMapInfo(p.x(),p.y());
    }
    else {
        FindAnimal(p.x(),p.y());
    }
  }
}

void MainWindow::DisplayMapInfo(const unsigned x, const unsigned y) {
    ui->textEdit->clear();
    ui->textEdit->moveCursor(QTextCursor::End);
    ui->textEdit->insertPlainText(habMap.getMapText(x,y));
    ui->textEdit->moveCursor(QTextCursor::End);
}

void MainWindow::FindAnimal(const unsigned x, const unsigned y) {
   ui->textEdit->clear();
   ui->textEdit->moveCursor(QTextCursor::End);
   ui->textEdit->insertPlainText(habMap.getAnimalText(x,y));
   ui->textEdit->moveCursor(QTextCursor::End);
}

void MainWindow::on_map_clicked2()
{
}


void MainWindow::on_actionHive_Tool_triggered()
{
    hivetool=new MWHiveTool(this);
    hivetool->show();
}

void MainWindow::on_actionPause_toggled(bool arg1)
{
    m_Paused=arg1;
}

void MainWindow::on_actionMap_Update_toggled(bool arg1)
{
    drawingMap=arg1;
}

void MainWindow::on_actionExit_triggered()
{
   QMessageBox::StandardButton reply;
   reply = QMessageBox::question(this, "Exit QtALMaSS", "Are you sure you want to quit?",
                                                QMessageBox::Yes|QMessageBox::No);
          if (reply == QMessageBox::Yes) {
          exit(0);
          }
}

void MainWindow::on_actionExit_triggered(bool checked)
{

}
