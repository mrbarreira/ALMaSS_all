//
// weather.cpp
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#define _CRT_SECURE_NO_DEPRECATE

#include <cstdlib>
#include <cstdio>

#include "ls.h"

extern void FloatToDouble(double&,float);

// First year of weather data used from datafile. Ignored if 0.
static CfgInt l_weather_starting_year("WEATHER_STARTING_YEAR",
				      CFG_CUSTOM, 0);

class Weather     *g_weather;

const int WindDirections[12][100] = {
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
};

void Weather::Tick( void ) {
	/** The complicated calculation of weatherday is due to the potential to run off either end of the rain data, especially since this is called on Day0 */
	int weatherday = (g_date->Date() + m_datemodulus) % m_datemodulus;
	double snowtempthreshold = -1.0;
	if (m_snowdepth > 0.0 && m_temp[ weatherday ] < snowtempthreshold) {
		m_snowdepth -= 0.1;  // We decay the snow depth by 0.1 cm per day when temperatures are sub zero
	}
	else if (m_snowdepth > 0.0) // If temperatures are above 0.0, we decay snow 1 cm per degree C
	{
		m_snowdepth -= m_temp[ weatherday ];
		if (m_snowdepth < 0.0) m_snowdepth = 0.0;
	}

	if (m_temp[ weatherday ] < snowtempthreshold && m_rain[ weatherday ] > 0.0) {
		m_snowdepth += m_rain[ weatherday ];  // rain is in mm and snow in cm, so we get the conversion for free.
	}
	if (m_temp[ weatherday ] > snowtempthreshold && m_rain[ weatherday ] > 0.0) {
		m_snowdepth -= m_rain[ weatherday ];  // rain is in mm and snow in cm, so we get the conversion for free.
		if (m_snowdepth < 0.0) m_snowdepth = 0.0;
	}
	if (m_snowdepth > 0.0) {
		m_snowtoday = true;
	}
	else {
		m_snowtoday = false;
	}

	m_insolation = c_insolation[ g_date->DayInYear() ];
	m_temptoday = m_temp[ weatherday ];
	m_raintoday = m_rain[ weatherday ];
	m_windtoday = m_wind[ weatherday ];

	if (m_raintoday > 0.001)
		m_rainingtoday = true;
	else
		m_rainingtoday = false;
	/**
	* Humidity calculation. This is based on an assumption that humidity is relative to rainfall in the days before.
	* The humidity score is the mean of rainfall over the last 5 days assuming 50% loss each day. Therefore rainfall of 10,0,0,2,0 mm in the previous 5 days would
	* give a humidity of 0.625+0+0+1+0 = 1.625/5 = 0.325
	* But we also need to calculate temperature into this. Lets assume <10 degrees has no effect, 10-20 and this figure is halved, 20-40 and it is half again.
	*/
	double rainscale = 0;
	for (int i = 0; i < 5; i++) {
		/** datemodulus is added to prevent negative overun on the rain or temperature data array, important since this is called on Day0 */
		rainscale += GetRain( (weatherday + m_datemodulus) - (i + 1) ) * pow( 0.5, i );
	}
	double temp = GetTempPeriod( weatherday + m_datemodulus - 1, 5 ) / 5.0;
	if (temp > 10.0) rainscale *= 0.5;
	if (temp > 20.0) rainscale *= 0.5;
	if (temp > 30.0) rainscale *= 0.5;
	m_humiditytoday = rainscale;
	/** Wind directions is based on probabilities. Can also use the #WindDirections array which allows montly probabilities */
	double chance = g_rand_uni();
	if (chance < 0.1) m_winddirtoday = 2; // South
	else if (chance < 0.3) m_winddirtoday = 1; // East
	else if (chance < 0.5) m_winddirtoday = 0; // North
	else m_winddirtoday = 3; // West
}



double Weather::GetMeanTemp( long a_date, unsigned int a_period )
{
  double sum = 0.0;

  for ( unsigned int i=0; i<a_period; i++ )
    sum += GetTemp( a_date - i );

  return sum/(double)a_period;
}



bool  Weather::GetSnow( long a_date )
{
  if ( a_date == g_date->Date() ) {
    return m_snowtoday;
  }

  int weatherday = a_date % m_datemodulus;
  bool snow = false;

  if ( m_temp[ weatherday ] < 0 &&
       m_rain[ weatherday ] > 1 &&
       random(100) < 50 ) {
    snow = true;
  }

  //  if ( ((dayinyear<90) || (dayinyear>330)) && (rand()%4==1) )
  //  snow = true;

  return snow;
}



double Weather::GetDDDegs( long a_date )
{
  double temp = m_temp[ a_date%m_datemodulus ];
  if ( temp < 0.0 ) {
    temp = 0.0;
  }

  return temp;
}



Weather::Weather( const char* a_weatherdatafile )
{
  int NoDays, Day, Month, Year, FirstYear=0, LastYear=0;
  double Temp, Rain, Wind;

  ifstream inFile(a_weatherdatafile);
  if (!inFile) {
    g_msg->Warn(WARN_FILE, "Weather::Weather(): Unable to open file",
		a_weatherdatafile );
    exit(1);
  }

  string buf;
  std::getline(inFile,buf);
  NoDays=std::stoi(buf);


  m_rain.resize( NoDays );
  m_wind.resize( NoDays );
  m_temp.resize( NoDays );

  bool storing = false; // Whether we are storing weather data.
  unsigned int index = 0;

  g_date->SetLastYear( 0 );


  for ( int i=0; i<NoDays; i++) {
      inFile >> Year >> Month >> Day >> Temp >> Wind >> Rain;

    if ( Month == 2 && Day == 29 ) {
      // Skip leap days.
      continue;
    }

    if ( Month == 1 && Day == 1 && !storing &&
	 (
	  l_weather_starting_year.value() == 0 ||
	  l_weather_starting_year.value() == Year
	  )
	 ) {
      // Commence storing of data from Jan. 1st of the first
      // year requested.
      storing = true;
      g_date->SetFirstYear( Year );
      FirstYear = Year;
      g_date->Reset();
    }


    if ( storing ) {
			//if (Temp > 0.0 )
      //	DDegs += Temp;
      m_rain[ index ] = Rain;
      m_wind[ index ] = Wind;
      m_temp[ index ] = Temp;
      index++;
    }

    //cout << "i: " << i << " index: " << index << " Month: " << Month << " Year: " << Year <<"\n";

    if ( (Month == 12) && (Day == 31) && (storing) ) {
        cout << "LastYear: " << Year << "\n";
      // Found yet another full year worth of weather data.
      g_date->SetLastYear( Year );
      LastYear = Year;
    }
  }

  // Did we find at least one full year worth of data?
  if ( g_date->GetLastYear() == 0 ) {
    // Nope...
    g_msg->Warn(WARN_FILE, "Weather::Weather(): Weather data file did",
		"not contain at least one year of data!" );
    exit(1);
  }
  //fclose( ifile );
  m_datemodulus = (LastYear - FirstYear + 1)*365;
  m_snowdepth = 0;
  Tick();
}

Weather::~Weather( void )
{
}

double Weather::GetRainPeriod( long a_date, unsigned int a_period )
{
  double sum = 0.0;

  for ( unsigned int i=0; i<a_period; i++ )
    sum += GetRain( a_date - i );
  return sum;
}

double Weather::GetWindPeriod( long a_date, unsigned int a_period )
{
  double sum = 0.0;

  for ( unsigned int i=0; i<a_period; i++ )
    sum += GetWind( a_date - i );

  return sum;
}

double Weather::GetTempPeriod( long a_date, unsigned int a_period )
{
	/**
	* Sums the temperature for the period from a_date back a_period days. 
	* \param [in] a_date the day to start summing degrees
	* \param [in] a_period the number of days períod to sum
	* \return The sum of day degrees
	*/
  double sum = 0.0;

  for ( unsigned int i=0; i<a_period; i++ )
    sum += GetTemp( a_date - i );

  return sum;
}
