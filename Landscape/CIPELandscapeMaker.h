//
// CIPELandscapeMaker.h
//
/*

Copyright (c) 2006, National Environmental Research Institute, Denmark (NERI)

All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer. *) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution. *) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#ifndef CIPEMAKER_H
#define CIPEMAKER_H

struct EGP_Data {
	int m_polynum;
	int m_orig_size;
	int m_cx;
	int m_cy;
	//....continue as needed
};

struct Edges {
	int m_x;
	int m_y;
	bool m_cangrow;
};

class EdgeGrowingPoints {
	int m_polynum;
	int m_originalsize;
	int m_centre_x;
	int m_centre_y;
	vector<Edges> m_OurEdges;

public:
	EdgeGrowingPoints(EGP_Data egpd);
	bool GetCanGrow(int x, int y);
	void SetCanGrow(int x, int y, bool cg);
	unsigned int ShrinkList();
};

class PolygonDataVector {
	vector<EdgeGrowingPoints*> m_polylist;
	int m_polyrefs[25000]; // if we run into memory problems then we can always do this better
public:
	PolygonDataVector();
	~PolygonDataVector();
	// Interface functions
	inline bool HaveSeen(int a_poly);
	void AddPolygon(EGP_Data egpdata);
	// You access stuff in the polylist list like this:
	bool CanGrow(int polyref, int x, int y) { return m_polylist[m_polyrefs[polyref]]->GetCanGrow(x,y); }
protected:
	// Nothing here yet
};

#endif // CIPEMAKER
