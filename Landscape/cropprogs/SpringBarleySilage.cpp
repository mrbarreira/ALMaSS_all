//
// SpringBarleySilage.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/SpringBarleySilage.h"


extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgBool cfg_pest_SBS_ERA;
extern CfgBool cfg_pest_springbarley_on;
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgInt 	cfg_SB_InsecticideDay;
extern CfgInt   cfg_SB_InsecticideMonth;
extern CfgInt 	cfg_SB_InsecticideDay;
extern CfgInt   cfg_SB_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


bool SpringBarleySilage::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  int d1;

  bool done = false;

  switch ( m_ev->m_todo )
  {
  case sbs_start:
    {
      // Set up the date management stuff
      // Could save the start day in case it is needed later
      // m_field->m_startday = m_ev->m_startday;
      m_last_date=g_date->DayInYear(25,7);
      // Start and stop dates for all events after harvest
      int noDates=1;
      m_field->SetMDates(0,0,g_date->DayInYear(1,7));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(25,7));
    // Check the next crop for early start, unless it is a spring crop
    // in which case we ASSUME that no checking is necessary!!!!
    // So DO NOT implement a crop that runs over the year boundary

	//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
	 if(!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){

    if (m_ev->m_startday>g_date->DayInYear(1,7))
    {
      if (m_field->GetMDates(0,0) >=m_ev->m_startday)
      {
        g_msg->Warn( WARN_BUG, "SpringBarleySilage::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
        exit( 1 );
      }
      // Now fix any late finishing problems
      for (int i=0; i<noDates; i++) {
			if(m_field->GetMDates(0,i)>=m_ev->m_startday) { 
				m_field->SetMDates(0,i,m_ev->m_startday-1); //move the starting date
			}
			if(m_field->GetMDates(1,i)>=m_ev->m_startday){
				m_field->SetMConstants(i,0); 
				m_field->SetMDates(1,i,m_ev->m_startday-1); //move the finishing date
			}
		}
    }
      // Now no operations can be timed after the start of the next crop.

      if ( ! m_ev->m_first_year )
      {
	// Are we before July 1st?
	d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
	if (g_date->Date() < d1)
        {
	  // Yes, too early. We assumme this is because the last crop was late
          g_msg->Warn( WARN_BUG, "SpringBarleySilage::Do(): "
		 "Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
	}
        else
        {
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (g_date->Date() > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "SpringBarleySilage::Do(): "
		 "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      }
      else
      {
         SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
                                                     sbs_spring_harrow, false );
         break;
      }
	}//if

      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear( 1,11 );
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }

      // OK, let's go.
      SimpleEvent( d1, sbs_autumn_plough, false );
      SBS_ISAUTUMNPLOUGH=false;
      SBS_FERTI_DONE=false;
      SBS_SPRAY=0;
      SBS_MANURE_DONE=false;
      SBS_NPK_DONE=false;
    }
    break;

  case sbs_autumn_plough:
    if ( m_ev->m_lock || m_farm->DoIt( 70 ))
    {
      if (!m_farm->AutumnPlough( m_field, 0.0,
           g_date->DayInYear( 30,11 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbs_autumn_plough, true );
        break;
      }
      SBS_ISAUTUMNPLOUGH=true;
    }
    // +365 for next year
    if (m_farm->IsStockFarmer()) // StockFarmer
    {
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,3 )+365,
               sbs_ferti_s1, false );
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,3 )+365,
               sbs_ferti_s2, false );
      SimpleEvent( g_date->OldDays() + g_date->DayInYear(  1,3 )+365,
               sbs_ferti_s3, false );
    }
    else                         // PlantFarmer
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,3 )+365,
               sbs_ferti_p1, false );
		break;

  //*** The stock farmers thread
  case sbs_ferti_s1:
    if ( m_ev->m_lock || m_farm->DoIt( 90 ))
    {
      if (!m_farm->FA_Slurry( m_field, 0.0,
           g_date->DayInYear( 15, 4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbs_ferti_s1, true );
        break;
      }
      else
      {
        // Done fertilizer so remember
        SBS_FERTI_DONE=true;
      }
    }
    // Queue up the catch all
    SimpleEvent( g_date->Date(),sbs_ferti_s4, false );
    break;

  case sbs_ferti_s2:
    if ( m_ev->m_lock || m_farm->DoIt( 70 ))
    {
      if (!m_farm->FA_NPK( m_field, 0.0,
           g_date->DayInYear( 15, 4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbs_ferti_s2, true );
        break;
      }
      else
      {
        // Done fertilizer so remember
        SBS_FERTI_DONE=true;
      }
    }
    SBS_NPK_DONE=true;
    break;

  case sbs_ferti_s3:
    if (( m_ev->m_lock || m_farm->DoIt( 67 ) ) && (!SBS_ISAUTUMNPLOUGH))
    {
      if (!m_farm->FA_Manure( m_field, 0.0,
           g_date->DayInYear( 15, 4 ) - g_date->DayInYear()))
      {
        SimpleEvent( g_date->Date()+1, sbs_ferti_s3, true );
        break;
      }
      else
      {
        // Done fertilizer so remember
        SBS_FERTI_DONE=true;
      }
    }
    SBS_MANURE_DONE=true;
    break;

  case sbs_ferti_s4:
    if (!SBS_FERTI_DONE)
    {
      if (!m_farm->FA_NPK( m_field, 0.0,
           g_date->DayInYear( 15, 4 ) - g_date->DayInYear()))
      {
        SimpleEvent( g_date->Date()+1, sbs_ferti_s4, true );
        break;
      }
    }
    // re-join main thread
    SimpleEvent( g_date->Date(), sbs_spring_plough, false );
    break;

  //*** The plant farmers thread
  case sbs_ferti_p1:
    // Make sure plant farmers don't suffer because they have not set the
    // stock farmers SBS_flags
    SBS_NPK_DONE=true;
    SBS_MANURE_DONE=true;
    if ( m_ev->m_lock || m_farm->DoIt( 75 ))
    {
      if (!m_farm->FP_NPK( m_field, 0.0,
           g_date->DayInYear( 10, 4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbs_ferti_p1, true );
        break;
      }
      else
      {
        // Done fertilizer so try to do spring plough
        SimpleEvent(  g_date->Date()+1, sbs_spring_plough, false );
        SBS_FERTI_DONE=true;
        break;
      }
    }
    // Did not do ferti_p1 so must do ferti_p2
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,3 ),
                 sbs_ferti_p2, false );
    break;

  case sbs_ferti_p2:
    if (!m_farm->FP_LiquidNH3( m_field, 0.0,
         g_date->DayInYear( 10, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbs_ferti_p2, true );
      break;
    }
    SimpleEvent( g_date->Date(), sbs_ferti_p3, false );
    break;

  case sbs_ferti_p3:
    if (!m_farm->FP_PK( m_field, 0.0,
         g_date->DayInYear( 10, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date()+1, sbs_ferti_p3, true );
      break;
    }
    SimpleEvent( g_date->Date(), sbs_spring_plough, false );
    break;

  // re-join main thread
  case sbs_spring_plough:
    if ( !SBS_ISAUTUMNPLOUGH ) // Don't plough if you have already
    {
      if ((!SBS_NPK_DONE)||(!SBS_MANURE_DONE))
      {
        SimpleEvent( g_date->Date() + 1, sbs_spring_plough, true );
        break;
      }
      if (!m_farm->SpringPlough( m_field, 0.0,
           g_date->DayInYear( 10, 4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbs_spring_plough, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,3 ),
                 sbs_spring_harrow, false );
    break;

  case sbs_spring_harrow:
    if (!m_farm->SpringHarrow( m_field, 0.0,
         g_date->DayInYear( 10,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbs_spring_harrow, true );
      break;
    }
    SimpleEvent( g_date->Date(), sbs_spring_sow, false );
    break;

  case sbs_spring_sow:
    if (!m_farm->SpringSow( m_field, 0.0,
         g_date->DayInYear( 10,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbs_spring_sow, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,4 ),
                        sbs_spring_roll, false );
    break;

  case sbs_spring_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 35 ))
    {
      if (!m_farm->SpringRoll( m_field, 0.0,
           g_date->DayInYear( 20,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbs_spring_roll, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,4 ),
                 sbs_strigling1, false );
    break;

  case sbs_strigling1:
    if ( m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if (!m_farm->Strigling( m_field, 0.0,
           g_date->DayInYear( 25,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbs_strigling1, true );
        break;
      }
      // did strigling so do it again
      SimpleEvent( g_date->Date()+10 , sbs_strigling2, false );
      // --FN--
    }
    else
    {
      //No strigling so do herbicide
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,5 ),
                 sbs_herbicide1, false );
    }
    // Whether did it or not then start fungicide/GR/water
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,5 ),
                 sbs_fungicide1, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,5 ),
                 sbs_GR, false );
	SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 5),
		sbs_water1, false);
	if (cfg_pest_SBS_ERA.value())
	{
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(cfg_SB_InsecticideDay.value(), cfg_SB_InsecticideMonth.value()), // Was 15,5 - changed for skylark testing
	sbs_insecticide1, false);
	}
	break;

  case sbs_strigling2:
    if (!m_farm->Strigling( m_field, 0.0,
         g_date->DayInYear( 10,5 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbs_strigling2, true );
      break;
    }
    break;

  // Herbicide thread
  case sbs_herbicide1:
    if ( m_ev->m_lock || m_farm->DoIt(  (int) (100*cfg_herbi_app_prop.value() * m_farm->Prob_multiplier() ))) //modified probability
    {
		//new - for decision making
		TTypesOfVegetation tov = m_field->GetVegType();
		if(!m_ev->m_lock && !m_farm->Spraying_herbicides(tov)){
			Field * pf =  dynamic_cast<Field*>(m_field); 
			pf->Add_missed_herb_app();
			break;
		} //end of the part for dec. making

      if (!m_farm->HerbicideTreat( m_field, 0.0,
           g_date->DayInYear( 10,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbs_herbicide1, true );
        break;
      }
    }
    // End of thread
    break;

  // GReg thread
  case sbs_GR:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (5*cfg_greg_app_prop.value() )))
    {
      if (!m_farm->GrowthRegulator( m_field, 0.0,
           g_date->DayInYear( 25,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sbs_GR, true );
        break;
      }
    }
    // End of thread
    break;

  // Water thread
  case sbs_water1:
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if (SBS_SPRAY==g_date->DayInYear())
      {
        SimpleEvent( g_date->Date() + 1, sbs_water1, true );
        break;
      }
      if (!m_farm->Water( m_field, 0.0,
           g_date->DayInYear( 30,5 ) - g_date->DayInYear()))
      {
        SimpleEvent( g_date->Date() + 1, sbs_water2, true );
        break;
      }
      if (g_date->DayInYear()+10<g_date->DayInYear( 1,6 ))
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,6 ),
                 sbs_water2, false );
      else
        SimpleEvent( g_date->Date()+10, sbs_water2, false );
      break;
    }
    // End of thread
		// No, water2 have been started.
    break;

  case sbs_water2:
    if ( m_ev->m_lock || m_farm->DoIt( 50 ) )
    {
      if (SBS_SPRAY==g_date->DayInYear())
      {
        SimpleEvent( g_date->Date() + 1, sbs_water2, true );
        break;
      }
      if (!m_farm->Water( m_field, 0.0,
                       g_date->DayInYear( 1,7 ) - g_date->DayInYear() ))
      {
        SimpleEvent( g_date->Date() + 1, sbs_water2, true );
        break;
      }
    }
    // End of thread
    break;

case sbs_insecticide1:
	  if (m_ev->m_lock || m_farm->DoIt((int)floor(0.5 + 35 * cfg_ins_app_prop1.value() * m_farm->Prob_multiplier()))) //modified probability
	  {
		  // Here we check wheter we are using ERA pesticde or not
		  if (!cfg_pest_springbarley_on.value() || !g_landscape_p->SupplyShouldSpray())
		  {
			  if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear()))
			  {
				  SimpleEvent(g_date->Date() + 1, sbs_insecticide1, true);
				  break;
			  }
			  else       SBS_INSECT_DATE = g_date->Date();
		  }
		  else {
			  m_farm->ProductApplication_DateLimited(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			  SBS_INSECT_DATE = g_date->Date();
		  }
	  }
	  SimpleEvent(g_date->Date() + 14, sbs_insecticide2, false);
	  break;

  case sbs_insecticide2:
	  if (m_ev->m_lock || m_farm->DoIt((int)floor(0.5 + 35 * cfg_ins_app_prop2.value()*m_farm->Prob_multiplier()))) //modified probability
	  {
		  if (cfg_pest_springbarley_on.value() && g_landscape_p->SupplyShouldSpray())
		  {
			  m_farm->ProductApplication_DateLimited(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		  }
		  else
		  {
			  if (!m_farm->InsecticideTreat(m_field, 0.0, SBS_INSECT_DATE + 28 - g_date->DayInYear())) {
				  SimpleEvent(g_date->Date() + 1, sbs_insecticide2, true);
			  }
		  }
	  }
	  SimpleEvent(g_date->Date() + 14, sbs_insecticide3, false);
	  break;

  case sbs_insecticide3:
	  if (m_ev->m_lock || m_farm->DoIt((int)floor(0.5 + 35 * cfg_ins_app_prop3.value()*m_farm->Prob_multiplier()))) //modified probability
	  {
		  if (cfg_pest_springbarley_on.value() && g_landscape_p->SupplyShouldSpray())
		  {
			  m_farm->ProductApplication_DateLimited(m_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear(), cfg_pest_product_1_amount.value(), ppp_1);
		  }
		  else
		  {
			  if (!m_farm->InsecticideTreat(m_field, 0.0, SBS_INSECT_DATE + 28 - g_date->DayInYear())) {
				  SimpleEvent(g_date->Date() + 1, sbs_insecticide3, true);
			  }
		  }
	  }
	  break;
  
	  // Fungicide thread  & MAIN THREAD
  case sbs_fungicide1:
    if ( m_ev->m_lock || m_farm->DoIt(  (int) (62*cfg_herbi_app_prop.value()  * m_farm->Prob_multiplier()))) //modified probability
    {
		//new - for decision making
		TTypesOfVegetation tov = m_field->GetVegType();
		if(!m_ev->m_lock && !m_farm->Spraying_fungins(tov)){
			Field * pf =  dynamic_cast<Field*>(m_field); 
			pf->Add_missed_fi_app();
		} //end of the part for dec. making
		else{
		  if (!m_farm->FungicideTreat( m_field, 0.0, g_date->DayInYear( 25,5 ) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, sbs_fungicide1, true );
			break;
		  }
		
		  else
		  {
			SBS_SPRAY=g_date->DayInYear();
			// can also try insecticide
		  }
		}
    }
	ChooseNextCrop (1);
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,6 ),
                 sbs_harvest1, false );
    break;

  case sbs_harvest1:
	  if (m_ev->m_lock || m_farm->DoIt(0)) //then there are just 2 cuts, i.e. the 2nd and the 3rd! The 1st one is impossible (the prob is now 0 instead of 50)
	  {
		  if (!m_farm->Harvest(m_field, 0.0, g_date->DayInYear(8, 7) - g_date->DayInYear())) {
			  SimpleEvent(g_date->Date() + 1, sbs_harvest1, true);
			  break;
		  }
		  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 7),
			  sbs_wait, false);
		  break;
	  }
    else
    {
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,7 ),
                 sbs_harvest2, false );
    }
    break;

  case sbs_harvest2:
    if (!m_farm->Harvest( m_field, 0.0,
         g_date->DayInYear( 8,8 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sbs_harvest2, true );
      break;
    }
    // END MAIN THREAD
    done=true;
    break;

  case sbs_wait: // is only called after June so just finish here.
     // END MAIN THREAD
     d1=g_date->DayInYear();
     done=true;
     break;

  default:
    g_msg->Warn( WARN_BUG, "SpringBarleySilage::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}


