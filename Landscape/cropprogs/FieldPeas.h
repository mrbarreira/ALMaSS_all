//
// FieldPeas.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef FieldPeas_H
#define FieldPeas_H

#define FPEAS_BASE 5300

#define FPEAD_INSECT_DATE m_field->m_user[0]
#define FPEAD_FUNGI_DATE  m_field->m_user[1]
#define FPEAD_WATER_DATE  m_field->m_user[2]
#define FPEAS_DECIDE_TO_HERB  m_field->m_user[3]


typedef enum {
  fp_start = 1, // Compulsory, start event must always be 1 (one).
  fp_harvest= FPEAS_BASE,
  fp_autumn_plough,
  fp_spring_plough,
  fp_spring_harrow,
  fp_fertmanure_plant,
  fp_fertmanure_stock,
  fp_spring_sow,
  fp_spring_roll,
  fp_herbicide_one,
  fp_herbicide_two,
  fp_insecticide,
  fp_fungicide,
  fp_water_one,
  fp_water_two,
  fp_growth_reg,
  fp_straw_chopping
} FieldPeasToDo;



class FieldPeas: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  FieldPeas()
  {
      m_first_date=g_date->DayInYear(1,10);
  }

};

#endif // FieldPeas_H
