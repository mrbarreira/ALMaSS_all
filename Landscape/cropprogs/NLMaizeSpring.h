/**
\file
\brief
<B>NLMaizeSpring.h This file contains the headers for the MaizeSpring class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLMaizeSpring.h
//


#ifndef NLMAIZESPRING_H
#define NLMAIZESPRING_H

#define NLMAIZESPRING_BASE 26400
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_MS_START_FERTI	a_field->m_user[1]

/** Below is the list of things that a farmer can do if he is growing mazie, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_ms_start = 1, // Compulsory, must always be 1 (one).
	nl_ms_sleep_all_day = NLMAIZESPRING_BASE,
	nl_ms_spring_plough_sandy,
	nl_ms_preseeding_cultivator,
	nl_ms_spring_sow_with_ferti,
	nl_ms_spring_sow,
	nl_ms_harrow,
	nl_ms_ferti_p2,
	nl_ms_ferti_s2,
	nl_ms_herbicide1,
	nl_ms_harvest,
	nl_ms_straw_chopping,
} NLMaizeSpringToDo;


/**
\brief
NLMaizeSpring class
\n
*/
/**
See NLMaizeSpring.h::NLMaizeSpringToDo for a complete list of all possible events triggered codes by the mazie management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLMaizeSpring: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLMaizeSpring()
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 30,4 );
   }
};

#endif // NLMAIZESPRING_H

