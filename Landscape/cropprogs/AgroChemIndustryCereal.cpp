//
// AgroChemIndustryCereal.cpp
//
/*

Copyright (c) 2003, National Environmental Research Institute, Denmark (NERI)

All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/
//#include "stdafx.h"

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/AgroChemIndustryCereal.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgFloat cfg_pest_product_1_amount;

static CfgBool cfg_syn_no_pesticide("SYN_NO_PESTICIDE",CFG_CUSTOM, false );
static CfgInt cfg_syn_startspray("SYN_STARTSPRAY",CFG_CUSTOM, 0 );
static CfgInt cfg_syn_endspray("SYN_ENDSPRAY",CFG_CUSTOM, 100000 );
static CfgInt cfg_syn_sprayinterval("SYN_SPRAYINTERVAL",CFG_CUSTOM, 21 );
static CfgInt cfg_syn_spraywindow("SYN_SPRAYWINDOW",CFG_CUSTOM, 15 );



bool AgroChemIndustryCereal::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;
  int d1;

  switch ( m_ev->m_todo )
  {
  case sc_start:
    {
      SC_AUTUMN_PLOUGH         = false;
      SC_WAIT_FOR_PLOUGH       = false;
      // Set up the date management stuff
      // Could save the start day in case it is needed later
      // m_field->m_startday = m_ev->m_startday;
      m_last_date=g_date->DayInYear(15,10);
      // Start and stop dates for all events after harvest
      int noDates=5;
      m_field->SetMDates(0,0,g_date->DayInYear(20,8));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(20,8));
      m_field->SetMDates(0,1,0); // Subbleharrow start
      m_field->SetMDates(1,1,g_date->DayInYear(20,8));
      m_field->SetMDates(0,2,g_date->DayInYear(5,8));
      m_field->SetMDates(1,2,g_date->DayInYear(25,8));
      m_field->SetMDates(0,3,g_date->DayInYear(10,8));
      m_field->SetMDates(1,3,g_date->DayInYear(15,9));
      m_field->SetMDates(0,4,g_date->DayInYear(15,8));
      m_field->SetMDates(1,4,g_date->DayInYear(15,10));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary
      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "AgroChemIndustryCereal::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++)
        {
          if  (m_field->GetMDates(0,i)>=m_ev->m_startday)
                                     m_field->SetMDates(0,i,m_ev->m_startday-1);
          if  (m_field->GetMDates(1,i)>=m_ev->m_startday)
                                     m_field->SetMDates(1,i,m_ev->m_startday-1);
        }
      }
      // Now no operations can be timed after the start of the next crop.

      // CJT note:
      // Start single block date checking code to be cut-'n-pasted...
      int d1;
      if ( ! m_ev->m_first_year )
      {
	// Are we before July 1st?
	d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
	if (g_date->Date() < d1)
        {
	  // Yes, too early. We assumme this is because the last crop was late
          g_msg->Warn( WARN_BUG, "AgroChemIndustryCereal::Do(): "
		 "Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
	}
        else
        {
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (g_date->Date() > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "AgroChemIndustryCereal::Do(): "
		 "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      }
      else
      {
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
             sc_spring_roll, false );
        break;
      }
      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear( 21,8 );
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }

      // OK, let's go.
      if (m_farm->IsStockFarmer()) // StockFarmer
      {
        SimpleEvent( d1, sc_ferti_s1, false );
      }
      else SimpleEvent( d1, sc_ferti_p1, false );
    }
    break;

  case sc_ferti_p1:
    if ( m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if (!m_farm->FP_Slurry( m_field, 0.0,
           g_date->DayInYear( 1,10 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_ferti_p1, true );
        break;
      }
    }
    SimpleEvent( g_date->Date(),sc_autumn_plough, false );
    break;

  case sc_ferti_s1:
    if (!m_farm->FA_Slurry( m_field, 0.0,
           g_date->DayInYear( 1,10 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sc_ferti_s1, true );
      break;
    }
    // This may cause two applications of fertilizer in one day...
    // --FN--
    SimpleEvent( g_date->Date(),sc_ferti_s2, false );
    // --FN--
    break;

  case sc_ferti_s2:
    if ( m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if (!m_farm->FA_Manure( m_field, 0.0,
           g_date->DayInYear( 1,10 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_ferti_s2, true );
        break;
      }
    }
    SimpleEvent( g_date->Date(),sc_autumn_plough, false );
    break;

  case sc_autumn_plough:
    if ( m_ev->m_lock || m_farm->DoIt( 95 ))
    {
      if (!m_farm->AutumnPlough( m_field, 0.0,
           g_date->DayInYear( 1,10 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_autumn_plough, true );
        break;
      }
      else
      {
        SC_AUTUMN_PLOUGH=true;
        SimpleEvent( g_date->Date()+1,sc_autumn_harrow, false );
        break;
      }
    }
    SimpleEvent( g_date->Date()+1,sc_stubble_harrow1, false );
    break;

  case sc_autumn_harrow:
    if (!m_farm->AutumnHarrow( m_field, 0.0,
           g_date->DayInYear( 10,10 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sc_autumn_harrow, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,9 ),
               sc_autumn_sow, false );
    break;

  case sc_stubble_harrow1:
    if (!m_farm->StubbleHarrowing( m_field, 0.0,
           g_date->DayInYear( 10,10 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sc_stubble_harrow1, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,9 ),
               sc_autumn_sow, false );
    break;

  case sc_autumn_sow:
    if (!m_farm->AutumnSow( m_field, 0.0,
           g_date->DayInYear( 20,10 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sc_autumn_sow, true );
      break;
    }
    SimpleEvent( g_date->Date() + 1, sc_autumn_roll, false );
    break;

  case sc_autumn_roll:
    if (( m_ev->m_lock || m_farm->DoIt( 5 ))&& (SC_AUTUMN_PLOUGH))
    {
      if (!m_farm->AutumnRoll( m_field, 0.0,
           g_date->DayInYear( 27,10 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_autumn_roll, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,9 ),
                      sc_ferti_p2, false );
    break;

  case sc_ferti_p2:
    if (( m_ev->m_lock || m_farm->DoIt( 20 )) && (!m_farm->IsStockFarmer()))
    {
      if ( m_field->GetVegBiomass()>0)
      //only when there has been a bit of growth
      {
        if (!m_farm->FP_ManganeseSulphate( m_field, 0.0,
           g_date->DayInYear( 30,10 ) - g_date->DayInYear()))
        {
          SimpleEvent( g_date->Date() + 1, sc_ferti_p2, true );
          break;
        }
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,9 ),
                      sc_herbicide1, false );
    break;

  case sc_herbicide1:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (90*cfg_herbi_app_prop.value() )))
    {
      if (!m_farm->HerbicideTreat( m_field, 0.0,
             g_date->DayInYear( 5,10 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_herbicide1, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 )+365,
             sc_spring_roll, false );
    break;

  case sc_spring_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 5 ))
    {
      if (!m_farm->SpringRoll( m_field, 0.0,
           g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_spring_roll, true );
        break;
      }
    }
    if (m_farm->IsStockFarmer()) // StockFarmer
    {
      SimpleEvent( g_date->Date() + 1, sc_ferti_s3, false );
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,4 ),
                    sc_ferti_s4, false );
    }
    else
    	SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,3 ),
                   sc_ferti_p3, false );
    // All need the next threads
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,4 ),
             sc_herbicide2, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,4 ),
             sc_GR, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,4 ),
             sc_fungicide, false );
    if (!cfg_syn_no_pesticide.value())
    {
      d1=g_date->OldDays() + g_date->DayInYear();
      if ((d1>(cfg_syn_startspray.value()*365)) && (d1<(cfg_syn_endspray.value()*365))) {
        // New version here 26/11/04
        d1=GetSprayDate()+random(cfg_syn_spraywindow.value());
        SimpleEvent( g_date->OldDays() + d1 ,sc_insecticide1, true );
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,4 ),
             sc_strigling1, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,5 ),
             sc_water1, false );
    break;

  case sc_herbicide2:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (33*cfg_herbi_app_prop.value() )))
    {
      if (!m_farm->HerbicideTreat( m_field, 0.0,
             g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_herbicide2, true );
        break;
      }
    }
    // End of thread
    break;

  case sc_GR:
    if ( m_ev->m_lock || m_farm->DoIt( 50 ))
    {
      if (!m_farm->GrowthRegulator( m_field, 0.0,
             g_date->DayInYear( 10,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_GR, true );
        break;
      }
    }
    // End of thread
    break;

  case sc_fungicide:
    if ( m_ev->m_lock || m_farm->DoIt( 95 ))
    {
      if (!m_farm->FungicideTreat( m_field, 0.0,
             g_date->DayInYear( 10,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_fungicide, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,5 ),
             sc_fungicide2, false );
    break;

  case sc_fungicide2:
    if ( m_ev->m_lock || m_farm->DoIt( 53 ))
    {
      if (!m_farm->FungicideTreat( m_field, 0.0,
             g_date->DayInYear( 15,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_fungicide2, true );
        break;
      }
    }
    // End of thread
    break;

// This version creates evenly distributed sprays after the first one is fixed
case sc_insecticide1:
  if ( m_ev->m_lock || m_farm->DoIt( (int) (100*cfg_ins_app_prop1.value() )))
  {
    if (!m_farm->ProductApplication(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1))
       SimpleEvent( g_date->Date() + 1, sc_insecticide1, true );
    break;
  }
  SimpleEvent( g_date->OldDays() + g_date->DayInYear() + cfg_syn_sprayinterval.value(), sc_insecticide2, false );
  break;

case sc_insecticide2:
  if ( m_ev->m_lock || m_farm->DoIt( (int) (100*cfg_ins_app_prop1.value() )))
  {
    if (!m_farm->ProductApplication( m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1))
       SimpleEvent( g_date->Date() + 1, sc_insecticide2, true );
//    SimpleEvent( g_date->OldDays() + g_date->DayInYear() + cfg_syn_sprayinterval.value(),
//               sc_insecticide3, false );
    break;
  }
  break;

/*
case sc_insecticide3:
  if ( m_ev->m_lock || m_farm->DoIt( 67*cfg_ins_app_prop1.value() ))
  {
     m_farm->SynInsecticideTreat( m_field, 0.0, 0);
  }
  break;
*/


  case sc_strigling1:
    if ( m_ev->m_lock || m_farm->DoIt( 5 ))
    {
      if (!m_farm->Strigling( m_field, 0.0,
             g_date->DayInYear( 25,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_strigling1, true );
        break;
      }
      else
      {
        if ((g_date->Date()+7)<( g_date->OldDays() + g_date->DayInYear( 15,6 )))
              SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,4),
    	               sc_strigling2, false );
        else
        	SimpleEvent( g_date->Date()+7,sc_strigling2, false );
      }
    }
    break;

  case sc_strigling2:
      if (!m_farm->Strigling( m_field, 0.0,
             g_date->DayInYear( 5,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_strigling2, true );
        break;
    }
    // End of thread
    break;

  case sc_water1:
    if ( m_ev->m_lock || m_farm->DoIt( 10 )) // **CJT** Soil type 1-4 only
    {
      if (!m_farm->Water( m_field, 0.0,
             g_date->DayInYear( 15,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_water1, true );
        break;
      }
      else
        if ((g_date->Date()+5)<( g_date->OldDays() + g_date->DayInYear( 2,5 )))
            SimpleEvent( g_date->OldDays() + g_date->DayInYear( 2,5 ),
                         sc_water2, false );
        else
          SimpleEvent( g_date->Date()+5, sc_water2, false );
    }
    break;

  case sc_water2:
    if (!m_farm->Water( m_field, 0.0,
           g_date->DayInYear( 1,6 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sc_water2, true );
      break;
    }
    // End of thread
    break;

  case sc_ferti_p3:
    if (!m_farm->FP_NPK( m_field, 0.0,
         g_date->DayInYear( 15,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sc_ferti_p3, true );
      break;
    }

    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,4 ),
                   sc_ferti_p4, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
                   sc_ferti_p5, false );
    break;

  case sc_ferti_p4:
    if ( m_ev->m_lock || m_farm->DoIt( 50 ))
    {
      if (!m_farm->FP_NPK( m_field, 0.0,
             g_date->DayInYear( 15,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_ferti_p4, true );
        break;
      }
    }
    // The Main thread
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,8 ),
                 sc_harvest, false );
    break;

   case sc_ferti_p5:
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if (!m_farm->FP_ManganeseSulphate( m_field, 0.0,
             g_date->DayInYear( 5,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_ferti_p5, true );
        break;
      }
    }
    break;

  case sc_ferti_s3:
    if (!m_farm->FA_Slurry(m_field, 0.0,
         g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sc_ferti_s3, true );
      break;
    }
    // The Main thread
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,8 ),
                 sc_harvest, false );
    break;

  case sc_ferti_s4:
    if ( m_ev->m_lock || m_farm->DoIt( 75 ))
    {
      if (!m_farm->FA_NPK( m_field, 0.0,
           g_date->DayInYear( 20,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_ferti_s4, true );
        break;
      }
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 21,4 ),
                   sc_ferti_s5, false );
    }
   break;

  case sc_ferti_s5:
    if ( m_ev->m_lock || m_farm->DoIt( 40 ))
    {
      if (!m_farm->FA_NPK( m_field, 0.0,
           g_date->DayInYear( 1,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_ferti_s5, true );
        break;
      }
    }
    break; // End of thread

  case sc_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
           g_date->DayInYear( 20,8 ) - g_date->DayInYear()))
    {
        SimpleEvent( g_date->Date() + 1, sc_harvest, true );
        break;
    }
    SimpleEvent( g_date->Date(), sc_straw_chopping, false );
    break;

  case sc_straw_chopping:
    if ( m_ev->m_lock || m_farm->DoIt( 75 ))
    {
      if (!m_farm->StrawChopping( m_field, 0.0,
           m_field->GetMDates(1,0) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_straw_chopping, true );
        break;
      }
      else
      {
          SimpleEvent( g_date->Date()+1, sc_stubble_harrow2, false );
      }
    }
    else
    {
      SimpleEvent( g_date->Date()+1, sc_hay_turning, false );
    }
   break;

  case sc_hay_turning:
    if ( m_ev->m_lock || m_farm->DoIt( 5 ))
    {
      if (!m_farm->HayTurning( m_field, 0.0,
           m_field->GetMDates(1,1) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_hay_turning, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,2),
                 sc_hay_baling, false );
    break;

  case sc_hay_baling:
    if (!m_farm->HayBailing( m_field, 0.0,
       m_field->GetMDates(1,2) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sc_hay_baling, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,3),
                   sc_stubble_harrow2, false );
    break;

  case sc_stubble_harrow2:
    if ( m_ev->m_lock || m_farm->DoIt( 65 ))
    {
      if (!m_farm->StubbleHarrowing( m_field, 0.0,
           m_field->GetMDates(1,3) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_stubble_harrow2, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,4),
                   sc_grubning, false );
    break;

  case sc_grubning:
    if ( m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if (!m_farm->DeepPlough( m_field, 0.0,
           m_field->GetMDates(1,4) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sc_grubning, true );
        break;
      }
    }
    done=true;
    // END OF MAIN THREAD
    break;

  default:
    g_msg->Warn( WARN_BUG, "AgroChemIndustryCereal::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}



int AgroChemIndustryCereal::GetSprayDate() {
  int d1;
  int r=random(100);
  if (r<0) { // April
    d1=April1;
  }  else {
     if (r<0) { // May
      d1=May1;
    } else {
      if (r<100) { // June
        d1=June1;
      } else {  // July
          d1=July1;
        }
    }
  }
  //d1+=random(30);
  d1+=30-(cfg_syn_spraywindow.value()/2);
  return d1;
}
