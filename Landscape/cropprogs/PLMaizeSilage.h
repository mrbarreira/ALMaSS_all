/**
\file
\brief
<B>PLMaizeSilage.h This file contains the headers for the MaizeSilage class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PLMaizeSilage.h
//


#ifndef PLMAIZESILAGE_H
#define PLMAIZESILAGE_H

#define PLMAIZESILAGE_BASE 20800
/**
\brief A flag used to indicate autumn ploughing status
*/
#define PL_MS_FERTI_P1	a_field->m_user[1]
#define PL_MS_FERTI_S1	a_field->m_user[2]
#define PL_MS_STUBBLE_PLOUGH	a_field->m_user[3]
#define PL_MS_WINTER_PLOUGH	a_field->m_user[4]
#define PL_MS_FERTI_P3	a_field->m_user[5]
#define PL_MS_FERTI_S3	a_field->m_user[6]
#define PL_MS_SPRING_FERTI a_field->m_user[7]
#define PL_MS_START_FERTI	a_field->m_user[8]

/** Below is the list of things that a farmer can do if he is growing mazie, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_ms_start = 1, // Compulsory, must always be 1 (one).
	pl_ms_sleep_all_day = PLMAIZESILAGE_BASE,
	pl_ms_ferti_p1, // 20801
	pl_ms_ferti_s1,
	pl_ms_stubble_plough,
	pl_ms_autumn_harrow1,
	pl_ms_autumn_harrow2,
	pl_ms_stubble_harrow,
	pl_ms_ferti_p2,
	pl_ms_ferti_s2,
	pl_ms_winter_plough,
	pl_ms_winter_stubble_cultivator_heavy,	// 20810
	pl_ms_ferti_p3,
	pl_ms_ferti_s3,
	pl_ms_spring_harrow,
	pl_ms_ferti_p4,
	pl_ms_ferti_s4,
	pl_ms_ferti_p5,
	pl_ms_ferti_s5,
	pl_ms_heavy_cultivator,
	pl_ms_preseeding_cultivator,
	pl_ms_spring_sow_with_ferti,
	pl_ms_spring_sow,	// 20821	
	pl_ms_herbicide1,
	pl_ms_herbicide2,
	pl_ms_fungicide1,
	pl_ms_insecticide1,
	pl_ms_insecticide2,
	pl_ms_biocide,
	pl_ms_ferti_p6,
	pl_ms_ferti_s6,	
	pl_ms_ferti_p7,
	pl_ms_ferti_s7,	// 20831
	pl_ms_harvest,
	pl_ms_straw_chopping,
	pl_ms_hay_bailing,
	pl_ms_ferti_p8,
	pl_ms_ferti_s8,
	pl_ms_ferti_p9,	
	pl_ms_ferti_s9,	// 20838
} PLMaizeSilageToDo;


/**
\brief
PLMaizeSilage class
\n
*/
/**
See PLMaizeSilage.h::PLMaizeSilageToDo for a complete list of all possible events triggered codes by the maize management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLMaizeSilage: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PLMaizeSilage()
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 5,11 );
   }
};

#endif // PLMAIZESILAGE_H

