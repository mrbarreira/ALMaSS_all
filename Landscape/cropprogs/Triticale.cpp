//
// Triticale.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/Triticale.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;

bool Triticale::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  int d1=0;
  int noDates=2;
  bool done = false;

  switch ( m_ev->m_todo )
  {
  case tri_start:
	  TRI_DECIDE_TO_HERB=1;
      // Set up the date management stuff
      m_last_date=g_date->DayInYear(15,9);
      // Start and stop dates for all events after harvest
      m_field->SetMDates(0,0,g_date->DayInYear(5,8));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(20,8));
      m_field->SetMDates(0,1,g_date->DayInYear(20,8));
      m_field->SetMDates(1,1,g_date->DayInYear(15,9));
    // Check the next crop for early start, unless it is a spring crop
    // in which case we ASSUME that no checking is necessary!!!!
    // So DO NOT implement a crop that runs over the year boundary

	//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
	 if(!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){
    if (m_ev->m_startday>g_date->DayInYear(1,7))
    {
      if (m_field->GetMDates(0,0) >=m_ev->m_startday)
      {
        g_msg->Warn( WARN_BUG, "Triticale::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
        exit( 1 );
      }
      // Now fix any late finishing problems
      for (int i=0; i<noDates; i++) {
			if(m_field->GetMDates(0,i)>=m_ev->m_startday) { 
				m_field->SetMDates(0,i,m_ev->m_startday-1); //move the starting date
			}
			if(m_field->GetMDates(1,i)>=m_ev->m_startday){
				m_field->SetMConstants(i,0); 
				m_field->SetMDates(1,i,m_ev->m_startday-1); //move the finishing date
			}
		}
    }
      // Now no operations can be timed after the start of the next crop.
      // Start single block date checking code to be cut-'n-pasted...
      if ( ! m_ev->m_first_year )
      {
	// Are we before July 1st?
	d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
	if (g_date->Date() < d1)
        {
	  // Yes, too early. We assumme this is because the last crop was late
          g_msg->Warn( WARN_BUG, "Triticale::Do(): "
		 "Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
	}
        else
        {
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (g_date->Date() > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "Triticale::Do(): "
		 "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      }
      else
      {
        // Is the first year so must start in spring like nothing was unusual
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,8 )
                                                     ,tri_harvest, false );
        break;
      }
	}//if

      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear( 20,8 );
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }

      // OK, let's go.
    TRI_OCCUP_DATE = 0;
    TRI_WATER_DATE = 0;

    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,8 ),
		 tri_fa_manure, false );
    break;

  case tri_fa_manure:
    if ( m_ev->m_lock || m_farm->DoIt( 40 )) {
      if (!m_farm->FA_Manure( m_field, 0.0,
			      g_date->DayInYear( 5,10 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, tri_fa_manure, true );
	break;
      }
    }
    {
      d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 1,9 ))
	d1 = g_date->OldDays() + g_date->DayInYear( 1,9 );
      SimpleEvent( d1, tri_autumn_plough, false );
    }
    break;

  case tri_autumn_plough:
    if (!m_farm->AutumnPlough( m_field, 0.0,
			       g_date->DayInYear( 15,10 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, tri_autumn_plough, true );
      break;
    }
    SimpleEvent( g_date->Date() + 1, tri_autumn_harrow, false );
    break;

  case tri_autumn_harrow:
    if (!m_farm->AutumnHarrow( m_field, 0.0,
			       g_date->DayInYear( 15,10 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, tri_autumn_harrow, true );
      break;
    }
    SimpleEvent( g_date->Date(), tri_autumn_sow, false );
    break;

  case tri_autumn_sow:
    if (!m_farm->AutumnSow( m_field, 0.0,
			    g_date->DayInYear( 15,10 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, tri_autumn_sow, true );
      break;
    }
    SimpleEvent( g_date->Date(), tri_autumn_roll, false );
    break;

  case tri_autumn_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 5 )) {
      if (!m_farm->AutumnRoll( m_field, 0.0,
			       g_date->DayInYear( 10,10 ) -
			       g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, tri_autumn_roll, true );
	break;
      }
    }
    {
      d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 15,9 ))
	d1 = g_date->OldDays() + g_date->DayInYear( 15,9 );
      SimpleEvent( d1, tri_herbi_one, false );
    }
    break;

  case tri_herbi_one:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (90*cfg_herbi_app_prop.value()  * m_farm->Prob_multiplier()))) { //modified probability
     		
		//new - for decision making
		TTypesOfVegetation tov = m_field->GetVegType();
		if(!m_ev->m_lock && !m_farm->Spraying_herbicides(tov)){
			Field * pf =  dynamic_cast<Field*>(m_field); 
			pf->Add_missed_herb_app();
			if(m_farm->DoIt(50)) pf->Add_missed_herb_app(); //the 2nd missed application
			TRI_DECIDE_TO_HERB=0;
		} //end of the part for dec. making
		else{
			if (!m_farm->HerbicideTreat( m_field, 0.0, g_date->DayInYear( 15,10 ) - g_date->DayInYear())) {
				SimpleEvent( g_date->Date() + 1, tri_herbi_one, true );
				break;
			}
		}
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,3 ) + 365,
		 tri_spring_roll, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ) + 365,
		 tri_fa_npk, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,4 ) + 365,
		 tri_fa_slurry, false );
    if ( m_farm->DoIt( 15 ))
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,4 ) + 365,
		   tri_herbi_two, false );
    else
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ) + 365,
		   tri_strigling, false );
    break;

  case tri_spring_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 15 ))
    {
      if (!m_farm->SpringRoll( m_field, 0.0,
			       g_date->DayInYear( 10,4 ) -
			       g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, tri_spring_roll, true );
        break;
      }
    }
    break;

  case tri_fa_npk:
    if (!m_farm->FA_NPK( m_field, 0.0,
			 g_date->DayInYear( 30, 4 ) -
			 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, tri_fa_npk, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,5 ),
		 tri_GR_one, false );
    break;

  case tri_fa_slurry:
    if ( m_ev->m_lock || m_farm->DoIt( 90 ))
    {
      if (!m_farm->FA_Slurry( m_field, 0.0,
			      g_date->DayInYear( 25,4 ) -
			      g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, tri_fa_slurry, true );
        break;
      }
    }
    break;

  case tri_herbi_two:
    if ( m_ev->m_lock || m_farm->DoIt(  (int) (50*cfg_herbi_app_prop.value() *TRI_DECIDE_TO_HERB * m_farm->Prob_multiplier()))) { //modified probability
      if (!m_farm->HerbicideTreat( m_field, 0.0,
				   g_date->DayInYear( 30,5 ) -
				   g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, tri_herbi_two, true );
	break;
      }
    }
    break;

  case tri_strigling:
    if ( m_ev->m_lock || m_farm->DoIt( 10 )) {
      if (!m_farm->Strigling( m_field, 0.0,
			      g_date->DayInYear( 30,4 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, tri_strigling, true );
	break;
      }
    }
    break;

  case tri_GR_one:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (70*cfg_greg_app_prop.value() )))
    {
      if (!m_farm->GrowthRegulator( m_field, 0.0,
				    g_date->DayInYear( 15,5 ) -
				    g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, tri_GR_one, true );
        break;
      }
    }
    {
      d1 = g_date->Date() + 10;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 20,5 ))
	d1 = g_date->OldDays() + g_date->DayInYear( 20,5 );
      SimpleEvent( d1, tri_GR_two, true );
      SimpleEvent( d1, tri_fungicide, true );
    }
    break;

  case tri_GR_two:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (60*cfg_greg_app_prop.value() )))
    {
      if (!m_farm->GrowthRegulator( m_field, 0.0,
				    g_date->DayInYear( 1,6 ) -
				    g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, tri_GR_two, true );
        break;
      }

    }
    SimpleEvent( g_date->Date() + 1, tri_water, true );
    break;

  case tri_fungicide:
    if ( g_date->Date() < TRI_WATER_DATE + 1 ) {
      SimpleEvent( g_date->Date() + 1, tri_fungicide, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( (int) (75*cfg_fungi_app_prop1.value()  * m_farm->Prob_multiplier()))) //modified probability
    {
		//new - for decision making
		TTypesOfVegetation tov = m_field->GetVegType();
		if(!m_ev->m_lock && !m_farm->Spraying_fungins(tov)){
			Field * pf =  dynamic_cast<Field*>(m_field); 
			pf->Add_missed_fi_app();
		} //end of the part for dec. making
		else{
		  if (!m_farm->FungicideTreat( m_field, 0.0, g_date->DayInYear( 10,6 ) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, tri_fungicide, true );
			break;
		  }
		}
      TRI_OCCUP_DATE = g_date->Date();
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,6 ),
		 tri_insecticide, false );
    break;

  case tri_insecticide:
    if ( g_date->Date() < TRI_WATER_DATE + 1 ) {
      SimpleEvent( g_date->Date() + 1, tri_insecticide, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( (int) (16*cfg_ins_app_prop1.value()  * m_farm->Prob_multiplier()))) //modified probability
    {
		//new - for decision making
		TTypesOfVegetation tov = m_field->GetVegType();
		if(!m_ev->m_lock && !m_farm->Spraying_fungins(tov)){
			Field * pf =  dynamic_cast<Field*>(m_field); 
			pf->Add_missed_fi_app();
		} //end of the part for dec. making
		else{
		  if (!m_farm->InsecticideTreat( m_field, 0.0, g_date->DayInYear( 10,7 ) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, tri_insecticide, true );
			break;
		  }
		}
      TRI_OCCUP_DATE = g_date->Date();
    }
	ChooseNextCrop (2);
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,8 ),
		 tri_harvest, false );
    break;

  case tri_water:
    if ( g_date->Date() < TRI_OCCUP_DATE + 1 ) {
      SimpleEvent( g_date->Date() + 1, tri_water, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( 15 ))
    {
      if (!m_farm->Water( m_field, 0.0,
			  g_date->DayInYear( 30,6 ) -
			  g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, tri_water, true );
        break;
      }
      TRI_WATER_DATE = g_date->Date();
    }
    break;

  case tri_harvest:
    if (m_field->GetMConstants(0)==0) {
		if (!m_farm->Harvest( m_field, 0.0, -1)) { //raise an error
			g_msg->Warn( WARN_BUG, "Triticale::Do(): failure in 'Harvest' execution", "" );
			exit( 1 );
		} 
	}
	else {  
		if (!m_farm->Harvest( m_field, 0.0, m_field->GetMDates(1,0) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, tri_harvest, true );
			break;
		}
	}
    if ( m_farm->DoIt( 75 ))
      SimpleEvent( g_date->Date(), tri_chopping, true );
    else
      SimpleEvent( g_date->Date(), tri_hay_turning, true );
    break;

  case tri_chopping:
	if (m_field->GetMConstants(0)==0) {
		if (!m_farm->StrawChopping( m_field, 0.0, -1)) { //raise an error
			g_msg->Warn( WARN_BUG, "Triticale::Do(): failure in 'StrawChopping' execution", "" );
			exit( 1 );
		} 
	}
	else { 
		if (!m_farm->StrawChopping( m_field, 0.0, m_field->GetMDates(1,0) - g_date->DayInYear())) {
		  SimpleEvent( g_date->Date() + 1, tri_chopping, true );
		  break;
		}
	}
    SimpleEvent( g_date->Date(), tri_stubble_harrow, false );
    break;

  case tri_hay_turning:
    if ( m_ev->m_lock || m_farm->DoIt( 20 )) {
		if (m_field->GetMConstants(0)==0) {
			if (!m_farm->HayTurning( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "Triticale::Do(): failure in 'HayTurning' execution", "" );
				exit( 1 );
			} 
		}
		else { 
			if (!m_farm->HayTurning( m_field, 0.0, m_field->GetMDates(1,0) - g_date->DayInYear())) {
				SimpleEvent( g_date->Date() + 1, tri_hay_turning, true );
				break;
			}
		}
    }
    SimpleEvent( g_date->Date(), tri_hay_bailing, false );
    break;

  case tri_hay_bailing:
    if (m_field->GetMConstants(0)==0) {
			if (!m_farm->HayBailing( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "Triticale::Do(): failure in 'HayBailing' execution", "" );
				exit( 1 );
			} 
	}
	else { 
		if (!m_farm->HayBailing( m_field, 0.0, m_field->GetMDates(1,0) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, tri_hay_bailing, true );
			break;
		}
	}
    SimpleEvent( g_date->Date(), tri_stubble_harrow, false );
    break;

  case tri_stubble_harrow:
     if (m_field->GetMConstants(1)==0) {
		if (!m_farm->StubbleHarrowing( m_field, 0.0, -1)) { //raise an error
			g_msg->Warn( WARN_BUG, "Triticale::Do(): failure in 'StubbleHarrowing' execution", "" );
			exit( 1 );
		} 
	}
	else { 
		if (!m_farm->StubbleHarrowing( m_field, 0.0, m_field->GetMDates(1,1) - g_date->DayInYear())) {
		  SimpleEvent( g_date->Date() + 1, tri_stubble_harrow, true );
		  break;
		}
	}
    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "Triticale::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}


