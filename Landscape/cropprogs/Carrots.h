//
// CarrotsEat.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef CARROTS_H
#define CARROTS_H

#define CARROTS_BASE 7100
#define CA_SLURRY_DATE m_field->m_user[0]
#define CA_DECIDE_TO_HERB m_field->m_user[1]
#define CA_DECIDE_TO_FI m_field->m_user[2]



typedef enum {
  ca_start = 1, // Compulsory, start event must always be 1 (one).
  ca_fa_slurry = CARROTS_BASE,
  ca_fp_slurry,
  ca_spring_plough,
  ca_spring_harrow_one,
  ca_spring_harrow_two,
  ca_spring_sow,
  ca_herbi_one,
  ca_herbi_two,
  ca_herbi_three,
  ca_herbi_four,
  ca_rowcul_one,
  ca_rowcul_two,
  ca_rowcul_three,
  ca_insect_one,
  ca_insect_two,
  ca_insect_three,
  ca_water_one,
  ca_water_two,
  ca_water_three,
  ca_fp_npk_one,
  ca_fp_npk_two,
  ca_fa_npk_one,
  ca_fa_npk_two,
  ca_harvest
} CarrotsToDo;

class Carrots: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  Carrots() {
    m_first_date=g_date->DayInYear(25,4);
  }
};

#endif // CARROTS_H
