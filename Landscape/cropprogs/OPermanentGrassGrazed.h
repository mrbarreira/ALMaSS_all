//
// OPermanentGrassGrazed.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OPermanentGrassGrazed_H
#define OPermanentGrassGrazed_H

#define OPGG_BASE 4600
#define OPGG_CUT_DATE    m_field->m_user[0]
#define OPGG_FERTI_DATE  m_field->m_user[1]
#define OPGG_TURN_DATE   m_field->m_user[2]

typedef enum {
  opgg_start = 1, // Compulsory, start event must always be 1 (one).
  opgg_cut_to_hay = OPGG_BASE,
  opgg_cattle_out1,
  opgg_cattle_out2,
  opgg_cattle_is_out,
  opgg_cut_weeds,
  opgg_ferti_s,
  opgg_raking1,
  opgg_raking2,
  opgg_compress_straw
} OPermanentGrassGrazedToDo;
//--FN--


class OPermanentGrassGrazed: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OPermanentGrassGrazed()
  {
    m_first_date=g_date->DayInYear(14,4);
  }
};

#endif // OPermanentGrassGrazed_H
