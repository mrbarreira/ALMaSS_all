/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

//#define __EcoSol_01
#define _CRT_SECURE_NO_DEPRECATE
#include "../../BatchALMaSS/ALMaSS_Setup.h"
#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DummyCropPestTesting.h"
#include "../../BatchALMaSS/BoostRandomGenerators.h"

extern Landscape * g_landscape_p;
extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;


using namespace std;

bool DummyCropPestTesting::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code
	switch (a_ev->m_todo)
	{
	case dcpt_start:
	{
		// dcpt_start just sets up all the starting conditions and reference dates that are needed to start a pl_sb
		DCPT_APRIL_PEST = 0;
		DCPT_MAY_PEST = 0;
		DCPT_JUNE_PEST = 0;
		DCPT_JULY_PEST = 0;
		DCPT_SEPT_PEST = 0;
		DCPT_OCT_PEST = 0;

		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 5 start and stop dates for all 'movable' events for this crop
		int noDates = 1;
		a_field->SetMDates(0, 0, g_date->DayInYear(15, 11));
		a_field->SetMDates(1, 0, g_date->DayInYear(15, 11));

		a_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "PLSpringBarleySpr::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = g_landscape_p->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (a_field->GetMDates(0, i) >= a_ev->m_startday) {
						a_field->SetMDates(0, i, a_ev->m_startday - 1); //move the starting date
					}
					if (a_field->GetMDates(1, i) >= a_ev->m_startday) {
						a_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						a_field->SetMDates(1, i, a_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				int today = g_date->Date();
				d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
				if (today > d1) {
					// Yes too late - should not happen - raise an error
					g_msg->Warn(WARN_BUG, "PLSpringBarleySpr::Do(): " "Crop start attempt after last possible start date", "");
					exit(1);
				}
			}
			else {
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), dcpt_March_pest1, false, a_farm, a_field);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line
		 // comment as well.
		 // Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + 365 + g_date->DayInYear(1, 3);
		// OK, let's go.
		SimpleEvent_(d1, dcpt_March_pest1, false, a_farm, a_field);
		break;
	}
	break;

	// This is the first real farm operation
	case dcpt_March_pest1:
		if (a_ev->m_lock || a_farm->DoIt(3))
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, dcpt_March_pest1, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4), dcpt_April_pest1, false, a_farm, a_field);
		break;
	case dcpt_April_pest1:
		if (a_ev->m_lock || a_farm->DoIt(29)) //40% will spray just once in April
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, dcpt_April_pest1, true, a_farm, a_field);
				break;
			}
			DCPT_APRIL_PEST = 1;
		}
		SimpleEvent_(g_date->Date() + 10, dcpt_April_pest2, false, a_farm, a_field);
		break;
	case dcpt_April_pest2:
		if (a_ev->m_lock || a_farm->DoIt(1) && (DCPT_APRIL_PEST == 1)) //1.08% will spray twice in April
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, dcpt_April_pest2, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), dcpt_May_pest1, false, a_farm, a_field);
		break;
	case dcpt_May_pest1:
		if (a_ev->m_lock || a_farm->DoIt(22)) //38% will spray just once
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, dcpt_May_pest1, true, a_farm, a_field);
				break;
			}
			DCPT_MAY_PEST = 1;
		}
		SimpleEvent_(g_date->Date() + 10, dcpt_May_pest2, false, a_farm, a_field);
		break;
	case dcpt_May_pest2:
			if (a_ev->m_lock || a_farm->DoIt(2) && (DCPT_MAY_PEST == 1)) //11.50% will spray twice
			{
				if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, dcpt_May_pest2, true, a_farm, a_field);
					break;
				}
			}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), dcpt_June_pest1, false, a_farm, a_field);
		break;
	case dcpt_June_pest1:
		if (a_ev->m_lock || a_farm->DoIt(12)) //9% will spray just once
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, dcpt_June_pest1, true, a_farm, a_field);
				break;
			}
			DCPT_JUNE_PEST = 1;
		}
		SimpleEvent_(g_date->Date() + 10, dcpt_June_pest2, false, a_farm, a_field);
		break;
	case dcpt_June_pest2:
		if (a_ev->m_lock || a_farm->DoIt(0) && (DCPT_JUNE_PEST == 1)) //0.41% will spray twice
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, dcpt_June_pest2, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), dcpt_July_pest1, false, a_farm, a_field);
		break;
	case dcpt_July_pest1:
		if (a_ev->m_lock || a_farm->DoIt(0)) //3% will spray just once
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, dcpt_July_pest1, true, a_farm, a_field);
				break;
			}
			DCPT_JULY_PEST = 1;
		}
		SimpleEvent_(g_date->Date() + 10, dcpt_July_pest2, false, a_farm, a_field);
		break;
	case dcpt_July_pest2:
		if (a_ev->m_lock || a_farm->DoIt(0) && (DCPT_JULY_PEST == 1)) //0.95% will spray twice
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, dcpt_July_pest2, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 8), dcpt_August_pest1, false, a_farm, a_field);
		break;
	case dcpt_August_pest1:
		if (a_ev->m_lock || a_farm->DoIt(1)) //0.02% will spray just once
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(31, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, dcpt_August_pest1, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 9), dcpt_September_pest1, false, a_farm, a_field);
		break;
	case dcpt_September_pest1:
		if (a_ev->m_lock || a_farm->DoIt(3)) //2.90% will spray just once
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, dcpt_August_pest1, true, a_farm, a_field);
				break;
			}
			DCPT_SEPT_PEST = 1;
		}
		SimpleEvent_(g_date->Date() + 10, dcpt_September_pest2, false, a_farm, a_field);
		break;
	case dcpt_September_pest2:
		if (a_ev->m_lock || a_farm->DoIt(21) && (DCPT_SEPT_PEST == 1)) //2.90% will spray just once
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, dcpt_August_pest1, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 10), dcpt_October_pest1, false, a_farm, a_field);
		break;
	case dcpt_October_pest1:
		if (a_ev->m_lock || a_farm->DoIt(4)) //1% will spray just once
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, dcpt_October_pest1, true, a_farm, a_field);
				break;
			}
			DCPT_OCT_PEST = 1;
		}
		SimpleEvent_(g_date->Date() + 10, dcpt_October_pest2, false, a_farm, a_field);
		break;
	case dcpt_October_pest2:
		if (a_ev->m_lock || a_farm->DoIt(1) && (DCPT_OCT_PEST == 1)) //1% will spray just once
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, dcpt_October_pest1, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 11), dcpt_November_pest1, false, a_farm, a_field);
		break;
	case dcpt_November_pest1:
		if (a_ev->m_lock || a_farm->DoIt(0)) //1% will spray just once
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, dcpt_October_pest1, true, a_farm, a_field);
				break;
			}
		}
		done = true;
		break;

	default:
		g_msg->Warn( WARN_BUG, "DummyCropPestTesting::Do(): "
			"Unknown event type! ", "" );
		exit( 1 );
	}
	return done;
}
//---------------------------------------------------------------------------
