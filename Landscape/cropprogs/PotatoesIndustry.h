//
// Potatoes.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
#ifndef POTATOESIND_H
#define POTATOESIND_H

#define POTATOESIND_BASE 16950
#define POT_SLURRY_DATE m_field->m_user[0]
#define POT_HERBI_DATE  m_field->m_user[1]
#define POT_STRIG_DATE  m_field->m_user[2]
#define POT_HILL_DATE   m_field->m_user[3]
#define POT_DID_TREAT   m_field->m_user[4]
#define POT_DID_HILL    m_field->m_user[5]
#define POT_WATER_DATE  m_field->m_user[6]
#define POT_FUNGI_DATE  m_field->m_user[7]

typedef enum {
  pi_start = 1, // Compulsory, start event must always be 1 (one).
  pi_autumn_plough = POTATOESIND_BASE,
  pi_spring_plough,
  pi_spring_harrow,
  pi_fa_slurry,
  pi_spring_sow,
  pi_fa_npk,
  pi_fp_npk,
  pi_herbi_one,
  pi_herbi_two,
  pi_herbi_three,
  pi_strigling_one,
  pi_strigling_two,
  pi_strigling_three,
  pi_hilling,
  pi_insecticide,
  pi_water_one,
  pi_water_two,
  pi_water_three,
  pi_fungi_one,
  pi_fungi_two,
  pi_fungi_three,
  pi_fungi_four,
  pi_fungi_five,
  pi_growth_reg,
  pi_harvest
} PotatoesIndToDo;



class PotatoesIndustry: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  PotatoesIndustry()
  {
    m_first_date=g_date->DayInYear(1,11);
  }
};

#endif // POTATOESIND_H
