//
// OrchardCrop.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OrchardCrop.h"

//is this ok?
extern CfgInt cfg_pest_productapplic_startdate;
extern CfgInt cfg_pest_productapplic_startdate2;
extern CfgInt cfg_productapplicstartyear;
extern CfgInt cfg_productapplicendyear;
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_pest_product_1_amount;

//declared in elements.cpp
extern CfgInt cfg_OrchardNoCutsDay;  


bool OrchardCrop::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  bool done = false;
  int d1;
  int today = g_date->Date();
  int noDates= 1;
  int sprayday=cfg_pest_productapplic_startdate.value();

  switch ( m_ev->m_todo ) {
  case orch_start:
      // Set up the date management stuff
      m_last_date=g_date->DayInYear(1,9);
      // Start and stop dates for all events after harvest
      m_field->SetMDates(0,0,g_date->DayInYear(1,9));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(1,9));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary

	//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
	 if(!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){

      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday){
          g_msg->Warn( WARN_BUG, "OrchardCrop::Do(): Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++) {
			if(m_field->GetMDates(0,i)>=m_ev->m_startday) { 
				m_field->SetMDates(0,i,m_ev->m_startday-1); //move the starting date
			}
			if(m_field->GetMDates(1,i)>=m_ev->m_startday){
				m_field->SetMConstants(i,0); 
				m_field->SetMDates(1,i,m_ev->m_startday-1); //move the finishing date
			}
		}
      }
      // Now no operations can be timed after the start of the next crop.

      d1 = g_date->OldDays() + m_first_date+365; // Add 365 for spring crop
      if (today > d1) {
          // Yes too late - should not happen - raise an error
          g_msg->Warn( WARN_BUG, " OrchardCrop::Do(): Crop start attempt after last possible start date", "" );
          exit( 1 );
      }
	}//if
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays()+m_first_date;;
      if ( ! m_ev->m_first_year ) d1+=365; // Add 365 for spring crop (not 1st yr)
      if ( g_date->Date() > d1 ){
       d1 = g_date->Date();
      }
    // OK, let's go.
    
	SimpleEvent( d1, orch_cut, false );   //call cutting orchard
    SimpleEvent( d1, orch_insecticide1, false );  //call insecticide - so there are two threds
	break;
    
	
  case orch_cut: 
	
	switch ( cfg_OrchardNoCutsDay.value() ) {
    case 99:
      if ( today == ( sprayday - 7 ) ) m_farm->CutOrch( m_field, 0.0, -1); //so cut no matter what. ok?
    break;
    case 4:
      if ( ( today == 259 ) || ( today == 122 ) || ( today == 92 ) || ( today == 196 ) )
        m_farm->CutOrch( m_field, 0.0, -1);
    break;
    case 3:
      if ( ( today == 259 ) || ( today == 122 ) || ( today == 92 ) ) m_farm->CutOrch( m_field, 0.0, -1);
    break;
    case 2:
      if ( ( today == 259 ) || ( today == 122 ) ) m_farm->CutOrch( m_field, 0.0, -1);
    break;
    case 1:
      if ( ( today == 259 ) ) m_farm->CutOrch( m_field, 0.0, -1);
    break;
    default: // No cut
    break;
  }
	  
	 /* if (!m_farm->CutOrch( m_field, 0.0, g_date->DayInYear( 30, 7 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, psa_cut_to_hay, false );
      break;
    }*/
	SimpleEvent( g_date->Date(), orch_do_nothing_start, false );
	break;  
 
  case orch_do_nothing_start:
	SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 9 ), orch_do_nothing_stop, false );
    break;

  case orch_do_nothing_stop:
    done = true;
    break;

  case orch_insecticide1:
	  // This will prevent product usage outside the test period
	  if (g_landscape_p->SupplyYearNumber() < cfg_productapplicstartyear.value()) break;
	  if (g_landscape_p->SupplyYearNumber() >= cfg_productapplicendyear.value()) break;
    if ( m_ev->m_lock || m_farm->DoIt( (int) (100*cfg_ins_app_prop1.value() ))) {
		if (!m_farm->ProductApplication( m_field, 0.0, cfg_pest_productapplic_startdate.value() - g_date->DayInYear(), cfg_pest_product_1_amount.value(), ppp_1)) {
			SimpleEvent( g_date->Date() + 1, orch_insecticide1, true );
			break;
		}
		else {
			if ((cfg_pest_productapplic_startdate.value()<366) && ( cfg_pest_productapplic_startdate.value() <-1)) 
				SimpleEvent( g_date->OldDays() + cfg_pest_productapplic_startdate2.value(), orch_insecticide2, false );
			break;
		}
    }
    break;

  case orch_insecticide2:
    if ( m_ev->m_lock || m_farm->DoIt( (int)( 50*cfg_ins_app_prop1.value() ))) {
      if (!m_farm->ProductApplication( m_field, 0.0, cfg_pest_productapplic_startdate2.value() - g_date->DayInYear(), cfg_pest_product_1_amount.value(), ppp_1)) {
        SimpleEvent( g_date->Date() + 1, orch_insecticide2, true );
        break;
      }
    }
    // End of thread

	ChooseNextCrop (1);


    break;



  default:
    g_msg->Warn( WARN_BUG, "OrchardCrop::Do(): Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


