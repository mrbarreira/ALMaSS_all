//
// SpringBarleyCloverGrass.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/SpringBarleyCloverGrass.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgBool cfg_pest_springbarley_on;
extern CfgBool cfg_pest_winterwheat_on;
extern CfgInt 	cfg_SB_InsecticideDay;
extern CfgInt   cfg_SB_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;



bool SpringBarleyCloverGrass::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev ) {
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	int d1 = 0;
	int noDates = 3;
	bool done = false;

	switch (m_ev->m_todo) {
		case sbcg_start:
		{
			SBCG_ISAUTUMNPLOUGH = false;
			SBCG_FERTI_DONE = false;
			SBCG_SPRAY = 0;
			// Set up the date management stuff
			// Could save the start day in case it is needed later
			// m_field->m_startday = m_ev->m_startday;
			m_last_date = g_date->DayInYear( 15, 10 );
			// Start and stop dates for all events after harvest
			m_field->SetMDates( 0, 0, g_date->DayInYear( 1, 8 ) );
			// Determined by harvest date - used to see if at all possible
			m_field->SetMDates( 1, 0, g_date->DayInYear( 25, 8 ) );
			m_field->SetMDates( 0, 1, g_date->DayInYear( 5, 8 ) );
			m_field->SetMDates( 1, 1, g_date->DayInYear( 30, 8 ) );
			m_field->SetMDates( 0, 2, g_date->DayInYear( 1, 9 ) );
			m_field->SetMDates( 1, 2, g_date->DayInYear( 15, 10 ) );
			// Check the next crop for early start, unless it is a spring crop
			// in which case we ASSUME that no checking is necessary!!!!
			// SO DO NOT implement a crop that runs over the year boundary
			if (m_ev->m_startday > g_date->DayInYear( 1, 7 )) {
				if (m_field->GetMDates( 0, 0 ) >= m_ev->m_startday) {
					g_msg->Warn( WARN_BUG, "SpringBarleyCloverGrass::Do(): "
						"Harvest too late for the next crop to start!!!", "" );
					exit( 1 );
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (m_field->GetMDates( 0, i ) >= m_ev->m_startday)
						m_field->SetMDates( 0, i, m_ev->m_startday - 1 );
					if (m_field->GetMDates( 1, i ) >= m_ev->m_startday)
						m_field->SetMDates( 1, i, m_ev->m_startday - 1 );
				}
			}
			// Now no operations can be timed after the start of the next crop.
			if (!m_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear( 1, 7 );
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					g_msg->Warn( WARN_BUG, "SpringBarleyCloverGrass::Do(): "
						"Crop start attempt between 1st Jan & 1st July", "" );
					exit( 1 );
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // start day
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn( WARN_BUG, "SpringBarleyCloverGrass::Do(): "
							"Crop start attempt after last possible start date", "" );
						exit( 1 );
					}
				}
			}
			else {
				m_field->SetLastSownVeg( tov_SpringBarleyCloverGrass ); //Force last sown, needed for goose habitat classification
				SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 3 ),sbcg_spring_plough, false );
				break;
			}
			// End single block date checking code. Please see next line
			// comment as well.
			// Reinit d1 to first possible starting date.
			d1 = g_date->OldDays() + g_date->DayInYear( 1, 11 );
			if (g_date->Date() >= d1) d1 = g_date->Date();
			SimpleEvent( d1, sbcg_autumn_plough, false );
		}
		break;

		case sbcg_autumn_plough:
			if (m_ev->m_lock || m_farm->DoIt( 70 )) {
				if (!m_farm->AutumnPlough( m_field, 0.0,
					g_date->DayInYear( 30, 11 ) - g_date->DayInYear() )) { // 30,11 is m_first_date
					SimpleEvent( g_date->Date() + 1, sbcg_autumn_plough, true );
					break;
				}
				SBCG_ISAUTUMNPLOUGH = true;
			}
			// +365 for next year
			SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 3 ) + 365,sbcg_ferti_s1, false );
			SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 3 ) + 365,sbcg_ferti_s2, false );
			break;

		case sbcg_ferti_s1:
			if (m_ev->m_lock || m_farm->DoIt( 90 )) {
				if (!m_farm->FA_Slurry( m_field, 0.0,
					g_date->DayInYear( 15, 4 ) - g_date->DayInYear() )) {
					SimpleEvent( g_date->Date() + 1, sbcg_ferti_s1, true );
					break;
				}
				// Done fertilizer so remember
				SBCG_FERTI_DONE = true;
			}
			SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 3 ),
				sbcg_spring_plough, false );
			break;

		case sbcg_ferti_s2:
			if ((m_ev->m_lock || m_farm->DoIt( 67 )) && (!SBCG_ISAUTUMNPLOUGH)) {
				if (!m_farm->FA_Manure( m_field, 0.0,
					g_date->DayInYear( 15, 4 ) - g_date->DayInYear() )) {
					SimpleEvent( g_date->Date() + 1, sbcg_ferti_s2, true );
					break;
				}
				else {
					// Done fertilizer so remember
					SBCG_FERTI_DONE = true;
				}
			}
			break;

		case sbcg_spring_plough:
			if (!SBCG_ISAUTUMNPLOUGH) // Don't plough if you have already
			{
				if (!m_farm->SpringPlough( m_field, 0.0,
					g_date->DayInYear( 10, 4 ) - g_date->DayInYear() )) {
					SimpleEvent( g_date->Date() + 1, sbcg_spring_plough, true );
					break;
				}
			}
			SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25, 3 ),
				sbcg_spring_harrow, false );
			break;

		case sbcg_spring_harrow:
			if (!m_farm->SpringHarrow( m_field, 0.0,
				g_date->DayInYear( 10, 4 ) - g_date->DayInYear() )) {
				SimpleEvent( g_date->Date() + 1, sbcg_spring_harrow, true );
				break;
			}
			SimpleEvent( g_date->Date(), sbcg_ferti_s3, false );
			break;

		case sbcg_ferti_s3: // The catch all
			if ((!SBCG_FERTI_DONE) || (m_ev->m_lock || m_farm->DoIt( 70 ))) {
				if (!m_farm->FA_NPK( m_field, 0.0,
					g_date->DayInYear( 10, 4 ) - g_date->DayInYear() )) {
					SimpleEvent( g_date->Date() + 1, sbcg_ferti_s3, true );
					break;
				}
			}
			SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25, 3 ),
				sbcg_spring_sow, false );
			break;

		case sbcg_spring_sow:
			if (!m_farm->SpringSow( m_field, 0.0,
				g_date->DayInYear( 20, 4 ) - g_date->DayInYear() )) {
				SimpleEvent( g_date->Date() + 1, sbcg_spring_sow, true );
				break;
			}
			SimpleEvent( g_date->Date() + 1, sbcg_spring_roll, false );
			break;

		case sbcg_spring_roll:
			if (m_ev->m_lock || m_farm->DoIt( 90 )) {
				if (!m_farm->SpringRoll( m_field, 0.0,
					g_date->DayInYear( 21, 4 ) - g_date->DayInYear() )) {
					SimpleEvent( g_date->Date() + 1, sbcg_spring_roll, true );
					break;
				}
			}
			SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 5 ),
				sbcg_herbicide, false );
			break;

		case sbcg_herbicide:
			if (m_ev->m_lock || m_farm->DoIt( (int)(80 * cfg_herbi_app_prop.value()) ))   // was 60
			{
				if (!m_farm->HerbicideTreat( m_field, 0.0,
					g_date->DayInYear( 10, 5 ) - g_date->DayInYear() )) {
					SimpleEvent( g_date->Date() + 1, sbcg_herbicide, true );
					break;
				}
				// Did first spray so see if should do another, 7 days later (min)
				else if (g_date->DayInYear() + 7 < g_date->DayInYear( 15, 5 ))
					SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 5 ),
					sbcg_herbicide2, false );
				else SimpleEvent( g_date->Date() + 7, sbcg_herbicide2, false );
			}
			// Carry on with the next after first treatment or no treatment
			SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 5 ),
				sbcg_GR, false );
			SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 5 ),
				sbcg_fungicide, false );
			SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 6 ),
				sbcg_water1, false );
			break;

		case sbcg_herbicide2:
			if (m_ev->m_lock || m_farm->DoIt( (int)(50 * cfg_herbi_app_prop.value()) ))      // was 67
			{
				if (!m_farm->HerbicideTreat( m_field, 0.0,
					g_date->DayInYear( 30, 5 ) - g_date->DayInYear() )) {
					SimpleEvent( g_date->Date() + 1, sbcg_herbicide2, true );
					break;
				}
			}
			break;

			// GR Thread
		case sbcg_GR:
			if (m_ev->m_lock || m_farm->DoIt( (int)(5 * cfg_greg_app_prop.value()) ))      // was 60
			{
				if (!m_farm->GrowthRegulator( m_field, 0.0,
					g_date->DayInYear( 25, 5 ) - g_date->DayInYear() )) {
					SimpleEvent( g_date->Date() + 1, sbcg_GR, true );
					break;
				}
			}
			break;

			// Water thread
		case sbcg_water1:
			if (m_ev->m_lock || m_farm->DoIt( 20 )) {
				if ((!m_farm->Water( m_field, 0.0,
					g_date->DayInYear( 15, 6 ) - g_date->DayInYear() ))
					|| (SBCG_SPRAY == g_date->DayInYear())) {
					SimpleEvent( g_date->Date() + 1, sbcg_water1, true );
					break;
				}
				if (g_date->DayInYear() + 5 < g_date->DayInYear( 16, 6 ))
					SimpleEvent( g_date->OldDays() + g_date->DayInYear( 16, 6 ),
					sbcg_water2, false );
				else
					SimpleEvent( g_date->Date() + 5, sbcg_water2, false );
			}
			break;

		case sbcg_water2:
			if (m_ev->m_lock || m_farm->DoIt( 50 )) {
				if ((!m_farm->Water( m_field, 0.0,
					g_date->DayInYear( 1, 7 ) - g_date->DayInYear() ))
					|| (SBCG_SPRAY == g_date->DayInYear())) {
					SimpleEvent( g_date->Date() + 1, sbcg_water2, true );
					break;
				}
			}
			break;

			// Fungicide thread  & MAIN THREAD
		case sbcg_fungicide:
			if (m_ev->m_lock || m_farm->DoIt( (int)(40 * cfg_herbi_app_prop.value()) ))    // was 60
			{
				if (!m_farm->FungicideTreat( m_field, 0.0,
					g_date->DayInYear( 25, 5 ) - g_date->DayInYear() )) {
					SimpleEvent( g_date->Date() + 1, sbcg_fungicide, true );
					break;
				}
				else {
					SBCG_SPRAY = g_date->DayInYear();
				}
			}
			// can also try insecticide
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(cfg_SB_InsecticideDay.value(), cfg_SB_InsecticideMonth.value()), 
			sbcg_insecticide1, false );
			break;

			// Insecticide & MAIN THREAD
		case sbcg_insecticide1:
			if (m_ev->m_lock || m_farm->DoIt( (int)(35 * cfg_ins_app_prop1.value()) ))    // was 50
			{
				// Here we check wheter we are using ERA pesticde or not
				if (!cfg_pest_springbarley_on.value() || !g_landscape_p->SupplyShouldSpray()) {
					if (!m_farm->InsecticideTreat( m_field, 0.0, g_date->DayInYear( 10, 6 ) - g_date->DayInYear() )) {
						SimpleEvent( g_date->Date() + 1, sbcg_insecticide1, true );
						break;
					}
					else SBCG_SPRAY = g_date->DayInYear();
				}
				else {
					// Using test pesticide
					m_farm->ProductApplication_DateLimited(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
					SBCG_SPRAY = g_date->DayInYear();
				}
			}
			ChooseNextCrop(2);
			SimpleEvent(g_date->Date() + 14, sbcg_insecticide2, false);
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), sbcg_harvest, false);
			break;

		case sbcg_insecticide2:
			if (m_ev->m_lock || m_farm->DoIt((int)floor(0.5 + 35 * cfg_ins_app_prop2.value()*m_farm->Prob_multiplier()))) //modified probability
			{
				if (cfg_pest_springbarley_on.value() && g_landscape_p->SupplyShouldSpray())
				{
					m_farm->ProductApplication_DateLimited(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
				}
				else
				{
					if (!m_farm->InsecticideTreat(m_field, 0.0, SBCG_SPRAY + 28 - g_date->DayInYear())) {
						SimpleEvent(g_date->Date() + 1, sbcg_insecticide2, true);
					}
				}
			}
			SimpleEvent(g_date->Date() + 14, sbcg_insecticide3, false);
			break;

		case sbcg_insecticide3:
			if (m_ev->m_lock || m_farm->DoIt((int)floor(0.5 + 35 * cfg_ins_app_prop3.value()*m_farm->Prob_multiplier()))) //modified probability
			{
				if (cfg_pest_springbarley_on.value() && g_landscape_p->SupplyShouldSpray())
				{
					m_farm->ProductApplication_DateLimited(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
				}
				else
				{
					if (!m_farm->InsecticideTreat(m_field, 0.0, SBCG_SPRAY + 28 - g_date->DayInYear())) {
						SimpleEvent(g_date->Date() + 1, sbcg_insecticide3, true);
					}
				}
			}
			break;

		case sbcg_harvest:
			if (!m_farm->Harvest( m_field, 0.0,
				m_field->GetMDates( 1, 0 ) - g_date->DayInYear() )) {
				SimpleEvent( g_date->Date() + 1, sbcg_harvest, true );
				break;
			}
			SimpleEvent( g_date->OldDays() + m_field->GetMDates( 0, 1 ),
				sbcg_hay_baling, false );
			break;

		case sbcg_hay_baling:
			if (!m_farm->HayBailing( m_field, 0.0,
				m_field->GetMDates( 1, 1 ) - g_date->DayInYear() )) {
				SimpleEvent( g_date->Date() + 1, sbcg_hay_baling, true );
				break;
			}
			// Something special here.
			// If the cattle out period is very short then we don't want to do it at all
			if (m_field->GetMDates( 1, 2 ) - m_field->GetMDates( 0, 2 ) < 14) done = true;
			else SimpleEvent( g_date->OldDays() + m_field->GetMDates( 0, 2 ),
				sbcg_cattle_out, false );
			break;

		case sbcg_cattle_out:
			if (!m_farm->CattleOut( m_field, 0.0,
				m_field->GetMDates( 1, 2 ) - g_date->DayInYear() )) {
				SimpleEvent( g_date->Date() + 1, sbcg_cattle_out, true );
				break;
			}
			SimpleEvent( g_date->Date() + 1, sbcg_cattle_is_out, false );
			break;

		case sbcg_cattle_is_out:
			if (!m_farm->CattleIsOut( m_field, 0.0,
				m_field->GetMDates( 1, 2 ) - g_date->DayInYear(), m_field->GetMDates( 1, 2 ) )) {
				SimpleEvent( g_date->Date() + 1, sbcg_cattle_is_out, false );
				break;
			}
			// END OF MAIN THREAD
			done = true;
			break;

		default:
			g_msg->Warn( WARN_BUG, "SpringBarleyCloverGrass::Do(): "
				"Unknown event type! ", "" );
			exit( 1 );
	}
	return done;
}


