//
// SpringBarleySilage.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef SpringBarleySilage_H
#define SpringBarleySilage_H

#define SBARLEYSILAGE_BASE 4900
#define SBS_ISAUTUMNPLOUGH    m_field->m_user[0]
#define SBS_FERTI_DONE        m_field->m_user[1]
#define SBS_SPRAY             m_field->m_user[2]
#define SBS_MANURE_DONE       m_field->m_user[3]
#define SBS_NPK_DONE          m_field->m_user[4]
#define SBS_INSECT_DATE       m_field->m_user[5]

typedef enum {
  sbs_start = 1, // Compulsory, start event must always be 1 (one).
  sbs_ferti_p1 = SBARLEYSILAGE_BASE,
  sbs_ferti_p2,
  sbs_ferti_p3,
  sbs_ferti_s1,
  sbs_ferti_s2,
  sbs_ferti_s3,
  sbs_ferti_s4,
  sbs_harvest1,
  sbs_harvest2,
  sbs_spring_plough,
  sbs_autumn_plough,
  sbs_spring_harrow,
  sbs_spring_roll,
  sbs_spring_sow,
  sbs_hay_baling,
  sbs_strigling1,
  sbs_strigling2,
  sbs_straw_chopping,
  sbs_GR,
  sbs_water1,
  sbs_water2,
  sbs_herbicide1,
  sbs_herbicide2,
  sbs_fungicide1,
  sbs_fungicide2,
  sbs_stubble_harrow,
  sbs_insecticide1,
  sbs_insecticide2,
  sbs_insecticide3,
  sbs_wait
} SBSToDo;



class SpringBarleySilage: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  SpringBarleySilage()
  {
      m_first_date=g_date->DayInYear(30,11);
  }

};

#endif // SpringBarleySilage_H
