/**
\file
\brief
<B>NLSpringBarley.h This file contains the headers for the SpringBarley class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLSpringBarley.h
//


#ifndef NLSPRINGBARLEY_H
#define NLSPRINGBARLEY_H

#define NLSPRINGBARLEY_BASE 25400
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_SB_FUNGII	a_field->m_user[1]


/** Below is the list of things that a farmer can do if he is growing spring barley, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_sb_start = 1, // Compulsory, must always be 1 (one).
	nl_sb_sleep_all_day = NLSPRINGBARLEY_BASE,
	nl_sb_stubble_harrow_sandy,
	nl_sb_ferti_p1_sandy,
	nl_sb_ferti_s1_sandy,
	nl_sb_ferti_p2_sandy,
	nl_sb_ferti_s2_sandy,
	nl_sb_spring_plough_sandy,
	nl_sb_stubble_harrow_clay,
	nl_sb_winter_plough_clay,
	nl_sb_ferti_p2_clay,
	nl_sb_ferti_s2_clay,
	nl_sb_ferti_p3,
	nl_sb_ferti_s3,
	nl_sb_preseeding_cultivator,
	nl_sb_preseeding_cultivator_sow,
	nl_sb_spring_sow,
	nl_sb_harrow,
	nl_sb_ferti_p4_clay,
	nl_sb_ferti_s4_clay,
	nl_sb_herbicide1,
	nl_sb_fungicide1,
	nl_sb_fungicide2,
	nl_sb_insecticide1,
	nl_sb_growth_regulator1,
	nl_sb_harvest,
	nl_sb_straw_chopping,
	nl_sb_hay_bailing,
} NLSpringBarleyToDo;


/**
\brief
NLSpringBarley class
\n
*/
/**
See NLSpringBarley.h::NLSpringBarleyToDo for a complete list of all possible events triggered codes by the spring barley management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLSpringBarley: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLSpringBarley()
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,10 );
   }
};

#endif // NLSPRINGBARLEY_H

