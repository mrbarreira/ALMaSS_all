/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>NLCatchPeaCrop.cpp This file contains the source for the NLCatchPeaCrop class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// NLCatchPeaCrop.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/NLCatchPeaCrop.h"

extern CfgBool  g_farm_fixed_crop_enable;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional CatchPeaCrop.
*/
bool NLCatchPeaCrop::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code
	switch (a_ev->m_todo)
	{
	case nl_cpc_start:
	{

		// Set up the date management stuff
		
		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "NLCatchPeaCrop::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = g_landscape_p->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				// We need to skip this test if its a catch crop that preceded
				
				if ((g_date->Date() < d1) && (!g_farm_fixed_crop_enable.value())) { // condition preventing from getting errors regarding looping catch crop 
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "NLCatchPeaCrop::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = g_landscape_p->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousCrop(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = g_landscape_p->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "NLCatchPeaCrop::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						a_field->GetOwner()->GetPreviousCrop(a_field->GetRotIndex());
						int almassnum = g_landscape_p->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 8), nl_cpc_stubble_cultivator, false, a_farm, a_field);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 8);
		// OK, let's go.
		// Here we queue up the first event - this differs depending on whether we have a
		// stock or arable farmer
		SimpleEvent_(d1, nl_cpc_stubble_cultivator, false, a_farm, a_field);
	}
	break;

	// This is the first real farm operation
	case nl_cpc_stubble_cultivator:
		if (!a_farm->StubbleHarrowing(a_field, 0.0, g_date->DayInYear(10, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_cpc_stubble_cultivator, true, a_farm, a_field);
			break;
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 5, nl_cpc_ferti_s1, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->Date() + 5, nl_cpc_ferti_p1, false, a_farm, a_field);
		break;

	case nl_cpc_ferti_p1:
		if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_cpc_ferti_p1, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_cpc_preseeding_cultivator_with_sow, false, a_farm, a_field);
		break;
	case nl_cpc_ferti_s1:
		if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_cpc_ferti_s1, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_cpc_preseeding_cultivator_with_sow, false, a_farm, a_field);
		break;
	case nl_cpc_preseeding_cultivator_with_sow:
		if (!a_farm->PreseedingCultivatorSow(a_field, 0.0, g_date->DayInYear(16, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_cpc_preseeding_cultivator_with_sow, true, a_farm, a_field);
			break;
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
		}
		if (a_field->GetSoilType() == 2 || a_field->GetSoilType() == 6) { // on sandy soils (NL ZAND & LOSS)
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 2) + 365, nl_cpc_ferti_s2_sandy, false, a_farm, a_field);
		}
		else SimpleEvent_(d1, nl_cpc_ferti_s2_clay, false, a_farm, a_field);
		break;
	case nl_cpc_ferti_s2_clay:
		if (a_farm->IsStockFarmer())
		{
			if (!a_farm->FA_Manure(a_field, 0.0, g_date->DayInYear(25, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_cpc_ferti_s2_clay, true, a_farm, a_field);
				break;
			}
			d1 = g_date->Date() + 40;
			if (d1 < g_date->OldDays() + g_date->DayInYear(15, 10)) {
				d1 = g_date->OldDays() + g_date->DayInYear(15, 10);
			}
			SimpleEvent_(d1, nl_cpc_winter_plough_clay, false, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_cpc_ferti_p2_clay, false, a_farm, a_field);
		break;
	case nl_cpc_ferti_p2_clay:
		if (!a_farm->FP_Manure(a_field, 0.0, g_date->DayInYear(25, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_cpc_ferti_p2_clay, true, a_farm, a_field);
			break;
		}
		d1 = g_date->Date() + 40;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 10)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 10);
		}
		SimpleEvent_(d1, nl_cpc_winter_plough_clay, false, a_farm, a_field);
		break;
	case nl_cpc_winter_plough_clay:
		if (!a_farm->WinterPlough(a_field, 0.0, g_date->DayInYear(15, 12) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_cpc_winter_plough_clay, true, a_farm, a_field);
			break;
		}
		// The plan is finished on clay soils (catch crop in incorporated into soil on autumn)
		// Calling sleep_all_day to move to the next year 
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 1) + 365, nl_cpc_sleep_all_day, false, a_farm, a_field);
		break;
	case nl_cpc_ferti_s2_sandy:
		if (a_farm->IsStockFarmer())
		{
			if (!a_farm->FA_Manure(a_field, 0.0, g_date->DayInYear(15, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_cpc_ferti_s2_sandy, true, a_farm, a_field);
				break;
			}
			// The plan is finished on sandy soils; spring plough (to incorporate catch crop into soil) will follow but it is called from the next crop 
			done = true;
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_cpc_ferti_p2_sandy, false, a_farm, a_field);
		break;
	case nl_cpc_ferti_p2_sandy:
		if (!a_farm->FP_Manure(a_field, 0.0, g_date->DayInYear(15, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_cpc_ferti_p2_sandy, true, a_farm, a_field);
			break;
		}
		// The plan is finished on sandy soils; spring plough (to incorporate catch crop into soil) will follow but it is called from the next crop 
		done = true;
		break;
	case nl_cpc_sleep_all_day:
		if (!a_farm->SleepAllDay(a_field, 0.0, g_date->DayInYear(15, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_cpc_sleep_all_day, true, a_farm, a_field);
			break;
		}
		done = true;
		break;


	default:
		g_msg->Warn(WARN_BUG, "NLCatchPeaCrop::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}