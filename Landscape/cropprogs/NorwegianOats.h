//
// NorwegianOats.h
//
/* 
*******************************************************************************************************
Copyright (c) 2016, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef NorwegianOats_H
#define NorwegianOats_H

#define NO_OATS_BASE 10020
#define NO_OATS_DID_AUTUMN_PLOUGH m_field->m_user[0]

typedef enum {
  no_oats_start = 1, // Compulsory, start event must always be 1 (one).
  no_oats_autumn_plough = NO_OATS_BASE,
  no_oats_spring_plough,
  no_oats_spring_harrow,
  no_oats_spring_sow,
  no_oats_spring_roll,
  no_oats_harvest,
  no_oats_haybailing,
} NO_OatsToDo;



class NorwegianOats : public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  NorwegianOats()
  {
      m_first_date=g_date->DayInYear(7,9);
	  m_ddegstoharvest = 1400; // this is hard coded for each crop that uses this 
  }
};

#endif // NorwegianOats_H
