//
// FieldPeas.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/FieldPeas.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;

bool FieldPeas::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  double ins_app_prop=cfg_ins_app_prop1.value();
  double herbi_app_prop=cfg_herbi_app_prop.value();

  bool done = false;

  switch ( m_ev->m_todo )
  {
  case fp_start:
    {
      FPEAD_INSECT_DATE = 0;
      FPEAD_FUNGI_DATE = 0;
      FPEAD_WATER_DATE = 0;
	  FPEAS_DECIDE_TO_HERB=1;
      // Set up the date management stuff
      // Could save the start day in case it is needed later
      // m_field->m_startday = m_ev->m_startday;
      m_last_date=g_date->DayInYear(15,10);
      // Start and stop dates for all events after harvest
      int noDates=1;
      m_field->SetMDates(0,0,g_date->DayInYear(30,8));
      // 0,0 determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(30,8));
    // Check the next crop for early start, unless it is a spring crop
    // in which case we ASSUME that no checking is necessary!!!!
    // So DO NOT implement a crop that runs over the year boundary

	//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
	int d1;
	 if(!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){

    if (m_ev->m_startday>g_date->DayInYear(1,7))
    {
      if (m_field->GetMDates(0,0) >=m_ev->m_startday)
      {
        //g_msg->Warn( WARN_BUG, "FieldPeas::Do(): Harvest too late for the next crop to start!!!", "" );

		char veg_type[ 20 ];
		sprintf( veg_type, "%d", m_ev->m_next_tov );
		g_msg->Warn( WARN_FILE, "FieldPeas::Do(): Harvest too late for the next crop to start!!! The next crop is: ", veg_type );

        exit( 1 );
      }
      // Now fix any late finishing problems
      for (int i=0; i<noDates; i++) {
			if(m_field->GetMDates(0,i)>=m_ev->m_startday) { 
				m_field->SetMDates(0,i,m_ev->m_startday-1); //move the starting date
			}
			if(m_field->GetMDates(1,i)>=m_ev->m_startday){
				m_field->SetMConstants(i,0); 
				m_field->SetMDates(1,i,m_ev->m_startday-1); //move the finishing date
			}
		}
    }
      // Now no operations can be timed after the start of the next crop.

      // CJT note:
      // Start single block date checking code to be cut-'n-pasted...

      if ( ! m_ev->m_first_year )
      {
	// Are we before July 1st?
	d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
	if (g_date->Date() < d1)
        {
	  // Yes, too early. We assumme this is because the last crop was late
          g_msg->Warn( WARN_BUG, "FieldPeas::Do(): "
		 "Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
	}
        else
        {
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (g_date->Date() > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "FieldPeas::Do(): Crop start attempt after last possible start date", "" ); 
			  exit( 1 );
          }
        }
      }
      else
      {
        // If this is the first year of running then it is possible to start
        // on day 0, so need this to tell us what to do:
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,3 ),
             fp_spring_plough, false );
        break;
      }
	}//if
      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear( 1,10 );
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      // OK, let's go.
      SimpleEvent( d1, fp_autumn_plough, false );
    }
    break;

  case fp_autumn_plough:
    if ( m_ev->m_lock || m_farm->DoIt( 90 )) {
      if (!m_farm->AutumnPlough( m_field, 0.0,
				 g_date->DayInYear( 1, 12 ) -
				 g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, fp_autumn_plough, true );
	break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,3 ) + 365,
		 fp_spring_plough, false );
    break;

  case fp_spring_plough:
    if ( m_ev->m_lock || m_farm->DoIt( 100 )) {
      if (!m_farm->SpringPlough( m_field, 0.0,
				 g_date->DayInYear( 1,4 ) -
				 g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, fp_spring_plough, true );
	break;
      }
    }
    {
      int d1 = g_date->Date()+1;
      int d2 = g_date->OldDays() + g_date->DayInYear( 5,3 );
      if ( d1 < d2 ) {
	d1 = d2;
      }
      SimpleEvent( d1, fp_spring_harrow, false );
    }
    break;

  case fp_spring_harrow:
    if (!m_farm->SpringHarrow( m_field, 0.0,
         g_date->DayInYear( 5,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, fp_spring_harrow, true );
      break;
    }
    {
      FieldPeasToDo dothis = fp_fertmanure_plant;
      if ( m_farm->IsStockFarmer()) {
	dothis = fp_fertmanure_stock;
      }

      int d1 = g_date->Date();
      int d2 = g_date->OldDays() + g_date->DayInYear( 20,3 );
      if ( d1 < d2 ) {
	d1 = d2;
      }
      SimpleEvent( d1, dothis, false );
    }
    break;

  case fp_fertmanure_plant:
    if (!m_farm->FP_Manure( m_field, 0.0,
			    g_date->DayInYear( 15,4 ) -
			    g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, fp_fertmanure_plant, true );
	break;
    }
    {
      int d1 = g_date->Date();
      int d2 = g_date->OldDays() + g_date->DayInYear( 25,3 );
      if ( d1 < d2 ) {
	d1 = d2;
      }
      SimpleEvent( d1, fp_spring_sow, false );
    }
    break;

  case fp_fertmanure_stock:
    if ( m_ev->m_lock || m_farm->DoIt( 75 )) {
      if (!m_farm->FA_Manure( m_field, 0.0,
			      g_date->DayInYear( 15,4 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, fp_fertmanure_stock, true );
	break;
      }
    }
    {
      int d1 = g_date->Date();
      int d2 = g_date->OldDays() + g_date->DayInYear( 25,3 );
      if ( d1 < d2 ) {
	d1 = d2;
      }
      SimpleEvent( d1, fp_spring_sow, false );
    }
    break;

  case fp_spring_sow:
    if (!m_farm->SpringSow( m_field, 0.0,
			    g_date->DayInYear( 15,4 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, fp_spring_sow, true );
      break;
    }
    SimpleEvent( g_date->Date(), fp_spring_roll, false );
    break;

  case fp_spring_roll:
    if (!m_farm->SpringRoll( m_field, 0.0,
			     g_date->DayInYear( 15,4 ) -
			     g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, fp_spring_roll, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,4 ),
                 fp_herbicide_one, false );
    break;

  case fp_herbicide_one:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (100*herbi_app_prop * m_farm->Prob_multiplier() ))) { //modified probability

		//new - for decision making
		TTypesOfVegetation tov = m_field->GetVegType();
		if(!m_ev->m_lock && !m_farm->Spraying_herbicides(tov)){
			Field * pf =  dynamic_cast<Field*>(m_field); 
			pf->Add_missed_herb_app();
			if(m_farm->DoIt(95)) pf->Add_missed_herb_app(); //the 2nd missed application
			FPEAS_DECIDE_TO_HERB=0;
		} //end of the part for dec. making
		else{
		  if (!m_farm->HerbicideTreat( m_field, 0.0, g_date->DayInYear( 20,4 ) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, fp_herbicide_one, true );
			break;
		  }
		}
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 25,4 );
      if ( d1 < g_date->Date() + 7 ) {
	d1 = g_date->Date() + 7;
      }
      SimpleEvent( d1, fp_herbicide_two, false );
    }
    break;

  case fp_herbicide_two:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (95*herbi_app_prop *FPEAS_DECIDE_TO_HERB * m_farm->Prob_multiplier()))) { //modified probability
      if (!m_farm->HerbicideTreat( m_field, 0.0,
				   g_date->DayInYear( 10,5 ) -
				   g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, fp_herbicide_two, true );
	break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,6 ),
		 fp_insecticide, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,6 ),
		 fp_fungicide, false );

    // Water carries the main thread.
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,6 ),
		 fp_water_one, false );
    break;

  case fp_fungicide:
    if ( m_ev->m_lock || m_farm->DoIt( (int)( 10*cfg_fungi_app_prop1.value() ))) {
     
		//new - for decision making
		TTypesOfVegetation tov = m_field->GetVegType();
		if(!m_ev->m_lock && !m_farm->Spraying_fungins(tov)){
			Field * pf =  dynamic_cast<Field*>(m_field); 
			pf->Add_missed_fi_app();
			break;
		} //end of the part for dec. making
		
		// Check for proximity of watering.
      {
		int d1 = g_date->Date();
		if ( d1 < FPEAD_WATER_DATE  + 2 ) {
		  // Too close, try tomorrow.
		  SimpleEvent( g_date->Date() + 1, fp_fungicide, true );
		  break;
		}
      }
      if (!m_farm->FungicideTreat( m_field, 0.0, g_date->DayInYear( 15,6 ) -  g_date->DayInYear())) {
		SimpleEvent( g_date->Date() + 1, fp_fungicide, true );
		break;
      }
      FPEAD_FUNGI_DATE = g_date->Date();
    }
    // End of thread.
    break;

  case fp_insecticide:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (90*ins_app_prop ))) {
      
		//new - for decision making
		TTypesOfVegetation tov = m_field->GetVegType();
		if(!m_ev->m_lock && !m_farm->Spraying_fungins(tov)){
			Field * pf =  dynamic_cast<Field*>(m_field); 
			pf->Add_missed_fi_app();
			break;
		} //end of the part for dec. making

		{
		int d1 = g_date->Date();
		if ( d1 < FPEAD_WATER_DATE + 2 ) {
		  // Too close, try tomorrow.
		  SimpleEvent( g_date->Date() + 1, fp_insecticide, true );
		  break;
		}
		}
      if (!m_farm->InsecticideTreat( m_field, 0.0,
				     g_date->DayInYear( 15,6 ) -
				     g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, fp_insecticide, true );
	break;
      }
      FPEAD_INSECT_DATE = g_date->Date();
    }
    // End of thread.
    break;

  case fp_water_one:
    if ( m_ev->m_lock || m_farm->DoIt( 30 ))
    {
      {
	int d1 = g_date->Date();
	if ( d1 < FPEAD_INSECT_DATE + 2 ||
	     d1 < FPEAD_FUNGI_DATE  + 2 ) {
	  // Too close, try tomorrow.
	  SimpleEvent( g_date->Date() + 1, fp_water_one, true );
	  break;
	}
      }
      if (!m_farm->Water( m_field, 0.0,
           g_date->DayInYear( 15,6 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, fp_water_one, true );
        break;
      }
      FPEAD_WATER_DATE = g_date->Date();
      // Did first water, so do the second one too.
      {
	int d1 = g_date->OldDays() + g_date->DayInYear( 16,6 );
	if ( d1 < g_date->Date() + 7 ) {
	  d1 = g_date->Date() + 7;
	}
	SimpleEvent( d1, fp_water_two, false );
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,8 ),
                  fp_growth_reg, false );
    break;

  case fp_water_two:
    {
      int d1 = g_date->Date();
      if ( d1 < FPEAD_INSECT_DATE + 2 ||
	   d1 < FPEAD_FUNGI_DATE  + 2 ) {
	// Too close, try tomorrow.
	SimpleEvent( g_date->Date() + 1, fp_water_two, true );
	break;
      }
    }
    if (!m_farm->Water( m_field, 0.0,
			g_date->DayInYear( 1,7 ) -
			g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, fp_water_two, true );
      break;
    }
    FPEAD_WATER_DATE = g_date->Date();
    // End of thread.
    break;

  case fp_growth_reg:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (20*cfg_greg_app_prop.value() )))
    {
      if (!m_farm->GrowthRegulator( m_field, 0.0,
				    g_date->DayInYear( 20,8 ) -
				    g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, fp_growth_reg, true );
        break;
      }
      // Did apply growth regulator, so wait seven days before
      // harvesting.

	  ChooseNextCrop (1);
      SimpleEvent( g_date->Date()+7, fp_harvest, false );
      break;
    }
    // No growth regulator, try harvesting today.
	ChooseNextCrop (1);
    SimpleEvent( g_date->Date(), fp_harvest, false );
    break;

  case fp_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
			  g_date->DayInYear( 30,8 ) -
			  g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, fp_harvest, true );
      break;
    }
    SimpleEvent( g_date->Date(), fp_straw_chopping, false );
    break;

  case fp_straw_chopping:
	if (m_field->GetMConstants(0)==0) {
		if (!m_farm->StrawChopping( m_field, 0.0, -1)) { //raise an error
			g_msg->Warn( WARN_BUG, "FieldPeas::Do(): failure in 'StrawChopping' execution", "" );
			exit( 1 );
		} 
	}
	else { 
		if (!m_farm->StrawChopping( m_field, 0.0, m_field->GetMDates(1,0) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date()+1, fp_straw_chopping, true );
			break;
		}
	}
    // End Main Thread
    done=true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "FieldPeas::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


