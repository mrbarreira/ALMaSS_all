//
// SpringBarleyStrigling.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef SpringBarleyStrigling_H
#define SpringBarleyStrigling_H

#define SBST_BASE 30100
#define SBST_SLURRY_DONE       m_field->m_user[0]
#define SBST_MANURE_DONE       m_field->m_user[1]
#define SBST_SLURRY_EXEC       m_field->m_user[2]
#define SBST_MANURE_EXEC       m_field->m_user[3]
#define SBST_DID_AUTUMN_PLOUGH m_field->m_user[4]

#define SBST_HERBI_DATE        m_field->m_user[0]
#define SBST_GR_DATE           m_field->m_user[1]
#define SBST_FUNGI_DATE        m_field->m_user[2]
#define SBST_WATER_DATE        m_field->m_user[3]
#define SBST_INSECT_DATE       m_field->m_user[4]

typedef enum {
  sbst_start = 1, // Compulsory, start event must always be 1 (one).
  sbst_autumn_plough = SBST_BASE,
  sbst_fertslurry_stock,
  sbst_fertmanure_stock_one,
  sbst_spring_plough,
  sbst_spring_harrow,
  sbst_fertmanure_plant,
  sbst_fertlnh3_plant,
  sbst_fertpk_plant,
  sbst_fertmanure_stock_two,
  sbst_fertnpk_stock,
  sbst_spring_sow,
  sbst_spring_roll,
  sbst_strigling_one,
  sbst_strigling_two,
  sbst_strigling_three,
  sbst_strigling_four,
  sbst_GR,
  sbst_fungicide_one,
  sbst_insecticide,
  sbst_fungicide_two,
  sbst_water_one,
  sbst_water_two,
  sbst_harvest,
  sbst_straw_chopping,
  sbst_hay_baling,
  sbst_stubble_harrow
} SBTSoDo;



class SpringBarleyStrigling: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  SpringBarleyStrigling()
  {
      m_first_date=g_date->DayInYear(1,11);
  }
};

#endif // SpringBarleyStrigling_H
