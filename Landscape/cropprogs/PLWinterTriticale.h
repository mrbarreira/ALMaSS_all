/**
\file
\brief
<B>PLWinterTriticale.h This file contains the headers for the WinterTriticale class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PLWinterTriticale.h
//


#ifndef PLWINTERTRITICALE_H
#define PLWINTERTRITICALE_H

#define PLWINTERTRITICALE_BASE 20300
/**
\brief A flag used to indicate autumn ploughing status
*/
#define PL_WT_FERTI_P1	a_field->m_user[1]
#define PL_WT_FERTI_S1	a_field->m_user[2]
#define PL_WT_STUBBLE_PLOUGH	a_field->m_user[3]

/** Below is the list of things that a farmer can do if he is growing winter triticale, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_wt_start = 1, // Compulsory, must always be 1 (one).
	pl_wt_sleep_all_day = PLWINTERTRITICALE_BASE,
	pl_wt_ferti_p1, // 20301
	pl_wt_ferti_s1,
	pl_wt_stubble_plough,
	pl_wt_autumn_harrow1,
	pl_wt_autumn_harrow2,
	pl_wt_stubble_harrow,
	pl_wt_ferti_p2,
	pl_wt_ferti_s2,
	pl_wt_autumn_plough,
	pl_wt_autumn_roll, // 20310
	pl_wt_stubble_cultivator_heavy,
	pl_wt_ferti_p3,
	pl_wt_ferti_s3,
	pl_wt_preseeding_cultivator,
	pl_wt_preseeding_cultivator_sow,
	pl_wt_autumn_sow, // 20316
	pl_wt_herbicide1,
	pl_wt_fungicide1,
	pl_wt_insecticide1,
	pl_wt_ferti_p4, // 20320
	pl_wt_ferti_s4,
	pl_wt_ferti_p5,
	pl_wt_ferti_s5,
	pl_wt_ferti_p6,
	pl_wt_ferti_s6,
	pl_wt_ferti_p7,
	pl_wt_ferti_s7,
	pl_wt_ferti_p8,
	pl_wt_ferti_s8,
	pl_wt_ferti_p9,	// 20330
	pl_wt_ferti_s9,
	pl_wt_herbicide2,
	pl_wt_fungicide2,
	pl_wt_fungicide3,
	pl_wt_fungicide4,
	pl_wt_insecticide2,
	pl_wt_insecticide3,
	pl_wt_growth_regulator1,
	pl_wt_harvest,
	pl_wt_straw_chopping,	// 20340
	pl_wt_hay_bailing,
	pl_wt_ferti_p10,
	pl_wt_ferti_s10,
	pl_wt_ferti_p11,
	pl_wt_ferti_s11, // 20345
} PLWinterTriticaleToDo;


/**
\brief
PLWinterTriticale class
\n
*/
/**
See PLWinterTriticale.h::PLWinterTriticaleToDo for a complete list of all possible events triggered codes by the winter triticale management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLWinterTriticale: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PLWinterTriticale()
   {
		// When we start it off, the first possible date for a farm operation is 20th September
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 20,9 );
   }
};

#endif // PLWINTERTRITICALE_H

