/**
\file
\brief
<B>NLCarrots.h This file contains the headers for the Carrots class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLCarrots.h
//


#ifndef NLCARROTS_H
#define NLCARROTS_H

#define NLCARROTS_BASE 25100
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_CA_WINTER_PLOUGH	a_field->m_user[1]
#define NL_CA_HERBI1	a_field->m_user[2]
#define NL_CA_HERBI2	a_field->m_user[3]
#define NL_CA_FUNGI1	a_field->m_user[4]
#define NL_CA_FUNGI2	a_field->m_user[5]

/** Below is the list of things that a farmer can do if he is growing carrots, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_ca_start = 1, // Compulsory, must always be 1 (one).
	nl_ca_sleep_all_day = NLCARROTS_BASE,
	nl_ca_spring_plough_sandy,
	nl_ca_winter_plough_clay,
	nl_ca_winter_deep_harrow_clay,
	nl_ca_ferti_p1,
	nl_ca_ferti_s1,
	nl_ca_preseeding_cultivator,
	nl_ca_bed_forming,
	nl_ca_spring_sow,
	nl_ca_ferti_p2,
	nl_ca_ferti_s2,
	nl_ca_herbicide1,
	nl_ca_herbicide2,
	nl_ca_herbicide3,
	nl_ca_fungicide1,
	nl_ca_fungicide2,
	nl_ca_fungicide3,
	nl_ca_harvest,
} NLCarrotsToDo;


/**
\brief
NLCarrots class
\n
*/
/**
See NLCarrots.h::NLCarrotsToDo for a complete list of all possible events triggered codes by the carrots management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLCarrots: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLCarrots()
   {
		// When we start it off, the first possible date for a farm operation is 20th October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 1,12 );
   }
};

#endif // NLCARROTS_H

