/**
\file
\brief
<B>NLPotatoes.h This file contains the headers for the Potatoes class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLPotatoes.h
//


#ifndef NLPOTATOES_H
#define NLPOTATOES_H

#define NLPOTATOES_BASE 25300
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_POT_HERBI		a_field->m_user[1]
#define NL_POT_FUNGI1		a_field->m_user[2]
#define NL_POT_FUNGI2		a_field->m_user[3]
#define NL_POT_FUNGI3		a_field->m_user[4]
#define NL_POT_FUNGI4		a_field->m_user[5]
#define NL_POT_FUNGI5		a_field->m_user[6]

/** Below is the list of things that a farmer can do if he is growing potatoes, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_pot_start = 1, // Compulsory, must always be 1 (one).
	nl_pot_sleep_all_day = NLPOTATOES_BASE,
	nl_pot_stubble_harrow,
	nl_pot_winter_plough_clay,
	nl_pot_ferti_p2_clay,
	nl_pot_ferti_s2_clay,
	nl_pot_ferti_p1_sandy,
	nl_pot_ferti_s1_sandy,	
	nl_pot_spring_plough_sandy,
	nl_pot_ferti_p2_sandy,
	nl_pot_ferti_s2_sandy,
	nl_pot_bed_forming,
	nl_pot_spring_planting,
	nl_pot_hilling1,
	nl_pot_ferti_p3_clay,
	nl_pot_ferti_s3_clay,
	nl_pot_ferti_p3_sandy,
	nl_pot_ferti_s3_sandy,
	nl_pot_ferti_p4,
	nl_pot_ferti_s4,
	nl_pot_herbicide1,
	nl_pot_herbicide2,
	nl_pot_fungicide1,
	nl_pot_fungicide2,
	nl_pot_fungicide3,
	nl_pot_fungicide4,
	nl_pot_fungicide5,
	nl_pot_fungicide6,
	nl_pot_fungicide7,
	nl_pot_fungicide8,
	nl_pot_fungicide9,
	nl_pot_fungicide10,
	nl_pot_fungicide11,
	nl_pot_fungicide12,
	nl_pot_fungicide13,
	nl_pot_fungicide14,
	nl_pot_fungicide15,
	nl_pot_insecticide,
	nl_pot_dessication1,
	nl_pot_dessication2,
	nl_pot_harvest,
} NLPotatoesToDo;


/**
\brief
NLPotatoes class
\n
*/
/**
See NLPotatoes.h::NLPotatoesToDo for a complete list of all possible events triggered codes by the potatoes management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLPotatoes: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLPotatoes()
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 5,11 );
   }
};

#endif // NLPOTATOES_H

