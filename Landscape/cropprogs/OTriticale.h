//
// OTriticale.h
//
/* 
*******************************************************************************************************
Copyright (c) 2014, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OTriticale_H
#define OTriticale_H

#define OTRITICALE_BASE 7440

#define OTRI_OCCUP_DATE        m_field->m_user[0]
#define OTRI_WATER_DATE        m_field->m_user[1]


typedef enum {
  otri_start = 1, // Compulsory, start event must always be 1 (one).
  otri_fa_manure = OTRITICALE_BASE,
  otri_autumn_plough,
  otri_autumn_harrow,
  otri_autumn_sow,
  otri_autumn_roll,
  otri_spring_roll,
  otri_fa_npk,
  otri_fa_slurry,
  otri_strigling,
  otri_water,
  otri_harvest,
  otri_chopping,
  otri_hay_turning,
  otri_hay_bailing,
  otri_stubble_harrow
} OTriticaleToDo;



class OTriticale: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OTriticale()
  {
      m_first_date=g_date->DayInYear(5,10);
  }
};

#endif // OTriticale_H
