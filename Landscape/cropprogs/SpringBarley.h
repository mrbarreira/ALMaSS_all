//
// SpringBarley.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef SpringBarley_H
#define SpringBarley_H

#define SBARLEY_BASE 4700
#define SB_SLURRY_DONE       m_field->m_user[0]
#define SB_MANURE_DONE       m_field->m_user[1]
#define SB_SLURRY_EXEC       m_field->m_user[2]
#define SB_MANURE_EXEC       m_field->m_user[3]
#define SB_DID_AUTUMN_PLOUGH m_field->m_user[4]

#define SB_HERBI_DATE        m_field->m_user[0]
#define SB_GR_DATE           m_field->m_user[1]
#define SB_FUNGI_DATE        m_field->m_user[2]
#define SB_WATER_DATE        m_field->m_user[3]
#define SB_INSECT_DATE       m_field->m_user[4]
#define SB_DECIDE_TO_HERB	 m_field->m_user[5]
#define SB_DECIDE_TO_FI		 m_field->m_user[6]

typedef enum {
  sb_start = 1, // Compulsory, start event must always be 1 (one).
  sb_autumn_plough = SBARLEY_BASE,
  sb_fertslurry_stock,
  sb_fertmanure_stock_one,
  sb_spring_plough,
  sb_spring_harrow,
  sb_fertmanure_plant,
  sb_fertlnh3_plant,
  sb_fertpk_plant,
  sb_fertmanure_stock_two,
  sb_fertnpk_stock,
  sb_spring_sow,
  sb_spring_roll,
  sb_herbicide_one,
  sb_herbicide_two,
  sb_GR,
  sb_fungicide_one,
  sb_insecticide,
  sb_insecticide2,
  sb_insecticide3,
  sb_fungicide_two,
  sb_water_one,
  sb_water_two,
  sb_harvest,
  sb_straw_chopping,
  sb_hay_baling,
  sb_stubble_harrow
} SBToDo;



class SpringBarley: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  SpringBarley()
  {
      m_first_date=g_date->DayInYear(2,11);
  }
};

#endif // SpringBarley_H
