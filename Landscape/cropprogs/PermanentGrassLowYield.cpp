//
// PermanentGrassLowYield.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/PermanentGrassLowYield.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;

bool PermanentGrassLowYield::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;

	int d1;

	bool done = false;

	switch (m_ev->m_todo) {
	case pgly_start:
	{
					   PGLY_CUT_DATE = 0;
					   // Set up the date management stuff
					   m_last_date = g_date->DayInYear(1, 10);
					   // Start and stop dates for all events after harvest
					   int noDates = 1;
					   // Start and stop dates for all events after harvest
					   m_field->SetMDates(0, 0, g_date->DayInYear(15, 9));
					   // 0,0 determined by harvest date - used to see if at all possible
					   m_field->SetMDates(1, 0, g_date->DayInYear(1, 10));
					   // Check the next crop for early start, unless it is a spring crop
					   // in which case we ASSUME that no checking is necessary!!!!
					   // So DO NOT implement a crop that runs over the year boundary

					   //new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
					   if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)){

						   if (m_ev->m_startday > g_date->DayInYear(1, 7))
						   {
							   if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
							   {
								   g_msg->Warn(WARN_BUG, "PermanentGrassLowYield::Do(): "
									   "Harvest too late for the next crop to start!!!", "");
								   exit(1);
							   }
							   // Now fix any late finishing problems
							   for (int i = 0; i < noDates; i++) {
								   if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
									   m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
								   }
								   if (m_field->GetMDates(1, i) >= m_ev->m_startday){
									   m_field->SetMConstants(i, 0);
									   m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
								   }
							   }
						   }
						   // Now no operations can be timed after the start of the next crop.
						   int today = g_date->Date();
						   d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
						   if (today > d1)
						   {
							   // Yes too late - should not happen - raise an error
							   g_msg->Warn(WARN_BUG, " PermanentGrassLowYield::Do(): "
								   "Crop start attempt after last possible start date", "");
							   exit(1);
						   }
					   }
					   // Reinit d1 to first possible starting date.
					   d1 = g_date->OldDays() + m_first_date;
					   if (!m_ev->m_first_year) d1 += 365; // Add 365 for spring crop (not 1st yr)
					   if (g_date->Date() > d1) {
						   d1 = g_date->Date();
					   }
					   // OK, let's go.
					   m_field->SetLastSownVeg( m_field->GetVegType() ); //Force last sown, needed for goose habitat classification

					   SimpleEvent(d1, pgly_cut_to_hay, false);
	}
	break;

	case pgly_cut_to_hay:
		if (m_ev->m_lock || m_farm->DoIt(20))
		{
			if (!m_farm->CutToHay(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, pgly_cut_to_hay, true);
				break;
			}
			// did cut to hay so try turning
			else
			{
				SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 6),
					pgly_raking1, false);
				PGLY_CUT_DATE = g_date->Date();
			}
			break;
		}
		SimpleEvent(g_date->OldDays() + m_first_date, pgly_cattle_out1, false); // non-cutters graze

		break;

	case pgly_raking1:
		if (PGLY_CUT_DATE + 20 >= g_date->Date()) m_ev->m_lock = true;
		if (m_ev->m_lock || m_farm->DoIt(50))
		{
			if (!m_farm->HayTurning(m_field, 0.0,
				g_date->DayInYear(25, 6) - g_date->DayInYear()))
			{
				SimpleEvent(g_date->Date() + 1, pgly_raking1, true);
				break;
			}
			SimpleEvent(g_date->Date() + 1, pgly_raking2, false);
			break;
		}
		ChooseNextCrop(1);
		SimpleEvent(PGLY_CUT_DATE + 21, pgly_cattle_out1, false);	// 100% of these graze
		break;

	case pgly_raking2:
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->HayTurning(m_field, 0.0,
				g_date->DayInYear(26, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, pgly_raking2, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + 1, pgly_compress_straw, false);
		break;

	case pgly_compress_straw:
		if (!m_farm->HayBailing(m_field, 0.0,
			g_date->DayInYear(1, 7) - g_date->DayInYear()))
		{
			SimpleEvent(g_date->Date() + 1, pgly_compress_straw, true);
			break;
		}
		// Did compress straw

		ChooseNextCrop(1);

		SimpleEvent(PGLY_CUT_DATE + 21, pgly_cattle_out1, false);	// 100% of these graze
		break;

	case pgly_cattle_out1:
		ChooseNextCrop(1);

		if (m_field->GetMConstants(0) == 0) {
			if (!m_farm->CattleOut(m_field, 0.0, -1)) { 
				//raise an error
				g_msg->Warn(WARN_BUG, "PermanentGrassLowYield::Do(): failure in 'CattleOut' execution", "");
				exit(1);
			}
		}
		else {
			if (!m_farm->CattleOut(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, pgly_cattle_out1, true);
				break;
			}
		}
		SimpleEvent(g_date->Date() + m_field->GetMConstants(0), pgly_cattle_is_out, false);
		break;

	case pgly_cattle_out2:
		if ((m_ev->m_lock || m_farm->DoIt(33)))
		{
			if (m_field->GetMConstants(0) == 0) {
				if (!m_farm->CattleOut(m_field, 0.0, -1)) { //raise an error
					g_msg->Warn(WARN_BUG, "PermanentGrassLowYield::Do(): failure in 'CattleOut' execution", "");
					exit(1);
				}
			}
			else {
				if (!m_farm->CattleOut(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
					SimpleEvent(g_date->Date() + 1, pgly_cattle_out2, true);
					break;
				}
			}
			// Success
			SimpleEvent(g_date->Date() + m_field->GetMConstants(0), pgly_cattle_is_out, false);
			break;
		}
		//Don't graze - but don't end too early;
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(2, 7), pgly_wait, false);
		break;

	case pgly_wait:
		done = true;
		break;

	case pgly_cattle_is_out:
		if (m_field->GetMConstants(0) == 0) {
			if (!m_farm->CattleIsOut(m_field, 0.0, -1, m_field->GetMDates(1, 0))) { //raise an error
				//added 28.08 - issue a warning only if we fail on the last day that this can be done, i.e. MDate
				if (g_date->Date() == m_field->GetMDates(1, 0)){
					g_msg->Warn(WARN_BUG, "PermanentGrassLowYield::Do(): failure in 'CattleIsOut' execution", "");
					exit(1);
				}
			}
		}
		else {
			if (!m_farm->CattleIsOut(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear(), m_field->GetMDates(1, 0))) {
				SimpleEvent(g_date->Date() + 1, pgly_cattle_is_out, true);
				break;
			}
		}
		// if they come in then send them out if too early
		if (g_date->DayInYear() < g_date->DayInYear(10, 9))
		{
			SimpleEvent(g_date->Date() + 1, pgly_cattle_out2, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 6), pgly_herbicide, false);
		break;

	case pgly_herbicide:
		if ((m_ev->m_lock || m_farm->DoIt((int)(5 * cfg_herbi_app_prop.value()  * m_farm->Prob_multiplier()))) && //modified probability
			// --FN--
			(g_date->DayInYear() < g_date->DayInYear(15, 8)))
		{
			//new - for decision making
			TTypesOfVegetation tov = m_field->GetVegType();
			if (!m_ev->m_lock && !m_farm->Spraying_herbicides(tov)){
				Field * pf = dynamic_cast<Field*>(m_field);
				pf->Add_missed_herb_app();
			} //end of the part for dec. making
			else{
				if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
					// --FN--
					SimpleEvent(g_date->Date() + 1, pgly_herbicide, true);
					break;
				}
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 8),
			pgly_cut_weeds, false);
		break;

	case pgly_cut_weeds:
		if ((m_ev->m_lock || m_farm->DoIt(25)) &&
			//--FN--
			(g_date->DayInYear() < g_date->DayInYear(15, 9)))
		{
			if (!m_farm->CutWeeds(m_field, 0.0,
				g_date->DayInYear(15, 9) - g_date->DayInYear())) {
				// --FN--
				SimpleEvent(g_date->Date() + 1, pgly_cut_weeds, true);
				break;
			}
		}
		// END MAIN THREAD
		done = true;
		break;

	default:
		g_msg->Warn(WARN_BUG, "PermanantGrassLowYield::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}


