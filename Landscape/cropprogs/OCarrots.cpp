//
// OCarrots.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OCarrots.h"


bool OCarrots::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;

  switch ( m_ev->m_todo ) {
  case oca_start:
    {
      OCA_WATER_DATE = 0;
      // Set up the date management stuff
      m_last_date=g_date->DayInYear(30,11);
      // Start and stop dates for all events after harvest
      m_field->SetMDates(0,0,g_date->DayInYear(30,11));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(30,11));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary
      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "OCarrots::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Not possible to fix any late finishing problems, harvest too late
      }

      int d1;

      int today=g_date->Date();
      d1 = g_date->OldDays() + m_first_date;
      if ( ! m_ev->m_first_year ) d1+=365; // Add 365 for spring crop (not 1st yr)
      if (today > d1)
      {
          // Yes too late - should not happen - raise an error
          g_msg->Warn( WARN_BUG, "OCarrots::Do(): "
		 "Crop start attempt after last possible start date", "" );
          exit( 1 );
      }
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + m_first_date+365; // Add 365 for spring crop
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      // OK, let's go.
      if ( m_farm->IsStockFarmer()) {
        SimpleEvent( d1+14, oca_fa_slurry, false );
      } else {
        SimpleEvent( d1, oca_fp_slurry, false );
      }
      break;
    }
  case oca_fa_slurry:
    if ( m_ev->m_lock || m_farm->DoIt( 50 )) {
      if (!m_farm->FA_Slurry( m_field, 0.0,
			      g_date->DayInYear( 30, 3 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, oca_fa_slurry, true );
	break;
      }
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 20, 3 );
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      SimpleEvent( d1, oca_spring_plough, false );
    }
    break;

  case oca_fp_slurry:
    if ( m_ev->m_lock || m_farm->DoIt( 50 )) {
      if (!m_farm->FP_Slurry( m_field, 0.0,
			      g_date->DayInYear( 30, 3 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, oca_fp_slurry, true );
	break;
      }
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 20, 3 );
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      SimpleEvent( d1, oca_spring_plough, false );
    }
    break;

  case oca_spring_plough:
    if (!m_farm->SpringPlough( m_field, 0.0,
			       g_date->DayInYear( 10, 4 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oca_spring_plough, false );
      break;
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 1, 4 );
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      SimpleEvent( d1, oca_spring_harrow_one, false );
    }
    break;

  case oca_spring_harrow_one:
    if (!m_farm->SpringHarrow( m_field, 0.0,
			       g_date->DayInYear( 15, 4 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oca_spring_harrow_one, false );
      break;
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 20, 4 );
      if ( g_date->Date() + 14 > d1 ) {
	d1 = g_date->Date() + 14;
      }
      SimpleEvent( d1, oca_spring_harrow_two, false );
    }
    break;

  case oca_spring_harrow_two:
    if (!m_farm->SpringHarrow( m_field, 0.0,
			       g_date->DayInYear( 15, 5 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oca_spring_harrow_two, false );
      break;
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 25, 4 );
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      SimpleEvent( d1, oca_spring_sow, false );
    }
    break;

  case oca_spring_sow:
    if (!m_farm->SpringSow( m_field, 0.0,
			    g_date->DayInYear( 25, 5 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oca_spring_sow, false );
      break;
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 3, 5 );
      if ( g_date->Date() + 7 > d1 ) {
	d1 = g_date->Date() + 7;
      }
      SimpleEvent( d1, oca_rowcul_one, false );
    }
    break;

  case oca_rowcul_one:
    if (!m_farm->RowCultivation( m_field, 0.0,
				 g_date->DayInYear( 3, 6 ) -
				 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oca_rowcul_one, false );
      break;
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 14, 5 );
      if ( g_date->Date() + 10 > d1 ) {
	d1 = g_date->Date() + 10;
      }
      SimpleEvent( d1, oca_rowcul_two, false );
    }
    break;

  case oca_rowcul_two:
    if (!m_farm->RowCultivation( m_field, 0.0,
				 g_date->DayInYear( 14, 6 ) -
				 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oca_rowcul_two, false );
      break;
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 25, 5 );
      if ( g_date->Date() + 10 > d1 ) {
	d1 = g_date->Date() + 10;
      }
      SimpleEvent( d1, oca_rowcul_three, false );
    }
    break;

  case oca_rowcul_three:
    if (!m_farm->RowCultivation( m_field, 0.0,
				 g_date->DayInYear( 25, 6 ) -
				 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oca_rowcul_three, false );
      break;
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 8, 6 );
      if ( g_date->Date() + 10 > d1 ) {
	d1 = g_date->Date() + 10;
      }
      SimpleEvent( d1, oca_rowcul_four, false );
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 1, 7 );
      if ( g_date->Date() + 3 > d1 ) {
	d1 = g_date->Date() + 3;
      }
      SimpleEvent( d1, oca_water_one, false );
    }
    break;

  case oca_rowcul_four:
    if ( OCA_WATER_DATE &&
	 OCA_WATER_DATE >= g_date->Date() - 1 ) {
      SimpleEvent( g_date->Date() + 1, oca_rowcul_four, false );
      break;
    }
    if (!m_farm->RowCultivation( m_field, 0.0,
				 g_date->DayInYear( 10, 7 ) -
				 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oca_rowcul_four, false );
      break;
    }
    break;

  case oca_water_one:
    if ( m_ev->m_lock || m_farm->DoIt( 90 )) {
      if (!m_farm->Water( m_field, 0.0,
			  g_date->DayInYear( 20, 7 ) -
			  g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, oca_water_one, true );
	break;
      }
      OCA_WATER_DATE = g_date->Date();
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 21, 7 );
      if ( g_date->Date() + 7 > d1 ) {
	d1 = g_date->Date() + 7;
      }
      SimpleEvent( d1, oca_water_two, false );
    }
    break;

  case oca_water_two:
    if (!m_farm->Water( m_field, 0.0,
			g_date->DayInYear( 10, 8 ) -
			g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oca_water_two, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 11, 8 ),
		 oca_water_three, false );
    break;

  case oca_water_three:
    if ( m_ev->m_lock || m_farm->DoIt( 90 )) {
      if (!m_farm->Water( m_field, 0.0,
			  g_date->DayInYear( 5, 9 ) -
			  g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, oca_water_three, true );
	break;
      }
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 1, 9 );
      if ( g_date->Date() + 7 > d1 ) {
	d1 = g_date->Date() + 7;
      }
      SimpleEvent( d1, oca_harvest, false );
    }
    break;

  case oca_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
         g_date->DayInYear( 30, 11 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, oca_harvest, false );
      break;
    }
    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "OCarrots::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


