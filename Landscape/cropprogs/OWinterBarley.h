//
// OWinterBarley.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OWinterBarley_H
#define OWinterBarley_H

#define OWBarley_BASE 5800
#define OWB_DID_SPRING_SOW  m_field->m_user[0]

typedef enum {
  owb_start = 1, // Compulsory, start event must always be 1 (one).
  owb_fertmanure_plant_one = OWBarley_BASE,
  owb_fertmanure_stock_one,
  owb_autumn_plough,
  owb_autumn_harrow,
  owb_autumn_sow,
  owb_strigling_one,
  owb_spring_sow,
  owb_fertslurry_plant,
  owb_fertslurry_stock,
  owb_strigling_two,
  owb_harvest,
  owb_straw_chopping,
  owb_hay_bailing,
  owb_stubble_harrowing
// --FN--
} OWBToDo;



class OWinterBarley: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OWinterBarley()
  {
    m_first_date=g_date->DayInYear(23,9);
  }
};

#endif // OWinterBarley_H
