//
//  OCloverGrassSilage1.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OCloverGrassSilage1.h"


//---------------------------------------------------------------------------

//#pragma package(smart_init)


using namespace std;

bool OCloverGrassSilage1::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;

  switch ( m_ev->m_todo ) {
  case ocgs1_start:
    {
      // Set up the date management stuff
      // Could save the start day in case it is needed later
      // m_field->m_startday = m_ev->m_startday;
      m_last_date=g_date->DayInYear(30,8);
      // Start and stop dates for all events after harvest
      int noDates= 1;
      m_field->SetMDates(0,0,g_date->DayInYear(27,7));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(30,8));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary
      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "OCloverGrassSilage1::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++)
        {
          if  (m_field->GetMDates(0,i)>=m_ev->m_startday)
                                     m_field->SetMDates(0,i,m_ev->m_startday-1);
          if  (m_field->GetMDates(1,i)>=m_ev->m_startday)
                                     m_field->SetMDates(1,i,m_ev->m_startday-1);
        }
      }
      // Now no operations can be timed after the start of the next crop.

      int d1;

      int today=g_date->Date();
      d1 = g_date->OldDays() + m_first_date+365; // Add 365 for spring crop
      if (today > d1)
      {
          // Yes too late - should not happen - raise an error
          g_msg->Warn( WARN_BUG, " OCloverGrassSilage1::Do(): "
		 "Crop start attempt after last possible start date", "" );
          exit( 1 );
      }
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays()+m_first_date;;
      if ( ! m_ev->m_first_year ) d1+=365; // Add 365 for spring crop (not 1st yr)
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
	  m_field->SetLastSownVeg( m_field->GetVegType() ); //Force last sown, needed for goose habitat classification
	  // OK, let's go.
    OCGS1_CUT_DATE=0;
    OCGS1_SLURRY=false;
    SimpleEvent( d1, ocgs1_ferti_zero, false );
    }
    break;

  case ocgs1_ferti_zero:
    if ( m_ev->m_lock || m_farm->DoIt( 100 ))
    {
      if (!m_farm->FA_Slurry( m_field, 0.0,
           g_date->DayInYear( 30, 4 ) - g_date->DayInYear())) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, ocgs1_ferti_zero, true );
        break;
      }
      else OCGS1_SLURRY=true;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25, 5 ),
                 ocgs1_cut_to_silage, false );
    // Start a watering thread
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 6 ),
                 ocgs1_water_zero, false );
    break;

  case ocgs1_cut_to_silage:
    if (!m_farm->CutToSilage( m_field, 0.0,
         g_date->DayInYear( 15, 6 ) - g_date->DayInYear())) {
      // We didn't do it today, try again tomorrow.
      SimpleEvent( g_date->Date() + 1, ocgs1_cut_to_silage, true );
      break;
    }
    OCGS1_CUT_DATE = g_date->DayInYear();
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,5 ),
                 ocgs1_ferti_one, false );
    break;

  case ocgs1_water_zero:
    if ( m_ev->m_lock || m_farm->DoIt( 30 ))
    {
      if (!m_farm->Water( m_field, 0.0,
           g_date->DayInYear( 15, 6 ) - g_date->DayInYear())) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, ocgs1_water_zero, true );
        break;
      }
      // Success
     	if ( (g_date->DayInYear()+7)<g_date->DayInYear( 25,6 ) )
	    	SimpleEvent( g_date->Date() + 7,
  	                ocgs1_water_one, false );
     	else
     		SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,6 ),
        		         ocgs1_water_one, false );
     	break;
   	}
   	// Didn't water so let the thread die
   	break;

  case ocgs1_water_one:
    if ( m_ev->m_lock || m_farm->DoIt( 67 ))    //**CJT** check this or 20
    {
      if (!m_farm->Water( m_field, 0.0,
           g_date->DayInYear( 25, 7 ) - g_date->DayInYear())) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, ocgs1_water_one, true );
        break;
      }
    }
    break;
    // End of watering thread

  case ocgs1_ferti_one:
    if(OCGS1_SLURRY)   // if did it before
    {
      if (!m_farm->FA_Slurry( m_field, 0.0,
         (OCGS1_CUT_DATE+4) - g_date->DayInYear()))
      {
        SimpleEvent( g_date->Date() + 1, ocgs1_ferti_one, true );
        break;
      }
    }
    // Three weeks later
    SimpleEvent( g_date->Date()+21, ocgs1_cut_to_silage1, false );
    break;

  case ocgs1_cut_to_silage1:
    if (!m_farm->CutToSilage( m_field, 0.0,
         g_date->DayInYear( 30, 6 ) - g_date->DayInYear())) {
      // We didn't do it today, try again tomorrow.
      // --FN-- 26/01-2001
      SimpleEvent( g_date->Date() + 1, ocgs1_cut_to_silage1, true );
      break;
    }
    // Success so queue up the next event
    OCGS1_CUT_DATE=g_date->DayInYear();
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 6 ),
                 ocgs1_ferti_two, false );
    break;

  case ocgs1_ferti_two:
    if (!m_farm->FA_Slurry( m_field, 0.0,
         (OCGS1_CUT_DATE+4) - g_date->DayInYear())) {
       SimpleEvent( g_date->Date() + 1, ocgs1_ferti_two, true );
      break;
    }
    if ( (g_date->DayInYear(16,7))< OCGS1_CUT_DATE+21 )
	    SimpleEvent( g_date->OldDays() + (OCGS1_CUT_DATE+21),
  	               ocgs1_cut_to_silage2, false );
    else
    	SimpleEvent( g_date->OldDays() + g_date->DayInYear( 6,7 ),
      	           ocgs1_cut_to_silage2, false );
    break;

  case ocgs1_cut_to_silage2:
    if (!m_farm->CutToSilage( m_field, 0.0,
         g_date->DayInYear( 5,8 ) - g_date->DayInYear()))
    {
      // We didn't do it today, try again tomorrow.
      SimpleEvent( g_date->Date() + 1, ocgs1_cut_to_silage2, true );
      break;
    }
    OCGS1_CUT_DATE=g_date->DayInYear();
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 6,7 ),
                 ocgs1_ferti_three, false );
    break;

  case ocgs1_ferti_three:
    if ( m_ev->m_lock || m_farm->DoIt( 60 ))
    {
      if (!m_farm->FA_Slurry( m_field, 0.0,
           (OCGS1_CUT_DATE+4) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, ocgs1_ferti_three, true );
        break;
      }
    }
    if ( (g_date->DayInYear(27,7))< OCGS1_CUT_DATE+21 )
	    SimpleEvent( g_date->OldDays() + (OCGS1_CUT_DATE+21),
      // --FN-- 26/01-2001
  	               ocgs1_cut_to_silage3, false );
    else
    	SimpleEvent( g_date->OldDays() + g_date->DayInYear( 27,7 ),
      	           ocgs1_cut_to_silage3, false );
    break;

  case ocgs1_cut_to_silage3:
    if ( m_ev->m_lock || m_farm->DoIt( 40 ))
    {
      if (!m_farm->CutToSilage( m_field, 0.0,
           m_field->GetMDates(1,0) - g_date->DayInYear()))
      {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, ocgs1_cut_to_silage3, true );
        break;
      }
    }
    done=true; // END MAIN THREAD
    break;

  default:
    g_msg->Warn( WARN_BUG, "OCloverGrassGrazed1::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}
//---------------------------------------------------------------------------
