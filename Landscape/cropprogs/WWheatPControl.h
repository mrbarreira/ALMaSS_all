//
// WWheatPControl.h
//
/*

Copyright (c) 2003, National Environmental Research Institute, Denmark (NERI)

All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/
#ifndef WWHEATPCONTROL_H
#define WWHEATPCONTROL_H

#define WWHEATPCONTROL_BASE 60000
#define WWPC_WAIT_FOR_PLOUGH       m_field->m_user[0]
#define WWPC_AUTUMN_PLOUGH         m_field->m_user[1]

typedef enum {
  wwpc_start = 1, // Compulsory, must always be 1 (one).
  wwpc_sleep_all_day = WWHEATPCONTROL_BASE,
  wwpc_ferti_p1,
  wwpc_ferti_s1,
  wwpc_ferti_s2,
  wwpc_autumn_plough,
  wwpc_autumn_harrow,
  wwpc_stubble_harrow1,
  wwpc_autumn_sow,
  wwpc_autumn_roll,
  wwpc_ferti_p2,
  wwpc_herbicide1,
  wwpc_spring_roll,
  wwpc_herbicide2,
  wwpc_GR,
  wwpc_fungicide,
  wwpc_fungicide2,
  wwpc_insecticide1,
  wwpc_insecticide2,
  wwpc_insecticide3,
  wwpc_strigling1,
  wwpc_strigling2,
  wwpc_water1,
  wwpc_water2,
  wwpc_ferti_p3,
  wwpc_ferti_p4,
  wwpc_ferti_p5,
  wwpc_ferti_s3,
  wwpc_ferti_s4,
  wwpc_ferti_s5,
  wwpc_harvest,
  wwpc_straw_chopping,
  wwpc_hay_turning,
  wwpc_hay_baling,
  wwpc_stubble_harrow2,
  wwpc_grubning
} WWheatPControlToDo;


class WWheatPControl: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
};

#endif // WHEATPCONTROL_H
 
