//
// seedgrass2.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/seedgrass2.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;

bool SeedGrass2::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  int d1;

  bool done = false;

  switch ( m_ev->m_todo ) {
  case sg2_start:
    {
      SG2_WATER_DATE = g_date->Date();
      SG2_FUNGI_DATE = g_date->Date();
	  SG2_DECIDE_TO_HERB = 1;
          // Set up the date management stuff
      m_last_date=g_date->DayInYear(15,8);
      // Start and stop dates for all events after harvest
      int noDates= 5;
      m_field->SetMDates(0,0,g_date->DayInYear(10,7));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(20,7));
      m_field->SetMDates(0,1,g_date->DayInYear(11,7));
      m_field->SetMDates(1,1,g_date->DayInYear(25,7));
      m_field->SetMDates(0,2,g_date->DayInYear(26,7));
      m_field->SetMDates(1,2,g_date->DayInYear(20,7));
      m_field->SetMDates(0,3,g_date->DayInYear(15,7));
      m_field->SetMDates(1,3,g_date->DayInYear(15,8));
      m_field->SetMDates(0,4,g_date->DayInYear(15,8));
      m_field->SetMDates(1,4,g_date->DayInYear(20,8));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary

	  //new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
	  if(!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){

      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "SeedGrass2::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++) {
			if(m_field->GetMDates(0,i)>=m_ev->m_startday) { 
				m_field->SetMDates(0,i,m_ev->m_startday-1); //move the starting date
			}
			if(m_field->GetMDates(1,i)>=m_ev->m_startday){
				m_field->SetMConstants(i,0); 
				m_field->SetMDates(1,i,m_ev->m_startday-1); //move the finishing date
			}
		}
      }
      // Now no operations can be timed after the start of the next crop.
      int today=g_date->Date();
      d1 = g_date->OldDays() + m_first_date;
      if ( ! m_ev->m_first_year ) d1+=365; // Add 365 for spring crop (not 1st yr)
      if (today > d1)
      {
          // Yes too late - should not happen - raise an error
          g_msg->Warn( WARN_BUG, "SeedGrass2::Do(): "
		 "Crop start attempt after last possible start date", "" );
          exit( 1 );
      }
	  }//if

      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + m_first_date+365; // Add 365 for spring crop
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
	  m_field->SetLastSownVeg( m_field->GetVegType() ); //Force last sown, needed for goose habitat classification
	  // OK, let's go.
      SimpleEvent( d1, sg2_ferti_zero, false );
    }
    break;

  case sg2_ferti_zero:
    if ( m_ev->m_lock || m_farm->DoIt( 60 )) {
      if (!m_farm->FP_NPK( m_field, 0.0,
			   g_date->DayInYear( 15, 4 ) -
			   g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sg2_ferti_zero, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 4 ),
                 sg2_herbi_zero, false );
    break;

  case sg2_herbi_zero:
    if ( m_ev->m_lock || m_farm->DoIt(  (int) (75*cfg_herbi_app_prop.value()  * m_farm->Prob_multiplier()))) { //modified probability
     
		//new - for decision making
		TTypesOfVegetation tov = m_field->GetVegType();
		if(!m_ev->m_lock && !m_farm->Spraying_herbicides(tov)){
			Field * pf =  dynamic_cast<Field*>(m_field); 
			pf->Add_missed_herb_app();
			if(m_farm->DoIt(67)) pf->Add_missed_herb_app(); //the 2nd missed application
			SG2_DECIDE_TO_HERB=0;
		} //end of the part for dec. making
		else{
			if (!m_farm->HerbicideTreat( m_field, 0.0, g_date->DayInYear( 25, 4 ) - g_date->DayInYear())) {
				SimpleEvent( g_date->Date() + 1, sg2_herbi_zero, true );
				break;
			 }
		}
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear() + 14, 
                 sg2_herbi_one, false );
	SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20, 5 ),
                 sg2_fungi_zero, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 5 ),
                 sg2_water_zero, false );
    break;

 case sg2_herbi_one:
    if ( m_ev->m_lock || m_farm->DoIt(  (int) (67*cfg_herbi_app_prop.value()  * SG2_DECIDE_TO_HERB * m_farm->Prob_multiplier()))) { //modified probability
			if (!m_farm->HerbicideTreat( m_field, 0.0, g_date->DayInYear( 25, 4 ) - g_date->DayInYear())) {
				SimpleEvent( g_date->Date() + 1, sg2_herbi_zero, true );
				break;
			 }
    }
    break;

  case sg2_fungi_zero:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (17 * m_farm->Prob_multiplier()) )) { //modified probability

		//new - for decision making
		TTypesOfVegetation tov = m_field->GetVegType();
		if(!m_ev->m_lock && !m_farm->Spraying_fungins(tov)){
			Field * pf =  dynamic_cast<Field*>(m_field); 
			pf->Add_missed_fi_app();
			break;
		} //end of the part for dec. making

      if ( SG2_WATER_DATE > g_date->Date() - 2 && SG2_WATER_DATE < g_date->Date() + 2 ) {
        SimpleEvent( g_date->Date() + 1, sg2_fungi_zero, true );
		break;
      }
      if ( !m_farm->FungicideTreat( m_field, 0.0,
				    g_date->DayInYear( 1, 6 ) -
				    g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sg2_fungi_zero, true );
        break;
      }
      SG2_FUNGI_DATE = g_date->Date();
    }
    // End of thread.
    break;

  case sg2_water_zero:
    if ( m_ev->m_lock || m_farm->DoIt( 70 )) {
      if ( SG2_FUNGI_DATE > g_date->Date() - 2 &&
           SG2_FUNGI_DATE < g_date->Date() + 2 ) {
        SimpleEvent( g_date->Date() + 1, sg2_water_zero, true );
        break;
      }
      if ( !m_farm->Water( m_field, 0.0,
			   g_date->DayInYear( 30, 5 ) -
			   g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sg2_water_zero, true );
	break;
      }
      SG2_WATER_DATE = g_date->Date();
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 6 ),
		   sg2_water_zero_b, false );
      break;
    }
    // Din't water, so skip second one too.
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 7 ),
		 sg2_swarth, false );
    break;

  case sg2_water_zero_b:
    if ( m_ev->m_lock || m_farm->DoIt( 70 )) {
      if ( SG2_FUNGI_DATE > g_date->Date() - 2 &&
           SG2_FUNGI_DATE < g_date->Date() + 2 ) {
        SimpleEvent( g_date->Date() + 1, sg2_water_zero_b, true );
        break;
      }
      if ( !m_farm->Water( m_field, 0.0,
			   g_date->DayInYear( 30, 6 ) -
			   g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sg2_water_zero_b, true );
	break;
      }
      SG2_WATER_DATE = g_date->Date();
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 7 ),
		 sg2_swarth, false );
    break;

  case sg2_swarth:
    if ( m_ev->m_lock || m_farm->DoIt( 5 )) {
      if ( !m_farm->Swathing( m_field, 0.0,
			      g_date->DayInYear( 15, 7 ) -
			      g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sg2_swarth, true );
        break;
      } 

	  ChooseNextCrop (5);

      SimpleEvent( g_date->Date() + 2, sg2_harvest, false );
      break;
    }

	ChooseNextCrop (5);

    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 7 ),
		 sg2_harvest, false );
    break;

  case sg2_harvest:
     if (m_field->GetMConstants(0)==0) {
		if (!m_farm->Harvest( m_field, 0.0, -1)) { //raise an error
			g_msg->Warn( WARN_BUG, "SeedGrass2::Do(): failure in 'Harvest' execution", "" );
			exit( 1 );
		} 
	}
	else { 
		if ( !m_farm->Harvest( m_field, 0.0, m_field->GetMDates(1,0) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, sg2_harvest, false );
			break;
		}
	}
    SimpleEvent( g_date->Date(), sg2_strawchopping, false );
    break;

  case sg2_strawchopping:
    if ( m_ev->m_lock || m_farm->DoIt( 30 )) {
		if (m_field->GetMConstants(0)==0) {
			if (!m_farm->StrawChopping( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "SeedGrass2::Do(): failure in 'StrawChopping' execution", "" );
				exit( 1 );
			} 
		}
		else { 
			if ( !m_farm->StrawChopping( m_field, 0.0, m_field->GetMDates(1,0) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, sg2_strawchopping, true );
			break;
			}
		}
		done=true;
		break;
    }
    SimpleEvent( g_date->Date()+1, sg2_compress, false );
    break;

  case sg2_compress:
    if ( m_ev->m_lock || m_farm->DoIt( 60 )) {
		if (m_field->GetMConstants(1)==0) {
				if (!m_farm->HayTurning( m_field, 0.0, -1)) { //raise an error
					g_msg->Warn( WARN_BUG, "SeedGrass2::Do(): failure in 'HayTurning' execution", "" );
					exit( 1 );
				} 
		}
		else { 
		  if ( !m_farm->HayTurning( m_field, 0.0, m_field->GetMDates(1,1) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, sg2_compress, true );
			break;
		  }
		}
    }
    {
      d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + m_field->GetMDates(0,3))
		d1 = g_date->OldDays() + m_field->GetMDates(0,3);
      SimpleEvent( d1, sg2_burnstrawstubble, false );
    }
    break;

  case sg2_burnstrawstubble:
    if ( m_ev->m_lock || m_farm->DoIt( 70 )) {
		if (m_field->GetMConstants(3)==0) {
			if (!m_farm->BurnStrawStubble( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "SeedGrass2::Do(): failure in 'BurnStrawStubble' execution", "" );
				exit( 1 );
			} 
		}
		else { 
			if ( !m_farm->BurnStrawStubble( m_field, 0.0, m_field->GetMDates(1,3) - g_date->DayInYear())) {
				SimpleEvent( g_date->Date() + 1, sg2_burnstrawstubble, true );
				break;
			}
		}
		done = true;
		break;
    }
    SimpleEvent( g_date->Date(), sg2_growthregulation, false );
    break;

  case sg2_growthregulation:
    if ( m_ev->m_lock || m_farm->DoIt( 20 )) {
		if (m_field->GetMConstants(4)==0) {
			if (!m_farm->GrowthRegulator( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "SeedGrass2::Do(): failure in 'GrowthRegulator' execution", "" );
				exit( 1 );
			} 
		}
		else { 		
			if ( !m_farm->GrowthRegulator( m_field, 0.0, m_field->GetMDates(0,4) - g_date->DayInYear())) {
				SimpleEvent( g_date->Date() + 1, sg2_growthregulation, true );
				break;
			}
		}
		done=true;
    }
    SimpleEvent( g_date->Date(), sg2_stubbleharrow, false );
    break;

  case sg2_stubbleharrow:
    if (m_field->GetMConstants(4)==0) {
		if (!m_farm->StubbleHarrowing( m_field, 0.0, -1)) { //raise an error
			g_msg->Warn( WARN_BUG, "SeedGrass2::Do(): failure in 'StubbleHarrowing' execution", "" );
			exit( 1 );
		} 
	}
	else { 
		if ( !m_farm->StubbleHarrowing( m_field, 0.0, m_field->GetMDates(1,4) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, sg2_stubbleharrow, true );
			break;
		}
	}
    done=true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "SeedGrass2::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


