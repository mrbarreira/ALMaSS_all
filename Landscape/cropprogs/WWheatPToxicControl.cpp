///
// WWheatPToxicControl.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/WWheatPToxicControl.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgFloat cfg_pest_product_1_amount;


bool WWheatPToxicControl::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;

  switch ( m_ev->m_todo )
  {
  case wwptc_start:
    {
      WWTC_AUTUMN_PLOUGH         = false;
      WWTC_WAIT_FOR_PLOUGH       = false;
      int d1 = g_date->OldDays() + g_date->DayInYear( 15,8 );
      if (g_date->Date() >= d1)
      {
         // if we are really too late then lie and say we are done
         if (g_date->DayInYear()>g_date->DayInYear(1,10)) {
           // ---FN---
         	 done=true;
	         break;
         }
      }
      if (g_date->Date() >= d1) d1 += 365;
      if (m_farm->IsStockFarmer()) // StockFarmer
      {
        SimpleEvent( d1, wwptc_ferti_s1, false );
      }
      else SimpleEvent( d1, wwptc_ferti_p1, false );
    }
    break;

  case wwptc_ferti_p1:
    if ( m_ev->m_lock || m_farm->DoIt( 0 ))
    {
      if (!m_farm->FP_Slurry( m_field, 0.0,
           g_date->DayInYear( 1,10 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_ferti_p1, true );
        break;
      }
    }
    SimpleEvent( g_date->Date(),wwptc_autumn_plough, false );
    break;

  case wwptc_ferti_s1:
    if (!m_farm->FA_Slurry( m_field, 0.0,
           g_date->DayInYear( 1,10 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wwptc_ferti_s1, true );
      break;
    }
    // This may cause two applications of fertilizer in one day...
    // --FN--
    SimpleEvent( g_date->Date(),wwptc_ferti_s2, false );
    // --FN--
    break;

  case wwptc_ferti_s2:
    if ( m_ev->m_lock || m_farm->DoIt( 0 ))
    {
      if (!m_farm->FA_Manure( m_field, 0.0,
           g_date->DayInYear( 1,10 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_ferti_s2, true );
        break;
      }
    }
    SimpleEvent( g_date->Date(),wwptc_autumn_plough, false );
    break;

  case wwptc_autumn_plough:
    if ( m_ev->m_lock || m_farm->DoIt( 100 ))
    {
      if (!m_farm->AutumnPlough( m_field, 0.0,
           g_date->DayInYear( 1,10 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_autumn_plough, true );
        break;
      }
      else
      {
        WWTC_AUTUMN_PLOUGH=true;
        SimpleEvent( g_date->Date()+1,wwptc_autumn_harrow, false );
        break;
      }
    }
    SimpleEvent( g_date->Date()+1,wwptc_stubble_harrow1, false );
    break;

  case wwptc_autumn_harrow:
    if (!m_farm->AutumnHarrow( m_field, 0.0,
           g_date->DayInYear( 10,10 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wwptc_autumn_harrow, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,9 ),
               wwptc_autumn_sow, false );
    break;

  case wwptc_stubble_harrow1:
    if (!m_farm->StubbleHarrowing( m_field, 0.0,
           g_date->DayInYear( 10,10 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wwptc_stubble_harrow1, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,9 ),
               wwptc_autumn_sow, false );
    break;

  case wwptc_autumn_sow:
    if (!m_farm->AutumnSow( m_field, 0.0,
           g_date->DayInYear( 20,10 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wwptc_autumn_sow, true );
      break;
    }
		// --FN-- Oooo!
    //SimpleEvent( g_date->OldDays() + g_date->Date( ),
    //               wwptc_autumn_roll, false );
		SimpleEvent( g_date->Date() + 1, wwptc_autumn_roll, false );
    break;

  case wwptc_autumn_roll:
    if (( m_ev->m_lock || m_farm->DoIt( 0 ))&& (WWTC_AUTUMN_PLOUGH))
    {
      if (!m_farm->AutumnRoll( m_field, 0.0,
           g_date->DayInYear( 27,10 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_autumn_roll, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,9 ),
                      wwptc_ferti_p2, false );
    break;

  case wwptc_ferti_p2:
    if (( m_ev->m_lock || m_farm->DoIt( 0 )) && (!m_farm->IsStockFarmer()))
    {
      if ( m_field->GetVegBiomass()>0)
      //only when there has been a bit of growth
      {
        if (!m_farm->FP_ManganeseSulphate( m_field, 0.0,
           g_date->DayInYear( 30,10 ) - g_date->DayInYear()))
        {
          SimpleEvent( g_date->Date() + 1, wwptc_ferti_p2, true );
          break;
        }
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,9 ),
                      wwptc_herbicide1, false );
    break;

  case wwptc_herbicide1:
    if ( m_ev->m_lock || m_farm->DoIt( (int) ( 100*cfg_herbi_app_prop.value() )))
    {
      if (!m_farm->HerbicideTreat( m_field, 0.0,
             g_date->DayInYear( 5,10 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_herbicide1, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 )+365,
             wwptc_spring_roll, false );
    break;

  case wwptc_spring_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 0 ))
    {
      if (!m_farm->SpringRoll( m_field, 0.0,
           g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_spring_roll, true );
        break;
      }
    }
    if (m_farm->IsStockFarmer()) // StockFarmer
    {
      SimpleEvent( g_date->Date() + 1, wwptc_ferti_s3, false );
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,4 ),
                    wwptc_ferti_s4, false );
    }
    else
    	SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,3 ),
                   wwptc_ferti_p3, false );
    // All need the next threads
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,4 ),
             wwptc_herbicide2, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,4 ),
             wwptc_GR, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,4 ),
             wwptc_fungicide, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,5 ),
             wwptc_insecticide1, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,4 ),
             wwptc_strigling1, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,5 ),
             wwptc_water1, false );
    break;

  case wwptc_herbicide2:
    if ( m_ev->m_lock || m_farm->DoIt( (int) ( 1*cfg_herbi_app_prop.value() )))
    {
      if (!m_farm->HerbicideTreat( m_field, 0.0,
             g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_herbicide2, true );
        break;
      }
    }
    // End of thread
    break;

  case wwptc_GR:
    if ( m_ev->m_lock || m_farm->DoIt( 0 ))
    {
    	// --FN--
      if (!m_farm->GrowthRegulator( m_field, 0.0,
             g_date->DayInYear( 10,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_GR, true );
        break;
      }
    }
    // End of thread
    break;

  case wwptc_fungicide:
    if ( m_ev->m_lock || m_farm->DoIt( 100 ))
    {
      if (!m_farm->FungicideTreat( m_field, 0.0,
             g_date->DayInYear( 10,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_fungicide, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,5 ),
             wwptc_fungicide2, false );
    break;

  case wwptc_fungicide2:
    if ( m_ev->m_lock || m_farm->DoIt( 100 ))
    {
      if (!m_farm->FungicideTreat( m_field, 0.0,
             g_date->DayInYear( 15,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_fungicide2, true );
        break;
      }
    }
    // End of thread
    break;

  case wwptc_insecticide1:
    if ( m_ev->m_lock || m_farm->DoIt( 100 ))
    {
      if (!m_farm->ProductApplication(m_field, 0, cfg_pest_product_1_amount.value(), cfg_pest_product_1_amount.value(), ppp_1)) {
        SimpleEvent( g_date->Date() + 1, wwptc_insecticide1, true );
        break;
      }
     	else
        {
          SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,6 ),
                   wwptc_insecticide2, false );
          break;
        }
    }
    break;

  case wwptc_insecticide2:
    if ( m_ev->m_lock || m_farm->DoIt( 100 ))
    {
      if (!m_farm->ProductApplication(m_field, 0, cfg_pest_product_1_amount.value(), cfg_pest_product_1_amount.value(), ppp_1)) {
        SimpleEvent( g_date->Date() + 1, wwptc_insecticide2, true );
        break;
      }
      else
      {
        if ((g_date->Date()+7)<( g_date->OldDays() + g_date->DayInYear( 15,6 )))
          SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,6 ),
             wwptc_insecticide3, false );
        else
        	SimpleEvent( g_date->Date()+7, wwptc_insecticide3, false );
        break;
      }
    }
    break;

  case wwptc_insecticide3:
    if ( m_ev->m_lock || m_farm->DoIt( 100 ))
    {
      if (!m_farm->ProductApplication(m_field, 0, cfg_pest_product_1_amount.value(), cfg_pest_product_1_amount.value(), ppp_1)) {
        SimpleEvent( g_date->Date() + 1, wwptc_insecticide3, true );
        break;
      }
    }
    // End of thread
    break;

  case wwptc_strigling1:
    if ( m_ev->m_lock || m_farm->DoIt( 0 ))
    {
      if (!m_farm->Strigling( m_field, 0.0,
             g_date->DayInYear( 25,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_strigling1, true );
        break;
      }
      else
      {
        if ((g_date->Date()+7)<( g_date->OldDays() + g_date->DayInYear( 15,6 )))
              SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,4),
    	               wwptc_strigling2, false );
        else
        	SimpleEvent( g_date->Date()+7,wwptc_strigling2, false );
      }
    }
    break;

  case wwptc_strigling2:
      if (!m_farm->Strigling( m_field, 0.0,
             g_date->DayInYear( 5,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_strigling2, true );
        break;
    }
    // End of thread
    break;

  case wwptc_water1:
    if ( m_ev->m_lock || m_farm->DoIt( 0 )) // **CJT** Soil type 1-4 only
    {
      if (!m_farm->Water( m_field, 0.0,
             g_date->DayInYear( 15,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_water1, true );
        break;
      }
      else
        if ((g_date->Date()+5)<( g_date->OldDays() + g_date->DayInYear( 2,5 )))
            SimpleEvent( g_date->OldDays() + g_date->DayInYear( 2,5 ),
                         wwptc_water2, false );
        else
          SimpleEvent( g_date->Date()+5, wwptc_water2, false );
    }
    break;

  case wwptc_water2:
    if (!m_farm->Water( m_field, 0.0,
           g_date->DayInYear( 1,6 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wwptc_water2, true );
      break;
    }
    // End of thread
    break;

  case wwptc_ferti_p3:
    if (!m_farm->FP_NPK( m_field, 0.0,
         g_date->DayInYear( 15,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wwptc_ferti_p3, true );
      break;
    }

    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,4 ),
                   wwptc_ferti_p4, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
                   wwptc_ferti_p5, false );
    break;

  case wwptc_ferti_p4:
    if ( m_ev->m_lock || m_farm->DoIt( 100 ))
    {
      if (!m_farm->FP_NPK( m_field, 0.0,
             g_date->DayInYear( 15,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_ferti_p4, true );
        break;
      }
    }

    // The Main thread
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,8 ),
                 wwptc_harvest, false );
    break;

   case wwptc_ferti_p5:
    if ( m_ev->m_lock || m_farm->DoIt( 0 ))
    {
      if (!m_farm->FP_ManganeseSulphate( m_field, 0.0,
             g_date->DayInYear( 5,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_ferti_p5, true );
        break;
      }
    }
    break;

  case wwptc_ferti_s3:
    if (!m_farm->FA_Slurry(m_field, 0.0,
         g_date->DayInYear( 30,5 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wwptc_ferti_s3, true );
      break;
    }
    // The Main thread
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,8 ),
                 wwptc_harvest, false );
    break;

  case wwptc_ferti_s4:
    if ( m_ev->m_lock || m_farm->DoIt( 100 ))
    {
      if (!m_farm->FA_NPK( m_field, 0.0,
           g_date->DayInYear( 20,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_ferti_s4, true );
        break;
      }
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 21,4 ),
                   wwptc_ferti_s5, false );
    }
   break;

  case wwptc_ferti_s5:
    if ( m_ev->m_lock || m_farm->DoIt( 0 ))
    {
      if (!m_farm->FA_NPK( m_field, 0.0,
           g_date->DayInYear( 1,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_ferti_s5, true );
        break;
      }
    }
    break;

  case wwptc_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
           g_date->DayInYear( 20,8 ) - g_date->DayInYear()))
    {
        SimpleEvent( g_date->Date() + 1, wwptc_harvest, true );
        break;
    }
    SimpleEvent( g_date->Date(), wwptc_straw_chopping, false );
    break;

  case wwptc_straw_chopping:
    if ( m_ev->m_lock || m_farm->DoIt( 0 ))
    {
      if (!m_farm->StrawChopping( m_field, 0.0,
           g_date->DayInYear( 20,8 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_straw_chopping, true );
        break;
      }
      else
      {
        if ((g_date->Date()+5)<( g_date->OldDays() + g_date->DayInYear( 2,5 )))
                 SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,8 ),
                   wwptc_stubble_harrow2, false );
        else
          SimpleEvent( g_date->Date()+1, wwptc_stubble_harrow2, false );
      }
    } else {
	    SimpleEvent( g_date->Date()+1, wwptc_hay_turning, false );
    }
   break;

  case wwptc_hay_turning:
    if ( m_ev->m_lock || m_farm->DoIt( 0 ))
    {
      if (!m_farm->HayTurning( m_field, 0.0,
           g_date->DayInYear( 20,8 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_hay_turning, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,8 ),
                 wwptc_hay_baling, false );
    break;

  case wwptc_hay_baling:
    if (!m_farm->HayBailing( m_field, 0.0,
       g_date->DayInYear( 25,8 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wwptc_hay_baling, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,8 ),
                   wwptc_stubble_harrow2, false );
    break;

  case wwptc_stubble_harrow2:
    if ( m_ev->m_lock || m_farm->DoIt( 100 ))
    {
      if (!m_farm->StubbleHarrowing( m_field, 0.0,
           g_date->DayInYear( 15,9 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_stubble_harrow2, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,8 ),
                   wwptc_grubning, false );
    break;

  case wwptc_grubning:
    if ( m_ev->m_lock || m_farm->DoIt( 0 ))
    {
      if (!m_farm->DeepPlough( m_field, 0.0,
           g_date->DayInYear( 15,10 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wwptc_grubning, true );
        break;
      }
    }
    done=true;
    // END OF MAIN THREAD
    break;

  default:
    g_msg->Warn( WARN_BUG, "WheatPControl::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}
