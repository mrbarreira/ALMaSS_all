//
// WWheatPTreatment.h
//
/*

Copyright (c) 2003, National Environmental Research Institute, Denmark (NERI)

All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef WWHEATPTREATMENT_H
#define WWHEATPTREATMENT_H

#define WWHEATPTREATMENT_BASE 60050
#define WWP_WAIT_FOR_PLOUGH       m_field->m_user[0]
#define WWP_AUTUMN_PLOUGH         m_field->m_user[1]

typedef enum {
  wwpt_start = 1, // Compulsory, must always be 1 (one).
  wwpt_sleep_all_day = WWHEATPTREATMENT_BASE,
  wwpt_ferti_p1,
  wwpt_ferti_s1,
  wwpt_ferti_s2,
  wwpt_autumn_plough,
  wwpt_autumn_harrow,
  wwpt_stubble_harrow1,
  wwpt_autumn_sow,
  wwpt_autumn_roll,
  wwpt_ferti_p2,
  wwpt_herbicide1,
  wwpt_spring_roll,
  wwpt_herbicide2,
  wwpt_GR,
  wwpt_fungicide,
  wwpt_fungicide2,
  wwpt_insecticide1,
  wwpt_insecticide2,
  wwpt_insecticide3,
  wwpt_strigling1,
  wwpt_strigling2,
  wwpt_water1,
  wwpt_water2,
  wwpt_ferti_p3,
  wwpt_ferti_p4,
  wwpt_ferti_p5,
  wwpt_ferti_s3,
  wwpt_ferti_s4,
  wwpt_ferti_s5,
  wwpt_harvest,
  wwpt_straw_chopping,
  wwpt_hay_turning,
  wwpt_hay_baling,
  wwpt_stubble_harrow2,
  wwpt_grubning,
} WinterWheatTreatmentToDo;


class WWheatPTreatment: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   WWheatPTreatment()
   {
     m_first_date=g_date->DayInYear( 1,10 );
   }
};

#endif // WHEATPCONTROL_H
