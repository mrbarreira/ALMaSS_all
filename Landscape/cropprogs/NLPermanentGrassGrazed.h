/**
\file
\brief
<B>NLPermanentGrassGrazed.h This file contains the headers for the PermanentGrassGrazed class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLPermanentGrassGrazed.h
//


#ifndef NLPERMANENTGRASSLANDGRAZED_H
#define NLPERMANENTGRASSLANDGRAZED_H

#define NLPERMANENTGRASSLANDGRAZED_BASE 26000
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_PGG_FERTI_DATE		a_field->m_user[1]
#define NL_PGG_CUT_DATE		a_field->m_user[2]
#define NL_PGG_WATER_DATE		a_field->m_user[3]


/** Below is the list of things that a farmer can do if he is growing PermanentGrassGrazed, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_pgg_start = 1, // Compulsory, must always be 1 (one).
	nl_pgg_sleep_all_day = NLPERMANENTGRASSLANDGRAZED_BASE,
	nl_pgg_ferti_p1,
	nl_pgg_ferti_s1,
	nl_pgg_cut_to_silage1,
	nl_pgg_cut_to_silage2,
	nl_pgg_cut_to_silage3,
	nl_pgg_cut_to_silage4,
	nl_pgg_cut_to_silage5,
	nl_pgg_cut_to_silage6,
	nl_pgg_ferti_p2,
	nl_pgg_ferti_s2,
	nl_pgg_ferti_p3,
	nl_pgg_ferti_s3,
	nl_pgg_ferti_p4,
	nl_pgg_ferti_s4,
	nl_pgg_ferti_p5,
	nl_pgg_ferti_s5,
	nl_pgg_ferti_p6,
	nl_pgg_ferti_s6,
	nl_pgg_ferti_p7,
	nl_pgg_ferti_s7,
	nl_pgg_ferti_p8,
	nl_pgg_ferti_s8,
	nl_pgg_ferti_p9,
	nl_pgg_ferti_s9,
	nl_pgg_ferti_p10,
	nl_pgg_ferti_s10,
	nl_pgg_ferti_p11,
	nl_pgg_ferti_s11,
	nl_pgg_watering,
	nl_pgg_cattle_out,
	nl_pgg_cattle_is_out,
} NLPermanentGrassGrazedToDo;


/**
\brief
NLPermanentGrassGrazed class
\n
*/
/**
See NLPermanentGrassGrazed.h::NLPermanentGrassGrazedToDo for a complete list of all possible events triggered codes by the PermanentGrassGrazed management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLPermanentGrassGrazed: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLPermanentGrassGrazed()
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 30,3 );
   }
};

#endif // NLPERMANENTGRASSLANDGRAZED_H

