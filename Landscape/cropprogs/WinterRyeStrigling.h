//
// WinterRyeStrigling.h
//
/*

Copyright (c) 2003, National Environmental Research Institute, Denmark (NERI)

All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef WinterRyeStrigling_H
#define WinterRyeStrigling_H

#define WRYES_BASE 30700
#define WRYS_DID_MANURE          m_field->m_user[0]
#define WRYS_DID_SLUDGE          m_field->m_user[1]

typedef enum {
  wrys_start = 1, // Compulsory, start event must always be 1 (one).
  wrys_fertmanure_stock = WRYES_BASE,
  wrys_fertsludge_stock,
  wrys_autumn_plough,
  wrys_autumn_harrow,
  wrys_autumn_sow,
  wrys_autumn_roll,
  wrys_fertmanganese_plant_one,
  wrys_fertnpk_plant,
  wrys_fertmanganese_plant_two,
  wrys_fertslurry_stock,
  wrys_fert_ammonium_stock,
  wrys_spring_roll,
  wrys_growth_reg_one,
  wrys_strigling_one,
  wrys_strigling_two,
  wrys_strigling_three,
  wrys_growth_reg_two,
  wrys_fungicide,
  wrys_insecticide,
  wrys_water,
  wrys_harvest,
  wrys_straw_chopping,
  wrys_hay_turning,
  wrys_hay_bailing,
  wrys_stubble_harrowing
} WinterRyeSToDo;



class WinterRyeStrigling: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  WinterRyeStrigling()
  {
      m_first_date=g_date->DayInYear(10,10);
  }
};

#endif // WinterRye_H
