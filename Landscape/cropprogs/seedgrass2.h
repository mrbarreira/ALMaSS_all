//
// seedgrass2.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef SEEDGRASS2_H
#define SEEDGRASS2_H

#define SEEDGRASS2_BASE 500
#define SG2_WATER_DATE     m_field->m_user[0]
#define SG2_FUNGI_DATE     m_field->m_user[1]
#define SG2_DECIDE_TO_HERB m_field->m_user[2]

typedef enum {
  sg2_start = 1, // Compulsory, start event must always be 1 (one).
  sg2_ferti_zero = SEEDGRASS2_BASE,
  sg2_herbi_zero,
  sg2_herbi_one,
  sg2_fungi_zero,
  sg2_water_zero,
  sg2_water_zero_b,
  sg2_swarth,
  sg2_harvest,
  sg2_strawchopping,
  sg2_burnstrawstubble,
  sg2_compress,
  sg2_growthregulation,
  sg2_stubbleharrow
} SeedGrass2ToDo;



class SeedGrass2: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  SeedGrass2()
  {
    m_first_date=g_date->DayInYear(14,3);
  }

};

#endif // SEEDGRASS1_H
