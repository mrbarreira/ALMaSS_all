//
// WinterWheatStriglingCulm.h
//
/*

Copyright (c) 2005, National Environmental Research Institute, Denmark (NERI)

All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef WINTERWHEATStriglingCulm_H
#define WINTERWHEATStriglingCulm_H

#define WINTERWHEATStriglingCulm_BASE 5180
#define WWStriglingCulm_WAIT_FOR_PLOUGH       m_field->m_user[0]
#define WWStriglingCulm_AUTUMN_PLOUGH         m_field->m_user[1]

typedef enum {
  wwsc_start = 1, // Compulsory, must always be 1 (one).
  wwsc_sleep_all_day = WINTERWHEATStriglingCulm_BASE,
  wwsc_ferti_p1,
  wwsc_ferti_s1,
  wwsc_ferti_s2,
  wwsc_autumn_plough,
  wwsc_autumn_harrow,
  wwsc_stubble_harrow1,
  wwsc_autumn_sow,
  wwsc_autumn_roll,
  wwsc_ferti_p2,
  wwsc_strigling0a,
  wwsc_spring_roll,
  wwsc_strigling0b,
  wwsc_GR,
  wwsc_fungicide,
  wwsc_fungicide2,
  wwsc_insecticide1,
  wwsc_insecticide2,
  wwsc_insecticide3,
  wwsc_strigling1,
  wwsc_strigling2,
  wwsc_water1,
  wwsc_water2,
  wwsc_ferti_p3,
  wwsc_ferti_p4,
  wwsc_ferti_p5,
  wwsc_ferti_s3,
  wwsc_ferti_s4,
  wwsc_ferti_s5,
  wwsc_harvest,
  wwsc_straw_chopping,
  wwsc_hay_turning,
  wwsc_hay_baling,
  wwsc_stubble_harrow2,
  wwsc_grubning,
} WinterWheatStriglingCulmToDo;


class WinterWheatStriglingCulm: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   WinterWheatStriglingCulm()
   {
     m_first_date=g_date->DayInYear( 1,10 );
   }
};

#endif // WINTERWHEATStriglingCulm_H
