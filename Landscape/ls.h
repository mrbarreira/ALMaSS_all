/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
//
// ls.h
//


#ifndef LS_H
#define LS_H

#include <array>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
using namespace std;
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/configurator.h"
#include "../BatchALMaSS/BoostRandomGenerators.h"
extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;
#include "../RodenticideModelling/Rodenticide.h"
#include "../Landscape/maperrormsg.h"
#include "../Landscape/rastermap.h"
#include "../Landscape/treatment.h"
#include "../Landscape/plants.h"
#include "../Landscape/elements.h"
#include "../Landscape/croprotation.h"
#include "../Landscape/lowqueue.h"
#include "../Landscape/treatment.h"
#include "../Landscape/farm.h"
#include "../Landscape/calendar.h"
#include "../Landscape/weather.h"
#include "../Landscape/tole_declaration.h"
#include "../Landscape/tov_declaration.h"
#include "../BatchALMaSS/CurveClasses.h"
#include "../Landscape/landscape.h"
#include "../Landscape/pesticide.h"
#include "../Landscape/Misc.h"


#endif
