//
// pesticide.h
//
/*
*******************************************************************************************************
Copyright (c) 2003, Christopher John Topping, EcoSol
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef PESTICIDE_H
#define PESTICIDE_H

// Side length of a square in the pesticide map is given by this parameter
// as a power of two. Ie. 3 will give a side length of 8 main map units
// as 2^3 equals 8, which must also be set below
//
// Unfortunately it would be a huge performance penalty to have to make
// this a runtime configuration variable, it has to be compiled in.
// Standard rough settings here:

#define PEST_GRIDSIZE_POW2 0
#define PEST_GRIDSIZE      1
#define PEST_GRIDAREA      1  // i.e. PEST_GRIDSIZE^2
/*
#define PEST_GRIDSIZE_POW2 2
#define PEST_GRIDSIZE      4
#define PEST_GRIDAREA      16  // i.e. PEST_GRIDSIZE^2
*/


// Consider undef for production code, no speed penalty if defined.
#define PEST_DEBUG

// Check for water bodies when asking for pesticide concentration
// at main map coords x,y? Speed vs. reality tradeoff. You
// probably want this defined.
//#define PEST_WATER_CHECK


extern class Pesticide *g_pest;


struct PesticideEvent
{
	/**
	* Records the amount sprayed per unit area of a polygon. Each cell in the polygon is assumed to receive this amount (effectively the field rate per unit area).
	*/
  LE*   m_sprayed_elem;
  double m_amount;
  PlantProtectionProducts m_ppp;

  PesticideEvent(LE* a_sprayed_elem, double a_amount, PlantProtectionProducts a_ppp) {
	  m_sprayed_elem = a_sprayed_elem;
	  m_amount = a_amount;
	  m_ppp = a_ppp;
  }
};

class Diffusor
{
	/**
	Diffusor is the fraction of the total amount sprayed at 0,0 at the co-ordinates given by m_dx, and m_dy, relative to the origin at 0,0.
	If a pesticide dispersed 10m from the point source in all directions the grid would be 21 x 21 squares, each with its own diffusor. This grid
	of diffusors forms the Diffusion_mask
	*/
  int   m_dx;
  int   m_dy;
  double m_fraction;

public:
  Diffusor( int a_dx, int a_dy, double a_fraction ) {
    m_dx       = a_dx;
    m_dy       = a_dy;
    m_fraction = a_fraction;
  }

  void SetFraction(double a_frac) { m_fraction = a_frac; }
  double GetFraction() { return m_fraction; }
  int Getdx() { return m_dx; }
  int Getdy() { return m_dy; }

};

/** \brief The grid of diffusors for each point in a polygon that is sprayed. */
typedef vector <Diffusor*> Diffusion_mask;


class Pesticide
{
  // Method call stack and ordering:
  //
  // Pesticide()
  //   DiffusionMaskInit()
  //     DiffusionFunction()
  //
  // Tick()
  //   MainMapDecay()
  //   DailyQueueProcess()
  //     TwinMapClear()
  //     TwinMapSpray()
  //       TwinMapSprayPixel()
  //       TwinMapSprayCorrectBorders()
  //     TwinMapCopyToMask()
  //     TwinMapDiffusion()
  //       DiffusionSprayPixel()
  //         ElementIsWater()
  //     TwinMapAddToMain()
  //   DailyQueueClear()
  //
  // SupplyPesticide()
  // #ifdef PEST_WATER_CHECK
  //   ElementIsWater()
  // #endif

 protected:
  /** \brief 
  * Speed hack. Only actually run the daily decay routine on the
  * pesticide map if and only if we are sure that there *is*
  * some pesticide anywhere on the map for us to process.
  * 
  * Cleared daily by MainMapDecay().
  * 
  * Set to true when either adding pesticide to the map, or by the
  * MainMapDecay() method itself, when it finds remains of pesticide
  * anywhere on the pesticide map during its run.
  * One copy is needed for each active PPP
  */
  bool  m_something_to_decay[ppp_foobar];

  /** \brief The number of active PPPs to track */
  int m_NoPPPs;

  // Main map coordinates beyond which we have to use special
  // proportional constants when adding new amounts of pesticide
  // to the pesticide map.
  int   m_x_excess;
  int   m_y_excess;
  double m_prop;
  double m_corr_x;
  double m_corr_y;
  int m_wind;

  // Daily decay fraction, precalculated from the pesticide
  // half life parameter, for general, vegetation and soil
  double m_pest_daily_decay_frac;
  double m_pest_daily_decay_frac_Veg;
  double m_pest_daily_decay_frac_Soil;

  // Local copy of pointer to the main map object.
  RasterMap *m_land;

  // Local copy of pointer to the main landscape object.
  Landscape *m_map;

  // The pesticide map itself, and its twins used when adding newly
  // sprayed squares to the main map.
#ifdef __DETAILED_PESTICIDE_FATE
  vector<vector <double>  >     m_pest_map_vegcanopy;
  vector<vector <double>  >     m_pest_map_soil;
#else
	  vector<vector <double>  >     m_pest_map_main;
#endif
	  /** \brief The complete pesticide map */
	  double       *m_pest_map_twin;
	  /** \brief The total size of one map in cellsize resolution */
	  unsigned int m_pest_map_size;
	  /** \brief The width of one map in cellsize resolution */
	  unsigned int m_pest_map_width;
	  /** \brief The height of one map in cellsize resolution */
	  unsigned int m_pest_map_height;

	  /** \brief a structure to hold pre-calculated pesticide rain wash off factor (Rw) */
	  double m_RainWashoffFactor[10000];
	  /** \brief Daily rainfall saved here * 100 to use as an index to the Pesticide::m_RainWashoffFactor array - an optimisation to stop repeated calls to Landscape::SupplyRain */
	  unsigned m_rainfallcategory;

	  /** \brief  Pre-calculated square diffusion map, assuming wind directions (4) <br>
	  Used after spraying an element in the pesticide map to determine how much of the sprayed material spreads into the surroundings.*/
	  Diffusion_mask m_diffusion_mask[4];

	  /** \brief List lists of landscape elements, which was sprayed on a given day. One for each PPP we track */
	  vector<vector <PesticideEvent*>> m_daily_spray_queue;

	  void DailyQueueClear(PlantProtectionProducts a_ppp);
	  void DailyQueueProcess(PlantProtectionProducts a_ppp);

	  bool ElementIsWater( int a_x, int a_y );

	  void MainMapDecay(PlantProtectionProducts a_ppp);

	  void TwinMapClear( int a_minx, int a_miny, int a_maxx, int a_maxy );
	  void TwinMapSpray(  LE* a_element_spryaed, double a_amount, int a_minx, int a_miny, int a_maxx, int a_maxy );
	  void TwinMapSprayPixel( int a_large_map_x,
			  int a_large_map_y,
			  double a_fractional_amount );
	  void TwinMapSprayCorrectBorders( void );
	  //void TwinMapCopyToMask( int a_minx, int a_miny, int a_maxx, int a_maxy );
	  //void TwinMapAddToMain( void );
	  void TwinMapDiffusion( int a_minx, int a_miny, int a_maxx, int a_maxy, double a_cover, PlantProtectionProducts a_ppp);

	  void  DiffusionMaskInit( void );
	  double DiffusionFunction( double a_dist_meters );
	  void  DiffusionSprayPixel( int a_x, int a_limit_x, int a_y, int a_limit_y, double a_amount, double a_cover, PlantProtectionProducts a_ppp );
	  /** \brief Pre-calculates the constants required for rain wash off with increasing rainfall and stores this in #m_RainWashoffFactor */
	  void CalcRainWashOffFactors();

public:
  void  Tick( void );
  void  DailyQueueAdd( LE* a_element_sprayed, double a_amount, PlantProtectionProducts a_ppp);
  bool GetAnythingToDecay(PlantProtectionProducts a_ppp){ return (m_something_to_decay[a_ppp]); }
  // Doesn't make sense for the pesticide map, as one cannot guarantee
  // that all the area covered by a particular polygon has the
  // same concentration of pesticide, but as an approximation we use the
  // grid cell covering ValidX & ValidY for that polygon.
  //
#ifdef __DETAILED_PESTICIDE_FATE
  double SupplyPesticideS(int a_x, int a_y, PlantProtectionProducts a_ppp);
  double SupplyPesticideP(int a_x, int a_y, PlantProtectionProducts a_ppp);
  double SupplyPesticideS(int a_polyref, PlantProtectionProducts a_ppp);
  double SupplyPesticideP(int a_polyref, PlantProtectionProducts a_ppp);
#else
  double SupplyPesticide( int a_x, int a_y, PlantProtectionProducts a_ppp);
  double SupplyPesticide(int a_polyref, PlantProtectionProducts a_ppp);
#endif

  Pesticide( RasterMap *a_land, Landscape *a_map );
  virtual ~Pesticide( void );

#ifdef PEST_DEBUG
  /* *** For testing of the pesticide engine. */

  bool SavePPM( double *a_map,
                int a_beginx, int a_width,
                int a_beginy, int a_height,
                char* a_filename );

  void DiffusionMaskInitTest( void );
  //void Test( Landscape *a_map );
#endif

};



inline bool Pesticide::ElementIsWater( int a_x, int a_y )
{
  TTypesOfLandscapeElement l_eletype =
    m_map->SupplyElementType( a_x, a_y );

  if ( l_eletype == tole_Freshwater ||
       l_eletype == tole_River      ||
	   l_eletype == tole_Pond       ||
	   l_eletype == tole_FishFarm   ||
       l_eletype == tole_Saltwater
       )
    return true;

  return false;
}


#ifdef __DETAILED_PESTICIDE_FATE

inline double Pesticide::SupplyPesticideS(int a_x, int a_y, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_x the x-coord in landscape x units
	@param  a_y the y-coord in landscape x units
	@return Returns the value of the soil surface pesticide grid cell containing a_x,a_y
	*/
	int l_x = a_x >> PEST_GRIDSIZE_POW2;
	int l_y = a_y >> PEST_GRIDSIZE_POW2;

	return m_pest_map_soil[a_ppp][l_y * m_pest_map_width + l_x];
}

inline double Pesticide::SupplyPesticideP(int a_x, int a_y, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_x the x-coord in landscape x units
	@param  a_y the y-coord in landscape x units
	@return Returns the value of the plant canopy pesticide grid cell containing a_x,a_y
	*/
	int l_x = a_x >> PEST_GRIDSIZE_POW2;
	int l_y = a_y >> PEST_GRIDSIZE_POW2;

	return m_pest_map_vegcanopy[a_ppp][l_y * m_pest_map_width + l_x];
}

inline double Pesticide::SupplyPesticideS(int a_ele, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_ele the polygon reference number of the relevant polygon
	@return Returns the value of the soil pesticide grid cell for the polygon
	*/
	int l_c = m_map->SupplyPesticideCell(a_ele);
	return m_pest_map_soil[a_ppp][l_c];
}

inline double Pesticide::SupplyPesticideP(int a_ele, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_ele the polygon reference number of the relevant polygon
	@return Returns the value of the plant canopy pesticide grid cell for the polygon
	*/
	int l_c = m_map->SupplyPesticideCell(a_ele);
	return m_pest_map_vegcanopy[a_ppp][l_c];
}

#else
inline double Pesticide::SupplyPesticide(int a_x, int a_y, PlantProtectionProducts a_ppp)
{
	/**
	@param  a_x the x-coord in landscape x units
	@param  a_y the y-coord in landscape x units
	@return Returns the value of the pesticide grid cell containing a_x,a_y
	*/
#ifdef PEST_WATER_CHECK
	if (ElementIsWater(a_x, a_y))
		return 0.0;
#endif

	int l_x = a_x >> PEST_GRIDSIZE_POW2;
	int l_y = a_y >> PEST_GRIDSIZE_POW2;

	return m_pest_map_main[a_ppp][l_y * m_pest_map_width + l_x];
}

inline double Pesticide::SupplyPesticide(int a_ele, PlantProtectionProducts a_ppp)
{
	// This is an approximation because we have no idea about the actual variation
	// in pesticide concentrations within the polygon.
	int l_c = m_map->SupplyPesticideCell(a_ele);
	return m_pest_map_main[a_ppp][l_c];
}

#endif

inline void Pesticide::TwinMapSprayPixel(int a_large_map_x,
	int a_large_map_y,
	double a_fractional_amount)
{
	int l_x = a_large_map_x >> PEST_GRIDSIZE_POW2;
	int l_y = a_large_map_y >> PEST_GRIDSIZE_POW2;

	m_pest_map_twin[l_y * m_pest_map_width + l_x] +=
		a_fractional_amount;
}


class PesticideMap
{
	/**
	* PesticideMap is a class for handling pesticide mapping output. This can be used to sum up pesticide concentrations over time. It works currently for general pesticides
	* split into insecticides, herbicides, and fungicides, but also for the primary test pesticide if one is being used.
	*/
protected:
	/** \brief insecticide map data */
	vector<double> * m_pmap_insecticides;
	/** \brief herbicide map data */
	vector<double> * m_pmap_fungicides;
	/** \brief fungicide map data */
	vector<double> * m_pmap_herbicides;
	/** \brief first simultion year to record */
	int m_startyear;
	/** \brief last year of data to record */
	int m_endyear;
	/** \brief the size of the cell for pesticide data in m */
	int m_cellsize;
	/** \brief based on cellsize the width of the map */
	int m_pmap_width;
	/** \brief  based on cellsize the height of the map */
	int m_pmap_height;
	/** \brief true if using test pesticide, false for general pesticides */
	bool m_typeofmap;
	/** \brief pointer to the landscape  */
	Landscape* m_OurLandscape;
	/** \brief pointer to the landscape map */
	RasterMap* m_Rastermap;

public:
	PesticideMap(int a_startyear, int a_noyears, int a_cellsize, Landscape* a_landscape, RasterMap* a_land, bool a_typeofmap);
	~PesticideMap();
	bool DumpPMap(vector<double>* a_map);
	bool DumpPMapI() { return DumpPMap(m_pmap_insecticides); }
	bool DumpPMapH() { return DumpPMap(m_pmap_herbicides); }
	bool DumpPMapF() { return DumpPMap(m_pmap_fungicides); }
	void Spray(LE * a_element_sprayed, TTypesOfPesticideCategory a_type);

};
#endif // PESTICIDE_H
