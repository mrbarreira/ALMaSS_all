//
// plants.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/


#ifndef PLANTS_H
  #define PLANTS_H

#include <vector>
#include <algorithm>
#include <cstdio>
#include <fstream>
#include "tov_declaration.h"
#include "tole_declaration.h"

using namespace std;

const unsigned int MaxNoInflections = 15;

/**\brief Growth phase indicator*/
/**
janfirst = from Jan 1st
sow = after sowing
marchfirst = after march 1st
harvest1 = after first harvest/cut
harvest2 = after 2nd harvest/cut and subsequent ones (if different veg behaviour to '3') 
*/
typedef enum {
  janfirst = 0,
  sow,
  marchfirst,
  harvest1,
  harvest2,
  vegphase_foobar
}
Growth_Phases;



// 0: Leaf area green.
// 1: Leaf area total.
// 2: Height.

class CropGrowth {
public:
  bool m_lownut; // Low nutrient status.
  double m_dds[ 5 ] [ MaxNoInflections ]; // DDegs at inflection
  double m_slopes[ 5 ] [ 3 ] [ MaxNoInflections ];
  double m_start[ 5 ] [ 3 ];
  bool m_start_valid[ 5 ];

  CropGrowth( void );
};

extern class PlantGrowthData * g_crops;


class PlantGrowthData {
  vector < CropGrowth * > m_growth;
  vector < int > m_numbers;
  int m_num_crops;
  //FILE * m_ifile;
  ifstream m_ifile;
  double * m_weed_percent;
  double * m_bug_percent_a;
  double * m_bug_percent_b;
  double * m_bug_percent_c;
  int * m_bug_percent_d;

  double FindDiff( double a_ddegs, double a_yddegs, int a_plant, int a_phase, int a_type );
  unsigned int FindCropNum( ifstream& ist );
  //void SetVegNum( unsigned int a_i, const char * a_cropcurvefile );
  void SetVegNum( unsigned int a_i, ifstream& ist,const char * a_cropcurvefile );


  void MakeBugPercentArray( void );
  void ReadBugPercentageFile( void );

public:

  // NOTICE: These functions take *plant numbers* like in the
  // CropGrowth.txt file, *NOT* indices into the m_growth array.
	/** \brief Get the differential in LA green for the day degrees experienced */
	double GetLAgreenDiff(double a_ddegs, double a_yddegs, int a_plant, int a_phase) {
		return FindDiff(a_ddegs, a_yddegs, a_plant, a_phase, 0);
	}
	/** \brief Get the differential in LA total for the day degrees experienced */
	double GetLAtotalDiff(double a_ddegs, double a_yddegs, int a_plant, int a_phase) {
		return FindDiff(a_ddegs, a_yddegs, a_plant, a_phase, 1);
	}
	/** \brief Get the differential in veg height for the day degrees experienced */
	double GetHeightDiff(double a_ddegs, double a_yddegs, int a_plant, int a_phase) {
		return FindDiff(a_ddegs, a_yddegs, a_plant, a_phase, 2);
	}

	// The versions below allow scaling of the curve on calling
	/** \brief Get the differential in LA green for the day degrees experienced, scalable depending on plant growth ability */
	double GetLAgreenDiffScaled(double a_ddegs, double a_yddegs, int a_plant, int a_phase, double a_scaler) { return a_scaler* GetLAgreenDiff(a_ddegs, a_yddegs, a_plant, a_phase); }
	/** \brief Get the differential in LA total for the day degrees experienced, scalable depending on plant growth ability */
	double GetLAtotalDiffScaled(double a_ddegs, double a_yddegs, int a_plant, int a_phase, double a_scaler) { return a_scaler* GetLAtotalDiff(a_ddegs, a_yddegs, a_plant, a_phase); }
	/** \brief Get the differential in veg height for the day degrees experienced, scalable depending on plant growth ability */
	double GetHeightDiffScaled(double a_ddegs, double a_yddegs, int a_plant, int a_phase, double a_scaler) { return a_scaler* GetHeightDiff(a_ddegs, a_yddegs, a_plant, a_phase); }

	double GetStartValue(int a_veg_type, int a_phase, int a_type) {
		return m_growth[m_numbers[a_veg_type]]->m_start[a_phase][a_type];
	}

  bool StartValid( int a_veg_type, int a_phase );

  /* { return m_growth[ m_numbers[ a_veg_type ]]-> m_start_valid[ a_phase ]; } */
  // Number of different crops read from disk.
  int GetNumCrops() {
    return m_num_crops;
  }

  // Translation between growth curve file crop numbers and the internal
  // representation.
  //TTypesOfVegetation TranslateFileVegTypes( int a_filetype );
  //int BackTranslateFileVegTypes( TTypesOfVegetation a_systemtype );
  int VegTypeToCurveNum( TTypesOfVegetation VegReference );

  // **cjt** added 30/01/2004
  double GetWeedPercent( TTypesOfVegetation a_letype ) {
    return m_weed_percent[ a_letype ];
  }

  // **cjt** added 27/05/2003
  double GetBugPercentA( TTypesOfVegetation a_letype ) {
    return m_bug_percent_a[ a_letype ];
  }

  // **cjt** added 27/05/2003
  double GetBugPercentB( TTypesOfVegetation a_letype ) {
    return m_bug_percent_b[ a_letype ];
  }

  // **cjt** added 27/05/2003
  double GetBugPercentC( TTypesOfVegetation a_letype ) {
    return m_bug_percent_c[ a_letype ];
  }

  // **cjt** added 12/03/2003
  double GetBugPercentD( TTypesOfVegetation a_letype ) {
    return (double) m_bug_percent_d[ a_letype ];
  }

  bool GetNutStatus( int a_plant_num ) {
    return m_growth[ a_plant_num ]->m_lownut;
  }

  bool GetNutStatusExt( int a_plant ) {
    return m_growth[ m_numbers[ a_plant ]]->m_lownut;
  }

  PlantGrowthData( const char * a_cropcurvefile );
  ~PlantGrowthData();
};

	/** \brief A standard class to contain a pollen or nectar curve based on indexed rates */
class PollenNectarDevelopmentCurve
{
public:
	PollenNectarDevelopmentCurve(vector<int>* a_index, vector<double>* a_slopes) {
		m_index = *a_index;
		m_data = *a_slopes;
	}
	~PollenNectarDevelopmentCurve() { ; }

	/** \brief The basic return function for the curve - day indexed */
	double GetData(int a_index) {
		return m_data[a_index];
	}
	inline int closest(int a_value) {	
		// returns the vector index for m_index where the value contained is the first number greater than a_value
		return int(std::distance(m_index.begin(), upper_bound(m_index.begin(), m_index.end(), a_value)));
	}
protected:
	/** \brief a reference to identify this precise curve - needs external management */
	int m_CurveRefNum;
	/** \brief The slopes used */
	vector <double> m_data;
	/** \brief the index values to the slopes */
	vector <int> m_index;
};

class PollenNectarDevelopmentCurveSet {
public:
	PollenNectarDevelopmentCurve* m_pollencurveptr;
	PollenNectarDevelopmentCurve* m_nectarcurveptr;
};

class PollenNectarQuality {
public:
	PollenNectarQuality();
	PollenNectarQuality(double a_quantity, double a_quality);
	double m_quantity;
	double m_quality;
};

/** \brief A standard class to manage a range of pollen and nectar development curves based on indexed rates */
class PollenNectarDevelopmentData
{
public:
	PollenNectarDevelopmentData(string a_tovinputfile, string a_toleinputfile, Landscape* a_land);
	~PollenNectarDevelopmentData();
	double toleGetPollen(int a_tole, int a_index) { return m_tolePollenCurves[int(a_tole)]->GetData(a_index); }
	double toleGetNectar(int a_tole, int a_index) { return m_toleNectarCurves[int(a_tole)]->GetData(a_index); }
	double tovGetPollen(int a_tov, int a_index) { return m_tovPollenCurves[int(a_tov)]->GetData(a_index); }
	double tovGetNectar(int a_tov, int a_index) { return m_tovNectarCurves[int(a_tov)]->GetData(a_index); }
	int tovGetNPCurveNum(TTypesOfVegetation a_tov) { return m_tov_pollencurvetable[a_tov]; }
	int toleGetNPCurveNum(TTypesOfVegetation a_tov) { return m_tole_pollencurvetable[a_tov]; }
	PollenNectarDevelopmentCurveSet GetPollenNectarCurvePtr(int a_almassLEref);
	PollenNectarDevelopmentCurveSet tovGetPollenNectarCurvePtr(int a_tov_ref);
protected:
	vector<PollenNectarDevelopmentCurve*> m_tovPollenCurves;
	vector<PollenNectarDevelopmentCurve*> m_tovNectarCurves;
	vector<PollenNectarDevelopmentCurve*> m_tolePollenCurves;
	vector<PollenNectarDevelopmentCurve*> m_toleNectarCurves;
	vector<int> m_tov_pollencurvetable;
	vector<int> m_tole_pollencurvetable;
};

#endif // PLANTS_H

