/*
*******************************************************************************************************
Copyright (c) 2013, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Hunters_all.h
Version of  10 October 2013. \n
By Chris J. Topping
*/
/**
* \brief <B>The header file for all hunter types and population manager classes</B>
*/

//---------------------------------------------------------------------------
#ifndef HuntersH
#define HuntersH
//---------------------------------------------------------------------------
class Hunter;
class Hunter_Population_Manager;
class Field;

//------------------------------------------------------------------------------

/**
A list of hunter types
*/
typedef enum
{
      toh_GooseHunter=0,
      toh_foobar
} TTypeOfHunters;

/**
\brief
Hunters like other ALMaSS animals work using a state/transition concept. These are the hunter behavioural states.
*/
enum  TypeOfHunterState
{
      tohts_InitialState=0,
      tohts_Hunting,
	  tohts_OutHunting,
      tohts_Resting,
	  tohts_foobar
};

/**
\brief
Types of message a hunter leader can pass on to team members
*/
enum TypeOfHunterLeaderMessage
{
	hlm_shoot = 0,
	hlm_gohome,
	hlm_foobar
} ;

/**
\brief
Used for creation of a new hunter object
*/
class struct_Hunter
{
 public:
	 int m_ref;
	 int m_HType;
	 APoint m_home;
	 vector <int> m_huntlocrefs;
	 vector <APoint> m_huntlocs;
	 unsigned m_no_hunt_locs;
	 APoint m_validxy;
	 vector<Farm*> m_farms;
	 int m_huntingdayslimit;
	 int m_weekend;
	 double m_efficiency;
	 double m_goosecountchance;
	 Landscape* m_L;
	 Population_Manager * m_preyPM;
};


/**
* A simple data structure for the hunter allocation algorithm vectors to work with
*/
struct FarmOccupcancyData
{
	int m_FarmRef;
	int m_no_hunters;
};

/**
* A type definition for a list of hunters as a vector
*/
typedef vector < Hunter* > HunterList;

/**
\brief
The base class for hunters encompsassing all their general behaviours
*/
/**
* The hunter class specifies general hunter behaviours that are common to all hunter types. 
**/
class Hunter : public TAnimal
{
protected:
    // Attributes
	/** \brief A reference number unique to this hunter */
	int m_myname;
	/** \brief  The current hunter behavioural state */
    TypeOfHunterState m_CurrentHState;
	/** \brief  Pointer to the population manager */
    Hunter_Population_Manager * m_OurPopulationManager;
	/** \brief  Records the time spent hunting per day */
    int m_clock;
	/** \brief  The numbers of game items shot todate this year */
	int m_bag;
	/** \brief  The numbers of shots to-date this year */
	int m_myShots;
	/** \brief  The number of shells in the magazine*/
	int m_myMagazine;
	/** \brief  The numbers of days used for hunting this year */
	int m_huntingdays;
	/** \brief  The date for the last hunt day */
	int m_lasthuntday;
	/** \brief  Annual self-imposed limit on number shot - unused at present*/
	int m_baglimit;
	/** \brief  Annual self-imposed limit on number of days spent hunting */
	int m_huntlimit;
	/** \brief  Probability of 'hitting' a game item */
	double m_efficiency;
	/** \brief  Probability of checking for geese in hunting area */
	double m_goosecountchance;
	/** \brief  Code for weekly hunting activity */
	int m_weekend;
	/** \brief List of pointers to the farmers whose land the hunter hunts  */
	vector<Farm*> m_OurFarmers;
	/** /brief The home location for the hunter */
	APoint m_Home;
	/** \brief The polygon reference number to our current hunting field (which is at a hunting location) */
	int m_huntfield_polyref;
	/** /brief The number of farms he can hunt on */
	int m_NoHuntLocs;
	/** \ The hunting location ref numbers */
	vector <int> m_HuntLocRefs;
	/** \ The hunting locations centroid of the farm held in m_OurFarmer */
	vector <APoint> m_HuntLocs;
	
	// Methods
	/** \brief Decide whether to go out hunting on a specific day */
	virtual TypeOfHunterState st_ShouldGoHunting( void );
	/** \brief The basic hunting behaviour */
	TypeOfHunterState st_OutHunting( void ); // Must be overridden in descendent classes;
	/** \brief Finished hunting and waiting for the next opportunity */
	TypeOfHunterState st_Resting( void );
	/** \brief Initiation of a basic hunter here */
    virtual void Init(struct_Hunter* p_data);
	/** \brief Checks for the hunting day preference (weekday or any day) */
	bool IsTodayAPreferredHuntDay( int a_today );
	/** \brief Uses a probability test to determine whether to go hunting today */
	bool IsTodayAChosenHuntDay( int a_today );
	/** \brief If the hunter checks for game at their hunting locations then this is done here. Must be overridden */
	virtual int CheckForGame( void ) {
		return false;
	}
	
public:
	/** \brief The constructor for the Hunter class */
    Hunter(struct_Hunter* p_data, Hunter_Population_Manager* p_PPM);
	/** \brief The destructor for the Hunter class */
    virtual ~Hunter();
    /** \brief  Gets the annual hunting attempts count */
	int GetHuntingDays() {
		return m_huntingdays;
	}
	/** \brief  Gets the annual hunting bag */
	int GetBag() {
		return m_bag;
	}
	/** \brief  Gets the number of shots this season to-date */
	int GetShots() {
		return m_myShots;
	}
	/** \brief  Sets the annual hunting bag to zero */
	virtual void ResetBag() {
		m_bag = 0;
		m_myShots = 0;
	}
	/** \brief  Sets the annual hunting attempts count to zero */
	void ResetHuntingOpportunities() {
		m_huntingdays = 0;
	}
	/** \brief  Supplies the clock time */
	int GetClock() {
		return m_clock;
	}
	/** \brief  Sets the clock back to zero */
	void ResetClock() {
		m_clock = 0;
	}
	/** \brief  Sets the bag and hunting counters to zero */
	void ResetSeasonData() {
		ResetBag();
		ResetHuntingOpportunities();
	}
	/** \brief A debug function, but may be useful in other contexts. Returns true of currently out hunting */
	bool IsOutHunting() {
		if (m_CurrentHState == tohts_OutHunting) return true;
		return false;
	}
    /** \brief  Optimism in the morning, perhaps we should hunt? */
    void OnMorning() { m_CurrentHState = tohts_Hunting; }
	/** \brief  Provide our ref name */
	int GetRef() { return m_myname; }
	/** \brief  Provide our home coords */
	APoint GetHome() { return m_Home; }
	/** \brief Get the polygon reference number to our current hunting field (which is at a hunting location) */
	int GetHuntField() {
		return m_huntfield_polyref;
	}
	/** \brief  Provide our hunting location coords */
	APoint GetHuntLoc(unsigned a_ref) { return m_HuntLocs[a_ref]; }
	/** \brief  Provide our ref name */
	Farm* GetFarmerRef(unsigned a_ref) { return m_OurFarmers[a_ref]; }
	/** \brief Is it hunting season? - MUST be overridden in descendent class */
	virtual bool InSeason( int /* day */ ) {
		return false;
	}
	/** \brief Is it the end of the hunting season? - MUST be overridden in descendent class */
	virtual bool IsSeasonEnd( int /* day */ ) {
		return true;
	}
	/** \brief Returns the length of the hunting season in days - MUST be overridden in descendent class */
	virtual int GetSeasonLengthLeft( int /* day */ ) {
		return 0;
	}
	/** \brief Each hunter needs to save different kinds of data, so we use a polymorphic method for this */
	virtual void SaveMyData( ofstream* /* a_ofile */ ) {
		; // Base class does nothing
	}
	/** \brief On shoot message handler - must be overidden in descendent classes */
	virtual void OnShoot() { ; }
	/** \brief On gohome message handler - must be overidden in descendent classes */
	virtual void OnGoHome(){ ; }

};

/**
\brief
The class for goose hunters encompsassing all their specific behaviours
*/
class GooseHunter : public Hunter
{
protected:
// Attributes
	/** \brief Pointer to our game population, the geese */
	Goose_Population_Manager* m_preyPopulationManger;
	/** \brief Our list of possible hunting fields as polygon reference numbers as supplied by Landscape::SupplyPolyRef(int,int); */
	polylist* m_huntfields; 
	/** \brief Flag to show whether a hunting location has been found */
	bool m_dugin;
	/** \brief A special bag data structure so the hunter knows what kind of geese he shot */
	int m_goosebag[ gst_foobar ];
	/** \brief Bag limit for pinkfoot */
	int m_pinkfootbaglimit;
	/** \brief Bag limit for greylag */
	int m_greylagbaglimit;
	/** \brief When hunting this indicates whether the hunter is a team leader */
	bool m_leader;
// Methods
	/** \brief Initiation of a specific goose hunter here */
    virtual void Init();
	/** \brief Is it goose hunting season? - MUST be overridden in descendent class */
	virtual bool InSeason( int day );
	/** \brief Is it the end of the goose hunting season? - MUST be overridden in descendent class */
	virtual bool IsSeasonEnd( int day );
	/** \brief Returns the length of the hunting season in days - MUST be overridden in descendent class */
	virtual int GetSeasonLengthLeft( int day );
	/** \brief The basic hunting behaviour */
	TypeOfHunterState st_OutHunting( void );
	/** \brief Behavior involved in deciding whether to go hunting */
	virtual TypeOfHunterState st_ShouldGoHunting(void);
	/** \brief Locate the hunt for today */
	bool FindHuntingLocation( void );
	/** \brief If the hunter checks for game at their hunting locations then this is done here. Must be overridden */
	virtual int CheckForGame( void );
	/** \brief On shoot message handler */
	virtual void OnShoot();
	/** \brief On gohome message handler */
	virtual void OnGoHome();
public:
// Methods
    /** \brief  GooseHunter constructor */
	GooseHunter(struct_Hunter* p_data, Hunter_Population_Manager* p_PPM);
    /** \brief  GooseHunter destructor */
    virtual ~GooseHunter();
    /** \brief  GooseHunter Step code */
	virtual void Step();
    /** \brief  Message received when a goose is successully shot */
	void OnShotABird( int a_birdtype, int a_poly );
	/** \brief Each hunter needs to save different kinds of data, so we use a polymorphic method for this */
	virtual void SaveMyData( ofstream* a_ofile );
	/** \brief  Sets the annual hunting bag to zero */
	virtual void ResetBag() {
		Hunter::ResetBag();
		for (int i = 0; i < gst_foobar; i++) m_goosebag[ i ] = 0;
	}
	/** \brief Returns the leader flag */
	bool IsLeader() {
		return m_leader;
	}
};

/**
\brief
The class to handle all predator population related matters
*/
class Hunter_Population_Manager : public Population_Manager
{
public:
	// Methods
	/** \brief Hunter population manager constructor */
	Hunter_Population_Manager(Landscape* p_L);
	/** \brief Hunter population manager destructor */
	virtual ~Hunter_Population_Manager (void);
	/** \brief Create the initial hunter population and initializes any output options.*/
	void Init(void);
	/** \brief Distributes hunters to hunting locations (farms).*/
	void DistributeHunters(void);
	/** \brief Implements the rule sets to distributes hunters to hunting locations (farms).*/
	void DistributeHuntersByRules( vector<HunterInfo> *a_hunterlist, int a_no_hunters, int a_ruleset );
	/** \brief Used to implement rule sets based on rule set 10.*/
	void RuleSet10Based( int a_no_hunters, vector<int>* a_farmsizes, vector<HunterInfo>* a_hunterlist, vector<APoint>* a_roostlocs, int a_ruleset );
	/** \brief Saves the results of the hunter distribution to an output file */
	void SaveDistributedHunters( vector<HunterInfo>* a_hunterlist, int a_no_hunters );
	/** \brief Saves the results of the hunter distribution to an output file by farm*/
	void SaveFarmHunters( vector<HunterInfo>* a_hunterlist, int a_no_hunters );
	/** \brief Creates hunter objects and assigns them to the population manager lists */
	void CreateObjects(int ob_type, TAnimal*, struct_Hunter* data,int number);
	/** \brief Hunting bag output */
	void RecordHuntingSuccess(int poly,int birds, int a_hunter);
	/** \brief Returns the hunter home location */
	APoint GetHunterHome(int a_index, int a_list);
	/** \brief Returns the hunter hunting location location */
	APoint GetHunterHuntLoc(int a_index, int a_list, unsigned a_ref);
	/** \brief Calculates the number of hunting locations based on a distribution */
	unsigned GetNoHuntLocs();
	/** \brief helper method to reduce code size in hunter rules - checks density rules */
	bool CheckDensity(int a_ref, vector<int> *a_illegalfarms, vector<FarmOccupcancyData>* a_FarmOccupancy);
	/** \brief Adds a hunter hunting, returns true if that hunter is the leader otherwise false*/
	bool AddHunterHunting( int a_polyref, Hunter* a_hunter );
	/** \brief A message system to rely messages from the leader hunter to others in his team */
	void HunterLeaderMessage(TypeOfHunterLeaderMessage a_signal, int a_polyref);
	/** \brief Lists of hunters at all active hunting locations (updated daily)*/
	vector<HunterList*> m_ActiveHuntingLocationsHunters;
	/** \brief  Lists of polygon reference numbers for all active hunting locations (updated daily) */
	vector<int> m_ActiveHuntingLocationsPolyrefs;
	/** \brief  Debugging check method */
	bool IsPolyrefOnActiveList( int a_polyref );
	/** \brief This returns the number of geese which are legal quarry on the polygon the day before. */
	void SetHuntingSeason();
	/** \brief Get the start of the overall hunting season*/
	int GetHuntingSeasonStart(){ return m_HuntingSeasonStart; }
	/** \brief Get the end of the overall hunting season*/
	int GetHuntingSeasonEnd(){ return m_HuntingSeasonEnd; }

protected:
	// Attributes
	/** \brief Used to follow the time of day in 10 minute steps */
	unsigned m_daytime;
	/** \brief Output file for hunting bag record*/
	ofstream* m_HuntingBagRecord;
	/** \brief Start of the overall hunting season*/
	int m_HuntingSeasonStart;
	/** \brief End of the overall hunting season*/
	int m_HuntingSeasonEnd;


	// Methods
	virtual bool StepFinished() { return true; }
	/** \brief Does general daily tasks e.g. reset time of day, reset bag lists if start of year etc..*/
	virtual void DoFirst();
	/** \brief  Available for hunter management - not used currently */
	virtual void DoBefore(){}
	/** \brief  Available for hunter management - not used currently */
	virtual void DoAfter(){}
	/** \brief  Available for hunter management - not used currently */
	virtual void DoLast(){}
};

//---------------------------------------------------------------------------
#endif
