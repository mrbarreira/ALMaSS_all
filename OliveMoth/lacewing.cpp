
#include <string.h>
#include <iostream>
#include <fstream>
#include<vector>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"

#include "lacewing.h"

/** \brief Controls the numbers of adults entering the simulation */
static CfgInt cfg_lw_startno("LACEWING_STARTNO",CFG_CUSTOM,10000);
static CfgInt cfg_lw_begintolay("LACEWING_ADULT_BEGINTOLAY",CFG_CUSTOM,2);
static CfgInt cfg_lw_ntolay("LACEWING_ADULT_NTOLAY",CFG_CUSTOM,10);

LaceWing_Population_Manager::LaceWing_Population_Manager(Landscape* L)
  : Population_Manager(L) {
      // Load List of Animal Classes
  m_ListNames[0] = "EGG";
  m_ListNames[1] = "LARVA";
  m_ListNames[2] = "PUPA";
  m_ListNames[3] = "ADULT";
  m_ListNameLength = 4;

  TListOfAnimals alist;
  //TheArray.insert( TheArray.end(), 3, alist );
  // We need one vector for each life stgage
  //  for (unsigned int i=0; i<(10-m_ListNameLength); i++)
  //  for (unsigned int i=0; i<(m_ListNameLength); i++)
  //  {
  //    TheArray.pop_back();
  //  }
  strcpy( m_SimulationName, "LACEWING Simulation" );

  // Create some animals
  LaceWing om = LaceWing(0,0,m_TheLandscape,this);
  for (int i=0; i< cfg_lw_startno.value(); i++) 
    {
      om.SetX(random(SimW));
      om.SetY(random(SimH));
      Add<LaceWingLarva>(om);
      int ob_type=LaceWingLarva::myID;
      float prog=random(20)/100.0;
      cout << "prog: " << prog << "\n";
      dynamic_cast<LaceWing*>(TheArray[ob_type][i])->setProgress(prog);
    }

  m_population_type = TOP_LaceWing;
}

LaceWing_Population_Manager::~  LaceWing_Population_Manager(void) {
}

LaceWing::LaceWing(int a_x, int a_y, Landscape* p_L, LaceWing_Population_Manager* p_PM)
  : TAnimal( a_x ,  a_y , p_L) {
  m_OurPopulationManager=(LaceWing_Population_Manager*)p_PM;
  m_CurrentOMState=toLaceWings_InitialState;
  m_Age=0;
  dev_progress=0.0;
  // Just default values. These should always be overwritten.
  devparam_a=170000.0;
  devparam_b=3.0;
}

void LaceWing::ReInit(int a_x, int a_y, Landscape* p_L, LaceWing_Population_Manager* p_PM) {
  TAnimal::ReinitialiseObject(a_x, a_y, p_L);
  m_OurPopulationManager=p_PM;
  m_CurrentOMState=toLaceWings_InitialState;
  m_Age=0;
  dev_progress=0.0;
  devparam_a=170000.0;
  devparam_b=3.0;
}
    
LaceWing::~LaceWing() {
}


void LaceWing::Step(void)
{
  m_Age+=1;
  if (m_StepDone || m_CurrentStateNo == -1) return;
  switch (m_CurrentOMState)
  {
   case toLaceWings_InitialState: 
    m_CurrentOMState=toLaceWings_Develop;
    break;
  case toLaceWings_Move:
    m_CurrentOMState=st_Movement(); // Will return develop
    break;
   case toLaceWings_Develop:
    m_CurrentOMState=st_Develop(); // Will return movement or die
    m_StepDone=true;
    break;
  case toLaceWings_Progress:
    m_CurrentOMState = st_Progress();
    m_StepDone = true;
    break;
  case toLaceWings_Die:
     st_Dying(); // No return value - no behaviour after this
     m_StepDone=true;
     break;
  default:
    m_OurLandscape->Warn("LaceWing::Step()","unknown state - default");
    exit(1);
  }
}

TTypeOfLaceWingState LaceWing::st_Develop(void)
{
  /** 
   * First takes the intrinsic mortality test.
   * Then if not dead, age by a day and see if we need to hatch
   */


  // NEED TO COMPLETELY REWRITE THIS. THIS IS FOR OLIVE MOTHS
  
  float dailyMortality = 0.01;
  if (g_rand_uni() < dailyMortality) return toLaceWings_Die;
  //m_Age++;

  auto T = m_OurLandscape->SupplyTemp();
  auto RH = m_OurLandscape->SupplyHumidity();
  
  // dww This is becuase the weather model is too cold!!!
  // Delete me for real model!!!!!!!
  T+=10;
  auto incr = 1.0/(devparam_a*pow(T,devparam_b));
  if (!std::isnan(incr))
    dev_progress+=incr;
  //cout << "Temp: " << T << " Humidity: " << RH << " Prog: " << dev_progress << "\n";
  if (dev_progress > 1.0)
    return toLaceWings_Progress;
  else
    return toLaceWings_Develop;
}

TTypeOfLaceWingState LaceWing::st_Progress(void)
{
  /**
     This creates a data object to pass on to the population manager to create a worker larva, then signals that this object should be destroyed.
  */
  // Create the larva
  nextStage();
  // Signal removal of the object
  m_CurrentStateNo = -1;
  m_StepDone = true;
  return toLaceWings_Die; // Not necessary, but for neatness
}

void LaceWing::st_Dying(void)
{
  m_CurrentStateNo = -1; // this will kill the animal object and free up space
}

///////////////// OLIVE MOTH EGG ////////////////////////

LaceWingEgg::LaceWingEgg(int  a_x, int  a_y, Landscape* p_L, LaceWing_Population_Manager* p_BPM)
  : LaceWing(a_x, a_y, p_L, p_BPM)
{
  dev_progress=0.0;
  devparam_a=1.0;
  devparam_b=1.0;
}

LaceWingEgg::~LaceWingEgg(void)
{
  ;
}


///////////////// OLIVE MOTH LARVA ////////////////////////


LaceWingLarva::LaceWingLarva(int  a_x, int  a_y, Landscape* p_L,
			       LaceWing_Population_Manager* p_BPM)
  : LaceWing(a_x, a_y, p_L, p_BPM)
{
  ;
}

LaceWingLarva::~LaceWingLarva(void)
{
  ;
}



///////////////// OLIVE MOTH PUPA ////////////////////////


LaceWingPupa::LaceWingPupa(int  a_x, int  a_y, Landscape* p_L, LaceWing_Population_Manager* p_BPM) : LaceWing(a_x, a_y, p_L, p_BPM)
{
  ;
}

LaceWingPupa::~LaceWingPupa(void)
{
  ;
}


///////////////// OLIVE MOTH ADULT ////////////////////////


LaceWingAdult::LaceWingAdult(int  a_x, int  a_y, Landscape* p_L, LaceWing_Population_Manager* p_BPM) : LaceWing(a_x, a_y, p_L, p_BPM)
{
  ;
}

LaceWingAdult::~LaceWingAdult(void)
{
  ;
}

void LaceWingAdult::Step(void)
{
  m_Age+=1;
  if (m_StepDone || m_CurrentStateNo == -1) return;
  switch (m_CurrentOMState)
  {
   case toLaceWings_InitialState: // Initial state always starts with develop
    m_CurrentOMState=toLaceWings_Move;
    break;
  case toLaceWings_Move:
    m_CurrentOMState=st_Movement(); // Will return develop
    break;
   case toLaceWings_Develop:
    m_CurrentOMState=st_Develop(); // Will return movement or die
    m_StepDone=true;
    break;
  case toLaceWings_Lay:
    m_CurrentOMState=st_Lay(); // Will return develop
    break;
  case toLaceWings_Progress:
    m_CurrentOMState = st_Progress();
    m_StepDone = true;
    break;
  case toLaceWings_Die:
    cout << "Dying: " << myID << "<n";
     st_Dying(); // No return value - no behaviour after this
     m_StepDone=true;
     break;
  default:
     m_OurLandscape->Warn("LaceWingAdult::Step()","unknown state - default");
     exit(1);
  }
}

TTypeOfLaceWingState LaceWingAdult::st_Movement( void )
{
  int SimW=m_OurPopulationManager->SimW;
  int SimH=m_OurPopulationManager->SimH;
  
  m_Location_x+=(random(11)-5);
  m_Location_y+=(random(11)-5);

  if (m_Location_x < 0)
    m_Location_x=0;
  if (m_Location_x >= SimW)
    m_Location_x=SimW-1;
      
  if (m_Location_y < 0)
    m_Location_y=0;
  if (m_Location_y >= SimH)
    m_Location_y=SimH-1;
      
  return toLaceWings_Lay;
}

TTypeOfLaceWingState LaceWingAdult::st_Lay(void)
{
  if (m_Age > cfg_lw_begintolay.value()) {
    for (int i=0;i < cfg_lw_ntolay.value(); i++) {
      lay();
    }
  } 
  return toLaceWings_Develop;
}

TTypeOfLaceWingState LaceWingAdult::st_Develop(void)
{
  /** 
   * First takes the intrinsic mortality test.
   * Then if not dead, age by a day and see if we need to hatch
   */
  //	if (g_rand_uni() < m_AdultDailyMortality) return toLaceWingS_Die;
  float T=20.0;
  dev_progress+=1.0/(devparam_a*pow(T,devparam_b));
  if (dev_progress > 1.0)
    return toLaceWings_Die;
  else
    return toLaceWings_Progress;
}
