
#ifndef OLIVEMOTH_H
#define OLIVEMOTH_H

const unsigned STEP_SIZE_MINS=60;

class OliveMoth_Population_Manager;
class OliveMoth;

typedef enum
{
      toOliveMoths_InitialState=0,
      toOliveMoths_Develop,
      toOliveMoths_Progress,
      toOliveMoths_Move,
      toOliveMoths_Lay,
      toOliveMoths_Die
} TTypeOfOliveMothState;


class OliveMoth_Population_Manager : public Population_Manager {
 public:
  OliveMoth_Population_Manager(Landscape* L);
  virtual ~OliveMoth_Population_Manager(void);

  //  void CreateObjects(int ob_type, TAnimal *pvo, struct_OliveMoth* data, int number);
  /** \brief Adds a new animal of type T. Must pass an existing
      animal to initialise from */


  TTypesOfLandscapeElement habitat(int x, int y) {
    return m_TheLandscape->SupplyElementType(x,y);
  }
  
  TTypesOfLandscapeElement habitat(std::pair<int,int> xy) {
    return m_TheLandscape->SupplyElementType(xy.first, xy.second);
  }
  
  bool isHabitat(int x, int y, TTypesOfLandscapeElement toh) {
    return (habitat(x,y)==toh);
  }

  bool isHabitat(std::pair<int,int> xy, TTypesOfLandscapeElement toh) {
    return (habitat(xy)==toh);
  }

  std::pair<int, int> randCoordsInHab(TTypesOfLandscapeElement toh) {
    //    int SimW=m_OurPopulationManager->SimW;
    //int SimH=m_OurPopulationManager->SimH;

    std::pair<int,int> xy = std::make_pair(random(SimW), random(SimH));
    while (!isHabitat(xy,toh)) {
      xy = std::make_pair(random(SimW), random(SimH));
    }
    return xy;
  }
  
#define PM_REUSE
  
#ifdef PM_CHUNKING
  template <class T>
    void Add(TAnimal &animal) {
  }
#endif
  
#ifdef PM_SIMPLE
  template <class T>
    void Add(TAnimal &animal) {
    int ob_type=T::myID;
    auto obsize=TheArray[ob_type].size();
    auto obcap=TheArray[ob_type].capacity();
    //    cout << "Adding: " << ob_type << " Size: " << obsize << " Capacity: " << obcap  << "\n";
    //TAnimal* newAnimal = new T(animal.Supply_m_Location_x(), animal.Supply_m_Location_y(), m_TheLandscape, this);
    auto newAnimal = new T(animal.Supply_m_Location_x(), animal.Supply_m_Location_y(), m_TheLandscape, this);
    TheArray[ob_type].push_back(newAnimal);
    IncLiveArraySize(ob_type);
  }
#endif
  
#ifdef PM_REUSE
  template <class T>
    void Add(TAnimal &animal) {
    int ob_type=T::myID;
    auto obsize=TheArray[ob_type].size();
    auto obcap=TheArray[ob_type].capacity();
    if (unsigned(TheArray[ob_type].size())>GetLiveArraySize(ob_type)) {
      //cout << "Reusing: " << ob_type << " Size: " << obsize << " Capacity: " << obcap  << "\n";
      dynamic_cast<T*>(TheArray[ob_type][GetLiveArraySize(ob_type)])->ReInit(animal.Supply_m_Location_x(), animal.Supply_m_Location_y(), m_TheLandscape, this);
      IncLiveArraySize(ob_type);
    }
    else {
      //      cout << "Adding: " << ob_type << " Size: " << obsize << " Capacity: " << obcap  << "\n";
      TAnimal* newAnimal = new T(animal.Supply_m_Location_x(), animal.Supply_m_Location_y(), m_TheLandscape, this);
      TheArray[ob_type].push_back(newAnimal);
      IncLiveArraySize(ob_type);
    }
  }
#endif
  
 protected:
  /** \brief  Things to do before anything else at the start of a timestep  */
  virtual void DoFirst() {}
  /** \brief Things to do before the Step */
  virtual void DoBefore(){}
  /** \brief Things to do before the EndStep */
  virtual void DoAfter(){}
  /** \brief Things to do after the EndStep */
  virtual void DoLast(){}
};

class OliveMoth : public TAnimal {
 public:
  OliveMoth(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM);
  virtual ~OliveMoth();
  void ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM);
  //  inline void FillData(struct_OliveMoth* a_data);
  //  inline void FillData(struct_OliveMoth* a_data);
    
  /** \brief Behavioural state development */
  virtual TTypeOfOliveMothState st_Develop( void );// { return toOliveMoths_Develop;}
  /** \brief Behavioural state movement */
  virtual TTypeOfOliveMothState st_Movement( void ) { return toOliveMoths_Develop;}
  virtual TTypeOfOliveMothState st_Progress(void);
  virtual void nextStage() {}
  /** \brief Behavioural state dying */
  void st_Dying( void );
  /** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
  virtual void BeginStep(void) {} // NB this is not used in the OliveMoth code
  /** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
  virtual void Step(void);
  /** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
  virtual void EndStep(void) {} // NB this is not used in the OliveMoth code
  /** \brief A typical interface function - this one returns the SpeciesID number as an unsigned integer */
  void setProgress(float p) {
    dev_progress=p;
  }
  
  //float FINISH=a*pow(T,b)*24;
  //   void setDevFinish() {  dev_finish=devparam_a*pow(20.0,devparam_b);}
  static const int myID = -1;
  
 protected:
  /** \brief Variable to record current behavioural state */
   TTypeOfOliveMothState m_CurrentOMState;
   OliveMoth_Population_Manager* m_OurPopulationManager;
   /** \brief Holds the age of the bee in days */
   unsigned m_Age;
   float dev_progress, devparam_a,devparam_b;
};

class OliveMoth;
class OliveMothEgg;
class OliveMothLarva;
class OliveMothPupa;
class OliveMothAdult;
class OliveMothPhiEgg;
class OliveMothPhiLarva;
class OliveMothPhiPupa;
class OliveMothPhiAdult;
class OliveMothAntEgg;
class OliveMothAntLarva;
class OliveMothAntPupa;
class OliveMothAntAdult;
class OliveMothCarEgg;
class OliveMothCarLarva;
class OliveMothCarPupa;
class OliveMothCarAdult;

class OliveMothEgg : public OliveMoth {
 public:
  // Methods
  /** \brief OliveMoth_Egg constructor */
  OliveMothEgg(int  a_x , int  a_y , Landscape* p_L, OliveMoth_Population_Manager* p_BPM);
  /** \brief OliveMoth_Egg destructor */
  virtual ~OliveMothEgg();
  virtual void nextStage() {
    m_OurPopulationManager->Add<OliveMothLarva>(*this);
  }
  //  TTypeOfOliveMothState st_Hatch(void);
  //TTypeOfOliveMothState st_Develop(void);

  static const int myID = -1;
  //virtual void Step(void);
};

class OliveMothLarva : public OliveMoth {
 public:
  // Methods
  /** \brief OliveMoth_Larva constructor */
  OliveMothLarva(int  a_x , int  a_y , Landscape* p_L, OliveMoth_Population_Manager* p_BPM);
  /** \brief OliveMoth_Larva destructor */
  virtual ~OliveMothLarva();
  virtual void nextStage() {
    m_OurPopulationManager->Add<OliveMothPupa>(*this);
  }
  //TTypeOfOliveMothState st_Pupate(void);
  //TTypeOfOliveMothState st_Develop(void);

  static const int myID = -1;
  //virtual void Step(void);
};

class OliveMothPupa : public OliveMoth {
 public:
  // Methods
  /** \brief OliveMoth_Pupa constructor */
  OliveMothPupa(int  a_x , int  a_y , Landscape* p_L, OliveMoth_Population_Manager* p_BPM);
  /** \brief ReInit for object pool */
  //  void ReInit(int  a_x, int  a_y, Landscape* p_L, OliveMoth_Population_Manager* p_BPM);
  /** \brief OliveMoth_Pupa destructor */
  virtual ~OliveMothPupa();

  virtual void nextStage() {
    m_OurPopulationManager->Add<OliveMothAdult>(*this);
  }

  //TTypeOfOliveMothState st_Emerge(void);
  //TTypeOfOliveMothState st_Develop(void);

  static const int myID = -1;
  //virtual void Step(void);
};

class OliveMothAdult : public OliveMoth {
 public:
  // Methods
  /** \brief OliveMoth_Adult constructor */
  OliveMothAdult(int  a_x , int  a_y , Landscape* p_L, OliveMoth_Population_Manager* p_BPM);
  /** \brief ReInit for object pool */
  //void ReInit(int  a_x, int  a_y, Landscape* p_L, OliveMoth_Population_Manager* p_BPM);
  /** \brief OliveMoth_Adult destructor */
  virtual ~OliveMothAdult();

  virtual TTypeOfOliveMothState st_Movement( void );
  //  TTypeOfOliveMothState st_Dying(void);
  virtual void nextStage() {
    m_CurrentStateNo=-1;
  }


  virtual TTypeOfOliveMothState lay() {
    cout << "Not making generic Egg. I shouldn't happen\n";
    return toOliveMoths_Develop;
  }

  TTypeOfOliveMothState st_Develop(void);
  TTypeOfOliveMothState st_Lay(void);
  static const int myID = -1;
  virtual void Step(void);
};

///////////////////////// PHI ////////////////////////////////////////

class OliveMothPhiEgg : public OliveMothEgg {
 public:
  // Methods
  /** \brief OliveMothPhiEgg constructor */
  OliveMothPhiEgg(int  a_x , int  a_y , Landscape* p_L, OliveMoth_Population_Manager* p_BPM);

  /** \brief OliveMothPhiEgg destructor */
  virtual ~OliveMothPhiEgg() {}

  void ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM);

  virtual void nextStage() {
    m_OurPopulationManager->Add<OliveMothPhiLarva>(*this);
  }
  static const int myID = 0;
};

class OliveMothPhiLarva : public OliveMothLarva {
 public:
  // Methods
  /** \brief OliveMothPhiLarva constructor */
  OliveMothPhiLarva(int  a_x , int  a_y , Landscape* p_L, OliveMoth_Population_Manager* p_BPM);

  /** \brief OliveMothPhiLarva destructor */
  virtual ~OliveMothPhiLarva() {}

  void ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM);

  virtual void nextStage() {
    m_OurPopulationManager->Add<OliveMothPhiPupa>(*this);
  }
  static const int myID = 1;
};


class OliveMothPhiPupa : public OliveMothPupa {
 public:
  // Methods
  /** \brief OliveMothPhiPupa constructor */
  OliveMothPhiPupa(int  a_x , int  a_y , Landscape* p_L, OliveMoth_Population_Manager* p_BPM);

  /** \brief OliveMothPhiPupa destructor */
  virtual ~OliveMothPhiPupa() {}

  void ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM);
  
  virtual void nextStage() {
    m_OurPopulationManager->Add<OliveMothPhiAdult>(*this);
  }
  static const int myID = 2;
};

class OliveMothPhiAdult : public OliveMothAdult {
 public:
  // Methods
  /** \brief OliveMothPhiAdult constructor */
  OliveMothPhiAdult(int  a_x , int  a_y , Landscape* p_L, OliveMoth_Population_Manager* p_BPM);

  /** \brief OliveMothPhiAdult destructor */
  virtual ~OliveMothPhiAdult() {}

  void ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM);
  TTypeOfOliveMothState lay() {
    //    cout << "Making Ant Egg\n";
    m_OurPopulationManager->Add<OliveMothAntEgg>(*this);
    return toOliveMoths_Develop;
  }
  static const int myID = 3;
};

///////////////////////// ANT ////////////////////////////////////////

class OliveMothAntEgg : public OliveMothEgg {
 public:
  // Methods
  /** \brief OliveMothAntEgg constructor */
  OliveMothAntEgg(int  a_x , int  a_y , Landscape* p_L, OliveMoth_Population_Manager* p_BPM);

  /** \brief OliveMothAntEgg destructor */
  virtual ~OliveMothAntEgg() {}

  void ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM);

  virtual void nextStage() {
    m_OurPopulationManager->Add<OliveMothAntLarva>(*this);
  }
  static const int myID = 4;
};

class OliveMothAntLarva : public OliveMothLarva {
 public:
  // Methods
  /** \brief OliveMothAntLarva constructor */
  OliveMothAntLarva(int  a_x , int  a_y , Landscape* p_L, OliveMoth_Population_Manager* p_BPM);

  /** \brief OliveMothAntLarva destructor */
  virtual ~OliveMothAntLarva() {}

  void ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM);

  virtual void nextStage() {
    m_OurPopulationManager->Add<OliveMothAntPupa>(*this);
  }
  static const int myID = 5;
};


class OliveMothAntPupa : public OliveMothPupa {
 public:
  // Methods
  /** \brief OliveMothAntPupa constructor */
  OliveMothAntPupa(int  a_x , int  a_y , Landscape* p_L, OliveMoth_Population_Manager* p_BPM);

  /** \brief OliveMothAntPupa destructor */
  virtual ~OliveMothAntPupa() {}

  void ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM);
  
  virtual void nextStage() {
    m_OurPopulationManager->Add<OliveMothAntAdult>(*this);
  }
  static const int myID = 6;
};

class OliveMothAntAdult : public OliveMothAdult {
 public:
  // Methods
  /** \brief OliveMothAntAdult constructor */
  OliveMothAntAdult(int  a_x , int  a_y , Landscape* p_L, OliveMoth_Population_Manager* p_BPM);

  /** \brief OliveMothAntAdult destructor */
  virtual ~OliveMothAntAdult() {}

  void ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM);
  TTypeOfOliveMothState lay() {
    //    cout << "Making Phi Egg\n";
    m_OurPopulationManager->Add<OliveMothCarEgg>(*this);
    return toOliveMoths_Develop;
  }

  static const int myID = 7;
};

///////////////////////// CAR ////////////////////////////////////////

class OliveMothCarEgg : public OliveMothEgg {
 public:
  // Methods
  /** \brief OliveMothCarEgg constructor */
  OliveMothCarEgg(int  a_x , int  a_y , Landscape* p_L, OliveMoth_Population_Manager* p_BPM);

  /** \brief OliveMothCarEgg destructor */
  virtual ~OliveMothCarEgg() {}

  void ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM);

  virtual void nextStage() {
    m_OurPopulationManager->Add<OliveMothCarLarva>(*this);
  }
  static const int myID = 8;
};

class OliveMothCarLarva : public OliveMothLarva {
 public:
  // Methods
  /** \brief OliveMothCarLarva constructor */
  OliveMothCarLarva(int  a_x , int  a_y , Landscape* p_L, OliveMoth_Population_Manager* p_BPM);

  /** \brief OliveMothCarLarva destructor */
  virtual ~OliveMothCarLarva() {}

  void ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM);

  virtual void nextStage() {
    m_OurPopulationManager->Add<OliveMothCarPupa>(*this);
  }
  static const int myID = 9;
};


class OliveMothCarPupa : public OliveMothPupa {
 public:
  // Methods
  /** \brief OliveMothCarPupa constructor */
  OliveMothCarPupa(int  a_x , int  a_y , Landscape* p_L, OliveMoth_Population_Manager* p_BPM);

  /** \brief OliveMothCarPupa destructor */
  virtual ~OliveMothCarPupa() {}

  void ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM);
  
  virtual void nextStage() {
    m_OurPopulationManager->Add<OliveMothCarAdult>(*this);
  }
  static const int myID = 10;
};

class OliveMothCarAdult : public OliveMothAdult {
 public:
  // Methods
  /** \brief OliveMothCarAdult constructor */
  OliveMothCarAdult(int  a_x , int  a_y , Landscape* p_L, OliveMoth_Population_Manager* p_BPM);

  /** \brief OliveMothCarAdult destructor */
  virtual ~OliveMothCarAdult() {}

  void ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM);
  TTypeOfOliveMothState lay() {
    //    cout << "Making Phi Egg\n";
    m_OurPopulationManager->Add<OliveMothPhiEgg>(*this);
    return toOliveMoths_Develop;
  }

  static const int myID = 11;
};

#endif 
