
#include <string.h>
#include <iostream>
#include <fstream>
#include<vector>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
//#include "../Landscape/landscape.h"
#include "../BatchALMaSS/PopulationManager.h"

#include "olivemoth.h"

/** \brief Controls the numbers of adults entering the simulation */
static CfgInt cfg_startno("OLIVEMOTH_STARTNO",CFG_CUSTOM,10000);

static CfgFloat cfg_phi_eggdev_a("OLIVEMOTH_PHI_EGGDEV_A",CFG_CUSTOM,10000);
static CfgFloat cfg_phi_eggdev_b("OLIVEMOTH_PHI_EGGDEV_B",CFG_CUSTOM,10000);
static CfgFloat cfg_phi_larvadev_a("OLIVEMOTH_PHI_LARVADEV_A",CFG_CUSTOM,10000);
static CfgFloat cfg_phi_larvadev_b("OLIVEMOTH_PHI_LARVADEV_B",CFG_CUSTOM,10000);
static CfgFloat cfg_phi_pupadev_a("OLIVEMOTH_PHI_PUPADEV_A",CFG_CUSTOM,10000);
static CfgFloat cfg_phi_pupadev_b("OLIVEMOTH_PHI_PUPADEV_B",CFG_CUSTOM,10000);
static CfgFloat cfg_phi_adultdev_a("OLIVEMOTH_PHI_ADULTDEV_A",CFG_CUSTOM,10000);
static CfgFloat cfg_phi_adultdev_b("OLIVEMOTH_PHI_ADULTDEV_B",CFG_CUSTOM,10000);

static CfgFloat cfg_ant_eggdev_a("OLIVEMOTH_ANT_EGGDEV_A",CFG_CUSTOM,10000);
static CfgFloat cfg_ant_eggdev_b("OLIVEMOTH_ANT_EGGDEV_B",CFG_CUSTOM,10000);
static CfgFloat cfg_ant_larvadev_a("OLIVEMOTH_ANT_LARVADEV_A",CFG_CUSTOM,10000);
static CfgFloat cfg_ant_larvadev_b("OLIVEMOTH_ANT_LARVADEV_B",CFG_CUSTOM,10000);
static CfgFloat cfg_ant_pupadev_a("OLIVEMOTH_ANT_PUPADEV_A",CFG_CUSTOM,10000);
static CfgFloat cfg_ant_pupadev_b("OLIVEMOTH_ANT_PUPADEV_B",CFG_CUSTOM,10000);
static CfgFloat cfg_ant_adultdev_a("OLIVEMOTH_ANT_ADULTDEV_A",CFG_CUSTOM,10000);
static CfgFloat cfg_ant_adultdev_b("OLIVEMOTH_ANT_ADULTDEV_B",CFG_CUSTOM,10000);

static CfgFloat cfg_car_eggdev_a("OLIVEMOTH_CAR_EGGDEV_A",CFG_CUSTOM,10000);
static CfgFloat cfg_car_eggdev_b("OLIVEMOTH_CAR_EGGDEV_B",CFG_CUSTOM,10000);
static CfgFloat cfg_car_larvadev_a("OLIVEMOTH_CAR_LARVADEV_A",CFG_CUSTOM,10000);
static CfgFloat cfg_car_larvadev_b("OLIVEMOTH_CAR_LARVADEV_B",CFG_CUSTOM,10000);
static CfgFloat cfg_car_pupadev_a("OLIVEMOTH_CAR_PUPADEV_A",CFG_CUSTOM,10000);
static CfgFloat cfg_car_pupadev_b("OLIVEMOTH_CAR_PUPADEV_B",CFG_CUSTOM,10000);
static CfgFloat cfg_car_adultdev_a("OLIVEMOTH_CAR_ADULTDEV_A",CFG_CUSTOM,10000);
static CfgFloat cfg_car_adultdev_b("OLIVEMOTH_CAR_ADULTDEV_B",CFG_CUSTOM,10000);

static CfgInt cfg_begintolay("OLIVEMOTH_ADULT_BEGINTOLAY",CFG_CUSTOM,2);
static CfgInt cfg_ntolay("OLIVEMOTH_ADULT_NTOLAY",CFG_CUSTOM,10);

static CfgFloat cfg_rh_aa_m("OLIVEMOTH_RH_AA_M",CFG_CUSTOM,-0.0385);
static CfgFloat cfg_rh_aa_c("OLIVEMOTH_RH_AA_M",CFG_CUSTOM, 0.0385);
static CfgFloat cfg_rh_bb_m("OLIVEMOTH_RH_BB_M",CFG_CUSTOM,-1.04);
static CfgFloat cfg_rh_bb_c("OLIVEMOTH_RH_BB_M",CFG_CUSTOM, 0.0546);
static CfgFloat cfg_rh_cc_m("OLIVEMOTH_RH_CC_M",CFG_CUSTOM,-0.837);
static CfgFloat cfg_rh_cc_c("OLIVEMOTH_RH_CC_M",CFG_CUSTOM, 0.0473);
static CfgFloat cfg_rh_dd_m("OLIVEMOTH_RH_DD_M",CFG_CUSTOM,-1.25);
static CfgFloat cfg_rh_dd_c("OLIVEMOTH_RH_DD_M",CFG_CUSTOM, 0.055);

OliveMoth_Population_Manager::OliveMoth_Population_Manager(Landscape* L)
  : Population_Manager(L) {
      // Load List of Animal Classes
  m_ListNames[0] = "Phi EGG";
  m_ListNames[1] = "Phi LARVA";
  m_ListNames[2] = "Phi PUPA";
  m_ListNames[3] = "Phi ADULT";
  m_ListNames[4] = "Ant EGG";
  m_ListNames[5] = "Ant LARVA";
  m_ListNames[6] = "Ant PUPA";
  m_ListNames[7] = "Ant ADULT";
  m_ListNames[8] = "Car EGG";
  m_ListNames[9] = "Car LARVA";
  m_ListNames[10] = "Car PUPA";
  m_ListNames[11] = "Car ADULT";
  m_ListNameLength = 12;

  TListOfAnimals alist;
  //TheArray.insert( TheArray.end(), 3, alist );
  // We need one vector for each life stage
  //  for (unsigned int i=0; i<(10-m_ListNameLength); i++)
  //  for (unsigned int i=0; i<(m_ListNameLength); i++)
  //  {
  //    TheArray.pop_back();
  //  }
  strcpy( m_SimulationName, "OLIVEMOTH Simulation" );

  // Create some animals
  OliveMoth om = OliveMoth(0,0,m_TheLandscape,this);
  for (int i=0; i< cfg_startno.value(); i++) 
    {
      std::pair<int,int> xy = randCoordsInHab(tole_Field);
      om.SetX(xy.first);
      om.SetY(xy.second);
      Add<OliveMothPhiLarva>(om);
      int ob_type=OliveMothPhiLarva::myID;
      float prog=random(20)/100.0;
      dynamic_cast<OliveMoth*>(TheArray[ob_type][i])->setProgress(prog);
    }

  m_population_type = TOP_OliveMoth;
}

OliveMoth_Population_Manager::~  OliveMoth_Population_Manager(void) {
}



OliveMoth::OliveMoth(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM)
  : TAnimal( a_x ,  a_y , p_L) {
  m_OurPopulationManager=(OliveMoth_Population_Manager*)p_PM;
  m_CurrentOMState=toOliveMoths_InitialState;
  m_Age=0;
  dev_progress=0.0;
  // Just default values. These should always be overwritten.
  devparam_a=170000.0;
  devparam_b=3.0;
}

void OliveMoth::ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM) {
  TAnimal::ReinitialiseObject(a_x, a_y, p_L);
  m_OurPopulationManager=p_PM;
  m_CurrentOMState=toOliveMoths_InitialState;
  m_Age=0;
  dev_progress=0.0;
  devparam_a=170000.0;
  devparam_b=3.0;
}
    
OliveMoth::~OliveMoth() {
}


void OliveMoth::Step(void)
{
  m_Age+=1;
  if (m_StepDone || m_CurrentStateNo == -1) return;
  switch (m_CurrentOMState)
  {
   case toOliveMoths_InitialState: 
    m_CurrentOMState=toOliveMoths_Develop;
    break;
  case toOliveMoths_Move:
    m_CurrentOMState=st_Movement(); // Will return develop
    break;
   case toOliveMoths_Develop:
    m_CurrentOMState=st_Develop(); // Will return movement or die
    m_StepDone=true;
    break;
  case toOliveMoths_Progress:
    m_CurrentOMState = st_Progress();
    m_StepDone = true;
    break;
  case toOliveMoths_Die:
     st_Dying(); // No return value - no behaviour after this
     m_StepDone=true;
     break;
  default:
    m_OurLandscape->Warn("OliveMoth::Step()","unknown state - default");
    exit(1);
  }
}

TTypeOfOliveMothState OliveMoth::st_Develop(void)
{
  /** 
   * First takes the intrinsic mortality test.
   * Then if not dead, age by a day and see if we need to hatch
   */
  
  float dailyMortality = 0.01;
  if (g_rand_uni() < dailyMortality) return toOliveMoths_Die;
  //m_Age++;

  auto T = m_OurLandscape->SupplyTemp();
  auto RH = m_OurLandscape->SupplyHumidity();


  
  
  // dww This is becuase the weather model is too cold!!!
  // Delete me for real model!!!!!!!
  T+=10;
  auto incr = 1.0/(devparam_a*pow(T,devparam_b));
  if (!std::isnan(incr))
    dev_progress+=incr;
  //cout << "Temp: " << T << " Humidity: " << RH << " Prog: " << dev_progress << "\n";
  if (dev_progress > 1.0)
    return toOliveMoths_Progress;
  else
    return toOliveMoths_Develop;
}

TTypeOfOliveMothState OliveMoth::st_Progress(void)
{
  /**
     This creates a data object to pass on to the population manager to create a worker larva, then signals that this object should be destroyed.
  */
  // Create the larva
  nextStage();
  // Signal removal of the object
  m_CurrentStateNo = -1;
  m_StepDone = true;
  return toOliveMoths_Die; // Not necessary, but for neatness
}

void OliveMoth::st_Dying(void)
{
  m_CurrentStateNo = -1; // this will kill the animal object and free up space
}

///////////////// OLIVE MOTH EGG ////////////////////////

OliveMothEgg::OliveMothEgg(int  a_x, int  a_y, Landscape* p_L, OliveMoth_Population_Manager* p_BPM)
  : OliveMoth(a_x, a_y, p_L, p_BPM)
{
  dev_progress=0.0;
  devparam_a=1.0;
  devparam_b=1.0;
}

OliveMothEgg::~OliveMothEgg(void)
{
  ;
}


///////////////// OLIVE MOTH LARVA ////////////////////////


OliveMothLarva::OliveMothLarva(int  a_x, int  a_y, Landscape* p_L,
			       OliveMoth_Population_Manager* p_BPM)
  : OliveMoth(a_x, a_y, p_L, p_BPM)
{
  ;
}

OliveMothLarva::~OliveMothLarva(void)
{
  ;
}



///////////////// OLIVE MOTH PUPA ////////////////////////


OliveMothPupa::OliveMothPupa(int  a_x, int  a_y, Landscape* p_L, OliveMoth_Population_Manager* p_BPM) : OliveMoth(a_x, a_y, p_L, p_BPM)
{
  ;
}

OliveMothPupa::~OliveMothPupa(void)
{
  ;
}


///////////////// OLIVE MOTH ADULT ////////////////////////


OliveMothAdult::OliveMothAdult(int  a_x, int  a_y, Landscape* p_L, OliveMoth_Population_Manager* p_BPM) : OliveMoth(a_x, a_y, p_L, p_BPM)
{
  ;
}

OliveMothAdult::~OliveMothAdult(void)
{
  ;
}

void OliveMothAdult::Step(void)
{
  m_Age+=1;
  if (m_StepDone || m_CurrentStateNo == -1) return;
  switch (m_CurrentOMState)
  {
   case toOliveMoths_InitialState: // Initial state always starts with develop
    m_CurrentOMState=toOliveMoths_Move;
    break;
  case toOliveMoths_Move:
    m_CurrentOMState=st_Movement(); // Will return develop
    break;
   case toOliveMoths_Develop:
    m_CurrentOMState=st_Develop(); // Will return movement or die
    m_StepDone=true;
    break;
  case toOliveMoths_Lay:
    m_CurrentOMState=st_Lay(); // Will return develop
    break;
  case toOliveMoths_Progress:
    m_CurrentOMState = st_Progress();
    m_StepDone = true;
    break;
  case toOliveMoths_Die:
    cout << "Dying: " << myID << "<n";
     st_Dying(); // No return value - no behaviour after this
     m_StepDone=true;
     break;
  default:
     m_OurLandscape->Warn("OliveMothAdult::Step()","unknown state - default");
     exit(1);
  }
}

TTypeOfOliveMothState OliveMothAdult::st_Movement( void )
{
  int SimW=m_OurPopulationManager->SimW;
  int SimH=m_OurPopulationManager->SimH;
  int newx=m_Location_x+(random(11)-5);
  int newy=m_Location_y+(random(11)-5);

  if (newx < 0)
    newx=0;
  if (newx >= SimW)
    newx=SimW-1;
      
  if (newy < 0)
    newy=0;
  if (newy >= SimW)
    newy=SimH-1;
      
  if (m_OurPopulationManager->isHabitat(newx,newy,tole_Field)) {
    m_Location_x=newx;
    m_Location_y=newy;
  } 
  //  cout << "Element Type: " <<  m_OurLandscape->SupplyElementType(m_Location_x, m_Location_y) << "\n";
  return toOliveMoths_Lay;
}

TTypeOfOliveMothState OliveMothAdult::st_Lay(void)
{
  if (m_Age > cfg_begintolay.value()) {
    for (int i=0;i < cfg_ntolay.value(); i++) {
      lay();
    }
  } 
  return toOliveMoths_Develop;
}

TTypeOfOliveMothState OliveMothAdult::st_Develop(void)
{
  /** 
   * First takes the intrinsic mortality test.
   * Then if not dead, age by a day and see if we need to hatch
   */
  //	if (g_rand_uni() < m_AdultDailyMortality) return toOliveMothS_Die;
  float T=20.0;
  dev_progress+=1.0/(devparam_a*pow(T,devparam_b));
  if (dev_progress > 1.0)
    return toOliveMoths_Die;
  else
    return toOliveMoths_Progress;
}

///////////////////////// PHI ////////////////////////////////////////////////////////
OliveMothPhiEgg::OliveMothPhiEgg(int  a_x, int  a_y,
       Landscape* p_L,
       OliveMoth_Population_Manager* p_BPM)
  : OliveMothEgg(a_x, a_y, p_L, p_BPM)
{
  devparam_a=cfg_phi_eggdev_a.value();
  devparam_b=cfg_phi_eggdev_b.value();
}

void OliveMothPhiEgg::ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM) {
  TAnimal::ReinitialiseObject(a_x, a_y, p_L);
  m_OurPopulationManager=p_PM;
  m_CurrentOMState=toOliveMoths_InitialState;
  m_Age=0;
  dev_progress=0.0;
  devparam_a=cfg_phi_eggdev_a.value();
  devparam_b=cfg_phi_eggdev_b.value();
}

OliveMothPhiLarva::OliveMothPhiLarva(int  a_x, int  a_y,
	Landscape* p_L,
	OliveMoth_Population_Manager* p_BPM)
  : OliveMothLarva(a_x, a_y, p_L, p_BPM)
{
  devparam_a=cfg_phi_larvadev_a.value();
  devparam_b=cfg_phi_larvadev_b.value();
}

void OliveMothPhiLarva::ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM) {
  TAnimal::ReinitialiseObject(a_x, a_y, p_L);
  m_OurPopulationManager=p_PM;
  m_CurrentOMState=toOliveMoths_InitialState;
  m_Age=0;
  dev_progress=0.0;
  devparam_a=cfg_phi_larvadev_a.value();
  devparam_b=cfg_phi_larvadev_b.value();
}

OliveMothPhiPupa::OliveMothPhiPupa(int  a_x, int  a_y,
	Landscape* p_L,
	OliveMoth_Population_Manager* p_BPM)
  : OliveMothPupa(a_x, a_y, p_L, p_BPM)
{
  devparam_a=cfg_phi_pupadev_a.value();
  devparam_b=cfg_phi_pupadev_b.value();
}

void OliveMothPhiPupa::ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM) {
  TAnimal::ReinitialiseObject(a_x, a_y, p_L);
  m_OurPopulationManager=p_PM;
  m_CurrentOMState=toOliveMoths_InitialState;
  m_Age=0;
  dev_progress=0.0;
  devparam_a=cfg_phi_pupadev_a.value();
  devparam_b=cfg_phi_pupadev_b.value();
}

OliveMothPhiAdult::OliveMothPhiAdult(int  a_x, int  a_y,
	Landscape* p_L,
	OliveMoth_Population_Manager* p_BPM)
  : OliveMothAdult(a_x, a_y, p_L, p_BPM)
{
  devparam_a=cfg_phi_adultdev_a.value();
  devparam_b=cfg_phi_adultdev_b.value();
}

void OliveMothPhiAdult::ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM) {
  TAnimal::ReinitialiseObject(a_x, a_y, p_L);
  m_OurPopulationManager=p_PM;
  m_CurrentOMState=toOliveMoths_InitialState;
  m_Age=0;
  dev_progress=0.0;
  devparam_a=cfg_phi_adultdev_a.value();
  devparam_b=cfg_phi_adultdev_b.value();
}

///////////////////////// ANT ////////////////////////////////////////////////////////

OliveMothAntEgg::OliveMothAntEgg(int  a_x, int  a_y,
       Landscape* p_L,
       OliveMoth_Population_Manager* p_BPM)
  : OliveMothEgg(a_x, a_y, p_L, p_BPM)
{
  devparam_a=cfg_phi_eggdev_a.value();
  devparam_b=cfg_phi_eggdev_b.value();
}

void OliveMothAntEgg::ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM) {
  TAnimal::ReinitialiseObject(a_x, a_y, p_L);
  m_OurPopulationManager=p_PM;
  m_CurrentOMState=toOliveMoths_InitialState;
  m_Age=0;
  dev_progress=0.0;
  devparam_a=cfg_phi_eggdev_a.value();
  devparam_b=cfg_phi_eggdev_b.value();
}

OliveMothAntLarva::OliveMothAntLarva(int  a_x, int  a_y,
	Landscape* p_L,
	OliveMoth_Population_Manager* p_BPM)
  : OliveMothLarva(a_x, a_y, p_L, p_BPM)
{
  devparam_a=cfg_phi_larvadev_a.value();
  devparam_b=cfg_phi_larvadev_b.value();
}

void OliveMothAntLarva::ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM) {
  TAnimal::ReinitialiseObject(a_x, a_y, p_L);
  m_OurPopulationManager=p_PM;
  m_CurrentOMState=toOliveMoths_InitialState;
  m_Age=0;
  dev_progress=0.0;
  devparam_a=cfg_phi_larvadev_a.value();
  devparam_b=cfg_phi_larvadev_b.value();
}

OliveMothAntPupa::OliveMothAntPupa(int  a_x, int  a_y,
	Landscape* p_L,
	OliveMoth_Population_Manager* p_BPM)
  : OliveMothPupa(a_x, a_y, p_L, p_BPM)
{
  devparam_a=cfg_phi_pupadev_a.value();
  devparam_b=cfg_phi_pupadev_b.value();
}

void OliveMothAntPupa::ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM) {
  TAnimal::ReinitialiseObject(a_x, a_y, p_L);
  m_OurPopulationManager=p_PM;
  m_CurrentOMState=toOliveMoths_InitialState;
  m_Age=0;
  dev_progress=0.0;
  devparam_a=cfg_phi_pupadev_a.value();
  devparam_b=cfg_phi_pupadev_b.value();
}

OliveMothAntAdult::OliveMothAntAdult(int  a_x, int  a_y,
	Landscape* p_L,
	OliveMoth_Population_Manager* p_BPM)
  : OliveMothAdult(a_x, a_y, p_L, p_BPM)
{
  devparam_a=cfg_phi_adultdev_a.value();
  devparam_b=cfg_phi_adultdev_b.value();
}

void OliveMothAntAdult::ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM) {
  TAnimal::ReinitialiseObject(a_x, a_y, p_L);
  m_OurPopulationManager=p_PM;
  m_CurrentOMState=toOliveMoths_InitialState;
  m_Age=0;
  dev_progress=0.0;
  devparam_a=cfg_phi_adultdev_a.value();
  devparam_b=cfg_phi_adultdev_b.value();
}


///////////////////////// CAR ////////////////////////////////////////////////////////

OliveMothCarEgg::OliveMothCarEgg(int  a_x, int  a_y,
       Landscape* p_L,
       OliveMoth_Population_Manager* p_BPM)
  : OliveMothEgg(a_x, a_y, p_L, p_BPM)
{
  devparam_a=cfg_phi_eggdev_a.value();
  devparam_b=cfg_phi_eggdev_b.value();
}

void OliveMothCarEgg::ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM) {
  TAnimal::ReinitialiseObject(a_x, a_y, p_L);
  m_OurPopulationManager=p_PM;
  m_CurrentOMState=toOliveMoths_InitialState;
  m_Age=0;
  dev_progress=0.0;
  devparam_a=cfg_phi_eggdev_a.value();
  devparam_b=cfg_phi_eggdev_b.value();
}

OliveMothCarLarva::OliveMothCarLarva(int  a_x, int  a_y,
	Landscape* p_L,
	OliveMoth_Population_Manager* p_BPM)
  : OliveMothLarva(a_x, a_y, p_L, p_BPM)
{
  devparam_a=cfg_phi_larvadev_a.value();
  devparam_b=cfg_phi_larvadev_b.value();
}

void OliveMothCarLarva::ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM) {
  TAnimal::ReinitialiseObject(a_x, a_y, p_L);
  m_OurPopulationManager=p_PM;
  m_CurrentOMState=toOliveMoths_InitialState;
  m_Age=0;
  dev_progress=0.0;
  devparam_a=cfg_phi_larvadev_a.value();
  devparam_b=cfg_phi_larvadev_b.value();
}

OliveMothCarPupa::OliveMothCarPupa(int  a_x, int  a_y,
	Landscape* p_L,
	OliveMoth_Population_Manager* p_BPM)
  : OliveMothPupa(a_x, a_y, p_L, p_BPM)
{
  devparam_a=cfg_phi_pupadev_a.value();
  devparam_b=cfg_phi_pupadev_b.value();
}

void OliveMothCarPupa::ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM) {
  TAnimal::ReinitialiseObject(a_x, a_y, p_L);
  m_OurPopulationManager=p_PM;
  m_CurrentOMState=toOliveMoths_InitialState;
  m_Age=0;
  dev_progress=0.0;
  devparam_a=cfg_phi_pupadev_a.value();
  devparam_b=cfg_phi_pupadev_b.value();
}

OliveMothCarAdult::OliveMothCarAdult(int  a_x, int  a_y,
	Landscape* p_L,
	OliveMoth_Population_Manager* p_BPM)
  : OliveMothAdult(a_x, a_y, p_L, p_BPM)
{
  devparam_a=cfg_phi_adultdev_a.value();
  devparam_b=cfg_phi_adultdev_b.value();
}

void OliveMothCarAdult::ReInit(int a_x, int a_y, Landscape* p_L, OliveMoth_Population_Manager* p_PM) {
  TAnimal::ReinitialiseObject(a_x, a_y, p_L);
  m_OurPopulationManager=p_PM;
  m_CurrentOMState=toOliveMoths_InitialState;
  m_Age=0;
  dev_progress=0.0;
  devparam_a=cfg_phi_adultdev_a.value();
  devparam_b=cfg_phi_adultdev_b.value();
}
