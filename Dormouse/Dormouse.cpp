/*
*******************************************************************************************************
Copyright (c) 2013, Lars Dalby & Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Dormouse.cpp 
\brief <B>The main source code for all dormouse lifestage and population manager classes</B>
*/
/**  \file Dormouse.cpp
Version of December 2013 \n
By Lars Dalby & Chris J. Topping \n \n
*/


#include <iostream>
#include <fstream>
#include <vector>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Dormouse/Dormouse.h"
#include "../Dormouse/Dormouse_Population_Manager.h"
#include "../BatchALMaSS/BoostRandomGenerators.h"

// Get the random number generator from the boost library:
extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;
extern MapErrorMsg* g_msg;

/** @todo LADA: These have previously been defined in the BoostRandomGenerators.h 
Testing this out as a way of being able to draw random numbers from a normal distribution
*/

Mersenne_Twister twist;
NormalDistDouble normal(4, 1);  // Based on table 6 page 53 in Juskaitis 2008. 
Variate_gen variate_gen(twist, normal);


using namespace std;

//-------------------------------------------------------------------------------------------

//								 Constants

//-------------------------------------------------------------------------------------------
/** 24 days gestation */
const unsigned TheGestationPeriod = 24;  // Gestation period between 22-26 days (Juskaitis 2008 p. 52-53)
/** Age at weaning */
const unsigned WeanedAge = 40;  // Juveniles become independent at age 35-45 days (Juskaitis 2008 p. 53)
/** @todo LADA: The FemaleMovement is made up - get some realistic values from Michelle and Rasmus + other telemetri studies */
const unsigned FemaleMovement = 75;   
/** @todo LADA: Copied from the vole code: arbitrary minimum movement of 5 - because we need one */
const unsigned MinFemaleMovement = 5;
const unsigned MaleMovement = 110;  // Telemetry data from Rasmus.
/** @todo LADA: Copied from the vole code: arbitrary minimum movement of 5 - because we need one */
const unsigned MinMaleMovement = 5;
/** Max lifespan. */
const unsigned MaxLifeSpan = 4 * 365;  // Life tables in Juskaitis, R. (1999) 44, 465-470. A few individuals survive to year 4

//----------------------------- External variables --------------------------
/** The probability of movement to less favourable habitat */
extern double g_MoveToLessFavourable;
extern double g_DailyMortRisk;
extern const int Vector_x[8];
extern const int Vector_y[8];

//-------------------------------------------------------------------------------------------

//								 Config variables

//-------------------------------------------------------------------------------------------

/** \brief Input parameter for daily hibernation mortality risk */
static CfgFloat cfg_HibernationMortality("DOR_HIBERNATIONMORTALITY", CFG_CUSTOM, 0.0001);  // Approximately 0.9851 survival probality (150 days of hibernation)
/** \brief Input parameter for time needed for preparation for hibernation */
static CfgInt cfg_FattningCounter("DOR_FATNINGCOUNTER", CFG_CUSTOM, 10);
/** \brief Input parameter for daily extra mortality chance while dispersing */
CfgFloat cfg_ExtraDispersalMortality("DOR_DISPERSALMORTALITY", CFG_CUSTOM, 0.055);  /**@todo LADA: This value is copied from the vole code - the value needs to be checked */
/** \brief Onset of hibernation */
static CfgInt cfg_HibernationStart("DOR_HIBERNATIONSTART", CFG_CUSTOM, 300);
/** \brief End of the hibernation period for males */
static CfgInt cfg_HibernationEndMale("DOR_HIBERNATIONENDMALE", CFG_CUSTOM, 100);  //  Early April
/** \brief End of the hibernation period for females */
static CfgInt cfg_HibernationEndFemale("DOR_HIBERNATIONENDFEMALE", CFG_CUSTOM, 114); // females emerge two weeks later (refs in Juskaitis 2008, p. 43)
/** \brief Start fattening up for hibernation */
static CfgInt cfg_FatteningPeriodStart("DOR_FATTENINGSTART", CFG_CUSTOM, 290);
/** Scales to the average minimum territory quality acceptable */
static CfgFloat cfg_Dor_HabQualScaler("DOR_HABQUALSCALER", CFG_CUSTOM, 0.25);  /** Min 0.2 ha of optimal habitat within the home range (Min effective habitat size from Shulz et al 2012) */
/** Density dependence constant */
CfgInt cfg_DorDensityDepConst("DOR_DENSITYDEPCONST", CFG_CUSTOM, 2);  /**@todo LADA: Random pick. This needs to be checked against the literature */
/** Minimum territory size for males */
CfgInt cfg_Dor_MinMaleTerritorySize("DOR_MINMALETERRITORYSIZE", CFG_CUSTOM, 89); /** Home range during active season 0.8 - 1 ha (Juskaitis 1997, p 114-115). */
/** Maximum territory size for males */
static CfgInt cfg_Dor_MaxMaleTerritorySize("DOR_MAXMALETERRITORYSIZE", CFG_CUSTOM, 100);  /** Home range during active season 0.8 - 1 ha (Juskaitis 1997, p 114-115). */
/** Minimum territory size for females */
CfgInt cfg_Dor_MinFemaleTerritorySize("DOR_MINFEMALETERRITORYSIZE", CFG_CUSTOM, 89); /** Home range during active season 0.8 - 1 ha (Juskaitis 1997, p 114-115). */
/** Maximum territory size for females */
static CfgInt cfg_Dor_MaxFemaleTerritorySize("DOR_MAXFEMALETERRITORYSIZE", CFG_CUSTOM, 100);  /** Home range during active season 0.8 - 1 ha (Juskaitis 1997, p 114-115). */
/** Maximum days in unsuitable habitat */
CfgInt cfg_Dor_MaxDaysInUnsuitable("DOR_MAXDAYSINUNSUITABLE", CFG_CUSTOM, 10);  /** Guestimate. An individual endured 7 days on a field according to Mortelliti et al. (2013). Mammalian Biology. */
/** @todo LADA: Currently testing the sensitivity of this value, hence the strange value. */

//-------------------------------------------------------------------------------------------

//                          Dormouse_Base

//-------------------------------------------------------------------------------------------

// Starting values (overwritten later in the decendent classes)
bool Dormouse_Base::m_BreedingSeason = false;
unsigned int Dormouse_Base::m_MaxMaleTerritorySize = 0;
unsigned int Dormouse_Base::m_MinMaleTerritorySize = 0;
unsigned int Dormouse_Base::m_MaxFemaleTerritorySize = 0;
unsigned int Dormouse_Base::m_MinFemaleTerritorySize = 0;  /** @todo LADA: Several versions of variables related to territory size exists. Clean this up. */
double Dormouse_Base::m_MinMDorHabQual = 0;
double Dormouse_Base::m_MinFDorHabQual = 0;


/** \brief Dormouse_Base constructor. */
Dormouse_Base::Dormouse_Base(struct_Dormouse* a_SD) : TAnimal(a_SD->x, a_SD->y, a_SD->L)
{
	// Assign the pointer to the population manager
	m_OurPopulationManager = a_SD->DPM;
	CurrentDorState = toDormouses_InitialState;
	SimH = m_OurPopulationManager->SimH;
	SimW = m_OurPopulationManager->SimW;
	m_DispersalMax = (unsigned)floor((g_rand_uni() * 90.0) + 0.5) + 10; // Max movement is a number between 10 & 100
	m_MinTerrRange = 40;  /** @todo LADA: This values needs to be corrected at a later stage - currently just testing*/
	m_TerrRange = 0;
	m_DispVector = -1;
	m_Have_Territory = false;
	m_FatCount = 0;
	m_DaysInUnsuitable = 0;
	m_Mature = false;
	m_MaxDaysInUnsuitable = (unsigned)cfg_Dor_MaxDaysInUnsuitable.value();
	m_Hibernating = true;
}

/** \brief Dormouse_Base deconstructor. */
Dormouse_Base::~Dormouse_Base(void)
{
	;
}

/** \brief Movement */
/**
This will alter m_Location_x & m_Location_y. \n
It will give a rather directed movement towards p_Vector. \n
Generally the dormouse will stay in the best habitat, but occasional mis-steps occur.\n

* \param[in] p_vector	Gives the preferred direction (0-7)
* \param[in] p_Distance	The distance to move (number of steps)
* \param[in] p_iterations	The number of iterations

*/
void Dormouse_Base::MoveTo(int p_Vector, int p_Distance, int p_iterations)
{
	int offset = p_Distance * p_iterations;
	if ((m_Location_x - offset < 0) || (m_Location_x + offset >= SimW) || (m_Location_y - offset < 0) || (m_Location_y + offset >= SimH))
	{
		// Need correct coords
		// Make sure that the coords can't become -ve
		int vx = m_Location_x + SimW;
		int vy = m_Location_y + SimH;
		do
		{
			DoWalkingCorrect(p_Distance, p_Vector, vx, vy);
			p_Distance = 1;
		} while ((GetLocation(vx%SimW, vy%SimH)) && (p_iterations-- > 0));
		// alter the location of the dormouse (& correct coords)
		FreeLocation();
		m_Location_x = vx%SimW;
		m_Location_y = vy%SimH;
		SetLocation();
		}
	else
	{
		// Don't need correct coords
		int vx = m_Location_x;
		int vy = m_Location_y;
		do
		{
			DoWalking(p_Distance, p_Vector, vx, vy);
			p_Distance = 1;
		} while ((GetLocation(vx, vy)) && (p_iterations-- > 0));
		// alter the location of the dormouse (& correct coords)
		FreeLocation();
		m_Location_x = vx;
		m_Location_y = vy;
		SetLocation();
	}
}
//-------------------------------------------------------------------------------------------

/** \brief Walking */
/**
* \param[in] p_Distance	The distance to walk
* \param[in] p_Vector	?
* \param[in] vx			The x-coordinate of the individual at start
* \param[in] vy			The y-coordinate of the individual at start
@todo LADA: Add the explanation to p_Vector here.
*/
void Dormouse_Base::DoWalking(int p_Distance, int &p_Vector, int &vx, int &vy)
{
	int t[5], q[5];
	t[0] = p_Vector;
	t[1] = (p_Vector + 1) & 0x07;
	t[2] = (p_Vector + 7) & 0x07;
	t[3] = (p_Vector + 2) & 0x07;
	t[4] = (p_Vector + 6) & 0x07;
	
	for (int i = 0; i < p_Distance; i++)
	{
		// test the squares at Vector, Vector+1+2, Vector-1-2
		// They have either a quality or are inaccessible (water,buildings)
		// if can go to one of these squares then pick the best one
		// if all or some are equal then take a random pick.
		// if all are 'bad' then add or subtract one from vector and try again

		/*	t[0] = p_Vector;
		t[1] = (p_Vector+1) & 0x07;
		t[2] = (p_Vector+7) & 0x07;
		t[3] = (p_Vector+2) & 0x07;
		t[4] = (p_Vector+6) & 0x07; */

		q[0] = MoveQuality((vx + Vector_x[t[0]]), (vy + Vector_y[t[0]]));
		q[1] = MoveQuality((vx + Vector_x[t[1]]), (vy + Vector_y[t[1]]));
		q[2] = MoveQuality((vx + Vector_x[t[2]]), (vy + Vector_y[t[2]]));
		q[3] = MoveQuality((vx + Vector_x[t[3]]), (vy + Vector_y[t[3]]));
		q[4] = MoveQuality((vx + Vector_x[t[4]]), (vy + Vector_y[t[4]]));
		if (g_rand_uni() < g_MoveToLessFavourable)  // probability test for whether or not a move to less favourable habitat can be made.
		{
			// allow a mistake once in a while
			for (int j = 1; j < 5; j++) q[j] = -1;
		}
		// Now pick the best of these
		int best = q[0];
		int score = 1;
		for (int i = 1; i<5; i++)
		{
			if (q[i]>best)
			{
				best = q[i];
				score = 1;
			}
			else if (q[i] == best) score++;
		}
		if (best == -1)
		{
			// can't go anywhere so change the vector
			if (random(2)) ++p_Vector; else (--p_Vector);
			p_Vector &= 0x07;
			t[0] = p_Vector;
			t[1] = (p_Vector + 1) & 0x07;
			t[2] = (p_Vector + 7) & 0x07;
			t[3] = (p_Vector + 2) & 0x07;
			t[4] = (p_Vector + 6) & 0x07;
		}
		else
		{
			// Can go to one of score squares
			int scored = random(score); // pick one
			int loop = 0;
			for (int i = 0; i<5; i++)
			{
				if (best == q[i]) loop++; // count the squares with 'best' quality
				if (loop>scored)
				{
					loop = i; // go to i-square
					break;
				}
			}
			// change co-ordinates
			vx += Vector_x[t[loop]];
			vy += Vector_y[t[loop]];
		}
	}
}
//-------------------------------------------------------------------------------------------

/** \brief Walking where there is a danger of stepping off the world. */
/**
This method does the actual stepping - there is no look ahead here, so steps are taken one at a time based on the habitat type and vector given.
\n This version corrects coords for wrap around. This is slower so is only called when necessary.
* \param[in] p_Distance	The distance to walk
* \param[in] p_Vector	?
* \param[in] vx			The x-coordinate of the individual at start
* \param[in] vy			The y-coordinate of the individual at start
@todo LADA: Add the explanation to p_Vector here.
*/
void Dormouse_Base::DoWalkingCorrect(int p_Distance, int &p_Vector, int &vx, int &vy)
{
	int t[5], q[5];
	t[0] = p_Vector;
	t[1] = (p_Vector + 1) & 0x07;
	t[2] = (p_Vector + 7) & 0x07;
	t[3] = (p_Vector + 2) & 0x07;
	t[4] = (p_Vector + 6) & 0x07;
	for (int i = 0; i < p_Distance; i++)
	{
		// test the squares at Vector, Vector+1+2, Vector-1-2
		// They have either a quality or are inaccessible (water,buildings)
		// if can go to one of these squares then pick the best one
		// if all or some are equal then take a random pick.
		// if all are 'bad' then add or subtract one from vector and try again
		/* t[0] = p_Vector;
		t[1] = (p_Vector+1) & 0x07;
		t[2] = (p_Vector+7) & 0x07;
		t[3] = (p_Vector+2) & 0x07;
		t[4] = (p_Vector+6) & 0x07; */
		q[0] = MoveQuality((vx + Vector_x[t[0]])%SimW, (vy + Vector_y[t[0]])%SimH);
		q[1] = MoveQuality((vx + Vector_x[t[1]])%SimW, (vy + Vector_y[t[1]])%SimH);
		q[2] = MoveQuality((vx + Vector_x[t[2]])%SimW, (vy + Vector_y[t[2]])%SimH);
		q[3] = MoveQuality((vx + Vector_x[t[3]])%SimW, (vy + Vector_y[t[3]])%SimH);
		q[4] = MoveQuality((vx + Vector_x[t[4]])%SimW, (vy + Vector_y[t[4]])%SimH);
		if (g_rand_uni() < g_MoveToLessFavourable)
		{
			// allow a mistake once in a while
			for (int j = 1; j < 5; j++) q[j] = -1;
		}
		// Now pick the best of these
		int best = q[0];
		int score = 1;
		for (int i = 1; i<5; i++)
		{
			if (q[i]>best)
			{
				best = q[i];
				score = 1;
			}
			else
			if (q[i] == best) score++;
		}
		if (best == -1)
		{
			// can't go anywhere so change the vector
			if (random(2)) ++p_Vector; else (--p_Vector);
			p_Vector &= 0x07;
			// /*
			t[0] = p_Vector;
			t[1] = (p_Vector + 1) & 0x07;
			t[2] = (p_Vector + 7) & 0x07;
			t[3] = (p_Vector + 2) & 0x07;
			t[4] = (p_Vector + 6) & 0x07; //*/
		}
		else
		{
			// Can go to one of score squares
			int scored = random(score); // pick one
			int loop = 0;
			for (int i = 0; i<5; i++)
			{
				if (best == q[i]) loop++; // count the squares with 'best' quality
				if (loop>scored)
				{
					loop = i; // go to i-square
					break;
				}
			}
			// change co-ordinates
			vx += Vector_x[t[loop]];
			vy += Vector_y[t[loop]];
		}
	}
}
//-------------------------------------------------------------------------------------------

/** \brief Test a location for quality while moving. */
/**
Can't walk through another dormouse though - so test to check if there is another individual in the location is done first.
\param[in] p_x	x-coordinate
\param[in] p_y	y-coordinate
\return The quality of a patch of habitat at p_x, p_y.
*/
int Dormouse_Base::MoveQuality(int p_x, int p_y)
{
	if (m_OurPopulationManager->DormouseMap->GetMapValue(p_x, p_y) != NULL) return -1;
	// Nobody there so carry on
	int poly_index = m_OurLandscape->SupplyPolyRefIndex(p_x, p_y);
	int score = -9999;
	TTypesOfLandscapeElement tole = m_OurLandscape->SupplyElementTypeFromVector(poly_index);
	switch (tole)
	{
	case tole_NaturalGrassDry: // 110
	case tole_NaturalGrassWet: // 110
	case tole_BeetleBank:
	case tole_PermPasture: // 35
	case tole_PermPastureLowYield: // 35
	case tole_PermPastureTussocky:
	case tole_Orchard: // 56
	case tole_MownGrass: // 58
	case tole_OrchardBand: // 57
	case tole_RoadsideSlope:
	case tole_Railway: // 118
	case tole_FieldBoundary: // 160
	case tole_RoadsideVerge: // 13
	case tole_HedgeBank:
	case tole_PermanentSetaside:
	case tole_Saltmarsh: // 95
	case tole_Marsh: // 95
	case tole_Heath:
	case tole_AmenityGrass:
	case tole_Garden: //11
	case tole_Track: // 123
	case tole_SmallRoad: // 122
	case tole_LargeRoad: // 121
	case tole_BareRock:
	case tole_UrbanNoVeg:
	case tole_UrbanPark:
	case tole_SandDune:
	case tole_UnsprayedFieldMargin:
	case tole_Field: // 20 & 30
		score = 1;
		break;
	case tole_Churchyard:  //204
	case tole_BuiltUpWithParkland:
	case tole_Parkland:
	case tole_RiversidePlants: // 98
	case tole_PitDisused: // 75
		score = 2;
		break;
	case tole_YoungForest: // 60
	case tole_Scrub: // 70
	case tole_Hedges: // 130 (internal ALMaSS representation for Hedges)
	case tole_RiversideTrees: // 97
	case tole_MixedForest: // 60
	case tole_DeciduousForest: // 40
	case tole_ConiferousForest: // 50
	case tole_Copse:
		score = 3;
		break;
	case tole_MetalledPath:
	case tole_Carpark:
	case tole_Stream:
	case tole_HeritageSite:
	case tole_Building: // 5
	case tole_ActivePit: // 115
	case tole_Freshwater: // 90
	case tole_Pond: // 219
	case tole_River: // 96
	case tole_Saltwater: // 80
	case tole_Coast: // 100
	case tole_StoneWall: // 15
	case tole_Fence:
		score = -1;
		break;
	case tole_Foobar: // 999 !! type unknown - should not happen
		/** @todo Decide where to classify new LE types for dormouse */
	case tole_Wasteland:
	case tole_IndividualTree:
	case tole_WoodyEnergyCrop:
	case tole_PlantNursery:
	case tole_Pylon:
	case tole_WindTurbine:
	case tole_WoodlandMargin:
	case tole_Vildtager:
		static char errornum[20];
		sprintf(errornum, "%d", m_OurLandscape->SupplyElementTypeFromVector(poly_index));
		m_OurLandscape->Warn("Dormouse_Base:MoveQuality: Unknown tole_type", errornum);
		exit(1);
	}
	return score;
}
//-------------------------------------------------------------------------------------------

/** \brief Calculate quality. */
/**
Returns the sum of the qualities in the area covered by the territory (even if the vole does not have one) \n\n
* Uses the algorithm for fast searching of square arrays with wrap around co-ordinates.\n
* parameters = x,y starting coordinates top left\n
* range = extent of the space to search\n
* bottom left coordinate is therefore x+range, y+range \n
* \n
* For each polygon it gets the quality and multiplies by area of that polygon in the territory.
* This is summed for all polygons in the territory to get overall quality. The value is then divided by
* the number of dormice present less a threshold value.\n
*/
double Dormouse_Base::CalculateQuality(int p_x, int p_y, int a_ddep)
{
	int NoPolygons = 0;
	int PolyRefData[500][2];
	// First convert centre co-rdinates to square co-ordinates
	double quality = 0;
	int x = p_x - m_MinTerrRange;
	if (x < 0) x += SimW;
	int y = p_y - m_MinTerrRange;
	if (y < 0) y += SimH;
	int range_x = m_MinTerrRange + m_MinTerrRange;
	int range_y = m_MinTerrRange + m_MinTerrRange;
	// Stage 1 make a list of polygons
	// create the extent variables
	int xextent0 = x + range_x;
	int yextent0 = y + range_y;
	int xextent1 = (x + range_x) - SimW;
	int yextent1 = (y + range_y) - SimH;
	// Create the looping variables needed
	int Dfinx = xextent0;
	int Dfiny = yextent0;
	int Afinx = 0;  // unless the finx values for A-C are changed
	int Bfinx = 0;  // the value of zero will stop the A-C loops from executing
	int Cfinx = 0;
	int Afiny = 0; // this one assigned 0 to stop compiler complaints
	// Now create the loop values;
	if (x + range_x <= SimW)
	{
		// Type B & D (overlap bottom, no overlap)
		if (yextent0 > SimH)
		{
			// Type B (overlap bottom only)
			Dfiny = SimH;
			Bfinx = x + range_x;
		}
	}
	else
	{
		// Type A & C overlap left edge or bottom & left
		if (yextent0 > SimH)
		{
			// Type C overlap bottom and left
			Afinx = xextent1;
			Afiny = SimH;
			Bfinx = SimW;
			Cfinx = xextent1;
			Dfinx = SimW;
			Dfiny = SimH;
		}
		else
		{
			// Type A overlap left edge
			Afinx = xextent1;
			Afiny = yextent0;
			Dfinx = SimW;
		}
	}
	// the default is:
	// Type D no overlap
	// A Loop
	for (int i = 0; i < Afinx; i++)
	{
		for (int j = y; j < Afiny; j++)
		{
			// Get the polyref
			int PRef = m_OurLandscape->SupplyPolyRefIndex(i, j);
			// check if we have had this one already
			///*
			bool found = false;
			for (int k = 0; k < NoPolygons; k++)
			{
				if (PolyRefData[k][0] == PRef)
				{
					PolyRefData[k][1]++;
					found = true;
					break;
				}
			}
			if (!found)
				//*/
			{
				// don't have this one so get the height & type to the PolyDatas arrays
				PolyRefData[NoPolygons][0] = PRef;
				PolyRefData[NoPolygons][1] = 1;
				NoPolygons++;
			}
		}
	}
	// B Loop
	for (int i = x; i < Bfinx; i++)
	{
		for (int j = 0; j < yextent1; j++)
		{
			// Get the polyref
			int PRef = m_OurLandscape->SupplyPolyRefIndex(i, j);
			// check if we have had this one already

			bool found = false;
			for (int k = 0; k < NoPolygons; k++)
			{
				if (PolyRefData[k][0] == PRef)
				{
					PolyRefData[k][1]++;
					found = true;
					break;
				}
			}
			if (!found)
				//*/
			{
				// don't have this one so get the height & type to the PolyDatas arrays
				PolyRefData[NoPolygons][0] = PRef;
				PolyRefData[NoPolygons][1] = 1;
				NoPolygons++;
			}
		}
	}
	// C Loop
	for (int i = 0; i < Cfinx; i++)
	{
		for (int j = 0; j < yextent1; j++)
		{
			// Get the polyref
			int PRef = m_OurLandscape->SupplyPolyRefIndex(i, j);
			// check if we have had this one already

			bool found = false;
			for (int k = 0; k < NoPolygons; k++)
			{
				if (PolyRefData[k][0] == PRef)
				{
					PolyRefData[k][1]++;
					found = true;
					break;
				}
			}
			if (!found)
				//*/
			{
				// Don't have this one so get the height & type to the PolyDatas arrays
				PolyRefData[NoPolygons][0] = PRef;
				PolyRefData[NoPolygons][1] = 1;
				NoPolygons++;
			}
		}
	}
	// D Loop
	int last = -99999;
	int k_index = -1;
	for (int i = x; i < Dfinx; i++)
	{
		for (int j = y; j < Dfiny; j++)
		{
			// Get the polyref
			int PRef = m_OurLandscape->SupplyPolyRefIndex(i, j);
			// check if we have had this one already
			if (last == PRef)
			{
				PolyRefData[k_index][1]++;
			}
			else
			{

				bool found = false;
				for (int k = 0; k < NoPolygons; k++)
				{
					if (PolyRefData[k][0] == PRef)
					{
						PolyRefData[k][1]++;
						found = true;
						last = PRef;
						k_index = k;
						break;
					}
				}
				if (!found)
				{
					// don't have this one so get the height & type to the PolyDatas arrays
					PolyRefData[NoPolygons][0] = PRef;
					PolyRefData[NoPolygons][1] = 1;
					NoPolygons++;
				}
			}
		}
	}
	// End of search algorithm
	/* for each polygon get the quality and multiply by amount of that polygon in the territory sum this to get overall quality */
	for (int i = 0; i < NoPolygons; i++)
	{
		quality += PolyRefData[i][1] * m_OurPopulationManager->GetHabitatQuality(PolyRefData[i][0]);
	}
	
	int Dormice = m_OurPopulationManager->SupplyHowManyDormice(m_Location_x, m_Location_y, m_TerrRange);
	if (Dormice > a_ddep) quality /= (Dormice - a_ddep);  // Devision assingment - read: a = a/b
	// return the total quality
	return quality;
}
//-------------------------------------------------------------------------------------------

/** \brief Fattening up for hibernation. */
/**
The dormice will spend a number of days fattening up before entering hibernation 
\return The dormouse state toDormouses_GetFat (until fattening period is over).
\return The dormouse state toDormouses_Hibernate (once the fattening period is over).
*/
TTypeOfDormouseState Dormouse_Base::st_GetFat()
{
	if (m_FatCount++ >= cfg_FattningCounter.value())
	{
		m_FatCount = 0;  // Zero the fatting counter for next season.
		return toDormouses_Hibernate;
	}
	return toDormouses_GetFat;
}
//-------------------------------------------------------------------------------------------

/** \brief Do a mortality test */
/**
Constant daily mortality rate - repeated calls increase the risk of dying
\return true (if the individual should die), false otherwise.
*/
bool Dormouse_Base::MortalityTest()
{
	// returns true if the dormouse should die
	if (m_Hibernating)
	{
		if (g_rand_uni() < cfg_HibernationMortality.value()) return true;		
	}
	else if (g_rand_uni() < g_DailyMortRisk)
	{		
		return true;
	}
	return false;
}
//-------------------------------------------------------------------------------------------

/**
This function kills the individual and will free up the space on the map.
*/
void Dormouse_Base::st_Dying()
{
	m_CurrentStateNo = -1; // this will kill the animal object and free up space (on the map)
	FreeLocation();
}

/** \brief The first of the three timestep divisions. */
/**
The BeginStep is one of the three timestep divisions. This is called once for each individual before Step and EndStep. \n
The main function here is to remove individuals that die before they take up CPU resources in the Step code.
*/
void Dormouse_Base::BeginStep()
{
	if (MortalityTest() || m_Age > MaxLifeSpan || m_DaysInUnsuitable >= m_MaxDaysInUnsuitable)
	{
		st_Dying();
		m_StepDone = true;
	}
}
//---------------------------------------------------------------------------

/** \brief The last of the three timestep divisions. Is called after BeginStep & Step. */
void Dormouse_Base::EndStep()
{
	m_Age++;  // All dormice must age at the end of the day (m_Age + 1 day).
}

/** \brief Location map function */
inline void Dormouse_Base::SetLocation() {
	m_OurPopulationManager->DormouseMap->SetMapValue(m_Location_x, m_Location_y, this);
};

/** \brief Location map function */
inline void Dormouse_Base::FreeLocation() {
	m_OurPopulationManager->DormouseMap->ClearMapValue(m_Location_x, m_Location_y);
};

/** \brief Location map function */
inline bool Dormouse_Base::GetLocation(int px, int py) {
	if (m_OurPopulationManager->DormouseMap->GetMapValue(px, py) != NULL) return true;
	return false;
};
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

//		Dormouse_JuvenileMale code

//---------------------------------------------------------------------------

/** \brief Dormouse_JuvenileMale constructor */
Dormouse_JuvenileMale::Dormouse_JuvenileMale(struct_Dormouse * a_DormouseStruct) : Dormouse_Base(a_DormouseStruct)
{
	m_MaxMaleTerritorySize = cfg_Dor_MaxMaleTerritorySize.value();
	m_MinMaleTerritorySize = cfg_Dor_MinMaleTerritorySize.value();
	m_TerrRange = m_MinMaleTerritorySize;
	m_Sex = true;
}
//---------------------------------------------------------------------------

/** \brief Dormouse_JuvenileMale deconstructor */
Dormouse_JuvenileMale::~Dormouse_JuvenileMale()
{
	;
}
//---------------------------------------------------------------------------

void Dormouse_JuvenileMale::Step()
{
	if (m_StepDone || m_CurrentStateNo == -1) return;  // If already done or dead now - do nothing
	// ... otherwise enter main switch:
	// Main switch: 
  switch (CurrentDorState)
  {	
   case toDormouses_InitialState: // Initial state always starts with develop
    CurrentDorState = toDormouses_Develop;
    break;
   case toDormouses_Develop:
    CurrentDorState = st_JMDevelop();  // Will return BecomeAdult, movement or die
    break;
   case toDormouses_BecomeAdult:  
	st_BecomeAdult();  // Will make a new adult and kill the juvenile, but currently no other behaviour.
	m_StepDone = true;
	break;
   case toDormouses_Move:
	   CurrentDorState = st_Initial_Explore(); // Will return Develop
	m_StepDone = true;
    break;
   case toDormouses_Die:
	st_Dying(); // No return value - no behaviour after this
	m_StepDone = true;
    break;
   default:
	   m_OurLandscape->Warn("Dormouse_JuvenileMale::Step()","unknown state - default");
    exit(1);
   }
}

TTypeOfDormouseState Dormouse_JuvenileMale::st_JMDevelop()
{
	if (m_Age > 70) return toDormouses_BecomeAdult;  /**@todo LADA: Become adult when older than 70 days. Made up stuff. Weaned age + a month.*/
	return toDormouses_Move;  // toDormouses_BecomeAdult
}

void Dormouse_JuvenileMale::st_BecomeAdult()
{
	struct_Dormouse* ad;
	ad = new struct_Dormouse;
	ad->DPM = m_OurPopulationManager;
	ad->L = m_OurLandscape;
	ad->x = m_Location_x;
	ad->y = m_Location_y;
	ad->age = m_Age;  // Give the new adult individual the age of the juvenile
	ad->territory = m_Have_Territory;
	ad->hibernating = m_Hibernating;
	m_OurPopulationManager->CreateObjects(dob_Male, this, ad, 1);
	// Remove the current object
	CurrentDorState = toDormouses_Die;  // Set m_CurrentStateNo = -1 which means the object is dead
}

/** \brief Juvenile male find the first territory after becoming independent */
TTypeOfDormouseState Dormouse_JuvenileMale::st_Initial_Explore()
{ 	// The dormouse must now find it's first good territory after leaving the nest
	// He will explore in all 8 directions from the nest and pick the best from these and his present position
	if (!m_Have_Territory)  // Check if they already have a territory
	{
		unsigned coords[9][2];
		double qualities[9];
		// Check the quality in the starting location:
		coords[8][0] = m_Location_x;
		coords[8][1] = m_Location_y;
		if (m_OurPopulationManager->SupplyOlderMales(m_Location_x, m_Location_y, m_Age, m_TerrRange))
		{
			qualities[8] = -1;
		}
		else
		{
			qualities[8] = CalculateQuality(m_Location_x, m_Location_y, cfg_DorDensityDepConst.value());
		}
		// Now check the quality in all 8 directions
		unsigned p_Distance = random(MaleMovement) + MaleMovement;  // Max 2 times MaleMovement
		for (unsigned i = 0; i < 8; i++)
		{
			MoveTo(i, p_Distance, 10); // changes m_Location_x & y
			coords[i][0] = m_Location_x;
			coords[i][1] = m_Location_y;
			if (m_OurPopulationManager->SupplyOlderMales(m_Location_x, m_Location_y, m_Age, m_TerrRange))
			{
				qualities[i] = -1;
			}
			else
			{
				qualities[i] = CalculateQuality(m_Location_x, m_Location_y, cfg_DorDensityDepConst.value());
			}
		}
		// find the best out of the nine qualities
		double best = qualities[8]; // starting location
		unsigned found = 8; // default stay here
		for (unsigned i = 0; i<8; i++)
		{
			if (qualities[i]>best)
			{
				best = qualities[i];
				found = i;
			}
			if (best > 1)  // This is asking whether an older male was present or not (would be -1 if the case).
			{
				if (best >= m_MinMDorHabQual)  // Only if the habitat is above the minimum threshold can the indv. have a territory.
				{
					m_Have_Territory = true;
					m_DaysInUnsuitable = 0;  // Reset the counter once a territory has been found.
				}
			}
			else
			{
				m_DaysInUnsuitable++;
				m_Have_Territory = false;
			}
			FreeLocation();
			m_Location_x = coords[found][0];
			m_Location_y = coords[found][1];
			SetLocation();
		}
	}
	return toDormouses_Develop;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

//		Dormouse_JuvenileFemale code

//---------------------------------------------------------------------------

/** \brief Dormouse_JuvenileFemale constructor */
Dormouse_JuvenileFemale::Dormouse_JuvenileFemale(struct_Dormouse * a_DormouseStruct) : Dormouse_Base(a_DormouseStruct)
{
	m_MaxFemaleTerritorySize = cfg_Dor_MaxFemaleTerritorySize.value();
	m_MinFemaleTerritorySize = cfg_Dor_MinFemaleTerritorySize.value();
	m_TerrRange = cfg_Dor_MinFemaleTerritorySize.value();  // m_TerrRange is called in some of the generic functions, so needs to be set in both male and female with the relvant cfg values.
	m_MinFDorHabQual = cfg_Dor_HabQualScaler.value() * (3 * m_TerrRange * m_TerrRange);
	m_Sex = false;
}
//---------------------------------------------------------------------------

/** \brief Dormouse_JuvenileFemale deconstructor */
Dormouse_JuvenileFemale::~Dormouse_JuvenileFemale()
{
	;
}
//---------------------------------------------------------------------------

void Dormouse_JuvenileFemale::Step(void)
{
	if (m_StepDone || m_CurrentStateNo == -1) return;  // If already done or dead now - do nothing
	// ... otherwise enter main switch:
	// Main switch: 
	switch (CurrentDorState)
	  {	
	   case toDormouses_InitialState:  // Initial state always starts with hibernate
		CurrentDorState = toDormouses_Develop;
		break;
	   case toDormouses_Develop:
		CurrentDorState = st_JFDevelop();  // Will return BecomeAdult, movement or die
		break;
	   case toDormouses_BecomeAdult:
		st_BecomeAdult();  // Will make a new adult and kill the juvenile, but currently no other behaviour.
		m_StepDone = true;
		break;
	   case toDormouses_Move:
		CurrentDorState = st_Initial_Explore(); // Will return develop
		m_StepDone = true;
		break;
	   case toDormouses_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone = true;
		break;
	   default:
		   m_OurLandscape->Warn("Dormouse_JuvenileFemale::Step()","unknown state - default");
		exit(1);
	   }
}

TTypeOfDormouseState Dormouse_JuvenileFemale::st_JFDevelop()
{
	if (m_Age > 70) return toDormouses_BecomeAdult;   /**@todo LADA: Become adult when older than 30 days. 30 is currently completely made up stuff.*/
	return toDormouses_Move;
}

void Dormouse_JuvenileFemale::st_BecomeAdult()
{
	struct_Dormouse* ad;
	ad = new struct_Dormouse;
	ad->DPM = m_OurPopulationManager;
	ad->L = m_OurLandscape;
	ad->x = m_Location_x;
	ad->y = m_Location_y;
	ad->age = m_Age;  // Give the new adult individual the age of the juvenile
	ad->territory = m_Have_Territory;
	ad->hibernating = m_Hibernating;
	m_OurPopulationManager->CreateObjects(dob_Female, this, ad, 1);
	// Remove the current object
	CurrentDorState = toDormouses_Die;  // Set m_CurrentStateNo = -1 which means the object is dead
}

/** \brief Juvenile female dispersal. */
TTypeOfDormouseState Dormouse_JuvenileFemale::st_Dispersal()
{
	/** @todo LADA: Currently testing this method without any habitat quality assesment.*/
	if (m_DispVector == -1) m_DispVector = random(8); // Choose direction 0-7
	int p_Distance = random(FemaleMovement) + MinFemaleMovement;  // Get a distance
	MoveTo(m_DispVector, p_Distance, 10);  /** @todo LADA: Here 10 is "iterations", need to check up on what that actually is doing... */
	return toDormouses_Develop;
}
//-------------------------------------------------------------------------------------------


TTypeOfDormouseState Dormouse_JuvenileFemale::st_Initial_Explore()
{ /** @todo LADA: Check that juvs cannot end up sitting in their mother's territory forever. */
	// The dormouse must now find it's first good territory after leaving the nest
	// She will explore in all 8 directions from the nest and pick the best from these and her present position
	if (!m_Have_Territory)  // Check if they already have a territory
	{
		unsigned coords[9][2];
		double qualities[9];
		// Check the quality in the starting location:
		coords[8][0] = m_Location_x;
		coords[8][1] = m_Location_y;
		if (m_OurPopulationManager->SupplyOlderFemales(m_Location_x, m_Location_y, m_Age, m_TerrRange))
		{
			qualities[8] = -1;
		}
		else
		{
			qualities[8] = CalculateQuality(m_Location_x, m_Location_y, cfg_DorDensityDepConst.value());
		}
		// Now check the quality in all 8 directions
		unsigned p_Distance = random(FemaleMovement) + FemaleMovement;  // Max 2 times FemaleMovement
		for (unsigned i = 0; i < 8; i++)
		{
			MoveTo(i, p_Distance, 10); // changes m_Location_x & y
			coords[i][0] = m_Location_x;
			coords[i][1] = m_Location_y;
			if (m_OurPopulationManager->SupplyOlderFemales(m_Location_x, m_Location_y, m_Age, m_TerrRange))
				qualities[i] = -1;
			else
			{
				qualities[i] = CalculateQuality(m_Location_x, m_Location_y, cfg_DorDensityDepConst.value());
			}
		}
		// find the best out of the nine qualities
		double best = qualities[8]; // starting location
		unsigned found = 8; // default stay here
		for (unsigned i = 0; i<8; i++)
		{
			if (qualities[i]>best)
			{
				best = qualities[i];
				found = i;
			}
			if (best > 1)  // This is asking whether an older female was present or not (would be -1 if the case).
			{
				if (best >= m_MinFDorHabQual)  // Only if the habitat is above the minimum threshold can the indv. have a territory.
				{
					m_Have_Territory = true;
					m_DaysInUnsuitable = 0;  // reset counter once territory has been found.
				}
			}
			else
			{
				m_DaysInUnsuitable++;
				m_Have_Territory = false;
			}
			FreeLocation();
			m_Location_x = coords[found][0];
			m_Location_y = coords[found][1];
			SetLocation();
		}
	}
	return toDormouses_Develop;
}
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------

//		Dormouse_Male code

//---------------------------------------------------------------------------

/** \brief Dormouse_Male constructor */
Dormouse_Male::Dormouse_Male(struct_Dormouse * a_DormouseStruct) : Dormouse_JuvenileMale(a_DormouseStruct)
{
	Set_Mature(true);
	m_MinMDorHabQual = cfg_Dor_HabQualScaler.value() * (3 * m_MinMaleTerritorySize * m_MinMaleTerritorySize);
}
//---------------------------------------------------------------------------

Dormouse_Male::~Dormouse_Male()
{
	;
}
//---------------------------------------------------------------------------

void Dormouse_Male::Step(void)
{
	if (m_StepDone || m_CurrentStateNo == -1) return;  // If already done or dead - do nothing
	// ... otherwise enter main switch:
	// Main switch: 
	switch (CurrentDorState)
		{
		case toDormouses_InitialState:  // Initial state always starts with Hibernate
			CurrentDorState = toDormouses_Hibernate;
			break;
		case toDormouses_HangOut:
			CurrentDorState = st_HangOut();  // Returns Check_n_Explore, HangOut or GetFat
			break;
		case toDormouses_Check_n_Explore:
			CurrentDorState = st_Check_n_Explore(); // Will return Explore or HangOut
			m_StepDone = true;
			break;
		case toDormouses_Mating:
			CurrentDorState = st_Mating();  // Will return Hangout
			m_StepDone = true;
			break;
		case toDormouses_Explore:
			CurrentDorState = st_Explore();  // Will return HangOut (but the individual will move if it can find a suitable place)
			break;
		case toDormouses_GetFat:
			CurrentDorState = st_GetFat();  // Will return Hibernation or GetFat
			m_StepDone = true;
			break;
		case toDormouses_Hibernate:
			CurrentDorState = st_Hibernate();  // Will return Hiberation, Die or InitialState
			break;
		case toDormouses_Die:
			st_Dying(); // No return value - no behaviour after this
			m_StepDone = true;
			break;
		default:
			m_OurLandscape->Warn("Dormouse_Male::Step()", "unknown state - default");
			exit(1);
		}
}

/** \brief Male simply hangout. */
TTypeOfDormouseState Dormouse_Male::st_HangOut()
{
	int today = m_OurPopulationManager->m_TheLandscape->SupplyDayInYear();
	if (today >= cfg_FatteningPeriodStart.value()) {
		return toDormouses_GetFat;
	}

	else if (g_rand_uni() > 0.1) return toDormouses_Check_n_Explore;  //90% chance of them moving, 10% chance that they simply hangout
	else {
		m_StepDone = true;	// Do nothing this time round
		return toDormouses_HangOut;
		}
}
//---------------------------------------------------------------------------

/** \brief Male hibernation. */
TTypeOfDormouseState Dormouse_Male::st_Hibernate()
{
	int today = m_OurPopulationManager->m_TheLandscape->SupplyDayInYear();
	if ((today >= cfg_HibernationEndMale.value()) && (today <= cfg_HibernationStart.value()))
	{
		m_Hibernating = false;
		return toDormouses_HangOut;
	}
	else
	{
		m_StepDone = true;
		m_Hibernating = true;
		return toDormouses_Hibernate;
	}
}
//-------------------------------------------------------------------------------------------

/** \brief Checks if there is any females in the territory. */
TTypeOfDormouseState Dormouse_Male::st_Check_n_Explore()
{
	if (!m_Have_Territory)
	{
		return toDormouses_Explore; 
	}
	else if (m_Have_Territory && !m_BreedingSeason)
	{
		return toDormouses_HangOut;
	}
	else if (m_Have_Territory && m_BreedingSeason)
	{
		int Females = m_OurPopulationManager->SupplyCountFemales(m_Location_x, m_Location_y, m_TerrRange);
		if (Females < 1)  //... if none, find a new territory  /**@todo LADA: maybe they should not leave their territory immediately if no females. Waiting a couple of days maybe?
		{
			return toDormouses_Explore;
		}
		else return toDormouses_Mating;
	}
	else return toDormouses_HangOut;
}

/** The male finds and mates with any females within its territory. */
TTypeOfDormouseState Dormouse_Male::st_Mating()
{
	m_OurPopulationManager->SendMessageMated(m_Location_x, m_Location_y, m_MinMaleTerritorySize);  // Tell the females that are present within the territory that they have been mated.
	return toDormouses_HangOut;
}

/** \brief Find territory. */
TTypeOfDormouseState Dormouse_Male::st_Explore()
{	// The dormouse must find  good territory.
	// He will explore in all 8 directions from the nest and pick the best from these and his present position
	unsigned coords[9][2];
	double qualities[9];
	// Check the quality in the starting location:
	coords[8][0] = m_Location_x;
	coords[8][1] = m_Location_y;
	if (m_OurPopulationManager->SupplyOlderMales(m_Location_x, m_Location_y, m_Age, m_TerrRange))
	{
		qualities[8] = -1;
	}
	else
	{
		qualities[8] = CalculateQuality(m_Location_x, m_Location_y, cfg_DorDensityDepConst.value());
	}
	// Now check the quality in all 8 directions
	unsigned p_Distance = random(MaleMovement) + MaleMovement;  // Max 2 times MaleMovement
	for (unsigned i = 0; i < 8; i++)
	{
		MoveTo(i, p_Distance, 10); // changes m_Location_x & y
		coords[i][0] = m_Location_x;
		coords[i][1] = m_Location_y;
		if (m_OurPopulationManager->SupplyOlderMales(m_Location_x, m_Location_y, m_Age, m_TerrRange))
			qualities[i] = -1;
		else
		{
			qualities[i] = CalculateQuality(m_Location_x, m_Location_y, cfg_DorDensityDepConst.value());
		}
	}
	// find the best out of the nine qualities
	double best = qualities[8]; // starting location
	unsigned found = 8; // default stay here
	for (unsigned i = 0; i < 8; i++)
	{
		if (qualities[i]>best)
		{
			best = qualities[i];
			found = i;
		}
		if (best > 1)  // This is asking whether an older male was present or not (would be -1 if the case).
		{
			if (best >= m_MinMDorHabQual)  // Only if the habitat is above the minimum threshold can the indv. have a territory.
			{
				m_Have_Territory = true;
				m_DaysInUnsuitable = 0;  // Reset counter once territory has been found.
			}
		}
		else
		{
			m_DaysInUnsuitable++;
			m_Have_Territory = false;
		}
		FreeLocation();
		m_Location_x = coords[found][0];
		m_Location_y = coords[found][1];
		SetLocation();
	}
	return toDormouses_HangOut;
}
//---------------------------------------------------------------------------





//---------------------------------------------------------------------------

//		Dormouse_Female code

//---------------------------------------------------------------------------

/** \brief Dormouse_Female constructor */
Dormouse_Female::Dormouse_Female(struct_Dormouse * a_DormouseStruct) : Dormouse_JuvenileFemale(a_DormouseStruct)
{
	Set_Mature(true);
	m_DaysUntilBirth = 0;
	m_Pregnant = false;
	m_Mate = false;
	m_Mated = false;
	m_NoOfYoung = 0;
}
//---------------------------------------------------------------------------

/** \brief Dormouse_Female deconstructor */
Dormouse_Female::~Dormouse_Female()
{
	;
}
//-------------------------------------------------------------------------------------------

/** \brief The main Step switch. */
void Dormouse_Female::Step()
{
	if (m_StepDone || m_CurrentStateNo == -1) return;  // If already done or dead - do nothing
	// ... otherwise enter main switch:
	// Main switch: 
	switch (CurrentDorState)
	{
	case toDormouses_InitialState:  // Initial state always starts with ...
		CurrentDorState = st_Hibernate();  // Returns Hibernate or ReproBehaviour
		break;
	case toDormouses_ReproBehaviour:
		CurrentDorState = st_ReproBehaviour();   // Returns Lactating, Mating or Gestation
		break;
	case toDormouses_Lactating:
		CurrentDorState = st_Lactating();  // Returns Lactating or Move
		m_StepDone = true;
		break;
	case toDormouses_Mating:
		CurrentDorState = st_Mating(); // Should return Gestation, GetFat or Mating
		break;
	case toDormouses_Gestation:
		CurrentDorState = st_UpdateGestation(); // Will return give birth or Gestation
		break;
	case toDormouses_GiveBirth:
		CurrentDorState = st_GiveBirth(); // Will return ReproBehaviour or Move
		m_StepDone = true;
		break;
	case toDormouses_Move:
		CurrentDorState = st_Explore(); // Will return Reprobehaviour
		m_StepDone = true;
		break;
	case toDormouses_GetFat:
		CurrentDorState = st_GetFat();  // Will return Hibernation or GetFat
		m_StepDone = true;
		break;
	case toDormouses_Hibernate:
		CurrentDorState = st_Hibernate();  // Will return Hiberation, Die or Reprobehaviour
		break;
	case toDormouses_Die:
		st_Dying();  // No return value - no behaviour after this
		m_StepDone = true;
		break;
	default:
		m_OurLandscape->Warn("Dormouse_Female::Step()", "unknown state - default");
		exit(1);
	}	
}
//-------------------------------------------------------------------------------------------

/** \brief Female hibernation. */
TTypeOfDormouseState Dormouse_Female::st_Hibernate()
{
	int today = m_OurPopulationManager->m_TheLandscape->SupplyDayInYear();
	if ((today >= cfg_HibernationEndFemale.value()) && (today <= cfg_HibernationStart.value()))
	{
		m_Hibernating = false;
		return toDormouses_ReproBehaviour;
	}
	else
	{
		m_StepDone = true;
		m_Hibernating = true;
		return toDormouses_Hibernate;
	}
}
//-------------------------------------------------------------------------------------------

/** \brief Reproductive switch. */
/**
* This state is simply a switch determing what behaviour to exhibit.
*/
TTypeOfDormouseState Dormouse_Female::st_ReproBehaviour()
{
	if (m_NoOfYoung > 0) return toDormouses_Lactating;  //Go to lactating
	else if ((!m_Pregnant) && (m_DaysUntilBirth == 0)) return toDormouses_Mating; // Go to Mating
	else return toDormouses_Gestation; // Go to UpdateGestation
}
//-------------------------------------------------------------------------------------------

/** \brief Female mating. */
TTypeOfDormouseState Dormouse_Female::st_Mating()
{
	int today = m_OurLandscape->SupplyDayInYear();
	if (today >= cfg_FatteningPeriodStart.value())	return toDormouses_GetFat;  /**@todo LADA: Follow up here. Potential issues if a litter exists on this date */
	//if it is the breeding season
	if (m_BreedingSeason && m_Have_Territory)
	{
		if (m_Mated)
		{
			m_Pregnant = true;
			m_DaysUntilBirth = TheGestationPeriod;
			return toDormouses_Gestation;
		}
		else
		{  // if there is no male, then stay in the territory (hopefully someone will come by...)
			m_Pregnant = false;
			m_StepDone = true;
			return toDormouses_ReproBehaviour;
		}
	}
	else if (!m_BreedingSeason && m_Have_Territory)
	{
		m_Pregnant = false;
		m_StepDone = true;
		return toDormouses_ReproBehaviour;
	}
	else if (m_BreedingSeason && !m_Have_Territory )
	{
		m_Pregnant = false;
		return toDormouses_Move;
	}
	else
	{
		m_Pregnant = false;
		return toDormouses_Move;
	}
}
//-------------------------------------------------------------------------------------------

/** \brief Gestation control. */
/** Decreases the number of days until birth by 1. */
TTypeOfDormouseState Dormouse_Female::st_UpdateGestation()
{
	m_DaysUntilBirth--;
	if (m_DaysUntilBirth == 0) return toDormouses_GiveBirth;
	/*else if (m_DaysUntilBirth < 0)
	{
		int rubbish = 0;
	}*/
	else return toDormouses_Move;
}
//-------------------------------------------------------------------------------------------

/** \brief Female gets juveniles as an attribute of herself */
TTypeOfDormouseState Dormouse_Female::st_GiveBirth()
{
	if (m_Pregnant)  /**@todo LADA: This step could also check if the female has got a territory */
	{
		// Must produce young
		//m_NoOfYoung = random(4);  /**@todo LADA: This should be randomly sampled from a normal distribution with mean 4 - or something...*/
		m_NoOfYoung = int( round(variate_gen()));  /**@todo LADA: Testing a draw from a normal distribution*/
		m_Pregnant = false;
		// Must age young
		m_YoungAge = 0;
		// Update the YoungProduced today counter
		m_OurPopulationManager->AddToYoung(m_NoOfYoung);
		m_NoOfYoungTotal += m_NoOfYoung;
		return toDormouses_ReproBehaviour;
	}
	else
	{
		m_NoOfYoung = 0;
	}
	m_Pregnant = false;
	return toDormouses_Move;
}
//-------------------------------------------------------------------------------------------

/** \brief Female dispersal. */
//TTypeOfDormouseState Dormouse_Female::st_Dispersal()
//{
//	if (m_DispVector == -1) m_DispVector = random(8); // Choose direction 0-7
//	// Store the current location in case we want to go back:
//	int oldx = m_Location_x;
//	int oldy = m_Location_y;
//	// What is the quality of the current territory?:
//	double OldQual = CalculateQuality(m_Location_x, m_Location_y, cfg_DorDensityDepConst.value());
//	if (OldQual < m_MinFDorHabQual)
//	{
//		m_Have_Territory = false;  // if the quality is too low they cant have a territory.
//		m_DaysInUnsuitable++;  //... and too long without a territory is no-go. So add to the counter which eventually will trigger death.
//		int p_Distance = random(FemaleMovement) + MinFemaleMovement;  // Get a distance
//		MoveTo(m_DispVector, p_Distance, 10);  /** @todo LADA: Here 10 is "iterations", need to check up on what that actually is doing... */
//		double NewQual = CalculateQuality(m_Location_x, m_Location_y, cfg_DorDensityDepConst.value());
//		if (NewQual < OldQual)
//		{
//			// Don't want to move
//			FreeLocation();
//			m_Location_x = oldx;
//			m_Location_y = oldy;
//			SetLocation();
//		}
//		else if (NewQual >= m_MinFDorHabQual)
//		{
//			m_Have_Territory = true;
//		}
//	}
//	else m_Have_Territory = true;
//	return toDormouses_ReproBehaviour;
//}
//-------------------------------------------------------------------------------------------

/** \brief Find territory. */
TTypeOfDormouseState Dormouse_Female::st_Explore()
{	// The dormouse must find  good territory.
	// He will explore in all 8 directions from the nest and pick the best from these and his present position
	unsigned coords[9][2];
	double qualities[9];
	// Check the quality in the starting location:
	coords[8][0] = m_Location_x;
	coords[8][1] = m_Location_y;
	if (m_OurPopulationManager->SupplyOlderFemales(m_Location_x, m_Location_y, m_Age, m_TerrRange))
	{
		qualities[8] = -1;
	}
	else
	{
		qualities[8] = CalculateQuality(m_Location_x, m_Location_y, cfg_DorDensityDepConst.value());
	}
	// Now check the quality in all 8 directions
	unsigned p_Distance = random(FemaleMovement) + FemaleMovement;  // Max 2 times FemaleMovement
	for (unsigned i = 0; i < 8; i++)
	{
		MoveTo(i, p_Distance, 10); // changes m_Location_x & y
		coords[i][0] = m_Location_x;
		coords[i][1] = m_Location_y;
		if (m_OurPopulationManager->SupplyOlderFemales(m_Location_x, m_Location_y, m_Age, m_TerrRange))
			qualities[i] = -1;
		else
		{
			qualities[i] = CalculateQuality(m_Location_x, m_Location_y, cfg_DorDensityDepConst.value());
		}
	}
	// find the best out of the nine qualities
	double best = qualities[8]; // starting location
	unsigned found = 8; // default stay here
	for (unsigned i = 0; i<8; i++)
	{
		if (qualities[i]>best)
		{
			best = qualities[i];
			found = i;
		}
		if (best > 1)  // This is asking whether an older female was present or not (would be -1 if the case).
		{
			if (best >= m_MinFDorHabQual)  // Only if the habitat is above the minimum threshold can the indv. have a territory.
			{
				m_DaysInUnsuitable = 0;
				m_Have_Territory = true;
			}
		}
		else
		{
			m_DaysInUnsuitable++;
			m_Have_Territory = false;
		}
		FreeLocation();
		m_Location_x = coords[found][0];
		m_Location_y = coords[found][1];
		SetLocation();
	}
	return toDormouses_ReproBehaviour;
}
//-------------------------------------------------------------------------------------------

/** \brief Lactation. 
Female lactates the litter until end of weaning period where juveniles start moving around independenly. */
/**
Once the litter reaches weaning age then individual dourmice are created and set in motion by this method.
*/
TTypeOfDormouseState Dormouse_Female::st_Lactating()
{
	if (m_YoungAge++ >= WeanedAge)
	{
		struct_Dormouse* ad;
		ad = new struct_Dormouse;
		ad->DPM = m_OurPopulationManager;
		ad->L = m_OurLandscape;
		ad->hibernating = m_Hibernating;  // Give juveniles same hibernation status as their mother (i.e. false)
		ad->age = m_YoungAge;  // Give juvs the age of the litter.
		// Create the new dourmice (50% chance of male/female)  // Normally close to 1:1 (Juskaitis 1997 Nat Croat.)
		for (int i = 0; i < m_NoOfYoung; i++)
		{
			ad->x = ((m_Location_x + random(10))%m_OurPopulationManager->SimW);
			ad->y = ((m_Location_y + random(10))%m_OurPopulationManager->SimH);

			if (random(2) == 1) // Even sex ratio
			{
				// Males
				m_OurPopulationManager->CreateObjects(dob_JuvenileMale, this, ad, 1);
			}
			else
			{
				// Females
				m_OurPopulationManager->CreateObjects(dob_JuvenileFemale, this, ad, 1);
			}
		}
		m_OurPopulationManager->AddToJuvs(m_NoOfYoung);
		m_NoOfYoung = 0; // No more young to feed
		delete ad; // clean up
		return toDormouses_Move; //  
	}
	else return toDormouses_Lactating; // carry on feeding young
}
//-------------------------------------------------------------------------------------------